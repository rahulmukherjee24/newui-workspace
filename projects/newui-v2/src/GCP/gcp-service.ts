import { LocalStorageService } from 'ngx-webstorage';
import { MuleEndpointsWithModuleAndOperation, MuleEndpointsWithoutModuleAndOperation } from './mule-endpoints';
import { Injectable } from '@angular/core';
import { environment } from './../environments/environment';

@Injectable()
export class GcpService {

    private GCP_ENDPOINT: string;
    private region: string;
    // getGcpEnvironment will return prod if region falls in this array.
    public prodRegionList: Array<string> = ['australia', 'hongkong', 'indonesia'];

    constructor(private _ls: LocalStorageService) {
        this.init();
    }

    private init(): void {
        let setupInfo = this._ls.retrieve('SETUP_INFO');
        if (setupInfo) {
            this.region = setupInfo.regionCode.code;
            this.setBaseURL(setupInfo.regionCode.code);
        }
    }

    public setBaseURL(region: string): void {
        this.GCP_ENDPOINT = 'icabsui-api-' + this.getGcpRegion(region) + '-' + this.getGcpEnvironment(environment['NODE_ENV'], region) + '.appspot.com/api/nextgenui/1.0/api/x/';
    }

    public isGcpEndpoint(method: string, module: string, operation: string): boolean {
        return !(this.isMuleEndpointWithModuleAndOperation(method, module, operation)
            || this.isMuleEndpointWithoutModuleAndOperation(method));
    }

    public getGcpBaseUrl(method?: string): string {
        return 'https://' + this.GCP_ENDPOINT;
    }

    public getNavisionUrl(): string {
        return 'https://reserve-stock-service-dot-icabsui-api-' + this.getGcpRegion(this.region) + '-' + this.getGcpEnvironment(environment['NODE_ENV']) + '.appspot.com/api/nextgenui/1.0/api/x/' + 'reserve/stock?';
    }

    public getReserveStockUrl(): any {
        const setupInfo = this._ls.retrieve('SETUP_INFO');
        return 'https://reserve-stock-service-dot-icabsui-api-' + this.getGcpRegion(setupInfo.regionCode.code) + '-' + this.getGcpEnvironment(environment['NODE_ENV']) + '.appspot.com/api/nextgenui/1.0/api/x/';
    }

    public getExportAPIUrl(): string {
        const setupInfo = this._ls.retrieve('SETUP_INFO');
        return 'https://export-to-sheets-dot-icabsui-api-' + this.getGcpRegion(setupInfo.regionCode.code) + '-' + this.getGcpEnvironment(environment['NODE_ENV'], setupInfo.regionCode.code) + '.appspot.com/api/nextgenui/1.0/exporttosheet/icabs';
    }

    private getGcpServiceForMethod(method: string, providedGcpService: string): string {
        if (providedGcpService === 'default') {
            return '';
        }
        return (providedGcpService
            ? providedGcpService
            : (method.split('/'))[0]) + '-dot-';
    }

    private isMuleEndpointWithModuleAndOperation(method: string, module: string, operation: string): boolean {
        return (MuleEndpointsWithModuleAndOperation[method]
            && MuleEndpointsWithModuleAndOperation[method][module]
            && MuleEndpointsWithModuleAndOperation[method][module][operation]);
    }

    private isMuleEndpointWithoutModuleAndOperation(path: string): boolean {
        return (MuleEndpointsWithoutModuleAndOperation[path]
            && MuleEndpointsWithoutModuleAndOperation[path]['useGcp']);
    }

    private getGcpEnvironment(currentEnv: string, region?: string): string {
        switch (currentEnv.toLowerCase()) {
            case 'dev':
                return 'dev';
            case 'devonqa':
            case 'qa':
                return 'test';
            case 'devonsg':
            case 'staging':
                return 'staging';
            case 'devontraining':
            case 'training':
                return 'staging2';
            case 'prod':
                return region && this.prodRegionList.indexOf(region.toLowerCase()) >= 0 ? 'prod' : 'production';
            default:
                return 'dev';
        }
    }

    private getGcpRegion(chosenRegion: string): string {
        switch (chosenRegion.toLowerCase()) {
            case 'asia':
                return 'asia';
            case 'india':
            case 'asiawest':
                return 'india';
            case 'india2':
                return 'india2';
            case 'pacific':
            case 'australia':
                return 'australia';
            case 'hongkong':
                return 'hongkong';
            case 'indonesia':
                return 'indonesia';
            case 'north america':
                return 'us';
            default:
                return 'europe';
        }
    }
}
