import { ITaskWorker } from './app-worker';

export class HttpWorker implements ITaskWorker {
    public static readonly c_s_KEY_PREFIX: string = 'ng2-webstorage|';

    private URL: string;
    private http: XMLHttpRequest;
    private token: string;
    private ignoreSecretKey: boolean;

    constructor() {
        this.http = new XMLHttpRequest();
    }

    public process = (params: any): any => {
        this.token = params.token;
        this.ignoreSecretKey = params.useClientId || false;
        this.URL = (!params.base ? params.url : params.base + params.url);
        if (!params.method || params.method === 'GET') {
            return this.doGetPromise({
                search: params.search
            });
        } else if (params.method === 'POST') {
            return this.doPostPromise({
                search: params.search,
                formData: params.formData,
                postHeaders: params.postHeaders
            });
        } else {
            console.error('Unrecognized HTTP Method; Only GET And Post Allowed.');
        }
    }

    public doGetPromise = (options: any): Promise<any> => {
        return new Promise((resolve, reject) => {
            this.doGet(resolve, reject, options);
        });
    }

    public doPostPromise = (options: any): Promise<any> => {
        return new Promise((resolve, reject) => {
            this.doPost(resolve, reject, options);
        });
    }

    private doGet = (resolve: any, reject: any, options: any) => {
        try {
            let queryString: string = '';
            let url: string = this.URL;

            //Append Search Params
            if (options.search && Object.keys(options.search).length) {
                url += '?';
            }
            for (let key in options.search) {
                if (options.search.hasOwnProperty(key)) {
                    if (queryString) {
                        queryString += '&';
                    }
                    queryString += key + '=' + options.search[key];
                }
            }

            this.http.open('GET', url + queryString);
            this.setSecurityHeaders();

            this.http.onreadystatechange = (data) => {
                let response: any = data.target;
                if (4 === response['readyState']) {
                    resolve(JSON.parse(response.responseText));
                }
            };
            this.http.onerror = reject;

            this.http.send();
        } catch (excp) {
            console.log(excp);
        }
    }

    private doPost = (resolve: any, reject: any, options: any) => {
        try {
            let queryString: string = '';
            let url: string = this.URL;
            //Append Search Params
            if (options.search && Object.keys(options.search).length) {
                url += '?';
            }
            for (let key in options.search) {
                if (options.search.hasOwnProperty(key)) {
                    if (queryString) {
                        queryString += '&';
                    }
                    queryString += key + '=' + options.search[key];
                }
            }
            this.http.open('POST', url + queryString);
            let requestBody: string = '';
            if (options.postHeaders['module']) {
                for (let key in options.formData) {
                    if (options.formData.hasOwnProperty(key)) {
                        if (requestBody) {
                            requestBody += '&';
                        }
                        requestBody += encodeURIComponent(key) + '=' + encodeURIComponent(options.formData[key]);
                    }
                }
            } else {
                requestBody = options.formData;
            }
            this.setSecurityHeaders();
            this.setPostHeaders(options.postHeaders);

            this.http.onreadystatechange = (data) => {
                let response: any = data.target;
                if (4 === response['readyState']) {
                    resolve(JSON.parse(response.responseText));
                }
            };
            this.http.onerror = reject;

            this.http.send(requestBody);
        } catch (excp) {
            console.log(excp);
        }
    }

    private setPostHeaders = (postHeaders: any) => {
        if (postHeaders.module) {
            this.http.setRequestHeader('Module', postHeaders.module);
        }
        if (postHeaders.operation) {
            this.http.setRequestHeader('Operation', postHeaders.operation);
        }
        this.http.setRequestHeader('Content-Type', postHeaders['Content-Type'] || 'application/x-www-form-urlencoded');
    }

    private setSecurityHeaders = () => {
        this.http.setRequestHeader('Authorization', 'Bearer ' + this.token);
        if (!this.ignoreSecretKey) {
            this.http.setRequestHeader('client_id', self['environment']['MULE_CLIENT_ID']);
            this.http.setRequestHeader('client_secret', self['environment']['MULE_CLIENT_SECRET']);
        }
    }
    public sendPromise(data: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            resolve(data);
        });
    }
}

export class PromiseHandler {
    public sendPromise(data: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            resolve(data);
        });
    }
}
