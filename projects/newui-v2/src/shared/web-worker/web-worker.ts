import { IWebWorkerService } from './web-worker.interface';

export class WebWorkerService implements IWebWorkerService {
    private workerFunctionToUrlMap = new WeakMap<Function, string>();
    private promiseToWorkerMap = new WeakMap<Promise<any>, Worker>();
    private dependencies: any[];
    private expectsPromise: boolean;
    public worker: Worker;

    constructor(...args: any[]) {
        this.dependencies = args;
    }

    run<T>(workerFunction: (input: any) => T, data?: any, expectsPromise?: boolean): Promise<T> {
        this.expectsPromise = expectsPromise || false;
        const url = this.getOrCreateWorkerUrl(workerFunction);
        return this.runUrl(url, data);
    }

    runUrl(url: string, data?: any): Promise<any> {
        this.worker = new Worker(url);
        const promise = this.createPromiseForWorker(this.worker, data);
        const promiseCleaner = this.createPromiseCleaner(promise);

        this.promiseToWorkerMap.set(promise, this.worker);

        promise
            .then(promiseCleaner)
            .catch(promiseCleaner);

        return promise;
    }

    terminate<T>(promise: Promise<T>): Promise<T> {
        return this.removePromise(promise);
    }

    getWorker(promise: Promise<any>): Worker {
        return this.promiseToWorkerMap.get(promise);
    }

    private createPromiseForWorker<T>(worker: Worker, data: any): any {
        return new Promise<T>((resolve, reject) => {
            worker.addEventListener('message', (event) => resolve(event.data));
            worker.addEventListener('error', reject);
            worker.postMessage(data);
        });
    }

    private getOrCreateWorkerUrl(fn: Function): string {
        if (!this.workerFunctionToUrlMap.has(fn)) {
            const url = this.expectsPromise ? this.createWorkerUrlForPromise(fn) : this.createWorkerUrl(fn);
            this.workerFunctionToUrlMap.set(fn, url);
            return url;
        }
        return this.workerFunctionToUrlMap.get(fn);
    }

    private createWorkerUrl(resolve: Function): string {
        const resolveString = resolve.toString();
        let webWorkerTemplate: string = `
            self.addEventListener('message', function(e) {
                postMessage((${resolveString})(e.data));
            });
        `;

        webWorkerTemplate = this.addDependencies(webWorkerTemplate);

        const blob = new Blob([webWorkerTemplate], { type: 'text/javascript' });
        return URL.createObjectURL(blob);
    }

    private createWorkerUrlForPromise(resolve: Function): string {
        const resolveString = resolve.toString();
        let webWorkerTemplate: string = '';

        webWorkerTemplate += this.addDependencies(webWorkerTemplate);
        webWorkerTemplate += `
            self.addEventListener('message', function(e) {
                let task = new TaskWorker();
                task.process(e.data).then(data => {
                    postMessage(data);
                }).catch(error => {
                    postMessage(error);
                });
            });
        `;

        const blob = new Blob([webWorkerTemplate], { type: 'text/javascript' });
        return URL.createObjectURL(blob);
    }

    private addDependencies(template: string): string {
        let dependants: string = '';
        if (this.dependencies.length !== 1) {
            this.dependencies.forEach((val, index) => {
                if (val) {
                    let name: string = val.name || 'environment';
                    let value: string = typeof val === 'function' ? val.toString() : JSON.stringify(val);
                    dependants += 'self.' + name + ' = ' + value + ';';
                }
            });
        }

        dependants += 'self.TaskWorker = ' + this.dependencies[0].toString() + ';';

        return template + dependants;
    }

    private createPromiseCleaner<T>(promise: Promise<T>): (input: any) => T {
        return (event) => {
            this.removePromise(promise);
            return event;
        };
    }

    private removePromise<T>(promise: Promise<T>): Promise<T> {
        const worker = this.promiseToWorkerMap.get(promise);
        if (worker) {
            worker.terminate();
        }
        this.promiseToWorkerMap.delete(promise);
        return promise;
    }
}
