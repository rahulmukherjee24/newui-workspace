import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { flatMap } from 'rxjs/operators';

import { LookUp } from './lookup';
import { ServiceConstants } from './../constants/service.constants';
import { Utils } from './utility';
import { RiExchange } from './riExchange';
import { GlobalizeService } from './globalize.service';
import { MntConst } from './riMaintenancehelper';

@Injectable()
export class CommonLookUpUtilsService {
    constructor(
        private serviceConstants: ServiceConstants,
        protected utils: Utils,
        protected LookUp: LookUp,
        protected riExchange: RiExchange,
        protected globalize: GlobalizeService
    ) {
    }

    public getEmployeeSurname(employeeCode: string, businessCode?: string): Promise<any> {
        let query: any = {};
        query['EmployeeCode'] = employeeCode;
        query[this.serviceConstants.BusinessCode] = businessCode ? businessCode : this.utils.getBusinessCode();
        let queryData = [
            {
                'table': 'Employee',
                'query': query,
                'fields': ['EmployeeSurname']
            }
        ];
        return this.LookUp.lookUpPromise(queryData);
    }

    public getContractName(contractNumber: string, businessCode?: string, accountNumber?: string): Promise<any> {
        let query: any = {};
        query['ContractNumber'] = contractNumber;
        if (accountNumber) {
            query['AccountNumber'] = accountNumber;
        }
        query[this.serviceConstants.BusinessCode] = businessCode ? businessCode : this.utils.getBusinessCode();
        let queryData = [
            {
                'table': 'Contract',
                'query': query,
                'fields': ['ContractName']
            }
        ];
        return this.LookUp.lookUpPromise(queryData);
    }

    public getPremiseName(premiseNumber: string, businessCode?: string, ...params: string[]): Promise<any> {
        let query: any = {};
        query['PremiseNumber'] = premiseNumber;
        query['ContractNumber'] = params[0];
        query[this.serviceConstants.BusinessCode] = businessCode ? businessCode : this.utils.getBusinessCode();
        let queryData = [
            {
                'table': 'Premise',
                'query': query,
                'fields': ['PremiseName']
            }
        ];
        return this.LookUp.lookUpPromise(queryData);
    }

    public getProductCode(productCode: string, businessCode?: string): Promise<any> {
        let query: any = {};
        query['ProductCode'] = productCode;
        query[this.serviceConstants.BusinessCode] = businessCode ? businessCode : this.utils.getBusinessCode();
        let queryData = [
            {
                'table': 'Product',
                'query': query,
                'fields': ['ProductDesc']
            }
        ];
        return this.LookUp.lookUpPromise(queryData);
    }

    public getReportGroup(reportGroupCode?: string): Promise<any> {
        let query: any = {};
        query[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        if (reportGroupCode) {
            query['ReportGroupCode'] = reportGroupCode;
        }
        let queryData = [
            {
                'table': 'ReportGroup',
                'query': query,
                'fields': ['ReportGroupCode', 'ReportGroupDesc']
            }
        ];
        return this.LookUp.lookUpPromise(queryData);
    }

    public getRegionDesc(branchNumber?: string): Observable<any> {
        let query: any = {};
        query['BranchNumber'] = branchNumber || this.utils.getBranchCode();
        query[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        let queryData = [
            {
                'table': 'Branch',
                'query': query,
                'fields': ['RegionCode']
            }
        ];
        return this.LookUp.lookUpRecord(queryData).pipe(
            flatMap(data => {
                query[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
                if (data && data[0] && data[0][0]) {
                    query['RegionCode'] = data[0][0].RegionCode;
                    queryData = [
                        {
                            'table': 'Region',
                            'query': query,
                            'fields': ['RegionCode', 'RegionDesc']
                        }];
                    return this.LookUp.lookUpRecord(queryData);
                }
            }));
    }

    public getBranchName(branch: string): Promise<any> {
        let branchNameQuery: Array<any> = [{
            'table': 'Branch',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(), 'BranchNumber': branch
            },
            'fields': ['BranchName']
        }];
        return this.LookUp.lookUpPromise(branchNameQuery);
    }

    public getRegionDescFromCode(regionCode: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'Region',
            'query': {
                'RegionCode': regionCode
            },
            'fields': ['RegionDesc']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public setTradingDates(form: FormGroup, fromDateField: string, toDateField: string, businessCode?: string): void {
        const salesTrading: Array<any> = [{
            'table': 'Business',
            'query': {
                'BusinessCode': businessCode ? businessCode : this.utils.getBusinessCode()
            },
            'fields': ['SalesTradingYear', 'SalesTradingMonth']
        }];
        this.LookUp.lookUpPromise(salesTrading).then((data) => {
            if (data && data[0] && data[0].length) {
                const salesTradingMonth: number = data[0][0].SalesTradingMonth;
                const salesTradingYear: number = data[0][0].SalesTradingYear;
                const fromDate: Date = new Date(salesTradingYear, salesTradingMonth - 1, 1);
                const toDate: Date = new Date(salesTradingYear, salesTradingMonth, 0);
                this.riExchange.riInputElement.SetValue(form, fromDateField, this.globalize.parseDateToFixedFormat(fromDate) as string, MntConst.eTypeDate);
                this.riExchange.riInputElement.SetValue(form, toDateField, this.globalize.parseDateToFixedFormat(toDate) as string, MntConst.eTypeDate);
            } else {
                this.riExchange.riInputElement.SetValue(form, fromDateField, '', MntConst.eTypeDate);
                this.riExchange.riInputElement.SetValue(form, toDateField, '', MntConst.eTypeDate);
            }
        });
    }

    public getSalesAreaDesc(area: string, branch: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'SalesArea',
            'query': {
                'SalesAreaCode': area, 'BranchNumber': branch
            },
            'fields': ['SalesAreaDesc']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getBranchServiceAreaDesc(area: string, branch: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'BranchServiceArea',
            'query': {
                'BranchServiceAreaCode': area, 'BranchNumber': branch
            },
            'fields': ['BranchServiceAreaDesc']

        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getBranchServiceAreaDescWithEmp(area: string, branch: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'BranchServiceArea',
            'query': {
                'BranchServiceAreaCode': area, 'BranchNumber': branch
            },
            'fields': ['BranchServiceAreaDesc', 'EmployeeCode']

        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getTurnoverTradingDates(businessCode?: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'Business',
            'query': {
                'BusinessCode': businessCode ? businessCode : this.utils.getBusinessCode()
            },
            'fields': ['TurnoverTradingYear', 'TurnoverTradingMonth']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getTierCode(businessCode: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'Tier',
            'query': {
                'BusinessCode': businessCode
            },
            'fields': ['TierCode', 'TierSystemDescription']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getWasteTransferDesc(WasteTransferTypeCode: string, businessCode?: string): Promise<any> {
        let query: any = {};
        query['WasteTransferTypeCode'] = WasteTransferTypeCode;
        query[this.serviceConstants.BusinessCode] = businessCode ? businessCode : this.utils.getBusinessCode();
        let queryData = [
            {
                'table': 'WasteTransferType',
                'query': query,
                'fields': ['WasteTransferTypeDesc']
            }
        ];
        return this.LookUp.lookUpPromise(queryData);
    }

    public getEWCDesc(EWCCode: string, businessCode?: string): Promise<any> {
        let query: any = {};
        query['EWCCode'] = EWCCode;
        query[this.serviceConstants.BusinessCode] = businessCode ? businessCode : this.utils.getBusinessCode();
        let queryData = [
            {
                'table': 'EWCCode',
                'query': query,
                'fields': ['EWCDescription']
            }
        ];
        return this.LookUp.lookUpPromise(queryData);
    }

    public getCompanyDesc(businessCode: string, companyCode: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'Company',
            'query': {
                'BusinessCode': businessCode
            },
            'fields': ['CompanyDesc']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getSystemParameterEndOfWeekDay(businessCode?: string): Promise<any> {
        let sysParameter: Array<any> = [{
            'table': 'SystemParameter',
            'query': {
                'BusinessCode': businessCode ? businessCode : this.utils.getBusinessCode()
            },
            'fields': ['SystemParameterEndOfWeekDay']
        }];
        return this.LookUp.lookUpPromise(sysParameter);
    }

    public getGroupAccountName(groupAccountNumber: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'GroupAccount',
            'query': {
                'GroupAccountNumber': groupAccountNumber
            },
            'fields': ['GroupName']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getUserName(userCode: string, businessCode?: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'UserInformation',
            'query': {
                'BusinessCode': businessCode ? businessCode : this.utils.getBusinessCode(),
                'UserCode': userCode

            },
            'fields': ['UserName']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getTeamName(teamId: string, businessCode?: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'Team',
            'query': {
                'BusinessCode': businessCode ? businessCode : this.utils.getBusinessCode(),
                'TeamID': teamId

            },
            'fields': ['TeamSystemName']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getTeamUserName(rowID: string, businessCode?: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'TeamUser',
            'query': {
                'BusinessCode': businessCode ? businessCode : this.utils.getBusinessCode(),
                'ROWID': rowID

            },
            'fields': ['TeamID', 'TeamSystemName', 'UserCode', 'UserName', 'DefaultTeamInd', 'AdministratorInd', 'TeamClosureSecurityLevel']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getAccountName(accountNumber: string, businessCode?: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'Account',
            'query': {
                'BusinessCode': businessCode || this.utils.getBusinessCode(),
                'AccountNumber': accountNumber
            },
            'fields': ['AccountName']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getInvoiceGroupName(invoiceGroupNumber: string, accountNumber: string, businessCode?: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'InvoiceGroup',
            'query': {
                'BusinessCode': businessCode || this.utils.getBusinessCode(),
                'InvoiceGroupNumber': invoiceGroupNumber,
                'AccountNumber': accountNumber
            },
            'fields': ['InvoiceGroupDesc']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getPrepDesc(prepCode: string, businessCode?: string): Promise<any> {
        let query: Array<any> = [{
            'table': 'Prep',
            'query': {
                'BusinessCode': businessCode || this.utils.getBusinessCode(),
                'PrepCode': prepCode
            },
            'fields': ['PrepDesc']
        }];
        return this.LookUp.lookUpPromise(query);
    }

    public getBusinessRegistryValue(businessCode: string, regSection: string, regKey: string): Promise<any> {
        let lookupIP = [{
            'table': 'BusinessRegistry',
            'query': {
                'BusinessCode': businessCode,
                'RegSection': regSection,
                'RegKey': regKey
            },
            'fields': ['BusinessCode', 'RegSection', 'RegKey', 'RegValue', 'EffectiveDate']
        }];

        return this.LookUp.lookUpPromise(lookupIP);
    }

    public getOriginDesc(businessOriginDetailCode: any, businessOriginDetailDesc: any): Observable<any> {
        let query: any = {};
        query['BusinessOriginDetailCode'] = businessOriginDetailCode;
        query['BusinessOriginDetailDesc'] = businessOriginDetailDesc;
        query[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        let queryData = [
            {
                'table': 'BusinessOriginDetailLang',
                'query': query,
                'fields': ['BusinessOriginCode']
            }
        ];
        return this.LookUp.lookUpRecord(queryData).pipe(
            flatMap(data => {
                query[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
                query[this.serviceConstants.LanguageCode] = this.riExchange.LanguageCode();
                if (data && data[0] && data[0][0]) {
                    query['BusinessOriginCode'] = data[0][0].BusinessOriginCode;
                    queryData = [
                        {
                            'table': 'BusinessOriginLang',
                            'query': query,
                            'fields': ['BusinessOriginCode', 'BusinessOriginDesc']
                        }];
                    return this.LookUp.lookUpRecord(queryData);
                }
            }));
    }

    public getBusinessSource(): Promise<any> {
        let LookUp1 =
            [{
                'table': 'BusinessSourceLang',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode(),
                    'LanguageCode': this.riExchange.LanguageCode()
                },
                'fields': ['BusinessSourceCode', 'BusinessSourceDesc']
            }];
        return this.LookUp.lookUpPromise(LookUp1);
    }

    public getEnvironmentalUsageDetails(): Promise<any> {
        let environmentalUsage: Array<any> = [{
            'table': 'EnvironmentalUsage',
            'query': {
                'BusinessCode': this.utils.getBusinessCode()
            },
            'fields': ['EnvironmentCode', 'EnvironmentDesc']
        }];
        return this.LookUp.lookUpPromise(environmentalUsage);
    }

    public getEmpSurNameFromBranchArea(brServiceArea: any, brNumber?: any, businessCode?: any): Observable<any> {
        let lookUpQuery: Array<any> = [{
            'table': 'BranchServiceArea',
            'query': {
                'BranchServiceAreaCode': brServiceArea, 'BranchNumber': brNumber ? brNumber : this.utils.getBranchCode()
            },
            'fields': ['BranchServiceAreaDesc', 'EmployeeCode']

        }];
        return this.LookUp.lookUpRecord(lookUpQuery).pipe(
            flatMap(data => {
                let query = {};
                query['EmployeeCode'] = data[0][0].EmployeeCode;
                query[this.serviceConstants.BusinessCode] = businessCode ? businessCode : this.utils.getBusinessCode();
                lookUpQuery = [
                    {
                        'table': 'Employee',
                        'query': query,
                        'fields': ['EmployeeSurname']
                    }
                ];
                return this.LookUp.lookUpRecord(lookUpQuery);
            }));
    }

    public getHistorydetails(hitoryTypeCode: string): Promise<any> {
        let lookupIP = [{
            'table': 'HistoryTypeLang',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'LanguageCode': this.riExchange.LanguageCode(),
                'HistoryTypeCode': hitoryTypeCode
            },
            'fields': ['HistoryTypeCode', 'HistoryTypeDesc']
        }];

        return this.LookUp.lookUpPromise(lookupIP);
    }

    public getBatchProcessProgramName(uniqueNumber: string): Promise<any> {
        const lookupIP = [{
            'table': 'BatchProcess',
            'query': {
                'BatchProcessUniqueNumber': uniqueNumber
            },
            'fields': ['BatchProcessProgramName']
        }];

        return this.LookUp.lookUpPromise(lookupIP);
    }

}
