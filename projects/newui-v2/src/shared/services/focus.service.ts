import { Injectable } from '@angular/core';

@Injectable()
export class FocusService {
    private elemToBeFocused: Element = null;
    private interval: NodeJS.Timer;
    constructor() { }

    public resetElemToBeFocused(setFocus: boolean): void {
        this.elemToBeFocused = null;
        if (setFocus && !this.interval) {
            this.interval = setInterval(() => {
                this.checkAndSetFocus();
            }, 100);
        }
    }

    private checkAndSetFocus(): void {
        let isSpinnerRunning = false;
        const spinnerElemList = document.querySelectorAll('.spinner-parent-elem .spinner');
        for (let index = 0; index < spinnerElemList.length; index++) {
            const spinnerElem = spinnerElemList[index];
            if (!spinnerElem.hasAttribute('hidden')) {
                isSpinnerRunning = true;
                break;
            }
        }
        if (!isSpinnerRunning && document.querySelectorAll('[bsmodal].in').length <= 0) {
            clearInterval(this.interval);
            this.interval = undefined;
            this.setFocusToFirstElement();
        }
    }

    public setFocusableElement(): void {
        if (
            document.activeElement.tagName.toLowerCase() !== 'body' &&
            !document.activeElement.parentElement.classList.contains('module-title') &&
            !document.activeElement.parentElement.classList.contains('autocomplete-holder')
        ) {
            this.elemToBeFocused = document.activeElement;
        }
    }

    public focusElement(): void {
        if (this.elemToBeFocused && !(this.elemToBeFocused.getAttribute('disabled') || this.elemToBeFocused.classList.contains('disabled'))) {
            this.elemToBeFocused['focus']();
        } else {
            this.setFocusToFirstElement();
        }
    }

    private setFocusToFirstElement(): void {
        setTimeout(() => {
            const formElementList = document.querySelectorAll('form .ui-select-toggle,form input:not([disabled]),form select:not([disabled])');
            for (let l = 0; l < formElementList.length; l++) {
                const el = formElementList[l];
                if (el) {
                    const type: string = el.getAttribute('type');
                    if (
                        (type === undefined) ||
                        (type === null) ||
                        (type && type.toLowerCase() !== 'hidden')
                    ) {
                        setTimeout(() => {
                            this.elemToBeFocused = el;
                            el['focus']();
                        }, 90);
                        break;
                    }
                }
            }
        }, 100);
    }
}
