import { ModalAdvService } from './../components/modal-adv/modal-adv.service';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Injectable } from '@angular/core';

export interface PushNotificationData {
    message: string;
    data: any;
    id?: string;
    type?: string;
    time?: Date;
    displayTime?: String;
}

export interface PushEmitData {
    type: string;
    data: Array<PushNotificationData>;
}

export enum ENotificationType {
    export = '1',
    generic = '2',
    translation = '3',
    import = '4'
}

export enum ETranslationResponseKeys {
    status = 'Status',
    business = 'BusinessCode',
    country = 'CountryCode',
    message = 'info'
}

@Injectable()
export class PushNotificationsService {
    private static readonly c_s_ID_DELIMITER: string = '_';
    private static readonly c_s_MESSAGE_DELIMITER: string = ' - ';
    public static readonly c_o_TYPE_DESC: Record<string, string> = {
        '1': 'Exports',
        '2': 'Generic',
        '3': 'Translations',
        '4': 'Import'
    };

    private list: Array<PushNotificationData> = [];

    public notify: BehaviorSubject<Array<PushEmitData>>;

    constructor(
        private modal: ModalAdvService
    ) {
        this.notify = new BehaviorSubject([]);
    }

    public get notifications(): Array<PushNotificationData> {
        return this.list;
    }

    private getPushTime(time: Date): string {
        return [('0' + time.getHours()).slice(-2),
        ('0' + time.getMinutes()).slice(-2),
        ('0' + time.getSeconds()).slice(-2)].join(':');
    }

    public set push(notification: PushNotificationData) {
        let data: Array<PushEmitData> = [];
        notification.id = (notification.type || ENotificationType.generic)
            + PushNotificationsService.c_s_ID_DELIMITER
            + (Math.floor(Math.random() * 90000000) + 10000000).toString();
        notification.time = new Date();
        notification.displayTime = this.getPushTime(notification.time || new Date());
        this.list.push(notification);

        this.list.sort((item1, item2) => {
            return parseInt(item1.type, 10) - parseInt(item2.type, 10);
        });

        Object.keys(PushNotificationsService.c_o_TYPE_DESC).forEach(key => {
            let temp: Array<PushNotificationData>;
            temp = this.list.filter(item => {
                return key === (item.type || ENotificationType.generic);
            });
            temp.sort((item1, item2) => {
                return item2.time > item1.time ? 1 : -1;
            });
            if (temp.length) {
                data.push({
                    type: PushNotificationsService.c_o_TYPE_DESC[key],
                    data: temp
                });
            }
        });

        this.notify.next(data);
    }

    public handleTranslationData(data: PushNotificationData): void {
        let messages: Array<string> = data.data.map(item => {
            return item[ETranslationResponseKeys.country]
                + PushNotificationsService.c_s_MESSAGE_DELIMITER
                + item[ETranslationResponseKeys.business]
                + PushNotificationsService.c_s_MESSAGE_DELIMITER
                + item[ETranslationResponseKeys.message];
        });

        this.modal.emitMessage({
            msg: messages
        });
    }

    public handleImportData(data: PushNotificationData): void {
        this.modal.emitHTMLMessage({
            msg: data.data
        });
    }

    public handleExportData(data: PushNotificationData): void {
        window.open(data.data, '_blank');
    }

    public onNotifcationClick(id: string): void {
        let data: PushNotificationData = this.list.filter(item => {
            return item.id === id;
        })[0];

        switch (data.type) {
            case ENotificationType.export:
                this.handleExportData(data);
                break;
            case ENotificationType.generic:
                this.modal.emitMessage({
                    msg: data.message
                });
                break;
            case ENotificationType.translation:
                this.handleTranslationData(data);
                break;
            case ENotificationType.import:
                this.handleImportData(data);
                break;
        }
    }
}
