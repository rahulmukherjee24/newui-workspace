import { ErrorConstant } from './../constants/error.constant';
import { HttpClient } from '@angular/common/http';
import { HttpRequestOptions } from './../../app/base/httpRequestOptions';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export enum HTTP_METHOD {
    METHOD_TYPE_POST = 'POST',
    METHOD_TYPE_GET = 'GET',
    METHOD_TYPE_PUT = 'PUT',
    METHOD_TYPE_DELETE = 'DELETE'
}

@Injectable()
export class RestService {

    constructor(private http: HttpClient) {
    }

    private extractData(res: any): Observable<any> {
        let body = res;
        if (typeof body['error_description'] !== 'undefined'
            && body['error_description'].toString().indexOf(ErrorConstant.Message.Invalid) !== -1) {
            return Observable.throw(res);
        }
        if (body && body.hasOwnProperty('oResponse')) {
            if (body.info && body.info.error) {
                body.oResponse = {};
                body.oResponse.errorMessage = body.info.error;
                body.oResponse.hasError = true;
            } else if (body.oResponse && body.oResponse.errorMessage) {
                body.oResponse.hasError = true;
            } else if (body.oResponse && body.oResponse.ErrorMessageDesc) {
                body.oResponse.hasError = true;
                body.oResponse.errorMessage = body.oResponse.ErrorMessageDesc;
            } else if (body.oResponse && body.oResponse.fullError) {
                body.oResponse.hasError = true;
            }
            return body.oResponse;
        }
        return body || {};
    }

    private handleError(error: Response | any): Promise<any> {
        return Promise.reject(error.error.oResponse);
    }

    //Exposed to outside classes / components
    public executeRequest(instance: HttpRequestOptions): Promise<any> {
        let returnValue: any;
        switch (instance.httpMethod) {
            case HTTP_METHOD.METHOD_TYPE_GET:
                returnValue = this.doGet(instance);
                break;
            case HTTP_METHOD.METHOD_TYPE_POST:
                returnValue = this.doPost(instance);
                break;
            case HTTP_METHOD.METHOD_TYPE_PUT:
                returnValue = this.doPut(instance);
                break;
            case HTTP_METHOD.METHOD_TYPE_DELETE:
                returnValue = this.doDelete(instance);
                break;
            default:
                break;
        }
        return returnValue;
    }

    private doPost(instance: HttpRequestOptions): Promise<any> {

        return this.http.post(instance.Url, instance.FormData, instance.Options).toPromise()
            .then((res: any): Promise<any> => {
                let response: any = this.extractData(res);
                return response;
            })
            .catch((error): Promise<any> => {
                return this.handleError(error);
            });
    }

    private doGet(instance: HttpRequestOptions): Promise<any> {

        return this.http.get(instance.Url, instance.Options).toPromise()
            .then((res: any): Promise<any> => {
                let response: any = this.extractData(res);
                return response;
            })
            .catch((error): Promise<any> => {
                return this.handleError(error);
            });
    }

    private doPut(instance: HttpRequestOptions): Promise<any> {

        return this.http.put(instance.Url, instance.FormData, instance.Options).toPromise()
            .then((res: any): Promise<any> => {
                let response: any = this.extractData(res);
                return response;
            })
            .catch((error): Promise<any> => {
                return this.handleError(error);
            });
    }

    private doDelete(instance: HttpRequestOptions): Promise<any> {

        return this.http.delete(instance.Url, instance.Options).toPromise()
            .then((res: any): Promise<any> => {
                let response: any = this.extractData(res);
                return response;
            })
            .catch((error): Promise<any> => {
                return this.handleError(error);
            });
    }
}
