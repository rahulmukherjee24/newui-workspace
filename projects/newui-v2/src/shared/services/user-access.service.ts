import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { MAIN_NAV_DESIGN } from './../components/main-nav/main-nav-design';
import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { GcpService } from './../../GCP/gcp-service';

import 'rxjs/add/operator/map';

@Injectable()
export class UserAccessService {
    private _userAccessData;
    private _accessURL = environment['USER_ACCESS_URL'];
    private _bypass = environment['BYPASS_MULE'];
    private _leftMenuData;
    private _autoCompleteData;
    private muleClientId = environment['MULE_CLIENT_ID'];
    private muleClienSecret = environment['MULE_CLIENT_SECRET'];
    private userMenuPostFix = environment['USER_ACCESS_POSTFIX'];
    private baseURL;

    constructor(private _http: HttpClient, private _ls: LocalStorageService, private gcpService: GcpService) { }

    setUserAccessData(data: any): void {
        if (data) {
            this._userAccessData = data;
        }
    }

    getUserAccessData(): any {
        return this._userAccessData;
    }

    setLeftMenuData(data: any): void {
        if (data) {
            this._leftMenuData = data;
        }
    }

    getLeftMenuData(): any {
        return this._leftMenuData;
    }
    setAutocompleteData(data: any): void {
        if (data) {
            this._autoCompleteData = data;
        }
    }

    getAutocompleteData(): any {
        return this._autoCompleteData;
    }


    getPageBusinessCodeMapping(): any {
        let pages = {};
        if (!this._userAccessData) {
            this._userAccessData = this._ls.retrieve('MENU');
        }
        this._userAccessData.pages.map(function (entry: any): any {
            if (pages[entry.ProgramURL]) {
                pages[entry.ProgramURL].push(entry.BusinessCode);
            } else {
                pages[entry.ProgramURL] = [entry.BusinessCode];
            }
        });
        return pages;
    }

    getPageKeys(): any {
        return Object.keys(this.getPageBusinessCodeMapping());
    }

    getRouteUrlKeys(): any {
        let keys = this.getPageKeys();
        let pageBusinessMapping = this.getPageBusinessCodeMapping();
        let domains = JSON.parse(MAIN_NAV_DESIGN).menu;

        let routes = {};
        for (let i = 0; i < (keys.length); i++) {
            for (let j = 0; j < domains.length; j++) {
                for (let k = 0; k < domains[j].feature.length; k++) {
                    for (let l = 0; l < domains[j].feature[k].module.length; l++) {
                        if (keys[i] === domains[j].feature[k].module[l].programURL) {
                            routes[domains[j].feature[k].module[l].routeURL] = pageBusinessMapping[keys[i]];
                        }
                    }
                }
            }
        }
        return routes;
    }

    setBaseURL(val: string): void {
        this.baseURL = val;
    }
}
