import { DeepLinkService } from './deeplink.service';
import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NGXLogger } from 'ngx-logger';
import { CacheDBConstants } from './../handlers/CacheHandler';
import { AppWebWorker } from './../web-worker/app-worker';
import { WebWorkerService } from './../web-worker/web-worker';
import { HttpWorker } from './../web-worker/http-worker';
import { StaticUtils } from './static.utility';
import { CBBConstants } from '../constants/cbb.constants';
import { CBBService } from './cbb.service';
import { UserAccessService } from './user-access.service';
import { Injectable, Injector, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ErrorService } from '../services/error.service';
import { AjaxObservableConstant } from '../constants/ajax-observable.constant';
import { ErrorConstant } from '../constants/error.constant';
import { GlobalConstant } from '../constants/global.constant';
import { GcpService } from './../../GCP/gcp-service';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/internal/operators/map';

@Injectable()
export class AuthService {
    private _token: any; //TODO - Store in cookie
    private _accessToken: any; //TODO - Store in cookie

    private _client_id = environment['CLIENT_ID'];
    private _scope = environment['SCOPE'];

    private _urlProfile = environment['PROFILE_URL'];
    private _urlRevoke = environment['REVOKE_URL'];
    private _gcpLoginAuth = environment['GCP_LOGIN_API'];
    private _muleLoginAuth = environment['MULE_LOGIN_API'];
    private setupInfo: any;
    private baseURL: string;

    private _error: any = {
        title: '',
        message: ''
    };
    private _timer: any;
    public data: any;

    public ajaxSource = new BehaviorSubject<any>(0);
    public displayName = '';
    public oauthResponse;
    public muleResponse;
    public googleData;
    public userCode;
    public ajaxSource$;
    private cbbService: CBBService;
    private isGapiInitialized: boolean = false;

    public UserObject: Object;
    private deeplink: DeepLinkService;

    constructor(private _http: HttpClient,
        private _ls: LocalStorageService,
        private _router: Router,
        private _logger: NGXLogger,
        private gcpService: GcpService,
        private _errorService: ErrorService,
        private _ajaxconstant: AjaxObservableConstant,
        private userAccessService: UserAccessService,
        private injector: Injector,
        private zone: NgZone,
        private session: SessionStorageService) {
        this.cbbService = this.injector.get(CBBService);
        this.deeplink = this.injector.get(DeepLinkService);
        this.ajaxSource$ = this.ajaxSource.asObservable();
    }

    public signIn(): void {
        if (window['gapi'].auth2 && window['gapi'].auth2.getAuthInstance() && this.isGapiInitialized) {
            window['gapi'].auth2.getAuthInstance().disconnect();
            setTimeout(() => {
                window['gapi'].auth2.getAuthInstance().signIn().then((response) => {
                    //Google OAuth 2 works fine
                    this._logger.log('Google Login Response', response);
                }, (error) => {
                    //Google OAuth 2 occured error
                    this._logger.log('Google Login Error', error);
                });
            }, 200);
        }
    }

    public signOut(): void {
        this.clearData();
        if (window['gapi'].auth2) {
            window['gapi'].auth2.getAuthInstance().disconnect();
        } else {
            this.googleRevoke();
        }
        this._router.navigate(['/application/login']);

    }

    public googleSignOut(): void {
        if (window['gapi'].auth2) {
            window['gapi'].auth2.getAuthInstance().signOut();
        }
        this.clearData();
        this._router.navigate(['/application/login']);
    }

    public googleRevoke(): Observable<any> {
        let url = this._urlRevoke + this._token + '&callback=JSON_CALLBACK';
        return this._http.get(url).pipe(map((res: Response) => {
            res.json();
        }));
    }

    public getProfile(): Observable<any> {
        let headers = new HttpHeaders();
        headers.append('Authorization', this._token);
        return this._http.get(this._urlProfile, { headers: headers }).map((res: Response) => res.json()).do(data => {
            //this._logger.info('Google profile');
            //this._logger.info('All: ' + JSON.stringify(data));
        });
    }

    public getError(): any {
        return this._error;
    }

    public isSignedIn(): boolean {
        if (window['gapi'].auth2 && window['gapi'].auth2.getAuthInstance()) {
            return window['gapi'].auth2.getAuthInstance().isSignedIn.get();
        } else {
            this._token = this._ls.retrieve('TOKEN');
            return (this._token && this._token.length > 10);
        }
    }

    public isTokenAvailable(): boolean {
        this._token = this._ls.retrieve('TOKEN');

        if (this._token && this._token.length > 10) {
            return true;

        } else if (window['gapi'].auth2 && window['gapi'].auth2.getAuthInstance()) {
            return window['gapi'].auth2.getAuthInstance().isSignedIn.get();

        } else {
            return false;
        }
    }

    public getToken(): any {
        let token: string = '';
        let oauth: any = this._ls.retrieve('OAUTH');
        if (new Date() < new Date(oauth.expires_at)) {
            token = this._ls.retrieve('TOKEN');
        } else if (window['gapi'].auth2 && window['gapi'].auth2.getAuthInstance()) {
            let currentUser = window['gapi'].auth2.getAuthInstance().currentUser.get();
            this.oauthResponse = currentUser.getAuthResponse();
            if (this.oauthResponse.id_token) {
                token = this.oauthResponse.id_token;
                this._ls.store('TOKEN', this.oauthResponse.id_token);
                this._ls.store('ACCESS_TOKEN', this.oauthResponse.access_token);
                this._ls.store('OAUTH', this.oauthResponse);
            }
        }
        return token;
        /* if (this._token) {
            if (window['gapi'].auth2 && window['gapi'].auth2.getAuthInstance()) {
                let currentUser = window['gapi'].auth2.getAuthInstance().currentUser.get();
                this.oauthResponse = currentUser.getAuthResponse();
                if (this.oauthResponse.id_token && this.oauthResponse.id_token !== this._token) {
                    this._token = this.oauthResponse.id_token;
                    this._ls.store('TOKEN', this.oauthResponse.id_token);
                    this._ls.store('ACCESS_TOKEN', this.oauthResponse.access_token);
                    this._ls.store('OAUTH', this.oauthResponse);
                }
            }
            return this._token;
        } else {
            let token = this._ls.retrieve('TOKEN');
            if (token) {
                return token;
            } else {
                return null;
            }
        } */
    }

    public getAccessToken(): any {
        if (this._accessToken) {
            return this._accessToken;
        } else {
            let accessToken = this._ls.retrieve('ACCESS_TOKEN');
            if (accessToken) {
                return accessToken;
            } else {
                return null;
            }
        }
    }

    public getSavedUserCode(): any {
        let userCode = this._ls.retrieve('USERCODE');
        if (userCode) {
            return userCode;
        } else {
            return null;
        }
    }

    public getMuleResponse(): boolean {
        if (this.muleResponse) {
            return this.muleResponse;
        } else {
            let muleData = this._ls.retrieve('MULE');
            if (muleData) {
                return muleData;
            } else {
                return null;
            }
        }
    }

    public getSavedEmail(): string {
        if (this.getGoogleData()) {
            return this.getGoogleData().email;
        } else {
            return '';
        }
    }

    public getGoogleData(): any {
        if (this.googleData) {
            return this.googleData;
        } else {
            let googleData = this._ls.retrieve('GOOGLEDATA');
            if (googleData) {
                return googleData;
            } else {
                return { locales: [{ value: '' }] };
            }
        }
    }

    public validateToken(): Observable<any> {
        let headers = new HttpHeaders();
        let url = this._muleLoginAuth;
        let authToken = this._token;
        let method = 'auths/login';
        let isGcpEndpoint = this.gcpService.isGcpEndpoint(method, null, null);
        if (isGcpEndpoint) {
            headers = headers.append('token', this._token);
            url = this._gcpLoginAuth;
            authToken = 'Bearer ' + this._token;
        }
        headers = headers.append('Authorization', authToken);
        headers = headers.append('client_id', environment['MULE_CLIENT_ID']);
        headers = headers.append('client_secret', environment['MULE_CLIENT_SECRET']);
        return this._http.post(url, {}, { headers: headers });
    }

    public makeApiCall(): void {
        if (typeof this._timer !== 'undefined') {
            clearTimeout(this._timer);
        }
        this._timer = setTimeout(() => {
            try {
                if (this.isSignedIn()) {
                    this.ajaxSource.next(this._ajaxconstant.START);
                    this.validateToken().subscribe(
                        data => {
                            let checkError = this.checkError(data, 'Login');
                            if (!checkError) {
                                this.muleResponse = data;
                                this.googleData = data;
                                this.displayName = data.name;
                                this._ls.store('GOOGLEDATA', data);
                                this._ls.store('MULE', data);
                                this._ls.store('DISPLAYNAME', data.name);
                                this.zone.run(() => {
                                    this._router.navigate(['/application/setup']);
                                });
                            }
                        },
                        error => {
                            this._error.title = 'Error';
                            this._error.message = ErrorConstant.Message.LoginFail;
                            this._errorService.emitError(this._error);
                            this.ajaxSource.next(this._ajaxconstant.COMPLETE);
                        });
                }
            } catch (err) {
                this._error.title = 'Error';
                this._error.message = ErrorConstant.Message.LoginFail;
                this._errorService.emitError(this._error);
                this.ajaxSource.next(this._ajaxconstant.COMPLETE);
            }
        }, 200);
    }


    public handleClientLoad(initialLoginLoad: any, triggerAPI: any): void {
        // Load the API client and auth2 library
        window['gapi'].load('client:auth2', this.initClient.bind(this, initialLoginLoad, triggerAPI));
    }

    public filterErrors(data: any): any {
        if (data.filter) {
            return data.filter((el: any): any => {
                return (typeof el['errorNumber'] === 'undefined' && typeof el['fullError'] === 'undefined' && typeof el['error'] === 'undefined' && (el['AuthorisedBusinesses'] instanceof Array && el['AuthorisedBusinesses'].length > 0));
            });
        } else {
            return data;
        }
    }

    public getUserAccessDetails(): void {
        this.ajaxSource.next(this._ajaxconstant.START);
        let workerService: WebWorkerService = new WebWorkerService(HttpWorker, environment);
        let worker: AppWebWorker = new AppWebWorker();
        workerService.run(worker.delegatePromise, {
            url: environment['USER_CODE_POSTFIX'],
            method: 'GET',
            token: this._ls.retrieve('token'),
            base: this.getBaseURL(),
            search: {
                email: this._ls.retrieve('GOOGLEDATA').email
            }
        }, true);
        this._router.navigate(['/postlogin']);
        workerService.worker.onmessage = (response) => {
            let data: any = response.data;
            try {
                data = this.filterErrors(data);
                let userCodeData: any;
                let checkError = this.checkError(data, 'UserESB');
                if (!checkError) {
                    // Save Response Data In Store
                    this._ls.store(CBBConstants.c_s_USERCODE_RESPONSE_STORAGE_KEY_NAME, data);
                    /**
                     * Set 0th index of data in userCodeData
                     * If data is not an array, i.e. User has access to only one country
                     * _ create array from data
                     */
                    userCodeData = StaticUtils.deepCopy(data);
                    if (!(data instanceof Array)) {// Create array from data
                        userCodeData = [];
                        userCodeData[0] = StaticUtils.deepCopy(data);
                        data = [data];
                    }
                    // Get User Code Only From Response
                    this.userCode = this.extractUserCode(userCodeData);
                    this._ls.store('USERCODE', this.userCode);
                    this.setUserObj(this.userCode);

                    // Set Response In CBB Service Object Instance
                    this.cbbService.setCBBList(this.cbbService.formatResponsedata(data), true);

                    if (this.deeplink.isURLDeepLinked) {
                        this.deeplink.navigateToDeepLinkPage();
                    }
                }
            } catch (err) {
                this.checkError({
                    error: true
                }, 'User');
            }
        };
    }

    public getBaseURL(): string {
        return this.gcpService.getGcpBaseUrl('user/information');
    }

    /**
     * Method: extractUserCode
     * Extracts user code from user api response
     * Country, Business and Branch information will be discarded
     */
    private extractUserCode(userCode: any): Object {
        let extractedData: any;
        let keysToDelete = [
            CBBConstants.c_s_BUSINESSES,
            CBBConstants.c_s_COUNTRY_NAME
        ];

        if (!userCode) {
            return;
        }
        this._ls.store('RIAllUserCodes', userCode);
        // If User Code Is Not An Array; Convert It To Array
        extractedData = userCode[0];

        // Delete Key/Value From Object
        for (let idx = 0; idx < keysToDelete.length; idx++) {
            delete extractedData[keysToDelete[idx]];
        }
        return extractedData;
    }

    public setUserObj(data: any): void {
        this._ls.store('RIUserCode', data.UserCode);
        this._ls.store('RIUserInfo', data.UserInformation);
        this._ls.store('RIUserTypeCode', data.UserTypeCode);
        this._ls.store('RIUserName', data.UserName);
        this._ls.store('RIUserEmail', data.Email);
        this._ls.store('RICountryCode', data.CountryCode);

        let email: string = this._ls.retrieve('RIUserEmail');
        window['ga']('send', 'userId', StaticUtils.getSHA256Hash({ value: email }));
        window['ga']('set', 'userId', StaticUtils.getSHA256Hash({ value: email }));
    }

    public checkError(data: any, type?: string): boolean {
        if ((typeof data.errorNumber !== undefined && data.errorNumber) || data.error) {
            this._error.title = 'Error';
            if (type) {
                switch (type) {
                    case 'Login':
                        this._error.message = ErrorConstant.Message.LoginFail;
                        break;
                    case 'User':
                        this._error.message = ErrorConstant.Message.UserFailNetwork;
                        break;
                    case 'UserESB':
                        this._error.message = ErrorConstant.Message.UserFailESB;
                        break;
                    case 'Menu':
                        this._error.message = ErrorConstant.Message.MenuFail;
                        break;
                    default:
                        this._error.message = ErrorConstant.Message.UserNotFound;
                        break;
                }
            } else {
                this._error.message = ErrorConstant.Message.UserNotFound;
            }
            this._errorService.emitError(this._error);
            this.ajaxSource.next(this._ajaxconstant.COMPLETE);
            this.clearData();
            return true;
        } else {
            return false;
        }
    }

    private initClient(initialLoginLoad: any, triggerAPI: any): void {
        setTimeout(() => {
            window['gapi'].client.init({
                discoveryDocs: ['https://people.googleapis.com/$discovery/rest?version=v1'],
                clientId: this._client_id,
                scope: this._scope
            }).then(() => {
                try {
                    this.isGapiInitialized = true;
                    window['gapi'].auth2.getAuthInstance().isSignedIn.listen(this.updateSigninStatus.bind(this));
                    this.updateSigninStatus(this.isSignedIn(), initialLoginLoad, triggerAPI);
                } catch (err) {
                    this._error.title = 'Error';
                    this._error.message = ErrorConstant.Message.OtherLoginTimeFail;
                    this._errorService.emitError(this._error);
                }
            });
        }, 500);
    }

    private updateSigninStatus(isSignedIn: boolean, initialLoginLoad: boolean, triggerAPI: boolean): void {
        if (isSignedIn && !initialLoginLoad) {
            let currentUser = window['gapi'].auth2.getAuthInstance().currentUser.get();
            this.oauthResponse = currentUser.getAuthResponse();
            this._ls.store('TOKEN', this.oauthResponse.id_token);
            this._ls.store('ACCESS_TOKEN', this.oauthResponse.access_token);
            this._ls.store('OAUTH', this.oauthResponse);
            this._token = this.oauthResponse.id_token;
            this._accessToken = this.oauthResponse.access_token;
            if (!this.session.retrieve(CBBConstants.c_s_URL_PARAM_SYSCHAR)) {
                this.cbbService.getsysCharDetails();
            }
            if (triggerAPI !== false) {
                this.makeApiCall();
            }
        } else {
            this.deeplink.registerDeepLink();
            this.clearData();
            this.ajaxSource.next(this._ajaxconstant.COMPLETE);
            this._router.navigate(['/application/login']);
        }
    }

    public clearData(): void {
        let cacheKey: string = this._ls.retrieve(CacheDBConstants.c_s_STORE_KEY_NAME) || '';
        let cacheVersion: string = this._ls.retrieve(CacheDBConstants.c_s_STORE_KEY_VERSION) || '';
        this.oauthResponse = null;
        this._ls.clear();
        this._ls.store(CacheDBConstants.c_s_STORE_KEY_NAME, cacheKey);
        this._ls.store(CacheDBConstants.c_s_STORE_KEY_VERSION, cacheVersion);
        this.cbbService.clearStore();
        this.session.clear('NAVIGATION_STACK');
        GlobalConstant.Configuration.DefaultTranslation = null;
        this._token = '';
        this._accessToken = '';
    }
}
