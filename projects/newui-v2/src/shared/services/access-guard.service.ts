import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AccessGuardService implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthService) {
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        this.authService.handleClientLoad(false, false);
        if (!this.authService.isTokenAvailable()) {
            this.router.navigate(['/application/login']);
            this.setDisplay('none');
            return false;
        }
        return true;
    }

    private setDisplay(val: string): void {
        document.querySelector('icabs-app .lazy-spinner .spinner')['style'].display = val;
        document.querySelector('icabs-app .ajax-overlay')['style'].display = val;
    }
}
