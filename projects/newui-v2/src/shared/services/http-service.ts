import { ENotificationType, PushNotificationsService } from '@shared/services/push.notification.service';
import { ActivatedRoute } from '@angular/router';
import { ICabsModalVO } from './../components/modal-adv/modal-adv-vo';
import { ModalAdvService } from './../components/modal-adv/modal-adv.service';
import { MessageConstant } from '@shared/constants/message.constant';
import { GlobalNotificationsService } from '@shared/services/global.notifications.service';
import { HttpConstants } from './../../app/base/HttpConstants';
import { ExportConfig } from './../../app/base/ExportConfig';
import { NGXLogger } from 'ngx-logger';
import { environment } from './../../environments/environment';
import { QueryParams } from './http-params-wrapper';
import { SearchRequestCacheWhiteList } from './../handlers/CacheWhiteList';
import { CacheDBHandler, Emitter } from './../handlers/CacheHandler';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { AuthService } from './auth.service';
import { ServiceConstants } from './../constants/service.constants';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpParameterCodec } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { ErrorConstant } from './../constants/error.constant';
import { GlobalConstant } from './../constants/global.constant';
import { GcpService } from './../../GCP/gcp-service';
import { CBBConstants } from '../constants/cbb.constants';
import { map, catchError } from 'rxjs/operators';

/**
 * Encoding Special Characters
 */
class PayloadEncoder implements HttpParameterCodec {
    encodeKey(key: string): string {
        return encodeURIComponent(key);
    }

    encodeValue(value: string): string {
        return encodeURIComponent(value);
    }

    decodeKey(key: string): string {
        return decodeURIComponent(key);
    }

    decodeValue(value: string): string {
        return decodeURIComponent(value);
    }
}

interface IFetchReportURL {
    operation: string;
    search: QueryParams;
    key?: string;
}

enum EDocumentReportConfig {
    c_s_DOCUMENT_REPORT_URL_KEY_DEFAULT = 'url',
    c_s_DOCUMENT_REPORT_METHOD = 'document/reports',
    c_s_DOCUMENT_REPORT_MODULE = 'document-reports',
    c_s_CONFIG_KEY_OPERATION = 'operation',
    c_s_CONFIG_KEY_SEARCH = 'search',
    c_s_CONFIG_KEY_URL_KEY = 'key'
}


@Injectable()
export class HttpService {
    private header: Headers;
    private url: string = '';
    private lookupCounter = 0;
    private setupInfo: any;
    private baseURL: string;
    private dataEventArray: Array<any> = [];
    private isAjaxRunning: boolean = false;
    private timeout: any;
    private cache: CacheDBHandler;
    private readonly c_n_WINDOW_CLOSE_TIME: number = 20;
    private tokenErrorWindowTimeout: any;
    private tokenErrorWindowCounter: number;
    private isNewWindow: boolean = false;

    constructor(private http: HttpClient,
        private _authService: AuthService,
        private gcpService: GcpService,
        private serviceConstants: ServiceConstants,
        private _ls: LocalStorageService,
        private sessionStorageService: SessionStorageService,
        private logger: NGXLogger,
        private notification: GlobalNotificationsService,
        private modalAdvService: ModalAdvService,
        private routeParams: ActivatedRoute,
        private pushNotification: PushNotificationsService) {
        this.init();

        this.cache = new CacheDBHandler(_ls);

        this.routeParams.queryParams.subscribe((data) => {
            if (data[CBBConstants.c_s_URL_PARAM_COUNTRYCODE]) {
                this.isNewWindow = true;
            }
        });
    }

    private init(): void {
        this.setupInfo = this._ls.retrieve('SETUP_INFO');
        if (this.setupInfo) {
            this.setBaseURL(this.setupInfo.regionCode.code);
        }
    }
    public getBaseURL(): string {
        if (!this.setupInfo) {
            this.setupInfo = this._ls.retrieve('SETUP_INFO');
            if (this.setupInfo) {
                this.setBaseURL(this.setupInfo.regionCode.code);
            }
        }
        return this.baseURL;
    }
    public setBaseURL(data: any): void {
        if (data === 'Europe') {
            this.baseURL = environment['BASE_URL_EUROPE'];
        } else if (data === 'America') {
            this.baseURL = environment['BASE_URL_AMERICA'];
        } else if (data === 'Australia') {
            this.baseURL = environment['BASE_URL_AUSTRALIA'];
        } else if (data === 'Pacific') {
            this.baseURL = environment['BASE_URL_PACIFIC'];
        } else if (data === 'Asia') {
            this.baseURL = environment['BASE_URL_ASIA'];
        }
    }

    public getData(url: string, search?: QueryParams): any {
        let options = {};
        options['headers'] = this.createHeaders();
        search.set(this.serviceConstants.email, this._authService.getSavedEmail());
        options['params'] = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: search.getMap()
        });

        return this.http.get(url, options).pipe(map((res: any): Observable<any> => {
            return this.extractData(res);
        }), catchError((error): Observable<any> => {
            return this.handleError(error);
        }));

    }

    public setUrl(url: string): void {
        if (url) {
            this.url = url;
        }
    }

    public clearUrl(): void {
        this.url = '';
    }

    public xhrGet(method: string, moduleAPI: string, operation: string, search: QueryParams): Promise<any> {
        let url: string = this.gcpService.getGcpBaseUrl(method) + method;
        let options = {};
        options['headers'] = this.createHeaders(moduleAPI, operation);
        search.set(this.serviceConstants.email, this._authService.getSavedEmail());
        options['params'] = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: search.getMap()
        });

        // Section for handling the caching
        let cacheKey: string = '';
        if (this.isRequestToBeChached(method, moduleAPI, operation)) {
            cacheKey = this.createCacheKey(method, moduleAPI, operation, search);
            // Check If Already Cached
            if (this.cache.isCached(cacheKey)) {
                let response: Emitter = new Emitter();
                try {
                    this.cache.readItem(cacheKey, response);
                    return response.result.toPromise();
                } catch (excp) {
                    this.logger.warn(excp);
                }
            }
        }

        // Section For Handling The Grid Request For Exporting
        if (search.get(this.serviceConstants.GridMode) || search.get(this.serviceConstants.GridHandle)) {
            this.cacheGridRequest(method, moduleAPI, operation, search);
        }

        document.querySelector('icabs-app .ajax-overlay')['style'].display = 'block';
        this.isAjaxRunning = true;
        return this.http.get(url, options).toPromise()
            .then((res: any): Observable<any> => {
                let response: any = this.extractData(res);
                // Cache if cache key has value
                try {
                    if (cacheKey) {
                        this.cache.setItem(cacheKey, response);
                    }
                } catch (excp) {
                    this.logger.warn(excp);
                }
                return response;
            })
            .catch((error): Observable<any> => {
                let obj: any = this.handleError(error);
                return obj.error;
            });
    }

    public makeGetRequest(method: string, moduleAPI: string, operation: string, search: QueryParams): Observable<any> {
        let url: string = this.gcpService.getGcpBaseUrl(method) + method;
        let options = {};
        options['headers'] = this.createHeaders(moduleAPI, operation);
        search.set(this.serviceConstants.email, this._authService.getSavedEmail());
        options['params'] = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: search.getMap()
        });

        // Section for handling the caching
        let cacheKey: string = '';
        if (this.isRequestToBeChached(method, moduleAPI, operation)) {
            cacheKey = this.createCacheKey(method, moduleAPI, operation, search);
            // Check If Already Cached
            if (this.cache.isCached(cacheKey)) {
                let response: Emitter = new Emitter();
                try {
                    this.cache.readItem(cacheKey, response);
                    return response.result;
                } catch (excp) {
                    this.logger.warn(excp);
                }
            }
        }

        // Section For Handling The Grid Request For Exporting
        if (search.get(this.serviceConstants.GridMode) || search.get(this.serviceConstants.GridHandle)) {
            this.cacheGridRequest(method, moduleAPI, operation, search);
        }

        document.querySelector('icabs-app .ajax-overlay')['style'].display = 'block';
        this.isAjaxRunning = true;
        return this.http.get(url, options).pipe(map((res: any): Observable<any> => {
            let response: any = this.extractData(res);
            // Cache if cache key has value
            try {
                if (cacheKey) {
                    this.cache.setItem(cacheKey, response);
                }
            } catch (excp) {
                this.logger.warn(excp);
            }
            return response;
        }), catchError((error): Observable<any> => {
            return this.handleError(error);
        }));

    }

    public xhrPost(method: string, moduleAPI: string, operation: string, search: QueryParams, form_data: any): Promise<any> {
        let url: string = this.gcpService.getGcpBaseUrl(method) + method;
        let options = {};
        options['headers'] = this.createHeaders(moduleAPI, operation, true);
        search.set(this.serviceConstants.email, this._authService.getSavedEmail());
        options['params'] = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: search.getMap()
        });
        let formData: HttpParams = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: form_data
        });

        // Section for handling the caching
        let cacheKey: string = '';
        if (this.isRequestToBeChached(method, moduleAPI, operation)) {
            cacheKey = this.createCacheKey(method, moduleAPI, operation, search);
            // Check If Already Cached
            if (this.cache.isCached(cacheKey)) {
                let response: Emitter = new Emitter();
                try {
                    this.cache.readItem(cacheKey, response);
                    return response.result.toPromise();
                } catch (excp) {
                    this.logger.warn(excp);
                }
            }
        }

        // Section For Handling The Grid Request For Exporting
        if (search.get(this.serviceConstants.GridMode) || form_data.hasOwnProperty(this.serviceConstants.GridMode) || search.get(this.serviceConstants.GridHandle) || form_data.hasOwnProperty(this.serviceConstants.GridHandle)) {
            this.cacheGridRequest(method, moduleAPI, operation, search, form_data);
        }

        document.querySelector('icabs-app .ajax-overlay')['style'].display = 'block';
        this.isAjaxRunning = true;
        return this.http.post(url, formData.toString(), options).toPromise()
            .then((res: any): Observable<any> => {
                let response: any = this.extractData(res);
                // Cache if cache key has value
                try {
                    if (cacheKey) {
                        this.cache.setItem(cacheKey, response);
                    }
                } catch (excp) {
                    this.logger.warn(excp);
                }
                return response;
            })
            .catch((error): Observable<any> => {
                let obj: any = this.handleError(error);
                return obj.error;
            });
    }


    public makePostRequest(method: string, moduleAPI: string,
        operation: string, search: QueryParams, form_data: any): Observable<any> {
        let url: string = this.gcpService.getGcpBaseUrl(method) + method;
        let options = {};
        options['headers'] = this.createHeaders(moduleAPI, operation, true);
        search.set(this.serviceConstants.email, this._authService.getSavedEmail());
        options['params'] = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: search.getMap()
        });
        let formData: HttpParams = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: form_data
        });

        // Section for handling the caching
        let cacheKey: string = '';
        if (this.isRequestToBeChached(method, moduleAPI, operation)) {
            cacheKey = this.createCacheKey(method, moduleAPI, operation, search, form_data);
            // Check If Already Cached
            if (this.cache.isCached(cacheKey)) {
                let response: Emitter = new Emitter();
                try {
                    this.cache.readItem(cacheKey, response);
                    return response.result;
                } catch (excp) {
                    this.logger.warn(excp);
                }
            }
        }

        // Section For Handling The Grid Request For Exporting
        if (search.get(this.serviceConstants.GridMode) || form_data.hasOwnProperty(this.serviceConstants.GridMode) || search.get(this.serviceConstants.GridHandle) || form_data.hasOwnProperty(this.serviceConstants.GridHandle)) {
            this.cacheGridRequest(method, moduleAPI, operation, search, form_data);
        }

        document.querySelector('icabs-app .ajax-overlay')['style'].display = 'block';
        this.isAjaxRunning = true;
        return this.http.post(url, formData.toString(), options).pipe(map((res: any): Observable<any> => {
            let response: any = this.extractData(res);
            // Cache if cache key has value
            try {
                if (cacheKey) {
                    this.cache.setItem(cacheKey, response);
                }
            } catch (excp) {
                this.logger.warn(excp);
            }
            return response;
        }), catchError((error): Observable<any> => {
            return this.handleError(error);
        }));
    }

    makePostJsonRequest(method: string, moduleAPI: string,
        operation: string, search: QueryParams, form_data: Object, contentType?: string): Observable<any> {
        //User Signed In Check
        //this.checkSignIn();
        this.header = new Headers();
        //this.header.append(this.serviceConstants.Authorization, 'Bearer ' + this._authService.getAccessToken());
        this.header.append(this.serviceConstants.Module, moduleAPI);
        this.header.append(this.serviceConstants.Operation, operation);
        this.header.append(this.serviceConstants.ContentType, contentType ? contentType : 'application/json');
        this.header.append('client_id', environment['MULE_CLIENT_ID'] ? environment['MULE_CLIENT_ID'] : '');
        this.header.append('client_secret', environment['MULE_CLIENT_SECRET'] ? environment['MULE_CLIENT_SECRET'] : '');
        search.set(this.serviceConstants.email, this._authService.getSavedEmail());
        let url: string = this.baseURL + method;

        if (this.gcpService.isGcpEndpoint(method, moduleAPI, operation)) {
            url = this.gcpService.getGcpBaseUrl(method) + method;
            this.header.append(this.serviceConstants.Authorization, 'Bearer ' + this._authService.getToken());
        } else {
            this.header.append(this.serviceConstants.Authorization, this._authService.getToken());
        }

        let options = {};
        options['headers'] = this.header;
        options['search'] = search;

        if (environment['BYPASS_MULE']) {
            return this.http.post(url, form_data, options).pipe(map((res: any): Observable<any> => {
                return this.extractData(res);
            }), catchError((error): Observable<any> => {
                return this.handleError(error);
            }));
        } else {
            document.querySelector('icabs-app .ajax-overlay')['style'].display = 'block';
            this.isAjaxRunning = true;
            return this.http.post(url, form_data, options).pipe(map((res: any): Observable<any> => {
                return this.extractData(res);
            }), catchError((error): Observable<any> => {
                return this.handleError(error);
            }));
        }
    }

    public lookUpRequest(search: QueryParams, form_data: Object): Observable<any> {
        let options = {};
        let url: string = this.gcpService.getGcpBaseUrl(environment['LOOK_UP']) + environment['LOOK_UP'];
        let token: string = this._authService.getToken();

        if (!token) {
            return Observable.of('');
        }

        options['headers'] = this.createHeaders();
        options['params'] = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: search.getMap()
        });

        document.querySelector('icabs-app .ajax-overlay')['style'].display = 'block';
        this.isAjaxRunning = true;
        return this.http.post(url, form_data, options).pipe(map((res: any): Observable<any> => {
            return this.extractData(res);
        }), catchError((error): Observable<any> => {
            return this.handleError(error);
        }));

    }

    public lookUpPromise(search: QueryParams, form_data: Object): Promise<any> {
        let options = {};
        let url: string = this.gcpService.getGcpBaseUrl(environment['LOOK_UP']) + environment['LOOK_UP'];

        options['headers'] = this.createHeaders();
        options['params'] = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: search.getMap()
        });

        document.querySelector('icabs-app .ajax-overlay')['style'].display = 'block';
        this.isAjaxRunning = true;
        return this.http.post(url, form_data, options)
            .toPromise()
            .then((res: any): Observable<any> => {
                return this.extractData(res);
            })
            .catch((error): Observable<any> => {
                let obj: any = this.handleError(error);
                return obj.error;
            });

    }

    /**
     * Will only be called while Country is changed from Contract/Account
     */
    public sysCharRequest(search: QueryParams): Observable<any> {
        let syscharsList: any = search.get(this.serviceConstants.SystemCharNumber);
        let syscharArray: Array<any> = typeof syscharsList === 'object' ? syscharsList : syscharsList.toString().split(',');
        let jsonObj: any = this.sessionStorageService.retrieve(CBBConstants.c_s_URL_PARAM_SYSCHAR);
        let businessCode = search.get(this.serviceConstants.BusinessCode);
        let responseObj: any = {};
        responseObj['statusCode'] = 200;
        responseObj['status'] = 'success';
        responseObj['info'] = '';
        let records: any = [];
        /**
         * This case will be active while country is changed from Contract/Account search
         */
        if (!jsonObj) {
            return this.getSysCharsFromDB(search);
        }
        for (let i = 0; i < syscharArray.length; i++) {
            let syscharNumber = jsonObj[businessCode][syscharArray[i]];
            if (syscharNumber) {
                records.push({
                    SystemCharNumber: syscharNumber['SystemCharNumber'] ? syscharNumber['SystemCharNumber'] : null,
                    Required: syscharNumber['SystemCharRequired'] ? syscharNumber['SystemCharRequired'] : false,
                    Logical: syscharNumber['SystemCharLogical'] ? syscharNumber['SystemCharLogical'] : false,
                    Integer: syscharNumber['SystemCharInteger'] ? syscharNumber['SystemCharInteger'] : 0,
                    Text: syscharNumber['SystemCharText'] ? syscharNumber['SystemCharText'] : '',
                    Value: syscharNumber['SystemCharValue'] ? syscharNumber['SystemCharValue'] : 0
                });
            } else {
                records.push({
                    SystemCharNumber: null,
                    Required: false
                });
            }
        }
        responseObj['records'] = records;
        return Observable.of(responseObj);

    }

    public getSysCharsFromDB(search: QueryParams): Observable<any> {
        let url: string = this.gcpService.getGcpBaseUrl(environment.SYS_CHAR) + environment.SYS_CHAR;
        let options: any = {};
        options['headers'] = this.createHeaders();
        search.set(this.serviceConstants.email, this._authService.getSavedEmail());
        search.set(this.serviceConstants.Action, '0');
        options['params'] = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: search.getMap()
        });

        return this.http.get(url, options)
            .map((res): Observable<any> => {
                return this.extractData(res);
            })
            .catch((error): Observable<any> => {
                return this.handleError(error);
            });
    }

    public postData(search: QueryParams, customUrl: string, form_data: any, operation?: string): Observable<any> {
        let options: any = {};
        search.set(this.serviceConstants.email, this._authService.getSavedEmail());
        options.headers = this.createHeaders('', operation, true);
        options.params = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: search.getMap()
        });

        const formData: HttpParams = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: form_data
        });

        return this.http.post(customUrl, formData.toString(), options).pipe(map((res: any): Observable<any> => {
            return this.extractData(res);
        }), catchError((error): Observable<any> => {
            return this.handleError(error);
        }));
    }

    public riGetErrorMessage(errorCode: number, countryCode: string, businessCode: string): Observable<any> {
        let queryLookUp: QueryParams = new QueryParams();
        let userCode = this._authService.getSavedUserCode();
        let gLanguageCode = this._ls.retrieve('languagecode') || '';
        let errorMsg = '';
        let data: any = [];
        let dataEvent;

        if (gLanguageCode !== '') {  // UI-22586 optimization :- if langugecode is available from localstorage then skip lookup call
            dataEvent = this.loadErrorCodeLookup(errorCode, queryLookUp, gLanguageCode, userCode, countryCode, businessCode);
            this.dataEventArray.push(dataEvent);
            return dataEvent.asObservable();

        } else {
            dataEvent = this.loadErrorCodeLookup3(errorCode, queryLookUp, gLanguageCode, userCode, countryCode, businessCode);
            this.dataEventArray.push(dataEvent);
            return dataEvent.asObservable();
        }
    }

    private loadErrorCodeLookup(errorCode: number, queryLookUp: QueryParams, gLanguageCode: string, userCode: string, countryCode: string, businessCode: string): Observable<any> {
        let errorData = [{
            'table': 'ErrorMessageLanguage',
            'query': { 'LanguageCode': gLanguageCode, 'ErrorMessageCode': errorCode.toString() },
            'fields': ['ErrorMessageCode', 'ErrorMessageDisplayDescription']
        }];
        let dataEvent;
        let errorMsg: string;
        queryLookUp.set(this.serviceConstants.Action, '0');
        queryLookUp.set(this.serviceConstants.BusinessCode, businessCode);
        queryLookUp.set(this.serviceConstants.CountryCode, countryCode);
        queryLookUp.set(this.serviceConstants.MaxResults, '100');

        this.lookUpRequest(queryLookUp, errorData).subscribe((f) => {
            if (f.status === GlobalConstant.Configuration.Failure) {
                dataEvent.next(ErrorConstant.Message.ErrorMessageNotFound);
                dataEvent.complete();
            } else {
                if (!f.errorMessage) {
                    if (f['results'] && f['results'].length > 0) {
                        errorMsg = f['results'][0][0].ErrorMessageDisplayDescription;
                        dataEvent.next(errorMsg);
                        dataEvent.complete();
                    } else {
                        let errorData2 = [{
                            'table': 'ErrorMessage',
                            'query': { 'ErrorMessageCode': errorCode.toString() },
                            'fields': ['ErrorMessageCode', 'ErrorMessageDescription']
                        }];
                        this.lookUpRequest(queryLookUp, errorData2).subscribe((k) => {
                            if (k.status === GlobalConstant.Configuration.Failure) {
                                dataEvent.next(ErrorConstant.Message.ErrorMessageNotFound);
                            } else {
                                if (!k.errorMessage) {
                                    if (k['results'] && k['results'].length > 0) {
                                        errorMsg = k['results'][0][0].ErrorMessageDescription;
                                    } else {
                                        errorMsg = ErrorConstant.Message.ErrorMessageNotFound;
                                    }
                                    dataEvent.next(errorMsg);
                                    dataEvent.complete();
                                }
                            }
                        },
                            (error) => {
                                dataEvent.next(ErrorConstant.Message.ErrorMessageNotFound);
                                dataEvent.complete();
                            });
                    }
                }
            }
        },
            (error) => {
                dataEvent.next(ErrorConstant.Message.ErrorMessageNotFound);
                dataEvent.complete();
            });

        dataEvent = new Subject<any>();
        // this.dataEventArray.push(dataEvent);
        // return dataEvent.asObservable();
        return dataEvent;
    }

    private loadErrorCodeLookup3(errorCode: number, queryLookUp: QueryParams, gLanguageCode: string, userCode: any, countryCode: string, businessCode: string): Observable<any> {
        let data = [{
            'table': 'UserInformation',
            'query': { 'UserCode': userCode.UserCode },
            'fields': ['LanguageCode']
        }];
        let dataEvent;
        let errorMsg = '';

        queryLookUp.set(this.serviceConstants.Action, '0');
        queryLookUp.set(this.serviceConstants.BusinessCode, businessCode);
        queryLookUp.set(this.serviceConstants.CountryCode, countryCode);
        queryLookUp.set(this.serviceConstants.MaxResults, '100');

        this.lookUpRequest(queryLookUp, data).flatMap((e) => {
            if (e.status === GlobalConstant.Configuration.Failure) {
                dataEvent.next(ErrorConstant.Message.ErrorMessageNotFound);
                dataEvent.complete();

            } else {

                if (!(e.errorMessage || e.fullError)) {
                    let found = false;
                    if (e['results'] && e['results'].length > 0) {
                        for (let i = 0; i < e['results'][0].length; i++) {
                            if (e['results'][0][i].LanguageCode !== '') {
                                gLanguageCode = e['results'][0][i].LanguageCode;
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            gLanguageCode = GlobalConstant.Configuration.ENG;
                        }

                        let errorData = [{
                            'table': 'ErrorMessageLanguage',
                            'query': { 'LanguageCode': gLanguageCode, 'ErrorMessageCode': errorCode.toString() },
                            'fields': ['ErrorMessageCode', 'ErrorMessageDisplayDescription']
                        }];

                        return this.lookUpRequest(queryLookUp, errorData);

                    } else {
                        dataEvent.next(ErrorConstant.Message.ErrorMessageNotFound);
                        dataEvent.complete();
                    }

                } else {
                    dataEvent.next(ErrorConstant.Message.ErrorMessageNotFound);
                    dataEvent.complete();
                }
            }

        }).flatMap(() => {
            let errorData2 = [{
                'table': 'ErrorMessage',
                'query': { 'ErrorMessageCode': errorCode.toString() },
                'fields': ['ErrorMessageCode', 'ErrorMessageDescription']
            }];
            return this.lookUpRequest(queryLookUp, errorData2);

        }).subscribe((k) => {
            if (k.status === GlobalConstant.Configuration.Failure) {
                dataEvent.next(ErrorConstant.Message.ErrorMessageNotFound);
            } else {
                if (!k.errorMessage) {
                    if (k['results'] && k['results'].length > 0) {
                        errorMsg = k['results'][0][0].ErrorMessageDescription;
                    } else {
                        errorMsg = ErrorConstant.Message.ErrorMessageNotFound;
                    }
                    dataEvent.next(errorMsg);
                    dataEvent.complete();
                }
            }
        },
            (error) => {
                dataEvent.next(ErrorConstant.Message.ErrorMessageNotFound);
                dataEvent.complete();
            });

        dataEvent = new Subject<any>();
        return dataEvent;
    }

    public removeSubjectSubscription(): void {
        let dataEventArrayLength = this.dataEventArray.length;
        for (let i = 0; i < dataEventArrayLength; i++) {
            if (this.dataEventArray[i]) {
                this.dataEventArray[i].unsubscribe();
            }
        }
    }

    private convertJSONtoFormData(data: Object): string {
        let str: Array<string> = [];
        for (let key in data) {
            if (data.hasOwnProperty(key)) {
                str.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
            }
        }
        return str.join('&');
    }

    private detectAjaxAndPreventEvent(): void {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            if (!this.isAjaxRunning) {
                document.querySelector('icabs-app .ajax-overlay')['style'].display = 'none';
            } else {
                clearTimeout(this.timeout);
                this.detectAjaxAndPreventEvent();
            }
        }, 400);
    }

    private extractData(res: any): Observable<any> {
        this.isAjaxRunning = false;
        this.detectAjaxAndPreventEvent();
        let body = res;
        if (typeof body['error_description'] !== 'undefined' && body['error_description'].toString().indexOf(ErrorConstant.Message.Invalid) !== -1) {
            return Observable.throw(res);
        }
        //Check for business error
        if (body && body.hasOwnProperty('oResponse')) {
            if (body.info && body.info.error) {
                body.oResponse = {};
                body.oResponse.errorMessage = body.info.error;
                body.oResponse.hasError = true;
                //this.setErrorMessage(body.oResponse.errorMessage, body.oResponse);
            } else if (body.oResponse && body.oResponse.errorMessage) {
                body.oResponse.hasError = true;
                //this.setErrorMessage(body.oResponse.errorMessage, body.oResponse);
            } else if (body.oResponse && body.oResponse.ErrorMessageDesc) {
                body.oResponse.hasError = true;
                body.oResponse.errorMessage = body.oResponse.ErrorMessageDesc;
                //this.setErrorMessage(body.oResponse.errorMessage, body.oResponse);
            } else if (body.oResponse && body.oResponse.fullError) {
                body.oResponse.hasError = true;
                //body.oResponse.errorMessage = body.oResponse.fullError;
                //this.setErrorMessage(body.oResponse.errorMessage, body.oResponse);
            }
            return body.oResponse;
        }
        return body || {};
    }

    private handleError(error: Response | any): Observable<any> {
        this.isAjaxRunning = false;
        if (error.name === 'HttpErrorResponse') {

            if (this.isNewWindow) {
                if (!this.tokenErrorWindowTimeout) {
                    this.tokenErrorWindowCounter = this.c_n_WINDOW_CLOSE_TIME;
                    this.tokenErrorWindowTimeout = setInterval(() => {
                        this.modalAdvService.emitError(new ICabsModalVO('Due to a long period of inactivity, please close and reopen this window.\n\n\n This window will automatically close in ' + this.tokenErrorWindowCounter + ' seconds'));
                        this.tokenErrorWindowCounter -= 1;
                        if (!this.tokenErrorWindowCounter) {
                            clearInterval(this.tokenErrorWindowTimeout);
                            this.tokenErrorWindowTimeout = '';
                            window.close();
                        }
                    }, 1000);
                }
            }
        }
        this.detectAjaxAndPreventEvent();
        return Observable.throw(error);
    }

    private convertNullOrUndefinedToEmpty(value: string): string {
        return (value = !(value === null || value === undefined) ? value : '');
    }

    private handleExportingMessage(error: Response | any): any {
        const errorMessage: string = error.error.oResponse.errorMessage || MessageConstant.Message.GeneralError;
        const fullError: string = error.error.oResponse.fullError || errorMessage;

        this.notification.displayMessage(errorMessage);
        this.logger.error(errorMessage + ' - ' + fullError);

        return error.error.oResponse;
    }

    // Methods Added For Caching
    private isRequestToBeChached(method: string, moduleAPI: string, operation: string): boolean {
        return method.indexOf('/search') >= 0 && SearchRequestCacheWhiteList.c_s_LIST[method + '-' + moduleAPI + '-' + operation];
    }

    private createCacheKey(method: string, moduleAPI: string, operation: string, search: QueryParams, form_data?: any): string {
        let cacheKey: string = '';
        search.keys().forEach((key) => {
            if (key !== 'email') {
                cacheKey += search.get(key);
            }
        });
        if (form_data) {
            Object.keys(form_data).forEach((key) => {
                cacheKey += form_data[key];
            });
        }
        cacheKey += method + moduleAPI + operation;

        return cacheKey;
    }

    private createHeaders(moduleAPI?: string, operation?: string, isPost?: boolean, method?: string, options?: Record<string, string>): HttpHeaders {
        let header = {};
        let token: string = this._authService.getToken();
        if (!token) {
            return;
        }
        if (moduleAPI) {
            header[this.serviceConstants.Module] = moduleAPI;
        }
        if (operation) {
            header[this.serviceConstants.Operation] = operation;
        }
        header['client_id'] = environment['MULE_CLIENT_ID'] ? environment['MULE_CLIENT_ID'] : '';
        header['client_secret'] = environment['MULE_CLIENT_SECRET'] ? environment['MULE_CLIENT_SECRET'] : '';
        header[this.serviceConstants.Authorization] = 'Bearer ' + this._authService.getToken();
        if (isPost) {
            header[this.serviceConstants.ContentType] = 'application/x-www-form-urlencoded';
        }
        if (method) {
            header['url'] = this.gcpService.getGcpBaseUrl() + method;
        }
        if (options) {
            for (let key in options) {
                if (options.hasOwnProperty(key) && options[key]) {
                    header[key] = options[key];
                }
            }
        }

        return new HttpHeaders(header);
    }

    //#region Grid Exporting
    private cacheGridRequest(method: string, module: string, operation: string, search: QueryParams, formData?: any): void {
        let gridCache: any = {};
        let lastGridRequest: Record<string, any> = this.sessionStorageService.retrieve(ExportConfig.c_s_STORAGE_KEY);

        gridCache[HttpConstants.c_s_API_METHOD] = method;
        gridCache[HttpConstants.c_s_API_MODULE] = module;
        gridCache[HttpConstants.c_s_API_OPERATION] = operation;
        gridCache[HttpConstants.c_s_API_SEARCH] = search;
        gridCache[HttpConstants.c_s_HTTP_METHOD] = HttpConstants.c_s_HTTP_GET;

        if (formData) {
            gridCache[HttpConstants.c_s_API_FORMDATA] = formData;
            gridCache[HttpConstants.c_s_HTTP_METHOD] = HttpConstants.c_s_HTTP_POST;
        }

        this.sessionStorageService.store(ExportConfig.c_s_STORAGE_KEY, gridCache);
    }

    public makeExportRequest(request?: Record<string, any>, title?: string): Promise<any> {
        let lastGridRequest: Record<string, any> = request || this.sessionStorageService.retrieve(ExportConfig.c_s_STORAGE_KEY);
        let url: string = this.gcpService.getExportAPIUrl();
        let options = {};

        options['headers'] = this.createHeaders(
            lastGridRequest[HttpConstants.c_s_API_MODULE],
            lastGridRequest[HttpConstants.c_s_API_OPERATION],
            lastGridRequest[HttpConstants.c_s_HTTP_METHOD] === HttpConstants.c_s_HTTP_POST,
            lastGridRequest[HttpConstants.c_s_API_METHOD],
            lastGridRequest[ExportConfig.c_s_OPTIONS_KEY]);
        // Update Action Before Sending The Request For Export
        if (!request) {
            lastGridRequest[HttpConstants.c_s_API_SEARCH].set(this.serviceConstants.Action, '5');
        } else {
            lastGridRequest[HttpConstants.c_s_API_SEARCH].set(this.serviceConstants.email, this._authService.getSavedEmail());
        }
        options['params'] = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: lastGridRequest[HttpConstants.c_s_API_SEARCH].getMap()
        });

        if (lastGridRequest[HttpConstants.c_s_HTTP_METHOD] === HttpConstants.c_s_HTTP_GET) {
            return this.http.get(url, options).toPromise()
                .then((res: any): Observable<any> => {
                    let response: any = this.extractData(res);
                    if (response && response.hasOwnProperty('SpreadsheetURL')) {
                        this.pushNotification.push = {
                            message: 'Successfully Exported - ' + title || '',
                            data: response['SpreadsheetURL'],
                            type: ENotificationType.export
                        };
                    }
                    return response;
                })
                .catch((error) => {
                    return Promise.reject(this.handleExportingMessage(error));
                });
        } else {
            let payload: HttpParams = new HttpParams({
                encoder: new PayloadEncoder(),
                fromObject: lastGridRequest.formData
            });
            return this.http.post(url, payload.toString(), options).toPromise()
                .then((res: any): Observable<any> => {
                    let response: any = this.extractData(res);
                    if (response && response.hasOwnProperty('SpreadsheetURL')) {
                        this.pushNotification.push = {
                            message: 'Successfully Exported - ' + title || '',
                            data: response['SpreadsheetURL'],
                            type: ENotificationType.export
                        };
                    }
                    return response;
                })
                .catch((error): Promise<any> => {
                    return Promise.reject(this.handleExportingMessage(error));
                });
        }
    }
    //#endregion

    //#region Document Report API
    public fetchReportURLGET(config: IFetchReportURL): Promise<any> {
        let url: string = this.gcpService.getGcpBaseUrl('') + EDocumentReportConfig.c_s_DOCUMENT_REPORT_METHOD;
        let options = {};

        options['headers'] = this.createHeaders(EDocumentReportConfig.c_s_DOCUMENT_REPORT_MODULE,
            config[EDocumentReportConfig.c_s_CONFIG_KEY_OPERATION],
            false);
        config[EDocumentReportConfig.c_s_CONFIG_KEY_SEARCH].set(this.serviceConstants.email, this._authService.getSavedEmail());
        options['params'] = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: config[EDocumentReportConfig.c_s_CONFIG_KEY_SEARCH].getMap()
        });

        this.isAjaxRunning = true;
        return this.http.get(url, options)
            .toPromise()
            .then((res: any): Promise<any> => {
                this.isAjaxRunning = false;
                let response: any = this.extractData(res);
                let key: string = config[EDocumentReportConfig.c_s_CONFIG_KEY_URL_KEY]
                    || EDocumentReportConfig.c_s_DOCUMENT_REPORT_URL_KEY_DEFAULT;
                if (response[key] && response[key].length) {
                    window.open(response[key], '_blank');
                } else {
                    this.logger.error('Key "' + key + '" Is Not Available');
                    this.notification.message = {
                        msg: MessageConstant.Message.GeneralError,
                        timestamp: (new Date()).getMilliseconds()
                    };
                }
                return response;
            })
            .catch((error): Promise<any> => {
                return Promise.reject(this.handleError(error));
            });
    }
    //#endregion
}
