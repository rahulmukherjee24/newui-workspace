import { QueryParams } from './../services/http-params-wrapper';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from './http-service';
import { ServiceConstants } from './../constants/service.constants';

@Injectable()
export class SpeedScript {
    constructor(private serviceConstants: ServiceConstants, private xhr: HttpService) { }

    /**
     * Method to get System Characters
     * @params: params: Object {module, operation, action, businessCode, countryCode, SysCharList}
     * @return: Observable
     */
    public sysChar(params: any): Observable<any> {
        let search = new QueryParams();
        search.set(this.serviceConstants.Action, params.action); //TODO - Can this be hardcoded to 0
        search.set(this.serviceConstants.BusinessCode, params.businessCode);
        search.set(this.serviceConstants.CountryCode, params.countryCode);
        search.set('systemCharNumber', params.SysCharList);
        return this.xhr.sysCharRequest(search);
    }


    /**
     * Method to get System Characters
     * @params: params: Object {module, operation, action, businessCode, countryCode, SysCharList}
     * @return: Promise
     */
    public sysCharPromise(params: any): Promise<any> {
        let search = new QueryParams();
        search.set(this.serviceConstants.BusinessCode, params.businessCode);
        search.set(this.serviceConstants.CountryCode, params.countryCode);
        search.set(this.serviceConstants.SystemCharNumber, params.SysCharList);
        return this.xhr.sysCharRequest(search).toPromise();
    }

}
