import { Injectable } from '@angular/core';

import { CustomAlertConstants } from './../components/alert/customalert.constants';

@Injectable({
    providedIn: 'root'
})
export class GlobalNotificationsService {
    public message: any;
    public type: string = '';

    public displayMessage(msg: string | any, type?: string): void {
        let text: string = typeof msg === 'string' ? msg : '';
        type = type || CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR;
        if (!text) {
            if (msg.hasOwnProperty('errorMessage')) {
                text += msg.errorMessage;
            } else if (msg.hasOwnProperty('ErrorMessage')) {
                text += msg.ErrorMessage;
            } else if (msg.hasOwnProperty('errormessage')) {
                text += msg.errormessage;
            } else if (msg.hasOwnProperty('error')) {
                text += msg.error.info;
            }
            if (text && msg.fullError) {
                text += ' - ';
            }
            text += msg.fullError || '';
        }
        this.type = type || '';
        this.message = {
            msg: (type === 'error' ? type.toUpperCase() + ' : ' : '') + text,
            timestamp: (new Date()).getMilliseconds()
        };
    }
}
