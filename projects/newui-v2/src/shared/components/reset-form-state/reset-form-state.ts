import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { MessageConstant } from '@shared/constants/message.constant';


@Component({
    selector: 'icabs-reset-form-state',
    template: `
    <button aria-label="Reset" class="btn btn-additional reset-form" title="{{'Reset Form' | translate }}" (click)="resetForm()" type="button">
        <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>
    </button>
    <icabs-prompt-modal #promptModal [(showHeader)]="showMessageHeader" [title]="promptTitle" [content]="promptContent"
    (saveEmit)="formReset()"></icabs-prompt-modal>
    `
})
export class ResetFormStateComponent {
    @Input() excludeElementList: Array<string>; //mandatory, must be array of string
    @Input() formList: Array<FormGroup>;  //mandatory, must be array of FormGroup
    @Output() success = new EventEmitter();
    @ViewChild('promptModal') public promptModal;

    public formData: FormGroup;
    public promptContent: string;
    public promptTitle: string;
    public showMessageHeader: boolean = true;

    constructor(private logger: NGXLogger) {
    }

    private validateInput(input: string[] | FormGroup[], type?: string): boolean {
        let isValidInput: boolean = true;
        if (!input || !input.length) { return false; }

        for (let i = 0; i < input.length; i++) {
            if (type === 'string') {
                if (!input[i]) { isValidInput = false; }
            } else {
                if (!input[i] || !(input[i] instanceof FormGroup)) { isValidInput = false; }
            }
        }
        return isValidInput;
    }

    public resetForm(): void {
        const isValidExcludeElementList = this.validateInput(this.excludeElementList, 'string');
        const isValidExcludeformList = this.validateInput(this.formList, 'formgroup');
        if (!(isValidExcludeElementList && isValidExcludeformList)) {
            this.logger.error(' RESET-FORM-STATE COMPONENT:- One or more Element(s) are INVALID or MISSING, Please check inputs.....');
            return;
        }

        let isFormDirty: boolean = false;
        //on All valid inputs
        this.formList.map((form: FormGroup) => {
            this.formData = form;
            if (form && form.pristine) {
                form.reset();
                form.disable();
            } else {
                this.promptTitle = MessageConstant.Message.WarningTitle;
                this.promptContent = MessageConstant.Message.RouteAway;
                this.promptModal.show();
                isFormDirty = true;
            }
        });
        if (!isFormDirty) {
            this.excludeElementList.map((item: string) => {  //enable element(s) of first form [parent]
                if (this.formList[0].controls.hasOwnProperty(item)) {
                    this.formList[0].controls[item].enable();
                }
            });
            this.success.emit('true');
        }

    }

    public formReset(): void {
        this.formData.reset();
        this.formData.disable();
        this.success.emit('true');
    }
}
