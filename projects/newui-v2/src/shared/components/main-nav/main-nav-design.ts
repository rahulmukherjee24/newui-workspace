export const MAIN_NAV_DESIGN = `{
  "menu": [{
    "domainname": "Management",
    "id": "Management",
    "alwaysdisplay": false,
    "visibility": false,
    "feature": [{
      "featurename": "Customer Contact",
      "visibility": false,
      "module": [{
        "modulename": "General Search",
        "programURL": "/ContactManagement/iCABSCMGeneralSearchGrid.htm",
        "routeURL": "/contractmanagement/generalsearchgrid",
        "visibility": false
      }, {
        "modulename": "Contact Centre Search",
        "programURL": "/ContactManagement/iCABSCMCallCentreGrid.htm",
        "routeURL": "/ccm/callcentersearch/",
        "visibility": false,
        "quickLinks": true,
        "icon": "glyphicon-phone-alt"

      }, {
        "modulename": "Contact Centre Review",
        "programURL": "/ContactManagement/iCABSCMCallCentreReviewGrid.htm",
        "routeURL": "/ccm/centreReview",
        "visibility": false,
        "quickLinks": true,
        "icon": "glyphicon-dashboard"
      }, {
        "modulename": "Contact Centre Assign",
        "programURL": "/ContactManagement/iCABSCMCallCentreAssignGrid.htm",
        "routeURL": "/ccm/customerContact/callCentreAssignGrid",
        "visibility": false
      }, {
        "modulename": "Account Review",
        "programURL": "/ContactManagement/iCABSCMAccountReviewGrid.htm",
        "routeURL": "grid/application/nav/reportContacts/accountReviewGrid",
        "visibility": false
      }, {
        "modulename": "Call Analysis",
        "programURL": "/ContactManagement/iCABSCMCallAnalysisGrid.htm",
        "routeURL": "/ccm/contactmanagement/callAnalysisGrid",
        "visibility": false
      }, {
        "modulename": "Contact Medium",
        "programURL": "/ContactManagement/iCABSContactMediumGrid.htm",
        "routeURL": "/ccm/service/contactmedium",
        "visibility": false
      }]
    }, {
      "featurename": "Finance",
      "visibility": false,
      "module": [{
        "modulename": "Credit / Charge Read-Only",
        "programURL": "/Application/iCABSACreditApprovalGrid.htm<readonly>",
        "routeURL": "/grid/application/creditApprovalGrid",
        "queryParams": {"readonly": "true"},
        "visibility": false
      }, {
        "modulename": "Credit / Charge Approval",
        "programURL": "/Application/iCABSACreditApprovalGrid.htm",
        "routeURL": "/grid/application/creditApprovalGrid",
        "visibility": false
      }, {
        "modulename": "Price / Cost Variance",
        "programURL": "/ApplicationReport/iCABSARVarianceFromPriceCostGrid.htm",
        "routeURL": "/billtocash/variancefromprice",
        "visibility": false
      }, {
        "modulename": "Renewal / Period",
        "programURL": "/Application/iCABSAContractRenewalMaintenance.htm",
        "routeURL": "application/contract/renewal",
        "visibility": false
      }]
    }, {
      "featurename": "Imports & Notifications",
      "visibility": false,
      "module": [{
        "modulename": "Bulk SMS Messages: Business",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/ccm/sendbulksms/business",
        "visibility": false
      }, {
        "modulename": "Bulk SMS Messages: Branch",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/ccm/sendbulksms/branch",
        "visibility": false
      }, {
        "modulename": "Import Records",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/itfunctions/importing",
        "visibility": false
      }]
    }, {
      "featurename": "Tools",
      "visibility": false,
      "module": [{
        "modulename": "Batch Process Monitor",
        "programURL": "/Model/riMBatchProcessMonitorSearch.htm",
        "routeURL": "/itfunctions/batchprocess/monitor",
        "visibility": false
      }, {
        "modulename": "Report Process Viewer",
        "programURL": "/Model/riMReportViewerSearch.htm",
        "routeURL": "/itfunctions/riMReportViewerSearch",
        "visibility": false
      }]
    }]
  }, {
    "domainname": "Customer Service",
    "id": "Customer Service",
    "alwaysdisplay": false,
    "visibility": false,
    "feature": [{
      "featurename": "Customer Contact",
      "visibility": false,
      "module": [{
        "modulename": "General Search",
        "programURL": "/ContactManagement/iCABSCMGeneralSearchGrid.htm",
        "routeURL": "/contractmanagement/generalsearchgrid",
        "visibility": false
      }, {
        "modulename": "Contact Centre Search",
        "programURL": "/ContactManagement/iCABSCMCallCentreGrid.htm",
        "routeURL": "/ccm/callcentersearch/",
        "visibility": false
      }, {
        "modulename": "Contact Centre Review",
        "programURL": "/ContactManagement/iCABSCMCallCentreReviewGrid.htm",
        "routeURL": "/ccm/centreReview",
        "visibility": false
      }, {
        "modulename": "Contact Centre Assign",
        "programURL": "/ContactManagement/iCABSCMCallCentreAssignGrid.htm",
        "routeURL": "/ccm/customerContact/callCentreAssignGrid",
        "visibility": false
      }, {
        "modulename": "Work Order Review",
        "programURL": "/ContactManagement/iCABSCMWorkorderReviewGrid.htm",
        "routeURL": "/ccm/workOrderReviewGrid",
        "visibility": false
      }, {
        "modulename": "Contact Redirection",
        "programURL": "/ContactManagement/iCABSCMContactRedirection.htm",
        "routeURL": "ccm/contactmanagement/scmContactRedirection",
        "visibility": false
      }, {
        "modulename": "Callout Search",
        "programURL": "/ContactManagement/iCABSCMCustomerContactCalloutGrid.htm",
        "routeURL": "/ccm/customercontact/callout/grid",
        "visibility": false
      }, {
        "modulename": "Call Analysis",
        "programURL": "/ContactManagement/iCABSCMCallAnalysisGrid.htm",
        "routeURL": "/ccm/contactmanagement/callAnalysisGrid",
        "visibility": false
      }, {
        "modulename": "Contact Medium",
        "programURL": "/ContactManagement/iCABSContactMediumGrid.htm",
        "routeURL": "/ccm/service/contactmedium",
        "visibility": false
      }]
    }, {
      "featurename": "Telesales",
      "visibility": false,
      "module": [{
        "modulename": "Telesales Order Grid",
        "programURL": "/Application/iCABSATeleSalesOrderGrid.htm",
        "routeURL": "/ccm/customerContact/telesalesordergrid",
        "visibility": false
      }]
    }, {
      "featurename": "Imports & Notifications",
      "visibility": false,
      "module": [{
        "modulename": "Bulk SMS Messages: Business",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/ccm/sendbulksms/business",
        "visibility": false
      }, {
        "modulename": "Bulk SMS Messages: Branch",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/ccm/sendbulksms/branch",
        "visibility": false
      }, {
        "modulename": "Email Messages",
        "programURL": "/Business/iCABSBEmailGrid.htm",
        "routeURL": "/ccm/business/email",
        "visibility": false
      }, {
        "modulename": "SMS Messages",
        "programURL": "/ContactManagement/iCABSCMSMSMessagesGrid.htm",
        "routeURL": "/ccm/service/smsmessages",
        "visibility": false
      }, {
        "modulename": "Notification Templates",
        "programURL": "/System/iCABSSNotificationTemplateMaintenance.htm",
        "routeURL": "/ccm/system/maintenance/notificationtemplate",
        "visibility": false
      }, {
        "modulename": "Notification Groups",
        "programURL": "/System/iCABSSNotificationGroupMaintenance.htm",
        "routeURL": "/ccm/notification/group/maintenance",
        "visibility": false
      }, {
        "modulename": "Team Maintenance",
        "programURL": "/System/iCABSSTeamMaintenance.htm",
        "routeURL": "/service/teamMaintenance",
        "visibility": false
      }, {
        "modulename": "Ticket Type",
        "programURL": "/System/iCABSSContactTypeMaintenance.htm",
        "routeURL": "/ccm/system/contact/contacttype",
        "visibility": false
      }, {
        "modulename": "Ticket Type Detail",
        "programURL": "/System/iCABSSContactTypeDetailMaintenance.htm",
        "routeURL": "/application/maintenance/contacttype/detail",
        "visibility": false
      }, {
        "modulename": "Ticket Type Detail - Translation",
        "programURL": "/System/iCABSSContactTypeDetailLangMaintenance.htm",
        "routeURL": "sales/contact/type/detial/langmaintenance",
        "visibility": false
      }]
    }, {
      "featurename": "Tools",
      "visibility": false,
      "module": [{
        "modulename": "Batch Process Monitor",
        "programURL": "/Model/riMBatchProcessMonitorSearch.htm",
        "routeURL": "/itfunctions/batchprocess/monitor",
        "visibility": false
      }, {
        "modulename": "Report Process Viewer",
        "programURL": "/Model/riMReportViewerSearch.htm",
        "routeURL": "/itfunctions/riMReportViewerSearch",
        "visibility": false
      }]
    }]
  }, {
    "domainname": "Administration",
    "id": "Administration",
    "alwaysdisplay": false,
    "visibility": false,
    "feature": [{
      "featurename": "Customer Contact",
      "visibility": false,
      "module": [{
        "modulename": "General Search",
        "programURL": "/ContactManagement/iCABSCMGeneralSearchGrid.htm",
        "routeURL": "/contractmanagement/generalsearchgrid",
        "visibility": false
      }, {
        "modulename": "Contact Centre Search",
        "programURL": "/ContactManagement/iCABSCMCallCentreGrid.htm",
        "routeURL": "/ccm/callcentersearch/",
        "visibility": false
      }, {
        "modulename": "Contact Centre Review",
        "programURL": "/ContactManagement/iCABSCMCallCentreReviewGrid.htm",
        "routeURL": "/ccm/centreReview",
        "visibility": false
      }, {
        "modulename": "Contact Centre Assign",
        "programURL": "/ContactManagement/iCABSCMCallCentreAssignGrid.htm",
        "routeURL": "/ccm/customerContact/callCentreAssignGrid",
        "visibility": false
      }, {
        "modulename": "Work Order Review",
        "programURL": "/ContactManagement/iCABSCMWorkorderReviewGrid.htm",
        "routeURL": "/ccm/workOrderReviewGrid",
        "visibility": false
      }, {
        "modulename": "Contact Redirection",
        "programURL": "/ContactManagement/iCABSCMContactRedirection.htm",
        "routeURL": "/ccm/contactmanagement/scmContactRedirection",
        "visibility": false
      }, {
        "modulename": "Contact Medium",
        "programURL": "/ContactManagement/iCABSContactMediumGrid.htm",
        "routeURL": "/ccm/service/contactmedium",
        "visibility": false
      }]
    }, {
      "featurename": "Prospects",
      "visibility": false,
      "module": [{
        "modulename": "Prospect Maintenance",
        "programURL": "/ContactManagement/iCABSCMPipelineProspectMaintenance.htm<Prospect>",
        "routeURL": "/prospecttocontract/maintenance/prospect",
        "visibility": false,
        "quickLinks": "true",
        "icon": "glyphicon-pencil"
      }, {
        "modulename": "Prospect Grid",
        "programURL": "/ContactManagement/iCABSCMProspectGrid.htm",
        "routeURL": "/prospecttocontract/prospectgrid",
        "visibility": false
      }, {
        "modulename": "Pipeline Prospect Grid",
        "programURL": "/Sales/iCABSSPipelineGrid.htm",
        "routeURL": "/prospecttocontract/SalesOrderProcessing/PipelineGrid",
        "visibility": false
      }, {
        "modulename": "Sales Order Prospects",
        "programURL": "/Sales/iCABSSSOProspectGrid.htm",
        "routeURL": "/grid/service/drilldown/prospectgrid",
        "visibility": false
      },{
        "modulename": "Contract Approval By Business",
        "programURL": "/Sales/iCABSSApprovalBusiness.htm",
        "routeURL": "/grid/sales/approvalbusiness",
        "visibility": false
      },{
        "modulename": "Contract Approval By Branch",
        "programURL": "/Sales/iCABSSdlContractApprovalGrid.htm",
        "routeURL": "/grid/sales/contractapprovalgrid",
        "visibility": false
      }, {
        "modulename": "Diary",
        "programURL": "/ContactManagement/iCABSCMDiaryMaintenance.htm",
        "routeURL": "/prospecttocontract/maintenance/diary",
        "visibility": false
      }, {
        "modulename": "Diary Day",
        "programURL": "/ContactManagement/iCABSCMDiaryDayMaintenance.htm",
        "routeURL": "/prospecttocontract/maintenance/diarydaymaintaianance",
        "visibility": false
      }, {
        "modulename": "Key Account Job",
        "programURL": "/ContactManagement/iCABSCMProspectEntryMaintenance.htm<NatAxJob>",
        "routeURL": "/prospecttocontract/maintenance/prospectentry/nataxjob",
        "visibility": false
      }, {
        "modulename": "Confirm Key Account Job",
        "programURL": "/ContactManagement/iCABSCMProspectEntryGrid.htm<Confirm>",
        "routeURL": "/prospecttocontract/contactmanagement/prospectEntryGrid",
        "visibility": false
      }]
    }, {
      "featurename": "Account & Invoice Group",
      "visibility": false,
      "module": [{
        "modulename": "Account Maintenance",
        "programURL": "/Application/iCABSAAccountMaintenance.htm",
        "routeURL": "/contractmanagement/accountmaintenance/account/maintenance",
        "visibility": false
      }, {
        "modulename": "Merge Account",
        "programURL": "/Application/iCABSAAccountMerge.htm",
        "routeURL": "/contractmanagement/accountadmin/account/merge/search",
        "visibility": false
      }, {
        "modulename": "Move Account",
        "programURL": "/Application/iCABSAAccountMove.htm",
        "routeURL": "/contractmanagement/accountadmin/account/move/search",
        "visibility": false
      }, {
        "modulename": "Assign Account",
        "programURL": "/Application/iCABSAAccountAssign.htm",
        "routeURL": "/contractmanagement/accountadmin/account/assign/search",
        "visibility": false
      }, {
        "modulename": "Account Bank Details",
        "programURL": "/Application/iCABSAAccountBankDetailsMaintenance.htm",
        "routeURL": "application/AccountBankDetailsMaintenance",
        "visibility": false
      }, {
        "modulename": "Account Address Change History",
        "programURL": "/Application/iCABSAAccountAddressChangeHistoryGrid.htm",
        "routeURL": "/grid/service/contractmanagement/account/addressChangeHistory",
        "visibility": false
      }, {
        "modulename": "Tax Registration Maintenance",
        "programURL": "/Application/iCABSATaxRegistrationChange.htm",
        "routeURL": "/billtocash/application/taxregistrationchange",
        "visibility": false
      }, {
        "modulename": "Account Owning Branch Maintenance",
        "programURL": "/Application/iCABSAAccountOwnerMaintenance.htm",
        "routeURL": "",
        "visibility": false
      }, {
        "modulename": "Group Account Maintenance",
        "programURL": "/System/iCABSSGroupAccountMaintenance.htm",
        "routeURL": "/contractmanagement/groupaccount/account/groupaccountmaintenance",
        "visibility": false
      }, {
        "modulename": "Group Account Move",
        "programURL": "/System/iCABSSGroupAccountMove.htm",
        "routeURL": "/contractmanagement/groupaccount/account/groupaccountmove",
        "visibility": false
      }, {
        "modulename": "Invoice Group Maintenance",
        "programURL": "/Application/iCABSAInvoiceGroupMaintenance.htm",
        "routeURL": "/billtocash/maintenance/invoicegroup/search",
        "visibility": false
      }, {
        "modulename": "Invoice Group / Premises",
        "programURL": "/Application/iCABSAInvoiceGroupPremiseMaintenance.htm",
        "routeURL": "sales/invoicepremisegroup/search",
        "visibility": false
      }, {
        "modulename": "Invoice Group Payment",
        "programURL": "/Application/iCABSAInvoiceGroupPaymentMaintenance.htm",
        "routeURL": "/application/grouppaymentmaintenance",
        "visibility": false
      }]
    }, {
      "featurename": "Contract",
      "visibility": false,
      "module": [{
        "modulename": "Contract Maintenance",
        "programURL": "/Application/iCABSAContractMaintenance.htm",
        "routeURL": "/contractmanagement/maintenance/contract",
        "visibility": false,
        "quickLinks": "true",
        "icon": "glyphicon-inbox"
      }, {
        "modulename": "Cancel Contract",
        "programURL": "/Application/iCABSAInactiveContractInfoMaintenance.htm<cancel>",
        "routeURL": "/contractmanagement/retention/inactive/contractinfo/cancel",
        "queryParams": {"CurrentContractType": "C"},
        "visibility": false
      }, {
        "modulename": "Terminate Contract",
        "programURL": "/Application/iCABSAContractSelectMaintenance.htm<Pending>",
        "routeURL": "/contractmanagement/retention/maintenance/ContractSelectMaintenance",
        "visibility": false
      }, {
        "modulename": "Amend Contract Termination",
        "programURL": "/Application/iCABSAInactiveContractInfoMaintenance.htm",
        "routeURL": "/contractmanagement/retention/inactive/contractinfo",
        "queryParams": {"CurrentContractType": "C" },
        "visibility": false
      }, {
        "modulename": "Reinstate Contract",
        "programURL": "/Application/iCABSAInactiveContractInfoMaintenance.htm<reinstate>",
        "routeURL": "/contractmanagement/retention/inactive/contractinfo/reinstate",
        "queryParams": {"CurrentContractType": "C" },
        "visibility": false
      }, {
        "modulename": "Contract Approval By Business",
        "programURL": "/Sales/iCABSSApprovalBusiness.htm",
        "routeURL": "/grid/sales/approvalbusiness",
        "visibility": false
      },{
        "modulename": "Contract Approval By Branch",
        "programURL": "/Sales/iCABSSdlContractApprovalGrid.htm",
        "routeURL": "/grid/sales/contractapprovalgrid",
        "visibility": false
      }, {
        "modulename": "Suspend Contract Invoice",
        "programURL": "/Application/iCABSAContractSuspendMaintenance.htm",
        "routeURL": "sales/contract/suspend",
        "queryParams": {"currentContractType": "C"},
        "visibility": false
      }, {
        "modulename": "Mass Suspend Service",
        "programURL": "/Application/iCABSAPremiseServiceSuspendMaintenance.htm",
        "routeURL": "sales/contract/suspendbymass",
        "visibility": false
      }, {
        "modulename": "Renewal / Period",
        "programURL": "/Application/iCABSAContractRenewalMaintenance.htm",
        "routeURL": "application/contract/renewal",
        "visibility": false
      },{
        "modulename": "Renegotiations",
        "programURL": "/Application/iCABSARenegGrid.htm",
        "routeURL": "renegotiations/grid",
        "visibility": false
      },
      {
        "modulename": "Invoice Changes",
        "programURL": "/Application/iCABSAInvoiceDetailsMaintenance.htm",
        "routeURL": "/billtocash/maintenance/invoiceDetailsMaintainance",
        "visibility": false
      }, {
        "modulename": "Negotiating Branch",
        "programURL": "/Application/iCABSANegBranchMaintenance.htm",
        "routeURL": "/contractmanagement/contractadmin/negbranchmaintenance",
        "visibility": false
      }, {
        "modulename": "Customer Information",
        "programURL": "/Application/iCABSACustomerInfoMaintenance.htm",
        "routeURL": "sales/customerInfoMaintenance",
        "visibility": false
      }, {
        "modulename": "Mass Price Change",
        "programURL": "/Application/iCABSAMassPriceChangeGrid.htm",
        "routeURL": "/contractmanagement/servicecoveradmin/application/massPriceChangeGrid",
        "visibility": false
      }, {
        "modulename": "Percentage Price Change",
        "programURL": "/Application/iCABSAPercentagePriceChange.htm<Contract>",
        "routeURL": "/contractmanagement/servicecoveradmin/percentagepricechange/perchanges/Contract",
        "visibility": false
      }, {
        "modulename": "Multi-Premises Special Instructions Change",
        "programURL": "/Application/iCABSAMultiPremiseSpecial.htm",
        "routeURL": "/contractmanagement/premisesadmin/application/multiPremisesSpecial",
        "visibility": false
      }, {
        "modulename": "Multi-Premises Purchase Order Number Change",
        "programURL": "/Application/iCABSAMultiPremisePurchaseOrderAmend.htm",
        "routeURL": "/contractmanagement/premisesadmin/contract/multipremise/purchaseorderamend",
        "visibility": false
      }, {
        "modulename": "Contract Details Report",
        "programURL": "/ApplicationReport/iCABSARBranchContractReport.htm",
        "routeURL": "/contractmanagement/reports/contractForBranchReport",
        "visibility": false
      }, {
        "modulename": "Price / Cost Variance",
        "programURL": "/ApplicationReport/iCABSARVarianceFromPriceCostGrid.htm",
        "routeURL": "/billtocash/variancefromprice",
        "visibility": false
      }]
    }, {
      "featurename": "Contract - Premises",
      "visibility": false,
      "module": [{
        "modulename": "Premises Maintenance",
        "programURL": "/Application/iCABSAPremiseMaintenance.htm",
        "routeURL": "/contractmanagement/premisesmaintenance/maintenance/premise",
        "queryParams": {"contractTypeCode": "C"},
        "visibility": false
      }, {
        "modulename": "Cancel Premises",
        "programURL": "/Application/iCABSAInactivePremiseInfoMaintenance.htm<cancel>",
        "routeURL": "/contractmanagement/retention/inactive/premiseinfo/contract/cancel",
        "visibility": false
      }, {
        "modulename": "Delete Premises",
        "programURL": "/Application/iCABSAPremiseSelectMaintenance.htm<Pending>",
        "routeURL": "/contractmanagement/retention/deletePremise/premiseSelectMaintenance",
        "visibility": false
      }, {
        "modulename": "Amend Premises Deletion",
        "programURL": "/Application/iCABSAInactivePremiseInfoMaintenance.htm",
        "routeURL": "/contractmanagement/retention/inactive/premiseinfo",
        "visibility": false
      }, {
        "modulename": "Reinstate Premises",
        "programURL": "/Application/iCABSAInactivePremiseInfoMaintenance.htm<reinstate>",
        "routeURL": "/contractmanagement/retention/inactive/premiseinfo/reinstate",
        "visibility": false
      }, {
        "modulename": "Suspend Premises Service",
        "programURL": "/Application/iCABSAPremiseServiceSuspendMaintenance.htm",
        "routeURL": "/contractmanagement/premisesmaintenance/servicesuspendmaintenance",
        "visibility": false
      }, {
        "modulename": "Suspend Premises Invoice",
        "programURL": "/Application/iCABSAPremiseSuspendMaintenance.htm",
        "routeURL": "application/premise/suspend/contract",
        "queryParams": {"currentContractType": "C"},
        "visibility": false
      }, {
        "modulename": "Percentage Price Change - Premises",
        "programURL": "/Application/iCABSAPercentagePriceChange.htm<Premise>",
        "routeURL": "/contractmanagement/servicecoveradmin/percentagepricechange/perchanges/Premise",
        "visibility": false
      }, {
        "modulename": "myRentokil Site Reference",
        "programURL": "/Application/iCABSAPNOLSiteReferenceMaintenance.htm",
        "routeURL": "/contractmanagement/servicecoveradmin/application/pnolSiteRef",
        "visibility": false
      }]
    }, {
      "featurename": "Contract - Service Cover",
      "visibility": false,
      "module": [{
        "modulename": "Service Cover Maintenance",
        "programURL": "/Application/iCABSAServiceCoverMaintenance.htm",
        "routeURL": "servicecovermaintenance/maintenance/servicecover/contract",
        "visibility": false
      }, {
        "modulename": "Cancel Service Cover",
        "programURL": "/Application/iCABSAInactiveServiceCoverInfoMaintenance.htm<cancel>",
        "routeURL": "/contractmanagement/retention/maintenance/inactiveservicecover/cancel",
        "queryParams": {"actionType": "cancel", "currentContractType": "C"},
        "visibility": false
      }, {
        "modulename": "Delete Service Cover",
        "programURL": "/Application/iCABSAServiceCoverSelectMaintenance.htm<Pending>",
        "routeURL": "/contractmanagement/retention/clientRetention/serviceCoverSelectMaintenance",
        "visibility": false
      }, {
        "modulename": "Amend Service Cover Deletion",
        "programURL": "/Application/iCABSAInactiveServiceCoverInfoMaintenance.htm",
        "routeURL": "/contractmanagement/retention/maintenance/inactiveservicecover/delete",
        "queryParams": {"actionType": "delete", "currentContractType": "C"},
        "visibility": false
      }, {
        "modulename": "Reinstate Service Cover",
        "programURL": "/Application/iCABSAInactiveServiceCoverInfoMaintenance.htm<reinstate>",
        "routeURL": "/contractmanagement/retention/maintenance/inactiveservicecover/reinstate",
        "queryParams": {"actionType": "reinstate", "currentContractType": "C"},
        "visibility": false
      }, {
        "modulename": "Delete Service Detail",
        "programURL": "/Application/iCABSAServiceCoverDetailMaintenance.htm<Pending>",
        "routeURL": "/application/ServiceCoverDetailMaintenance",
        "queryParams": {"Pending": "true"},
        "visibility": false
      }, {
        "modulename": "Reduce Service Cover",
        "programURL": "/Application/iCABSAServiceCoverMaintenance.htm",
        "routeURL": "servicecovermaintenance/maintenance/servicecover/reduce",
        "queryParams": {"PendingReduction": "true"},
        "visibility": false
      }, {
        "modulename": "Retained Service Cover Acceptance",
        "programURL": "/Application/iCABSAServiceCoverAcceptGrid.htm",
        "routeURL": "/billtocash/servicecover/acceptGrid",
        "visibility": false
      }, {
        "modulename": "Delivery Confirmation",
        "programURL": "/Application/iCABSAServiceCoverUnsuspendGrid.htm",
        "routeURL": "/contractmanagement/serviceProcesses/deliveryConfirmation",
        "visibility": false
      }, {
        "modulename": "Suspend Product Service",
        "programURL": "/Application/iCABSAServiceCoverServiceSuspendMaintenance.htm",
        "routeURL": "/application/maintenance/servicecover/service/suspend",
        "visibility": false
      }, {
        "modulename": "Suspend Product Invoice",
        "programURL": "/Application/iCABSAServiceCoverSuspendMaintenance.htm",
        "routeURL": "/application/servicecover/suspend/contract",
        "queryParams": {"currentContractType": "C"},
        "visibility": false
      }, {
        "modulename": "Product Upgrade",
        "programURL": "/Application/iCABSAProductCodeUpgrade.htm",
        "routeURL": "/contractmanagement/servicecoveradmin/contractservicecover/productupgrade",
        "visibility": false
      }, {
        "modulename": "Trial Period",
        "programURL": "/Application/iCABSAServiceCoverTrialPeriodReleaseGrid.htm",
        "routeURL": "/contractmanagement/servicecoveradmin/servicecover/TialPeriodRelease",
        "visibility": false
      }, {
        "modulename": "Visit Anniversary Date Change",
        "programURL": "/Application/iCABSAContractAnniversaryChange.htm",
        "routeURL": "/contractmanagement/contractadmin/contractAnniversaryChange",
        "visibility": false
      }, {
        "modulename": "YTD Maintenance",
        "programURL": "/Application/iCABSAServiceCoverYTDMaintenance.htm",
        "routeURL": "/contractmanagement/servicecoveradmin/servicecover/YTDMaintenance",
        "visibility": false
      }, {
        "modulename": "Percentage Price Change - Product",
        "programURL": "/Application/iCABSAPercentagePriceChange.htm<ServiceCover>",
        "routeURL": "/contractmanagement/servicecoveradmin/percentagepricechange/perchanges/ServiceCover",
        "visibility": false
      }]
    }, {
      "featurename": "Job",
      "visibility": false,
      "module": [{
        "modulename": "Job Maintenance",
        "programURL": "/Application/iCABSAContractMaintenance.htm<job>",
        "routeURL": "/contractmanagement/maintenance/job",
        "visibility": false,
        "quickLinks":"true",
        "icon": "glyphicon-duplicate"
      }, {
        "modulename": "Cancel Job",
        "programURL": "/Application/iCABSAInactiveContractInfoMaintenance.htm<jobcancel>",
        "routeURL": "/contractmanagement/retention/inactive/contractinfo/cancel",
        "queryParams": {"CurrentContractType": "J"},
        "visibility": false
      }, {
        "modulename": "Suspend Job Invoice",
        "programURL": "/Application/iCABSAContractSuspendMaintenance.htm<job>",
        "routeURL": "sales/job/suspend",
        "queryParams": {"currentContractType": "J"},
        "visibility": false
      }, {
        "modulename": "Job Premises Maintenance",
        "programURL": "/Application/iCABSAPremiseMaintenance.htm<job>",
        "routeURL": "/contractmanagement/premisesmaintenance/maintenance/premise",
        "queryParams": {"contractTypeCode": "J"},
        "visibility": false
      }, {
        "modulename": "Cancel Job Premises",
        "programURL": "/Application/iCABSAInactivePremiseInfoMaintenance.htm<jobcancel>",
        "routeURL": "/contractmanagement/retention/inactive/premiseinfo/job/cancel",
        "queryParams": {"contractTypeCode": "J"},
        "visibility": false
      }, {
        "modulename": "Suspend Job Premises Invoice",
        "programURL": "/Application/iCABSAPremiseSuspendMaintenance.htm<job>",
        "routeURL": "application/premise/suspend/job",
        "queryParams": {"currentContractType": "J"},
        "visibility": false
      }, {
        "modulename": "myRentokil Site Reference",
        "programURL": "/Application/iCABSAPNOLSiteReferenceMaintenance.htm",
        "routeURL": "/contractmanagement/servicecoveradmin/application/pnolSiteRef",
        "visibility": false
      }, {
        "modulename": "Job Service Cover Maintenance",
        "programURL": "/Application/iCABSAServiceCoverMaintenance.htm<job>",
        "routeURL": "servicecovermaintenance/maintenance/servicecover/job",
        "queryParams": {"currentContractType": "J"},
        "visibility": false
      }, {
        "modulename": "Cancel Job Service Cover",
        "programURL": "/Application/iCABSAInactiveServiceCoverInfoMaintenance.htm<jobcancel>",
        "routeURL": "/contractmanagement/retention/maintenance/inactiveservicecover/canceljob",
        "queryParams": {"actionType": "cancel", "currentContractType": "J"},
        "visibility": false
      }, {
        "modulename": "Suspend Job Product Invoice",
        "programURL": "/Application/iCABSAServiceCoverSuspendMaintenance.htm<job>",
        "routeURL": "/application/servicecover/suspend/job",
        "queryParams": {"currentContractType": "J"},
        "visibility": false
      }, {
        "modulename": "Invoice On First / Last Visit",
        "programURL": "/Application/iCABSAServiceCoverInvoiceOnFirstVisitMaintenance.htm<job>",
        "routeURL": "/contractmanagement/servicecoveradmin/maintenance/serviceCoverInvoiceMaintenceOnFirstVisit",
        "visibility": false
      }, {
        "modulename": "Visit Frequency",
        "programURL": "/Application/iCABSAServiceCoverFrequencyMaintenance.htm<job>",
        "routeURL": "/contractmanagement/servicecoveradmin/maintenance/ServiceCoverFrequencyMaintenance",
        "visibility": false
      }]
    }, {
      "featurename": "Product Sale",
      "visibility": false,
      "module": [{
        "modulename": "Product Sale Maintenance",
        "programURL": "/Application/iCABSAContractMaintenance.htm<product>",
        "routeURL": "/contractmanagement/maintenance/product",
        "visibility": false,
        "quickLinks":"true",
        "icon": "glyphicon-barcode"
      }, {
        "modulename": "Cancel Product Sale",
        "programURL": "/Application/iCABSAInactiveContractInfoMaintenance.htm<productcancel>",
        "routeURL": "/contractmanagement/retention/inactive/contractinfo/cancel",
        "queryParams": {"CurrentContractType": "P"},
        "visibility": false
      }, {
        "modulename": "Product Sale Premises Maintenance",
        "programURL": "/Application/iCABSAPremiseMaintenance.htm<product>",
        "routeURL": "/contractmanagement/premisesmaintenance/maintenance/premise",
        "queryParams": {"contractTypeCode": "P"},
        "visibility": false
      }, {
        "modulename": "Cancel Product Sale Premises",
        "programURL": "/Application/iCABSAInactivePremiseInfoMaintenance.htm<productcancel>",
        "routeURL": "/contractmanagement/retention/inactive/premiseinfo/product/cancel",
        "queryParams": {"contractTypeCode": "P"},
        "visibility": false
      }, {
        "modulename": "myRentokil Site Reference",
        "programURL": "/Application/iCABSAPNOLSiteReferenceMaintenance.htm",
        "routeURL": "/contractmanagement/servicecoveradmin/application/pnolSiteRef",
        "visibility": false
      }, {
        "modulename": "Product Sale Service Cover Maintenance",
        "programURL": "/Application/iCABSAProductSalesSCEntryGrid.htm",
        "routeURL": "grid/service/contractmanagement/maintenance/productSalesSCEntryGrid",
        "visibility": false
      }, {
        "modulename": "Cancel Product Sale Service Cover",
        "programURL": "/Application/iCABSAInactiveServiceCoverInfoMaintenance.htm<productcancel>",
        "routeURL": "/contractmanagement/retention/maintenance/inactiveservicecover/cancelproduct",
        "queryParams": {"actionType": "cancel", "currentContractType": "P"},
        "visibility": false
      }, {
        "modulename": "Deliveries Due",
        "programURL": "/Application/iCABSAProductSalesDeliveriesDueGrid.htm<product>",
        "routeURL": "/servicedelivery/reportsplanning/productsalesdeliverydue",
        "queryParams": {"currentContractType": "P"},
        "visibility": false
      }]
    }, {
      "featurename": "Sales Maintenance",
      "visibility": false,
      "module": [{
        "modulename": "New / Reneg Adjustment",
        "programURL": "/Sales/iCABSSSalesStatsAdjustmentGrid.htm",
        "routeURL": "/contractmanagement/reports/salesMaintenance/renegAdjustments",
        "visibility": false
      }, {
        "modulename": "Contract Service Values Adjustment",
        "programURL": "/Sales/iCABSSSalesStatisticsServiceValueGrid.htm",
        "routeURL": "/grid/sales/nav/statistics/servicevalue/contract",
        "queryParams": {"currentContractType": "C"},
        "visibility": false
      }, {
        "modulename": "Job Service Values Adjustment",
        "programURL": "/Sales/iCABSSSalesStatisticsServiceValueGrid.htm<job>",
        "routeURL": "/grid/sales/nav/statistics/servicevalue/job",
        "queryParams": {"currentContractType": "J"},
        "visibility": false
      }, {
        "modulename": "Product Sale Service Values Adjustment",
        "programURL": "/Sales/iCABSSSalesStatisticsServiceValueGrid.htm<product>",
        "routeURL": "/grid/sales/nav/statistics/servicevalue/product",
        "queryParams": {"currentContractType": "P"},
        "visibility": false
      }, {
        "modulename": "Negotiating Employee Reassign",
        "programURL": "/Business/iCABSBContractSalesEmployeeReassignGrid.htm",
        "routeURL": "/contractmanagement/business/contractSalesEmployeeReassignGrid",
        "visibility": false
      }, {
        "modulename": "Portfolio Rezone (Sales)",
        "programURL": "/Business/iCABSBSalesAreaPostcodeRezoneGrid.htm",
        "routeURL": "contractmanagement/general/rezone/salesarea/postcode",
        "visibility": false
      }, {
        "modulename": "Postcode Rezone (Sales)",
        "programURL": "/Business/iCABSBSalesAreaRezoneGrid.htm",
        "routeURL": "contractmanagement/general/rezone/salesarea/grid",
        "visibility": false
      }]
    }, {
      "featurename": "Invoicing & API",
      "visibility": false,
      "module": [{
        "modulename": "Credit / Charge Summary Grid",
        "programURL": "/Application/iCABSAProRataChargeBranchGrid.htm",
        "routeURL": "/grid/sales/prorata/charge/branch",
        "visibility": false
      }, {
        "modulename": "Credit / Charge Approval",
        "programURL": "/Application/iCABSACreditApprovalGrid.htm",
        "routeURL": "/grid/application/creditApprovalGrid",
        "visibility": false
      }, {
        "modulename": "Credit / Charge Read-Only",
        "programURL": "/Application/iCABSACreditApprovalGrid.htm<readonly>",
        "routeURL": "/grid/application/creditApprovalGrid",
        "queryParams": {"readonly": "true"},
        "visibility": false
      }, {
        "modulename": "Invoice Run Forecast: Forecast Generation",
        "programURL": "/ApplicationReport/iCABSARGenerateNextInvoiceRunForecast.htm",
        "routeURL": "/billtocash/application/invoiceRunForecastComponent",
        "visibility": false
      }, {
        "modulename": "Invoice Run Forecast: Forecast Grid",
        "programURL": "/ApplicationReport/iCABSARNextInvoiceRunForecastBusinessGrid.htm",
        "routeURL": "/bireports/invoice/forecastgrid",
        "visibility": false
      }, {
        "modulename": "Invoice Run",
        "programURL": "/Business/iCABSBInvoiceRunDatesGrid.htm",
        "routeURL": "/billtocash/rundatesgrid",
        "visibility": false,
        "quickLinks": true,
        "icon": "glyphicon-credit-card"
      }, {
        "modulename": "Single Invoice Run",
        "programURL": "/Business/iCABSBInvoiceRunDatesGrid.htm",
        "routeURL": "/billtocash/rundatesgrid",
         "queryParams": {"single": "true"},
        "visibility": false
      }, {
        "modulename": "Preview API",
        "programURL": "/Application/iCABSAApplyAPIGrid.htm",
        "routeURL": "/billtocash/apigrid",
        "visibility": false
      }, {
        "modulename": "Apply API",
        "programURL": "/Application/iCABSAApplyAPIGeneration.htm",
        "routeURL": "/billtocash/apigeneration",
        "visibility": false
      }, {
        "modulename": "API Code / Rate",
        "programURL": "/Business/iCABSBAPICodeMaintenance.htm",
        "routeURL": "/billtocash/apicodemaintenance",
        "visibility": false
      }, {
        "modulename": "Exempt A Contract",
        "programURL": "/Application/iCABSAContractAPIMaintenance.htm",
        "routeURL": "/billtocash/contract/apiexempt",
        "visibility": false
      }, {
        "modulename": "API Date Change",
        "programURL": "/Application/iCABSAAPIDateMaintenance.htm",
        "routeURL": "/billtocash/apidate",
        "visibility": false
      }, {
        "modulename": "Service Cover API Update",
        "programURL": "/Application/iCABSAServiceCoverAPIGrid.htm",
        "routeURL": "/billtocash/serviceCoverApiGrid",
        "visibility": false
      }, {
        "modulename": "Reverse API Contract",
        "programURL": "/Application/iCABSAAPIReverse.htm<Contract>",
        "routeURL": "/billtocash/contract/apireverse",
        "visibility": false
      }, {
        "modulename": "Reverse API Premises",
        "programURL": "/Application/iCABSAAPIReverse.htm<Premise>",
        "routeURL": "/billtocash/premise/apireverse",
        "visibility": false
      }, {
        "modulename": "Reverse API Service Cover",
        "programURL": "/Application/iCABSAAPIReverse.htm<Service>",
        "routeURL": "/billtocash/servicecover/apireverse",
        "visibility": false
      }, {
        "modulename": "Release For Invoicing",
        "programURL": "/Application/iCABSReleaseForInvoiceGrid.htm",
        "routeURL": "/billtocash/application/releaseforinvoiceGrid",
        "visibility": false
      }, {
        "modulename": "Retained Service Cover Acceptance",
        "programURL": "/Application/iCABSAServiceCoverAcceptGrid.htm",
        "routeURL": "/billtocash/servicecover/acceptGrid",
        "visibility": false
      }, {
        "modulename": "Invoice Changes",
        "programURL": "/Application/iCABSAInvoiceDetailsMaintenance.htm",
        "routeURL": "/billtocash/maintenance/invoiceDetailsMaintainance",
        "visibility": false
      }, {
        "modulename": "Credit & Re-invoice",
        "programURL": "/Application/iCABSACreditAndReInvoiceMaintenance.htm",
        "routeURL": "/billtocash/postInvoiceManagement/creditAndReInvoiceMaintenance",
        "visibility": false
      }, {
        "modulename": "Invoice Text Maintenance",
        "programURL": "/Application/iCABSAInvoicePrintMaintenance.htm",
        "routeURL": "application/invoice/print/maintenance",
        "visibility": false
      }, {
        "modulename": "Invoice & Credit Note History",
        "programURL": "/Application/iCABSAInvoiceHeaderGrid.htm",
        "routeURL": "grid/application/invoiceheadergridcomponent",
        "visibility": false
      }, {
        "modulename": "Invoices By Account",
        "programURL": "/Application/iCABSAInvoiceByAccountGrid.htm",
        "routeURL": "grid/application/nav/invoice/account",
        "visibility": false
      }]
    }, {
      "featurename": "Letters & Labels",
      "visibility": false,
      "module": [{
        "modulename": "Renewal Letter Generation",
        "programURL": "/ApplicationReport/iCABSRenewalGenerate.htm",
        "routeURL": "sales/renewalLetterGenerate",
        "visibility": false
      }, {
        "modulename": "Renewal Letter Print",
        "programURL": "/Sales/iCABSRenewalExtractGeneration.htm",
        "routeURL": "contractmanagement/reports/sales/renewalextractgeneration",
        "visibility": false
      }, {
        "modulename": "Anniversary Letter Generation",
        "programURL": "/ApplicationReport/iCABSAnniversaryGenerate.htm",
        "routeURL": "grid/service/AnniversaryGenerate",
        "visibility": false
      }, {
        "modulename": "Customer Quarterly Returns",
        "programURL": "/ApplicationReport/iCABSARCustomerQuarterlyReturnsPrint.htm",
        "routeURL": "grid/service/nav/customerquarterlyreturns",
        "visibility": false
      },
      {
        "modulename": "Non-Returned Paperwork Audit",
        "programURL": "/ApplicationReport/iCABSARReturnedPaperWorkGrid.htm",
        "routeURL": "/servicedelivery/lettersandlabels/returnedpaperworkgrid",
        "visibility": false
      },
      {
     "modulename": "Additional Letter Types Print",
        "programURL": "/ApplicationReport/iCABSARCustomerLettersByTypeGrid.htm",
       "routeURL": "/contractmanagement/reports/customerletters/bytype",
       "visibility": false
      }]
    }, {
      "featurename": "Imports & Notifications",
      "visibility": false,
      "module": [{
        "modulename": "Bulk SMS Messages: Business",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/ccm/sendbulksms/business",
        "visibility": false
      }, {
        "modulename": "Bulk SMS Messages: Branch",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/ccm/sendbulksms/branch",
        "visibility": false
      }, {
        "modulename": "Email Messages",
        "programURL": "/Business/iCABSBEmailGrid.htm",
        "routeURL": "/ccm/business/email",
        "visibility": false
      }, {
        "modulename": "SMS Messages",
        "programURL": "/ContactManagement/iCABSCMSMSMessagesGrid.htm",
        "routeURL": "/ccm/service/smsmessages",
        "visibility": false
      }, {
        "modulename": "Notification Templates",
        "programURL": "/System/iCABSSNotificationTemplateMaintenance.htm",
        "routeURL": "/ccm/system/maintenance/notificationtemplate",
        "visibility": false
      }, {
        "modulename": "Notification Groups",
        "programURL": "/System/iCABSSNotificationGroupMaintenance.htm",
        "routeURL": "/ccm/notification/group/maintenance",
        "visibility": false
      }, {
        "modulename": "Team Maintenance",
        "programURL": "/System/iCABSSTeamMaintenance.htm",
        "routeURL": "/service/teamMaintenance",
        "visibility": false
      }, {
        "modulename": "Ticket Type",
        "programURL": "/System/iCABSSContactTypeMaintenance.htm",
        "routeURL": "/ccm/system/contact/contacttype",
        "visibility": false
      }, {
        "modulename": "Ticket Type Detail",
        "programURL": "/System/iCABSSContactTypeDetailMaintenance.htm",
        "routeURL": "/application/maintenance/contacttype/detail",
        "visibility": false
      }, {
        "modulename": "Ticket Type Detail - Translation",
        "programURL": "/System/iCABSSContactTypeDetailLangMaintenance.htm",
        "routeURL": "sales/contact/type/detial/langmaintenance",
        "visibility": false
      }, {
        "modulename": "Import Records",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/itfunctions/importing",
        "visibility": false
      }]
    }, {
      "featurename": "Waste",
      "visibility": false,
      "module": [{
        "modulename": "Daily Prenotification Report",
        "programURL": "/ApplicationReport/iCABSARDailyPrenotificationReport.htm",
        "routeURL": "/servicedelivery/dailyprenotificationreport",
        "visibility": false
      }, {
        "modulename": "Environment Agency Business Waste Generation Report",
        "programURL": "/ApplicationReport/iCABSAREnvAgencyBusinessWaste.htm",
        "routeURL": "/servicedelivery/wasteconsignment/envagencybusinesswaste",
        "visibility": false
      }, {
        "modulename": "Environment Agency Exceptions",
        "programURL": "/ApplicationReport/iCABSAREnvAgencyExceptions.htm",
        "routeURL": "/servicedelivery/wasteconsignment/envagencyexceptions",
        "visibility": false
      }, {
        "modulename": "Environment Agency Quarterly Returns",
        "programURL": "/ApplicationReport/iCABSAREnvAgencyQuarterlyReturn.htm",
        "routeURL": "/servicedelivery/envagencyquarterly/envagencyquarterlyreturn",
        "visibility": false
      }, {
        "modulename": "Annual Prenotification Report",
        "programURL": "/ApplicationReport/iCABSARPrenotificationReport.htm",
        "routeURL": "/servicedelivery/reports/prenotificationreport",
        "visibility": false
      }, {
        "modulename": "Waste Transfer Notes",
        "programURL": "/ApplicationReport/iCABSARWasteTransferNotesPrint.htm",
        "routeURL": "/servicedelivery/lettersandlabels/wastetransfernotes",
        "visibility": false
      }]
    }, {
      "featurename": "Calendars",
      "visibility": false,
      "module": [{
        "modulename": "Apply Annual Calendar",
        "programURL": "/Application/iCABSAServiceCoverCalendarDatesMaintenance.htm",
        "routeURL": "sales/ServiceCoverCalendarDatesMaintenance",
        "visibility": false
      }, {
        "modulename": "Annual Calendar Summary",
        "programURL": "/Application/iCABSAServiceCoverCalendarDatesMaintenanceGrid.htm",
        "routeURL": "/serviceplanning/calendarandSeasons/serviceCoverCalendarDateMaintenanceGrid",
        "visibility": false
      }, {
        "modulename": "Annual Calendar Template",
        "programURL": "/Application/iCABSACalendarTemplateMaintenance.htm",
        "routeURL": "/application/calendarTemplateMaintenance",
        "visibility": false
      }, {
        "modulename": "Annual Calendar Template Access",
        "programURL": "/Application/iCABSACalendarTemplateBranchAccessGrid.htm",
        "routeURL": "/grid/application/nav/calendarTemplateBranchAccessGrid",
        "visibility": false
      }, {
        "modulename": "Annual Calendar Template Use",
        "programURL": "/Application/iCABSACalendarServiceGrid.htm<TemplateUse>",
        "routeURL": "/serviceplanning/Templates/CalendarTemplateUse",
        "visibility": false
      }, {
        "modulename": "Annual Calendar Template Change History",
        "programURL": "/Application/iCABSACalendarHistoryGrid.htm",
        "routeURL": "/serviceplanning/application/calenderHistoryGrid",
        "visibility": false
      }, {
        "modulename": "Closed Calendar Template",
        "programURL": "/Application/iCABSAClosedTemplateMaintenance.htm",
        "routeURL": "/serviceplanning/application/closedtemplatemaintenance",
        "visibility": false
      }, {
        "modulename": "Closed Calendar Template Access",
        "programURL": "/Application/iCABSAClosedTemplateBranchAccessGrid.htm",
        "routeURL": "/grid/service/drilldown/ClosedTemplateBranchAccessGrid",
        "visibility": false
      }, {
        "modulename": "Closed Calendar Template Use",
        "programURL": "/Application/iCABSAClosedServiceGrid.htm<TemplateUse>",
        "routeURL": "/serviceplanning/Templates/HolidayClosedTemplateUse",
        "visibility": false,
        "queryParams": {"noroutegaurd": true}
      }, {
        "modulename": "Apply Seasonal Service",
        "programURL": "/Application/iCABSAServiceCoverSeasonalDatesMaintenance.htm",
        "routeURL": "/application/servicecover/seasonaldatesmaintenance",
        "visibility": false
      }, {
        "modulename": "Seasonal Template",
        "programURL": "/Application/iCABSASeasonalTemplateMaintenance.htm",
        "routeURL": "application/seasonal/templatemaintenance",
        "visibility": false
      }, {
        "modulename": "Seasonal Template Details",
        "programURL": "/Application/iCABSASeasonalTemplateDetailGrid.htm",
        "routeURL": "/grid/service/drilldown/seasonaltemplatedetailgrid",
        "visibility": false
      }]
    }, {
      "featurename": "Users & Employees",
      "visibility": false,
      "module": [{
        "modulename": "User",
        "programURL": "/Model/riMUserInformationMaintenance.htm",
        "routeURL": "/itfunctions/maintenance/user",
        "visibility": false
      }, {
        "modulename": "User Authority",
        "programURL": "/Business/iCABSBUserAuthorityMaintenance.htm",
        "routeURL": "/people/business/authoritymaintainance",
        "visibility": false
      }, {
        "modulename": "User Type",
        "programURL": "/Model/riMUserTypeMaintenance.htm",
        "routeURL": "/itfunctions/maintenance/usertype",
        "visibility": false
      }, {
        "modulename": "User Type Menu Access",
        "programURL": "/Model/riMUserTypeMenuAccessMaintenance.htm",
        "routeURL": "/itfunctions/maintenance/usertype/menuaccess",
        "visibility": false
      }, {
        "modulename": "Employee",
        "programURL": "/Business/iCABSBEmployeeMaintenance.htm",
        "routeURL": "/people/business/employeemaintenance",
        "visibility": false
      }, {
        "modulename": "Employee Number Change",
        "programURL": "/Application/iCABSAChangeEmployeeNumber.htm",
        "routeURL": "/people/tablemaintenance/changeemployeenumber",
        "visibility": false
      }]
    }, {
      "featurename": "Business & Branch",
      "visibility": false,
      "module": [{
        "modulename": "Business Registry",
        "programURL": "/Business/iCABSBBusinessRegistryGrid.htm",
        "routeURL": "/itfunctions/businessRegistryGrid",
        "visibility": false
      }, {
        "modulename": "Company",
        "programURL": "/Business/iCABSBCompanyMaintenance.htm",
        "routeURL": "/itfunctions/maintenance/company",
        "visibility": false
      }, {
        "modulename": "Branch",
        "programURL": "/Business/iCABSBBranchMaintenance.htm",
        "routeURL": "/itfunctions/maintenance/branch",
        "visibility": false
      }, {
        "modulename": "Depot",
        "programURL": "/Business/iCABSBDepotMaintenance.htm",
        "routeURL": "/vehiclemanagement/depotmaintenance",
        "visibility": false
      }, {
        "modulename": "Postcode",
        "programURL": "/Business/iCABSBPostcodeMaintenance.htm",
        "routeURL": "/contractmanagement/areas/branch/postalcode/maintenance",
        "visibility": false
      }, {
        "modulename": "Sales Area",
        "programURL": "/Business/iCABSBSalesAreaGrid.htm",
        "routeURL": "/contractmanagement/areas/salesAreaGrid",
        "visibility": false
      }, {
        "modulename": "Service Area",
        "programURL": "/Business/iCABSBBranchServiceAreaGrid.htm",
        "routeURL": "/servicedelivery/branch/serviceArea",
        "visibility": false
      }, {
        "modulename": "Branch Holidays",
        "programURL": "/Business/iCABSBBranchHolidayGrid.htm",
        "routeURL": "/grid/service/business/branchholiday",
        "visibility": false
      }, {
        "modulename": "Invoice Ranges",
        "programURL": "/Business/iCABSBInvoiceRangeUpdateGrid.htm",
        "routeURL": "grid/sales/nav/invoice/range/update/grid",
        "visibility": false
      }]
    }, {
      "featurename": "Product & Service Setup",
      "visibility": false,
      "module": [{
        "modulename": "Product Full Access",
        "programURL": "/Business/iCABSBProductMaintenance.htm",
        "routeURL": "application/product/maintenance",
        "visibility": false
      }, {
        "modulename": "Product Language",
        "programURL": "/Business/iCABSBProductLanguageMaintenance.htm",
        "routeURL": "/servicedelivery/maintenance/productlanguage",
        "visibility": false
      }, {
        "modulename": "Product Service Group",
        "programURL": "/Business/iCABSBProductServiceGroupMaintenance.htm",
        "routeURL": "application/maintenance/productservice/group",
        "visibility": false
      }, {
        "modulename": "Expense Code",
        "programURL": "/Business/iCABSBExpenseCodeMaintenance.htm",
        "routeURL": "sales/expensecode",
        "visibility": false
      }, {
        "modulename": "Product Expense Code",
        "programURL": "/Business/iCABSBProductExpenseMaintenance.htm",
        "routeURL": "contractmanagement/productadmin/product/expense/maintenance",
        "visibility": false
      }, {
        "modulename": "Product Detail",
        "programURL": "/Business/iCABSBProductDetailMaintenance.htm",
        "routeURL": "application/maintenance/product/detail",
        "visibility": false
      }, {
        "modulename": "Product Cover",
        "programURL": "/Business/iCABSBProductCoverMaintenance.htm",
        "routeURL": "/contractmanagement/productadmin/business/service/productcover",
        "visibility": false
      }, {
        "modulename": "Valid Linked Products",
        "programURL": "/Business/iCABSBValidLinkedProductsGrid.htm",
        "routeURL": "/servicedelivery/products/valid/linked",
        "visibility": false
      }, {
        "modulename": "Prep",
        "programURL": "/Business/iCABSBPreparationMaintenance.htm",
        "routeURL": "/application/application/preparationmaintenance",
        "visibility": false
      }, {
        "modulename": "Prep Charge Rate",
        "programURL": "/Business/iCABSBPrepChargeRateMaintenance.htm",
        "routeURL": "/people/prepcharge/ratemaintenance",
        "visibility": false
      }, {
        "modulename": "Infestation Level",
        "programURL": "/Business/iCABSBInfestationLevelMaintenance.htm",
        "routeURL": "/maintenance/infestation/level",
        "visibility": false
      }, {
        "modulename": "Detector",
        "programURL": "/Business/iCABSBDetectorMaintenance.htm",
        "routeURL": "/maintenance/detector",
        "visibility": false
      }, {
        "modulename": "Visit Action",
        "programURL": "/Business/iCABSBVisitActionMaintenance.htm",
        "routeURL": "/servicedelivery/business/visitaction",
        "visibility": false
      }, {
        "modulename": "Visit Type",
        "programURL": "/Business/iCABSBVisitTypeMaintenance.htm",
        "routeURL": "/service/maintenance/visit/type",
        "visibility": false
      }, {
        "modulename": "Waste Consignment Note Range",
        "programURL": "/Business/iCABSBWasteConsignmentNoteRangeGrid.htm",
        "routeURL": "/contractmanagement/wasteconsign/business/waste/grid",
        "visibility": false
      }, {
        "modulename": "Waste Consignment Note Range Type",
        "programURL": "/Business/iCABSBWasteConsignmentNoteRangeTypeMaintenance.htm",
        "routeURL": "/contractmanagement/wasteconsign/note/range/type",
        "visibility": false
      },{
          "modulename": "Waste Consignment Note History",
          "programURL": "/Business/iCABSBWasteConsignmentNoteHistoryGrid.htm",
          "routeURL": "/contractmanagement/wasteconsign/note/history/grid",
          "visibility": false
      },{
        "modulename": "Vehicle",
        "programURL": "/Business/iCABSBVehicleMaintenance.htm<job>",
        "routeURL": "/vehiclemanagement/vehiclemaintenance",
        "visibility": false
      },{
        "modulename": "Vehicle Type",
        "programURL": "/Business/iCABSBVehicleTypeMaintenance.htm",
        "routeURL": "/vehiclemanagement/vehicletype",
        "visibility": false
      },{
        "modulename": "Vehicle Class",
        "programURL": "/Business/iCABSBVehicleClassMaintenance.htm",
        "routeURL": "/vehiclemanagement/vehicleclass",
        "visibility": false
      },{
        "modulename": "Vehicle Components",
        "programURL": "/Business/iCABSBVehicleComponentsMaintenance.htm",
        "routeURL": "/vehiclemanagement/vehiclecomponentsmaintenance",
        "visibility": false
      },{
        "modulename": "Vehicle Model",
        "programURL": "/Business/iCABSBVehicleManufacturerModelMaintenance.htm",
        "routeURL": "/vehiclemanagement/vehiclemanufacturermodelmaintenance",
        "visibility": false
      }]
    }, {
      "featurename": "Sales Setup",
      "visibility": false,
      "module": [{
        "modulename": "Business Origin",
        "programURL": "/Business/iCABSBBusinessOriginMaintenance.htm",
        "routeURL": "/prospecttocontract/prospect/business/businessOrigin",
        "visibility": false
      }, {
        "modulename": "Customer Type",
        "programURL": "/System/iCABSSCustomerTypeMaintenance.htm",
        "routeURL": "/application/maintenance/customertype",
        "visibility": false
      }, {
        "modulename": "Lost Business Detail",
        "programURL": "/Business/iCABSBLostBusinessDetailGrid.htm",
        "routeURL": "sales/lostbusinessmaintenance",
        "routeURL": "/prospecttocontract/lostbusinessdetailgrid",
        "visibility": false
      }]
    }, {
      "featurename": "Tools",
      "visibility": true,
      "module": [{
        "modulename": "Batch Process Monitor",
        "programURL": "/Model/riMBatchProcessMonitorSearch.htm",
        "routeURL": "/itfunctions/batchprocess/monitor",
        "visibility": false
      }, {
        "modulename": "Report Process Viewer",
        "programURL": "/Model/riMReportViewerSearch.htm",
        "routeURL": "/itfunctions/riMReportViewerSearch",
        "visibility": false
      }, {
        "modulename": "Program Schedule",
        "programURL": "/Model/riMBatchProgramScheduleMaintenance.htm",
        "routeURL": "sales/batchProgramSchedule",
        "visibility": false
      }, {
        "modulename": "Program Maintenance",
        "programURL": "/Model/riMGBatchProgramMaintenance.htm",
        "routeURL": "/itfunctions/batchprogram/maintenance",
        "visibility": false
      }, {
        "modulename": "User Search",
        "programURL": "/Model/riMUserSearch.htm",
        "routeURL": "search/user",
        "visibility": false
      }, {
        "modulename": "Employee Export: Business",
        "programURL": "/ApplicationReport/iCABSAREmployeeExportGrid.htm<Business>",
        "routeURL": "/people/employeeExport/business",
        "queryParams": {"Business": "exportType"},
        "visibility": false
      }, {
        "modulename": "Employee Export: Region",
        "programURL": "/ApplicationReport/iCABSAREmployeeExportGrid.htm<Region>",
        "routeURL": "/people/employeeExport/region",
        "queryParams": {"Region": "exportType"},
        "visibility": false
      }, {
        "modulename": "Employee Export: Branch",
        "programURL": "/ApplicationReport/iCABSAREmployeeExportGrid.htm<Branch>",
        "routeURL": "/people/employeeExport/branch",
        "queryParams": {"Branch": "exportType"},
        "visibility": false
      }, {
        "modulename": "Move Branch By Postcode",
        "programURL": "/Application/iCABSAPostcodeMoveBranch.htm",
        "routeURL": "/contractmanagement/premisesadmin/premise/postcodemovebranch",
        "visibility": false
      }, {
        "modulename": "Clear Down Plan Visits",
        "programURL": "/Service/iCABSSeClearDownPlanVisits.htm",
        "routeURL": "/serviceplanning/visitmaintenance/cleardownplanvisit",
        "visibility": false
      }, {
        "modulename": "Translations",
        "programURL": "/Model/riMGTranslationMaintenance.htm",
        "routeURL": "/itfunctions/maintenance/rimgtranslation",
        "visibility": false
      }, {
        "modulename": "System / Business Visit Type",
        "programURL": "/Business/iCABSBSystemBusinessVisitTypeMaintenance.htm",
        "routeURL": "/maintenance/systembusiness/visit/type",
        "visibility": false
      }, {
        "modulename": "Business System Characteristics",
        "programURL": "/System/iCABSSBusinessSystemCharacteristicsCustomMaintenance.htm",
        "routeURL": "/itfunctions/batchprocess/syscharmonitor",
        "visibility": false
      }, {
        "modulename": "System Parameters Maintenance",
        "programURL": "/System/iCABSSSystemParameterMaintenance.htm",
        "routeURL": "/itfunctions/maintenance/maintainParameters/system",
        "visibility": false
      }]
    }, {
      "featurename": "Registry Setup A",
      "visibility": false,
      "hidein": "All",
      "module": [{
          "modulename": "Advantage",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Advantage",
          "visibility": false
        },
        {
          "modulename": "Archiving",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Archiving",
          "visibility": false
        },
        {
          "modulename": "CCM Disputed Invoices",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/CCM Disputed Invoices",
          "visibility": false
        },
        {
          "modulename": "CCM Escalation",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/CCM Escalation",
          "visibility": false
        },
        {
          "modulename": "CCM Ticket Analysis",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/CCM Ticket Analysis",
          "visibility": false
        },
        {
          "modulename": "ClearDown",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/ClearDown",
          "visibility": false
        },
        {
          "modulename": "Contact Centre Assign",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Contact Centre Assign",
          "visibility": false
        },
        {
          "modulename": "Contact Centre Jobs",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Contact Centre Jobs",
          "visibility": false
        },
        {
          "modulename": "Contact Centre Review",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Contact Centre Review",
          "visibility": false
        },
        {
          "modulename": "Contact Centre Search",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Contact Centre Search",
          "visibility": false
        },
        {
          "modulename": "Contact Person",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Contact Person",
          "visibility": false
        },
        {
          "modulename": "CSV Export Codepage",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/CSV Export Codepage",
          "visibility": false
        },
        {
          "modulename": "Daily Transactions",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Daily Transactions",
          "visibility": false
        },
        {
          "modulename": "Dashboards",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Dashboards",
          "visibility": false
        },
        {
          "modulename": "EDIInvoicesAsPDFs",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/EDIInvoicesAsPDFs",
          "visibility": false
        },
        {
          "modulename": "Electronic Invoice Delivery",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Electrontic Invoice Delivery",
          "visibility": false
        },
        {
          "modulename": "Email Reports",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Email Reports",
          "visibility": false
        },
        {
          "modulename": "FOLE Extract",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/FOLE Extract",
          "visibility": false
        },
        {
          "modulename": "Freeway EDI",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Freeway EDI",
          "visibility": false
        },
        {
          "modulename": "General",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/General",
          "visibility": false
        }]
    }, {
      "featurename": "Registry Setup B",
      "visibility": false,
      "hidein": "all",
      "module": [{
          "modulename": "GoogleCalendar",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/GoogleCalendar",
          "visibility": false
        },
        {
          "modulename": "Invoice Feeder File Transfer",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Invoice Feeder File Transfer",
          "visibility": false
        },
        {
          "modulename": "Invoice PDF Transfer",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Invoice PDF Transfer",
          "visibility": false
        },
        {
          "modulename": "Luminos",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Luminos",
          "visibility": false
        },
        {
          "modulename": "MarktSelect",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/MarktSelect",
          "visibility": false
        },
        {
          "modulename": "MyMedical Feeder",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/MyMedical Feeder",
          "visibility": false
        },
        {
          "modulename": "MyRentokil Feeders",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/MyRentokil Feeders",
          "visibility": false
        },
        {
          "modulename": "MyWashrooms Feeder",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/MyWashrooms Feeder",
          "visibility": false
        },
        {
          "modulename": "Ortec",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Ortec",
          "visibility": false
        },
        {
          "modulename": "PestNet Online Report",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/PestNet Online Report",
          "visibility": false
        },
        {
          "modulename": "PestNetOnline Web Services",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/PestNetOnline Web Services",
          "visibility": false
        },
        {
          "modulename": "Prospect Defaults",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Prospect Defaults",
          "visibility": false
        },
        {
          "modulename": "Qualification User Types",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Qualification User Types",
          "visibility": false
        },
        {
          "modulename": "Reports",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Reports",
          "visibility": false
        },
        {
          "modulename": "Sales Order Processing",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Sales Order Processing",
          "visibility": false
        },
        {
          "modulename": "Scheduling",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Scheduling",
          "visibility": false
        },
        {
          "modulename": "SMS Messaging",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/SMS Messaging",
          "visibility": false
        },
        {
          "modulename": "SystemDirectories",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/SystemDirectories",
          "visibility": false
        },
        {
          "modulename": "Vehicle Parameters",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Vehicle Parameters",
          "visibility": false
        },
        {
          "modulename": "Vertex",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Vertex",
          "visibility": false
        },
        {
          "modulename": "Waste Consignment Note",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Waste Consignment Note",
          "visibility": false
        }]
    }, {
      "featurename": "Registry Setup C",
      "visibility": false,
      "hidein": "ALL",
      "module": [{
          "modulename": "WEB-SESSIONS",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/WEB-SESSIONS",
          "visibility": false
        },
        {
          "modulename": "WOReview",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/WOReview",
          "visibility": false
        },
        {
          "modulename": "Work Order Review",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/Work Order Review",
          "visibility": false
        },
        {
          "modulename": "XContact Person",
          "programURL": "/Model/riRegistry.htm",
          "routeURL": "/itfunctions/registry/XContact Person",
          "visibility": false
        }
      ]
    }]
  }, {
    "domainname": "Finance",
    "id": "Finance",
    "alwaysdisplay": false,
    "visibility": false,
    "feature": [{
      "featurename": "Customer Contact",
      "visibility": false,
      "module": [{
        "modulename": "General Search",
        "programURL": "/ContactManagement/iCABSCMGeneralSearchGrid.htm",
        "routeURL": "/contractmanagement/generalsearchgrid",
        "visibility": false
      }, {
        "modulename": "Contact Centre Search",
        "programURL": "/ContactManagement/iCABSCMCallCentreGrid.htm",
        "routeURL": "/ccm/callcentersearch/",
        "visibility": false
      }, {
        "modulename": "Contact Centre Review",
        "programURL": "/ContactManagement/iCABSCMCallCentreReviewGrid.htm",
        "routeURL": "/ccm/centreReview",
        "visibility": false
      }]
    }, {
      "featurename": "Account",
      "visibility": false,
      "module": [{
        "modulename": "Account Maintenance",
        "programURL": "/Application/iCABSAAccountMaintenance.htm",
        "routeURL": "/contractmanagement/accountmaintenance/account/maintenance",
        "visibility": false
      }, {
        "modulename": "Merge Account",
        "programURL": "/Application/iCABSAAccountMerge.htm",
        "routeURL": "/contractmanagement/accountadmin/account/merge/search",
        "visibility": false
      }, {
        "modulename": "Move Account",
        "programURL": "/Application/iCABSAAccountMove.htm",
        "routeURL": "/contractmanagement/accountadmin/account/move/search",
        "visibility": false
      }, {
        "modulename": "Assign Account",
        "programURL": "/Application/iCABSAAccountAssign.htm",
        "routeURL": "/contractmanagement/accountadmin/account/assign/search",
        "visibility": false
      }, {
        "modulename": "Account Bank Details",
        "programURL": "/Application/iCABSAAccountBankDetailsMaintenance.htm",
        "routeURL": "application/AccountBankDetailsMaintenance",
        "visibility": false
      }, {
        "modulename": "Account Address Change History",
        "programURL": "/Application/iCABSAAccountAddressChangeHistoryGrid.htm",
        "routeURL": "/grid/service/contractmanagement/account/addressChangeHistory",
        "visibility": false
      }, {
        "modulename": "Tax Registration Maintenance",
        "programURL": "/Application/iCABSATaxRegistrationChange.htm",
        "routeURL": "/billtocash/application/taxregistrationchange",
        "visibility": false
      }, {
        "modulename": "Account Owning Branch Maintenance",
        "programURL": "/Application/iCABSAAccountOwnerMaintenance.htm",
        "routeURL": "",
        "visibility": false
      }, {
        "modulename": "Group Account Maintenance",
        "programURL": "/System/iCABSSGroupAccountMaintenance.htm",
        "routeURL": "/contractmanagement/groupaccount/account/groupaccountmaintenance",
        "visibility": false
      }, {
        "modulename": "Group Account Move",
        "programURL": "/System/iCABSSGroupAccountMove.htm",
        "routeURL": "/contractmanagement/groupaccount/account/groupaccountmove",
        "visibility": false
      }]
    }, {
      "featurename": "Invoice Group",
      "visibility": false,
      "module": [{
        "modulename": "Invoice Group Maintenance",
        "programURL": "/Application/iCABSAInvoiceGroupMaintenance.htm",
        "routeURL": "/billtocash/maintenance/invoicegroup/search",
        "visibility": false
      }, {
        "modulename": "Invoice Group / Premises",
        "programURL": "/Application/iCABSAInvoiceGroupPremiseMaintenance.htm",
        "routeURL": "sales/invoicepremisegroup/search",
        "visibility": false
      }, {
        "modulename": "Invoice Group Payment",
        "programURL": "/Application/iCABSAInvoiceGroupPaymentMaintenance.htm",
        "routeURL": "/application/grouppaymentmaintenance",
        "visibility": false
      }]
    }, {
      "featurename": "Contract & Job",
      "visibility": false,
      "module": [{
        "modulename": "Suspend Contract Invoice",
        "programURL": "/Application/iCABSAContractSuspendMaintenance.htm",
        "routeURL": "sales/contract/suspend",
        "queryParams": {"currentContractType": "C"},
        "visibility": false
      }, {
        "modulename": "Suspend Premises Invoice",
        "programURL": "/Application/iCABSAPremiseSuspendMaintenance.htm",
        "routeURL": "application/premise/suspend/contract",
        "queryParams": {"currentContractType": "C"},
        "visibility": false
      }, {
        "modulename": "Suspend Product Invoice",
        "programURL": "/Application/iCABSAServiceCoverSuspendMaintenance.htm",
        "routeURL": "/application/servicecover/suspend/contract",
        "queryParams": {"currentContractType": "C"},
        "visibility": false
      }, {
        "modulename": "Suspend Job Invoice",
        "programURL": "/Application/iCABSAContractSuspendMaintenance.htm<job>",
        "routeURL": "sales/job/suspend",
        "queryParams": {"currentContractType": "J"},
        "visibility": false
      }, {
        "modulename": "Suspend Job Premises Invoice",
        "programURL": "/Application/iCABSAPremiseSuspendMaintenance.htm",
        "routeURL": "application/premise/suspend/job",
        "queryParams": {"currentContractType": "J"},
        "visibility": false
      }, {
        "modulename": "Suspend Job Product Invoice",
        "programURL": "/Application/iCABSAServiceCoverSuspendMaintenance.htm<job>",
        "routeURL": "/application/servicecover/suspend/job",
        "queryParams": {"currentContractType": "J"},
        "visibility": false
      }, {
        "modulename": "Renewal / Period",
        "programURL": "/Application/iCABSAContractRenewalMaintenance.htm",
        "routeURL": "application/contract/renewal",
        "visibility": false
      }, {
        "modulename": "Price / Cost Variance",
        "programURL": "/ApplicationReport/iCABSARVarianceFromPriceCostGrid.htm",
        "routeURL": "/billtocash/variancefromprice",
        "visibility": false
      }]
    }, {
      "featurename": "Invoicing",
      "visibility": false,
      "module": [{
        "modulename": "Credit / Charge Summary Grid",
        "programURL": "/Application/iCABSAProRataChargeBranchGrid.htm",
        "routeURL": "/grid/sales/prorata/charge/branch",
        "visibility": false
      }, {
        "modulename": "Credit / Charge Approval",
        "programURL": "/Application/iCABSACreditApprovalGrid.htm",
        "routeURL": "/grid/application/creditApprovalGrid",
        "visibility": false
      }, {
        "modulename": "Invoice Run Forecast: Forecast Generation",
        "programURL": "/ApplicationReport/iCABSARGenerateNextInvoiceRunForecast.htm",
        "routeURL": "/billtocash/application/invoiceRunForecastComponent",
        "visibility": false
      }, {
        "modulename": "Invoice Run Forecast: Forecast Grid",
        "programURL": "/ApplicationReport/iCABSARNextInvoiceRunForecastBusinessGrid.htm",
        "routeURL": "/bireports/invoice/forecastgrid",
        "visibility": false
      }, {
        "modulename": "Invoice Run",
        "programURL": "/Business/iCABSBInvoiceRunDatesGrid.htm",
       "routeURL": "/billtocash/rundatesgrid",
        "visibility": false
      }, {
        "modulename": "Invoice Run:Read-Only",
        "programURL": "/Business/iCABSBInvoiceRunDatesGrid.htm<readonly>",
        "routeURL": "/billtocash/rundatesgrid/view",
        "visibility": false,
        "queryParams": {"readonly": "true"}
      }, {
        "modulename": "Single Invoice Run",
        "programURL": "/Business/iCABSBInvoiceRunDatesGrid.htm<Single>",
        "routeURL": "/billtocash/rundatesgrid",
        "queryParams": {"single": "true"},
        "visibility": false
      }, {
        "modulename": "Release For Invoicing",
        "programURL": "/Application/iCABSReleaseForInvoiceGrid.htm",
        "routeURL": "/billtocash/application/releaseforinvoiceGrid",
        "visibility": false
      }, {
        "modulename": "Retained Service Cover Acceptance",
        "programURL": "/Application/iCABSAServiceCoverAcceptGrid.htm",
        "routeURL": "/billtocash/servicecover/acceptGrid",
        "visibility": false
      }, {
        "modulename": "Delivery Confirmation",
        "programURL": "/Application/iCABSAServiceCoverUnsuspendGrid.htm",
        "routeURL": "/contractmanagement/serviceProcesses/deliveryConfirmation",
        "visibility": false
      }, {
        "modulename": "Invoice Changes",
        "programURL": "/Application/iCABSAInvoiceDetailsMaintenance.htm",
        "routeURL": "/billtocash/maintenance/invoiceDetailsMaintainance",
        "visibility": false
      }, {
        "modulename": "Credit & Re-invoice",
        "programURL": "/Application/iCABSACreditAndReInvoiceMaintenance.htm",
        "routeURL": "/billtocash/postInvoiceManagement/creditAndReInvoiceMaintenance",
        "visibility": false
      }, {
        "modulename": "Invoice Text Maintenance",
        "programURL": "/Application/iCABSAInvoicePrintMaintenance.htm",
        "routeURL": "application/invoice/print/maintenance",
        "visibility": false
      }, {
        "modulename": "Invoice & Credit Note History",
        "programURL": "/Application/iCABSAInvoiceHeaderGrid.htm",
        "routeURL": "grid/application/invoiceheadergridcomponent",
        "visibility": false
      }, {
        "modulename": "Invoices By Account",
        "programURL": "/Application/iCABSAInvoiceByAccountGrid.htm",
        "routeURL": "grid/application/nav/invoice/account",
        "visibility": false
      }]
    }, {
      "featurename": "API",
      "visibility": false,
      "module": [{
        "modulename": "Preview API",
        "programURL": "/Application/iCABSAApplyAPIGrid.htm",
        "routeURL": "/billtocash/apigrid",
        "visibility": false
      }, {
        "modulename": "Apply API",
        "programURL": "/Application/iCABSAApplyAPIGeneration.htm",
        "routeURL": "/billtocash/apigeneration",
        "visibility": false
      }, {
        "modulename": "API Code / Rate",
        "programURL": "/Business/iCABSBAPICodeMaintenance.htm",
        "routeURL": "/billtocash/apicodemaintenance",
        "visibility": false
      }, {
        "modulename": "Exempt A Contract",
        "programURL": "/Application/iCABSAContractAPIMaintenance.htm",
        "routeURL": "/billtocash/contract/apiexempt",
        "visibility": false
      }, {
        "modulename": "API Date Change",
        "programURL": "/Application/iCABSAAPIDateMaintenance.htm",
        "routeURL": "/billtocash/apidate",
        "visibility": false
      }, {
        "modulename": "Service Cover API Update",
        "programURL": "/Application/iCABSAServiceCoverAPIGrid.htm",
        "routeURL": "/billtocash/serviceCoverApiGrid",
        "visibility": false
      }, {
        "modulename": "Reverse API Contract",
        "programURL": "/Application/iCABSAAPIReverse.htm<Contract>",
        "routeURL": "/billtocash/contract/apireverse",
        "visibility": false
      }, {
        "modulename": "Reverse API Premises",
        "programURL": "/Application/iCABSAAPIReverse.htm<Premise>",
        "routeURL": "/billtocash/premise/apireverse",
        "visibility": false
      }, {
        "modulename": "Reverse API Service Cover",
        "programURL": "/Application/iCABSAAPIReverse.htm<Service>",
        "routeURL": "/billtocash/servicecover/apireverse",
        "visibility": false
      }]
    }, {
      "featurename": "Tools",
      "visibility": false,
      "module": [{
        "modulename": "Batch Process Monitor",
        "programURL": "/Model/riMBatchProcessMonitorSearch.htm",
        "routeURL": "/itfunctions/batchprocess/monitor",
        "visibility": false
      }, {
        "modulename": "Report Process Viewer",
        "programURL": "/Model/riMReportViewerSearch.htm",
        "routeURL": "/itfunctions/riMReportViewerSearch",
        "visibility": false
      }]
    }]
  }, {
    "domainname": "Sales",
    "id": "Sales",
    "alwaysdisplay": false,
    "visibility": false,
    "feature": [{
      "featurename": "Customer Contact",
      "visibility": false,
      "module": [{
        "modulename": "General Search",
        "programURL": "/ContactManagement/iCABSCMGeneralSearchGrid.htm",
        "routeURL": "/contractmanagement/generalsearchgrid",
        "visibility": false
      }, {
        "modulename": "Contact Centre Search",
        "programURL": "/ContactManagement/iCABSCMCallCentreGrid.htm",
        "routeURL": "/ccm/callcentersearch/",
        "visibility": false
      }, {
        "modulename": "Contact Centre Review",
        "programURL": "/ContactManagement/iCABSCMCallCentreReviewGrid.htm",
        "routeURL": "/ccm/centreReview",
        "visibility": false
      }, {
        "modulename": "Account Review",
        "programURL": "/ContactManagement/iCABSCMAccountReviewGrid.htm",
        "routeURL": "grid/application/nav/reportContacts/accountReviewGrid",
        "visibility": false
      }, {
        "modulename": "Work Order Review",
        "programURL": "/ContactManagement/iCABSCMWorkorderReviewGrid.htm",
        "routeURL": "/ccm/workOrderReviewGrid",
        "visibility": false
      }]
    }, {
      "featurename": "Telesales",
      "visibility": false,
      "module": [{
        "modulename": "Telesales Order Grid",
        "programURL": "/Application/iCABSATeleSalesOrderGrid.htm",
        "routeURL": "/ccm/customerContact/telesalesordergrid",
        "visibility": false
      }]
    }, {
      "featurename": "Prospects",
      "visibility": false,
      "module": [{
        "modulename": "Prospect Maintenance",
        "programURL": "/ContactManagement/iCABSCMPipelineProspectMaintenance.htm<Prospect>",
        "routeURL": "/prospecttocontract/maintenance/prospect",
        "visibility": false
      }, {
        "modulename": "Prospect Grid",
        "programURL": "/ContactManagement/iCABSCMProspectGrid.htm",
        "routeURL": "/prospecttocontract/prospectgrid",
        "visibility": false
      }, {
        "modulename": "Pipeline Prospect Grid",
        "programURL": "/Sales/iCABSSPipelineGrid.htm",
        "routeURL": "/prospecttocontract/SalesOrderProcessing/PipelineGrid",
        "visibility": false
      }, {
        "modulename": "Sales Order Prospects",
        "programURL": "/Sales/iCABSSSOProspectGrid.htm",
        "routeURL": "/grid/service/drilldown/prospectgrid",
        "visibility": false
      }, {
        "modulename": "Contract Approval By Business",
        "programURL": "/Sales/iCABSSApprovalBusiness.htm",
        "routeURL": "/grid/sales/approvalbusiness",
        "visibility": false
      },{
        "modulename": "Contract Approval By Branch",
        "programURL": "/Sales/iCABSSdlContractApprovalGrid.htm",
        "routeURL": "/grid/sales/contractapprovalgrid",
        "visibility": false
      }, {
        "modulename": "Diary",
        "programURL": "/ContactManagement/iCABSCMDiaryMaintenance.htm",
        "routeURL": "/prospecttocontract/maintenance/diary",
        "visibility": false
      }, {
        "modulename": "Diary Day",
        "programURL": "/ContactManagement/iCABSCMDiaryDayMaintenance.htm",
        "routeURL": "/prospecttocontract/maintenance/diarydaymaintaianance",
        "visibility": false
      }, {
        "modulename": "Key Account Job",
        "programURL": "/ContactManagement/iCABSCMProspectEntryMaintenance.htm<NatAxJob>",
        "routeURL": "/prospecttocontract/maintenance/prospectentry/nataxjob",
        "visibility": false
      }, {
        "modulename": "Confirm Key Account Job",
        "programURL": "/ContactManagement/iCABSCMProspectEntryGrid.htm<Confirm>",
        "routeURL": "/prospecttocontract/contactmanagement/prospectEntryGrid",
        "visibility": false
      }]
    }, {
      "featurename": "Sales Maintenance",
      "visibility": false,
      "module": [{
        "modulename": "Sales Area",
        "programURL": "/Business/iCABSBSalesAreaGrid.htm",
        "routeURL": "/contractmanagement/areas/salesAreaGrid",
        "visibility": false
      }, {
        "modulename": "Negotiating Employee Reassign",
        "programURL": "/Business/iCABSBContractSalesEmployeeReassignGrid.htm",
        "routeURL": "/contractmanagement/business/contractSalesEmployeeReassignGrid",
        "visibility": false
      }, {
        "modulename": "Portfolio Rezone (Sales)",
        "programURL": "/Business/iCABSBSalesAreaPostcodeRezoneGrid.htm",
        "routeURL": "contractmanagement/general/rezone/salesarea/postcode",
        "visibility": false
      }, {
        "modulename": "Postcode Rezone (Sales)",
        "programURL": "/Business/iCABSBSalesAreaRezoneGrid.htm",
        "routeURL": "contractmanagement/general/rezone/salesarea/grid",
        "visibility": false
      }]
    }, {
      "featurename": "Contract",
      "visibility": false,
      "module": [{
        "modulename": "Contract Details Report",
        "programURL": "/ApplicationReport/iCABSARBranchContractReport.htm",
        "routeURL": "/contractmanagement/reports/contractForBranchReport",
        "visibility": false
      }]
    }, {
      "featurename": "Imports & Notifications",
      "visibility": false,
      "module": [{
        "modulename": "Bulk SMS Messages: Business",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/ccm/sendbulksms/business",
        "visibility": false
      }, {
        "modulename": "Bulk SMS Messages: Branch",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/ccm/sendbulksms/branch",
        "visibility": false
      }, {
        "modulename": "Import Records",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/itfunctions/importing",
        "visibility": false
      }]
    }, {
      "featurename": "Tools",
      "visibility": false,
      "module": [{
        "modulename": "Batch Process Monitor",
        "programURL": "/Model/riMBatchProcessMonitorSearch.htm",
        "routeURL": "/itfunctions/batchprocess/monitor",
        "visibility": false
      }, {
        "modulename": "Report Process Viewer",
        "programURL": "/Model/riMReportViewerSearch.htm",
        "routeURL": "/itfunctions/riMReportViewerSearch",
        "visibility": false
      }]
    }]
  }, {
    "domainname": "Service",
    "id": "Service",
    "alwaysdisplay": false,
    "visibility": false,
    "feature": [{
      "featurename": "Customer Contact",
      "visibility": false,
      "module": [{
        "modulename": "General Search",
        "programURL": "/ContactManagement/iCABSCMGeneralSearchGrid.htm",
        "routeURL": "/contractmanagement/generalsearchgrid",
        "visibility": false
      }, {
        "modulename": "Contact Centre Search",
        "programURL": "/ContactManagement/iCABSCMCallCentreGrid.htm",
        "routeURL": "/ccm/callcentersearch/",
        "visibility": false
      }, {
        "modulename": "Contact Centre Review",
        "programURL": "/ContactManagement/iCABSCMCallCentreReviewGrid.htm",
        "routeURL": "/ccm/centreReview",
        "visibility": false
      }, {
        "modulename": "Callout Search",
        "programURL": "/ContactManagement/iCABSCMCustomerContactCalloutGrid.htm",
        "routeURL": "/ccm/customercontact/callout/grid",
        "visibility": false
      }]
    }, {
      "featurename": "Service Maintenance",
      "visibility": false,
      "module": [{
        "modulename": "Service Area",
        "programURL": "/Business/iCABSBBranchServiceAreaGrid.htm",
        "routeURL": "/servicedelivery/branch/serviceArea",
        "visibility": false
      }, {
        "modulename": "Service Verification",
        "programURL": "/Service/iCABSSeAwaitingVerificationGrid.htm",
        "routeURL": "/serviceplanning/Service/verification",
        "visibility": false
      }, {
        "modulename": "Portfolio Rezone (Service)",
        "programURL": "/Business/iCABSBPremisePostcodeRezoneGrid.htm",
        "routeURL": "/grid/application/nav/premisePostcodeRezoneGrid",
        "visibility": false
      }, {
        "modulename": "Postcode Rezone (Service)",
        "programURL": "/Business/iCABSBServiceAreaRezoneGrid.htm",
        "routeURL": "/grid/servicearea/rezone",
        "visibility": false
      }, {
        "modulename": "Postcodes Allocated To Areas",
        "programURL": "/Business/iCABSBPostcodesGrid.htm",
        "routeURL": "/contractmanagement/areas/branchgeography/postcodesgrid",
        "visibility": false
      }, {
        "modulename": "Area Detail",
        "programURL": "/Service/iCABSSeServiceAreaDetailGrid.htm",
        "routeURL": "/grid/service/drilldown/serviceplanning/serviceareadetail",
        "visibility": false
      }, {
        "modulename": "Visit Anniversary Date Change",
        "programURL": "/Application/iCABSAContractAnniversaryChange.htm",
        "routeURL": "/contractmanagement/contractadmin/contractAnniversaryChange",
        "visibility": false
      }, {
        "modulename": "Suspend Premises Service",
        "programURL": "/Application/iCABSAPremiseServiceSuspendMaintenance.htm",
        "routeURL": "/contractmanagement/premisesmaintenance/servicesuspendmaintenance",
        "visibility": false
      }, {
        "modulename": "Suspend Product Service",
        "programURL": "/Application/iCABSAServiceCoverServiceSuspendMaintenance.htm",
        "routeURL": "/application/maintenance/servicecover/service/suspend",
        "visibility": false
      }, {
        "modulename": "Multi-Premises Special Instructions Change",
        "programURL": "/Application/iCABSAMultiPremiseSpecial.htm",
        "routeURL": "/contractmanagement/premisesadmin/application/multiPremisesSpecial",
        "visibility": false
      }]
    }, {
      "featurename": "Calendars",
      "visibility": false,
      "module": [{
        "modulename": "Apply Annual Calendar",
        "programURL": "/Application/iCABSAServiceCoverCalendarDatesMaintenance.htm",
        "routeURL": "sales/ServiceCoverCalendarDatesMaintenance",
        "visibility": false
      }, {
        "modulename": "Annual Calendar Summary",
        "programURL": "/Application/iCABSAServiceCoverCalendarDatesMaintenanceGrid.htm",
        "routeURL": "/serviceplanning/calendarandSeasons/serviceCoverCalendarDateMaintenanceGrid",
        "visibility": false
      }, {
        "modulename": "Annual Calendar Template",
        "programURL": "/Application/iCABSACalendarTemplateMaintenance.htm",
        "routeURL": "/application/calendarTemplateMaintenance",
        "visibility": false
      }, {
        "modulename": "Annual Calendar Template Access",
        "programURL": "/Application/iCABSACalendarTemplateBranchAccessGrid.htm",
        "routeURL": "/grid/application/nav/calendarTemplateBranchAccessGrid",
        "visibility": false
      }, {
        "modulename": "Annual Calendar Template Use",
        "programURL": "/Application/iCABSACalendarServiceGrid.htm<TemplateUse>",
        "routeURL": "/serviceplanning/Templates/CalendarTemplateUse",
        "visibility": false
      }, {
        "modulename": "Annual Calendar Template Change History",
        "programURL": "/Application/iCABSACalendarHistoryGrid.htm",
        "routeURL": "/serviceplanning/application/calenderHistoryGrid",
        "visibility": false
      }, {
        "modulename": "Closed Calendar Template",
        "programURL": "/Application/iCABSAClosedTemplateMaintenance.htm",
        "routeURL": "/serviceplanning/application/closedtemplatemaintenance",
        "visibility": false
      }, {
        "modulename": "Closed Calendar Template Access",
        "programURL": "/Application/iCABSAClosedTemplateBranchAccessGrid.htm",
        "routeURL": "/grid/service/drilldown/ClosedTemplateBranchAccessGrid",
        "visibility": false
      }, {
        "modulename": "Closed Calendar Template Use",
        "programURL": "/Application/iCABSAClosedServiceGrid.htm<TemplateUse>",
        "routeURL": "/serviceplanning/Templates/HolidayClosedTemplateUse",
        "visibility": false,
        "queryParams": {"noroutegaurd": true}
      }, {
        "modulename": "Apply Seasonal Service",
        "programURL": "/Application/iCABSAServiceCoverSeasonalDatesMaintenance.htm",
        "routeURL": "/application/servicecover/seasonaldatesmaintenance",
        "visibility": false
      }, {
        "modulename": "Seasonal Template",
        "programURL": "/Application/iCABSASeasonalTemplateMaintenance.htm",
        "routeURL": "application/seasonal/templatemaintenance",
        "visibility": false
      }, {
        "modulename": "Seasonal Template Details",
        "programURL": "/Application/iCABSASeasonalTemplateDetailGrid.htm",
        "routeURL": "/grid/service/drilldown/seasonaltemplatedetailgrid",
        "visibility": false
      }]
    }, {
      "featurename": "Planning",
      "visibility": false,
      "module": [{
        "modulename": "Create Service Plan",
        "programURL": "/Service/iCABSSeServicePlanningGrid.htm",
        "routeURL": "/grid/service/drilldown/planning/grid",
        "visibility": false,
        "quickLinks": true,
        "icon": "glyphicon-calendar"
      }, {
        "modulename": "Create Service Plan (Hygiene)",
        "programURL": "/Service/iCABSSeServicePlanningGridHg.htm",
        "routeURL": "/serviceplanning/serviceplanninggridhg",
        "visibility": false
      }, {
        "modulename": "Plan Visit Maintenance",
        "programURL": "/Service/iCABSSePlanVisitGrid.htm",
        "routeURL": "grid/application/visitmaintenance/planvisitGrid",
        "visibility": false
      }, {
        "modulename": "Service Planning Maintenance",
        "programURL": "/Service/iCABSSeServicePlanningMaintenance.htm",
        "routeURL": "application/maintenance/servicePlanningMaintenance",
        "visibility": false
      }, {
        "modulename": "Planning Diary",
        "programURL": "/Service/iCABSSePlanningDiary.htm",
        "routeURL": "serviceplanning/planning/diary",
        "visibility": false
      }, {
      }, {
        "modulename": "Service+ Planning",
        "programURL": "/Service/iCABSSeServicePlanningExportEntry.htm",
        "routeURL": "serviceplanning/exportentry",
        "visibility": false
      }, {
        "modulename": "Confirmed Plans",
        "programURL": "/Service/iCABSSeServicePlanGrid.htm",
        "routeURL": "/grid/sales/serviceplan/grid",
        "visibility": false
      }, {
        "modulename": "Area Reallocation",
        "programURL": "/Service/iCABSSeAreaReallocationGrid.htm",
        "routeURL": "serviceplanning/area/reallocation",
        "visibility": false
      }, {
        "modulename": "Service Area Allocation",
        "programURL": "/Business/iCABSBServiceAreaAllocationGrid.htm",
        "routeURL": "/servicedelivery/servicearea/allocation",
        "visibility": false
      }, {
        "modulename": "Area Sequence",
        "programURL": "/Service/iCABSSeServiceAreaSequenceGrid.htm",
        "routeURL": "/grid/sales/servicearea/sequence",
        "visibility": false
      }, {
        "modulename": "Area Sequence By Postcode",
        "programURL": "/Service/iCABSSeServiceAreaPostcodeSequenceGrid.htm",
        "routeURL": "/serviceplanning/servicearea/postcode/sequence",
        "visibility": false
      }, {
        "modulename": "Service Planning Re-Sequencing",
        "programURL": "/Service/iCABSSeServiceAreaReSequencing.htm",
        "routeURL": "/serviceplanning/servicearea/resequence",
        "visibility": false
      }, {
        "modulename": "Summary Workload",
        "programURL": "/Service/iCABSSESummaryWorkloadGridMonthBranch.htm",
        "routeURL": "/grid/summary/workload",
        "visibility": false
      }, {
        "modulename": "Summary Workload Reroute",
        "programURL": "/Service/iCABSSESummaryWorkloadGridMonthBranch.htm<reroute>",
        "routeURL": "/grid/summary/workload/reroute",
        "queryParams": {"reroute": "true"},
        "visibility": false
      }, {
        "modulename": "Despatch Grid",
        "programURL": "/Service/iCABSSeDespatchGrid.htm",
        "routeURL": "/grid/application/despatchgrid",
        "queryParams": {"CurrentContractTypeURLParameter": "C"},
        "visibility": false
      }, {
        "modulename": "Deliveries Due",
        "programURL": "/Application/iCABSAProductSalesDeliveriesDueGrid.htm<product>",
        "routeURL": "/servicedelivery/reportsplanning/productsalesdeliverydue",
        "queryParams": {"currentContractType": "P"},
        "visibility": false
      }, {
        "modulename": "Stock Requirements",
        "programURL": "/ApplicationReport/iCABSARIWSStockRequirements.htm",
        "routeURL": "/bireports/service/stock/requirements",
        "visibility": false
      }, {
        "modulename": "Service Cover Visit Mismatch",
        "programURL": "/Service/iCABSSeVisitDateDiscrepancyGrid.htm",
        "routeURL":   "/grid/service/drilldown/serviceplanning/visitdatemismatch",
        "visibility": false
      }]
    }, {
      "featurename": "Paperwork Generation",
      "visibility": false,
      "module": [{
        "modulename": "Work List",
        "programURL": "/Service/iCABSWorkListConfirm.htm",
        "routeURL": "/servicedelivery/service/worklist",
        "visibility": false
      }, {
        "modulename": "Worklist By Date Range",
        "programURL": "/Service/iCABSSeServiceWorkListDateGrid.htm",
        "routeURL": "/grid/service/nav/service/worklist/dategrid",
        "visibility": false
      }, {
        "modulename": "Install & Removal Receipts",
        "programURL": "/Sales/iCABSSInstallReceipt.htm",
        "routeURL": "servicedelivery/installReceipt",
        "visibility": false
      }, {
        "modulename": "Service Listing / Receipts",
        "programURL": "/Service/iCABSSeServicePlanDeliveryNoteGrid.htm",
        "routeURL": "/servicedelivery/paperwork/servicelisting",
        "visibility": false,
        "quickLinks": true,
        "icon": "glyphicon-folder-open"
      }, {
        "modulename": "Single Service Receipt",
        "programURL": "/Service/iCABSSeServicePlanDeliveryNotebyProduct.htm",
        "routeURL": "/servicedelivery/paperwork/singleServiceReceipt",
        "visibility": false
      }, {
        "modulename": "Service Planning List",
        "programURL": "/Service/iCABSSeServicePlanningListEntry.htm",
        "routeURL": "/grid/service/internal/maintenance/serviceplanninglistentry",
        "visibility": false
      }, {
        "modulename": "Van Loading Report",
        "programURL": "/ApplicationReport/iCABSARVanLoadingReport.htm",
        "routeURL": "/servicedelivery/report/vanloading",
        "visibility": false
      }, {
        "modulename": "Waste Consignment Note Generation",
        "programURL": "/Service/iCABSSeWasteConsignmentNoteGenerate.htm",
        "routeURL": "/servicedelivery/wasteconsignmentnote",
        "visibility": false
      }, {
        "modulename": "Blank Consignment Notes",
        "programURL": "/Service/iCABSSeBlankConsignmentNotePrint.htm",
        "routeURL": "/servicedelivery/blankconsignmentnoteprint",
        "visibility": false
      }]
    }, {
      "featurename": "Service Monitoring",
      "visibility": false,
      "module": [{
        "modulename": "Tech Work Grid",
        "programURL": "/Service/iCABSSeTechnicianWorkSummaryGrid.htm",
        "routeURL": "/servicedelivery/techWorkGridService/TechnicianWorkSummaryGrid",
        "visibility": false
      }, {
        "modulename": "Tech Work Summary Report",
        "programURL": "/ApplicationReport/iCABSARTechnicianWorkSummaryReport.htm",
        "routeURL": "/bireports/pda/technicianwork/summary",
        "visibility": false
      }, {
        "modulename": "Branch Service Monitor",
        "programURL": "/Service/iCABSSeDebriefBranchGrid.htm",
        "routeURL": "/servicedelivery/debriefbranchgrid",
        "visibility": false,
        "quickLinks": true,
        "icon": "glyphicon-list-alt"
      }, {
        "modulename": "Employee Service Monitor",
        "programURL": "/Service/iCABSSeDebriefEmployeeGrid.htm",
        "routeURL": "/servicedelivery/debrief/employee",
        "visibility": false
      }, {
        "modulename": "Outstanding Tasks",
        "programURL": "/Service/iCABSSeDebriefOutstandingGrid.htm",
        "routeURL": "/grid/service/debrief/outstandinggrid",
        "visibility": false
      }, {
        "modulename": "Debrief",
        "programURL": "/Service/iCABSSeDebriefSummaryGrid.htm",
        "routeURL": "/grid/service/debrief/summary",
        "visibility": false
      }, {
        "modulename": "Visit Rejections",
        "programURL": "/Service/iCABSSeServiceVisitRejectionsGrid.htm",
        "routeURL": "/servicedelivery/visit/rejections",
        "visibility": false,
        "quickLinks": true,
        "icon": "glyphicon-warning-sign"
      }, {
        "modulename": "Visit Rejections (All Branches)",
        "programURL": "/Service/iCABSSeServiceVisitRejectionsGrid.htm<AllBranches>",
        "routeURL": "/servicedelivery/visit/rejections/Allbranches",
        "queryParams": {"Allbranches": "true"},
        "visibility": false
      }, {
        "modulename": "Tech Sync Summary",
        "programURL": "/Service/iCABSSeTechnicianSyncSummaryGrid.htm",
        "routeURL": "/servicedelivery/pdareturns/techniciansyncsummary",
        "visibility": false
      }, {
        "modulename": "Unprocessed PDA Sync Visit (PC)",
        "programURL": "/Service/iCABSSePESVisitGrid.htm<Business>",
        "routeURL": "/servicedelivery/pdareturns/SePESVisitGrid",
        "queryParams": {"Business": "true"},
        "visibility": false
      }]
    }, {
      "featurename": "Mobile Service Updates",
      "visibility": false,
      "module": [{
        "modulename": "Customer Contact Update",
        "programURL": "/Service/iCABSSePremiseContactChangeGrid.htm",
        "routeURL": "/contractmanagement/premisesadmin/PDAReturns/premiseContactChange",
        "visibility": false
      }, {
        "modulename": "Customer Data Update",
        "programURL": "/Service/iCABSSeDataChangeGrid.htm",
        "routeURL": "/contractmanagement/customerinfo/customerdataupdate/datachangegrid",
        "visibility": false
        }, {
        "modulename": "New Locations",
        "programURL": "/Service/iCABSSeHCANewLocationGrid.htm",
        "routeURL": "/grid/service/drilldown/hca/newlocation",
        "visibility": false
      }, {
        "modulename": "Risk Assessment",
        "programURL": "/Service/iCABSSeHCARiskAssessmentGrid.htm",
        "routeURL": "/servicedelivery/riskassessmentgrid",
        "visibility": false
      }, {
        "modulename": "Special Instructions",
        "programURL": "/Service/iCABSSeHCASpecialInstructionsGrid.htm",
        "routeURL": "/grid/service/nav/hca/specialinstructions",
        "visibility": false
      }, {
        "modulename": "Area Rezone Rejections",
        "programURL": "/Service/iCABSSeServiceAreaRezoneRejectionsGrid.htm",
        "routeURL": "/grid/service/nav/area/rezone/rejectiongrid",
        "visibility": false
      }]
    }, {
      "featurename": "Visits",
      "visibility": false,
      "module": [{
        "modulename": "Shared Visit Release",
        "programURL": "/Service/iCABSSeServiceVisitReleaseGrid.htm",
        "routeURL": "/billtocash/shared/visitreleasegrid",
        "visibility": false
      }, {
        "modulename": "Visit Rejections",
        "programURL": "/Service/iCABSSeServiceVisitRejectionsGrid.htm",
        "routeURL": "/servicedelivery/visit/rejections",
        "visibility": false
      }, {
        "modulename": "Visit Rejections (All Branches)",
        "programURL": "/Service/iCABSSeServiceVisitRejectionsGrid.htm",
        "routeURL": "/servicedelivery/visit/rejections/Allbranches",
        "queryParams": {"Allbranches": "true"},
        "visibility": false
      }, {
        "modulename": "Visit Maintenance",
        "programURL": "/Service/iCABSSeServiceVisitEntryGrid.htm",
        "routeURL": "/grid/service/nav/visitentry",
        "visibility": false,
        "quickLinks": true,
        "icon": "glyphicon-wrench"
      }, {
        "modulename": "Group Visit Maintenance",
        "programURL": "/Service/iCABSSeGroupServiceVisitEntryGrid.htm",
        "routeURL": "/servicedelivery/groupvisit/serviceentrygrid",
        "visibility": false
      }, {
        "modulename": "Entitlement Visit Maintenance",
        "programURL": "/Service/iCABSSeServiceVisitEntitlementEntryGrid.htm",
        "routeURL": "/grid/service/nav/visitentitlemententry",
        "visibility": false
      }, {
        "modulename": "Service Docket Data Entry",
        "programURL": "/Service/iCABSSeServiceDocketDataEntry.htm",
        "routeURL": "/servicedelivery/service/visit/maintenance/docketentry",
        "visibility": false
      }, {
        "modulename": "Service Work List Entry",
        "programURL": "/Service/iCABSSePDAWorkListEntryGrid.htm",
        "routeURL": "/servicedelivery/service/pda/worklistentry",
        "visibility": false
      }, {
        "modulename": "Non-Returned Paperwork Audit",
        "programURL": "/ApplicationReport/iCABSARReturnedPaperWorkGrid.htm",
        "routeURL": "/servicedelivery/lettersandlabels/returnedpaperworkgrid",
        "visibility": false
      }, {
        "modulename": "Manual Waste Consignment Note Entry",
        "programURL": "/Service/iCABSSeManualWasteConsignmentNote.htm",
        "routeURL": "/maintenance/wasteconsignment/manual",
        "visibility": false
      },{
        "modulename": "Unreturned Waste Consignment Notes",
        "programURL": "/Service/iCABSSEUnreturnedConsignmentNotesGrid.htm",
        "routeURL": "/maintenance/unreturnedconsignment/notes",
        "visibility": false
      },{
        "modulename": "Void Waste Consignment Note",
        "programURL": "/Service/iCABSSeManualWasteConsignmentNote.htm",
        "routeURL": "/maintenance/wasteconsignment/void",
        "queryParams": {"mode": "void"},
        "visibility": false
      },{
        "modulename": "Despatch Grid",
        "programURL": "/Service/iCABSSeDespatchGrid.htm",
        "routeURL": "/grid/application/despatchgrid",
        "queryParams": {"CurrentContractTypeURLParameter": "C"},
        "visibility": false
      }, {
        "modulename": "Service Activity",
        "programURL": "/Service/iCABSSeServiceActivityUpdateGrid.htm",
        "routeURL": "/grid/service/nav/visit/activity",
        "visibility": false
      }]
    }, {
      "featurename": "Service Delivery",
      "visibility": false,
      "module": [{
        "modulename": "Adjust Productivity",
        "programURL": "/Service/iCABSSeProductivityAdjustmentMaintenance.htm",
        "routeURL": "/service/productivity/adjust/maintenance",
        "visibility": false
      }, {
        "modulename": "Customer Signature Summary: Business",
        "programURL": "/Service/iCABSSeCustomerSignatureSummary.htm<Business>",
        "routeURL": "grid/service/nav/customerSignatureSummary/business",
        "visibility": false
      }, {
        "modulename": "Customer Signature Summary: Region",
        "programURL": "/Service/iCABSSeCustomerSignatureSummary.htm<Region>",
        "routeURL": "grid/service/nav/customerSignatureSummary/region",
        "visibility": false
      }, {
        "modulename": "Customer Signature Summary: Branch",
        "programURL": "/Service/iCABSSeCustomerSignatureSummary.htm<Branch>",
        "routeURL": "grid/service/nav/customerSignatureSummary/branch",
        "visibility": false
      }, {
        "modulename": "Follow Up Calls",
        "programURL": "/Service/iCABSSeFollowUpGrid.htm",
        "routeURL": "grid/service/followupcalls",
        "visibility": false
      }, {
        "modulename": "Service Call Type: Business",
        "programURL": "/Service/iCABSServiceCallTypeGrid.htm<Business>",
        "routeURL": "servicedelivery/businessservicecalltype",
        "queryParams": {"mode": "Business"},
        "visibility": false
      }, {
        "modulename": "Service Call Type: Region",
        "programURL": "/Service/iCABSServiceCallTypeGrid.htm<Region>",
        "routeURL": "servicedelivery/regionservicecalltype",
        "queryParams": {"mode": "Region"},
        "visibility": false
      }, {
        "modulename": "Service Call Type: Branch",
        "programURL": "/Service/iCABSServiceCallTypeGrid.htm<Branch>",
        "routeURL": "servicedelivery/branchservicecalltype",
         "queryParams": {"mode": "Branch"},
        "visibility": false
      }]
    }, {
      "featurename": "Waste",
      "visibility": false,
      "module": [{
        "modulename": "Daily Prenotification Report",
        "programURL": "/ApplicationReport/iCABSARDailyPrenotificationReport.htm",
        "routeURL": "/servicedelivery/dailyprenotificationreport",
        "visibility": false
      }, {
        "modulename": "Environment Agency Business Waste Generation Report",
        "programURL": "/ApplicationReport/iCABSAREnvAgencyBusinessWaste.htm",
        "routeURL": "/servicedelivery/wasteconsignment/envagencybusinesswaste",
        "visibility": false
      }, {
        "modulename": "Environment Agency Exceptions",
        "programURL": "/ApplicationReport/iCABSAREnvAgencyExceptions.htm",
        "routeURL": "/servicedelivery/wasteconsignment/envagencyexceptions",
        "visibility": false
      }, {
        "modulename": "Environment Agency Quarterly Returns",
        "programURL": "/ApplicationReport/iCABSAREnvAgencyQuarterlyReturn.htm",
        "routeURL": "/servicedelivery/envagencyquarterly/envagencyquarterlyreturn",
        "visibility": false
      }, {
        "modulename": "Annual Prenotification Report",
        "programURL": "/ApplicationReport/iCABSARPrenotificationReport.htm",
        "routeURL": "/servicedelivery/reports/prenotificationreport",
        "visibility": false
      }, {
        "modulename": "Waste Transfer Notes",
        "programURL": "/ApplicationReport/iCABSARWasteTransferNotesPrint.htm",
        "routeURL": "/servicedelivery/lettersandlabels/wastetransfernotes",
        "visibility": false
      }]
    }, {
      "featurename": "Imports & Notifications",
      "visibility": false,
      "module": [{
        "modulename": "Bulk SMS Messages: Business",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/ccm/sendbulksms/business",
        "visibility": false
      }, {
        "modulename": "Bulk SMS Messages: Branch",
        "programURL": "/ContactManagement/iCABSBulkSMSMessageMaintenance.htm<Branch>",
        "routeURL": "/ccm/sendbulksms/branch",
        "visibility": false
      }]
    }, {
      "featurename": "Tools",
      "visibility": false,
      "module": [{
        "modulename": "Batch Process Monitor",
        "programURL": "/Model/riMBatchProcessMonitorSearch.htm",
        "routeURL": "/itfunctions/batchprocess/monitor",
        "visibility": false
      }, {
        "modulename": "Report Process Viewer",
        "programURL": "/Model/riMReportViewerSearch.htm",
        "routeURL": "/itfunctions/riMReportViewerSearch",
        "visibility": false
      }]
    }]
  },
  {
      "domainname": "Reporting & Dashboards",
      "id": "Dashboard",
      "visibility": false,
      "feature": [
        {
          "featurename": "Dashboards",
          "visibility": false,
          "hidein": "",
          "module": [
            {
              "modulename": "Account Management",
                  "programURL": "extensions/AccountManagement/index.html#am",
                  "routeURL": "extensions/AccountManagement/index.html#am",
                  "visibility": false,
                  "external": true,
                  "QAP": true,
                  "alwaysdisplay": true
            },
            {
              "modulename": "Contract/Jobs Released for Invoicing",
              "programURL": "extensions/jobs-released-for-invoicing/index.html#sheet1",
              "routeURL": "extensions/jobs-released-for-invoicing/index.html#sheet1",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Contracts/Jobs Due to Expire",
              "programURL": "extensions/cons-jobs-to-expire/index.html#numbersandvalues",
              "routeURL": "extensions/cons-jobs-to-expire/index.html#numbersandvalues",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "DSP",
              "programURL": "extensions/dsp/index.html#key-ratio-analysis",
              "routeURL": "extensions/dsp/index.html#key-ratio-analysis",
              "visibility": false,
              "hidein": "",
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "DSP UK Wash",
              "programURL": "extensions/dsp-ukws/index.html#key-ratio-analysis",
              "routeURL": "extensions/dsp-ukws/index.html#key-ratio-analysis",
              "visibility": false,
              "hidein": "",
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Deferred Turnover",
              "programURL": "extensions/deferred-turnover/index.html#filters",
              "routeURL": "extensions/deferred-turnover/index.html#filters",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "HWCN",
              "programURL": "extensions/hwcn/index.html#dashboard",
              "routeURL": "extensions/hwcn/index.html#dashboard",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "King+",
              "programURL": "extensions/king-plus/index.html#filters",
              "routeURL": "extensions/king-plus/index.html#filters",
              "visibility": false,
              "hidein": "|PROD|",
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Lost Business",
              "programURL": "extensions/lostbusiness/index.html#summary",
              "routeURL": "extensions/lostbusiness/index.html#summary",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "National Accounts - Contracts",
              "programURL": "extensions/national-accounts/index.html#sheet1",
              "routeURL": "extensions/national-accounts/index.html#sheet1",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Operational",
              "programURL": "extensions/operational/index.html#filters",
              "routeURL": "extensions/operational/index.html#filters",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Operational(IWS)",
              "programURL": "extensions/operationalIWS/index.html#howto",
              "routeURL": "extensions/operationalIWS/index.html#howto",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Portfolio By Negotiating Branch",
              "programURL": "extensions/portfolio-neg-branch/index.html#filters",
              "routeURL": "extensions/portfolio-neg-branch/index.html#filters",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Portfolio/Portfolio Arrears",
              "programURL": "extensions/portfolio/index.html#sheet1",
              "routeURL": "extensions/portfolio/index.html#sheet1",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Profitability RSA",
              "programURL": "extensions/profit-model-rsa/index.html#general",
              "routeURL": "extensions/profit-model-rsa/index.html#general",
              "visibility": false,
              "hidein": "",
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Profitability(Hygiene)",
              "programURL": "extensions/hyg-profit/index.html",
              "routeURL": "extensions/hyg-profit/index.html",
              "visibility": false,
              "hidein": "",
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Profitability(Pest)",
              "programURL": "extensions/profitability/index.html",
              "routeURL": "extensions/profitability/index.html",
              "visibility": false,
              "hidein": "",
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Prospect UK (Pest/IWS)",
              "programURL": "sso/extensions/ISSIWSProspectUK/index.html#main",
              "routeURL": "extensions/ISSIWSProspectUK/index.html#main",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Sales Statistics/Portfolio Movement",
              "programURL": "extensions/sales-statistics/index.html#sheet1",
              "routeURL": "extensions/sales-statistics/index.html#sheet1",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Service Suspend",
              "programURL": "extensions/service-suspend/index.html#filters",
              "routeURL": "extensions/service-suspend/index.html#filters",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Service+",
              "programURL": "extensions/service-plus/index.html#sheet1",
              "routeURL": "extensions/service-plus/index.html#sheet1",
              "visibility": false,
              "hidein": "|PROD|",
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Servicing/Productivity",
              "programURL": "extensions/state-of-service/index.html#sheet1",
              "routeURL": "extensions/state-of-service/index.html#sheet1",
              "visibility": false,
              "hidein": "|PROD|",
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Solo",
              "programURL": "extensions/SoloSales/index.html#sales",
              "routeURL": "extensions/SoloSales/index.html#sales",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Speed Reporter",
              "programURL": "extensions/speed/index.html#speed-reporter",
              "routeURL": "extensions/speed/index.html#speed-reporter",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "TPA",
              "programURL": "extensions/tpa/index.html#master",
              "routeURL": "extensions/tpa/index.html#master",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Ticket Analysis",
              "programURL": "extensions/ticket-analysis/index.html#sheet1",
              "routeURL": "extensions/ticket-analysis/index.html#sheet1",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Turnover",
              "programURL": "extensions/turnover/index.html#turnover",
              "routeURL": "extensions/turnover/index.html#turnover",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Vehicle Check",
              "programURL": "extensions/vehiclecheck/index.html#vc_summary",
              "routeURL": "extensions/vehiclecheck/index.html#vc_summary",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            },
            {
              "modulename": "Work Time",
              "programURL": "extensions/worktime/index.html#worktime_dashboard",
              "routeURL": "extensions/worktime/index.html#worktime_dashboard",
              "visibility": false,
              "external": true,
              "QAP": true,
              "alwaysdisplay": true
            }
          ]
        },
        {
          "featurename": "Sales",
          "visibility": false,
          "hidein": "",
          "module": [
        {
          "modulename": "Leads Report by Business",
              "programURL": "/ContactManagement/iCABSCMLeadsSubmittedBusiness.htm",
              "routeURL": "/pda-lead/leads/submittedbusiness",
              "queryParams": {"type": "business"},
              "visibility": false
        },
        {
          "modulename": "Leads Report by Region",
              "programURL": "/ContactManagement/iCABSCMLeadsSubmittedRegion.htm",
              "routeURL": "/pda-lead/leads/submittedregion",
              "queryParams": {"type": "region"},
              "visibility": false
        },
        {
          "modulename": "Leads Report by Branch",
              "programURL": "/ContactManagement/iCABSCMLeadsSubmittedBranch.htm",
              "routeURL": "/pda-lead/leads/submittedbranch",
              "queryParams": {"type": "branch"},
              "visibility": false
        },
        {
          "modulename": "Sales Statistics by Business",
          "programURL": "/Sales/iCABSSSalesStatisticsBusiness.htm",
          "routeURL": "/bireports/sales/statistics/business",
          "queryParams": {"pageType": "business"},
          "visibility": false
        },
        {
          "modulename": "Sales Statistics by Region",
          "programURL": "/Sales/iCABSSSalesStatisticsRegion.htm",
          "routeURL": "/bireports/sales/statistics/region",
          "queryParams": {"pageType": "region"},
          "visibility": false
        },
        {
          "modulename": "Sales Statistics by Branch",
          "programURL": "/Sales/iCABSSSalesStatisticsBranch.htm",
          "routeURL": "/bireports/sales/statistics/branch",
          "queryParams": {"pageType": "branch"},
          "visibility": false
        },
        {
          "modulename": "Sales Statistics Details",
          "programURL": "/Sales/iCABSSStatisticsDetail.htm",
          "routeURL": "/bireports/sales/statistics/sales/detail",
          "visibility": false
        },
        {
          "modulename": "Job Credits by Business",
          "programURL": "/ApplicationReport/iCABSARJobCreditBusiness.htm",
          "routeURL": "/bireports/sales/jobcredit/business",
          "queryParams": {"pageType": "Business"},
          "visibility": false
        },
        {
          "modulename": "Job Credits by Region",
          "programURL": "/ApplicationReport/iCABSARJobCreditRegion.htm",
          "routeURL": "/bireports/sales/jobcredit/region",
          "queryParams": {"pageType": "Region"},
          "visibility": false
        },
        {
          "modulename": "Job Credits by Branch",
          "programURL": "/ApplicationReport/iCABSARJobCreditBranch.htm",
          "routeURL": "/bireports/sales/jobcredit/branch",
          "queryParams": {"pageType": "Branch"},
          "visibility": false
        },
        {
          "modulename": "Business Origin by Business",
          "programURL": "/ApplicationReport/iCABSAROriginOfBusinessBusinessGrid.htm",
          "routeURL": "/bireports/sales/originofbusiness/business",
          "queryParams": {"pageType": "Business"},
          "visibility": false
        },
        {
          "modulename": "Business Origin by Region",
          "programURL": "/ApplicationReport/iCABSAROriginOfBusinessRegionGrid.htm",
          "routeURL": "/bireports/sales/originofbusiness/region",
          "queryParams": {"pageType": "Region"},
          "visibility": false
        },
        {
          "modulename": "Business Origin by Branch",
          "programURL": "/ApplicationReport/iCABSAROriginOfBusinessBranchGrid.htm",
          "routeURL": "/bireports/sales/originofbusiness/branch",
          "queryParams": {"pageType": "Branch"},
          "visibility": false
        }
      ]
        },
        {
          "featurename": "Finance",
          "visibility": false,
          "module": [
            {
              "modulename": "Suspended Portfolio by Business",
              "programURL": "/Application/iCABSASuspendedPortfolioBusiness.htm",
              "routeURL": "bireports/portfolio/suspended/by/business",
              "visibility": false,
              "queryParams": {"pageType": "business"}
            },
            {
              "modulename": "Suspended Portfolio by Branch",
              "programURL": "/Application/iCABSASuspendedPortfolioBranch.htm",
              "routeURL": "bireports/portfolio/suspended",
              "visibility": false,
              "queryParams": {"pageType": "branch"}
            },
            {
              "modulename": "Invoice Analysis By Branch",
              "programURL": "/ApplicationReport/iCABSARInvoiceAnalysisGrid.htm",
              "routeURL": "bireports/invoice/analysis",
              "visibility": false,
              "hidein": ""
            },
            {
              "modulename": "Credit Analysis by Business",
              "programURL": "/ApplicationReport/iCABSARCreditReasonAnalysisBusiness.htm",
              "routeURL": "bireports/creditanalysis/reasonbusiness",
              "queryParams": {"type": "business"},
              "visibility": false,
              "hidein": ""
            },
            {
              "modulename": "Credit Analysis by Region",
              "programURL": "/ApplicationReport/iCABSARCreditReasonAnalysisRegion.htm",
              "routeURL": "bireports/creditanalysis/reasonregion",
              "queryParams": {"type": "region"},
              "visibility": false,
              "hidein": ""
            },
            {
              "modulename": "Credit Analysis by Branch",
              "programURL": "/ApplicationReport/iCABSARCreditReasonAnalysisBranch.htm",
              "routeURL": "bireports/creditanalysis/reasonanalysis",
              "queryParams": {"type": "branch"},
              "visibility": false,
              "hidein": ""
            },
            {
              "modulename": "Portfolio by Business",
              "programURL": "/ApplicationReport/iCABSARPortfolioReportBusiness.htm",
              "routeURL": "bireports/portfolio/business",
              "visibility": false
            },
            {
              "modulename": "Portfolio by Region",
              "programURL": "/ApplicationReport/iCABSARPortfolioReportRegion.htm",
              "routeURL": "bireports/portfolio/region",
              "queryParams": {"type": "region"},
              "visibility": false
            },
            {
              "modulename": "Portfolio by Branch",
              "programURL": "/ApplicationReport/iCABSARPortfolioReportBranch.htm",
              "routeURL": "bireports/portfolio/branch",
              "queryParams": {"type": "branch"},
              "visibility": false
           },
           {
            "modulename": "Portfolio Movement by Business",
            "programURL": "/Sales/iCABSSNettGainBusiness.htm",
            "routeURL": "bireports/portfolio/movement/nettgain/business",
            "visibility": false
          },
          {
            "modulename": "Portfolio Movement by Branch",
            "programURL": "/Sales/iCABSSNettGainBranch.htm",
            "routeURL": "bireports/portfolio/movement/nettgain/branch",
            "queryParams": {"type": "branch"},
            "visibility": false
          },
          {
            "modulename": "Turnover by Business",
            "programURL": "/ApplicationReport/iCABSARTurnoverBusiness.htm",
            "routeURL": "bireports/turnover/business",
            "queryParams": {"type": "Business"},
            "visibility": false
          },
          {
            "modulename": "Turnover by Branch",
            "programURL": "/ApplicationReport/iCABSARTurnoverBranch.htm",
            "routeURL": "bireports/turnover/branch",
            "queryParams": {"type": "Branch"},
            "visibility": false
          },
          {
            "modulename": "Deffered Turnover by Business",
            "programURL": "/Application/iCABSADeferredTurnoverBusinessGrid.htm",
            "routeURL": "bireports/turnover/deferred/turnover/business",
            "visibility": false
          },
          {
            "modulename": "Deffered Turnover by Branch",
            "programURL": "/Application/iCABSADeferredTurnoverBranchGrid.htm",
            "routeURL": "bireports/turnover/deferred/turnover/branch",
            "queryParams": {"type": "branch"},
            "visibility": false
          },
          {
            "modulename": "API Exempt Contracts",
            "programURL": "/Application/iCABSAContractsAPIExemptGrid.htm",
            "routeURL": "bireports/api/exempt",
            "queryParams": {"type": "branch"},
            "visibility": false
          },
          {
            "modulename": "Tax Code Report",
            "programURL": "/ApplicationReport/iCABSARTaxCodeReport.htm",
            "routeURL": "/bireports/invoice/taxcodereport",
            "visibility": false
          },
          {
            "modulename": "Tax Code Summary",
            "programURL": "/ApplicationReport/iCABSARTaxCodeSummary.htm",
            "routeURL": "/bireports/invoice/taxcodesummary",
            "visibility": false
          },
          {
            "modulename": "Financial Accounts Extracts AU/NZ",
            "programURL": "riBatch/financialaccounts.html#demo",
            "routeURL": "/riBatch/financialaccounts",
            "visibility": false,
            "alwaysdisplay": true,
            "queryParams": {"pageType": "Financial Accounts Extracts"}
          }
      ]
    },
    {
      "featurename": "Servicing",
      "visibility": false,
      "module": [
  {
    "modulename": "Outstanding Installations by Business",
    "programURL": "/Service/iCABSSeOutstandingInstallationsBusinessGrid.htm",
    "routeURL": "bireports/service/oustanding/installations/business",
    "visibility": false,
    "queryParams": {"pageType": "Business","mode" : "Installations"}
  },
  {
    "modulename": "Outstanding Installations by Branch",
    "programURL": "/Service/iCABSSeOutstandingInstallationsGrid.htm",
    "routeURL": "bireports/service/oustanding/installations/branch",
    "visibility": false,
    "queryParams": {"pageType": "Branch","mode" : "Installations"}
  },
  {
    "modulename": "Outstanding Removals by Business",
    "programURL": "/Service/iCABSSeOutstandingRemovalsBusinessGrid.htm",
    "routeURL": "bireports/service/oustanding/removals/business",
    "visibility": false,
    "queryParams": {"pageType": "Business","mode" : "Removals"}
  },
  {
    "modulename": "Outstanding Removals by Branch",
    "programURL": "/Service/iCABSSeOutstandingRemovalsGrid.htm",
    "routeURL": "bireports/service/oustanding/removals/branch",
    "visibility": false,
    "queryParams": {"pageType": "Branch","mode" : "Removals"}
  },
  {
    "modulename": "Suspended Service by Business",
    "programURL": "/ApplicationReport/iCABSARServiceAndInvoiceSuspendBusiness.htm",
    "routeURL": "bireports/service/suspended/by/business",
     "visibility": false,
     "queryParams": {"pageType": "Business"}
  },
  {
    "modulename": "Suspended Service by Region",
    "programURL": "/ApplicationReport/iCABSARServiceAndInvoiceSuspendRegion.htm",
    "routeURL": "bireports/service/suspended/by/region",
     "visibility": false,
      "queryParams": {"pageType": "Region"}
  },
  {
    "modulename": "Suspended Service by Branch",
    "programURL": "/ApplicationReport/iCABSARServiceandInvoiceSuspendGrid.htm",
    "routeURL": "bireports/service/suspended/by/branch",
     "visibility": false,
     "queryParams": {"pageType": "Branch"}
  },
  {
     "modulename": "Suspended Service History",
     "programURL": "/ApplicationReport/iCABSARServiceSuspendHistory.htm",
     "routeURL": "bireports/service/suspended/servicehistory",
     "visibility": false
  },
  {
    "modulename": "Daily Productivity by Business",
    "programURL": "/Service/iCABSSeDailyProductivityBusinessGrid.htm",
    "routeURL": "bireports/service/productivity/daily/business",
    "visibility": false,
    "queryParams": {"pageType": "business"}
  },
  {
    "modulename": "Daily Productivity by Branch",
    "programURL": "/Service/iCABSSeDailyProductivityBranchGrid.htm",
    "routeURL": "bireports/service/productivity/daily/branch",
    "visibility": false,
    "queryParams": {"pageType": "branch"}
  },
  {
    "modulename": "Daily Productivity by Employee",
    "programURL": "/Service/iCABSSeDailyProductivitySummaryGrid.htm",
    "routeURL": "bireports/service/productivity/daily/summary",
    "visibility": false,
    "queryParams": {"pageType": "summary"}
  },
  {
    "modulename": "Daily Productivity by Employee Detail",
    "programURL": "/Service/iCABSSeServiceDailyProductivityGrid.htm",
    "routeURL": "bireports/service/productivity/daily",
    "visibility": false
  },
  {
    "modulename": "Daily Productivity by Employee Detail (France)",
    "programURL": "/Service/iCABSSeServiceDailyProductivityGridFR.htm",
    "routeURL": "bireports/service/productivity/daily/fr",
    "visibility": false,
    "queryParams": {"enablePerHour": true}
  },
  {
    "modulename": "Stock Usage Estimates by Business",
    "programURL": "/Service/iCABSSeStockUsageEstimatesBusiness.htm",
    "routeURL": "bireports/stockusage/business",
    "queryParams": {"pageType": "Business" },
    "visibility": false
  },
  {
    "modulename": "Stock Usage Estimates by Region",
    "programURL": "/Service/iCABSSeStockUsageEstimatesRegion.htm",
    "routeURL": "bireports/stockusage/region",
    "queryParams": {"pageType": "Region" },
    "visibility": false
  },
  {
    "modulename": "Stock Usage Estimates by Branch",
    "programURL": "/Service/iCABSSeStockUsageEstimatesBranch.htm",
    "routeURL": "bireports/stockusage/branch",
    "visibility": false,
    "queryParams": {"pageType": "Branch"}
  },
  {
    "modulename": "Stock Usage Estimates by Premises",
    "programURL": "/Service/iCABSSeStockUsageEstimatesPremises.htm",
    "routeURL": "bireports/stockusage/premises",
    "visibility": false,
    "queryParams": {"pageType": "Premises"}
  },
  {
    "modulename": "Technician Service Visits",
    "programURL": "/Service/iCABSTechnicianServiceVisitGrid.htm",
    "routeURL": "/bireports/service/technician/servicevisit",
    "visibility": false
  },
  {
    "modulename": "State Of Service by Business",
    "programURL": "/Service/iCABSSeStateOfServiceReportsGrid.htm<Business>",
    "routeURL": "bireports/service/stateofservice/by/business",
    "visibility": false,
    "queryParams": {"pageType": "Business"}
  },
  {
    "modulename": "State Of Service by Region",
    "programURL": "/Service/iCABSSeStateOfServiceReportsGrid.htm<Region>",
    "routeURL": "bireports/service/stateofservice/by/region",
    "visibility": false,
    "queryParams": {"pageType": "Region"}
  },
  {
    "modulename": "State Of Service by Branch",
    "programURL": "/Service/iCABSSeStateOfServiceReportsGrid.htm<Branch>",
    "routeURL": "bireports/service/stateofservice/by/branch",
    "visibility": false,
    "queryParams": {"pageType": "Branch"}
  },
  {
    "modulename": "State of Service Aged Profile by Business",
    "programURL": "/Service/iCABSSeStateOfServiceReportsGrid.htm<Business>",
    "routeURL": "bireports/service/sosagedprofile/by/business",
    "visibility": false,
    "queryParams": {"pageType": "Business","Aged":true}
  },
  {
    "modulename": "State of Service Aged Profile by Branch",
    "programURL": "/Service/iCABSSeStateOfServiceReportsGrid.htm<Branch>",
    "routeURL": "bireports/service/sosagedprofile/by/branch",
    "visibility": false,
    "queryParams": {"pageType": "Branch","Aged":true}
  },
  {
    "modulename": "Productivity for Pest Control by Business",
    "programURL": "/Service/iCABSSeProductivityMulti(PES)Grid.htm",
    "routeURL": "bireports/service/productivity/multi/pes/business",
    "visibility": false,
    "queryParams": {"pageType": "Business"}
  },
  {
    "modulename": "Productivity for Pest Control by Region",
    "programURL": "/Service/iCABSSeProductivityMulti(PES)Grid.htm",
    "routeURL": "bireports/service/productivity/multi/pes/region",
    "visibility": false,
    "queryParams": {"pageType": "Region"}
  },
  {
    "modulename": "Productivity for Pest Control by Branch",
    "programURL": "/Service/iCABSSeProductivityMulti(PES)Grid.htm",
    "routeURL": "bireports/service/productivity/multi/pes/branch",
    "visibility": false,
    "queryParams": {"pageType": "Branch"}
  },
  {
    "modulename": "Productivity for Washrooms by Business",
    "programURL": "/Service/iCABSSeProductivityBusinessGrid.htm",
    "routeURL": "bireports/service/productivity/hc/business",
    "visibility": false,
    "queryParams": {"type": "Business"}
  },
  {
    "modulename": "Productivity for Washrooms by Region",
    "programURL": "/Service/iCABSSeProductivityRegionGrid.htm",
    "routeURL": "bireports/service/productivity/hc/region",
    "visibility": false,
    "queryParams": {"type": "Region"}
  },
  {
    "modulename": "Productivity for Washrooms by Branch",
    "programURL": "/Service/iCABSSeProductivityBranchGrid.htm",
    "routeURL": "bireports/service/productivity/hc/branch",
    "visibility": false,
    "queryParams": {"type": "Branch"}
  },
  {
    "modulename": "State of Service Detail Report",
    "programURL": "/ApplicationReport/iCABSARStateOfServiceDetailReport.htm",
    "routeURL": "bireports/service/stateofservice/detailreport",
    "visibility": false
  },
  {
    "modulename": "Actual vs. Contractual Report",
    "programURL": "/ApplicationReport/iCABSARActualVsContractualReport.htm",
    "routeURL": "bireports/actualvscontractual/acual/vscontractual",
    "visibility": false
  },
  {
    "modulename": "State of Service Aged Arrears for Hygiene by Business",
    "programURL": "/Service/iCABSSeAgedArrearsReportsGrid.htm<BusinessAged>",
    "routeURL": "bireports/service/stateofservice/agedarrears/business",
    "visibility": false,
    "queryParams": {"type": "Business"}
  },
  {
    "modulename": "State of Service Aged Arrears for Hygiene by Branch",
    "programURL": "/Service/iCABSSeAgedArrearsReportsGrid.htm<BranchAged>",
    "routeURL": "bireports/service/stateofservice/agedarrears/branch",
    "visibility": false,
    "queryParams": {"type": "Branch"}
  },
  {
    "modulename": "Business Aged Arrears",
    "programURL": "/Service/iCABSSeBatchSOSAgedArrearsBusinessGrid.htm",
    "routeURL": "stateofservice/batchsos/agedarrears/business",
    "visibility": false,
    "queryParams": {"type": "Business"}
  },
  {
    "modulename": "Additional Visit (Chargeable Callouts) Report",
    "programURL": "/ApplicationReport/iCABSARAdditionalVisitReportGrid.htm",
    "routeURL": "bireports/service/additionalvisit/report/grid",
    "visibility": false
  },
  {
    "modulename": "Customer Chargeable Callouts",
    "programURL": "/ApplicationReport/iCABSARCustomerCCOReportGrid.htm",
    "routeURL": "bireports/service/customer/cco",
    "visibility": false
  },
  {
    "modulename": "Infestations (All)",
    "programURL": "/Service/iCABSSeInfestationServiceCoverGrid.htm",
    "routeURL": "bireports/service/infestations/by/all",
    "visibility": false
  },
  {
    "modulename": "Infestations by Branch",
    "programURL": "/Service/iCABSSeInfestationGrid.htm",
    "routeURL": "bireports/service/infestations/by/branch",
    "visibility": false
  },
  {
    "modulename": "Infestations by Contract",
    "programURL": "/Service/iCABSSeInfestationGrid.htm",
    "routeURL": "bireports/service/infestations/by/branch",
    "visibility": false,
    "queryParams": {"NatAccount":"true"}
  },
  {
    "modulename": "Proof of Service",
    "programURL": "/Application/iCABSBulkProofofService.htm",
    "routeURL": "/bireports/service/proof",
    "visibility": false
   },
   {
    "modulename": "Service Visits by Type",
    "programURL": "/Service/iCABSSeVisitTypeGrid.htm",
    "routeURL": "/bireports/service/servicevisit/by/types",
    "visibility": false
    },
    {
      "modulename": "Stock Control Extracts AU/NZ",
      "programURL": "riBatch/stockcontrol.html#demo",
      "routeURL": "/riBatch/stockcontrol",
      "visibility": false,
      "alwaysdisplay": true,
      "queryParams": {"pageType": "Stock Control Extracts"}
    },
  {
    "modulename": "Branch Static Visits",
    "programURL": "/Service/iCABSSeStaticVisitGridYearBranch.htm",
    "routeURL": "/bireports/service/staticvisit/by/branch",
    "visibility": false
   },
   {
    "modulename": "Component Replacement Ordering List (Ambius)",
    "programURL": "/Service/iCABSSeComponentOrderingListGrid.htm<Branch>",
    "routeURL": "/bireports/service/componentordering/by/branch",
    "visibility": false,
    "queryParams": {"reportLevel": "Branch"}
    }]
    },
        {
          "featurename": "Contractual",
          "visibility": false,
          "module": [
            {
              "modulename": "Jobs Released for Invoicing by Business",
              "programURL": "/ApplicationReport/iCABSARReleasedForInvoiceBusinessGrid.htm",
              "routeURL": "/bireports/sales/releasedinvoicebusiness/job",
              "queryParams": {"contractType": "J","pageType": "Business" },
              "visibility": false
            },
            {
              "modulename": "Jobs Released for Invoicing by Region",
              "programURL": "/ApplicationReport/iCABSARReleasedForInvoiceRegionGrid.htm",
              "routeURL": "/bireports/sales/releasedinvoiceregion/job",
              "queryParams": {"contractType": "J","pageType": "Region" },
              "visibility": false
            },
            {
              "modulename": "Jobs Released for Invoicing by Branch",
              "programURL": "/ApplicationReport/iCABSARReleasedForInvoiceBranchGrid.htm",
              "routeURL": "/bireports/sales/releasedinvoicebranch/job",
              "queryParams": {"contractType": "J","pageType": "Branch" },
              "visibility": false
            },
            {
              "modulename": "Jobs Released for Invoicing by Employee",
              "programURL": "/ApplicationReport/iCABSARReleasedForInvoiceGrid.htm",
              "routeURL": "/bireports/sales/releasedinvoice/job",
              "queryParams": {"contractType": "J" },
              "visibility": false
            },
            {
              "modulename": "Product Sales Invoiced by Region",
              "programURL": "/ApplicationReport/iCABSARReleasedForInvoiceRegionGrid.htm",
              "routeURL": "/bireports/sales/releasedinvoiceregion/product",
              "queryParams": {"contractType": "P","pageType": "Region"},
              "visibility": false
            },
            {
              "modulename": "Product Sales Invoiced by Branch",
              "programURL": "/ApplicationReport/iCABSARReleasedForInvoiceBranchGrid.htm",
              "routeURL": "/bireports/sales/releasedinvoicebranch/product",
              "queryParams": {"contractType": "P","pageType": "branch"},
              "visibility": false
            },
            {
              "modulename": "Product Sales Invoiced by Employee",
              "programURL": "/ApplicationReport/iCABSARReleasedForInvoiceGrid.htm",
              "routeURL": "/bireports/sales/releasedinvoice/product",
              "queryParams": {"contractType": "P"},
              "visibility": false
            },
            {
              "modulename": "Daily Transactions by Business",
              "programURL": "/ApplicationReport/iCABSARDailyTransactionsGridBusiness.htm",
              "routeURL": "bireports/portfolio/dailytransactiongrid/by/business",
              "queryParams": {"ReportLevel": "Business"},
              "visibility": false
            },
            {
              "modulename": "Daily Transactions by Branch",
              "programURL": "/ApplicationReport/iCABSARDailyTransactionsGrid.htm",
              "routeURL": "bireports/portfolio/dailytransactiongrid",
              "visibility": false
            },
            {
              "modulename": "Contract / Job Report by Business",
              "programURL": "/ApplicationReport/iCABSARContractReport.htm<Business>",
              "routeURL": "bireports/portfolio/general/contract",
              "queryParams": {"ReportLevel": "Business"},
              "visibility": false
            },
            {
              "modulename": "Contract / Job Report by Region",
              "programURL": "/ApplicationReport/iCABSARContractReport.htm<Region>",
              "routeURL": "bireports/portfolio/general/contract",
              "queryParams": {"ReportLevel": "Region"},
              "visibility": false
            },
            {
              "modulename": "Contract / Job Report by Branch",
              "programURL": "/ApplicationReport/iCABSARContractReport.htm<Branch>",
              "routeURL": "bireports/portfolio/general/contract",
              "queryParams": {"ReportLevel": "Branch"},
              "visibility": false
            },
            {
              "modulename": "Contracts / Jobs Due to Expire by Business",
              "programURL": "/Application/iCABSAContractsDueToExpireBusinessGrid.htm",
              "routeURL": "bireports/portfolio/general/contractexpirebusinessgrid",
              "visibility": false
            },
            {
              "modulename": "Contracts / Jobs Due to Expire by Branch",
              "programURL": "/Application/iCABSAContractsDueToExpireGrid.htm",
              "routeURL": "bireports/portfolio/general/contractduetoexpiregrid",
              "queryParams": {"pageType": "Branch"}
            },
            {
              "modulename": "Continuous Contracts With POs by Business",
              "programURL": "/Application/iCABSAContractsWithExpiringPOBusinessGrid.htm",
              "routeURL": "bireports/portfolio/contracts/withexpiring/pob"
            },
            {
              "modulename": "Continuous Contracts With POs by Branch",
              "programURL": "/Application/iCABSAContractsWithExpiringPOGrid.htm",
              "routeURL": "bireports/portfolio/contracts/withexpiring/pogrid"
            },
            {
              "modulename": "Accounts by Category",
              "programURL": "/ApplicationReport/iCABSARAccountsByCategory.htm",
              "routeURL": "bireports/portfolio/accounts/category",
              "visibility": false
            },
            {
              "modulename": "Unallocated Units",
              "programURL": "/Application/iCABSAUnallocatedUnitsGrid.htm",
              "routeURL": "/bireports/portfolio/unallocatedunitsgrid",
              "visibility": false
            },
            {
              "modulename": "Branch Function Extracts AU/NZ",
              "programURL": "riBatch/branchfunction.html#demo",
              "routeURL": "/riBatch/branchfunction",
              "visibility": false,
              "alwaysdisplay": true,
              "queryParams": {"pageType": "Branch Functions Extracts"}
            },
            {
              "modulename": "Displays & Locations Extracts AU/NZ",
              "programURL": "riBatch/displaysandlocations.html#demo",
              "routeURL": "/riBatch/displaysandlocations",
              "visibility": false,
              "alwaysdisplay": true,
              "queryParams": {"pageType": "Displays & Locations Extracts"}
            }
          ]
        },
  {
    "featurename": "Client Retention",
    "visibility": false,
    "module": [{
      "modulename": "Requests Outstanding by Business",
      "programURL": "/ApplicationReport/iCABSARLostBusinessRequestsOutstandingBusiness.htm",
      "routeURL": "/bireports/clientretention/lostrequest/business/outstandingbusiness",
      "visibility": false,
      "queryParams": {"pageType": "Business"}
    },
    {
      "modulename": "Requests Outstanding by Region",
      "programURL": "/ApplicationReport/iCABSARLostBusinessRequestsOutstandingRegion.htm",
      "routeURL": "/bireports/clientretention/lostrequest/business/outstandingregion",
      "visibility": false,
      "queryParams": {"pageType": "Region"}
    },
    {
      "modulename": "Requests Outstanding by Branch",
      "programURL": "/ApplicationReport/iCABSARLostBusinessRequestsOutstandingBranch.htm",
      "routeURL": "/bireports/clientretention/lostrequest/business/outstandingbranch",
      "visibility": false,
      "queryParams": {"pageType": "Branch"}
    },
    {
      "modulename": "Requests Outstanding by Employee",
      "programURL": "/ApplicationReport/iCABSARLostBusinessRequestsOutstandingEmployee.htm",
      "routeURL": "/bireports/clientretention/lostrequest/business/outstandingemployee",
      "visibility": false,
      "queryParams": {"pageType": "Employee"}
    },
    {
      "modulename": "Requests Outcome by Business",
      "programURL": "/ApplicationReport/iCABSARLostBusinessRequestsOutcomeBusiness.htm",
      "routeURL": "/bireports/clientretention/requestsoutcome/by/business",
      "visibility": false,
      "queryParams": {"pageType": "business"}
    },
    {
      "modulename": "Requests Outcome by Region",
      "programURL": "/ApplicationReport/iCABSARLostBusinessRequestsOutcomeRegion.htm",
      "routeURL": "/bireports/clientretention/requestsoutcome/by/region",
      "visibility": false,
      "queryParams": {"pageType": "region"}
    },
    {
      "modulename": "Requests Outcome by Branch",
      "programURL": "/ApplicationReport/iCABSARLostBusinessRequestsOutcomeBranch.htm",
      "routeURL": "/bireports/clientretention/requestsoutcome/by/branch",
      "visibility": false,
      "queryParams": {"pageType": "branch"}
    },
    {
      "modulename": "Lost Business Analysis by Business",
      "programURL": "/ApplicationReport/iCABSARLostBusinessAnalysisBusinessGrid.htm",
      "routeURL": "/bireports/portfolio/lostbusiness/analysisgrid/business",
      "queryParams": {"type": "main"},
      "visibility": false
    },
    {
      "modulename": "Lost Business Analysis by Branch",
      "programURL": "/ApplicationReport/iCABSARLostBusinessAnalysisGrid.htm",
      "routeURL": "/bireports/portfolio/lostbusiness/analysisgrid/branch",
      "queryParams": {"type": "Branch"},
      "visibility": false
    }]
  },
  {
    "featurename": "Customer Contact",
    "visibility": false,
    "module": [{
      "modulename": "CCM Ticket Analysis",
      "programURL": "/ContactManagement/iCABSCMTicketAnalysisGeneration.htm",
      "routeURL": "/bireports/cm/ticket/analysis",
      "visibility": false
    },
    {
      "modulename": "Customer Complaints by Business",
      "programURL": "/ContactManagement/iCABSCMRootCauseAnalysisGrid.htm",
      "routeURL": "/bireports/cm/rootcause/analysis/business",
      "queryParams": {"pageType": "Business"},
      "visibility": false
    },
    {
      "modulename": "Customer Complaints by Region",
      "programURL": "/ContactManagement/iCABSCMRootCauseAnalysisGrid.htm",
       "routeURL": "/bireports/cm/rootcause/analysis/region",
       "queryParams": {"pageType": "Region"},
       "visibility": false
    },
    {
      "modulename": "Customer Complaints by Branch",
      "programURL": "/ContactManagement/iCABSCMRootCauseAnalysisGrid.htm",
      "routeURL": "/bireports/cm/rootcause/analysis/branch",
      "queryParams": {"pageType": "Branch"},
      "visibility": false
    }]
  }]
  }]
}`;
