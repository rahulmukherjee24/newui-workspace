import { ITaskWorker } from '../../web-worker/app-worker';

export class MenuWorker implements ITaskWorker {
    private currentEnvPatternComparer: string;
    private userAccess: any;
    private MainMenu: any;
    private mainNavData: Array<Object> = [];
    private http: any;
    private quickLinksArr: any = [];
    private email: string = '';

    private readonly c_o_QAPMapping: any = {
        'rentokil-initial.com': 'sso',
        'rentokil.com': 'saml',
        'ambius.com': 'ambius'
    };

    constructor() {
        this.currentEnvPatternComparer = '|' + self['environment']['NODE_ENV'] + '|';
    }

    public process = (data?: any): any => {
        this.email = data.usermail;
        return this.createUserAccessJson(data);
    }

    private createUserAccessJson = (data?: any): any => {
        let domains = JSON.parse(data.design).menu;
        this.userAccess = data.access.pages;
        this.MainMenu = this.checkAndUpdateDomainVisibility(domains);
        return {
            mainmenu: this.MainMenu,
            mainnavdata: this.mainNavData,
            quicklinks: this.quickLinksArr
        };
    }

    private checkAndUpdateDomainVisibility = (domains: any): any => {
        let retValue = false;
        let temp = domains.slice(0);
        let isDomainHiddenInCurrentEnvironment: boolean;
        for (let i = 0; i < temp.length; i++) {
            let domain = temp[i];
            let featureArray = domain.feature;
            isDomainHiddenInCurrentEnvironment = !this.isVisibleInCurrentEnvironment(domain['hidein']);
            domain.visibility = this.checkAndUpdateFeatureVisibility(featureArray, isDomainHiddenInCurrentEnvironment);
            if ((!domain.visibility && !domain.alwaysdisplay) || !this.isVisibleInCurrentEnvironment(domain['hidein'])) {
                domains.splice(i, 1);
                temp.splice(i, 1);
                i--;
            }
            if (!retValue) {
                retValue = domain.visibility;
            }
        }
        return domains;
    }

    private checkAndUpdateFeatureVisibility = (featureArray: any, isHiddenInEnvironment?: boolean): boolean => {
        let retValue = false;
        let temp = featureArray.slice(0);
        let isFeatureHiddenInCurrentEnvironment: boolean;
        for (let j = 0; j < temp.length; j++) {
            let feature = temp[j];
            let moduleArray = feature.module;
            isFeatureHiddenInCurrentEnvironment = isHiddenInEnvironment || !this.isVisibleInCurrentEnvironment(feature['hidein']);
            feature.visibility = this.checkAndUpdateModuleVisibility(moduleArray, isFeatureHiddenInCurrentEnvironment);
            if ((!feature.visibility) || isFeatureHiddenInCurrentEnvironment) {
                featureArray.splice(j, 1);
                temp.splice(j, 1);
                j--;
            }
            if (!retValue) {
                retValue = feature.visibility;
            }
        }
        return retValue;
    }

    private checkAndUpdateModuleVisibility = (moduleArray: any, isHiddenInEnvironment?: boolean): boolean => {
        let retValue = false;
        let temp = moduleArray.slice(0);
        for (let k = 0; k < temp.length; k++) {
            let module = temp[k];
            /**
             * Always Display Flags Has Been Moved To Module Level
             * So That The Domain And Feature Level Can Be Flexible
             */
            module.visibility = module.alwaysdisplay;
            if (!module.visibility) {
                module.visibility = this.hasMenuAccess(module.programURL);
            }
            if (module.quickLinks && module.visibility) {
                this.quickLinksArr[this.quickLinksArr.length] = { text: module.modulename, route: module.routeURL, image: module.icon };
            }
            if ((!module.visibility) || isHiddenInEnvironment || !this.isVisibleInCurrentEnvironment(module['hidein'])) {
                moduleArray.splice(k, 1);
                temp.splice(k, 1);
                k--;
            }
            else if (temp[k]) {
                if (module.QAP) {
                    module.routeURL = this.getQAPUrl(module.routeURL);
                }
                this.mainNavData.push(temp[k]);
            }
            if (!retValue) {
                retValue = module.visibility;
            }
        }
        return retValue;
    }

    private isVisibleInCurrentEnvironment = (hideInEnvList: string): boolean => {
        let visible: boolean = true;
        if (hideInEnvList && (hideInEnvList.toLowerCase() === 'all' || hideInEnvList.indexOf(this.currentEnvPatternComparer) >= 0)) {
            visible = false;
        }

        return visible;
    }

    public hasMenuAccess = (value: any): any => {
        let keys = this.userAccess;
        for (let i = 0; i < (keys.length); i++) {
            if (keys[i].ProgramURL === value) {
                return true;
            }
        }
        return false;
    }

    private getQAPUrl = (url: string): string => {
        let domain: string = this.email.split('@')[1];
        return 'https://qap.rentokil-initial.com/' + this.c_o_QAPMapping[domain] + '/' + url;
    }
}
