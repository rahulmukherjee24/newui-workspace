import { environment } from './../../../environments/environment';
import { QuickLinks } from './../../../app/login/quickLinks';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { LocalStorageService } from 'ngx-webstorage';
import { Component, OnInit, AfterViewInit, NgZone, OnDestroy, ElementRef, Output, EventEmitter } from '@angular/core';
import { VariableService } from '../../services/variable.service';
import { Utils } from '../../services/utility';
import { MAIN_NAV_DESIGN } from '../main-nav/main-nav-design';
import { DatepickerComponent } from '../../components/datepicker/datepicker';
import { WebWorkerService } from '../../web-worker/web-worker';
import { MenuWorker } from './menu-worker';
import { AppWebWorker } from '../../web-worker/app-worker';
import { HttpWorker } from '../../web-worker/http-worker';

@Component({
    selector: 'icabs-main-nav',
    templateUrl: 'main-nav.html'

})
export class MainNavComponent implements OnInit, AfterViewInit, OnDestroy {
    private workerService: WebWorkerService;

    constructor(private _authService: AuthService,
        private _ls: LocalStorageService,
        private ele: ElementRef,
        private zone: NgZone,
        private _route: ActivatedRoute,
        private route: Router,
        private utils: Utils,
        private variableService: VariableService
    ) {
        this.loader = '';
        this.loaderIntervalId = setInterval(() => {
            this.handleLoader();
        }, 150);
    }
    @Output() public closeNavEmit = new EventEmitter();
    @Output() public onOpenNav = new EventEmitter();
    public data: any;
    public pageKeys: any;
    public mainNavDesign = MAIN_NAV_DESIGN;
    public MainMenu: any;
    public selectedDomain: string;
    public displayFeatureNav: boolean = false;
    public autocompleteMenuData: Array<Object> = [];
    public showMenu: Object = {};
    private documentOnClickRef: any;
    public loader: string = '';
    private loaderIntervalId: any;

    getUserAccess(data: any): void {
        let worker: AppWebWorker = new AppWebWorker();
        if (data && data.pages && data.pages.length) {
            let pages = this.utils.sortByKey(data.pages,'ProgramURL', false, true);
            data.pages = pages;
            this._ls.store('menu', data);
            this.workerService = new WebWorkerService(MenuWorker, environment);
            this.workerService.run(worker.delegate, {
                access: data,
                design: this.mainNavDesign,
                usermail: this._ls.retrieve('RIUserEmail')
            }).then(data => {
                this.createNavData(data);
            }).catch(error => {
                console.log(error);
            });
        } else {
            this._authService.clearData();
            this._authService.signOut();
            this.route.navigate(['/application/login']);
        }
    }

    ngOnInit(): void {
        if (!this._route.snapshot.data['domain']) {
            this.selectedDomain = '';
        } else {
            this.selectedDomain = this._route.snapshot.data['domain'];
        }

        this.documentOnClickRef = this.onDocumentClick.bind(this);
        document.addEventListener('click', this.documentOnClickRef);

        this.getUserAccessResponse();
    }

    ngAfterViewInit(): void {
        this.mainNavSetHeight();
        this.featureWrapperSetHeight();
        window.onresize = () => {
            this.mainNavSetHeight();
            this.featureWrapperSetHeight();
        };
    }

    ngOnDestroy(): void {
        document.removeEventListener('click', this.documentOnClickRef);
    }

    menuClick(): void {
        this.variableService.setMenuClick(true);
    }

    createNavData(data: any): void {
        this.zone.run(() => {
            this.MainMenu = data.mainmenu;
            this.autocompleteMenuData = [];
            this.autocompleteMenuData = this.removeDuplicatesBy(x => x.modulename, data.mainnavdata);
            this.autocompleteMenuData = this.utils.sortByKey(this.autocompleteMenuData, 'modulename');
            this._ls.store('AUTOCOMPLETE', this.autocompleteMenuData);
            this._ls.store('MAINMENU', this.MainMenu);
            this._ls.store('QUICKLINKS', data.quicklinks);
            QuickLinks.List = this.getQuickLinks(data.quicklinks);
            QuickLinks.List = data.quicklinks;

            if (this.MainMenu) {
                for (let i = 0; i < this.MainMenu.length; i++) {
                    this.showMenu[this.MainMenu[i]['id']] = false;
                }
            }
        });
    }

    onModuleSelect(data: any): void {
        let params = this.genQueryParam(data.originalObject);
        this.route.navigate([data.originalObject.routeURL], { queryParams: params });
    }
    public mainNavSetHeight(): void {
        let elem = document.getElementById('main-nav');
        if (elem !== null) {
            if (window.innerWidth < 768) {
                elem.style.height = window.innerHeight - 66 + 'px';
            } else {
                elem.style.height = 'auto';
            }
        }
    }
    public featureWrapperSetHeight(): void {
        if (document.querySelectorAll('.feature-wrapper') !== null && document.querySelectorAll('.feature-wrapper').length > 0 && document.querySelector('.custom-container') !== null) {
            if (window.innerWidth > 767 && window.innerWidth < 1200) {
                this.setCss('.feature-wrapper{height: ' + (window.innerHeight - 150) + 'px}');
            } else if (window.innerWidth > 1200) {
                this.setCss('.feature-wrapper{height: ' + (window.innerHeight - 180) + 'px}');
            } else {
                this.setCss('.feature-wrapper{height: auto}');
            }
        }
    }

    private removeDuplicatesBy(keyFn: any, array: Array<any>): any {
        let mySet = new Set();
        return array.filter((x) => {
            let key = keyFn(x), isNew = !mySet.has(key);
            if (isNew) mySet.add(key);
            return isNew;
        });
    }

    public setCss(css: any): void {
        let head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');

        style.type = 'text/css';
        style.id = 'feature-wrapper-style';
        if (document.getElementById('feature-wrapper-style')) {
            document.getElementById('feature-wrapper-style').innerText = css + 'sudeep';
        } else {
            style.appendChild(document.createTextNode(css + 'dutta'));
            head.appendChild(style);
        }
    }

    public openFeature(id: any, event: any): void {
        event.stopPropagation();
        if (window.innerWidth < 768) {
            let eleDomain = this.ele.nativeElement.querySelector('#' + id);
            if (eleDomain.className !== 'domain active') {
                eleDomain.className += ' active';
                this.displayFeatureNav = true;
            } else {
                return;
            }
        } else {
            for (let i in this.showMenu) {
                if (i !== id)
                    this.showMenu[i] = false;
            }
            this.showMenu[id] = !this.showMenu[id];
            this.mainNavSetHeight();
            this.featureWrapperSetHeight();
            this.onOpenNav.emit({
                open: true
            });
        }
    }

    public openModule(id: any): void {
        if (window.innerWidth < 768) {
            let eleDomain = this.ele.nativeElement.querySelector('#' + id);
            if (eleDomain.className !== 'feature active') {
                if (this.ele.nativeElement.querySelectorAll('.feature-wrapper .feature.active').length > 0) {
                    this.ele.nativeElement.querySelector('.feature-wrapper .feature.active').className = 'feature';
                }
                eleDomain.className += ' active';
            }
        }
    }

    public backToMain(e: any): void {
        this.ele.nativeElement.querySelector('.main-nav .domain.active').classList.remove('active');
        this.displayFeatureNav = false;
    }

    public closeNav(e: any): void {
        this.closeNavEmit.emit(e);
    }

    public onDocumentClick(event: any): void {
        setTimeout(() => {
            for (let i in this.showMenu) {
                if (this.showMenu.hasOwnProperty(i))
                    this.showMenu[i] = false;
            }
        }, 0);
        if (DatepickerComponent && DatepickerComponent.dateInstance.length > 0) {
            for (let j = 0; j < DatepickerComponent.dateInstance.length; j++) {
                DatepickerComponent.dateInstance[j].opened = false;
            }
        }
        let elem = document.querySelector('.gridtable .selected');
        if (elem)
            this.utils.removeClass(elem, 'selected');
        elem = null;
    }

    public genQueryParam(obj: any): any {
        let param = { fromMenu: true };
        if (obj.hasOwnProperty('queryParams')) {
            for (let i in obj.queryParams) {
                if (i !== '') {
                    param[i] = obj.queryParams[i];
                }
            }
        }
        return param;
    }

    private getUserAccessResponse(): void {
        let googleData: any = this._ls.retrieve('GOOGLEDATA');
        if (!googleData) {
            return;
        }
        this._ls.store('RIUserEmail', googleData.email);
        this.MainMenu = this._ls.retrieve('MAINMENU');
        if (this.MainMenu) {
            this.autocompleteMenuData = this._ls.retrieve('AUTOCOMPLETE');
            QuickLinks.List = this._ls.retrieve('QUICKLINKS');
            return;
        }
        let worker: AppWebWorker = new AppWebWorker();
        this.workerService = new WebWorkerService(HttpWorker, environment);
        this.workerService.run(worker.delegatePromise, {
            url: environment['USER_ACCESS_POSTFIX'],
            token: this._ls.retrieve('token'),
            base: this._authService.getBaseURL(),
            search: {
                email: googleData.email,
                action: '0'
            }
        }, true);
        this.workerService.worker.onmessage = response => {
            let userAccess: any = response.data.oResponse;
            try {
                let checkError = this._authService.checkError(userAccess, 'Menu');
                if (checkError) {
                    throw userAccess;
                }
                this.getUserAccess(userAccess);
            } catch (err) {
                console.log('Exception');
            }

        };
    }

    private handleLoader(): void {
        if (this.MainMenu) {
            clearInterval(this.loaderIntervalId);
            this.loader = '';
            return;
        }

        if (this.loader.length !== 25) {
            this.loader += '.';
        } else {
            this.loader = '';
        }
    }

    private getQuickLinks(data: any): any {
        let colArr: any = [];
        let rowArr: any = [];
        for (let i = 0; i < data.length; i++) {
            colArr[colArr.length] = data[i];
            if ((i + 1) % 3 === 0 || i + 1 === data.length) {
                rowArr[rowArr.length] = colArr;
                colArr = [];
            }
        }
        return rowArr;
    }
}
