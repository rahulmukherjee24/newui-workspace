/**
 * Class To Contain Main Navigation Constants
 */

export class MainNavConstants {
    public static readonly c_s_MAIN_NAV_ENV_ACCESS_DELIMITER: string = '|';
    public static readonly c_s_FEATURE_NAME: string = 'featureName';
    public static readonly c_s_HIDE_IN_ENV_LIST: string = 'hidein';
}
