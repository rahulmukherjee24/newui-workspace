import { Component, Input, Output, EventEmitter, OnInit, OnChanges, ViewChild, ElementRef } from '@angular/core';
import { FocusService } from '@shared/services/focus.service';
import { Utils } from '../../../shared/services/utility';

@Component({
    selector: 'icabs-modal',
    templateUrl: 'modal.html',
    exportAs: 'child',
    styles: [`
        .modal-body {
            min-height: 150px;
        }
    `]
})
export class ModalComponent implements OnInit, OnChanges {
    @ViewChild('childModal') childModal;
    @ViewChild('closeBtn') closeBtn: ElementRef;
    @Input() config: any;
    @Input() title: string;
    @Input() content: any;
    @Input() showHeader: boolean;
    @Input() showCloseButton: boolean;
    @Input() size: string = 'lg';
    @Input() triggerEmitterOnClose: boolean;
    @Output() modalClose = new EventEmitter<any>();

    public isArrayType = false;
    public isFullError = false;
    public isFullErrorArrayType = false;
    public fullErrorContent: any;
    public isfirstfocus: boolean = true;
    constructor(private ele: ElementRef, private util: Utils, private focusService: FocusService) { }

    ngOnInit(): void {
        if (typeof this.config !== 'object' || !this.config || (Object.keys(this.config).length === 0 && this.config.constructor === Object)) {
            this.config = {
                backdrop: 'static',
                keyboard: true
            };
        }
        this.showCloseButton = (this.showCloseButton == null) || this.showCloseButton;

        if (this.triggerEmitterOnClose === null || this.triggerEmitterOnClose === undefined) {
            this.triggerEmitterOnClose = true;
        }
    }

    ngOnChanges(...args: any[]): void {
        if (typeof this.config !== 'object' || !this.config || (Object.keys(this.config).length === 0 && this.config.constructor === Object)) {
            this.config = {
                backdrop: 'static',
                keyboard: true
            };
        }
        this.showCloseButton = (this.showCloseButton == null) || this.showCloseButton;
    }

    show(data: any, error?: any): void {
        this.fullErrorContent = '';
        this.isFullError = false;
        if (error === true) {
            if (data && data.error) {
                this.title = data.error.title;
                this.content = data.error.message;
            } else if (data && (data.errorMessage || data.fullError)) {
                this.title = 'Error';
                this.content = data.errorMessage;
                this.isArrayType = (this.content instanceof Array);
                if (data.fullError) {
                    this.isFullError = true;
                    this.isFullErrorArrayType = (data.fullError instanceof Array);
                    this.fullErrorContent = data.fullError;
                }
            } else if (data && (data.msg && data.stack)) {
                this.title = 'UI Console Error';
                this.content = data.msg + ' ' + data.stack;
            }
        }
        else if (error === false) {
            if (data && data.msg) {
                this.title = data.title;
                this.content = data.msg;
                this.isArrayType = (typeof this.content !== 'string');
            }
        } else {
            if (data && data.error) {
                this.title = data.error.title;
                this.content = data.error.message;
            }
        }
        this.focusService.setFocusableElement();
        this.childModal.show();
    }

    hide(isDataClicked?: boolean): void {
        this.isfirstfocus = true;
        this.childModal.hide();
        if (this.triggerEmitterOnClose === false) {
            return;
        }
        this.modalClose.emit({
            isDataClicked: isDataClicked
        });
        this.focusService.focusElement();
    }

    onHidden(event: any): void {
        try {
            if (document.querySelectorAll('[bsmodal].in').length <= 0) {
                let elem = document.getElementsByClassName('modal-backdrop');
                let bodyElem: any = document.querySelector('body');
                let styleClass: string = bodyElem.getAttribute('class');
                bodyElem.setAttribute('class', styleClass.replace(' modal-open', ''));
                while (elem[0]) {
                    if (elem[0].parentNode)
                        elem[0].parentNode.removeChild(elem[0]);
                }
            } else {
                let elem: any = document.querySelector('body');
                let styleClass: string = elem.getAttribute('class');
                elem.setAttribute('class', styleClass + ' modal-open');
            }
            this.util.resetOptions();
            this.modalClose.emit();
            this.focusService.focusElement();
        } catch (excp) {
            console.log('Modal Excp: ', excp);
        }
    }
    gotofirst(): void {
        this.ele.nativeElement.querySelectorAll('.modal-content input, .modal-content textarea, .modal-content button, .modal-content a, .modal-content select')[0].focus();
    }
    gotolast(): void {
        let tabbables = this.ele.nativeElement.querySelectorAll('.modal-content input, .modal-content textarea, .modal-content button, .modal-content a, .modal-content select');
        if (this.isfirstfocus) {
            tabbables[0].focus();
            this.isfirstfocus = false;
        } else {
            tabbables[tabbables.length - 2].focus();
        }
    }
    public onShown(): void {
        if (this.showCloseButton) {
            this.closeBtn.nativeElement.focus();
        }
    }
}
