/**
 * @description
 * Advanced Modal component
 * AVOID CALLING METHODS MARKED AS 'View Only' FROM OUTSIDE THE COMPONENT
 * @class ModalAdvComponent
 * @implements OnInit, OnChanges, OnDestroy
 * @todo
 *  - Remove commented unused class properties and method
 * @version 1.0.0
 */
import { ICabsModalVO, ICabsModalConstants } from './modal-adv-vo';
import { Component, OnInit, OnChanges, OnDestroy, ViewChild, HostListener, ElementRef } from '@angular/core';
import { FocusService } from '@shared/services/focus.service';

@Component({
    selector: 'icabs-modal-adv',
    templateUrl: 'modal-adv.html',
    exportAs: 'child',
    styles: [`
        .shiftTop {
            top: 30%;
        }
        .modal-body-min-height {
            min-height: 100px;
            white-space: pre-line;
        }
    `]
})
export class ModalAdvComponent implements OnInit, OnChanges, OnDestroy {
    @ViewChild('childModal') childModal;
    @ViewChild('closeBtn') closeBtn: ElementRef;

    public config: any = {
        backdrop: 'static',
        keyboard: true
    };
    public options: ICabsModalVO = new ICabsModalVO();
    public showFooter: boolean = false;
    public showMsg: boolean = true;
    public showFullError: boolean = false;
    public showHTMLmsg: boolean = false;
    public msgArr: any = [];
    public fullErrorArr: any = [];

    constructor(private focusService: FocusService) {
        //todo
    }
    /************************* Start: Lifecycle Hooks ********************************/
    public ngOnInit(): void {
        //todo
    }

    public ngOnChanges(...args: any[]): void {
        //todo
    }
    public ngOnDestroy(): void {
        //todo
    }
    @HostListener('window:keydown', ['$event'])
    keyboardInput(e: KeyboardEvent): void {
        this.onKeyDownDocumnt(e);
    }
    /************************* End: Lifecycle Hooks ********************************/

    public show(data: any): void {
        this.processData(data);
        this.focusService.setFocusableElement();
        this.childModal.show();
    }

    public hide(): void {
        if (this.options.closeCallback) {
            this.options.closeCallback.call();
        }
        this.resetView();
        this.focusService.focusElement();
        this.childModal.hide();
    }

    public onHidden(event: any): void {
        if (document.querySelectorAll('[bsmodal].in').length <= 0) {
            let elem = document.getElementsByClassName('modal-backdrop');
            let bodyElem: any = document.querySelector('body');
            let styleClass: string = bodyElem.getAttribute('class');
            bodyElem.setAttribute('class', styleClass.replace(' modal-open', ''));
            while (elem[0]) {
                if (elem[0].parentNode)
                    elem[0].parentNode.removeChild(elem[0]);
            }
        } else {
            let elem: any = document.querySelector('body');
            let styleClass: string = elem.getAttribute('class');
            document.querySelector('body').setAttribute('class', styleClass + ' modal-open');
        }
        this.focusService.focusElement();
    }

    public cancel(): void {
        let callbackData: any;
        if (this.options.cancelCallback) {
            callbackData = {
                value: ICabsModalConstants.CANCEL,
                data: this.options.data
            };
            this.options.cancelCallback.call(callbackData, callbackData);
        }
        this.hide();
    }

    public confirm(): void {
        let callbackData: any;
        if (this.options.confirmCallback) {
            callbackData = {
                value: ICabsModalConstants.CONFIRM,
                data: this.options.data
            };
            this.options.confirmCallback.call(callbackData, callbackData);
        }
        this.hide();
    }

    private processData(data: ICabsModalVO): void {
        this.resetView();
        if (data) {
            this.options.modalType = data.modalType;
            if (data.msg) {
                this.msgArr = data.msg;
                if (!Array.isArray(data.msg)) {
                    this.msgArr = [data.msg];
                }
            } else {
                this.showMsg = false;
            }

            if (data.fullError) {
                this.showFullError = true;
                this.fullErrorArr = data.fullError;
                if (!Array.isArray(data.fullError)) {
                    this.fullErrorArr = [data.fullError];
                }
            }
            if (this.options.config) {
                this.config = this.options.config;
            }
            this.options.msg = data.msg;
            this.options.confirmCallback = data.confirmCallback;
            this.options.cancelCallback = data.cancelCallback;
            this.options.closeCallback = data.closeCallback;
            this.options.data = data.data;
            switch (data.modalType) {
                case ICabsModalConstants.MODAL_TYPE_ERROR:
                    this.options.title = (data.title) ? data.title : 'Error';
                    break;
                case ICabsModalConstants.MODAL_TYPE_MESSAGE:
                    this.options.title = (data.title) ? data.title : 'Message';
                    break;
                case ICabsModalConstants.MODAL_TYPE_HTML:
                    this.options.title = (data.title) ? data.title : 'Message';
                    this.showMsg = false;
                    this.showHTMLmsg = true;
                    break;
                case ICabsModalConstants.MODAL_TYPE_PROMPT:
                    this.options.title = (data.title) ? data.title : '';
                    this.options.confirmLabel = (data.confirmLabel) ? data.confirmLabel : 'Confirm';
                    this.options.cancelLabel = (data.cancelLabel) ? data.cancelLabel : 'Cancel';
                    this.showFooter = true;
                    break;
            }
            this.options.shiftTop = data.shiftTop;
        }
    }
    private resetView(): void {
        this.options = new ICabsModalVO();
        this.showFooter = false;
        this.showMsg = true;
        this.showFullError = false;
        this.showHTMLmsg = false;
        this.msgArr = [];
        this.fullErrorArr = [];
    }
    private onKeyDownDocumnt(e: any): void {
        switch (e.keyCode) {
            case 27:
                this.hide();
                break;
        }
    }
    public onShown(): void {
        if (this.options.showCloseButton) {
            this.closeBtn.nativeElement.focus();
        }
    }
}
