import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { ICabsModalConstants } from './modal-adv-vo';
import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';

@Injectable()
export class ModalAdvService {
    private _source = new BehaviorSubject<any>(0);
    private _source$ = this._source.asObservable();

    constructor(private _logger: NGXLogger) {
    }

    public emitError(data: any): void {
        if (data) {
            data.modalType = ICabsModalConstants.MODAL_TYPE_ERROR;
        }
        this._source.next(data);
    }

    public emitMessage(data: any): void {
        if (data) {
            data.modalType = ICabsModalConstants.MODAL_TYPE_MESSAGE;
        }
        this._source.next(data);
    }

    public emitHTMLMessage(data: any): void {
        if (data) {
            data.modalType = ICabsModalConstants.MODAL_TYPE_HTML;
        }
        this._source.next(data);
    }

    public emitPrompt(data: any): void {
        if (data) {
            data.modalType = ICabsModalConstants.MODAL_TYPE_PROMPT;
        }
        this._source.next(data);
    }

    public getSource(): any {
        return this._source;
    }

    public getObservableSource(): any {
        return this._source$;
    }
}
