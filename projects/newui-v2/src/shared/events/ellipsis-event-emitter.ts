import { Subject } from 'rxjs/internal/Subject';
import { Observable } from 'rxjs/internal/Observable';

export class SelectedDataEvent {
    private selectedDataEvent = new Subject<any>();

    public getSelectedDataEvent(): Observable<any> {
        return this.selectedDataEvent.asObservable();
    }
    public emitSelectedData(data: any): void {
        this.selectedDataEvent.next(data);
    }
}
