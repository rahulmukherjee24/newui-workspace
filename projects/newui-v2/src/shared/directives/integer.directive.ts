import { Directive, ElementRef } from '@angular/core';
import { NgControl, ControlContainer } from '@angular/forms';
import { GlobalizeService } from './../services/globalize.service';
import { BaseDirective } from '@app/base/BaseDirective';

@Directive({
    selector: '[eTypeInteger]'
})
export class IntegerDirective extends BaseDirective {

    constructor(el: ElementRef, control: NgControl, controlContainer: ControlContainer, private globalize: GlobalizeService) {
        super(el, control, controlContainer);
    }

    updateFormControl(value: any): void {
        if (typeof value !== 'undefined' && value !== null && value !== '') {
            let formattedValue: any = this.globalize.formatIntegerToLocaleFormat(value);
            if (formattedValue === false) {
                this.setError();
            } else {
                this.controlContainer['form'].controls[this.control.name].setValue(formattedValue);
            }
        }
    }
}
