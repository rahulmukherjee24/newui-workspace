import { LocalStorageService } from 'ngx-webstorage';
import { Subject } from 'rxjs';
import * as moment from 'moment';

/**
 * @class Emitter
 * @description Emitter for cached object
 */
export class Emitter {
    public result: Subject<any> = new Subject<any>();
}

/**
 * @class CacheHandler
 * @description Abstract class to be consumed by all cache handlers
 */
export abstract class CacheHandler {
    // Abstract Methods
    public abstract readItem(key: string): void;
    public abstract setItem(key: string, response: any): void;
    public abstract deleteItem(key: string): void;
    public abstract clearCache(): void;

    // Error methods
    protected onError(event: any): void {
        console.log(event);
    }
}

/**
 * @class CacheDBConstants
 * @description Constant utilities for DB Cache class
 */
export class CacheDBConstants {
    public static readonly c_s_DB_NAME: string = 'iCABS';
    public static readonly c_s_STORE_KEY_NAME: string = 'search-cache';
    public static readonly c_s_STORE_KEY_VERSION: string = 'version';
    public static readonly c_n_CACHE_TIMEOUT: number = 24;
}

// #region Indexed DB Handlers
/**
 * @class CacheDBFormat
 * @description DB values format
 */
export class CacheDBFormat {
    key: string;
    response: any;
    timestamp: Date;
}

export class CacheDBHandler extends CacheHandler {
    private factory: IDBFactory;
    private db: IDBDatabase;

    // Constructor
    constructor(private ls: LocalStorageService) {
        super();
        this.factory = window.indexedDB;
        this.init();
    }

    // #region Private Methods
    /**
     * @method init
     * @description Initializes the database
     * @param none
     * @returns void
     */
    private init(): void {
        let req: IDBOpenDBRequest = this.factory.open(CacheDBConstants.c_s_DB_NAME, this.getVersion());

        // Assign Event Callbacks
        req.onsuccess = (event: any) => {
            this.onDBOpen(event);
        };
        req.onupgradeneeded = (event: any) => {
            this.onDBSchemaDefine(event);
        };
        req.onerror = this.onError;
    }

    /**
     * @method onDBOpen
     * @description Success callback on opening the database
     * @param event Database details passed by success callback
     * @returns void
     */
    private onDBOpen(event: any): void {
        this.db = event.target.result;

        this.clearCache();
    }

    /**
     * @method onDBSchemaDefine
     * @description Success callback on opening the database
     * @param event Database details passed by success callback
     * @returns void
     */
    private onDBSchemaDefine(event: any): void {
        let store: IDBObjectStore;
        let isStoreCreated: boolean = false;

        this.db = event.target.result;

        for (let idx = 0; idx < this.db.objectStoreNames.length; idx++) {
            if (this.db.objectStoreNames[idx] === 'cache') {
                isStoreCreated = true;
                break;
            }
        }

        if (!isStoreCreated) {
            store = this.db.createObjectStore('cache', { keyPath: 'key' });

            store.createIndex('key', 'key', { unique: true });
            store.createIndex('response', 'response', { unique: false });
            store.createIndex('timestamp', 'timestamp', { unique: false });
        }
    }

    /**
     * @method onSaveComplete
     * @description Add the cachekey in the key store
     * @param event Save complete event
     * @return void
     */
    private onSaveComplete(key: string): void {
        let storedKey: string = this.ls.retrieve(CacheDBConstants.c_s_STORE_KEY_NAME) || '';

        storedKey += ',' + key;

        this.ls.store(CacheDBConstants.c_s_STORE_KEY_NAME, storedKey);
    }


    /**
     * @method hasTimedOut
     * @description Checks If The Passed Timestamp Is More Aged Than Timeout Time
     * @param recordTimestamp Timestamp For The Record
     * @returns boolean
     */
    private hasTimedOut(recordTimestamp: string): boolean {
        let currentDate: any = moment(new Date());
        let recordDate: any = moment(new Date(recordTimestamp));

        return moment.duration(currentDate.diff(recordDate)).asHours() >= CacheDBConstants.c_n_CACHE_TIMEOUT;
    }

    /**
     * @method getVersion
     * @description Returns The Version To Be Set For The DB
     * @returns number
     */
    private getVersion(): number {
        let version: number = this.ls.retrieve(CacheDBConstants.c_s_STORE_KEY_VERSION) || 0;

        this.ls.store(CacheDBConstants.c_s_STORE_KEY_VERSION, ++version);

        return version;
    }
    // #endregion

    // #region Public Methods
    /**
     * @method isCached
     * @description Check if the cache key already exists in key store
     * @param key Cache key to be set
     * @returns void
     */
    public isCached(key: string): boolean {
        let storedKey: string = this.ls.retrieve(CacheDBConstants.c_s_STORE_KEY_NAME) || '';

        return storedKey.indexOf(key) >= 0;
    }

    /**
     * @method readItem
     * @description Read for
     * @param key Key name to be saved
     * @param emitter Emitter subject for response
     * @returns void
     */
    public readItem(key: string, emitter?: Emitter): void {
        let transaction: IDBTransaction;
        let store: IDBObjectStore;
        let cursor: IDBRequest;

        transaction = this.db.transaction(['cache']);
        store = transaction.objectStore('cache');
        cursor = store.openCursor();

        cursor.onsuccess = (event) => {
            let cursor: any = event.target['result'];

            if (cursor) {
                if (cursor.value.key === key) {
                    try {
                        emitter.result.next(cursor.value.response);
                        return;
                    } catch (excp) {
                        emitter.result.next(cursor.value.response);
                        return;
                    }
                } else {
                    cursor.continue();
                }
            }
        };
        cursor.onerror = this.onError;
    }

    /**
     * @method setItem
     * @description Save item in cache
     * @param key Key name to be saved
     * @param response Key value to be saved
     * @returns void
     */
    public setItem(key: string, response: any): void {
        let transaction: IDBTransaction;
        let store: IDBObjectStore;

        transaction = this.db.transaction(['cache'], 'readwrite');
        store = transaction.objectStore('cache');

        let data: CacheDBFormat = new CacheDBFormat();
        data.key = key;
        data.response = response;
        data.timestamp = new Date();
        store.add(data);

        transaction.onerror = (event: any) => {
            console.log('Error for - ' + key, event);
        };
        transaction.oncomplete = (event: any) => {
            this.onSaveComplete(key);
        };
    }

    /**
     * @method deleteItem
     * @description Deletes key from the cache
     * @param key Key to be delete
     * @returns void
     */
    public deleteItem(key: string): void {
        let trans: IDBTransaction;
        let store: IDBObjectStore;

        trans = this.db.transaction(['cache'], 'readwrite');
        store = trans.objectStore('cache');
        store.delete(key);
    }

    /**
     * @method clearCache
     * @description Deletes all key from the cache if keys are 24 hours old
     * @returns void
     */
    public clearCache(): void {
        if (this.db) {
            let transaction: IDBTransaction;
            let store: IDBObjectStore;
            let cursor: IDBRequest;

            transaction = this.db.transaction(['cache']);
            store = transaction.objectStore('cache');
            cursor = store.openCursor();

            cursor.onsuccess = (event) => {
                let cursor: any = event.target['result'];

                if (cursor) {
                    if (this.hasTimedOut(cursor.value.timestamp)) {
                        setTimeout(() => {
                            let trans: IDBTransaction;
                            let store: IDBObjectStore;

                            trans = this.db.transaction(['cache'], 'readwrite');
                            store = trans.objectStore('cache');
                            store.clear();
                            this.factory.deleteDatabase(CacheDBConstants.c_s_DB_NAME);

                            this.ls.clear(CacheDBConstants.c_s_STORE_KEY_NAME);
                            this.ls.clear(CacheDBConstants.c_s_STORE_KEY_VERSION);
                        });
                    }
                } else {
                    this.ls.clear(CacheDBConstants.c_s_STORE_KEY_NAME);
                    this.ls.clear(CacheDBConstants.c_s_STORE_KEY_VERSION);
                }
            };
            cursor.onerror = this.onError;
        }
    }
    // #endregion
}
// #endregion
