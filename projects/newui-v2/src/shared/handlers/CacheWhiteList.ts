export class SearchRequestCacheWhiteList {
    public static readonly c_s_LIST: any = {
        'contract-management/search-api-Business/iCABSBAPICodeSearch': true,
        'service-planning/search-service-Application/iCABSARMMCategoryLanguageSearch': true,
        'service-planning/search-template-Business/iCABSBPreferredDayOfWeekReasonSearch': true,
        'service-planning/search-template-Business/iCABSCustomerAvailabilityTemplateSearch': true,
        'service-planning/search-template-Business/iCABSBClosedTemplateSearch': true,
        'it-functions/search-structure-Business/iCABSBBranchSearch': true,
        'contract-management/search-contract-admin-Application/iCABSACountryCodeSearch': true,
        'bill-to-cash/search-payment-Business/iCABSBPaymentTypeSearch': true,
        'contract-management/search-duration-Business/iCABSBContractDurationSearch': true,
        'it-functions/search-structure-Business/iCABSBCompanySearch': true,
        'ccm/search-retention-Business/iCABSBLostBusinessLanguageSearch': true,
        'ccm/search-segmentation-System/iCABSSCustomerTypeSearch': true,
        'prospect-to-contract/search-segmentation-Business/iCABSBBusinessOriginLanguageSearch': true,
        'contract-management/search-contract-admin-Business/iCABSBBusinessOriginDetailLanguageSearch': true,
        'service-planning/search-template-Business/iCABSBCalendarTemplateSearch': true,
        'service-delivery/search-service-Business/iCABSBServiceTypeSearch': true
    };
}
