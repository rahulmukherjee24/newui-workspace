import { PnolSiteReferenceMaintenanceComponent } from './ContractManagement/iCABSAPNOLSiteReferenceMaintenance.component';
import { ServiceCoverFirstVisitComponent } from './JobManagement/InvoiceOnFirstandLastVisit/iCABSAServiceCoverInvoiceOnFirstVisitMaintenance.component';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { ContractManagementModuleRoutes } from './../base/PageRoutes';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { ServiceVisitDetailSummaryComponent } from './ServiceVisitDetailSummary/iCABSAServiceVisitDetailSummary';
import { ServiceCoverFrequencyMaintenanceComponent } from './JobManagement/iCABSAServiceCoverFrequencyMaintenance';
import { YTDMaintenanceComponent } from './ContractServiceCoverMaintenance/YTDMaintenance/iCABSAServiceCoverYTDMaintenance.component';
import { TrialPeriodReleaseGridComponent } from './ContractServiceCoverMaintenance/TrialService/iCABSAServiceCoverTrialPeriodReleaseGrid.component';
import { ProductCodeUpgradeComponent } from './ContractServiceCoverMaintenance/iCABSAProductCodeUpgrade';
import { PercentagePriceChangeComponent } from './ContractManagement/PercentagePriceChange/iCABSAPercentagePriceChange.component';
import { MassPriceChangeGridComponent } from './ContractManagement/iCABSAMassPriceChangeGrid.component';
@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class ServiceCoverAdminRootComponent {
    constructor(viewContainerRef: ViewContainerRef) {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: ServiceCoverAdminRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSASERVICECOVERYTDMAINTENANCE_SUB, component: YTDMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSASERVICECOVERTRIALPERIODRELEASEGRID_SUB, component: TrialPeriodReleaseGridComponent },
                    { path: ContractManagementModuleRoutes.ICABSAPRODUCTCODEUPGRADE_SUB, component: ProductCodeUpgradeComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSASERVICECOVERINVOICEONFIRSTVISITMAINTENANCE_SUB, component: ServiceCoverFirstVisitComponent },
                    { path: ContractManagementModuleRoutes.ICABSASERVICECOVERFREQUENCYMAINTENANCE_SUB, component: ServiceCoverFrequencyMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSASERVICEVISITDETAILSUMMARY_SUB, component: ServiceVisitDetailSummaryComponent },
                    { path: ContractManagementModuleRoutes.ICABSAPERCENTAGEPRICECHANGECONTRACT_SUB, component: PercentagePriceChangeComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAPERCENTAGEPRICECHANGEPREMISE_SUB, component: PercentagePriceChangeComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAPERCENTAGEPRICECHANGEJOB_SUB, component: PercentagePriceChangeComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAMASSPRICECHANGEGRID_SUB, component: MassPriceChangeGridComponent },
                    { path: ContractManagementModuleRoutes.ICABSAPNOLSITEREFERENCEMAINTENANCE_SUB, component: PnolSiteReferenceMaintenanceComponent, canDeactivate: [RouteAwayGuardService] }
                ], data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        ServiceCoverAdminRootComponent,
        YTDMaintenanceComponent,
        TrialPeriodReleaseGridComponent,
        ProductCodeUpgradeComponent,
        ServiceCoverFirstVisitComponent,
        ServiceVisitDetailSummaryComponent,
        ServiceCoverFrequencyMaintenanceComponent,
        PercentagePriceChangeComponent,
        MassPriceChangeGridComponent,
        PnolSiteReferenceMaintenanceComponent
    ],
    exports: [
        ServiceCoverFirstVisitComponent,
        ServiceVisitDetailSummaryComponent,
        ServiceCoverFrequencyMaintenanceComponent,
        PercentagePriceChangeComponent,
        MassPriceChangeGridComponent
    ],
    entryComponents: [
        ServiceCoverFrequencyMaintenanceComponent,
        MassPriceChangeGridComponent
    ]
})

export class ServiceCoverAdminModule { }
