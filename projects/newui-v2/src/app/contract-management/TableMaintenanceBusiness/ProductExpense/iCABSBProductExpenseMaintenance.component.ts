import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';

import { PageIdentifier } from './../../../base/PageIdentifier';
import { BaseComponent } from '../../../../app/base/BaseComponent';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { MessageConstant } from '../../../../shared/constants/message.constant';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { EllipsisComponent } from '../../../../shared/components/ellipsis/ellipsis';
import { AjaxConstant } from '../../../../shared/constants/AjaxConstants';
import { EllipsisGenericComponent } from '@shared/components/ellipsis-generic/ellipsis-generic';
import { ProductSearchGridComponent } from '../../../internal/search/iCABSBProductSearch';
import { ExpenseCodeSearchComponent } from '../../../internal/search/iCABSBExpenseCodeSearch.component';
import { ProductExpenseSearchComponent } from '../../../internal/search/iCABSBProductExpenseSearch';

@Component({
    templateUrl: 'iCABSBProductExpenseMaintenance.html'
})

export class ProductExpenseMaintenanceComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('ProductExpenseMaintenanceEllipsis') public ProductExpenseMaintenanceEllipsis: EllipsisComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('promptConfirmModal') public promptConfirmModal;
    @ViewChild('promptConfirmModalDelete') public promptConfirmModalDelete;
    @ViewChild('TaxCodeEllipsis') TaxCodeEllipsis: EllipsisGenericComponent;

    private queryParams: any = {
        operation: 'Business/iCABSBProductExpenseMaintenance',
        module: 'contract-admin',
        method: 'contract-management/admin'
    };

    public pageId: string = '';
    public promptConfirmContent: any;
    public productSearchGridComponent: any = ProductSearchGridComponent;
    public isvSCEnableTaxCodeDefaulting: boolean = false;
    public isvbMultipleTaxRates: boolean = false;
    public isVisibilty: boolean = false;
    public inputParamsExpenseCodeSearch: any = { 'parentMode': 'LookUp' };
    public expenseCodeSearchComponent = ExpenseCodeSearchComponent;

    public controls = [
        { name: 'ProductCode', required: true, type: MntConst.eTypeCode },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractTypeCode' },
        { name: 'ContractTypeDesc' },
        { name: 'ExpenseCode', required: true, type: MntConst.eTypeCode },
        { name: 'ExpenseDesc', disabled: true },
        { name: 'InitialChargeExpenseCode', type: MntConst.eTypeCode },
        { name: 'InitialChargeExpenseDesc', disabled: true },
        { name: 'InstallationChargeExpenseCode', type: MntConst.eTypeCode },
        { name: 'InstallationChargeExpenseDesc', disabled: true },
        { name: 'RemovalChargeExpenseCode', type: MntConst.eTypeCode },
        { name: 'RemovalChargeExpenseDesc', disabled: true },
        { name: 'MaterialsExpenseCode', type: MntConst.eTypeCode },
        { name: 'MaterialsExpenseDesc', disabled: true },
        { name: 'LabourExpenseCode', type: MntConst.eTypeCode },
        { name: 'LabourExpenseDesc', disabled: true },
        { name: 'ReplacementExpenseCode', type: MntConst.eTypeCode },
        { name: 'ReplacementExpenseDesc', disabled: true },
        { name: 'TaxCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'TaxCodeDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ROWID' }
    ];

    public ellipsisQueryParams: any = {
        productExpense: {
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'Search',
                'showAddNew': true,
                'productCode': '',
                'productDesc': ''
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: ProductExpenseSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        },
        productSearch: {
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp',
                'showAddNew': true
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: ProductSearchGridComponent,
            hideIcon: true,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        },
        TaxCodeSearchConfig: {
            autoOpen: false,
            ellipsisTitle: 'Tax Code Search',
            configParams: {
                table: '',
                shouldShowAdd: true,
                parentMode: 'LookUp'
            },
            httpConfig: {
                operation: 'System/iCABSSTaxCodeSearch',
                module: 'tax',
                method: 'bill-to-cash/search'
            },
            tableColumn: [
                { title: 'Tax Code', name: 'TaxCode', type: MntConst.eTypeCode },
                { title: 'Description', name: 'TaxCodeDesc', type: MntConst.eTypeText },
                { title: 'System Default', name: 'TaxCodeDefaultInd', type: MntConst.eTypeCheckBox }
            ],
            disable: false
        },
        expenseCode: {
            childConfigParams: {
                parentMode: 'LookUp'
            }
        }
    };

    public dropdown: any = {
        contractTypeSearch: {
            isRequired: true,
            isDisabled: false,
            isTriggerValidate: false,
            isActive: {
                id: '',
                text: ''
            },
            params: {
                method: 'contract-management/search',
                module: 'contract',
                operation: 'Business/iCABSBContractTypeSearch'
            },
            displayFields: ['ContractTypeCode', 'ContractTypeDesc']
        }
    };

    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBPRODUCTEXPENSEMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Product Expense Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.getSysCharDtetails();
        this.riMaintenance.FunctionDelete = false;
        this.riMaintenance.FunctionSelect = true;
        this.pageParams.isvSCEnableTaxCodeDefaulting = false;
        this.pageParams.isvbMultipleTaxRates = false;

    }
    ngAfterViewInit(): void {
        this.ellipsisQueryParams.productExpense.autoOpenSearch = true;
        this.ellipsisQueryParams.productSearch.disabled = false;
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    //SysChar call
    private getSysCharDtetails(): void {
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharMultipleTaxRates,
            this.sysCharConstants.SystemCharDefaultTaxCodeOnServiceCoverMaint

        ];
        let sysCharIP = {
            module: this.queryParams.module,
            operation: this.queryParams.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };
        this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            this.pageParams.vMultipleTaxRates = data.records[0]['Required'];
            if (this.pageParams.vMultipleTaxRates) {
                this.pageParams.isvbMultipleTaxRates = true;
                this.isVisibilty = true;
            }
            this.pageParams.vDefaultTaxCode = data.records[1]['Text'];
            if (this.pageParams.vDefaultTaxCode.indexOf('6') > -1) {
                this.pageParams.isvSCEnableTaxCodeDefaulting = true;
            }
        });
    }

    //add mode
    private addProductExpense(): void {
        this.riMaintenance.FunctionDelete = true;
        this.pageParams.mode = 'add';
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '1');
        let data: any = {
            'ProductCode': this.getControlValue('ProductCode'),
            'ContractTypeCode': this.getControlValue('ContractTypeCode'),
            'ExpenseCode': this.getControlValue('ExpenseCode'),
            'InitialChargeExpenseCode': this.getControlValue('InitialChargeExpenseCode'),
            'InstallationChargeExpenseCode': this.getControlValue('InstallationChargeExpenseCode'),
            'RemovalChargeExpenseCode': this.getControlValue('RemovalChargeExpenseCode'),
            'MaterialsExpenseCode': this.getControlValue('MaterialsExpenseCode'),
            'LabourExpenseCode': this.getControlValue('LabourExpenseCode'),
            'ReplacementExpenseCode': this.getControlValue('ReplacementExpenseCode'),
            'TaxCode': this.getControlValue('TaxCode')
        };
        this.ajaxSource.next(AjaxConstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, data)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(AjaxConstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                        this.setControlValue('ROWID', data['ttProductExpense']);
                        this.pageParams.mode = 'update';
                        this.dropdown.contractTypeSearch.isDisabled = true;
                        this.disableControl('ProductCode', true);
                        this.ellipsisQueryParams.productSearch.hideIcon = true;
                        this.ellipsisQueryParams.productExpense.hideIcon = false;
                        this.riMaintenance.FunctionSelect = false;
                        this.formPristine();
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                });
    }
    //delete mode
    private deleteProductExpense(data: any): void {
        this.pageParams.mode = 'delete';
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '3');
        this.ajaxSource.next(AjaxConstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, data)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(AjaxConstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        let promptVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordDeleted);
                        promptVO.closeCallback = this.closeModal.bind(this);
                        this.modalAdvService.emitMessage(promptVO);
                        this.riExchange.resetCtrl(this.controls);
                        this.riExchange.renderForm(this.uiForm, this.controls);
                        this.uiForm.reset();
                        this.disableControl('ProductCode', false);
                        this.disableControl('TaxCode', true);
                        this.setControlValue('ROWID', '');
                        this.ellipsisQueryParams.productSearch.disabled = false;
                        this.ellipsisQueryParams.TaxCodeSearchConfig.disable = true;
                        this.riMaintenance.FunctionSelect = true;
                        this.riMaintenance.FunctionDelete = false;
                        this.riMaintenance.FunctionSearch = false;
                        this.resetDropdown();
                        this.resetProductCodeParams();
                        this.dropdown.contractTypeSearch.isActive = { id: '', text: '' };
                        this.dropdown.contractTypeSearch.isDisabled = false;
                        this.pageParams.mode = 'select';
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(error);
                });
    }
    private lookupDetailsTaxCode(query: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(query).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data[0].length > 0) {
                this.setControlValue('TaxCodeDesc', data[0][0].TaxCodeDesc);
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'TaxCode', false);
                this.uiForm.controls['TaxCode'].updateValueAndValidity();
            } else if (data[0].length <= 0) {
                this.uiForm.controls['TaxCode'].setErrors({});
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'TaxCode', true);
                this.setControlValue('TaxCodeDesc', '');
            }
            else if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.recordNotFound));
                this.setControlValue('TaxCode', '');
                this.setControlValue('TaxCodeDesc', '');
            }
        }).catch(() => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
        });
    }
    private lookupDetail(query: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(query).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data[0].length) {
                this.setControlValue('ProductDesc', data[0][0].ProductDesc);
                if (this.pageParams.mode !== 'update') {
                    this.ellipsisQueryParams.productExpense.childConfigParams.productCode = this.getControlValue('ProductCode');
                    this.ellipsisQueryParams.productExpense.childConfigParams.productDesc = this.getControlValue('ProductDesc');
                }
            }
            else {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                this.setControlValue('ProductDesc', '');
                this.setControlValue('ProductCode', '');
            }
        }).catch(() => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
        });
    }

    private resetDropdown(): void {
        for (let key in this.dropdown) {
            if (this.dropdown.hasOwnProperty(key)) {
                this.dropdown[key].isDisabled = true;
                this.dropdown[key].active = { id: '', text: '' };
            }
        }
    }
    public closeModal(): void {
        this.ellipsisQueryParams.productExpense.autoOpenSearch = true;
    }

    public productDetailsOnkeydown(data: any): void {
        this.setControlValue('ProductCode', data.ProductCode);
        this.setControlValue('ProductDesc', data.ProductDesc);
        this.ellipsisQueryParams.productExpense.childConfigParams.productCode = this.getControlValue('ProductCode');
        this.ellipsisQueryParams.productExpense.childConfigParams.productDesc = this.getControlValue('ProductDesc');
        if (this.pageParams.mode === 'add') {
            this.uiForm.controls['ProductCode'].markAsDirty();
            this.resetProductCodeParams();
        } else {
            this.uiForm.controls['ProductCode'].markAsPristine();
        }
    }

    public onChangeproductExpense(): void {
        let gridParams: QueryParams = this.getURLSearchParamObject();
        gridParams.set(this.serviceConstants.Action, '0');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridParams).subscribe(
            () => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                let lookupIP: any = [
                    {
                        'table': 'ProductExpense',
                        'query': {
                            'ProductCode': this.getControlValue('ProductCode')
                        },
                        'fields': ['ProductDesc']
                    }];
                this.lookupDetailProductExpense(lookupIP);
            },
            () => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }
    public lookupDetailProductExpense(query: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(query).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data[0].length) {
                this.setControlValue('ProductDesc', data[0][0].ProductDesc);
            }
        }).catch(() => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
        });
    }

    public onChangeProductDetails(): void {
        if (this.pageParams.mode === 'add') {
            this.uiForm.controls['ProductCode'].markAsDirty();
        } else {
            this.uiForm.controls['ProductCode'].markAsPristine();
        }
        let gridParams: QueryParams = this.getURLSearchParamObject();
        gridParams.set(this.serviceConstants.Action, '0');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridParams).subscribe(
            () => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                let lookupIP: any = [
                    {
                        'table': 'Product',
                        'query': {
                            'ProductCode': this.getControlValue('ProductCode')
                        },
                        'fields': ['ProductDesc']
                    }];
                this.lookupDetail(lookupIP);
            },
            () => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
            });
    }

    public onChangeexpenseCode(): void {
        let gridParams: QueryParams = this.getURLSearchParamObject();
        gridParams.set(this.serviceConstants.Action, '0');
        gridParams.set('ProductCode', this.getControlValue('ProductCode'));
        gridParams.set('ContractTypeCode', this.getControlValue('ContractTypeCode'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                    return;
                }
                this.setControlValue('ExpenseCode', data['ExpenseCode']);
                this.setControlValue('InitialChargeExpenseCode', data['InitialChargeExpenseCode']);
                this.setControlValue('InstallationChargeExpenseCode', data['InstallationChargeExpenseCode']);
                this.setControlValue('RemovalChargeExpenseCode', data['RemovalChargeExpenseCode']);
                this.setControlValue('MaterialsExpenseCode', data['MaterialsExpenseCode']);
                this.setControlValue('LabourExpenseCode', data['LabourExpenseCode']);
                this.setControlValue('ReplacementExpenseCode', data['ReplacementExpenseCode']);
                this.setControlValue('TaxCode', data['TaxCode']);
                this.setControlValue('ROWID', data['ttProductExpense']);
                this.onChangeTaxCode();
                this.formPristine();
                let lookupIP: any = [
                    {
                        'table': 'ExpenseCode',
                        'query': {
                            'ExpenseCode': this.getControlValue('InitialChargeExpenseCode')
                        },
                        'fields': ['ExpenseDesc']
                    },
                    {
                        'table': 'ExpenseCode',
                        'query': {
                            'ExpenseCode': this.getControlValue('InstallationChargeExpenseCode')
                        },
                        'fields': ['ExpenseDesc']
                    },
                    {
                        'table': 'ExpenseCode',
                        'query': {
                            'ExpenseCode': this.getControlValue('RemovalChargeExpenseCode')
                        },
                        'fields': ['ExpenseDesc']
                    },
                    {
                        'table': 'ExpenseCode',
                        'query': {
                            'ExpenseCode': this.getControlValue('ReplacementExpenseCode')
                        },
                        'fields': ['ExpenseDesc']
                    },
                    {
                        'table': 'ExpenseCode',
                        'query': {
                            'ExpenseCode': this.getControlValue('LabourExpenseCode')
                        },
                        'fields': ['ExpenseDesc']
                    },
                    {
                        'table': 'ContractType',
                        'query': {
                            'ContractTypeCode': this.getControlValue('ContractTypeCode')
                        },
                        'fields': ['ContractTypeDesc']
                    },
                    {
                        'table': 'ExpenseCode',
                        'query': {
                            'ExpenseCode': this.getControlValue('RemovalChargeExpenseCode')
                        },
                        'fields': ['ExpenseDesc']
                    },
                    {
                        'table': 'ExpenseCode',
                        'query': {
                            'ExpenseCode': this.getControlValue('ExpenseCode')
                        },
                        'fields': ['ExpenseDesc']
                    },
                    {
                        'table': 'ExpenseCode',
                        'query': {
                            'ExpenseCode': this.getControlValue('MaterialsExpenseCode')
                        },
                        'fields': ['ExpenseDesc']
                    }];
                this.lookupDetailsExpense(lookupIP);
            },
            () => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
            });

    }
    public lookupDetailsExpense(query: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(query).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.errorMessage) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            }
            else if (data.length) {
                for (let key in this.dropdown) {
                    if (this.dropdown.hasOwnProperty(key)) {
                        this.dropdown[key].isDisabled = false;
                    }
                }
                this.disableControl('ProductCode', true);
                this.disableControl('TaxCode', false);
                this.ellipsisQueryParams.productSearch.disabled = true;
                this.ellipsisQueryParams.taxCode.disabled = false;
                this.dropdown.contractTypeSearch.isDisabled = true;
                this.riMaintenance.FunctionSelect = false;
                this.riMaintenance.FunctionDelete = true;
                this.pageParams.mode = 'update';
                if (data[0].length) {
                    this.setControlValue('InitialChargeExpenseDesc', data[0][0].ExpenseDesc);
                } else {
                    this.setControlValue('InitialChargeExpenseDesc', '');
                }
                if (data[1].length) {
                    this.setControlValue('InstallationChargeExpenseDesc', data[1][0].ExpenseDesc);
                } else {
                    this.setControlValue('InstallationChargeExpenseDesc', '');
                }
                if (data[2].length) {
                    this.setControlValue('removalExpenseCodeDesc', data[2][0].ExpenseDesc);
                } else {
                    this.setControlValue('removalExpenseCodeDesc', '');
                }
                if (data[3].length) {
                    this.setControlValue('ReplacementExpenseDesc', data[3][0].ExpenseDesc0);
                } else {
                    this.setControlValue('ReplacementExpenseDesc', '');
                }
                if (data[4].length) {
                    this.setControlValue('LabourExpenseDesc', data[4][0].ExpenseDesc);
                } else {
                    this.setControlValue('LabourExpenseDesc', '');
                }
                if (data[5].length) {
                    this.dropdown.contractTypeSearch.active = {
                        id: this.getControlValue('ContractTypeCode'),
                        text: this.getControlValue('ContractTypeCode') + ' - ' + data[5][0].ExpenseDesc
                    };
                }
                else {
                    this.dropdown.contractTypeSearch.active = {
                        id: '',
                        text: ''
                    };
                }
                if (data[6].length) {
                    this.setControlValue('RemovalChargeExpenseDesc', data[6][0].ExpenseDesc);
                } else {
                    this.setControlValue('RemovalChargeExpenseDesc', '');
                }
                if (data[7].length) {
                    this.setControlValue('ExpenseDesc', data[7][0].ExpenseDesc);
                } else {
                    this.setControlValue('ExpenseDesc', '');
                }
                if (data[8].length) {
                    this.setControlValue('MaterialsExpenseDesc', data[8][0].ExpenseDesc);
                } else {
                    this.setControlValue('MaterialsExpenseDesc', '');
                }
                this.pageParams.mode = 'update';
            }
        }).catch(() => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
        });
    }
    public onChangeTaxCode(): void {
        let gridParams: QueryParams = this.getURLSearchParamObject();
        gridParams.set(this.serviceConstants.Action, '0');
        if (this.getControlValue('TaxCode')) {
            let lookupIP: any = [
                {
                    'table': 'TaxCode',
                    'query': {
                        'TaxCode': this.getControlValue('TaxCode')
                    },
                    'fields': ['TaxCodeDesc']
                }];
            this.lookupDetailsTaxCode(lookupIP);
        } else {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'TaxCode', false);
            this.uiForm.controls['TaxCode'].updateValueAndValidity();
            this.setControlValue('TaxCodeDesc', '');
        }

    }

    public onChangecontractTypeCode(): void {
        let gridParams: QueryParams = this.getURLSearchParamObject();
        gridParams.set(this.serviceConstants.Action, '0');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridParams).subscribe(
            () => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                let lookupIP: any = [
                    {
                        'table': 'ContractType',
                        'query': {
                            'ContractTypeCode': this.getControlValue('ContractTypeCode')
                        },
                        'fields': ['ContractTypeDesc']
                    }];
                this.lookupQueryDetails(lookupIP);
            },
            () => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }
    public lookupQueryDetails(query: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(query).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data[0].length) {
                this.setControlValue('ContractTypeDesc', data[0][0].ContractTypeDesc);
            }
        }).catch(() => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
        });
    }
    public saveData(): void {
        if (this.uiForm.valid) {
            if (this.pageParams.mode === 'add') {
                this.promptConfirmContent = MessageConstant.Message.ConfirmRecord;
                this.promptConfirmModal.show();
            }
            else if (this.pageParams.mode === 'update') {
                this.riMaintenance.FunctionDelete = true;
                this.ellipsisQueryParams.productSearch.disabled = true;
                this.promptConfirmContent = MessageConstant.Message.ConfirmRecord;
                this.promptConfirmModal.show();
            }
        }
        else {
            this.dropdown.contractTypeSearch.isTriggerValidate = true;
        }
    }

    //update mode
    public updateProductExpense(data: any): void {
        this.pageParams.mode = 'update';
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set('ROWID', this.getControlValue('ROWID'));
        searchParams.set('ProductCode', this.getControlValue('ProductCode'));
        searchParams.set('ContractTypeCode', this.getControlValue('ContractTypeCode'));
        searchParams.set('ExpenseCode', this.getControlValue('ExpenseCode'));
        searchParams.set('InitialChargeExpenseCode', this.getControlValue('InitialChargeExpenseCode'));
        searchParams.set('InstallationChargeExpenseCode', this.getControlValue('InstallationChargeExpenseCode'));
        searchParams.set('RemovalChargeExpenseCode', this.getControlValue('RemovalChargeExpenseCode'));
        searchParams.set('MaterialsExpenseCode', this.getControlValue('MaterialsExpenseCode'));
        searchParams.set('LabourExpenseCode', this.getControlValue('LabourExpenseCode'));
        searchParams.set('ReplacementExpenseCode', this.getControlValue('ReplacementExpenseCode'));
        searchParams.set('TaxCode', this.getControlValue('TaxCode'));
        this.ajaxSource.next(AjaxConstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, data)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(AjaxConstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    }
                    else {
                        this.setControlValue('ROWID', this.getControlValue('ROWID'));
                        this.formPristine();
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error));
                });
    }
    public deleteData(): void {
        this.riMaintenance.FunctionDelete = true;
        this.promptConfirmContent = MessageConstant.Message.DeleteRecord;
        this.promptConfirmModalDelete.show();
    }

    public promptConfirm(type: any): void {
        let formdData: any = {};
        formdData['ROWID'] = this.getControlValue('ROWID');
        switch (type) {
            case 'save':
                if (this.pageParams.mode === 'update') {
                    this.updateProductExpense(formdData);
                } else {
                    this.addProductExpense();
                }
                break;
            case 'delete':
                this.deleteProductExpense(formdData);
                break;
            default:
                break;
        }
    }
    public search(): void {
        this.formPristine();
        if (this.pageParams.mode === 'add') {
            this.cancel();
            this.pageParams.mode = 'select';
            this.riMaintenance.FunctionSelect = true;
        } else if (this.pageParams.mode === 'update') {
            this.resetProductCodeParams();
            this.ProductExpenseMaintenanceEllipsis.openModal();
        } else {
            if (!this.getControlValue('ProductCode')) {
                this.resetProductCodeParams();
            }
            this.ProductExpenseMaintenanceEllipsis.openModal();
        }
    }

    public resetProductCodeParams(): void {
        this.ellipsisQueryParams.productExpense.childConfigParams.productCode = '';
        this.ellipsisQueryParams.productExpense.childConfigParams.productDesc = '';
    }

    public cancel(): void {
        this.formPristine();
        if (this.pageParams.mode === 'update') {
            this.onChangeexpenseCode();
            this.ellipsisQueryParams.productSearch.hideIcon = true;
            this.ellipsisQueryParams.productExpense.hideIcon = false;
            this.ProductExpenseMaintenanceEllipsis.openModal();
            return;
        }
        this.riExchange.resetCtrl(this.controls);
        this.riExchange.renderForm(this.uiForm, this.controls);
        this.riMaintenance.FunctionDelete = false;
        this.resetDropdown();
        if (this.pageParams.mode === 'add') {
            this.dropdown.contractTypeSearch.isActive = { id: '', text: '' };
            this.dropdown.contractTypeSearch.isDisabled = false;
            this.ellipsisQueryParams.taxCode.disabled = true;
            this.riMaintenance['FunctionSelect'] = true;
        }
        this.resetProductCodeParams();
        this.ProductExpenseMaintenanceEllipsis.openModal();
        this.pageParams.mode = 'select';
    }
    public productSearchOnchange(data: any): void {
        this.ellipsisQueryParams.productExpense.autoOpenSearch = false;
        for (let key in this.dropdown) {
            if (this.dropdown.hasOwnProperty(key)) {
                this.dropdown[key].isDisabled = false;
            }
        }
        for (let control in this.uiForm.controls) {
            if (control) {
                this.uiForm.controls[control].setValue('');
            }
        }
        if (data.mode === 'add') {
            this.pageParams.mode = 'add';
            for (let key in this.dropdown) {
                if (this.dropdown.hasOwnProperty(key)) {
                    this.dropdown[key].active = { id: '', text: '' };
                }
            }
            this.riMaintenance.FunctionDelete = false;
            this.riMaintenance.FunctionSelect = false;
            for (let key in this.dropdown) {
                if (this.dropdown.hasOwnProperty(key)) {
                    this.dropdown[key].isDisabled = false;
                }
            }
            this.ellipsisQueryParams.productSearch.disabled = false;
            this.ellipsisQueryParams.TaxCodeSearchConfig.disable = false;
            this.ellipsisQueryParams.productExpense.hideIcon = true;
            this.ellipsisQueryParams.productSearch.hideIcon = false;
            this.dropdown.contractTypeSearch.isActive = { id: '', text: '' };

            this.riExchange.resetCtrl(this.controls);
            this.riExchange.renderForm(this.uiForm, this.controls);
            this.disableControl('TaxCode', false);
            this.disableControl('ProductCode', false);
        }
        else {
            this.pageParams.mode = 'update';
            this.dropdown.contractTypeSearch.isDisabled = true;
            this.dropdown.contractTypeSearch.isActive = {
                id: data.ExpenseCode,
                text: data.ContractTypeCode + ' - ' + data.ContractTypeDesc
            };
            this.setControlValue('ExpenseCode', data.ExpenseCode);
            this.setControlValue('ProductCode', data.ProductCode);
            this.setControlValue('ContractTypeCode', data.ContractTypeCode);
            this.setControlValue('ROWID', data.ttProductExpense);
            this.onChangeProductDetails();
            this.onChangeexpenseCode();
            this.onChangeTaxCode();
            this.disableControl('ProductCode', true);
            this.ellipsisQueryParams.TaxCodeSearchConfig.disable = false;
            this.riMaintenance.FunctionDelete = true;
            this.riMaintenance.FunctionSelect = false;
            this.ellipsisQueryParams.productExpense.hideIcon = false;
            this.ellipsisQueryParams.productSearch.hideIcon = true;
        }
    }
    public onExpenseCodeReceived(data: any): void {
        this.setControlValue('ExpenseCode', data.ExpenseCode);
        this.setControlValue('ExpenseDesc', data.ExpenseDesc);
        this.uiForm.controls['ExpenseCode'].markAsDirty();
        this.uiForm.controls['ExpenseDesc'].markAsDirty();
    }
    public onDefaultTaxChange(data: any): void {
        this.setControlValue('TaxCode', data.TaxCode);
        this.setControlValue('TaxCodeDesc', data.TaxCodeDesc);
        this.uiForm.controls['TaxCode'].markAsDirty();
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'TaxCode', false);
        this.uiForm.controls['TaxCode'].updateValueAndValidity();
    }
    public onInitialExpenseCodeReceived(data: any): void {
        this.setControlValue('InitialChargeExpenseCode', data.ExpenseCode);
        this.setControlValue('InitialChargeExpenseDesc', data.ExpenseDesc);
        this.uiForm.controls['InitialChargeExpenseCode'].markAsDirty();
        this.uiForm.controls['InitialChargeExpenseDesc'].markAsDirty();
    }
    public onInstallationExpenseCodeReceived(data: any): void {
        this.setControlValue('InstallationChargeExpenseCode', data.ExpenseCode);
        this.setControlValue('InstallationChargeExpenseDesc', data.ExpenseDesc);
        this.uiForm.controls['InstallationChargeExpenseCode'].markAsDirty();
        this.uiForm.controls['InstallationChargeExpenseDesc'].markAsDirty();
    }
    public onRemovalExpenseCodeReceived(data: any): void {
        this.setControlValue('RemovalChargeExpenseCode', data.ExpenseCode);
        this.setControlValue('RemovalChargeExpenseDesc', data.ExpenseDesc);
        this.uiForm.controls['RemovalChargeExpenseCode'].markAsDirty();
        this.uiForm.controls['RemovalChargeExpenseDesc'].markAsDirty();
    }
    public onMaterialsExpenseCodeReceived(data: any): void {
        this.setControlValue('MaterialsExpenseCode', data.ExpenseCode);
        this.setControlValue('MaterialsExpenseDesc', data.ExpenseDesc);
        this.uiForm.controls['MaterialsExpenseCode'].markAsDirty();
        this.uiForm.controls['MaterialsExpenseDesc'].markAsDirty();
    }
    public onLabourExpenseCodeReceived(data: any): void {
        this.setControlValue('LabourExpenseCode', data.ExpenseCode);
        this.setControlValue('LabourExpenseDesc', data.ExpenseDesc);
        this.uiForm.controls['LabourExpenseCode'].markAsDirty();
        this.uiForm.controls['LabourExpenseDesc'].markAsDirty();
    }
    public onReplacementExpenseCodeReceived(data: any): void {
        this.setControlValue('ReplacementExpenseCode', data.ExpenseCode);
        this.setControlValue('ReplacementExpenseDesc', data.ExpenseDesc);
        this.uiForm.controls['ReplacementExpenseCode'].markAsDirty();
        this.uiForm.controls['ReplacementExpenseDesc'].markAsDirty();
    }
    public onContractTypeDataReceive(data: any): void {
        this.setControlValue('ContractTypeCode', data.ContractTypeCode);
        if (this.pageParams.mode === 'add') {
            this.uiForm.controls['ContractTypeCode'].markAsDirty();
        } else {
            this.uiForm.controls['ContractTypeCode'].markAsPristine();
            if (data.ContractTypeCode) {
                this.onChangeexpenseCode();
            }
        }
    }
}
