import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AccountAssignComponent } from './AccountAssign/iCABSAAccountAssign.component';
import { AccountMergeComponent } from './AccountMerge/iCABSAAccountMerge.component';
import { AccountMoveComponent } from './AccountMove/iCABSAAccountMove.component';
import { ContractManagementModuleRoutes } from '@base/PageRoutes';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { RouteAwayGuardService } from '@shared/services/route-away-guard.service';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { SharedModule } from '@shared/shared.module';

@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class AccountAdminRootComponent {
    constructor(viewContainerRef: ViewContainerRef) {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchEllipsisModule,
        InternalSearchModule,
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: AccountAdminRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSAACCOUNTASSIGN_SUB, component: AccountAssignComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAACCOUNTASSIGN_SUB, component: AccountAssignComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAACCOUNTMERGE_SUB, component: AccountMergeComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAACCOUNTMERGE_SUB, component: AccountMergeComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAACCOUNTMOVE_SUB, component: AccountMoveComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAACCOUNTMOVE_SUB, component: AccountMoveComponent, canDeactivate: [RouteAwayGuardService] }
            ], data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        AccountAdminRootComponent,
        AccountAssignComponent,
        AccountMergeComponent,
        AccountMoveComponent
    ]
})

export class AccountAdminModule { }
