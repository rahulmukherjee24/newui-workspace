import { InternalSearchModuleRoutes } from './../../base/PageRoutes';
import { Subscription } from 'rxjs';
import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../base/BaseComponent';
import { Component, Injector, OnInit, OnDestroy, AfterContentInit } from '@angular/core';
import { ContractSearchComponent } from '../../internal/search/iCABSAContractSearch';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { QueryParams } from '../../../shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSAContractSelectMaintenance.html'
})

export class ContractSelectMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    public pageId: string = '';

    public controls = [
        { name: 'ContractNumber', disabled: false, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'AccountNumber', disabled: true, type: MntConst.eTypeText },
        { name: 'Status', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractCommenceDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'NegBranchNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'ContractAnnualValue', disabled: true, type: MntConst.eTypeCurrency },
        { name: 'CurrentPremises', disabled: true, type: MntConst.eTypeInteger },
        { name: 'InvoiceAnnivDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'InvoiceFrequencyCode', disabled: true, type: MntConst.eTypeInteger },
        { name: 'LostBusinessNoticePeriod', disabled: true, type: MntConst.eTypeInteger }
    ];

    public queryParams: any = {
        operation: 'Application/iCABSAContractSelectMaintenance',
        module: 'contract',
        method: 'contract-management/maintenance'
    };

    public contractSearchParams: any = {
        'parentMode': 'LookUp',
        'showAddNew': false,
        'currentContractType': 'C',
        'currentContractTypeURLParameter': '<contract>'
    };

    public lookUpSubscription: Subscription;
    public showCloseButton: boolean = true;
    public contractSearchComponent = ContractSearchComponent;
    public isContractEllipsisDisabled: boolean = false;
    public viewTypesArray: Array<any>;
    public autoOpenSearch: boolean;
    public showHeader: boolean = true;
    public pending: boolean;
    public ismenudisabled: boolean = true;

    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public lookupObj = {
        table: 'Contract',
        fields: ['ContractName', 'ContractCommenceDate', 'ContractAnnualValue', 'CurrentPremises', 'AccountNumber', 'NegBranchNumber', 'InvoiceAnnivDate',
            'InvoiceFrequencyCode', 'LostBusinessNoticePeriod']
    };
    public trigger;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSACONTRACTSELECTMAINTENANCE;
        this.contractSearchComponent = ContractSearchComponent;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageTitle = 'Contract Select';
        this.windowOnLoad();
    }

    ngAfterContentInit(): void {
        if (this.getControlValue('ContractNumber').trim() !== '') {
            this.autoOpenSearch = false;
            this.doLookupformData();
            this.trigger = this.utils.randomSixDigitString();
        } else {
            this.autoOpenSearch = true;
        }
    }

    public windowOnLoad(): void {
        if (this.parentMode === 'CallCentreSearchNew' || this.parentMode === 'ContactManagement') {
            this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
            this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
            this.doLookupformData();
            this.trigger = this.utils.randomSixDigitString();
        }
        if (this.riExchange.getRouterParams()['fromMenu']) {
            this.pending = true;
            this.viewTypesArray = [
                { text: 'Options', value: '' },
                { text: 'Contract', value: 'Contract' },
                { text: 'Request', value: 'Request' }];
        }
        else {
            this.viewTypesArray = [
                { text: 'Options', value: '' },
                { text: 'Request', value: 'Request' }];
        }
    }

    public doLookupformData(): void {
        this.ajaxSource.next(this.ajaxconstant.START);

        const searchParams: QueryParams = new QueryParams();
        searchParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        searchParams.set(this.serviceConstants.Action, '6');
        searchParams.set('ContractNumber', this.riExchange.riInputElement.GetValue(this.uiForm, 'ContractNumber'));

        const bodyParams: any = {};
        bodyParams['Function'] = 'GetStatus';

        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                if (data.ErrorMessageDesc !== '') {
                    this.ismenudisabled = true;
                    this.globalNotifications.displayMessage(data.errorMessage);
                    document.getElementById('ContractNumber').focus();
                    if (data.Status !== '') {
                        this.ismenudisabled = false;
                        this.riExchange.riInputElement.SetValue(this.uiForm, 'Status', data.Status);
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                }
                else {
                    this.ismenudisabled = false;
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'Status', data.Status);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                }
            });
    }

    public onContractDataReceived(data: any): void {
        this.setControlValue('ContractNumber', data.ContractNumber);
        this.trigger = this.utils.randomSixDigitString();
        this.doLookupformData();
        this.setFormMode('update');
    }

    public onContractChange(): any {
        if (this.getControlValue('ContractNumber').trim() === '') {
            this.uiForm.reset();
        } else {
            this.doLookupformData();
        }
    }

    public onDataChanged(data: any): void {
        this.doLookupformData();
    }

    public onViewTypeCodeChange(viewType: any): void {
        if (viewType === 'Contract') {
            this.parentMode = 'Request';
            this.navigate('Request', this.ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                parentMode: 'Request',
                ContractNumber: this.riExchange.riInputElement.GetValue(this.uiForm, 'ContractNumber'),
                ContractName: this.riExchange.riInputElement.GetValue(this.uiForm, 'ContractName')
            });
        }
        else if (viewType === 'Request') {
            if (this.pending || this.parentMode === 'CallCentreSearchNew') {
                this.parentMode = 'Contract';
                if (this.riExchange.URLParameterContains('CurrentOnly')) {
                    this.navigate('Contract', InternalSearchModuleRoutes.ICABSALOSTBUSINESSREQUESTSEARCH, {
                        'CurrentOnly': true
                    });
                }
                else if (this.parentMode === 'ContactManagement') {
                    this.navigate('Contract', InternalSearchModuleRoutes.ICABSALOSTBUSINESSREQUESTSEARCH, {
                        'ContactManagement': true
                    });
                }
                else {
                    this.navigate('Contract', InternalSearchModuleRoutes.ICABSALOSTBUSINESSREQUESTSEARCH);
                }
            }
        }
    }

    ngOnDestroy(): void {
        if (this.lookUpSubscription) {
            this.lookUpSubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }

}
