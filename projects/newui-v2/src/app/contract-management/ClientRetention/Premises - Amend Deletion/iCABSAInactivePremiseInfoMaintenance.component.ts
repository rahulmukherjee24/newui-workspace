import { Component, OnInit, Injector, OnDestroy, AfterContentInit, ViewChild } from '@angular/core';
import { BaseComponent } from './../../../base/BaseComponent';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { ContractManagementModuleRoutes, InternalMaintenanceApplicationModuleRoutes } from './../../../base/PageRoutes';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { ErrorConstant } from './../../../../shared/constants/error.constant';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { ContractSearchComponent } from './../../../internal/search/iCABSAContractSearch';
import { PremiseSearchComponent } from './../../../internal/search/iCABSAPremiseSearch';
import { EmployeeSearchComponent } from './../../../internal/search/iCABSBEmployeeSearch';
import { LostBusinessLanguageSearchComponent } from './../../../internal/search/iCABSBLostBusinessLanguageSearch.component';
import { LostBusinessDetailLanguageSearchComponent } from './../../../internal/search/iCABSBLostBusinessDetailLanguageSearch.component';
import { QueryParams } from '../../../../shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSAInactivePremiseInfoMaintenance.html'
})

export class InactivePremiseInfoMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('icabsLostBusinessLang') public icabsLostBusinessLang: LostBusinessDetailLanguageSearchComponent;
    @ViewChild('lostbusinesslanguagesearchDropDown') public lostbusinesslanguagesearchDropDown: LostBusinessLanguageSearchComponent;

    private queryParams: any = {
        method: 'contract-management/maintenance',
        module: 'contract-admin',
        operation: 'Application/iCABSAInactivePremiseInfoMaintenance'
    };
    private currentContractTypeLabel: string;
    private onSave: boolean = false;
    public numberLabel: string;
    public isAutoOpenContract: boolean = false;
    public pageId: string = '';
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public inputParams: any = {
        contractSearch: {
            parentMode: 'LookUp',
            contentComp: ContractSearchComponent
        },
        premiseSearch: {
            parentMode: 'LookUp',
            ContractNumber: '',
            ContractName: '',
            contentComp: PremiseSearchComponent
        },
        employeeSearch: {
            contentComponent: EmployeeSearchComponent,
            parentMode: 'LookUp-Commission',
            isDisabled: true
        }
    };
    public dropdown: any = {
        lostBusiness: {
            isRequired: false,
            isDisabled: true,
            isTriggerValidate: false,
            inputParams: {
                parentMode: 'LookUp-Active',
                languageCode: this.riExchange.LanguageCode()
            }
        },
        lostBusinessDetails: {
            isRequired: false,
            isDisabled: true,
            isTriggerValidate: false,
            inputParams: {
                parentMode: 'LookUp'
            }
        },
        reasoncode: {
            inputParams: {
                parentMode: 'LookUpReplace',
                operation: 'Business/iCABSBVisitNarrativeSearch',
                module: 'service',
                method: 'service-delivery/search'
            },
            displayFields: ['VisitNarrativeCode', 'VisitNarrativeDesc'],
            activeSelected: {
                id: '',
                text: ''
            },
            isDisabled: true,
            isRequired: false,
            isFirstItemSelected: false,
            isTriggerValidate: false
        }
    };
    public activeLostBusinessSearch: any = {
        id: '',
        text: ''
    };
    public activeLostBusinessDetailSearch: any = {
        id: '',
        text: ''
    };
    public controls: Array<any> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, required: true, commonValidator: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, required: true, commonValidator: true },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true },
        { name: 'Status', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseAddressLine1', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseAddressLine2', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseAddressLine3', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseAddressLine4', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseAddressLine5', type: MntConst.eTypeText, disabled: true },
        { name: 'NegBranchNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'InvoiceAnnivDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'InvoiceFrequencyCode', type: MntConst.eTypeInteger, disabled: true },
        { name: 'InvoiceFrequencyDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseAnnualValue', type: MntConst.eTypeCurrency, disabled: true },
        { name: 'PremiseCommenceDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'PremisePostcode', type: MntConst.eTypeCode, disabled: true },
        { name: 'LostBusinessCode', type: MntConst.eTypeCode, required: true, value: '' },
        { name: 'LostBusinessDesc', type: MntConst.eTypeText },
        { name: 'LostBusinessDetailCode', type: MntConst.eTypeCode, value: '', required: true },
        { name: 'LostBusinessDetailDesc', type: MntConst.eTypeText },
        { name: 'InactiveEffectDate', type: MntConst.eTypeDate, required: true },
        { name: 'CommissionEmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'SalesEmployeeText', type: MntConst.eTypeText },
        { name: 'InactivePremiseText', type: MntConst.eTypeText },
        { name: 'RemovalVisitText', type: MntConst.eTypeText },
        { name: 'CreateContact', type: MntConst.eTypeCheckBox, value: false },
        { name: 'CancelProRataChargeInd', type: MntConst.eTypeCheckBox },
        { name: 'menu' },
        { name: 'CurrentContractType' },
        { name: 'ActionType' },
        { name: 'LostBusinessRequestNumber', type: MntConst.eTypeInteger },
        { name: 'DetailRequiredInd', type: MntConst.eTypeCheckBox },
        { name: 'ErrorMessageDesc', type: MntConst.eTypeText },
        { name: 'UninvoicedProRataExist', type: MntConst.eTypeCheckBox },
        { name: 'InfoMessage', type: MntConst.eTypeTextFree },
        { name: 'VisitNarrativeCode' },
        { name: 'VisitNarrativeDesc' },
        { name: 'InactivePremiseInfoStatus' }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSAINACTIVEPREMISEINFOMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Premises Deletion Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.pageTitle = this.pageParams.pageTitle;
            this.utils.setTitle(this.pageTitle);
            this.currentContractTypeLabel = this.pageParams.currentContractTypeLabel;
            this.numberLabel = this.pageParams.numberLabel;
            this.inputParams.contractSearch.currentContractType = this.getControlValue('CurrentContractType');
            this.inputParams.premiseSearch.CurrentContractType = this.getControlValue('CurrentContractType');
            this.inputParams.premiseSearch.ContractNumber = this.getControlValue('ContractNumber');
            this.inputParams.premiseSearch.ContractName = this.getControlValue('ContractName');
            this.inputParams.employeeSearch.isDisabled = this.pageParams.EmployeeElipDisable;
            this.riExchange.riInputElement.Disable(this.uiForm, 'ContractNumber');
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseNumber');
            this.setRequiredStatus('LostBusinessDetailCode', this.pageParams.isLostBusinessDetailCodeReq);
            if (this.pageParams.isInactiveEffectDateDisable) {
                this.riExchange.riInputElement.Disable(this.uiForm, 'InactiveEffectDate');
            }
            if (this.pageParams.isCommissionEmployeeCodeDisable) {
                this.riExchange.riInputElement.Disable(this.uiForm, 'CommissionEmployeeCode');
                this.inputParams.employeeSearch.isDisabled = true;
            }
            if (this.pageParams.isSalesEmployeeTextDisable) {
                this.riExchange.riInputElement.Disable(this.uiForm, 'SalesEmployeeText');
            }
            this.setRequiredStatus('InactiveEffectDate', this.pageParams.isInactiveEffectDateReq);
            this.dropdown.lostBusiness.isDisabled = this.pageParams.isLostBusinessDisabled;
            this.dropdown.lostBusinessDetails.isDisabled = this.pageParams.isLostBusinessDetailsDisabled;
            this.dropdown.lostBusiness.isRequired = this.pageParams.isLostBusinessRequired;
            this.dropdown.lostBusinessDetails.isRequired = this.pageParams.isLostBusinessDetailsRequired;
            this.dropdown.lostBusinessDetails.inputParams.LostBusinessCode = this.getControlValue('LostBusinessCode');
            this.dropdown.reasoncode.isDisabled = this.pageParams.isReasoncodeDisabled;
            this.activeLostBusinessSearch = {
                id: this.getControlValue('LostBusinessCode'),
                text: this.getControlValue('LostBusinessCode') ? this.getControlValue('LostBusinessCode') + ' - ' + this.getControlValue('LostBusinessDesc') : ''
            };
            this.activeLostBusinessDetailSearch = {
                id: this.getControlValue('LostBusinessDetailCode'),
                text: this.getControlValue('LostBusinessDetailCode') ? this.getControlValue('LostBusinessDetailCode') + ' - ' + this.getControlValue('LostBusinessDetailDesc') : ''
            };
            this.dropdown.reasoncode.activeSelected = {
                id: this.getControlValue('VisitNarrativeCode'),
                text: this.getControlValue('VisitNarrativeCode') ? this.getControlValue('VisitNarrativeCode') + ' - ' + this.getControlValue('VisitNarrativeDesc') : ''
            };
            if (this.pageParams['isTrLostBusinessDetail']) {
                this.icabsLostBusinessLang.fetchDropDownData();
            }
        } else {
            this.pageParams.isTrLostBusiness = true;
            this.pageParams.isTrText = true;
            this.pageParams.isTrContact = true;
            this.pageParams.isTrVisitNarrative = true;
            this.pageParams.isTrRemovalVisitNotes = true;
            this.pageParams.isTrEffectDate = true;
            this.pageParams.isTrLostBusinessDetail = false;
            this.pageParams.isTrCancelProRata = false;
            this.pageParams.isTdPremiseAnnualValue = false;
            this.pageParams.isBtnSaveDisable = true;
            this.disableControls(['ContractNumber', 'PremiseNumber']);
            this.isAutoOpenContract = true;
            this.numberLabel = 'Contract Number';
            this.windowOnLoad();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        this.setCurrentContractType();
        this.hideShowFields();
        if (this.parentMode === 'Contact' || this.parentMode === 'Contact-View') {
            this.isAutoOpenContract = false;
            this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
            this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
            this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
            this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
            this.setControlValue('LostBusinessRequestNumber', this.riExchange.getParentHTMLValue('LostBusinessRequestNumber'));
            this.inputParams.premiseSearch.ContractNumber = this.getControlValue('ContractNumber');
            this.inputParams.premiseSearch.ContractName = this.getControlValue('ContractName');
            this.riExchange.riInputElement.Disable(this.uiForm, 'ContractNumber');
            this.pageParams.isContractDis = true;
            if ((this.getControlValue('ContractNumber') !== '') && (this.getControlValue('PremiseNumber') !== '')) {
                this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseNumber');
                this.pageParams.isPremiseDis = true;
                this.checkContractType();
            }
        }
        if (this.urlParameterContains('reinstate')) {
            this.pageTitle = this.currentContractTypeLabel + ' Premises Reinstate Maintenance';
            this.utils.setTitle(this.pageTitle);
            this.pageParams.isTrLostBusiness = false;
            this.pageParams.isTrText = false;
            this.pageParams.isTrContact = false;
            this.setControlValue('ActionType', 'Reinstate');
            this.pageParams.actionType = this.getControlValue('ActionType');
            this.pageParams.isTrVisitNarrative = false;
            this.pageParams.isTrRemovalVisitNotes = false;
        }
        if (this.urlParameterContains('cancel')) {
            this.pageTitle = this.currentContractTypeLabel + ' Premises Cancel Maintenance';
            this.utils.setTitle(this.pageTitle);
            this.numberLabel = this.currentContractTypeLabel + ' Number';
            this.pageParams.isTrLostBusiness = false;
            this.pageParams.isTrEffectDate = false;
            this.pageParams.isTrContact = false;
            this.setControlValue('ActionType', 'Cancel');
            this.pageParams.actionType = this.getControlValue('ActionType');
        }
        if (this.parentMode === 'Contact-View') {
            this.pageParams.isTrLostBusinessDetail = true;
            if (this.getControlValue('PremiseNumber') === '') {
                let search: QueryParams = this.getURLSearchParamObject();
                search.set(this.serviceConstants.Action, '6');
                let formData: any = {};
                formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
                formData['LostBusinessRequestNumber'] = this.getControlValue('LostBusinessRequestNumber');
                formData[this.serviceConstants.Function] = 'GetLostBusinessRequestDetails';
                this.queryParams.search = search;
                this.ajaxSource.next(this.ajaxconstant.START);
                this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        }
                    }, (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
                    });
            }
            this.checkContractType();
        }
    }

    //to check the URL Parameters
    private urlParameterContains(parameter: string): boolean {
        let url = window.location.href;
        if (url.indexOf('/' + parameter + '?') !== -1) {
            return true;
        }
        return false;
    }

    //To set Contract Type
    private setCurrentContractType(): void {
        this.setControlValue('CurrentContractType', this.riExchange.getCurrentContractType());
        this.inputParams.contractSearch.currentContractType = this.getControlValue('CurrentContractType');
        this.inputParams.premiseSearch.CurrentContractType = this.getControlValue('CurrentContractType');
        this.currentContractTypeLabel = this.utils.getCurrentContractLabel(this.getControlValue('CurrentContractType'));
    }

    //API to check contract type
    private checkContractType(): void {
        if ((this.getControlValue('ContractNumber'))) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formData: any = {};
            formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
            formData['ContractTypeCode'] = this.getControlValue('CurrentContractType');
            formData[this.serviceConstants.Function] = 'CheckContractType';
            this.queryParams.search = search;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.getFormData();
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
                });
        } else {
            this.uiForm.reset();
            this.setCurrentContractType();
            this.disableControls(['ContractNumber', 'PremiseNumber']);
            this.pageParams.isBtnSaveDisable = true;
            this.activeLostBusinessSearch = {
                id: '',
                text: ''
            };
            this.activeLostBusinessDetailSearch = {
                id: '',
                text: ''
            };
            this.dropdown.lostBusiness.isDisabled = true;
            this.dropdown.lostBusinessDetails.isDisabled = true;
            this.dropdown.reasoncode.isDisabled = true;
            this.setControlValue('ActionType', this.pageParams.actionType);
        }
    }

    //API to get form data
    private getFormData(): void {
        if ((this.getControlValue('ContractNumber')) && (this.getControlValue('PremiseNumber'))) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '0');
            let formData: any = {};
            formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
            formData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
            formData[this.serviceConstants.Function] = 'CheckContractType';
            this.pageParams.function = 'CheckContractType';
            this.queryParams.search = search;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.formPristine();
                        this.riExchange.riInputElement.Disable(this.uiForm, 'ContractNumber');
                        if (this.pageParams.isPremiseDis) { this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseNumber');}
                        this.riExchange.riInputElement.Enable(this.uiForm, 'CommissionEmployeeCode');
                        this.inputParams.employeeSearch.isDisabled = false;
                        this.riExchange.riInputElement.Enable(this.uiForm, 'SalesEmployeeText');
                        this.riExchange.riInputElement.Enable(this.uiForm, 'CreateContact');
                        this.riExchange.riInputElement.Enable(this.uiForm, 'CancelProRataChargeInd');
                        this.riExchange.riInputElement.Enable(this.uiForm, 'menu');
                        this.setControlValue('CancelProRataChargeInd', this.utils.convertResponseValueToCheckboxInput(data.CancelProRataChargeInd));
                        this.setControlValue('CommissionEmployeeCode', data.CommissionEmployeeCode);
                        this.setControlValue('ContractName', data.ContractName);
                        this.setControlValue('DetailRequiredInd', this.utils.convertResponseValueToCheckboxInput(data.DetailRequiredInd));
                        if (this.pageParams.isTrEffectDate) {
                            this.setRequiredStatus('InactiveEffectDate', true);
                            this.riExchange.riInputElement.Enable(this.uiForm, 'InactiveEffectDate');
                            this.pageParams.isInactiveEffectDateReq = true;
                            this.setControlValue('InactiveEffectDate', (data.InactiveEffectDate !== '') ? this.globalize.formatDateToLocaleFormat(data.InactiveEffectDate) : '');
                        } else {
                            this.setControlValue('InactiveEffectDate', '');
                        }
                        if (this.pageParams.isTrText) {
                            this.riExchange.riInputElement.Enable(this.uiForm, 'InactivePremiseText');
                            this.setControlValue('InactivePremiseText', data.InactivePremiseText);
                        }
                        if (this.pageParams.isTrLostBusiness) {
                            this.activeLostBusinessSearch = {
                                id: data.LostBusinessCode,
                                text: data.LostBusinessCode ? data.LostBusinessCode + ' - ' + data.LostBusinessDesc : ''
                            };
                            this.setControlValue('LostBusinessCode', data.LostBusinessCode);
                            this.setControlValue('LostBusinessDesc', data.LostBusinessDesc);
                        }
                        if ((this.getControlValue('DetailRequiredInd')) && this.pageParams.isTrLostBusiness) {
                            this.activeLostBusinessDetailSearch = {
                                id: data.LostBusinessDetailCode,
                                text: data.LostBusinessDetailCode ? data.LostBusinessDetailCode + ' - ' + data.LostBusinessDetailDesc : ''
                            };
                            this.setControlValue('LostBusinessDetailCode', data.LostBusinessDetailCode);
                            this.setControlValue('LostBusinessDetailDesc', data.LostBusinessDetailDesc);
                        }
                        this.setControlValue('NegBranchNumber', data.NegBranchNumber);
                        this.setControlValue('PremiseAddressLine1', data.PremiseAddressLine1);
                        this.setControlValue('PremiseAddressLine2', data.PremiseAddressLine2);
                        this.setControlValue('PremiseAddressLine3', data.PremiseAddressLine3);
                        this.setControlValue('PremiseAddressLine4', data.PremiseAddressLine4);
                        this.setControlValue('PremiseAddressLine5', data.PremiseAddressLine5);
                        this.setControlValue('PremiseAnnualValue', this.globalize.formatCurrencyToLocaleFormat(data.PremiseAnnualValue));
                        this.setControlValue('PremiseCommenceDate', this.globalize.formatDateToLocaleFormat(data.PremiseCommenceDate));
                        this.setControlValue('PremiseName', data.PremiseName);
                        this.setControlValue('PremisePostcode', data.PremisePostcode);
                        if (this.pageParams.isTrRemovalVisitNotes) {
                            this.riExchange.riInputElement.Enable(this.uiForm, 'RemovalVisitText');
                            this.setControlValue('RemovalVisitText', data.RemovalVisitText);
                        }
                        this.setControlValue('Status', data.Status);
                        this.setControlValue('UninvoicedProRataExist', this.utils.convertResponseValueToCheckboxInput(data.UninvoicedProRataExist));
                        if (this.pageParams.isTrVisitNarrative) {
                            this.riExchange.riInputElement.Enable(this.uiForm, 'VisitNarrativeCode');
                            this.setControlValue('VisitNarrativeCode', data.VisitNarrativeCode);
                            this.dropdown.reasoncode.isDisabled = false;
                            if (!this.getControlValue('VisitNarrativeCode')) {
                                this.dropdown.reasoncode.activeSelected = {
                                    id: '',
                                    text: ''
                                };
                            }
                            this.getVisitNarrativeDesc();
                        }
                        this.dropdown.lostBusinessDetails.inputParams.LostBusinessCode = this.getControlValue('LostBusinessCode');
                        this.icabsLostBusinessLang.fetchDropDownData();
                        this.dropdown.lostBusiness.isDisabled = false;
                        this.dropdown.lostBusiness.isRequired = true;
                        this.dropdown.lostBusinessDetails.isDisabled = false;
                        this.dropdown.lostBusinessDetails.isRequired = true;
                        this.pageParams.isBtnSaveDisable = false;
                        this.doLookUpCall();
                        this.defaultLostBusiness();
                        if (this.parentMode === 'Contact-View') {
                            this.riExchange.riInputElement.Disable(this.uiForm, 'CommissionEmployeeCode');
                            this.inputParams.employeeSearch.isDisabled = true;
                            this.riExchange.riInputElement.Disable(this.uiForm, 'InactiveEffectDate');
                            this.riExchange.riInputElement.Disable(this.uiForm, 'SalesEmployeeText');
                            this.riExchange.riInputElement.Disable(this.uiForm, 'CreateContact');
                            this.riExchange.riInputElement.Disable(this.uiForm, 'CancelProRataChargeInd');
                            this.riExchange.riInputElement.Disable(this.uiForm, 'InactivePremiseText');
                            this.riExchange.riInputElement.Disable(this.uiForm, 'RemovalVisitText');
                            this.dropdown.reasoncode.isDisabled = true;
                            this.riExchange.riInputElement.Disable(this.uiForm, 'VisitNarrativeCode');
                            this.dropdown.lostBusiness.isDisabled = true;
                            this.dropdown.lostBusinessDetails.isDisabled = true;
                            this.dropdown.lostBusiness.isRequired = false;
                            this.dropdown.lostBusinessDetails.isDisabled = true;
                            this.dropdown.lostBusinessDetails.isRequired = false;
                            this.pageParams.isBtnSaveDisable = true;
                        } else {
                            this.afterSave();
                            this.beforeUpdateMode();
                            this.displayFields();
                            this.hideShowFields();
                        }
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
                });
        }
    }

    //Lookup to get data
    private doLookUpCall(): void {
        let lookupIP = [
            {
                'table': 'LostBusinessLang',
                'query': {
                    BusinessCode: this.businessCode(),
                    LanguageCode: this.riExchange.LanguageCode(),
                    LostBusinessCode: this.getControlValue('LostBusinessCode')
                },
                'fields': ['LostBusinessDesc']
            },
            {
                'table': 'LostBusinessDetailLang',
                'query': {
                    BusinessCode: this.businessCode(),
                    LanguageCode: this.riExchange.LanguageCode(),
                    LostBusinessCode: this.getControlValue('LostBusinessCode'),
                    LostBusinessDetailCode: this.getControlValue('LostBusinessDetailCode')
                },
                'fields': ['LostBusinessDetailDesc']
            },
            {
                'table': 'Employee',
                'query': {
                    BusinessCode: this.businessCode(),
                    EmployeeCode: this.getControlValue('CommissionEmployeeCode')
                },
                'fields': ['EmployeeSurname']
            },
            {
                'table': 'Branch',
                'query': {
                    BusinessCode: this.businessCode(),
                    BranchNumber: this.getControlValue('NegBranchNumber')
                },
                'fields': ['BranchName']
            },
            {
                'table': 'Contract',
                'query': {
                    BusinessCode: this.businessCode(),
                    ContractNumber: this.getControlValue('ContractNumber')
                },
                'fields': ['InvoiceAnnivDate', 'InvoiceFrequencyCode']
            }
        ];
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupIP).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                let isCallConfirm: boolean = false;
                let lostBusinessLangRecord = data[0][0];
                let lostBusinessDetailLangRecord = data[1][0];
                let employeeRecord = data[2][0];
                let branchRecord = data[3][0];
                let contractRecord = data[4][0];
                if (lostBusinessLangRecord) {
                    this.setControlValue('LostBusinessDesc', lostBusinessLangRecord.LostBusinessDesc);
                    this.activeLostBusinessSearch = {
                        id: this.getControlValue('LostBusinessCode'),
                        text: this.getControlValue('LostBusinessCode') ? this.getControlValue('LostBusinessCode') + ' - ' + lostBusinessLangRecord.LostBusinessDesc : ''
                    };
                }
                if (lostBusinessDetailLangRecord) {
                    this.setControlValue('LostBusinessDetailDesc', lostBusinessDetailLangRecord.LostBusinessDetailDesc);
                    this.activeLostBusinessDetailSearch = {
                        id: this.getControlValue('LostBusinessDetailCode'),
                        text: this.getControlValue('LostBusinessDetailCode') ? this.getControlValue('LostBusinessDetailCode') + ' - ' + lostBusinessDetailLangRecord.LostBusinessDetailDesc : ''
                    };
                }
                if (this.getControlValue('CommissionEmployeeCode')) {
                    if (employeeRecord) {
                        isCallConfirm = true;
                        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'CommissionEmployeeCode', false);
                        this.uiForm.controls['CommissionEmployeeCode'].updateValueAndValidity();
                        this.setControlValue('EmployeeSurname', employeeRecord.EmployeeSurname);
                    } else {
                        isCallConfirm = false;
                        this.uiForm.controls['CommissionEmployeeCode'].setErrors({});
                        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'CommissionEmployeeCode', true);
                        this.setControlValue('EmployeeSurname', '');
                    }
                } else if (this.getControlValue('CommissionEmployeeCode') === '') {
                    this.setControlValue('EmployeeSurname', '');
                    isCallConfirm = true;
                } else {
                    isCallConfirm = true;
                }
                if (branchRecord) {
                    this.setControlValue('BranchName', branchRecord.BranchName);
                }
                if (contractRecord) {
                    this.setControlValue('InvoiceAnnivDate', this.globalize.formatDateToLocaleFormat(contractRecord.InvoiceAnnivDate));
                    this.setControlValue('InvoiceFrequencyCode', contractRecord.InvoiceFrequencyCode);
                    this.invoiceFrequencyCodeLookUpCall();
                }
                if (this.onSave) {
                    this.onSave = false;
                    if (isCallConfirm) {
                        this.onConfirmCallBack();
                    } else {
                        this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                    }
                }
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
        });
    }

    //Lookup to get frequency desc
    private invoiceFrequencyCodeLookUpCall(): void {
        let lookupIP = [
            {
                'table': 'SystemInvoiceFrequency',
                'query': {
                    BusinessCode: this.businessCode(),
                    InvoiceFrequencyCode: this.getControlValue('InvoiceFrequencyCode')
                },
                'fields': ['InvoiceFrequencyDesc']
            }];
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupIP).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    let invoiceFrequencyRecord = data[0][0];
                    if (invoiceFrequencyRecord) {
                        this.setControlValue('InvoiceFrequencyDesc', invoiceFrequencyRecord.InvoiceFrequencyDesc);
                    }
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
            });
    }

    private defaultLostBusiness(): void {
        if ((this.getControlValue('LostBusinessRequestNumber'))) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formData: any = {};
            formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
            formData[this.serviceConstants.Function] = 'DefaultLostBusiness';
            this.queryParams.search = search;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
                });
        }
    }

    //Before save method
    private beforeSave(): void {
        if (this.urlParameterContains('cancel')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formData: any = {};
            formData[this.serviceConstants.Function] = 'WarnCancel';
            this.pageParams.function = 'WarnCancel';
            this.queryParams.search = search;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    let modalVO: ICabsModalVO = new ICabsModalVO();
                    if (data.hasOwnProperty('ErrorMessageDesc')) {
                        modalVO.closeCallback = this.doLookUpCall.bind(this);
                    }
                    if (data.hasError) {
                        this.onSave = true;
                        modalVO.msg = data.errorMessage;
                        modalVO.fullError = data.fullError;
                        this.modalAdvService.emitMessage(modalVO);
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
                });
        }
    }

    //After save method
    private afterSave(): void {
        if (this.getControlValue('InfoMessage')) {
            this.modalAdvService.emitError(new ICabsModalVO(this.getControlValue('InfoMessage')));
            this.setControlValue('InfoMessage', '');
        }
        if (this.getControlValue('InactiveEffectDate')) {
            this.setControlValue('InactivePremiseInfoStatus', 'Update');
            if (this.urlParameterContains('reinstate')) {
                this.setControlValue('InactivePremiseInfoStatus', 'Reinst');
            }
            this.pageParams.isBtnSaveDisable = false;
        } else {
            this.setControlValue('InactivePremiseInfoStatus', 'Delete');
            this.pageParams.isBtnSaveDisable = false;
            if (this.urlParameterContains('cancel')) {
                this.setControlValue('InactivePremiseInfoStatus', 'Cancel');
                this.pageParams.isBtnSaveDisable = false;
            } else if (this.urlParameterContains('reinstate')) {
                this.pageParams.isBtnSaveDisable = true;
                this.disableControls(['ContractNumber', 'PremiseNumber']);
            }
        }
    }

    //Function to disable fields
    private displayFields(): void {
        if ((this.getControlValue('DetailRequiredInd')) && this.pageParams.isTrLostBusiness) {
            this.pageParams.isTrLostBusinessDetail = true;
            if (this.parentMode === 'Contact') {
                this.pageParams.isTrContact = false;
            }
            else {
                this.pageParams.isTrContact = true;
            }
            this.setRequiredStatus('LostBusinessDetailCode', true);
            this.pageParams.isLostBusinessDetailCodeReq = true;
        } else {
            this.pageParams.isTrLostBusinessDetail = false;
            this.pageParams.isTrContact = false;
            this.setControlValue('LostBusinessDetailCode', '');
            this.activeLostBusinessDetailSearch = {
                id: '',
                text: ''
            };
            this.setRequiredStatus('LostBusinessDetailCode', false);
            this.pageParams.isLostBusinessDetailCodeReq = false;
        }
    }

    //function to hide and show fields
    private hideShowFields(): void {
        this.pageParams.isTdPremiseAnnualValue = false;
        if (this.riExchange.ClientSideValues.Fetch('FullAccess') === 'Full' || this.utils.getBranchCode() === this.getControlValue('NegBranchNumber')) {
            this.pageParams.isTdPremiseAnnualValue = true;
        }
        if (this.urlParameterContains('cancel')) {
            if (this.getControlValue('UninvoicedProRataExist')) {
                this.pageParams.isTrCancelProRata = true;
            } else {
                this.pageParams.isTrCancelProRata = false;
            }
        }
    }

    //Before update method
    private beforeUpdateMode(): void {
        this.displayFields();
        if (!this.urlParameterContains('cancel')) {
            if (this.getControlValue('InactiveEffectDate')) {
                this.riExchange.riInputElement.Disable(this.uiForm, 'InactiveEffectDate');
                this.pageParams.isInactiveEffectDateDisable = true;
            } else {
                this.pageParams.isInactiveEffectDateDisable = false;
            }
        }
        if (this.parentMode === 'Contact') {
            this.setControlValue('InactiveEffectDate', this.riExchange.getParentHTMLValue('OutcomeEffectDate'));
        }
        if (this.getControlValue('InactivePremiseInfoStatus') === 'Update') {
            this.riExchange.riInputElement.Disable(this.uiForm, 'CommissionEmployeeCode');
            this.inputParams.employeeSearch.isDisabled = true;
            this.riExchange.riInputElement.Disable(this.uiForm, 'SalesEmployeeText');
            this.pageParams.isCommissionEmployeeCodeDisable = true;
            this.pageParams.isSalesEmployeeTextDisable = true;
        } else {
            this.inputParams.employeeSearch.isDisabled = false;
            this.pageParams.isCommissionEmployeeCodeDisable = false;
            this.pageParams.isSalesEmployeeTextDisable = false;
        }
    }

    //On lost business language change API
    private callServiceForLostBusinessDetailRequired(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData['LostBusinessCode'] = this.getControlValue('LostBusinessCode');
        formData[this.serviceConstants.Function] = 'LostBusinessDetailRequired';
        this.pageParams.function = 'LostBusinessDetailRequired';
        this.queryParams.search = search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('DetailRequiredInd', data.DetailRequiredInd === 'yes' ? true : false);
                    this.displayFields();
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
            });
    }

    // Warn cancel function
    private showWarnMessage(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formData: any = {
            ContractNumber: this.getControlValue('ContractNumber'),
            InactiveEffectDate: this.getControlValue('InactiveEffectDate'),
            Function: 'WarnAnniversaryDate'
        };
        this.pageParams.function = 'WarnAnniversaryDate';
        this.queryParams.search = search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitMessage(new ICabsModalVO(data.errorMessage, data.fullError));
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
            });
    }

    //Promt method
    private onConfirmCallBack(): void {
        setTimeout(() => {
            let promptVO: ICabsModalVO = new ICabsModalVO();
            promptVO.msg = MessageConstant.Message.ConfirmRecord;
            promptVO.confirmCallback = this.saveConfirm.bind(this);
            this.modalAdvService.emitPrompt(promptVO);
        }, 0);
    }

    private onAddPremiseConfirmCallBack(): void {
        if (this.parentMode === 'Contact') {
            setTimeout(() => {
                let addPremise: ICabsModalVO = new ICabsModalVO();
                addPremise.msg = MessageConstant.PageSpecificMessage.addPremiseDeletion;
                addPremise.confirmCallback = this.resetForm.bind(this);
                addPremise.cancelCallback = this.disableForm.bind(this);
                addPremise.confirmLabel = 'Yes';
                addPremise.cancelLabel = 'No';
                this.modalAdvService.emitPrompt(addPremise);
            }, 0);
        }
    }

    private disableForm(): void {
        this.disableControls(['menu']);
        this.dropdown.lostBusiness.isDisabled = true;
        this.dropdown.lostBusinessDetails.isDisabled = true;
        this.dropdown.reasoncode.isDisabled = true;
        this.pageParams.isPremiseDis = true;
        this.inputParams.employeeSearch.isDisabled = true;
    }

    private resetForm(): void {
        this.uiForm.reset({'ContractNumber': this.getControlValue('ContractNumber'),'ContractName': this.getControlValue('ContractName')});
        this.setControlValue('LostBusinessRequestNumber', this.riExchange.getParentHTMLValue('LostBusinessRequestNumber'));
        this.setCurrentContractType();
        this.riExchange.riInputElement.Enable(this.uiForm, 'PremiseNumber');
        this.riExchange.riInputElement.Disable(this.uiForm, 'menu');
        this.numberLabel = 'Contract Number';
        this.pageParams.isTrLostBusiness = true;
        this.pageParams.isTrText = true;
        this.pageParams.isTrVisitNarrative = true;
        this.pageParams.isTrRemovalVisitNotes = true;
        this.pageParams.isTrEffectDate = true;
        this.pageParams.isTrLostBusinessDetail = false;
        this.pageParams.isTrCancelProRata = false;
        this.pageParams.isTdPremiseAnnualValue = false;
        this.pageParams.isBtnSaveDisable = true;
        this.pageParams.isPremiseDis = false;
        this.dropdown.lostBusiness.isRequired = false;
        this.dropdown.lostBusiness.isTriggerValidate = false;
        this.dropdown.lostBusinessDetails.isRequired = false;
        this.dropdown.lostBusinessDetails.isTriggerValidate = false;
        this.inputParams.employeeSearch.isDisabled = true;
        this.activeLostBusinessSearch = {
            id: '',
            text: ''
        };
        this.activeLostBusinessDetailSearch = {
            id: '',
            text: ''
        };
        this.dropdown.reasoncode.activeSelected = {
            id: '',
            text: ''
        };
    }

    //Confirm call back
    private saveConfirm(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        let formData: any = {};
        formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        formData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        if (this.pageParams.isTrLostBusiness) {
            formData['LostBusinessCode'] = this.getControlValue('LostBusinessCode');
        }
        if (this.pageParams.isTrLostBusinessDetail) {
            formData['LostBusinessDetailCode'] = this.getControlValue('LostBusinessDetailCode');
        }
        if (this.pageParams.isTrEffectDate) {
            formData['InactiveEffectDate'] = this.getControlValue('InactiveEffectDate');
        }
        formData['CommissionEmployeeCode'] = this.getControlValue('CommissionEmployeeCode');
        if (this.pageParams.isTrText) {
            formData['InactivePremiseText'] = this.getControlValue('InactivePremiseText');
        }
        if (this.pageParams.isTrVisitNarrative) {
            formData['VisitNarrativeCode'] = this.getControlValue('VisitNarrativeCode');
        }
        if (this.pageParams.isTrRemovalVisitNotes) {
            formData['RemovalVisitText'] = this.getControlValue('RemovalVisitText');
        }
        formData['ActionType'] = this.getControlValue('ActionType');
        formData['LostBusinessRequestNumber'] = this.getControlValue('LostBusinessRequestNumber');
        formData['ContractTypeCode'] = this.getControlValue('CurrentContractType');
        formData['ErrorMessageDesc'] = this.getControlValue('ErrorMessageDesc');
        formData['SalesEmployeeText'] = this.getControlValue('SalesEmployeeText');
        formData['ContractName'] = this.getControlValue('ContractName');
        formData['PremiseName'] = this.getControlValue('PremiseName');
        formData['PremiseAddressLine1'] = this.getControlValue('PremiseAddressLine1');
        formData['PremiseAddressLine2'] = this.getControlValue('PremiseAddressLine2');
        formData['PremiseAddressLine3'] = this.getControlValue('PremiseAddressLine3');
        formData['PremiseAddressLine4'] = this.getControlValue('PremiseAddressLine4');
        formData['PremiseAddressLine5'] = this.getControlValue('PremiseAddressLine5');
        formData['PremisePostcode'] = this.getControlValue('PremisePostcode');
        formData['PremiseAnnualValue'] = this.getControlValue('PremiseAnnualValue');
        formData['PremiseCommenceDate'] = this.getControlValue('PremiseCommenceDate');
        formData['NegBranchNumber'] = this.getControlValue('NegBranchNumber');
        formData['CreateContact'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('CreateContact'));
        formData['UninvoicedProRataExist'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('UninvoicedProRataExist'));
        formData['CancelProRataChargeInd'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('CancelProRataChargeInd'));
        formData['InfoMessage'] = this.getControlValue('InfoMessage');
        formData[this.serviceConstants.Function] = this.pageParams.function;
        this.queryParams.search = search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError || data.errorMessage && data.fullError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    if (data.errorNumber === 2007) {
                        this.disableForm();
                    }
                    return;
                } else {
                    this.setControlValue('Status', data.Status);
                    this.setControlValue('PremiseAnnualValue', this.globalize.formatCurrencyToLocaleFormat(this.getControlValue(data.PremiseAnnualValue)));
                    let messageVO = new ICabsModalVO(MessageConstant.Message.SavedSuccessfully);
                    messageVO.closeCallback = this.onAddPremiseConfirmCallBack.bind(this);
                    this.modalAdvService.emitMessage(messageVO);
                    this.formPristine();
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
            });
    }

    //Save button click
    public onSaveClick(): void {
        if (this['pageParams']['isTrEffectDate'] && this.getControlValue('InactiveEffectDate') === '') {
            return;
        }
        this.riExchange.riInputElement.isError(this.uiForm, 'InactiveEffectDate');
        this.dropdown.lostBusiness.isTriggerValidate = true;
        if (this.pageParams['isTrLostBusinessDetail']) {
            this.dropdown.lostBusinessDetails.isTriggerValidate = true;
        }
        if (this.urlParameterContains('cancel')) {
            this.beforeSave();
        } else {
            if (this.uiForm.valid) {
                this.onSave = true;
                this.doLookUpCall();
            }
        }
    }

    //Cancel button click
    public onCancelClick(): void {
        this.dropdown.lostBusiness.isRequired = false;
        this.dropdown.lostBusiness.isTriggerValidate = false;
        this.dropdown.lostBusinessDetails.isRequired = false;
        this.dropdown.lostBusinessDetails.isTriggerValidate = false;
        this.uiForm.controls['InactiveEffectDate'].markAsUntouched();
        this.setControlValue('EmployeeSurname', '');
        this.setControlValue('SalesEmployeeText', '');
        this.getFormData();
    }

    //Contract ellipsis click
    public onContractSearchDataReturn(data: any): void {
        if ((this.getControlValue('ContractNumber')) && (this.getControlValue('PremiseNumber'))) {
            this.uiForm.reset();
            this.setCurrentContractType();
            this.disableControls(['ContractNumber', 'PremiseNumber']);
            this.pageParams.isBtnSaveDisable = true;
            this.activeLostBusinessSearch = {
                id: '',
                text: ''
            };
            this.activeLostBusinessDetailSearch = {
                id: '',
                text: ''
            };
            this.dropdown.lostBusiness.isDisabled = true;
            this.dropdown.lostBusinessDetails.isDisabled = true;
            this.dropdown.reasoncode.isDisabled = true;
            this.inputParams.employeeSearch.isDisabled = true;
            this.riExchange.riInputElement.Enable(this.uiForm, 'ContractNumber');
            this.riExchange.riInputElement.Enable(this.uiForm, 'PremiseNumber');
            this.setControlValue('ContractNumber', data.ContractNumber);
            this.setControlValue('ContractName', data.ContractName);
            this.inputParams.premiseSearch.ContractNumber = data.ContractNumber;
            this.inputParams.premiseSearch.ContractName = data.ContractName;
            this.dropdown.lostBusiness.isTriggerValidate = false;
            this.dropdown.lostBusinessDetails.isTriggerValidate = false;
            this.setControlValue('ActionType', this.pageParams.actionType);
        } else {
            this.setControlValue('ContractNumber', data.ContractNumber);
            this.setControlValue('ContractName', data.ContractName);
            this.inputParams.premiseSearch.ContractNumber = data.ContractNumber;
            this.inputParams.premiseSearch.ContractName = data.ContractName;
        }
    }

    //Contract onchange event
    public onContractNumberChange(event: any): void {
        this.uiForm.controls['ContractNumber'].markAsPristine();
        if (this.getControlValue('ContractNumber')) {
            this.setControlValue('ContractNumber', event.target.value);
            this.setControlValue('PremiseNumber', '');
            this.inputParams.premiseSearch.ContractNumber = event.target.value;
        } else {
            this.setControlValue('PremiseNumber', '');
        }
    }

    //Premise Ellipsis event
    public onPremiseSearchDataReturn(data: any): void {
        this.setControlValue('PremiseNumber', data.PremiseNumber);
        this.setControlValue('PremiseName', data.PremiseName);
        if (this.parentMode !== 'Contact-View') {
            this.checkContractType();
        }
    }

    //Premise Onchange
    public onPremiseNumberChange(event: any): void {
        this.uiForm.controls['PremiseNumber'].markAsPristine();
        if ((this.getControlValue('ContractNumber')) && (this.getControlValue('PremiseNumber'))) {
            this.setControlValue('PremiseNumber', event.target.value);
            if (this.parentMode !== 'Contact-View') {
                this.checkContractType();
            }
        } else {
            this.setControlValue('PremiseNumber', '');
        }
    }

    //Lost business language dropdown
    public onLostBusinessLanguageSearch(data: any): void {
        this.uiForm.controls['LostBusinessCode'].markAsDirty();
        this.dropdown.lostBusinessDetails.inputParams.LostBusinessCode = data['LostBusinessLang.LostBusinessCode'];
        this.icabsLostBusinessLang.fetchDropDownData();
        if (data['LostBusinessLang.LostBusinessCode'] !== this.getControlValue('LostBusinessCode')) {
            this.setControlValue('LostBusinessCode', data['LostBusinessLang.LostBusinessCode']);
            this.setControlValue('LostBusinessDesc', data['LostBusinessLang.LostBusinessDesc']);
            this.setControlValue('LostBusinessDetailCode', '');
            this.setControlValue('LostBusinessDetailDesc', '');
            this.activeLostBusinessDetailSearch = {
                id: '',
                text: ''
            };
            this.callServiceForLostBusinessDetailRequired();
        }
    }

    //lost business detail method
    public onLostBusinessDetailLangDataReceived(data: any): void {
        if (data) {
            this.uiForm.controls['LostBusinessDetailCode'].markAsDirty();
            this.setControlValue('LostBusinessDetailCode', data['LostBusinessDetailLang.LostBusinessDetailCode']);
            this.setControlValue('LostBusinessDetailDesc', data['LostBusinessDetail.LostBusinessDetailDesc']);
        }
    }

    //Employee Ellipsis event
    public onCommissionEmployeeChange(data: any): void {
        this.uiForm.controls['CommissionEmployeeCode'].markAsDirty();
        this.setControlValue('CommissionEmployeeCode', data['CommissionEmployeeCode']);
        this.setControlValue('EmployeeSurname', data['EmployeeSurname']);
    }

    //Effect date onchange
    public onChangeInactiveEffectDateDate(value: any): void {
        if (value && value.value) {
            this.setControlValue('InactiveEffectDate', value.value);
            this.uiForm.controls['InactiveEffectDate'].markAsDirty();
            this.showWarnMessage();
        }
    }

    //removal visit dropdown
    public onVisitNarrativeCodeChange(data: any): void {
        if (data) {
            this.uiForm.controls['VisitNarrativeCode'].markAsDirty();
            this.setControlValue('VisitNarrativeCode', data.VisitNarrativeCode || '');
            this.setControlValue('VisitNarrativeDesc', data.VisitNarrativeDesc || '');
        }
        this.getVisitNarrativeDesc();
    }

    //APi to get VisitNarrativeDesc
    public getVisitNarrativeDesc(): void {
        if (this.getControlValue('VisitNarrativeCode')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formData: any = {
                VisitNarrativeCode: this.getControlValue('VisitNarrativeCode'),
                Function: 'GetVisitNarrativeDesc'
            };
            this.pageParams.function = 'GetVisitNarrativeDesc';
            this.queryParams.search = search;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('VisitNarrativeDesc', data.VisitNarrativeDesc);
                        this.dropdown.reasoncode.activeSelected = {
                            id: this.getControlValue('VisitNarrativeCode'),
                            text: this.getControlValue('VisitNarrativeCode') + ' - ' + data.VisitNarrativeDesc
                        };
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
                });
        } else {
            this.setControlValue('VisitNarrativeCode', '');
        }
    }

    //Create checkbox event
    public onCreateContact(): void {
        this.pageParams.pageTitle = this.pageTitle;
        this.pageParams.currentContractTypeLabel = this.currentContractTypeLabel;
        this.pageParams.numberLabel = this.numberLabel;
        this.pageParams.EmployeeElipDisable = this.inputParams.employeeSearch.isDisabled;
        this.pageParams.isLostBusinessDisabled = this.dropdown.lostBusiness.isDisabled;
        this.pageParams.isLostBusinessRequired = this.dropdown.lostBusiness.isRequired;
        this.pageParams.isLostBusinessDetailsDisabled = this.dropdown.lostBusinessDetails.isDisabled;
        this.pageParams.isReasoncodeDisabled = this.dropdown.reasoncode.isDisabled;
        this.pageParams.isLostBusinessDetailsRequired = this.dropdown.lostBusinessDetails.isRequired;
        if (this.getControlValue('CreateContact')) {
            this.formPristine();
            this.navigate('New', InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_1);
        }
    }

    //options change event
    public onMenuChange(): void {
        this.pageParams.pageTitle = this.pageTitle;
        this.pageParams.currentContractTypeLabel = this.currentContractTypeLabel;
        this.pageParams.numberLabel = this.numberLabel;
        this.pageParams.EmployeeElipDisable = this.inputParams.employeeSearch.isDisabled;
        this.pageParams.isLostBusinessDisabled = this.dropdown.lostBusiness.isDisabled;
        this.pageParams.isLostBusinessRequired = this.dropdown.lostBusiness.isRequired;
        this.pageParams.isLostBusinessDetailsDisabled = this.dropdown.lostBusinessDetails.isDisabled;
        this.pageParams.isReasoncodeDisabled = this.dropdown.reasoncode.isDisabled;
        this.pageParams.isLostBusinessDetailsRequired = this.dropdown.lostBusinessDetails.isRequired;
        this.pageParams.isPremiseDis = true;
        switch (this.getControlValue('menu')) {
            case 'Premise':
                this.setControlValue('menu', '');
                this.uiForm.controls['menu'].markAsPristine();
                this.navigate('Request', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    contracttypecode: this.getControlValue('CurrentContractType')
                });
                break;
        }
    }

    public onEmployeeChange(): void {
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'CommissionEmployeeCode', false);
        this.uiForm.controls['CommissionEmployeeCode'].updateValueAndValidity();
    }
}
