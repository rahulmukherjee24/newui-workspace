import { StaticUtils } from './../../../../shared/services/static.utility';
import { Injectable } from '@angular/core';
import { ContractMaintenanceFieldList } from './ContractMaintenanceFieldList';


@Injectable()
export class ContractMaintenanceFieldService {

    public visibility: ContractMaintenanceFieldList;
    public required: ContractMaintenanceFieldList;

    private initialVisibility: ContractMaintenanceFieldList;
    private initialRequired: ContractMaintenanceFieldList;

    constructor() {
        this.visibility = new ContractMaintenanceFieldList();
        this.required = new ContractMaintenanceFieldList();
    }

    public setFieldRequiredStatus(fieldName: string, required: boolean): void {
        this.required[fieldName] = required;
    }

    public setFieldRequired(fieldName: string): void {
        this.setFieldRequiredStatus(fieldName, true);
    }

    public setFieldUnrequired(fieldName: string): void {
        this.setFieldRequiredStatus(fieldName, false);
    }

    public setFieldVisibilityStatus(fieldName: string, visible: boolean): void {
        this.visibility[fieldName] = visible;
    }

    public setFieldVisible(fieldName: string): void {
        this.setFieldVisibilityStatus(fieldName, true);
    }

    public setFieldInvisible(fieldName: string): void {
        this.setFieldVisibilityStatus(fieldName, false);
    }

    public storeInitialValues(): void {
        this.initialVisibility = StaticUtils.deepCopy(this.visibility);
        this.initialRequired = StaticUtils.deepCopy(this.required);
    }

    public resetToInitialValues(): void {
        this.visibility = StaticUtils.deepCopy(this.initialVisibility);
        this.required = StaticUtils.deepCopy(this.initialRequired);
    }

}
