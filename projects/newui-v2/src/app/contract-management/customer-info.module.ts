import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ContractManagementModuleRoutes } from '@base/PageRoutes';
import { DataChangeGridComponent } from './PDAReturns/CustomerDataUpdate/iCABSSeDataChangeGrid.component';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { SharedModule } from '@shared/shared.module';

@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class CustomerInfoRootComponent {
    constructor(viewContainerRef: ViewContainerRef) {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchEllipsisModule,
        InternalSearchModule,
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: CustomerInfoRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSSEDATACHANGEGRID_SUB, component: DataChangeGridComponent }

                ], data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        CustomerInfoRootComponent,
        DataChangeGridComponent
    ],
    exports: [
        DataChangeGridComponent
    ],
    entryComponents: [
        DataChangeGridComponent
    ]
})

export class CustomerInfoModule { }
