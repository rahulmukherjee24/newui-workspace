import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Component, OnInit, OnDestroy, Injector, ViewChild } from '@angular/core';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PageIdentifier } from './../../../app/base/PageIdentifier';
import { BaseComponent } from './../../../app//base/BaseComponent';
import { StaticUtils } from './../../../shared/services/static.utility';
import { BranchSearchComponent } from './../../../app/internal/search/iCABSBBranchSearch';

@Component({
    templateUrl: 'iCABSARCustomerLettersForTypeGrid.html'
})

export class CustomerLettersForTypeGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;

    private queryParams: any = {
        operation: 'ApplicationReport/iCABSARCustomerLettersForTypeGrid',
        module: 'letters',
        method: 'ccm/grid'
    };

    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'BranchNumber', type: MntConst.eTypeInteger },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true },
        { name: 'LetterTypeCode', type: MntConst.eTypeText },
        { name: 'ContractTypeCode', type: MntConst.eTypeText },
        { name: 'RepDest', value: 'ReportViewer' }
    ];

    //Branch Search
    public inputParams: any = {
        branchParams: {
            'parentMode': 'LookUp'
        }
    };

    public ContractTypeCodeList: Array<any> = [];

    public postData: any = {};
    public URLReturn: string;

    public pageSize: number = 10;
    public pageCurrent: number = 1;
    public totalRecords: number = 0;
    public itemsTotal: number = 0;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARCUSTOMERLETTERSFORTYPEGRID;
        this.pageTitle = this.browserTitle = 'Letters For Type';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.riExchange.getParentHTMLValue('BranchNumber');
        this.riExchange.getParentHTMLValue('DateFrom');
        this.riExchange.getParentHTMLValue('DateTo');
        this.windowOnload();
        this.onRefresh();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public windowOnload(): void {
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = 10;

        this.riMaintenance.FunctionAdd = false;
        this.riMaintenance.FunctionUpdate = false;
        this.riMaintenance.FunctionDelete = false;
        this.riMaintenance.FunctionSnapShot = false;
        this.branchSearchDropDown.active = {
            id: this.utils.getBranchCode(),
            text: this.utils.getBranchText()
        };
        this.setControlValue('BranchNumber', this.branchSearchDropDown.active.id);
        this.setControlValue('LetterTypeCode', this.riExchange.getParentAttributeValue('LetterTypeCode'));
        this.riExchange.getParentHTMLValue('ContractTypeCode');
        this.riExchange.getParentHTMLValue('DateFrom');
        this.riExchange.getParentHTMLValue('DateTo');
        this.pageTitle = 'Letters For Type: ' + this.getControlValue('LetterTypeCode') + ' - ' + this.riExchange.getParentAttributeValue('LetterTypeDesc');
        this.utils.setTitle(this.pageTitle); this.riExchange.getParentAttributeValue('LetterTypeDesc');
        this.getContractTypes();
        this.buildGrid();
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('EffectDate', 'EffectDate', 'EffectDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('Contract', 'Contract', 'Contract', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('Name', 'Name', 'Name', MntConst.eTypeText, 25);
        //The following 'if' is presently not going to function as for this code, right now the parent won't navigate to this page. But, for future possibilities keeping the code here
        if (this.getControlValue('LetterTypeCode') === '06') {
            this.riGrid.AddColumn('Premise', 'Premise', 'Premise', MntConst.eTypeInteger, 0);
            this.riGrid.AddColumnAlign('Premise', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('Product', 'Product', 'Product', MntConst.eTypeText, 6);
        }
        this.riGrid.AddColumn('LastPrinted', 'LastPrinted', 'LastPrinted', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('LastPrinted', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('Print', 'Print', 'Print', MntConst.eTypeImage, 0);
        this.riGrid.Complete();
    }

    public riGridBeforeExecute(): void {
        let gridParams: QueryParams = new QueryParams();
        gridParams.set(this.serviceConstants.Action, '2');
        gridParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        gridParams.set(this.serviceConstants.CountryCode, this.countryCode());
        gridParams.set(this.serviceConstants.Function, 'LettersForType');
        gridParams.set('Level', 'Branch');
        gridParams.set('LetterTypeCode', this.getControlValue('LetterTypeCode'));
        gridParams.set('BranchNumber', this.getControlValue('BranchNumber'));
        gridParams.set('DateFrom', this.getControlValue('DateFrom'));
        gridParams.set('DateTo', this.getControlValue('DateTo'));
        gridParams.set('ContractFilterType', this.getControlValue('ContractTypeCode'));
        gridParams.set('MultiLanguageDocument', 'no');
        gridParams.set('LanguageCode', '');
        gridParams.set(this.serviceConstants.GridMode, '0');
        gridParams.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        gridParams.set(this.serviceConstants.PageSize, this.pageSize.toString());
        gridParams.set(this.serviceConstants.PageCurrent, this.pageCurrent.toString());
        gridParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridParams.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullerror']));
                        return;
                    }
                    this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                    this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageSize : 1;
                    this.riGrid.RefreshRequired();
                    this.riGrid.Execute(data);
                    this.itemsTotal = this.totalRecords;
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public onRefresh(): void {
        this.riGridBeforeExecute();
    }

    public onGenerateReport(event: any): void {
        let reportDestination: any = this.getControlValue('RepDest') === 'Email' ? 'email|User' : 'batch|ReportID';
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.BusinessCode, this.businessCode());
        query.set(this.serviceConstants.CountryCode, this.countryCode());
        //set parameters
        this.postData = {};
        this.postData.Description = this.riExchange.getParentAttributeValue('LetterTypeDesc');
        this.postData.ProgramName = this.riExchange.getParentAttributeValue('DocumentURL');
        this.postData.StartDate = this.utils.formatDate(new Date());
        this.postData.StartTime = StaticUtils.setTimeToMinutes();
        this.postData.Report = 'report';
        this.postData.ParameterName = 'BusinessCode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'BranchNumber' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'DateFrom' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'DateTo' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'ContractTypeCode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'RepManDest' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'ConsumablesOnly';
        //The ConsumablesOnly is hardcoded to 'True' because of the list of LetterTypeCodes hardcoded in parent screen. Presently for those codes which has a ConsumablesOnly set to True, only those will travel to this page
        this.postData.ParameterValue = this.businessCode() + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('BranchNumber') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('DateFrom') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('DateTo') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('ContractTypeCode') + StaticUtils.RI_SEPARATOR_VALUE_LIST + reportDestination + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'True';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module,
            this.queryParams.operation, query, this.postData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);

                    if (data && data['errorMessage']) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullerror']));
                    } else {
                        this.URLReturn = data['fullError'];
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullerror']));
                });
    }

    public getCurrentPage(event: any): void {
        this.pageCurrent = event.value;
        this.riGridBeforeExecute();
    }

    public onGridDblClick(event: any): void {
        let cellInfo = this.riGrid.CurrentColumnName;
        switch (cellInfo) {
            case 'Print':
                let query = this.getURLSearchParamObject();
                query.set(this.serviceConstants.Action, '6');
                query.set(this.serviceConstants.BusinessCode, this.businessCode());
                query.set(this.serviceConstants.CountryCode, this.countryCode());
                this.postData = {};
                this.postData[this.serviceConstants.Function] = 'ToggleCustomerLetterPrint';
                this.postData.RowID = this.riGrid.Details.GetAttribute('EffectDate', 'rowID');
                this.ajaxSource.next(this.ajaxconstant.START);
                this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module,
                    this.queryParams.operation, query, this.postData)
                    .subscribe(
                        (data) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            if (data && data['errorMessage']) {
                                this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullerror']));
                            } else {
                                this.onRefresh();
                                this.URLReturn = data['fullError'];
                            }
                        },
                        (error) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullerror']));
                        });
        }
    }

    public getContractTypes(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        let postData: Object = {};
        searchParams.set(this.serviceConstants.Action, '0');
        postData[this.serviceConstants.Function] = 'BuildContractTypeCombo';
        //making post request
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }
                    if (data.ContractTypeList) {
                        let contractTypeCodes: Array<string> = data.ContractTypeList.split(',');
                        for (let i = 0; i < contractTypeCodes.length; i++) {
                            let contractType: any = contractTypeCodes[i].split('|');
                            this.ContractTypeCodeList.push({ text: contractType[1], value: contractType[0] });
                        }
                        this.setControlValue('ContractTypeCode', this.riExchange.getParentHTMLValue('ContractTypeCode'));
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }
}
