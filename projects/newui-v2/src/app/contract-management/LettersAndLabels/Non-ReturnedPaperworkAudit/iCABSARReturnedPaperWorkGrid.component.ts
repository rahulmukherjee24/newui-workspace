import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { InternalGridSearchModuleRoutes } from './../../../base/PageRoutes';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { PaginationComponent } from './../../../../shared/components/pagination/pagination';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { BranchSearchComponent } from './../../../internal/search/iCABSBBranchSearch';

@Component({
    templateUrl: 'iCABSARReturnedPaperWorkGrid.html'
})

export class ReturnedPaperWorkGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private queryParams: any = {
        operation: 'ApplicationReport/iCABSARReturnedPaperWorkGrid',
        module: 'waste',
        method: 'service-delivery/grid'
    };
    public controls: Array<Object> = [
        { name: 'BranchNumber' },
        { name: 'BranchName' },
        { name: 'DateFrom', type: MntConst.eTypeDate },
        { name: 'DateTo', type: MntConst.eTypeDate },
        { name: 'ViewBy', value: 'Not Received' },
        { name: 'LetterTypeCode' },
        { name: 'LetterTypeDesc' }
    ];
    //Grid Component variables
    public gridParams: Object = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0,
        cacheRefresh: true
    };
    public pageId: string = '';
    public inputParams: any = {};
    public isHidePagination: boolean = true;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARRETURNEDPAPERWORKGRID;
        this.browserTitle = this.pageTitle = 'Non-Returned Paperwork Audit';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.riGridHandle = this.utils.randomSixDigitString();
    }

    ngAfterContentInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.buildGrid();
            this.setControlValue('ViewBy', this.getControlValue('ViewBy'));
        } else {
            this.windowOnload();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        let date: Date = new Date(), dateFrom: any, dateTo: any;

        this.riGrid.FunctionPaging = true;
        date.setMonth(new Date().getMonth() - 1);
        dateFrom = this.globalize.parseDateToFixedFormat(date).toString();
        dateTo = this.globalize.parseDateToFixedFormat(new Date()).toString();
        this.setControlValue('DateFrom', dateFrom);
        this.setControlValue('DateTo', dateTo);
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchText());
    }

    // Build Grid Options
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('LetterTypeCode', 'ReturnedPaperwork', 'LetterTypeCode', MntConst.eTypeCode, 5);
        this.riGrid.AddColumn('LetterTypeDesc', 'ReturnedPaperwork', 'LetterTypeDesc', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('Returned#', 'ReturnedPaperwork', 'Returned#', MntConst.eTypeText, 8);
        this.riGrid.AddColumnAlign('LetterTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('LetterTypeDesc', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Returned#', MntConst.eAlignmentCenter);
        this.riGrid.Complete();
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    // Load Grid Data
    private loadGridData(): void {
        let search: QueryParams = this.getURLSearchParamObject();

        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.GridMode, this.gridParams['riGridMode']);
        search.set(this.serviceConstants.GridHandle, this.pageParams.riGridHandle);
        search.set(this.serviceConstants.PageSize, this.gridParams['itemsPerPage']);
        search.set(this.serviceConstants.PageCurrent, this.gridParams['pageCurrent']);
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        if (this.gridParams['cacheRefresh']) {
            search.set(this.serviceConstants.GridCacheRefresh, 'true');
        }

        search.set('BranchNumber', this.getControlValue('BranchNumber'));
        search.set('ViewBy', this.getControlValue('ViewBy'));
        search.set('DateFrom', this.getControlValue('DateFrom'));
        search.set('DateTo', this.getControlValue('DateTo'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.gridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * this.gridParams['itemsPerPage'] : 1;
                    this.riGrid.UpdateBody = true;
                    this.riGrid.UpdateFooter = true;
                    this.riGrid.UpdateHeader = true;
                    this.riGrid.RefreshRequired();
                    this.riGrid.Execute(data);
                    if (data.pageData && (data.pageData.lastPageNumber * 10) > 0) {
                        this.isHidePagination = false;
                    } else {
                        this.isHidePagination = true;
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });

    }

    // Double click functionality in grid
    public onGridDblClick(data: any): void {
        if (this.riGrid.CurrentColumnName === 'LetterTypeCode') {
            this.setControlValue('LetterTypeCode', this.riGrid.Details.GetValue('LetterTypeCode'));
            this.setControlValue('LetterTypeDesc', this.riGrid.Details.GetAttribute('LetterTypeCode', 'additionalproperty'));
            this.navigate('ReturnedPaperwork', 'grid/' + InternalGridSearchModuleRoutes.ICABSARRETURNEDPAPERWORKTYPEGRID);
        }
    }

    // To get the current page in grid
    public getCurrentPage(currentPage: any): void {
        this.gridParams['cacheRefresh'] = false;
        this.gridParams['pageCurrent'] = currentPage.value;
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    public gridRefresh(): void {
        this.gridParams['cacheRefresh'] = true;
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }

    // On dropdown change
    public selectedOption(event: string): void {
        this.setControlValue('ViewBy', event);
    }

    //On DateFrom change
    public dateFromSelectedValue(value: any): void {
        if (value && value.value) {
            this.setControlValue('DateFrom', value.value);
        }
    }

    //On DateTo change
    public dateToSelectedValue(value: any): void {
        if (value && value.value) {
            this.setControlValue('DateTo', value.value);
        }
    }
}
