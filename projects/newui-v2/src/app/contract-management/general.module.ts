import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { ContractManagementModuleRoutes } from './../base/PageRoutes';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { SalesAreaPostcodeRezoneGridComponent } from './SalesAdjustments/PortfolioRezone/iCABSBSalesAreaPostcodeRezoneGrid.component';
import { SalesAreaRezoneGridComponent } from './SalesAdjustments/PortfolioRezone/iCABSBSalesAreaRezoneGrid.component';

@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class GeneralRootComponent {
    constructor(viewContainerRef: ViewContainerRef) { }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: GeneralRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSBSALESAREAPOSTCODEREZONEGRID_SUB, component: SalesAreaPostcodeRezoneGridComponent },
                    { path: ContractManagementModuleRoutes.ICABSBSALESAREAREZONEGRID_SUB, component: SalesAreaRezoneGridComponent }
                ], data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        GeneralRootComponent,
        SalesAreaPostcodeRezoneGridComponent,
        SalesAreaRezoneGridComponent
    ]
})

export class GeneralModule { }
