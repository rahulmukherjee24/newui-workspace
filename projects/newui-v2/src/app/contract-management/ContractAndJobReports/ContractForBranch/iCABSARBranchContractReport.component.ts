import { Component, OnInit, Injector, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';

import { PageIdentifier } from './../../../base/PageIdentifier';
import { BaseComponent } from '../../../../app/base/BaseComponent';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { ContractSearchComponent } from '../../../internal/search/iCABSAContractSearch';
import { PremiseSearchComponent } from '../../../internal/search/iCABSAPremiseSearch';
import { ProductSearchGridComponent } from '../../../internal/search/iCABSBProductSearch';
import { AjaxConstant } from '../../../../shared/constants/AjaxConstants';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { StaticUtils } from '../../../../shared/services/static.utility';

@Component({
    templateUrl: 'iCABSARBranchContractReport.html'
})

export class BranchContractReportComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('ContractSearch') ContractSearch;
    @ViewChild('productSearch') public productSearch;
    public pageId: string = '';
    public isDateEnabled: boolean = false;
    public isRequesting: boolean;
    public thInformation: any;
    public thInformationDisplayed: boolean = false;
    public isServicingBranchReadOnly: boolean = true;
    public btnDisableSubmit: boolean = true;
    public destinationType: any = {
        type: 'batch|ReportID'
    };
    public controls = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, required: true, disabled: false },
        { name: 'ContractName', type: MntConst.eTypeText, required: false, disabled: true, readonly: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'IncludeContractDetails', type: MntConst.eTypeCheckBox, value: true, disabled: true },
        { name: 'IncludeDeletedDetails', type: MntConst.eTypeCheckBox, value: true, disabled: true },
        { name: 'IncludeContractHistory', type: MntConst.eTypeCheckBox, value: false, disabled: true },
        { name: 'IncludeVisitHistory', type: MntConst.eTypeCheckBox, value: false, disabled: true },
        { name: 'IncludeProductLocations', type: MntConst.eTypeCheckBox, value: false, disabled: true },
        { name: 'RepDest', disabled: true },
        { name: 'ServiceBranchNumber', disabled: true },
        { name: 'IncludeCustomerInformation', type: MntConst.eTypeCheckBox, value: false, disabled: true },
        { name: 'IncludeServiceShedule', type: MntConst.eTypeCheckBox, disabled: true },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true, disabled: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true, disabled: true },
        { name: 'contractDetailsFor', readonly: false, required: false, disabled: true }
    ];

    public ellipsisConfig = {
        contract: {
            autoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp',
                'ContractNumber': '',
                'ContractName': '',
                'showAddNew': false,
                'currentContractType': 'C',
                'disabled': false
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: ContractSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        },
        premise: {
            disabled: false,
            showHeader: true,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp',
                'ContractNumber': '',
                'ContractName': '',
                'showAddNew': false
            },
            component: PremiseSearchComponent
        },
        product: {
            disabled: false,
            showHeader: true,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp',
                'ProductSaleGroupCode': ''
            },
            component: ProductSearchGridComponent
        }
    };

    ngAfterViewInit(): void {
        this.ellipsisConfig.contract.autoOpen = true;
    }

    public dropdownConfig = {
        branch: {
            inputParamsBranch: {},
            negBranchNumberSelected: {
                id: '',
                text: ''
            }
        }
    };

    public queryParams: any = {
        operation: 'ApplicationReport/iCABSARBranchContractReport',
        module: 'report',
        method: 'contract-management/maintenance'
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARBRANCHCONTRACTREPORT;
        this.browserTitle = 'Contract Details Report';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /*
    Method invoked On selecting Premise from ellipsis
    */
    public onPremiseSelect(data: any): void {
        if (data.PremiseNumber)
            this.setControlValue('PremiseNumber', data.PremiseNumber);
    }

    /*
    Method invoked on selecting branch from dropdown
    */
    public onBranchDataReceived(obj: any): void {
        if (obj) {
            if (obj.BranchNumber)
                this.setControlValue('ServiceBranchNumber', obj.BranchNumber);
        }
    }

    /*
    Method invoked on selecting Product from ellipsis
    */
    public onProductSelect(data: any): void {
        if (data.ProductCode)
            this.setControlValue('ProductCode', data.ProductCode);
    }

    /*
    Method invoked on checking IncludeVisitHistory checkbox
    */
    public onIncludeVisitHistoryClick(event: any): void {
        if (event.target.checked)
            this.isDateEnabled = true;
        else {
            this.isDateEnabled = false;
            this.setControlValue('DateFrom', '');
            this.setControlValue('DateTo', '');
            this.uiForm.controls['DateFrom'].markAsUntouched();
            this.uiForm.controls['DateTo'].markAsUntouched();
        }
    }

    public onIncludeServiceSheduleClick(): void {
        this.isDateEnabled = this.getControlValue('IncludeServiceShedule');
        if (this.isDateEnabled) {
            this.setControlValue('DateFrom', '');
            this.setControlValue('DateTo', '');
            this.uiForm.controls['DateFrom'].markAsUntouched();
            this.uiForm.controls['DateTo'].markAsUntouched();
        }
    }

    /*
    Method invoked on changing report destination
    */
    public onRepDestChange(event: any): void {
        if (this.getControlValue('RepDest') === 'direct') {
            this.destinationType['type'] = 'batch|ReportID';
        } else {
            this.destinationType['type'] = 'email|User';
        }
    }

    /*
    Method invoked after receiving contractNumber
    */
    public onContractKeyDown(event: any, call: boolean): void {
        if (call) {
            if (event.srcElement.value) {
                let search = this.getURLSearchParamObject();
                search.set(this.serviceConstants.Action, '0');
                search.set('ContractNumber', event.srcElement.value);
                this.ajaxSource.next(AjaxConstant.START);
                this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
                    .subscribe(
                        (e) => {
                            this.ajaxSource.next(AjaxConstant.COMPLETE);
                            if (e.hasError) {
                                this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                                this.setControlValue('ContractName', '');
                                this.ellipsisConfig.premise.childConfigParams.ContractName = '';
                            } else {
                                this.setControlValue('ContractName', e.ContractName);
                                this.ellipsisConfig.premise.childConfigParams.ContractNumber = e.ContractNumber;
                                this.ellipsisConfig.premise.childConfigParams.ContractName = e.ContractName;
                            }
                        },
                        (error) => {
                            this.ajaxSource.next(AjaxConstant.COMPLETE);
                            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        });
            } else {
                this.setControlValue('ContractName', '');
                this.setControlValue('contractDetailsFor', '');
                this.ellipsisConfig.premise.childConfigParams.ContractNumber = '';
                this.ellipsisConfig.premise.childConfigParams.ContractName = '';
                this.disableControls(['ContractNumber', 'contractDetailsFor']);
                this.btnDisableSubmit = !(this.getControlValue('ContractNumber') && this.getControlValue('contractDetailsFor'));
                this.isServicingBranchReadOnly = true;
            }
        } else {
            if (event.ContractNumber) {
                this.disableControl('contractDetailsFor', false);
                if (this.getControlValue('contractDetailsFor')) {
                    this.onContractDetailsForOnChange();
                }
                this.setControlValue('ContractNumber', event.ContractNumber);
                this.ellipsisConfig.premise.childConfigParams.ContractNumber = event.ContractNumber;
                if (event.ContractName) {
                    this.setControlValue('ContractName', event.ContractName);
                    this.ellipsisConfig.premise.childConfigParams.ContractName = event.ContractName;
                }
            } else {
                this.ellipsisConfig.premise.childConfigParams.ContractNumber = '';
                this.ellipsisConfig.premise.childConfigParams.ContractName = '';
            }
        }
        this.disableControl('contractDetailsFor', this.getControlValue('ContractNumber') === '');
        this.btnDisableSubmit = !(this.getControlValue('ContractNumber') && this.getControlValue('contractDetailsFor'));
        this.setControlValue('PremiseNumber', '');
        this.setControlValue('ProductCode', '');
        this.dropdownConfig.branch.inputParamsBranch = {};
        this.dropdownConfig.branch.negBranchNumberSelected = {
            id: '',
            text: ''
        };
        this.setControlValue('DateFrom', '');
        this.setControlValue('DateTo', '');
        this.uiForm.controls['DateFrom'].markAsUntouched();
        this.uiForm.controls['DateTo'].markAsUntouched();
        this.isDateEnabled = false;
        this.setControlValue('IncludeContractDetails', true);
        this.setControlValue('IncludeDeletedDetails', true);
        this.setControlValue('IncludeContractHistory', false);
        this.setControlValue('IncludeVisitHistory', false);
        this.setControlValue('IncludeProductLocations', false);
        this.setControlValue('IncludeCustomerInformation', false);
    }

    /*
    Method invoked on clicking Submit button
    */
    public onSubmitReportRequest(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            let postSearchParams = this.getURLSearchParamObject();
            let contractDetailsForVal = this.getControlValue('contractDetailsFor');
            postSearchParams.set(this.serviceConstants.Action, '2');
            let postParams: any = {};
            postParams.Description = contractDetailsForVal + ' Contract Report';
            postParams.ProgramName = (contractDetailsForVal === 'Branch') ? 'iCABSBranchContractReportGeneration.p' : 'iCABSCustomerContractReportGeneration.p';
            let date = new Date();
            postParams.StartDate = this.utils.formatDate(new Date());
            postParams.StartTime = StaticUtils.setTimeToMinutes();
            postParams.Report = 'report';
            postParams.ParameterName = contractDetailsForVal === 'Branch' ? 'Mode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'BusinessCode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'ContractNumber' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'ServiceBranchNumber' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'ProductCode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'PremiseNumber' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'IncludeContractDetails' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'IncludeDeletedDetails' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'IncludeContractHistory' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'IncludeVisitHistory' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'IncludeProductLocations' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'IncludeCustomerInformation' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'LoggedInBranch' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'VisitDateFrom' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'VisitDateTo' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'RepManDest' : 'Mode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'BusinessCode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'ContractNumber' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'ServiceBranchNumber' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'ProductCode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'PremiseNumber' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'IncludeContractDetails' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'IncludeContractHistory' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'IncludeVisitHistory' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'IncludeProductLocations' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'RepManDest' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'DateFrom' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'DateTo' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'IncludeServiceShedule';

            postParams.ParameterValue = contractDetailsForVal === 'Branch' ? contractDetailsForVal + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.businessCode() + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('ContractNumber') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('ServiceBranchNumber') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('ProductCode') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('PremiseNumber') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('IncludeContractDetails') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('IncludeDeletedDetails') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('IncludeContractHistory') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('IncludeVisitHistory') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('IncludeProductLocations') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('IncludeCustomerInformation') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.utils.getBranchCode() + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.globalize.parseDateToFixedFormat(this.getControlValue('DateFrom')) + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.globalize.parseDateToFixedFormat(this.getControlValue('DateTo')) + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.destinationType['type'] : contractDetailsForVal + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.businessCode() + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('ContractNumber') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('ServiceBranchNumber') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('ProductCode') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('PremiseNumber') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('IncludeContractDetails') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('IncludeContractHistory') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('IncludeVisitHistory') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('IncludeProductLocations') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.destinationType['type'] + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('DateFrom') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('DateTo') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('IncludeServiceShedule');

            this.ajaxSource.next(AjaxConstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
                .subscribe(
                    (e) => {
                        this.ajaxSource.next(AjaxConstant.COMPLETE);
                        if (e.hasError) {
                            this.thInformation = e.fullError;
                            this.thInformationDisplayed = true;
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(AjaxConstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage));
                    });
        }
    }

    public onContractDetailsForOnChange(): void {
        if ((this.getControlValue('contractDetailsFor')) && this.getControlValue('ContractNumber')) {
            this.clearControls(['ContractNumber', 'ContractName', 'contractDetailsFor', 'IncludeContractDetails', 'IncludeDeletedDetails', 'RepDest']);
            if (this.getControlValue('contractDetailsFor') === 'Branch') {
                this.setControlValue('IncludeDeletedDetails', true);
                //This is required when the page is opened and user selects Branch option on Contract Details For (contractDetailsFor) directly
                this.enableControls(['IncludeServiceShedule', 'ContractName']);
                this.disableControl('IncludeServiceShedule', true);
                //This is required is Contract Details For (contractDetailsFor) is changed from Customer to Branch
                this.btnDisableSubmit = false;
                this.isServicingBranchReadOnly = false;
                this.isDateEnabled = false;
            } else if (this.getControlValue('contractDetailsFor') === 'Customer') {
                this.setControlValue('IncludeDeletedDetails', false);
                //This is required when the page is opened and user selects Customer option on Contract Details For (contractDetailsFor) directly
                this.enableControls(['IncludeDeletedDetails', 'IncludeContractHistory', 'IncludeVisitHistory', 'IncludeCustomerInformation']);
                //This is required is Contract Details For (contractDetailsFor) is changed from Branch to Customer
                this.disableControls(['ContractNumber', 'PremiseNumber', 'ProductCode', 'IncludeContractDetails', 'IncludeProductLocations', 'IncludeServiceShedule', 'DateFrom', 'DateTo', 'RepDest']);
                this.disableControl('contractDetailsFor', false);
                this.isServicingBranchReadOnly = false;
                this.isDateEnabled = false;
            }
            this.btnDisableSubmit = false;
        } else {
            this.clearControls(['ContractNumber', 'ContractName', 'contractDetailsFor', 'IncludeContractDetails', 'IncludeDeletedDetails', 'RepDest']);
            this.disableControls(['contractDetailsFor', 'ContractNumber']);
            this.btnDisableSubmit = true;
            this.isServicingBranchReadOnly = true;
            this.isDateEnabled = false;
        }
    }
}
