import { Component, Injector, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { QueryParams } from './../../../../shared/services/http-params-wrapper';

import { PageIdentifier } from './../../../base/PageIdentifier';
import { BaseComponent } from '../../../../app/base/BaseComponent';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceSalesModuleRoutes } from './../../../base/PageRoutes';


@Component({
    templateUrl: 'iCABSBSalesAreaGrid.html'
})

export class SalesAreaGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private xhrParams = {
        module: 'contract-admin',
        method: 'contract-management/maintenance',
        operation: 'Business/iCABSBSalesAreaGrid'
    };

    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'BusinessCode', readonly: false, disabled: true, required: false, type: MntConst.eTypeCode, ignoreSubmit: false },
        { name: 'BusinessDesc', readonly: false, disabled: true, required: false, ignoreSubmit: true },
        { name: 'BranchNumber', readonly: false, disabled: true, required: false, type: MntConst.eTypeCode, ignoreSubmit: false },
        { name: 'BranchName', readonly: false, disabled: true, required: false, ignoreSubmit: true },
        { name: 'ActiveOnly' },
        { name: 'SearchType' },
        { name: 'SearchValue' },
        { name: 'menu', readonly: false, disabled: false, required: false, ignoreSubmit: true }
    ];
    public search: QueryParams = new QueryParams();
    public pageSize = 10;
    public curPage: number = 1;
    public totalRecords: number;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBSALESAREAGRID;
        this.browserTitle = 'Sales Area';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    ngAfterViewInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.buildGrid();
            this.riGridBeforeExecute();
        } else {
            this.windowOnLoad();
        }
        this.setControlValue('menu', 'Options');
    }

    private windowOnLoad(): void {
        this.setControlValue('BusinessCode', this.utils.getBusinessCode());
        this.setControlValue('BusinessDesc', this.utils.getBusinessText());
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.getBranchName());
        this.setControlValue('SearchType', 'Code');
        this.setControlValue('ActiveOnly', true);
        this.riGrid.DefaultBorderColor = 'ADD8E6';
        this.riGrid.DefaultTextColor = '0000FF';
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = 10;
        this.riGrid.FunctionUpdateSupport = true;
        this.buildGrid();
        this.riGridBeforeExecute();

    }

    private getBranchName(): string {
        let branchName = this.utils.getBranchText();
        if (branchName) {
            let branchDesc = this.utils.getBranchText().split('-');
            if (branchDesc && branchDesc.length > 0) {
                branchName = branchDesc[1];
            }
            if (branchName) {
                branchName = branchName.trim();
            }
        }
        return branchName;
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('SalesAreaCode', 'SalesArea', 'SalesAreaCode', MntConst.eTypeCode, 8, false, '');
        this.riGrid.AddColumnOrderable('SalesAreaCode', true);
        this.riGrid.AddColumn('SalesAreaDesc', 'SalesArea', 'SalesAreaDesc', MntConst.eTypeText, 30, false, '');
        this.riGrid.AddColumn('EmployeeCode', 'SalesArea', 'EmployeeCode', MntConst.eTypeCode, 15, false, '');
        this.riGrid.AddColumnOrderable('EmployeeCode', true);
        this.riGrid.AddColumn('EmployeeSurname', 'BranchServiceArea', 'EmployeeSurname', MntConst.eTypeText, 30, false, '');
        this.riGrid.AddColumn('LiveArea', 'SalesArea', 'LiveArea', MntConst.eTypeImage, 10, false, '');
        this.riGrid.Complete();
    }

    private riGridBeforeExecute(): void {
        this.riGrid.RefreshRequired();
        //Grid general parameter settings
        this.search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        this.search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        this.search.set(this.serviceConstants.Action, '2');
        this.search.set(this.serviceConstants.BranchNumber, this.utils.getBranchCode());
        this.search.set(this.serviceConstants.GridCacheRefresh, 'True');
        this.search.set(this.serviceConstants.GridMode, '0');
        this.search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        this.search.set(this.serviceConstants.PageSize, this.pageSize.toString());
        this.search.set(this.serviceConstants.PageCurrent, this.curPage.toString());
        this.search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        //Page specific parameter settings
        this.search.set('ActiveOnly', this.getControlValue('ActiveOnly'));
        this.search.set('SearchType', this.getControlValue('SearchType'));
        this.search.set('SearchValue', this.getControlValue('SearchValue'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, this.search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data) {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        if (this.riGrid.Update) {
                            this.riGrid.StartRow = this.getAttribute('SelectedLineRow');
                            this.riGrid.StartColumn = 0;
                            this.riGrid.RowID = this.getAttribute('SelectedLineRowID');
                            this.riGrid.UpdateBody = true;
                            this.riGrid.UpdateFooter = false;
                            this.riGrid.UpdateHeader = false;
                        }
                        this.curPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageSize : 1;
                        this.riGrid.Execute(data);
                    }
                }
            },
            (error) => {
                this.logger.log('Error', error);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });

    }

    private gridFocus(rsrcElement: any): void {
        rsrcElement.select();
        this.setAttribute('SelectedLineSequence', rsrcElement.value);
        this.setAttribute('SelectedLineRowID', rsrcElement.getAttribute('RowID'));
        this.setAttribute('SelectedLineRow', rsrcElement.parentElement.parentElement.sectionRowIndex);
        rsrcElement.focus();
        rsrcElement.select();
    }

    public riGridAfterExecute(): void {
        if (!this.riGrid.Update) {
            if (this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.children[0]) {
                if (this.riGrid.HTMLGridBody.children[0].children[0]) {
                    if (this.riGrid.HTMLGridBody.children[0].children[0].children[0]) {
                        if (this.riGrid.HTMLGridBody.children[0].children[0].children[0].children[0]) {
                            this.gridFocus(this.riGrid.HTMLGridBody.children[0].children[0].children[0].children[0]);
                        }
                    }
                }
            }
        }
    }

    public tbodyOnDblclick(event: any): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'SalesAreaCode':
                this.gridFocus(event.srcElement.parentElement.parentElement.parentElement.children[0].children[0].children[0]);
                this.riExchange.Mode = 'SalesAreaUpdate';
                this.navigate('SalesAreaUpdate', InternalMaintenanceSalesModuleRoutes.ICABSBSALESAREAMAINTENANCE,
                    {
                        RowID: this.getAttribute('SelectedLineRowID')
                    });
                break;
        }
    }

    public riGridSort(event: any): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public getCurrentPage(currentPage: any): void {
        this.curPage = currentPage.value;
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public menuOnchange(value: any): void {
        switch (value) {
            case 'Add':
                this.riExchange.Mode = 'SalesAreaAdd';
                this.navigate('SalesAreaAdd', InternalMaintenanceSalesModuleRoutes.ICABSBSALESAREAMAINTENANCE);
                break;
        }
    }
}
