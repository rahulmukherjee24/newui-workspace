import { Component, OnInit, Injector, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../../shared/services/http-params-wrapper';

import { SalesAreaSearchComponent } from './../../../internal/search/iCABSBSalesAreaSearch.component';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { BaseComponent } from '../../../base/BaseComponent';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';

@Component({
    templateUrl: 'iCABSBSalesAreaRezoneGrid.html'
})

export class SalesAreaRezoneGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('SalesAreaCode') salesAreaCode: any;

    private isError: boolean = false;

    public pageId: string = '';
    public itemsPerPage: number = 10;
    public curPage: number = 1;
    public totalRecords: number = 1;
    public batchInformation: string;
    public isBatchInformationShown: boolean = false;

    public ellipsisConfig = {
        codeFrom: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'showAddNew': false
            },
            component: SalesAreaSearchComponent
        },
        codeTo: {
            childConfigParams: {
                'parentMode': 'LookUp-To',
                'showAddNew': false
            },
            component: SalesAreaSearchComponent
        }
    };

    public controls = [
        { name: 'SalesAreaCode', type: MntConst.eTypeCode },
        { name: 'SalesAreaDesc', disabled: true },
        { name: 'SalesAreaCodeTo', type: MntConst.eTypeCode },
        { name: 'SalesAreaDescTo', disabled: true },
        { name: 'Town' },
        { name: 'County' },
        { name: 'cmdRezone', disabled: true }
    ];

    private headerParams: any = {
        method: 'prospect-to-contract/maintenance',
        module: 'sales',
        operation: 'Business/iCABSBSalesAreaRezoneGrid'
    };

    constructor(injector: Injector) {
        super(injector);
        this.browserTitle = 'Sales Area Postcode Rezoning';
        this.pageId = PageIdentifier.ICABSBSALESAREAREZONEGRID;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterViewInit(): void {
        this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.buildGrid();
        this.salesAreaCode.nativeElement.focus();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.pageParams.gridHandle = this.utils.gridHandle;
        this.riGrid.AddColumn('Postcode', 'Postcode', 'Postcode', MntConst.eTypeCode, 9, true);
        this.riGrid.AddColumnAlign('Postcode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('State', 'Postcode', 'State', MntConst.eTypeText, 15, true);
        this.riGrid.AddColumnAlign('State', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('Town', 'Postcode', 'Town', MntConst.eTypeText, 15, true);
        this.riGrid.AddColumnAlign('Town', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('SalesAreaCode', 'Postcode', 'SalesAreaCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('SalesAreaCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('SalesAreaDesc', 'Postcode', 'SalesAreaDesc', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('SalesAreaDesc', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('EmployeeCode', 'Postcode', 'EmployeeCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('EmployeeSurname', 'Postcode', 'EmployeeSurname', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('Rezoned', 'Postcode', 'Rezoned', MntConst.eTypeImage, 1, false, 'Click here to rezone');
        this.riGrid.AddColumnAlign('Rezoned', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('Postcode', true);
        this.riGrid.AddColumnOrderable('State', true);
        this.riGrid.AddColumnOrderable('Town', true);
        this.riGrid.Complete();
    }

    private onPromptOK(): void {
        this.isError = false;
        this.rezoneGroup();
    }

    private onPromptCancel(): void {
        this.isError = true;
    }

    private riGridBeforeExecute(): void {
        console.log('riGridBeforeExecute');
        let gridParams: QueryParams = new QueryParams();
        gridParams.set(this.serviceConstants.Action, '2');
        gridParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        gridParams.set(this.serviceConstants.CountryCode, this.countryCode());
        gridParams.set('BranchNumber', this.utils.getBranchCode());
        gridParams.set('SalesAreaCode', this.getControlValue('SalesAreaCode'));
        gridParams.set('SalesAreaCodeTo', this.getControlValue('SalesAreaCodeTo'));
        gridParams.set('Town', this.getControlValue('Town'));
        gridParams.set('County', this.getControlValue('County'));
        if (this.riGrid.Update) {
            gridParams.set('RowID', this.getAttribute('PostcodeRowID'));
        }
        gridParams.set(this.serviceConstants.GridMode, '0');
        gridParams.set(this.serviceConstants.GridHandle, this.pageParams.gridHandle);
        gridParams.set(this.serviceConstants.PageSize, this.itemsPerPage.toString());
        gridParams.set(this.serviceConstants.PageCurrent, this.curPage.toString());
        gridParams.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        gridParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, gridParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGrid.RefreshRequired();
                    if (this.riGrid.Update) {
                        this.riGrid.StartRow = this.getAttribute('Row');
                        this.riGrid.StartColumn = 0;
                        this.riGrid.RowID = this.getAttribute('PostcodeRowID');

                        this.riGrid.UpdateHeader = false;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = false;
                    } else {
                        this.curPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    }
                    this.riGrid.Execute(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
            });
    }

    private postcodeFocus(rsrcElement: any): void {
        let oTR;
        oTR = rsrcElement.parentElement.parentElement.parentElement;
        rsrcElement.focus();
        this.pageParams.PostcodeRowID = oTR.children[0].getAttribute('AdditionalProperty');
        this.pageParams.Row = oTR.sectionRowIndex;

        this.setAttribute('PostcodeRowID', oTR.children[0].getAttribute('AdditionalProperty'));
        this.setAttribute('Row', oTR.sectionRowIndex);
    }

    private populateDescriptions(): void {
        let searchParams: QueryParams = new QueryParams();
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());

        let bodyParams: any = {};
        bodyParams[this.serviceConstants.Function] = 'ReturnSalesAreaDescs';
        bodyParams['BranchNumber'] = this.utils.getBranchCode();
        bodyParams['SalesAreaCode'] = this.getControlValue('SalesAreaCode');
        bodyParams['SalesAreaCodeTo'] = this.getControlValue('SalesAreaCodeTo');
        bodyParams['SalesAreaDesc'] = this.getControlValue('SalesAreaDesc');
        bodyParams['SalesAreaDescTo'] = this.getControlValue('SalesAreaDescTo');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('SalesAreaDesc', data.SalesAreaDesc);
                    this.setControlValue('SalesAreaDescTo', data.SalesAreaDescTo);
                    this.buildGrid();
                    this.riGrid.RefreshRequired();
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private confirmRezone(): void {
        let searchParams: QueryParams = new QueryParams();
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());

        let bodyParams: any = {};
        bodyParams[this.serviceConstants.Function] = 'ConfirmRezone';
        bodyParams['BranchNumber'] = this.utils.getBranchCode();
        bodyParams['SalesAreaCode'] = this.getControlValue('SalesAreaCode');
        bodyParams['SalesAreaCodeTo'] = this.getControlValue('SalesAreaCodeTo');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    let promptModal = new ICabsModalVO(data.ErrorMessage, null, this.onPromptOK.bind(this), this.onPromptCancel.bind(this));
                    promptModal.title = data.MessageTitle;
                    this.modalAdvService.emitPrompt(promptModal);
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    public riGridBodyOnClick(event: MouseEvent): void {
        console.log(this.riGrid.CurrentColumnName);
        if (this.riGrid.CurrentColumnName === 'Rezoned') {
            this.postcodeFocus(event.srcElement);

            let searchParams: QueryParams = new QueryParams();
            searchParams.set(this.serviceConstants.Action, '2');
            searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
            searchParams.set(this.serviceConstants.CountryCode, this.countryCode());

            let bodyParams: any = {};
            bodyParams[this.serviceConstants.Function] = 'Rezone';
            bodyParams['BranchNumber'] = this.utils.getBranchCode();

            if (this.riGrid.Details.GetValue('Rezoned') !== 'yes') {
                bodyParams['SalesAreaCode'] = this.getControlValue('SalesAreaCode');
                bodyParams['SalesAreaCodeTo'] = this.getControlValue('SalesAreaCodeTo');
            } else {
                bodyParams['SalesAreaCode'] = this.getControlValue('SalesAreaCodeTo');
                bodyParams['SalesAreaCodeTo'] = this.getControlValue('SalesAreaCode');
            }
            bodyParams['PostcodeRowID'] = event.srcElement.getAttribute('RowID');
            bodyParams['Postcode'] = this.riGrid.Details.GetValue('Postcode');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.riGrid.Update = true;
                        this.riGridBeforeExecute();
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
        }
    }

    public salesAreaCodeOnchange(): void {
        this.populateDescriptions();
    }

    public salesAreaCodeToOnchange(): void {
        this.populateDescriptions();
    }

    public cmdRezoneOnclick(): void {
        let postcodeDefaultingEnabled;
        this.isError = false;
        if (!this.getControlValue('SalesAreaCode')) {
            this.riExchange.riInputElement.isError(this.uiForm, 'SalesAreaCode');
            this.isError = true;
        }
        if (!this.getControlValue('SalesAreaCodeTo')) {
            this.riExchange.riInputElement.isError(this.uiForm, 'SalesAreaCodeTo');
            this.isError = true;
        }

        if (!this.isError) {
            let searchParams: QueryParams = new QueryParams();
            searchParams.set(this.serviceConstants.Action, '2');
            searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
            searchParams.set(this.serviceConstants.CountryCode, this.countryCode());

            let bodyParams: any = {};
            bodyParams[this.serviceConstants.Function] = 'PostcodeDefaultingEnabled';
            bodyParams['BranchNumber'] = this.utils.getBranchCode();
            bodyParams['SalesAreaCode'] = this.getControlValue('SalesAreaCode');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        postcodeDefaultingEnabled = data.PostcodeDefaultingEnabled;
                        if (postcodeDefaultingEnabled === 'Y') {
                            this.confirmRezone();
                        } else {
                            this.rezoneGroup();
                        }
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
        }
    }

    public townOnChange(): void {
        this.disableControl('cmdRezone', true);
    }

    public countyOnChange(): void {
        this.disableControl('cmdRezone', true);
    }

    public riGridAfterExecute(): void {
        this.disableControl('cmdRezone', false);
    }

    private rezoneGroup(): void {
        let searchParams: QueryParams = new QueryParams();
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());

        let bodyParams: any = {};
        bodyParams[this.serviceConstants.Function] = 'RezoneGroup';
        bodyParams['BranchNumber'] = this.utils.getBranchCode();
        bodyParams['SalesAreaCode'] = this.getControlValue('SalesAreaCode');
        bodyParams['SalesAreaCodeTo'] = this.getControlValue('SalesAreaCodeTo');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGridBeforeExecute();

                    this.isBatchInformationShown = true;
                    this.batchInformation = data.BatchInformation;
                    this.modalAdvService.emitMessage(new ICabsModalVO(data.SubmitMessage));
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    public onSalesAreaSelect(data: any, type: string): void {
        switch (type) {
            case 'from':
                this.setControlValue('SalesAreaCode', data.SalesAreaCode);
                this.setControlValue('SalesAreaDesc', data.SalesAreaDesc);
                break;

            case 'to':
                this.setControlValue('SalesAreaCodeTo', data.SalesAreaCode);
                this.setControlValue('SalesAreaDescTo', data.SalesAreaDesc);
                break;
        }
    }

    public onRefresh(): void {
        this.riGridBeforeExecute();
    }

    public getCurrentPage(data: any): void {
        if (this.curPage !== data.value) {
            this.curPage = data.value;
            this.riGridBeforeExecute();
        }
    }

}
