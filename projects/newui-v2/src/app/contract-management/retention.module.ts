import { InactiveServiceCoverInfoMaintenanceComponent } from './ClientRetention/ServiceCover-AmendDeletion/iCABSAInactiveServiceCoverInfoMaintenance.component';
import { NgModule, Component } from '@angular/core';
import { ContractManagementModuleRoutes } from './../base/PageRoutes';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { PremiseSelectMaintenanceComponent } from './ClientRetention/Premises/DeletePremises/iCABSAPremiseSelectMaintenance.component';
import { ServiceCoverSelectMaintenanceComponent } from './ClientRetention/iCABSAServiceCoverSelectMaintenance';
import { ContractSelectMaintenanceComponent } from './ClientRetention/iCABSAContractSelectMaintenance';
import { InactiveContractInfoMaintenanceComponent } from './ClientRetention/Contract-AmendTermination/iCABSAInactiveContractInfoMaintenance.component';
import { InactivePremiseInfoMaintenanceComponent } from './ClientRetention/Premises - Amend Deletion/iCABSAInactivePremiseInfoMaintenance.component';
import { HttpClientModule } from '@angular/common/http';

@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class RetentionRootComponent {
    constructor() {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: RetentionRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSASERVICECOVERSELECTMAINTENANCE_SUB, component: ServiceCoverSelectMaintenanceComponent },
                    { path: ContractManagementModuleRoutes.ICABSACONTRACTSELECTMAINTENANCE_SUB, component: ContractSelectMaintenanceComponent },
                    { path: ContractManagementModuleRoutes.ICABSAPREMISESELECTMAINTENANCE_SUB, component: PremiseSelectMaintenanceComponent },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVESERVICECOVERINFOMAINTENANCE_SUB.DEFAULT, component: InactiveServiceCoverInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVESERVICECOVERINFOMAINTENANCE_SUB.CANCEL, component: InactiveServiceCoverInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVESERVICECOVERINFOMAINTENANCE_SUB.REINSTATE, component: InactiveServiceCoverInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVESERVICECOVERINFOMAINTENANCE_SUB.DELETE, component: InactiveServiceCoverInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVESERVICECOVERINFOMAINTENANCE_SUB.CANCEL_JOB, component: InactiveServiceCoverInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVESERVICECOVERINFOMAINTENANCE_SUB.CANCEL_PRODUCT, component: InactiveServiceCoverInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVECONTRACTINFOMAINTENANCE_SUB, component: InactiveContractInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVECONTRACTINFOMAINTENANCECANCEL_SUB, component: InactiveContractInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVECONTRACTINFOMAINTENANREINSTATEL_SUB, component: InactiveContractInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVEPREMISEINFOMAINTENANCE_SUB, component: InactivePremiseInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVEPREMISEINFOMAINTENANCE_CONTRACT_CANCEL_SUB, component: InactivePremiseInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVEPREMISEINFOMAINTENANCE_JOB_CANCEL_SUB, component: InactivePremiseInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVEPREMISEINFOMAINTENANCE_PRODUCT_CANCEL_SUB, component: InactivePremiseInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAINACTIVEPREMISEINFOMAINTENANCE_REINSTATE_SUB, component: InactivePremiseInfoMaintenanceComponent, canDeactivate: [RouteAwayGuardService] }
                ], data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        RetentionRootComponent,
        ServiceCoverSelectMaintenanceComponent,
        ContractSelectMaintenanceComponent,
        PremiseSelectMaintenanceComponent,
        InactiveContractInfoMaintenanceComponent,
        InactivePremiseInfoMaintenanceComponent,
        InactiveServiceCoverInfoMaintenanceComponent
    ],
    exports: [
        ServiceCoverSelectMaintenanceComponent,
        ContractSelectMaintenanceComponent,
        PremiseSelectMaintenanceComponent,
        InactiveContractInfoMaintenanceComponent,
        InactivePremiseInfoMaintenanceComponent,
        InactiveServiceCoverInfoMaintenanceComponent
    ],
    entryComponents: [
        ContractSelectMaintenanceComponent,
        ServiceCoverSelectMaintenanceComponent,
        PremiseSelectMaintenanceComponent,
        InactiveContractInfoMaintenanceComponent
    ]
})

export class RetentionModule { }
