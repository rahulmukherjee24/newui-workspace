import { InternalGridSearchApplicationModuleRoutes, InternalGridSearchServiceModuleRoutes, InternalMaintenanceServiceModuleRoutes, GoogleMapPagesModuleRoutes } from './../../base/PageRoutes';
import { QueryParams } from '../../../shared/services/http-params-wrapper';

import { LocalStorageService } from 'ngx-webstorage';

import { PremiseMaintenanceComponent } from './iCABSAPremiseMaintenance';
import { PremiseMaintenance0 } from './iCABSAPremiseMaintenance0';
import { PremiseMaintenance1 } from './iCABSAPremiseMaintenance1';
import { PremiseMaintenance2 } from './iCABSAPremiseMaintenance2';
import { RiMaintenance, MntConst, RiTab } from './../../../shared/services/riMaintenancehelper';
import { RiExchange } from './../../../shared/services/riExchange';
import { Utils } from './../../../shared/services/utility';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ContractManagementModuleRoutes } from '../../base/PageRoutes';
import { CBBConstants } from './../../../shared/constants/cbb.constants';

export class PremiseMaintenance3 {
    //Duplicated Parent Class objects
    public utils: Utils;
    private xhr: any;
    private xhrParams: any;
    private uiForm: any;
    private controls: any;
    private uiDisplay: any;
    private pageParams: any;
    private attributes: any;
    private formData: any;
    private LookUp: any;
    private logger: any;
    private riExchange: RiExchange;
    private riMaintenance: RiMaintenance;
    private riTab: RiTab;
    private viewChild: any;

    public pgPM0: PremiseMaintenance0;
    public pgPM1: PremiseMaintenance1;
    public pgPM2: PremiseMaintenance2;
    public pgPM3: PremiseMaintenance3;

    constructor(private parent: PremiseMaintenanceComponent, private _ls: LocalStorageService) {
        this.utils = this.parent.utils;
        this.logger = this.parent.logger;
        this.xhr = this.parent.xhr;
        this.xhrParams = this.parent.xhrParams;
        this.LookUp = this.parent.LookUp;
        this.uiForm = this.parent.uiForm;
        this.controls = this.parent.controls;
        this.uiDisplay = this.parent.uiDisplay;
        this.viewChild = this.parent.viewChild;
        this.pageParams = this.parent.pageParams;
        this.attributes = this.parent.attributes;
        this.formData = this.parent.formData;
        this.riExchange = this.parent.riExchange;
        this.riMaintenance = this.parent.riMaintenance;
        this.riTab = this.parent.riTab;
    }

    public window_onload(): void {
        this.pgPM0 = this.parent.pgPM0;
        this.pgPM1 = this.parent.pgPM1;
        this.pgPM2 = this.parent.pgPM2;
        this.pgPM3 = this.parent.pgPM3;
    }

    ////////////////////////////////////////////////Below Code is Grid based //NOT REQUIRED////////////////////////////////////////////////////////
    public totalRecords = 0;
    public itemsPerPage = this.parent.global.AppConstants().tableConfig.itemsPerPage;
    public maxColumn = 3;
    public currentPage = 1;
    public page: number = 1;
    public gridSortHeaders: Array<any> = [];
    public headerClicked: string = '';
    public sortType: string = 'DESC';
    public storeData: any;

    public refreshGrid(): void {
        this.page = 1;
        this.BuildSRAGrid();
    }

    public getCurrentPage(currentPage: any): void {
        if (this.page !== currentPage.value) {
            this.page = currentPage.value;
            this.BuildSRAGrid();
        }
    }

    public getGridInfo(info: any): void {
        this.totalRecords = info.totalRows;
    }

    public sortGrid(event: any): void {
        this.logger.log('sortGrid -- ', event);
    }

    public onGridRowClick(event: any): void {
        if (this.pageParams.glAllowUserAuthUpdate) {
            let cStrCanUpdate = 'Yes';
            if (!this.riExchange.riInputElement.isDisabled(this.uiForm, 'cmdSRAGenerateText')) {
                cStrCanUpdate = 'No';
            }
            if (event.cellIndex === 2) {
                this.riMaintenance.clear();
                this.riMaintenance.PostDataAdd('GridUniqueID', this.pageParams.GridCacheTime, MntConst.eTypeText);
                this.riMaintenance.PostDataAdd('CanUpdate', cStrCanUpdate, MntConst.eTypeText);
                this.riMaintenance.PostDataAdd('ContractNumber', this.parent.getControlValue('ContractNumber'), MntConst.eTypeText);
                this.riMaintenance.PostDataAdd('PremiseNumber', this.parent.getControlValue('PremiseNumber'), MntConst.eTypeText);
                this.riMaintenance.PostDataAdd('riGridMode', '0', MntConst.eTypeText);
                this.riMaintenance.PostDataAdd('riGridHandle', this.parent.getGridHandle(), MntConst.eTypeText);
                this.riMaintenance.PostDataAdd('ROWID', event.trRowData[0].rowID, MntConst.eTypeText);
                this.riMaintenance.PostDataAdd('Mode', 'SRAToggle', MntConst.eTypeText);
                this.riMaintenance.Execute(this, function (data: any): any {
                    if (!data.hasError) {
                        this.BuildSRAGrid();
                    }
                }, 'POST', 2);
            }
        }
    }

    public BuildSRAGrid(): void {
        let cStrCanUpdate = 'Yes';
        if (!this.riExchange.riInputElement.isDisabled(this.uiForm, 'cmdSRAGenerateText')) {
            cStrCanUpdate = 'No';
        }

        this.parent.Grid.clearGridData();
        let search = new QueryParams();
        search.set(this.parent.serviceConstants.Action, '2');
        search.set(this.parent.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.parent.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (!this.pageParams.GridCacheTime) {
            this.pageParams.GridCacheTime = (new Date()).toTimeString().split(' ')[0];
        }
        search.set('GridUniqueID', this.pageParams.GridCacheTime);
        search.set('CanUpdate', cStrCanUpdate);
        search.set('ContractNumber', this.parent.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.parent.getControlValue('PremiseNumber'));
        search.set('riGridMode', '0');
        search.set('riGridHandle', this.parent.getGridHandle());
        search.set('PageSize', this.itemsPerPage);
        search.set('PageCurrent', this.page.toString());

        let gridIP = {
            method: this.xhrParams.method,
            operation: this.xhrParams.operation,
            module: this.xhrParams.module,
            search: search
        };
        this.parent.Grid.loadGridData(gridIP);
    }

    public PremiseSRADate_OnChange(): void {
        this.parent.pgPM2.riExchange_CBORequest();
    }
    ////////////////////////////////////////////////Above Code is Grid based //NOT REQUIRED////////////////////////////////////////////////////////


    public HazardDesc_onkeydown(obj: any): void {
        if (obj.KeyCode === 34 && this.pageParams.vbGblSRAMode === 'Add') {
            //TODO - Page not ready
            this.parent.navigate('PremHazard', 'Business/iCABSBGblSRAHazardSearch.htm');
            this.SetHazardResponse();
        }
    }

    public LocationDesc_onkeydown(obj: any): void {
        if (obj.KeyCode === 34 && (this.pageParams.vbGblSRAMode === 'Add' || this.pageParams.vbGblSRAMode === 'Edit')) {
            this.parent.setControlValue('GridUniqueID', this.pageParams.GridCacheTime);
            //TODO - Page not ready
            this.parent.navigate('PremSRA', 'Application/iCABSALocationGrid.htm');
        }
    }

    public BuildPremGblSRACache(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.clear();
        this.riMaintenance.BusinessObject = 'iCABSPremiseMaintGblSRA.p';
        this.riMaintenance.PostDataAdd('Function', 'BuildPremGblSRACache', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeInteger);
        this.riMaintenance.PostDataAdd('GblSRATypeCode', ctrl.GblSRATypeCode.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('GridUniqueID', this.pageParams.GridCacheTime, MntConst.eTypeTextFree);
        this.riMaintenance.Execute(this, function (data: any): any {
            this.logger.log('PDA Callback 3A', data);
        });
    }

    public PremGblSRAFetch(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.clear();
        this.riMaintenance.BusinessObject = 'iCABSPremiseMaintGblSRA.p';
        this.riMaintenance.PostDataAdd('Function', 'FetchGblSRAHazard', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('GridUniqueID', this.pageParams.GridCacheTime, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('GblSRATypeCode', ctrl.GblSRATypeCode.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('HazardSequence', ctrl.HazardSequence.value, MntConst.eTypeInteger);
        this.riMaintenance.ReturnDataAdd('HazardFreeText', MntConst.eTypeTextFree);
        this.riMaintenance.ReturnDataAdd('LocationDesc', MntConst.eTypeTextFree);
        this.riMaintenance.ReturnDataAdd('AdditionalLocations', MntConst.eTypeTextFree);
        this.riMaintenance.ReturnDataAdd('AdditionalActionComments', MntConst.eTypeTextFree);
        this.riMaintenance.ReturnDataAdd('SafeToProceed', MntConst.eTypeInteger);
        this.riMaintenance.ReturnDataAdd('AdditionalCustomerRequirements', MntConst.eTypeTextFree);
        this.riMaintenance.Execute(this, function (data: any): any {
            this.parent.setControlValue('HazardFreeText', data['HazardFreeText']);
            this.parent.setControlValue('LocationDesc', data['LocationDesc']);
            this.parent.setControlValue('AdditionalLocations', data['AdditionalLocations']);
            this.parent.setControlValue('AdditionalActionComments', data['AdditionalActionComments']);
            this.parent.setControlValue('SafeToProceed', data['SafeToProceed']);
            this.parent.setControlValue('AdditionalCustomerRequirements', data['AdditionalCustomerRequirements']);
            this.SetHazardResponse();
            this.CheckSafeToProceed();
        });
    }

    public SetHazardResponse(): void {
        let ctrl = this.uiForm.controls;
        if (ctrl.HazardSequence.value !== '') {
            this.riMaintenance.clear();
            this.riMaintenance.BusinessObject = 'iCABSPremiseMaintGblSRA.p';
            this.riMaintenance.PostDataAdd('Function', 'SetHazardResponse', MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('GblSRATypeCode', ctrl.GblSRATypeCode.value, MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('HazardSequence', ctrl.HazardSequence.value, MntConst.eTypeInteger);
            this.riMaintenance.ReturnDataAdd('HazardResponse', MntConst.eTypeInteger);
            this.riMaintenance.ReturnDataAdd('HazardDesc', MntConst.eTypeTextFree);
            this.riMaintenance.Execute(this, function (data: any): any {
                this.parent.setControlValue('HazardResponse', data['HazardResponse']);
                this.parent.setControlValue('HazardDesc', data['HazardDesc']);
                this.PremGblSRAEnable();
            });
        }
    }

    public PremGblSRAEnable(): void {
        let ctrl = this.uiForm.controls;
        this.uiDisplay.tblTriggerGrid = true;
        this.uiDisplay.tblTriggerControl = true;
        if (ctrl.HazardResponse.value === '1') { //Require RCM
            this.uiDisplay.trRCMGrid = true;
            this.uiDisplay.tblRCMControl = true;
        } else {
            if (ctrl.HazardResponse.value === '2') {
                this.uiDisplay.trHazardFree = true; //Require Hazard Free Text
            }
            this.riExchange.riInputElement.Enable(this.uiForm, 'LocationDesc');
            this.riExchange.riInputElement.Enable(this.uiForm, 'AdditionalLocations');
            this.riExchange.riInputElement.Enable(this.uiForm, 'SafeToProceed');
        }
    }

    public PremGblSRADisable(): void {
        this.parent.setControlValue('HazardDesc', '');
        this.parent.setControlValue('HazardSequence', '');
        this.parent.setControlValue('HazardResponse', '');
        this.parent.setControlValue('HazardFreeText', '');
        this.parent.setControlValue('LocationCode', '');
        this.parent.setControlValue('LocationDesc', '');
        this.parent.setControlValue('AdditionalLocations', '');
        this.parent.setControlValue('AdditionalActionComments', '');
        this.parent.setControlValue('SafeToProceed', '');
        this.parent.setControlValue('AdditionalCustomerRequirements', '');
        this.uiDisplay.tblTriggerGrid = false;
        this.uiDisplay.tblTriggerControl = false;
        this.uiDisplay.trRCMGrid = false;
        this.uiDisplay.tblRCMControl = false;
        this.uiDisplay.trHazardFree = false;
        this.riExchange.riInputElement.Disable(this.uiForm, 'AdditionalLocations');
        this.riExchange.riInputElement.Disable(this.uiForm, 'SafeToProceed');
        if (this.pageParams.vbGblSRAMode === '') {
            this.uiDisplay.thGblSRAControl = false;
            this.uiDisplay.cmdQuestionnaireComp = false;
        }
    }

    public cmdSRAGenerateText_OnClick(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSPremiseMaintSRAGrid.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('Action', '6', MntConst.eTypeInteger);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('Function', 'GenerateSRAText', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('PremiseSpecialInstructions', ctrl.PremiseSpecialInstructions.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('GridUniqueID', this.pageParams.GridCacheTime, MntConst.eTypeTextFree);
        this.riMaintenance.ReturnDataAdd('PremiseSpecialInstructions', MntConst.eTypeTextFree);
        this.riMaintenance.Execute(this, function (data: any): any {
            this.parent.setControlValue('PremiseSpecialInstructions', data['PremiseSpecialInstructions']);
            this.parent.markControlAsDirty('PremiseSpecialInstructions');
        }, 'POST');
    }

    public PremGblSRASave(GblSRASaveMode: any): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.clear();
        this.riMaintenance.BusinessObject = 'iCABSPremiseMaintGblSRA.p';
        this.riMaintenance.PostDataAdd('Function', 'Save', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('AddMode', GblSRASaveMode, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('GridUniqueID', this.pageParams.GridCacheTime, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('GblSRATypeCode', ctrl.GblSRATypeCode.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('HazardSequence', ctrl.HazardSequence.value, MntConst.eTypeInteger);
        this.riMaintenance.PostDataAdd('HazardResponse', ctrl.HazardResponse.value, MntConst.eTypeInteger);
        this.riMaintenance.PostDataAdd('HazardFreeText', ctrl.HazardFreeText.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('AdditionalLocations', ctrl.AdditionalLocations.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('AdditionalActionComments', ctrl.AdditionalActionComments.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('SafeToProceed', ctrl.SafeToProceed.value, MntConst.eTypeInteger);
        this.riMaintenance.PostDataAdd('AdditionalCustomerRequirements', ctrl.AdditionalCustomerRequirements.value, MntConst.eTypeTextFree);
        this.riMaintenance.Execute(this, function (data: any): any {
            this.pageParams.vbGblSRAMode = ''; //Reset mode
            this.PremGblSRADisable();
        });

    }

    public DeletePremGblSRAHazardRCM(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSPremiseMaintGblSRA.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('Function', 'DeletePremSRAHazRCM', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('GridUniqueID', ctrl.GridUniqueID.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('GblSRATypeCode', ctrl.GblSRATypeCode.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('HazardSequence', ctrl.HazardSequence.value, MntConst.eTypeInteger);
        this.riMaintenance.PostDataAdd('RCMNumber', ctrl.RCMNumber.value, MntConst.eTypeInteger);
        this.riMaintenance.Execute(this, function (data: any): any {
            this.pageParams.vbGblSRAMode = ''; //Reset mode
            this.PremGblSRADisable();
        });
    }

    public CheckSafeToProceed(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.clear();
        this.riMaintenance.BusinessObject = 'iCABSPremiseMaintGblSRA.p';
        this.riMaintenance.PostDataAdd('Function', 'CheckSafeToProceed', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('GridUniqueID', this.pageParams.GridCacheTime, MntConst.eTypeTextFree);
        this.riMaintenance.PostDataAdd('GblSRATypeCode', ctrl.GblSRATypeCode.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('HazardSequence', ctrl.HazardSequence.value, MntConst.eTypeInteger);
        this.riMaintenance.ReturnDataAdd('NotSafeToProceed', MntConst.eTypeCheckBox);
        this.riMaintenance.Execute(this, function (data: any): any {
            if (data['NotSafeToProceed']) {
                ctrl.SafeToProceed.value = '0';
                this.riExchange.riInputElement.Disable(this.uiForm, 'SafeToProceed');
            }
            else {
                this.riExchange.riInputElement.Enable(this.uiForm, 'SafeToProceed');
            }
        });

    }

    public cmdAddNextHazard_onClick(): void {
        switch (this.pageParams.vbGblSRAMode) {
            case 'Add':
            case 'Edit':
                this.PremGblSRASave(this.pageParams.vbGblSRAMode);
                break;
            case '':
                this.uiDisplay.cmdQuestionnaireComp = true;
                this.pageParams.vbGblSRAMode = 'Add';
                this.viewChild.HazardDesc.focus();
                break;
        }
    }

    public cmdQuestionnaireComp_onClick(): void {

        switch (this.pageParams.vbGblSRAMode) {
            case 'Add':
            case 'Edit':
                this.PremGblSRASave(this.pageParams.vbGblSRAMode);
                break;
            case '':
                this.uiDisplay.cmdQuestionnaireComp = true;
                break;
        }
    }

    public cmdGeocode_OnClick(): void {
        this.parent.sanitizeURL();
        let url: string = '#' + GoogleMapPagesModuleRoutes.ICABSAROUTINGSEARCH;
        url += '?parentMode=Premise';
        url += '&' + CBBConstants.c_s_URL_PARAM_COUNTRYCODE + '=' + this.utils.getCountryCode();
        url += '&' + CBBConstants.c_s_URL_PARAM_BUSINESSCODE + '=' + this.utils.getBusinessCode();
        url += '&' + CBBConstants.c_s_URL_PARAM_BRANCHCODE + '=' + this.utils.getBranchCode();
        url += '&PremisePostcode=' + this.parent.getControlValue('PremisePostcode');
        url += '&PremiseAddressLine1=' + this.parent.getControlValue('PremiseAddressLine1');
        url += '&PremiseAddressLine2=' + this.parent.getControlValue('PremiseAddressLine2');
        url += '&PremiseAddressLine3=' + this.parent.getControlValue('PremiseAddressLine3');
        url += '&PremiseAddressLine4=' + this.parent.getControlValue('PremiseAddressLine4');
        url += '&PremiseAddressLine5=' + this.parent.getControlValue('PremiseAddressLine5');
        url += '&GPSCoordinateX=' + this.parent.getControlValue('GPSCoordinateX');
        url += '&GPSCoordinateY=' + this.parent.getControlValue('GPSCoordinateY');
        url += '&RoutingScore=' + this.parent.getControlValue('RoutingScore');
        url += '&SelRoutingSource=' + this.parent.getControlValue('SelRoutingSource');
        let routingSearchWindow: any = window.open(url,
            '_blank',
            'fullscreen=yes,menubar=no,resizable=no,status=no,titlebar=no,toolbar=no,width=' + screen.width + ',height=' + screen.height);
        routingSearchWindow.onbeforeunload = () => {
            this.getRoutingSearchScreenData();
        };
    }

    private updateFieldFromRoutingSearchReturnValue(routingSearchObj: any, returnName: string, controlName: string): void {
        if (routingSearchObj[returnName]) {
            this.parent.setControlValue(controlName, routingSearchObj[returnName]);
        }
    }

    public getRoutingSearchScreenData(): void {
        let routingSearchObj: any = this._ls.retrieve('PremiseRoutingSearch');
        if (routingSearchObj) {
            routingSearchObj = JSON.parse(routingSearchObj);
            if (routingSearchObj.applyClicked !== 'true') {
                return;
            }
            this.updateFieldFromRoutingSearchReturnValue(routingSearchObj, 'PremisePostCode', 'PremisePostCode');
            this.updateFieldFromRoutingSearchReturnValue(routingSearchObj, 'PremiseAddressLine1', 'PremiseAddressLine1');
            this.updateFieldFromRoutingSearchReturnValue(routingSearchObj, 'PremiseAddressLine2', 'PremiseAddressLine2');
            this.updateFieldFromRoutingSearchReturnValue(routingSearchObj, 'PremiseAddressLine3', 'PremiseAddressLine3');
            this.updateFieldFromRoutingSearchReturnValue(routingSearchObj, 'PremiseAddressLine4', 'PremiseAddressLine4');
            this.updateFieldFromRoutingSearchReturnValue(routingSearchObj, 'PremiseAddressLine5', 'PremiseAddressLine5');
            this.updateFieldFromRoutingSearchReturnValue(routingSearchObj, 'GPScoordinateX', 'GPSCoordinateX');
            this.updateFieldFromRoutingSearchReturnValue(routingSearchObj, 'GPScoordinateY', 'GPSCoordinateY');
            this.updateFieldFromRoutingSearchReturnValue(routingSearchObj, 'RoutingScore', 'RoutingScore');
            this.updateFieldFromRoutingSearchReturnValue(routingSearchObj, 'RoutingSource', 'SelRoutingSource');
            this._ls.clear('PremiseRoutingSearch');
        }
    }

    public cmdProductSales_onclick(event: any): void {
        this.parent.promptContent = MessageConstant.PageSpecificMessage.AddProductSales;
        this.parent.promptModal.show();
        this.parent.callbackPrompts.push(this.parent.pgPM3.cmdProductSalesHandle);
        this.parent.setControlValue('menu', 'Options');
    }

    public cmdProductSalesHandle(): void {
        let dtProductSaleCommenceDate: string = '';
        let ctrl = this.uiForm.controls;
        if (this.pageParams.promptAns) {
            if (this.pageParams.vbEnableProductSaleCommenceDate) {
                this.parent.showAlert(MessageConstant.Message.PageNotCovered, 2); //DELETE
            }
            dtProductSaleCommenceDate = this.utils.formatDate(new Date());
            if (dtProductSaleCommenceDate !== '') {
                this.riMaintenance.clear();
                this.riMaintenance.BusinessObject = 'iCABSPremiseEntry.p';
                this.riMaintenance.PostDataAdd('Function', 'AddProductSales', MntConst.eTypeText);
                this.riMaintenance.PostDataAdd('Action', '6', MntConst.eTypeCode);
                this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
                this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeCode);
                this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeCode);
                this.riMaintenance.PostDataAdd('ProductSaleCommenceDate', dtProductSaleCommenceDate, MntConst.eTypeDate);
                this.riMaintenance.ReturnDataAdd('NewPremiseRowID', MntConst.eTypeText);
                this.riMaintenance.Execute(this, function (data: any): any {
                    if (data.errorMessage) {
                        let msg: string = data.errorMessage;
                        msg += data.fullError ? ' - ' + data.fullError : '';
                        this.parent.showAlert(msg);
                        return;
                    }
                    let urlStrArr = this.location.path().split('?');
                    let urlStr = urlStrArr[0];
                    let qParams = urlStrArr[1]
                        + '&ContractNumber=' + this.parent.getControlValue('ContractNumber')
                        + '&PremiseNumber=' + this.parent.getControlValue('PremiseNumber')
                        + '&reload=true';
                    this.location.replaceState(urlStr, qParams);
                    this.parent.setControlValue('NewPremiseRowID', data['NewPremiseRowID']);
                    this.parent.navigate('AddFromPremise', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                        contractTypeCode: 'P'
                    });
                }, 'POST');
            }
        }
    }

    public TechRetentionReasonCode_onkeydown(obj: any): void {
        if (obj.keyCode === 34) {
            this.parent.navigate('Premise', 'Business/iCABSBPremiseTechRetentionReasonsLangSearch.htm');
        }
    }

    public TechRetentionReasonCode_onchange(): void {
        let ctrl = this.uiForm.controls;
        if (ctrl.TechRetentionReasonCode.value !== '') {
            this.riMaintenance.clear();
            this.riMaintenance.BusinessObject = 'iCABSExchangeFunctions.p';
            this.riMaintenance.PostDataAdd('PostDesc', 'SetTechRetentionReasonDesc', MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('TechRetentionReasonCode', ctrl.TechRetentionReasonCode.value, MntConst.eTypeCode);
            this.riMaintenance.ReturnDataAdd('TechRetentionReasonDesc', MntConst.eTypeText);

            this.riMaintenance.Execute(this, function (data: any): any {
                this.parent.setControlValue('TechRetentionReasonDesc', data['TechRetentionReasonDesc']);
            });
        }
        else {
            this.parent.setControlValue('TechRetentionReasonCode', '');
        }
    }

    public TechRetentionInd_onClick(): void {
        if (this.riExchange.riInputElement.checked(this.uiForm, 'TechRetentionInd')) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'TechRetentionReasonCode', true);
        } else {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'TechRetentionReasonCode', false);
            this.parent.setControlValue('TechRetentionReasonCode', '');
            this.parent.setControlValue('TechRetentionReasonDesc', '');
        }
    }
    public CustomerIndicationNumber_onChange(): void { //TODO ctrl not present in html
        this.parent.setControlValue('selCustomerIndicationNumber', this.parent.getControlValue('CustomerIndicationNumber') || '0');
    }

    public SelCustomerIndicationNumber_onChange(): void {
        this.parent.setControlValue('CustomerIndicationNumber', this.parent.getControlValue('selCustomerIndicationNumber'));
    }

    public setPurchaseOrderFields(): void {
        this.parent.isRequesting = true;
        let ctrl = this.uiForm.controls;
        let vbContinuousInd: string;
        this.riMaintenance.clear();
        this.riMaintenance.BusinessObject = 'iCABSPremiseEntry.p';
        this.riMaintenance.PostDataAdd('Function', 'getContinuousInd', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.parent.businessCode(), MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeText);
        this.riMaintenance.ReturnDataAdd('ContinuousInd', MntConst.eTypeText);
        this.riMaintenance.Execute(this, function (data: any): any {
            vbContinuousInd = this.utils.customTruthyCheck(data['ContinuousInd']) ? data['ContinuousInd'].trim().toUpperCase() : '';
            this.uiDisplay.tdPurchaseOrderLineNo = !(ctrl.PurchaseOrderNo.value === '' || vbContinuousInd === 'NO');
            this.uiDisplay.tdPurchaseOrderExpiryDate = !(ctrl.PurchaseOrderNo.value === '' || vbContinuousInd === 'NO');
            this.parent.isRequesting = false;
        }, 'POST', 6, false);
        setTimeout(() => { this.parent.isRequesting = false; }, 3000);
    }

    //Town Validation using Postcode
    public PremisePostcode_onchange(): void {
        let ctrl = this.uiForm.controls;
        if (this.pageParams.SCEnablePostcodeDefaulting && this.pageParams.SCEnableDatabasePAF && ctrl.PremisePostcode.value.trim() !== '') {
            this.riMaintenance.BusinessObject = 'iCABSGetPostCodeTownAndState.p';
            this.riMaintenance.clear();
            this.riMaintenance.PostDataAdd('Function', 'GetPostCodeTownAndState', MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('Postcode', ctrl.PremisePostcode.value, MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('State', ctrl.PremiseAddressLine5.value, MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('Town', ctrl.PremiseAddressLine4.value, MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('UniqueRecordFound', MntConst.eTypeCheckBox);
            this.riMaintenance.ReturnDataAdd('Postcode', MntConst.eTypeCode);
            this.riMaintenance.ReturnDataAdd('State', MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('Town', MntConst.eTypeText);
            this.riMaintenance.Execute(this, function (data: any): any {
                if (!data['UniqueRecordFound'] || data['UniqueRecordFound'] === 'no') {
                    setTimeout(() => {
                        this.parent.isModalOpen(true);
                        this.parent.PostCodeSearch.openModal();
                    }, 200);
                } else {
                    this.parent.setControlValue('PremisePostcode', data['Postcode']);
                    this.parent.setControlValue('PremiseAddressLine5', data['State']);
                    this.parent.setControlValue('PremiseAddressLine4', data['Town']);
                }
            }, 'GET', 0);
        }
        if (this.riMaintenance.CurrentMode === MntConst.eModeAdd && this.pageParams.SCEnableRepeatSalesMatching && ctrl.PremisePostcode.value.trim() !== '') {
            this.MatchPremise();
            this.GetMatchPremiseNames();
        }
    }

    public MatchPremise(): void {
        this.parent.isRequesting = true;
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSPremiseEntry.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('Action', '6', MntConst.eTypeInteger);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('Function', 'CheckMatchedPremise', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('AccountNumber', ctrl.AccountNumber.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('PremiseAddressLine4', ctrl.PremiseAddressLine4.value, MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('PremiseAddressLine5', ctrl.PremiseAddressLine5.value, MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('PremisePostcode', ctrl.PremisePostcode.value, MntConst.eTypeCode);
        this.riMaintenance.ReturnDataAdd('MatchedPremise', MntConst.eTypeCheckBox);
        this.riMaintenance.Execute(this, function (data: any): any {
            this.parent.isRequesting = false;
            if (!data.hasError) {
                if (data['MatchedPremise']) {
                    //TODO - Page not ready
                    this.parent.showAlert(MessageConstant.Message.PageNotCovered, 2);
                    // this.navigate('', 'Application/iCABSAPremiseMatchingGrid');
                }
            } else {
                this.parent.showAlert(data.errorMessage);
            }
        }, 'POST');
        setTimeout(() => { this.parent.isRequesting = false; }, 3000);
    }

    //************************************************************************************
    //* AFTER ABANDON                                                                    *
    //************************************************************************************
    public riMaintenance_AfterAbandon(): void {
        let ctrl = this.uiForm.controls;
        this.riExchange.riInputElement.Disable(this.uiForm, 'cmdSRAGenerateText');
        if (ctrl.ContractNumber.value !== '/wsscripts/riHTMLWrapper.p?rifileName=') {
            this.uiDisplay.InactiveEffectDate = (ctrl.InactiveEffectDate.value !== '');
            this.uiDisplay.labelInactiveEffectDate = this.uiDisplay.InactiveEffectDate;
            this.uiDisplay.LostBusinessDesc = this.uiDisplay.InactiveEffectDate && (ctrl.LostBusinessDesc.value !== '');
            this.uiDisplay.cmdValue = ctrl.ShowValueButton.checked;

            this.uiDisplay.tdDrivingChargeValueLab = (this.parent.getControlValue('SCEnableDrivingCharges') && this.parent.getControlValue('DrivingChargeInd') && this.pageParams.CurrentContractType !== 'P');
            this.uiDisplay.tdDrivingChargeValue = this.uiDisplay.tdDrivingChargeValueLab;
            this.riMaintenance_AfterSaveUpdate();
        }
        this.riExchange.riInputElement.Disable(this.uiForm, 'cmdResendPremises');
        if (this.pageParams.vbEnableGlobalSiteRiskAssessment) {
            this.pageParams.vbGblSRAMode = '';
            this.PremGblSRADisable();
        }
    }

    //************************************************************************************
    //* Query Unload HTML Document                                                       *
    //************************************************************************************
    public riExchange_UpdateHTMLDocument(): void {
        let ctrl = this.uiForm.controls;
        if (ctrl.WindowClosingName.value === 'InvoiceNarrativeAmendmentsMade') {
            this.riExchange.getParentAttributeValue('InvoiceNarrativeText');
        }

        //Returned From Contact Person Maintenance With Changes - So Force A Refresh Of The Details
        if (ctrl.forceRefresh.checked !== true && ctrl.WindowClosingName.value === 'AmendmentsMade') {
            this.riMaintenance.clear();
            this.riMaintenance.BusinessObject = 'iCABSContactPersonEntryGrids.p';
            this.riMaintenance.PostDataAdd('Action', '6', MntConst.eTypeInteger);
            this.riMaintenance.PostDataAdd('Function', 'GetContactPersonChanges', MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeInteger);
            this.riMaintenance.ReturnDataAdd('ContactPersonFound', MntConst.eTypeTextFree);
            this.riMaintenance.ReturnDataAdd('ContactPersonName', MntConst.eTypeTextFree);
            this.riMaintenance.ReturnDataAdd('ContactPersonPosition', MntConst.eTypeTextFree);
            this.riMaintenance.ReturnDataAdd('ContactPersonDepartment', MntConst.eTypeTextFree);
            this.riMaintenance.ReturnDataAdd('ContactPersonTelephone', MntConst.eTypeTextFree);
            this.riMaintenance.ReturnDataAdd('ContactPersonMobile', MntConst.eTypeTextFree);
            this.riMaintenance.ReturnDataAdd('ContactPersonFax', MntConst.eTypeTextFree);
            this.riMaintenance.ReturnDataAdd('ContactPersonEmail', MntConst.eTypeTextFree);
            this.riMaintenance.Execute(this, function (data: any): any {
                if (data['ContactPersonFound'] === 'Y') {
                    this.parent.setControlValue('PremiseContactName', data['ContactPersonName']);
                    this.parent.setControlValue('PremiseContactPosition', data['ContactPersonPosition']);
                    this.parent.setControlValue('PremiseContactDepartment', data['ContactPersonDepartment']);
                    this.parent.setControlValue('PremiseContactTelephone', data['ContactPersonTelephone']);
                    this.parent.setControlValue('PremiseContactMobile', data['ContactPersonMobile']);
                    this.parent.setControlValue('PremiseContactEmail', data['ContactPersonEmail']);
                    this.parent.setControlValue('PremiseContactFax', data['ContactPersonFax']);
                }
            });
        }
        if (ctrl.forceRefresh.checked) {
            this.riMaintenance.FetchRecord();
            ctrl.forceRefresh.checked = false;
        }
    }

    public riMaintenance_AfterSaveUpdate(): void {
        if (this.pageParams.vSCMultiContactInd) {
            this.uiDisplay.tdBtnAmendContact = true;
        }
        if (this.pageParams.ParentMode === 'AddFromPremise' && this.pageParams.vbAllowAutoOpenSCScreen) {
            this.parent.navigate('Premise-Add', InternalGridSearchServiceModuleRoutes.ICABSAPRODUCTSALESSCENTRYGRID.URL_1);
            this.pageParams.vbAllowAutoOpenSCScreen = false;
        }
    }

    public SensitiseContactDetails(lSensitise: boolean): void {
        if (lSensitise) {
            this.riExchange.riInputElement.Enable(this.uiForm, 'PremiseContactName');
            this.riExchange.riInputElement.Enable(this.uiForm, 'PremiseContactPosition');
            this.riExchange.riInputElement.Enable(this.uiForm, 'PremiseContactDepartment');
            this.riExchange.riInputElement.Enable(this.uiForm, 'PremiseContactMobile');
            this.riExchange.riInputElement.Enable(this.uiForm, 'PremiseContactTelephone');
            this.riExchange.riInputElement.Enable(this.uiForm, 'PremiseContactFax');
            this.riExchange.riInputElement.Enable(this.uiForm, 'PremiseContactEmail');
        } else {
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseContactName');
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseContactPosition');
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseContactDepartment');
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseContactMobile');
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseContactTelephone');
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseContactFax');
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseContactEmail');
        }

        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PremiseContactName', lSensitise);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PremiseContactPosition', lSensitise);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PremiseContactTelephone', lSensitise);
    }

    public BtnAmendContact_OnClick(): void {
        this.parent.sanitizeURL();
        this.parent.pgPM3.cmdContactDetails();
    }

    public cmdContactDetails(): void {
        this.parent.navigate('Premise', InternalMaintenanceServiceModuleRoutes.ICABSCMCONTACTPERSONMAINTENANCE, {
            parentMode: 'Premise',
            contractNumber: this.parent.getControlValue('ContractNumber'),
            premiseNumber: this.parent.getControlValue('PremiseNumber')
        });
    }

    private enableTimeWindowInput(windowNumber: number, enable: boolean): void {
        let num: string = this.utils.numberPadding(windowNumber, 2);
        if (enable) {
            this.riMaintenance.EnableInput('WindowStart' + num);
            this.riMaintenance.EnableInput('WindowEnd' + num);
        } else {
            this.riMaintenance.DisableInput('WindowStart' + num);
            this.riMaintenance.DisableInput('WindowEnd' + num);
        }
    }

    private setTimeWindowValues(windowNumber: number, startTime: string, endTime: string): void {
        let num: string = this.utils.numberPadding(windowNumber, 2);
        this.parent.setControlValue('WindowStart' + num, startTime);
        this.parent.setControlValue('WindowEnd' + num, endTime);
    }

    public selQuickWindowSet_onchange(event: any): void {
        let target: any = '', srcID = '', srcValue = '', srcRow = 0;
        if (event.id) {
            target = document.querySelector('#' + event.id);
            srcID = target.getAttribute('id');
        } else {
            target = event.target || event.srcElement || event.currentTarget;
            srcID = target.attributes.id.nodeValue;
        }
        srcValue = target.value;
        srcRow = parseInt(srcID.split('').pop(), 10);

        this.enableTimeWindowInput(srcRow, (srcValue === 'C'));
        this.enableTimeWindowInput(srcRow + 7, (srcValue === 'C'));

        switch (srcValue) {
            case 'D':
                this.setTimeWindowValues(srcRow,
                    this.parent.getControlValue('DefaultWindowStart' + this.utils.numberPadding(srcRow + srcRow - 1, 2)),
                    this.parent.getControlValue('DefaultWindowEnd' + this.utils.numberPadding(srcRow + srcRow - 1, 2)));
                this.setTimeWindowValues(srcRow + 7,
                    this.parent.getControlValue('DefaultWindowStart' + this.utils.numberPadding(srcRow + srcRow, 2)),
                    this.parent.getControlValue('DefaultWindowEnd' + this.utils.numberPadding(srcRow + srcRow, 2)));
                break;
            case 'U':
                this.setTimeWindowValues(srcRow, '00:00', '00:00');
                this.setTimeWindowValues(srcRow + 7, '00:00', '00:00');
                break;
            case 'A':
                this.setTimeWindowValues(srcRow, '00:00', '11:59');
                this.setTimeWindowValues(srcRow + 7, '12:00', '23:59');
                break;
        }
    }

    public UpdateBusinessDefaultTimes(): void {
        for (let i = 1; i <= 7; i++) {
            this.enableTimeWindowInput(i, false);
            this.enableTimeWindowInput(i + 7, false);

            this.setTimeWindowValues(i,
                this.parent.getControlValue('DefaultWindowStart' + this.utils.numberPadding(i + i - 1, 2)),
                this.parent.getControlValue('DefaultWindowEnd' + this.utils.numberPadding(i + i - 1, 2)));
            this.setTimeWindowValues(i + 7,
                this.parent.getControlValue('DefaultWindowStart' + this.utils.numberPadding(i + i, 2)),
                this.parent.getControlValue('DefaultWindowEnd' + this.utils.numberPadding(i + i, 2)));
        }
    }

    public TermiteServiceCheck(): void {
        this.riMaintenance.BusinessObject = 'iCABSPremiseEntry.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('Action', '6', MntConst.eTypeInteger);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('Function', 'TermiteServiceCheck', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('ContractNumber', this.parent.getControlValue('ContractNumber'), MntConst.eTypeCode);
        this.riMaintenance.ReturnDataAdd('TermiteWarningDesc', MntConst.eTypeText);
        this.riMaintenance.Execute(this, function (data: any): any {
            if (data['TermiteWarningDesc'] !== '') {
                this.parent.showAlert(data['TermiteWarningDesc'], 2);
            }
        }, 'POST');
    }

    public DefaultFromProspect(): void {
        if (this.riExchange.getParentHTMLValue('ProspectNumber') !== '') {
            this.riMaintenance.clear();
            this.riMaintenance.BusinessObject = 'iCABSPremiseEntry.p';
            this.riMaintenance.PostDataAdd('Function', 'DefaultFromProspect', MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('ProspectNumber', this.riExchange.getParentHTMLValue('ProspectNumber'), MntConst.eTypeInteger);
            this.riMaintenance.PostDataAdd('SICCodeEnable', this.pageParams.vSICCodeEnable, MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('CustomerTypeCode', MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('CustomerTypeDesc', MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('SICCode', MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('SICDesc', MntConst.eTypeText);
            let premAddressType: string = this.pageParams.SCCapitalFirstLtr ? MntConst.eTypeTextFree : MntConst.eTypeText;
            this.riMaintenance.ReturnDataAdd('PremiseName', premAddressType);
            this.riMaintenance.ReturnDataAdd('PremiseAddressLine1', premAddressType);
            this.riMaintenance.ReturnDataAdd('PremiseAddressLine2', premAddressType);
            this.riMaintenance.ReturnDataAdd('PremiseAddressLine3', premAddressType);
            this.riMaintenance.ReturnDataAdd('PremiseAddressLine4', premAddressType);
            this.riMaintenance.ReturnDataAdd('PremiseAddressLine5', premAddressType);
            this.riMaintenance.ReturnDataAdd('PremiseContactName', premAddressType);
            this.riMaintenance.ReturnDataAdd('PremiseContactPosition', premAddressType);
            this.riMaintenance.ReturnDataAdd('PremisePostcode', MntConst.eTypeCode);
            this.riMaintenance.ReturnDataAdd('PremiseVtxGeoCode', MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('PremiseOutsideCityLimits', MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('PremiseContactTelephone', MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('PremiseContactMobile', MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('PremiseContactFax', MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('PremiseContactEmail', MntConst.eTypeTextFree);
            this.riMaintenance.Execute(this, function (data: any): any {
                if (!data.hasError) {
                    this.parent.setControlValue('CustomerTypeDesc', data['CustomerTypeDesc']);
                    this.parent.setControlValue('SICCode', data['SICCode']);
                    this.parent.setControlValue('SICDesc', data['SICDesc']);
                    this.parent.setControlValue('PremiseName', data['PremiseName']);
                    this.parent.setControlValue('PremiseAddressLine1', data['PremiseAddressLine1']);
                    this.parent.setControlValue('PremiseAddressLine2', data['PremiseAddressLine2']);
                    this.parent.setControlValue('PremiseAddressLine3', data['PremiseAddressLine3']);
                    this.parent.setControlValue('PremiseAddressLine4', data['PremiseAddressLine4']);
                    this.parent.setControlValue('PremiseAddressLine5', data['PremiseAddressLine5']);
                    this.parent.setControlValue('PremisePostcode', data['PremisePostcode']);
                    this.parent.setControlValue('PremiseVtxGeoCode', data['PremiseVtxGeoCode']);
                    this.parent.setControlValue('PremiseContactName', data['PremiseContactName']);
                    this.parent.setControlValue('PremiseContactPosition', data['PremiseContactPosition']);
                    this.parent.setControlValue('PremiseContactTelephone', data['PremiseContactTelephone']);
                    this.parent.setControlValue('PremiseContactMobile', data['PremiseContactMobile']);
                    this.parent.setControlValue('PremiseContactFax', data['PremiseContactFax']);
                    this.parent.setControlValue('PremiseContactEmail', data['PremiseContactEmail']);
                    this.parent.setControlValue('OutsideCityLimits', data['PremiseOutsideCityLimits'] === 'Y');
                } else {
                    this.parent.setControlValue('CustomerTypeCode', '');
                    this.parent.setControlValue('CustomerTypeDesc', '');
                    this.parent.setControlValue('SICCode', '');
                    this.parent.setControlValue('SICDesc', '');
                }
            }, 'POST');
        }
    }

    public GetGblSRAValues(): void {
        this.riMaintenance.clear();
        this.riMaintenance.BusinessObject = 'iCABSExchangeFunctions.p';
        this.riMaintenance.PostDataAdd('PostDesc', 'SetGblSRAType', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('CustomerTypeCode', this.parent.getControlValue('CustomerTypeCode'), MntConst.eTypeText);
        this.riMaintenance.ReturnDataAdd('GblSRATypeCode', MntConst.eTypeText);
        this.riMaintenance.ReturnDataAdd('GblSRADesc', MntConst.eTypeText);
        this.riMaintenance.Execute(this, function (data: any): any {
            if (data['GblSRATypeCode'] !== '') {
                this.parent.setControlValue('GblSRATypeCode', data['GblSRATypeCode']);
                this.parent.setControlValue('GblSRADesc', data['GblSRADesc']);
            } else {
                this.parent.setControlValue('GblSRATypeCode', '');
                this.parent.setControlValue('GblSRADesc', '');
            }
        }, 'GET', 0);
    }

    //**********************
    //* BEFORE UPDATE MODE *
    //**********************
    public riMaintenance_BeforeUpdateMode(): void {
        if (this.pageParams.vSCMultiContactInd) {
            this.parent.pgPM3.SensitiseContactDetails(false);
        }

        this.riExchange.riInputElement.Enable(this.uiForm, 'SelServiceNotifyTemplateEmail');
        this.riExchange.riInputElement.Enable(this.uiForm, 'SelServiceNotifyTemplateSMS');

        this.riMaintenance.DisableInput('ServiceBranchNumber');
        this.riMaintenance.DisableInput('PremiseCommenceDate');
        this.pageParams.dtPremiseCommenceDate.disabled = true;
        this.riTab.TabFocus(1);

        if (this.riExchange.riInputElement.checked(this.uiForm, 'PNOL')) {
            this.riMaintenance.EnableInput('cmdResendPremises');
        }
        if ((this.parent.getControlValue('NationalAccountBranch') === 'no') && (parseInt(this.utils.getBranchCode(), 10) !== parseInt(this.parent.getControlValue('ServiceBranchNumber'), 10))) {
            this.riMaintenance.DisableInput('SalesAreaCode');
        }
        this.pageParams.PNOLMode = 'Update';
        this.parent.pgPM1.SetOkToUpgradeToPNOL();

        //Make request to get the default business windows
        this.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=GetBusinessDefaultWindows';
        this.riMaintenance.CBORequestAddCS('BusinessCode', MntConst.eTypeCode);
        this.riMaintenance.CBORequestExecute(this, function (data: any): any {
            if (!data.hasError) {
                this.parent.riMaintenance.renderResponseForCtrl(this, data);
            }
        }, false);
        this.parent.pgPM2.HideQuickWindowSet(true);
        this.parent.pgPM2.WindowPreferredIndChanged();
        if (!this.pageParams.vSICCodeRequire) {
            this.riExchange.riInputElement.Disable(this.uiForm, 'SICCode');
        }
        if (this.pageParams.vbShowPremiseWasteTab) {
            this.parent.pgPM2.WasteConsignmentNoteExemptInd_onClick();
        }

        //Next Waste Consignment Note Number should always be disabled
        this.riMaintenance.DisableInput('NextWasteConsignmentNoteNumber');
        this.riMaintenance.clear();
        this.riMaintenance.BusinessObject = 'iCABSPremiseEntry.p';
        this.riMaintenance.PostDataAdd('Function', 'GetWindowsType', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('Action', '6', MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('ContractNumber', this.parent.getControlValue('ContractNumber'), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('PremiseNumber', this.parent.getControlValue('PremiseNumber'), MntConst.eTypeCode);
        for (let i = 1; i <= 7; i++) {
            this.riMaintenance.ReturnDataAdd('WindowType' + i, MntConst.eTypeText);
        }
        this.riMaintenance.Execute(this, function (data: any): any {
            for (let i = 1; i <= 7; i++) {
                this.parent.setControlValue('selQuickWindowSet' + i, data['WindowType' + this.utils.numberPadding(i, 2)]);
            }

            for (let i = 1; i <= 7; i++) {
                // checked failsafe case
                if (this.pgPM3) {
                    // Need to use pgPM3 and not "this" because it's anonymous
                    this.pgPM3.enableTimeWindowInput(i, (this.parent.getControlValue('selQuickWindowSet' + i) === 'C'));
                    this.pgPM3.enableTimeWindowInput(i + 7, (this.parent.getControlValue('selQuickWindowSet' + i) === 'C'));
                } else {
                    this.enableTimeWindowInput(i, (this.parent.getControlValue('selQuickWindowSet' + i) === 'C'));
                    this.enableTimeWindowInput(i + 7, (this.parent.getControlValue('selQuickWindowSet' + i) === 'C'));
                }
            }

            this.parent.isRequestingInitial = false;
            this.parent.updateModel(); //IUI-20683
            this.parent.navToSCMnt();
        }, 'POST', 6, false);
        if (!this.pageParams.glAllowUserAuthUpdate) {
            this.riMaintenance.CurrentMode = this.pageParams.CurrentMode = MntConst.eNormalMode;
            this.uiForm.disable();
            this.parent.disableControl('menu', false);
            this.pageParams.dtPremiseSRADate.disabled = true;
            this.pageParams.dtPurchaseOrderExpiryDate.disabled = true;
            this.parent.ellipsis.vehicleTypeEllipsis.disabled = true;
            this.pageParams.dtPNOLEffectiveDate.disabled = true;
        }
    }

    public SICCode_onchange(): void {
        let ctrl = this.uiForm.controls;
        if (ctrl.SICCode.value !== '') {
            this.riMaintenance.clear();
            this.riMaintenance.BusinessObject = 'iCABSPremiseEntry.p';
            this.riMaintenance.PostDataAdd('Function', 'GetSICDesc', MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('SICCode', ctrl.SICCode.value, MntConst.eTypeCode);
            this.riMaintenance.ReturnDataAdd('SICDesc', MntConst.eTypeText);
            this.riMaintenance.Execute(this, function (data: any): any {
                if (data['SICDescription'] !== '') {
                    this.parent.setControlValue('SICDesc', data['SICDescription']);
                } else {
                    this.riExchange.riInputElement.markAsError(this.uiForm, 'SICCode');
                    this.parent.setControlValue('SICDesc', '');
                }
            }, 'POST');
        } else {
            this.parent.setControlValue('SICDesc', '');
        }
    }

    public SICCode_onkeydown(obj: any): void {
        if (obj.KeyCode === 34 && this.pageParams.vSICCodeRequire && this.pageParams.vSICCodeEnable) {
            //TODO - Page not ready - Page down out of scope
            this.parent.navigate('LookUp-Premise', 'System/iCABSSSICSearch.htm');
        }
    }

    public PurchaseOrderNo_onChange(): void {
        this.parent.setControlValue('PurchaseOrderLineNo', '');
        this.parent.setControlValue('PurchaseOrderExpiryDate', '');
        this.riExchange.riInputElement.Enable(this.uiForm, 'PurchaseOrderExpiryDate');
        this.parent.dateDisable('PurchaseOrderExpiryDate', false, true);
        this.setPurchaseOrderFields();
    }

    public cmdMatchPremise_onClick(): void {
        if (this.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            this.MatchPremise();
            this.GetMatchPremiseNames();
        }
    }

    public GetMatchPremiseNames(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSPremiseEntry.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('Function', 'MatchPremiseNames', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('Action', '6', MntConst.eTypeInteger);
        this.riMaintenance.PostDataAdd('MatchedContractNumber', ctrl.MatchedContractNumber.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('MatchedPremiseNumber', ctrl.MatchedPremiseNumber.value, MntConst.eTypeInteger);
        this.riMaintenance.ReturnDataAdd('MatchedContractName', MntConst.eTypeText);
        this.riMaintenance.ReturnDataAdd('MatchedPremiseName', MntConst.eTypeText);
        this.riMaintenance.Execute(this, function (data: any): any {
            if (!data.hasError) {
                this.parent.setControlValue('MatchedContractName', data['MatchedContractName']);
                this.parent.setControlValue('MatchedPremiseName', data['MatchedPremiseName']);
            }
        }, 'POST');
    }


    public cmdViewLinkedPremises_onclick(): void {
        this.parent.sanitizeURL();
        this.parent.navigate('LinkedPremises', InternalGridSearchApplicationModuleRoutes.ICABSALINKEDPREMISESUMMARYGRID, {
            ContractNumber: this.parent.getControlValue('ContractNumber'),
            PremiseNumber: this.parent.getControlValue('PremiseNumber')
        });
    }

    public cmdViewAssociatedPremises_onclick(): void {
        if (this.riMaintenance.CurrentMode === MntConst.eModeAdd || this.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            //TODO - Page not ready
            this.parent.showAlert(MessageConstant.Message.PageNotCovered, 2);
            //this.parent.navigate('LookUp', 'Application/iCABSAAssociatedPremiseSummaryGrid.htm');
        }
    }


    public riMaintenance_BeforeAdd(): void {
        this.parent.pgPM3.setPurchaseOrderFields();
        this.riExchange.riInputElement.Enable(this.uiForm, 'SelPrintRequired');
        this.parent.setControlValue('SelPrintRequired', 1);
        if (this.pageParams.SCEnablePestNetOnlineProcessing && this.pageParams.SCEnablePestNetOnlineDefaults && this.riExchange.routerParams['CurrentContractTypeURLParameter'] !== '<product>') {
            this.parent.setControlValue('PNOL', true);
            this.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=GetPNOLDefault';
            this.riMaintenance.CBORequestAddCS('BusinessCode', MntConst.eTypeCode);
            this.riMaintenance.CBORequestExecute(this, function (data: any): any {
                this.logger.log('CBO Callback 3B', data);
            });
        }
        if (this.pageParams.vbEnableGlobalSiteRiskAssessment) {
            this.pageParams.vbGblSRAMode = '';
            this.PremGblSRADisable();
        }

    }

    public riMaintenance_AfterAbandonAdd(): void {
        this.parent.pgPM3.setPurchaseOrderFields();
        this.riExchange.riInputElement.Disable(this.uiForm, 'SelPrintRequired');
        this.riExchange.riInputElement.Disable(this.uiForm, 'SelServiceNotifyTemplateEmail');
        this.riExchange.riInputElement.Disable(this.uiForm, 'SelServiceNotifyTemplateSMS');
    }

    public riMaintenance_AfterAbandonUpdate(): void {
        this.riMaintenance_AfterAbandonAdd();
    }

    public CheckPostcode(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSCheckContractPostcode.p';
        this.riMaintenance.clear();
        if (this.pageParams.vbEnableValidatePostcodeSuburb) {
            this.riMaintenance.PostDataAdd('EnableValidatePostcodeSuburb', this.pageParams.vbEnableValidatePostcodeSuburb, MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('SearchPostcode', ctrl.PremisePostcode.value, MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('SearchState', ctrl.PremiseAddressLine5.value, MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('SearchTown', ctrl.PremiseAddressLine4.value, MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('strFoundPostcode', MntConst.eTypeText);
        } else {
            this.riMaintenance.PostDataAdd('EnableValidatePostcodeSuburb', this.pageParams.vbEnableValidatePostcodeSuburb, MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('SearchPostcode', ctrl.PremisePostcode.value, MntConst.eTypeCode);
            this.riMaintenance.ReturnDataAdd('strFoundPostcode', MntConst.eTypeText);
        }
        this.riMaintenance.Execute(this, function (data: any): any {
            if (!data.hasError) {
                if (data['strFoundPostcode'] === 'Yes') {
                    this.parent.ellipsis.premiseNumberEllipsis.childparams.PremisePostcode = this.parent.getControlValue('PremisePostcode');
                    this.parent.ellipsis.premiseNumberEllipsis.childparams.PremiseAddressLine1 = this.parent.getControlValue('PremiseAddressLine1');
                    this.parent.ellipsis.premiseNumberEllipsis.childparams.PremiseAddressLine2 = this.parent.getControlValue('PremiseAddressLine2');
                    this.parent.ellipsis.premiseNumberEllipsis.childparams.PremiseAddressLine3 = this.parent.getControlValue('PremiseAddressLine3');
                    this.parent.ellipsis.premiseNumberEllipsis.childparams.PremiseAddressLine4 = this.parent.getControlValue('PremiseAddressLine4');
                    this.parent.ellipsis.premiseNumberEllipsis.childparams.PremiseAddressLine5 = this.parent.getControlValue('PremiseAddressLine5');
                    this.parent.ellipsis.premiseNumberEllipsis.childparams.showAddNew = false;

                    if (this.pageParams.vbEnableValidatePostcodeSuburb) {
                        this.parent.ellipsis.premiseNumberEllipsis.childparams.parentMode = 'PremisePostcodeSearch';
                    } else {
                        this.parent.ellipsis.premiseNumberEllipsis.childparams.parentMode = 'PremisePostcodeSearchNoSuburb';
                    }
                    this.parent.PremiseSearchMode = 'POSTCODE';
                    this.parent.ellipsis.premiseNumberEllipsis.disabled = false;
                    setTimeout(() => {
                        this.parent.ellipsis.premiseNumberEllipsis.childparams.showAddNew = false;
                        if (this.riMaintenance.isModalOpen) {
                            this.parent.callbackHooks.push(function (): void {
                                this.parent.isModalOpen(true);
                                this.parent.premiseNumberEllipsis.openModal();
                            });
                        } else {
                            this.parent.isModalOpen(true);
                            this.parent.premiseNumberEllipsis.openModal();
                        }
                    }, 200);
                }
            }
        }, 'GET', 0);
    }

    private updateNotifyTemplateFromSelect(inputField: string, selectField: string, contactField: string): void {
        let selectValue = this.parent.getControlValue(selectField);
        this.parent.setControlValue(inputField, selectValue);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, contactField, (selectValue !== null) && (selectValue !== ''));
    }

    public SelServiceNotifyTemplateEmail_OnChange(): void {
        this.updateNotifyTemplateFromSelect('ServiceNotifyTemplateEmail', 'SelServiceNotifyTemplateEmail', 'PremiseContactEmail');
    }

    public SelServiceNotifyTemplateSMS_OnChange(): void {
        this.updateNotifyTemplateFromSelect('ServiceNotifyTemplateSMS', 'SelServiceNotifyTemplateSMS', 'PremiseContactMobile');
    }

    public PremiseCommenceDate_onBlur(): void {
        if (this.pageParams.SCEnablePestNetOnlineProcessing && this.pageParams.SCEnablePestNetOnlineDefaults
            && this.riExchange.routerParams['currentContractTypeURLParameter'] !== 'product'
            && this.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.pageParams.dtPNOLEffectiveDate.value = this.parent.globalize.parseDateStringToDate(this.parent.getControlValue('PremiseCommenceDate'));
            this.parent.setControlValue('PNOLEffectiveDate', this.parent.getControlValue('PremiseCommenceDate'));
        }
    }

    public GetCustomerTypeDefault(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSPremiseEntry.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('Function', 'GetCustomerTypeDefault', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('Action', '6', MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('CustomerTypeCode', ctrl.CustomerTypeCode.value, MntConst.eTypeCode);
        this.riMaintenance.ReturnDataAdd('ServiceNotifyTemplateEmail', MntConst.eTypeCode);
        this.riMaintenance.ReturnDataAdd('ServiceNotifyTemplateSMS', MntConst.eTypeCode);
        this.riMaintenance.Execute(this, function (data: any): any {
            if (!data.hasError) {
                this.parent.setControlValue('SelServiceNotifyTemplateEmail', data['ServiceNotifyTemplateEmail']);
                this.parent.setControlValue('SelServiceNotifyTemplateSMS', data['ServiceNotifyTemplateSMS']);
                this.parent.pgPM3.SelServiceNotifyTemplateEmail_OnChange();
                this.parent.pgPM3.SelServiceNotifyTemplateSMS_OnChange();
            }
        }, 'POST');
    }
}
