import { PremiseMaintenanceAddrRes } from './iCABSAPremiseMaintenanceAddrRes';
import { PremiseMaintenanceAddrRes1 } from './iCABSAPremiseMaintenanceAddrRes1';
import { PremiseMaintenanceAddrRes2 } from './iCABSAPremiseMaintenanceAddrRes2';

import { RiMaintenance, MntConst, RiTab } from './../../../shared/services/riMaintenancehelper';
import { RiExchange } from './../../../shared/services/riExchange';
import { Utils } from './../../../shared/services/utility';
import { SysCharConstants } from './../../../shared/constants/syscharservice.constant';

export class PremiseMaintenanceAddrRes3 {
    public sysCharConstants: SysCharConstants;

    //Duplicated Parent Class objects
    public utils: Utils;
    private xhr: any;
    private xhrParams: any;
    private uiForm: any;
    private controls: any;
    private uiDisplay: any;
    private pageParams: any;
    private attributes: any;
    private formData: any;
    private LookUp: any;
    private logger: any;
    private riExchange: RiExchange;
    private riMaintenance: RiMaintenance;
    private riTab: RiTab;
    private viewChild: any;

    public pgPM_AR: PremiseMaintenanceAddrRes;
    public pgPM_AR1: PremiseMaintenanceAddrRes1;
    public pgPM_AR2: PremiseMaintenanceAddrRes2;
    public pgPM_AR3: PremiseMaintenanceAddrRes3;

    constructor(private parent: any) {
        this.utils = this.parent.utils;
        this.logger = this.parent.logger;
        this.xhr = this.parent.xhr;
        this.xhrParams = this.parent.xhrParams;
        this.LookUp = this.parent.LookUp;
        this.uiForm = this.parent.uiForm;
        this.controls = this.parent.controls;
        this.uiDisplay = this.parent.uiDisplay;
        this.viewChild = this.parent.viewChild;
        this.pageParams = this.parent.pageParams;
        this.attributes = this.parent.attributes;
        this.formData = this.parent.formData;
        this.sysCharConstants = this.parent.sysCharConstants;
        this.riExchange = this.parent.riExchange;
        this.riMaintenance = this.parent.riMaintenance;
        this.riTab = this.parent.riTab;
    }

    public window_onload(): void {
        this.pgPM_AR = this.parent.pgPM_AR;
        this.pgPM_AR1 = this.parent.pgPM_AR1;
        this.pgPM_AR2 = this.parent.pgPM_AR2;
        this.pgPM_AR3 = this.parent.pgPM_AR3;
    }

    /***************************/

    public SetDateAccessFieldError(nextFieldIndex: number, msgbox: string, tabIndex: number, validateField: string, errorField1: string, errorField2: string): void {
        this.pgPM_AR2.validateField(msgbox, tabIndex, validateField + nextFieldIndex);
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, errorField1 + nextFieldIndex, true);
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, errorField2 + nextFieldIndex, true);
    }

    public RefreshTechs(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSPremiseEntryAddrResFunctions.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('RefreshTechs', 'Refresh', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.riExchange.ClientSideValues.Fetch('BusinessCode'), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeInteger);

        for (let i = 1; i <= 10; i++) {
            this.riMaintenance.ReturnDataAdd('EmployeeCode' + i, MntConst.eTypeCode);
            this.riMaintenance.ReturnDataAdd('EmployeeSurname' + i, MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('OccupationDesc' + i, MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('Priority' + i, MntConst.eTypeInteger);
            this.riMaintenance.ReturnDataAdd('AllowAllTasks' + i, MntConst.eTypeCode);
        }

        this.riMaintenance.Execute(this, function (data: any): any {
            for (let i = 1; i <= 10; i++) {
                this.parent.setControlValue('EmployeeCode' + i, data['EmployeeCode' + i]);
                this.parent.setControlValue('EmployeeSurname' + i, data['EmployeeSurname' + i]);
                this.parent.setControlValue('OccupationDesc' + i, data['OccupationDesc' + i]);
                this.parent.setControlValue('Priority' + i, data['Priority' + i]);
                this.parent.setControlValue('AllowAllTasks' + i, data['AllowAllTasks' + i]);
            }
        });
    }

    public RefreshDates(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSPremiseEntryAddrResFunctions.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('RefreshTimes', 'Refresh', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.riExchange.ClientSideValues.Fetch('BusinessCode'), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeInteger);

        for (let i = 1; i <= 10; i++) {
            this.riMaintenance.ReturnDataAdd('DateFrom' + i, MntConst.eTypeDate);
            this.riMaintenance.ReturnDataAdd('DateTo' + i, MntConst.eTypeDate);
        }

        this.riMaintenance.Execute(this, function (data: any): any {
            for (let i = 1; i <= 10; i++) {
                this.parent.setControlValue('DateFrom' + i, data['DateFrom' + i]);
                this.parent.setControlValue('DateTo' + i, data['DateTo' + i]);
            }
        });
    }

    public RefreshTimes(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSPremiseEntryAddrResFunctions.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('RefreshTimes', 'Refresh', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.riExchange.ClientSideValues.Fetch('BusinessCode'), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeInteger);

        for (let i = 1; i <= 10; i++) {
            this.riMaintenance.ReturnDataAdd('AccessFrom' + i, MntConst.eTypeTime);
            this.riMaintenance.ReturnDataAdd('AccessTo' + i, MntConst.eTypeTime);
        }

        this.riMaintenance.Execute(this, function (data: any): any {
            for (let i = 1; i <= 10; i++) {
                this.parent.setControlValue('AccessFrom' + i, data['AccessFrom' + i]);
                this.parent.setControlValue('AccessTo' + i, data['AccessTo' + i]);

                if (this.parent.getControlValue('AccessFrom' + i) === '00:00') {
                    this.parent.setControlValue('AccessFrom' + i, '');
                }

                if (this.parent.getControlValue('AccessTo' + i) === '00:00') {
                    this.parent.setControlValue('AccessTo' + i, '');
                }
            }
        });
    }

    private EmployeeCode_ondeactivateCommon(fieldIndex: number): void {
        if (this.parent.getControlValue('EmployeeCode' + fieldIndex) === '') {
            this.parent.setControlValue('EmployeeSurname' + fieldIndex, '');
            this.parent.setControlValue('OccupationDesc' + fieldIndex, '');
            this.parent.setControlValue('Priority' + fieldIndex, '');
            this.parent.setControlValue('AllowAllTasks' + fieldIndex, '');
        }
    }

    public EmployeeCode1_ondeactivate(): void {
        this.EmployeeCode_ondeactivateCommon(1);
    }

    public EmployeeCode2_ondeactivate(): void {
        this.EmployeeCode_ondeactivateCommon(2);
    }

    public EmployeeCode3_ondeactivate(): void {
        this.EmployeeCode_ondeactivateCommon(3);
    }

    public EmployeeCode4_ondeactivate(): void {
        this.EmployeeCode_ondeactivateCommon(4);
    }

    public EmployeeCode5_ondeactivate(): void {
        this.EmployeeCode_ondeactivateCommon(5);
    }

    public EmployeeCode6_ondeactivate(): void {
        this.EmployeeCode_ondeactivateCommon(6);
    }

    public EmployeeCode7_ondeactivate(): void {
        this.EmployeeCode_ondeactivateCommon(7);
    }

    public EmployeeCode8_ondeactivate(): void {
        this.EmployeeCode_ondeactivateCommon(8);
    }

    public EmployeeCode9_ondeactivate(): void {
        this.EmployeeCode_ondeactivateCommon(9);
    }

    public EmployeeCode10_ondeactivate(): void {
        this.EmployeeCode_ondeactivateCommon(10);
    }

    private AccessFrom_ondeactivateCommon(fieldIndex: number): void {
        if (this.parent.getControlValue('AccessFrom' + fieldIndex) === '') {
            this.parent.setControlValue('AccessTo' + fieldIndex, '');
        }
    }

    public AccessFrom1_ondeactivate(): void {
        this.AccessFrom_ondeactivateCommon(1);
    }

    public AccessFrom2_ondeactivate(): void {
        this.AccessFrom_ondeactivateCommon(2);
    }

    public AccessFrom3_ondeactivate(): void {
        this.AccessFrom_ondeactivateCommon(3);
    }

    public AccessFrom4_ondeactivate(): void {
        this.AccessFrom_ondeactivateCommon(4);
    }

    public AccessFrom5_ondeactivate(): void {
        this.AccessFrom_ondeactivateCommon(5);
    }

    public AccessFrom6_ondeactivate(): void {
        this.AccessFrom_ondeactivateCommon(6);
    }

    public AccessFrom7_ondeactivate(): void {
        this.AccessFrom_ondeactivateCommon(7);
    }

    public AccessFrom8_ondeactivate(): void {
        this.AccessFrom_ondeactivateCommon(8);
    }

    public AccessFrom9_ondeactivate(): void {
        this.AccessFrom_ondeactivateCommon(9);
    }

    public AccessFrom10_ondeactivate(): void {
        this.AccessFrom_ondeactivateCommon(10);
    }

    public AddTabs(): void {
        this.riTab.TabSet();
        this.riTab.TabClear();
        this.riTab.TabAdd('Address');
        this.riTab.TabAdd('General');
        this.riTab.TabAdd('Notification Methods');
        this.riTab.TabAdd('Day Restrictions');
        this.riTab.TabAdd('Techs');
        this.riTab.TabAdd('Data Restrictions'); //Is it Data or Date Restrictions?
        this.riTab.TabAdd('Access Times');
        this.riTab.TabDraw();
    }

    public ShowInvoiceNarrativeTab(): void {
        let blnShowTab = false;
        if (!this.pageParams.blnShowInvoiceNarrativeTab && this.parent.getControlValue('InvoiceGroupNumber')) {
            //By default, we will not show tab
            this.riMaintenance.clear();
            this.riMaintenance.BusinessObject = 'iCABSPremiseEntryAddrRes.p';
            //Some of these fields may be blank (e.g. when adding), in which case the request will determine whether the tab should be
            //shown by default. Otherwise the request returns whether the tab should be shown for this premise.
            this.riMaintenance.PostDataAdd('Function', 'ShowInvoiceTab', MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('Action', '6', MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('AccountNumber', this.parent.getControlValue('AccountNumber'), MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('InvoiceGroupNumber', this.parent.getControlValue('InvoiceGroupNumber'), MntConst.eTypeInteger);
            this.riMaintenance.ReturnDataAdd('ShowInvoiceNarrativeTab', MntConst.eTypeText);
            this.riMaintenance.Execute(this, function (data: any): any {
                if (!data['ShowInvoiceNarrativeTab']) {
                    blnShowTab = true;
                }
                //if ( global default has not yet been set, ) { set it according to blnShowTab.
                if (this.pageParams.blnShowInvoiceNarrativeTab === '' && this.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                    this.pageParams.blnShowInvoiceNarrativeTab = blnShowTab;
                }

                if (blnShowTab) {
                    this.riTab.TabAdd('Invoice Narrative');
                    this.riTab.TabDraw();
                }
            }, 'POST', 6, false);
        } else {
            this.riTab.TabAdd('Invoice Narrative');
            this.riTab.TabDraw();
        }
    }

    // to test if a tech has been entered more than once
    public TestForDupTechs(strTestString: any): boolean {
        let vEmpCodesArray = [];
        let TestForDupTechs = false;

        let ctrl = this.uiForm.controls;
        let vCombEmpCodes = ctrl.EmployeeCode1.value + ',' + ctrl.EmployeeCode2.value + ','
            + ctrl.EmployeeCode3.value + ',' + ctrl.EmployeeCode4.value + ','
            + ctrl.EmployeeCode5.value + ',' + ctrl.EmployeeCode6.value + ','
            + ctrl.EmployeeCode7.value + ',' + ctrl.EmployeeCode8.value + ','
            + ctrl.EmployeeCode9.value + ',' + ctrl.EmployeeCode10.value;

        vEmpCodesArray = vCombEmpCodes.split(',').splice(0, 10);

        if (this.utils.mid(strTestString, this.utils.len(strTestString), 1) === '1') {
            for (let i = 0; !TestForDupTechs && i < 9; i++) {
                for (let j = i + 1; !TestForDupTechs && j < 10; j++) {
                    if (vEmpCodesArray[i].trim() !== '' && vEmpCodesArray[i] === vEmpCodesArray[j]) {
                        TestForDupTechs = true;
                    }
                }
            }
        }

        return TestForDupTechs;
    }

    public ValidatePostcodeSuburb(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSPremiseEntryAddrResFunctions.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('ValidatePostcodeSuburb', 'ValPost', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.riExchange.ClientSideValues.Fetch('BusinessCode'), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('PremiseAddressLine4', ctrl.PremiseAddressLine4.value, MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('PremiseAddressLine5', ctrl.PremiseAddressLine5.value, MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('PremisePostcode', ctrl.PremisePostcode.value, MntConst.eTypeCode);
        this.riMaintenance.ReturnDataAdd('ValidatePostcodeSuburbError', MntConst.eTypeText);
        this.riMaintenance.Execute(this, function (data: any): any {
            if (data['ValidatePostcodeSuburbError'] === 'true') {
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PremiseAddressLine4', true);
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PremiseAddressLine5', true);
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PremisePostcode', true);
                this.riTab.TabFocus(1);
                this.riMaintenance.CancelEvent = true;
            }
        });
    }

    public ValidateTechs(callback?: any): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSPremiseEntryAddrResFunctions.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('ValidateTechs', 'Validate', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('BranchNumber', this.riExchange.ClientSideValues.Fetch('BranchNumber'), MntConst.eTypeInteger);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('PremiseNumber', ctrl.PremiseNumber.value, MntConst.eTypeInteger);

        // Add in all employee codes
        for (let i = 1; i <= 10; i++) {
            this.riMaintenance.PostDataAdd('EmployeeCode' + i, ctrl.EmployeeCode1.value, MntConst.eTypeCode);
        }

        this.riMaintenance.ReturnDataAdd('ErrEmployeeCode', MntConst.eTypeText);
        this.riMaintenance.Execute(this, function (data: any): any {
            this.logger.log('ValidateTechs callback', data);
            let empErrNum = data.ErrEmployeeCode;
            if (this.parent.getControlValue('EmployeeCode' + empErrNum) !== '') {
                this.pgPM_AR2.validateField('Invalid Employee code', 6, 'EmployeeCode' + empErrNum);
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'EmployeeCode' + empErrNum, true);
                this.riMaintenance.CancelEvent = true;
                this.pageParams.ValidateTechs = true;
            }

            if (typeof callback === 'function') {
                callback.call(this);
            }
        }, 'POST', 0);
    }

    public CheckDatesErr(vDateFrom: any, vDateTo: any, v1DateFrom: any, v1DateTo: any): boolean {
        let verror = false;
        this.riMaintenance.CancelEvent = false;

        let strdate = '01/01/1900';

        let DateFrom = '';
        let DateTo = '';
        let compDateFrom = '';
        let compDateTo = '';

        let Date1Int = 0;
        let Date2Int = 0;
        let Date3Int = 0;
        let Date4Int = 0;

        if (this.utils.StrComp(this.utils.ucase(this.utils.trim(vDateFrom)), '') !== 0) {
            if (this.utils.StrComp(this.utils.ucase(this.utils.trim(vDateTo)), '') !== 0) {
                if (this.utils.StrComp(this.utils.ucase(this.utils.trim(v1DateFrom)), '') !== 0) {
                    if (this.utils.StrComp(this.utils.ucase(this.utils.trim(v1DateTo)), '') !== 0) {

                        DateFrom = this.utils.mid(vDateFrom, 1, 10);
                        DateTo = this.utils.mid(vDateTo, 1, 10);
                        compDateFrom = this.utils.mid(v1DateFrom, 1, 10);
                        compDateTo = this.utils.mid(v1DateTo, 1, 10);

                        Date1Int = this.utils.DateDiff('d', strdate, DateFrom);
                        Date2Int = this.utils.DateDiff('d', strdate, DateTo);
                        Date3Int = this.utils.DateDiff('d', strdate, compDateFrom);
                        Date4Int = this.utils.DateDiff('d', strdate, compDateTo);

                        if (Date3Int <= Date1Int) {
                            if (Date4Int >= Date1Int) {
                                verror = true;
                            }
                        } else if (Date3Int >= Date1Int) {
                            if (Date3Int <= Date2Int) {
                                verror = true;
                            }
                        }
                    }
                    if (verror) {
                        this.riMaintenance.CancelEvent = true;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public CheckTimesErr(vTimeFrom: any, vTimeTo: any, v1TimeFrom: any, v1TimeTo: any): boolean {
        let verror = false;
        this.riMaintenance.CancelEvent = false;

        if (this.utils.StrComp(vTimeFrom, '') !== 0) {
            if (this.utils.StrComp(vTimeTo, '') !== 0) {
                if (this.utils.StrComp(v1TimeFrom, '') !== 0) {
                    if (this.utils.StrComp(v1TimeTo, '') !== 0) {
                        let TimeFrom = this.utils.mid(vTimeFrom, 1, 5);
                        let TimeTo = this.utils.mid(vTimeTo, 1, 5);
                        let compTimeFrom = this.utils.mid(v1TimeFrom, 1, 5);
                        let compTimeTo = this.utils.mid(v1TimeTo, 1, 5);

                        if (this.utils.TimeValue(compTimeFrom) <= this.utils.TimeValue(TimeFrom)) {
                            if (this.utils.TimeValue(compTimeTo) >= this.utils.TimeValue(TimeFrom)) {
                                verror = true;
                            }
                        } else if (this.utils.TimeValue(compTimeFrom) >= this.utils.TimeValue(TimeFrom)) {
                            if (this.utils.TimeValue(compTimeFrom) <= this.utils.TimeValue(TimeTo)) {
                                verror = true;
                            }
                        }
                        if (verror) {
                            this.riMaintenance.CancelEvent = true;
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
