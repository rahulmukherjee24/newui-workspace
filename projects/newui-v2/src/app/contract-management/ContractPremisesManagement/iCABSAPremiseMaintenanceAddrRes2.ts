import { InternalGridSearchServiceModuleRoutes, InternalMaintenanceSalesModuleRoutes } from './../../base/PageRoutes';
import { PremiseMaintenanceAddrRes } from './iCABSAPremiseMaintenanceAddrRes';
import { PremiseMaintenanceAddrRes1 } from './iCABSAPremiseMaintenanceAddrRes1';
import { PremiseMaintenanceAddrRes3 } from './iCABSAPremiseMaintenanceAddrRes3';

import { RiMaintenance, MntConst, RiTab } from './../../../shared/services/riMaintenancehelper';
import { RiExchange } from './../../../shared/services/riExchange';
import { Utils } from './../../../shared/services/utility';
import { SysCharConstants } from './../../../shared/constants/syscharservice.constant';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ContractManagementModuleRoutes } from '../../base/PageRoutes';
import { ContractActionTypes } from '../../actions/contract';
import { InternalGridSearchSalesModuleRoutes } from './../../base/PageRoutes';

export class PremiseMaintenanceAddrRes2 {
    public sysCharConstants: SysCharConstants;

    //Duplicated Parent Class objects
    public utils: Utils;
    private xhr: any;
    private xhrParams: any;
    private uiForm: any;
    private controls: any;
    private uiDisplay: any;
    private pageParams: any;
    private attributes: any;
    private formData: any;
    private LookUp: any;
    private logger: any;
    private riExchange: RiExchange;
    private riMaintenance: RiMaintenance;
    private riTab: RiTab;
    private viewChild: any;
    public ellipsis: any;
    public pgPM_AR: PremiseMaintenanceAddrRes;
    public pgPM_AR1: PremiseMaintenanceAddrRes1;
    public pgPM_AR2: PremiseMaintenanceAddrRes2;
    public pgPM_AR3: PremiseMaintenanceAddrRes3;

    constructor(private parent: any) {
        this.utils = this.parent.utils;
        this.logger = this.parent.logger;
        this.xhr = this.parent.xhr;
        this.xhrParams = this.parent.xhrParams;
        this.LookUp = this.parent.LookUp;
        this.uiForm = this.parent.uiForm;
        this.controls = this.parent.controls;
        this.ellipsis = this.parent.ellipsis;
        this.uiDisplay = this.parent.uiDisplay;
        this.viewChild = this.parent.viewChild;
        this.pageParams = this.parent.pageParams;
        this.attributes = this.parent.attributes;
        this.formData = this.parent.formData;
        this.sysCharConstants = this.parent.sysCharConstants;
        this.riExchange = this.parent.riExchange;
        this.riMaintenance = this.parent.riMaintenance;
        this.riTab = this.parent.riTab;
    }

    //TODO - Move to parent - Duplicated in iCABSAPremiseMaintenanceAddrRes1.ts
    public PremiseAddressLine1Orignal: string = '';
    public PremiseAddressLine2Orignal: string = '';
    public PremiseAddressLine3Orignal: string = '';
    public PremiseAddressLine4Orignal: string = '';
    public PremiseAddressLine5Orignal: string = '';
    public PremisePostcodeOrignal: string = '';
    public HaveResolved: boolean = false;
    public PBusinessCode: string = '';

    public window_onload(): void {
        this.pgPM_AR = this.parent.pgPM_AR;
        this.pgPM_AR1 = this.parent.pgPM_AR1;
        this.pgPM_AR2 = this.parent.pgPM_AR2;
        this.pgPM_AR3 = this.parent.pgPM_AR3;
    }

    public validateField(message: string, tabIndex: number, elem: string, patterStr?: any): boolean {
        if (message.length > 0) setTimeout(() => { this.parent.showAlert(message); }, 300);
        this.riTab.TabFocus(tabIndex);
        this.riExchange.riInputElement.markAsError(this.uiForm, elem);
        return true;
    }

    private verifyConfirmationField(fieldName: string, errorMsg: string, tabIndex: number = 4): boolean {
        let error: boolean = false;
        let fieldValue = this.parent.getControlValue(fieldName).trim().toUpperCase();
        if (fieldValue !== '' && fieldValue !== 'Y') {
            error = this.validateField(errorMsg, tabIndex, fieldValue);
        }
        return error;
    }

    private verifyTelephoneNumber(confirmFieldName: string, detailFieldName: string, lengthErrorMsg: string, charErrorMsg: string, tabIndex: number = 4, minDigits: number = 10): boolean {
        let error: boolean = false;
        if (this.parent.getControlValue(confirmFieldName).trim().toUpperCase() === 'Y') {
            let detailValue = this.parent.getControlValue(detailFieldName).trim().toUpperCase();
            if (detailValue.length < minDigits) {
                error = this.validateField(lengthErrorMsg, tabIndex, detailFieldName);
            } else if (this.utils.TestForChar(detailValue)) {
                error = this.validateField(charErrorMsg, tabIndex, detailFieldName);
            }
        }
        return error;
    }

    private verifyEmailAddress(confirmFieldName: string, detailFieldName: string, blankErrorMsg: string, invalidErrorMsg: string, tabIndex: number = 4): boolean {
        let error: boolean = false;
        if (this.parent.getControlValue(confirmFieldName).trim().toUpperCase() === 'Y') {
            let detailValue = this.parent.getControlValue(detailFieldName).trim().toUpperCase();
            if (detailValue === '') {
                error = this.validateField(blankErrorMsg, tabIndex, detailFieldName);
            } else if (this.utils.validateEmail(detailValue)) {
                error = this.validateField(invalidErrorMsg, tabIndex, detailFieldName);
            }
        }
        return error;
    }

    private validateFieldIsInt(fieldName: string, errMsg: string = '', tabIndex: number = 4): boolean {
        let vErr: boolean = false;
        if (!isNaN(parseInt(this.parent.getControlValue(fieldName), 10))) {
            vErr = this.validateField(errMsg, tabIndex, fieldName);
        }
        return vErr;
    }

    private validateFieldIsIntLessThan(fieldName: string, maxValue: number, errMsg: string = '', tabIndex: number = 4): boolean {
        let vErr: boolean = false;
        if (!isNaN(parseInt(this.parent.getControlValue(fieldName), 10))) {
            if (parseInt(this.parent.getControlValue(fieldName), 10) > maxValue) {
                vErr = this.validateField(errMsg, tabIndex, fieldName);
            }
        } else {
            vErr = this.validateField('', tabIndex, fieldName);
        }
        return vErr;
    }

    //************************************************************************************
    //* BEFORE SAVE                                                                      *
    //************************************************************************************
    public riMaintenance_BeforeSave(): void {
        let ctrl = this.uiForm.controls;
        this.logger.log('LOG riMaintenance_BeforeSave', ctrl);
        let verror = false;
        let testCtrl: string = '';
        let testStr2: number = 0;

        let DateErrMsg = MessageConstant.PageSpecificMessage.DateErrMsg;

        this.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=CheckLength';
        this.riMaintenance.CBORequestAddCS('BusinessCode', MntConst.eTypeCode);
        if (this.parent.getControlValue('PremiseSpecialInstructions') !== '') {
            this.riMaintenance.CBORequestAdd('PremiseSpecialInstructions');
        }
        this.riMaintenance.CBORequestExecute(this, function (data: any): any {
            this.logger.log('CBORequestExecute:', data);
            //Display error message
            if (data.hasError) {
                this.parent.showAlert(data.errorMessage);
                this.parent.setControlValue('ErrorMessageDesc', data.errorMessage);
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PremiseSpecialInstructions', true);
                verror = true;
            }
        });

        // Validate input fields - validate the ServiceAdjHrs cannot be greater than 2
        if (!verror) {
            verror = this.validateFieldIsIntLessThan('ServiceAdjHrs', 2, MessageConstant.PageSpecificMessage.ServiceAdjHrs);
        }
        //validate the ServiceAdjMins cannot be greater than 59
        if (!verror) {
            verror = this.validateFieldIsIntLessThan('ServiceAdjMins', 59, MessageConstant.PageSpecificMessage.ServiceAdjMins);
        }
        // has a numeric character been entered
        if (!verror) {
            verror = this.validateFieldIsInt('PremiseType');
        }
        // test if (SS or NS has been entered in the PremiseType input field
        if (!verror) {
            if (this.parent.getControlValue('PremiseType') === '') {
                this.parent.setControlValue('PremiseType', 'NS');
            }
            if (this.parent.getControlValue('PremiseType').trim().toUpperCase() !== 'NS'
                && this.parent.getControlValue('PremiseType').trim().toUpperCase() !== 'SS') {
                verror = this.validateField(MessageConstant.PageSpecificMessage.PremiseType, 4, 'PremiseType');
            }
        }

        // test that a Y or a blank has been entered in the proof of service by fax method
        if (!verror) {
            verror = this.verifyConfirmationField('ProofOfServFax', MessageConstant.PageSpecificMessage.ProofOfServFax);
        }
        // test the length of the POS fax number it must be 10 digits long and there are no chars in it
        if (!verror) {
            verror = this.verifyTelephoneNumber('ProofOfServFax', 'PosFax', MessageConstant.PageSpecificMessage.PosFax, MessageConstant.PageSpecificMessage.PosFaxChar);
        }
        // test that a Y or a blank has been entered in the proof of service by SMS method
        if (!verror) {
            verror = this.verifyConfirmationField('ProofOfServSMS', MessageConstant.PageSpecificMessage.ProofOfServSMS);
        }
        // test the length of the POS SMS number it must be 10 digits long and there are no chars in it
        if (!verror) {
            verror = this.verifyTelephoneNumber('ProofOfServSMS', 'PosSMS', MessageConstant.PageSpecificMessage.PosSMS, MessageConstant.PageSpecificMessage.PosSMSChar);
        }
        // test that a Y or a blank has been entered in the proof of service by email method
        if (!verror) {
            verror = this.verifyConfirmationField('ProofOfServEmail', MessageConstant.PageSpecificMessage.ProofOfServEmail);
        }
        // test that the their is an email address if (it has been chosen as a delivery method
        // check that the email address does !have any invalid characters
        if (!verror) {
            verror = this.verifyEmailAddress('ProofOfServEmail', 'PosEmail', MessageConstant.PageSpecificMessage.PosEmail, MessageConstant.PageSpecificMessage.EmailInvalid);
        }
        // test that a Y or a blank has been entered in the notify of service by fax method
        if (!verror) {
            verror = this.verifyConfirmationField('NotifyFax', MessageConstant.PageSpecificMessage.NotifyFax);
        }
        // test the length of the Notify of service fax number it must be 10 digits long and there are no chars in it
        if (!verror) {
            verror = this.verifyTelephoneNumber('NotifyFax', 'NotifyFaxDetail', MessageConstant.PageSpecificMessage.NotifyFax10, MessageConstant.PageSpecificMessage.NotifyFaxChar);
        }
        // test that a Y or a blank has been entered in the notify of service by SMS method
        if (!verror) {
            verror = this.verifyConfirmationField('NotifySMS', MessageConstant.PageSpecificMessage.NotifySMS);
        }
        // test the length of the Notify of service SMS number it must be 10 digits long and there are no chars in it
        if (!verror) {
            verror = this.verifyTelephoneNumber('NotifySMS', 'NotifySMSDetail', MessageConstant.PageSpecificMessage.NotifySMS10, MessageConstant.PageSpecificMessage.NotifySMSChar);
        }

        // test that a Y or a blank has been entered in the notify of service by Phone method
        if (!verror) {
            verror = this.verifyConfirmationField('NotifyPhone', MessageConstant.PageSpecificMessage.NotifyPhone);
        }
        // test the length of the Notify of service Phone number it must be 10 digits long and there are no chars in it
        if (!verror) {
            verror = this.verifyTelephoneNumber('NotifyPhone', 'NotifyPhoneDetail', MessageConstant.PageSpecificMessage.NotifyPhone10, MessageConstant.PageSpecificMessage.NotifyPhoneChar);
        }

        // test that a Y or a blank has been entered in the notify of service by email method
        if (!verror) {
            verror = this.verifyConfirmationField('NotifyEmail', MessageConstant.PageSpecificMessage.NotifyEmail);
        }
        // test that the their is an email address if (it has been chosen as a delivery method
        // also check that the email address does !have any invalid characters
        if (!verror) {
            verror = this.verifyEmailAddress('NotifyEmail', 'NotifyEmailDetail', MessageConstant.PageSpecificMessage.NotifyEmailBlank, MessageConstant.PageSpecificMessage.EmailInvalid);
        }
        // check that the NotifyDaysBefore input field is !greater than 14 (or less than 0 just in some user is trying to smart)
        if (!verror) {
            testCtrl = 'NotifyDaysBefore';
            testStr2 = parseInt(this.parent.getControlValue(testCtrl), 10);
            if (!isNaN(testStr2)) {
                if (testStr2 > 14) {
                    verror = this.validateField(MessageConstant.PageSpecificMessage.NotifyDaysBefore14, 4, testCtrl);
                } else if (testStr2 < 0) {
                    verror = this.validateField(MessageConstant.PageSpecificMessage.NotifyDaysBefore0, 4, testCtrl);
                }
            }
        }

        if (!verror) {
            for (let i = 1; !verror && i <= 10; i++) {
                if (!this.utils.TestForY(this.parent.getControlValue('AllowAllTasks' + i))) {
                    verror = this.validateField(MessageConstant.PageSpecificMessage.AllowAllTasks, 6, 'AllowAllTasks' + i);
                }
            }
        }

        if (!verror) {
            for (let i = 1; !verror && i <= 10; i++) {
                if (this.pgPM_AR3.TestForDupTechs(this.parent.getControlValue('EmployeeCode' + i) + 1) && this.parent.getControlValue('EmployeeCode' + i) !== '') {
                    verror = this.validateField(MessageConstant.PageSpecificMessage.TechAddOnce, 6, 'EmployeeCode' + i);
                }
            }
        }

        if (!verror && this.pageParams.vSCEnableValidatePostcodeSuburb) {
            this.pgPM_AR3.ValidatePostcodeSuburb();
        }

        if (!verror) {
            this.pgPM_AR3.ValidateTechs(function (): void {

                for (let i = 1; !verror && i <= 10; i++) {
                    if ((this.utils.StrComp(this.parent.getControlValue('DateFrom' + i), '') !== 0) &&
                        (this.utils.StrComp(this.parent.getControlValue('DateTo' + i), '') !== 0) &&
                        (this.utils.DateDiff('d', this.parent.getControlValue('DateFrom' + i), this.parent.getControlValue('DateTo' + i)) < 0)) {
                        this.parent.showAlert(DateErrMsg);
                        this.riTab.TabFocus(7);
                        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'DateFrom' + i, true);
                        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'DateTo' + i, true);
                        verror = true;
                    }
                }

                if (!verror) {
                    for (let i = 1; !verror && i <= 10; i++) {
                        for (let j = i + 1; !verror && j <= 10; i++) {
                            if (this.pgPM_AR3.CheckDatesErr(
                                ctrl['DateFrom' + i].value,
                                ctrl['DateTo' + i].value,
                                ctrl['DateFrom' + j].value,
                                ctrl['DateTo' + j].value
                            )) {
                                this.pgPM_AR3.SetDateAccessFieldError(j,
                                    'A date within this range has already been added',
                                    7,
                                    'DateTo',
                                    'DateFrom',
                                    'DateTo');
                                verror = true;
                            }
                        }
                    }
                }

                if (!verror) {
                    for (let i = 1; !verror && i <= 10; i++) {
                        if ((this.utils.StrComp(ctrl['AccessFrom' + i].value, '') !== 0) &&
                            (this.utils.StrComp(ctrl['AccessTo' + i].value, '') !== 0) &&
                            (this.utils.TimeValue(ctrl['AccessFrom' + i].value) >= this.utils.TimeValue(ctrl['AccessTo' + i].value))) {
                            this.riTab.TabFocus(8);
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'AccessFrom' + i, true);
                            verror = true;
                        }
                    }
                }

                if (!verror) {
                    for (let i = 1; !verror && i <= 10; i++) {
                        for (let j = i + 1; !verror && j <= 10; i++) {
                            if (this.pgPM_AR3.CheckTimesErr(
                                ctrl['AccessFrom' + i].value,
                                ctrl['AccessTo' + i].value,
                                ctrl['AccessFrom' + j].value,
                                ctrl['AccessTo' + j].value
                            )) {
                                this.pgPM_AR3.SetDateAccessFieldError(j,
                                    'This time range has already been defined',
                                    8,
                                    'AccessFrom',
                                    'AccessFrom',
                                    'AccessTo');
                                verror = true;
                            }
                        }
                    }
                }
            });
        }

        this.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=WarnSRAEmployee';
        if (ctrl.PremiseSRAEmployee.value.trim() !== '') {
            this.riMaintenance.CBORequestAdd('PremiseSRAEmployee');
        }
        if (ctrl.PremiseSRADate.value !== '') {
            this.riMaintenance.CBORequestAdd('PremiseSRADate');
        }
        this.riMaintenance.CBORequestExecute(this, function (data: any): any {
            if (data.hasError) {
                this.parent.showAlert(data.errorMessage);
                this.parent.setControlValue('ErrorMessageDesc', data.errorMessage);
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PremiseSRAEmployee', true);
                verror = true;
            }
        });

        //Check Address and Postcode
        if (this.pageParams.vSCPostCodeMustExistInPAF && (this.pageParams.vSCEnableHopewiserPAF || this.pageParams.vSCEnableDatabasePAF)) {
            this.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=CheckPostcode';
            this.riMaintenance.CBORequestAddCS('BusinessCode', MntConst.eTypeCode);

            if (ctrl.PremiseName.value !== '') this.riMaintenance.CBORequestAdd('PremiseName');
            if (ctrl.PremiseAddressLine1.value !== '') this.riMaintenance.CBORequestAdd('PremiseAddressLine1');
            if (ctrl.PremiseAddressLine2.value !== '') this.riMaintenance.CBORequestAdd('PremiseAddressLine2');
            if (ctrl.PremiseAddressLine3.value !== '') this.riMaintenance.CBORequestAdd('PremiseAddressLine3');
            if (ctrl.PremiseAddressLine4.value !== '') this.riMaintenance.CBORequestAdd('PremiseAddressLine4');
            if (ctrl.PremiseAddressLine5.value !== '') this.riMaintenance.CBORequestAdd('PremiseAddressLine5');
            if (ctrl.PremisePostcode.value !== '') this.riMaintenance.CBORequestAdd('PremisePostcode');

            this.riMaintenance.CBORequestExecute(this, function (data: any): any {
                if (data.hasError) {
                    this.parent.showAlert(data.errorMessage);
                    this.parent.setControlValue('ErrorMessageDesc', data.errorMessage);
                    this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PremiseSRAEmployee', true);
                    verror = true;
                }
            });
        }

        if (verror) {
            this.riMaintenance.CancelEvent = true;
        }

        this.riMaintenance.CustomBusinessObjectAdditionalPostData = '';

        this.PremiseAddressLine1Orignal = this.parent.getControlValue('PremiseAddressLine1');
        this.PremiseAddressLine2Orignal = this.parent.getControlValue('PremiseAddressLine2');
        this.PremiseAddressLine3Orignal = this.parent.getControlValue('PremiseAddressLine3');
        this.PremiseAddressLine4Orignal = this.parent.getControlValue('PremiseAddressLine4');
        this.PremiseAddressLine5Orignal = this.parent.getControlValue('PremiseAddressLine5');
        this.PremisePostcodeOrignal = this.parent.getControlValue('PremisePostcode');
        this.HaveResolved = true;
    }

    public riMaintenance_AfterSave(): void {
        this.parent.setControlValue('ErrorMessageDesc', '');
        if (this.parent.actionSave === 1) this.parent.setControlValue('ContractTypeCode', this.pageParams.currentContractType);

        let fieldsArr = this.riExchange.getAllCtrl(this.controls);
        this.riMaintenance.clear();
        for (let i = 0; i < fieldsArr.length; i++) {
            let id = fieldsArr[i];
            let dataType = this.riMaintenance.getControlType(this.controls, id, 'type');
            let value = this.parent.getControlValue(id);
            switch (id) {
                case 'PremiseROWID':
                case 'PremiseRowID':
                    value = this.parent.getControlValue('Premise');
                    break;
            }
            this.riMaintenance.PostDataAdd(id, value, dataType);
        }

        this.riMaintenance.PostDataAdd('Mode', 'NewUI', MntConst.eTypeText);

        this.riMaintenance.Execute(this, function (data: any): any {
            if (data.hasError) {
                if (data.errorMessage.trim() !== '') {
                    this.logger.log('Post data Error', data);
                    this.parent.showAlert(data.errorMessage);
                }
            } else {
                this.logger.log('Post data Saved', data);
                this.parent.routeAwayGlobals.setSaveEnabledFlag(false);
                if (data.hasOwnProperty('InvoiceGroupDesc')) {
                    if (data.InvoiceGroupDesc !== '') {
                        this.parent.showAlert(MessageConstant.Message.SavedSuccessfully, 1);
                        this.parent.callbackHooks.push(function (): void {
                            this.parent.riMaintenance.renderResponseForCtrl(this, data);
                            this.parent.riMaintenance.CurrentMode = MntConst.eModeUpdate;
                            this.parent.pgPM2.riMaintenance_AfterSaveAdd_clbk();
                        });
                    } else {
                        this.parent.showAlert(MessageConstant.Message.SaveUnSuccessful, 0);
                    }
                }
            }
        }, 'POST', this.parent.actionSave);
    }

    //************************************************************************************
    //* AFTER SAVE ADD                                                                   *
    //************************************************************************************
    public riMaintenance_AfterSaveAdd_clbk(): void {
        //validate contracts are not added without premises
        if (this.pageParams.ParentMode === 'Contract-Add') {
            this.pageParams.PremiseAdded = true;
        }
        if (this.pageParams.CurrentContractType === 'C' || this.pageParams.CurrentContractType === 'J') {
            this.parent.navigate('Premise-Add', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE);
        } else {  //(Product Sales)
            this.parent.navigate('Premise-Add', InternalGridSearchServiceModuleRoutes.ICABSAPRODUCTSALESSCENTRYGRID.URL_1);
        }
        this.HaveResolved = true;
    }

    //************************************************************************************
    //* Functions (DefaultFromPostcode / CheckPostcode)                                  *
    //************************************************************************************
    public DefaultFromPostcode(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=DefaultFromPostcode' + '&ContractTypeCode=' + this.pageParams.CurrentContractType + '&NegBranchNumber=' + ctrl.NegBranchNumber.value + '&CurrentLoggedInBranch=' + this.utils.getBranchCode();

        this.riMaintenance.CBORequestAddCS('BusinessCode', MntConst.eTypeCode);
        this.riMaintenance.CBORequestAdd('ContractNumber');
        this.riMaintenance.CBORequestAdd('PremisePostcode');
        this.riMaintenance.CBORequestAdd('PremiseAddressLine5'); //State
        this.riMaintenance.CBORequestAdd('PremiseAddressLine4'); //Town
        this.riMaintenance.CBORequestExecute(this, function (data: any): any {
            this.logger.log('CBORequestExecute:', data);
            if (data.hasError) {
                this.parent.showAlert(data.errorMessage);
                this.parent.setControlValue('ErrorMessageDesc', data.errorMessage);
            }
        });
    }

    public CheckPostcode(): void {
        let ctrl = this.uiForm.controls;
        this.riMaintenance.BusinessObject = 'iCABSCheckContractPostcode.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('ContractNumber', ctrl.ContractNumber.value, MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('SearchPostcode', ctrl.SearchPostcode.value, MntConst.eTypeCode);
        this.riMaintenance.ReturnDataAdd('strFoundPostcode', MntConst.eTypeText);

        this.riMaintenance.Execute(this, function (data: any): any {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'SearchPostcode', (data['strFoundPostcode'] === 'Yes'));
        });
    }


    //************************************************************************************
    //* Menu                                                                             *
    //************************************************************************************
    public BuildMenuOptions(): void {
        let menuOptions = [];
        menuOptions.push({ value: 'ContactManagement', label: 'Contact Management' });
        menuOptions.push({ value: 'ServiceCover', label: 'Service Covers' });
        menuOptions.push({ value: 'History', label: 'Premises History' });
        menuOptions.push({ value: 'ProRataCharge', label: 'Pro Rata Charge' });
        if (this.pageParams.vEnableLocations) {
            menuOptions.push({ value: 'Allocate', label: 'Allocate Locations' });
        }
        menuOptions.push({ value: 'InvoiceNarrative', label: 'Invoice Narrative' });
        menuOptions.push({ value: 'InvoiceCharge', label: 'Invoice Charge' });
        menuOptions.push({ value: 'InvoiceHistory', label: 'Invoice History' });
        menuOptions.push({ value: 'StaticVisits', label: 'Static Visits (SOS)' });
        menuOptions.push({ value: 'VisitSummary', label: 'Visit Summary' });
        menuOptions.push({ value: 'StateOfService', label: 'State of Service' });
        menuOptions.push({ value: 'CustomerInformation', label: 'Customer Information' });
        if (this.pageParams.vEnableInstallsRemovals) {
            menuOptions.push({ value: 'ThirdPartyInstall', label: '3rd Party Installation Note' });
        }
        menuOptions.push({ value: 'EventHistory', label: 'Event History' });
        menuOptions.push({ value: 'Contract', label: 'Contract' });

        this.parent.dropDown.menu = menuOptions;
        setTimeout(() => { this.parent.setControlValue('menu', 'Options'); }, 500);
    }

    //************************************************************************************
    //* Menu - onchange/ondeactivate actions                                             *
    //************************************************************************************
    public menu_onchange(menu: any): void {
        this.logger.log('menu_onchange', menu);
        let ctrl = this.uiForm.controls;
        if (this.riMaintenance.RecordSelected()) {
            let blnAccess: boolean = (this.riExchange.ClientSideValues.Fetch('FullAccess') === 'Full' ||
                this.riExchange.ClientSideValues.Fetch('BranchNumber') === ctrl.ServiceBranchNumber.value ||
                this.riExchange.ClientSideValues.Fetch('BranchNumber') === ctrl.NegBranchNumber.value);

            switch (menu) {
                case 'ContactManagement': this.cmdContactManagement_onclick(); break;
                case 'ServiceCover': this.cmdServiceSummary_onclick(); break;
                case 'History': if (blnAccess) this.cmdHistory_onclick(); break;
                case 'Allocate': if (blnAccess) this.cmdAllocate_onclick(); break;
                case 'InvoiceNarrative': if (blnAccess) this.cmdInvoiceNarrative_onclick(); break;
                case 'ProRataCharge': if (blnAccess) this.cmdProRata_onclick(); break;
                case 'InvoiceCharge': if (blnAccess) this.cmdInvoiceCharge_onclick(); break;
                case 'InvoiceHistory': if (blnAccess) this.cmdInvoiceHistory_onclick(); break;
                case 'ThirdPartyInstall': if (blnAccess) this.cmdThirdPartyInstall_onclick(); break;
                case 'StaticVisits': if (blnAccess) this.cmdStaticVisits_onclick(); break;
                case 'VisitSummary': if (blnAccess) this.cmdVisitSummary_onclick(); break;
                case 'StateOfService': if (blnAccess) this.cmdStateOfService_onclick(); break;
                case 'EventHistory': this.cmdEventHistory_onclick(); break;
                case 'Contract': this.cmdContract_onclick(); break;
                case 'CustomerInformation': this.cmdCustomerInformation_onclick(); break;
            }
        }
    }

    public PremiseContactFax_ondeactivate(): void {
        this.parent.setControlValue('PosFax', this.parent.getControlValue('PremiseContactFax'));
    }

    public PremiseName_onchange(): void {
        if (this.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            if (this.parent.getControlValue('SCRunPAFSearchOnFirstAddressLine') === 'true') {
                this.cmdGetAddress_onclick();
            }
        }
    }

    public PremisePostcode_ondeactivate(): void {
        if (this.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.DefaultFromPostcode();
        }
    }

    public PremiseCommenceDate_ondeactivate(): void {
        if (this.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            if (this.parent.getControlValue('ErrorMessageDesc') === '') {
                this.riTab.TabFocus(1);
            } else {
                this.parent.setControlValue('ErrorMessageDesc', '');
            }
            this.parent.pgPM_AR1.riExchange_CBORequest();
        }
    }

    //************************************************************************************
    //* Menu Options (onclick actions)                                                   *
    //************************************************************************************
    // reset variable & disable buttons
    public cmdGetLatAndLong_onclick(): void {
        this.HaveResolved = true;
    }

    public cmdCustomerInformation_onclick(): void {
        this.parent.navigate('ServiceCover', InternalGridSearchServiceModuleRoutes.ICABSACUSTOMERINFORMATIONSUMMARY.URL_1, {
            parentMode: 'ServiceCover',
            contractNumber: this.parent.getControlValue('ContractNumber'),
            contractName: this.parent.getControlValue('ContractName'),
            accountNumber: this.parent.getControlValue('AccountNumber')
        }); //iCABSACustomerInformationSummary.htm
    }

    public tdCustomerInfo_onclick(): void {
        this.cmdCustomerInformation_onclick();
    }

    public cmdContactManagement_onclick(): void {
        this.parent.navigate('Premise', 'ccm/contact/search', {
            ContractNumber: this.parent.getControlValue('ContractNumber'),
            ContractName: this.parent.getControlValue('ContractName'),
            PremiseNumber: this.parent.getControlValue('PremiseNumber'),
            currentContractTypeURLParameter: this.pageParams.CurrentContractType,
            PremiseName: this.parent.getControlValue('PremiseName'),
            AccountNumber: this.parent.getControlValue('AccountNumber'),
            parentMode: 'Premise'
        });
    }

    public cmdHistory_onclick(): void {
        this.parent.navigate('Premise', InternalGridSearchSalesModuleRoutes.ICABSACONTRACTHISTORYGRID, {
            ContractNumber: this.parent.getControlValue('ContractNumber'),
            ContractName: this.parent.getControlValue('ContractName'),
            PremiseNumber: this.parent.getControlValue('PremiseNumber'),
            currentContractTypeURLParameter: this.pageParams.CurrentContractType,
            PremiseName: this.parent.getControlValue('PremiseName'),
            PremiseRowID: this.parent.getControlValue('Premise'),
            parentMode: 'Premise'
        });
    }

    public cmdServiceSummary_onclick(): void {
        if (this.pageParams.CurrentContractType === 'P') {
            this.parent.navigate('Premise', InternalGridSearchServiceModuleRoutes.ICABSAPRODUCTSALESSCENTRYGRID.URL_2);
        } else {
            this.parent.navigate('Premise', InternalGridSearchSalesModuleRoutes.ICABSAPREMISESERVICESUMMARY);
        }
    }

    public cmdAllocate_onclick(): void {
        this.parent.navigate('Premise-Allocate', '/application/premiseLocationAllocation', {
            'ServiceCoverRowID': this.parent.getControlValue('Premise'),
            'ContractTypeCode': this.pageParams.CurrentContractType,
            'ContractNumber': this.parent.getControlValue('ContractNumber'),
            'PremiseNumber': this.parent.getControlValue('PremiseNumber')
        });
    }

    public cmdServiceCover_onclick(): void {
        if (this.pageParams.CurrentContractType === 'P') {
            this.parent.navigate('Premise', InternalGridSearchServiceModuleRoutes.ICABSAPRODUCTSALESSCENTRYGRID.URL_1);
        } else {
            this.parent.navigate('Premise', 'Application/iCABSAServiceCoverSearch'); // Need to verify route
        }
    }

    public cmdInvoiceNarrative_onclick(): void {
        this.parent.navigate('Premise', InternalMaintenanceSalesModuleRoutes.ICABSAINVOICENARRATIVEMAINTENANCE);
    }

    public cmdInvoiceCharge_onclick(): void {
        this.parent.navigate('Premise', 'contractmanagement/account/invoiceCharge');
    }

    public cmdThirdPartyInstall_onclick(): void {
        this.parent.navigate('Premise', 'Application/iCABSARThirdPartyInstallationNoteReport');
    }

    public cmdGetAddress_onclick(): void {
        this.logger.log('cmdGetAddress_onclick', this.pageParams.vSCEnableHopewiserPAF, this.pageParams.vSCEnableDatabasePAF, this.riMaintenance.CurrentMode);

        if (this.pageParams.vSCEnableDatabasePAF) {
            this.parent.ellipsis.AUPostCodeSearch.childparams.postCode = this.parent.getControlValue('PremisePostcode');
            this.parent.ellipsis.AUPostCodeSearch.childparams.state = this.parent.getControlValue('PremiseAddressLine5');
            this.parent.ellipsis.AUPostCodeSearch.childparams.town = this.parent.getControlValue('PremiseAddressLine4');
            setTimeout(() => { this.parent.AUPostCodeSearch.openModal(); }, 200);
        }

        if (this.riMaintenance.CurrentMode === MntConst.eModeAdd) this.DefaultFromPostcode();
    }

    public cmdValue_onclick(): void {
        this.parent.navigate('Premise', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVALUEGRID);
    }

    public cmdProRata_onclick(): void {
        this.parent.navigate('Premise', InternalGridSearchSalesModuleRoutes.ICABSAPRORATACHARGESUMMARY);
    }

    public cmdInvoiceHistory_onclick(): void {
        this.parent.navigate('Premise', '/billtocash/contract/invoice');
    }

    public cmdStateOfService_onclick(): void {
        this.parent.navigate('SOS', 'Application/iCABSSeStateOfServiceNatAccountGrid');
    }

    public cmdStaticVisits_onclick(): void {
        this.parent.navigate('Premise', InternalGridSearchSalesModuleRoutes.ICABSASTATICVISITGRIDYEAR, {
            ContractNumber: this.parent.getControlValue('ContractNumber'),
            ContractName: this.parent.getControlValue('ContractName'),
            PremiseNumber: this.parent.getControlValue('PremiseNumber'),
            PremiseName: this.parent.getControlValue('PremiseName'),
            Premise: this.parent.getControlValue('Premise'),
            PremiseRowID: this.parent.getControlValue('Premise'),
            currentContractTypeURLParameter: this.pageParams.CurrentContractType
        });
    }

    public cmdVisitSummary_onclick(): void {
        this.parent.store.dispatch({
            type: ContractActionTypes.SAVE_DATA,
            payload: {
                'CurrentContractTypeURLParameter': this.pageParams.contractType,
                'ContractNumber': this.parent.getControlValue('ContractNumber'),
                'ContractName': this.parent.getControlValue('ContractName'),
                'PremiseNumber': this.parent.getControlValue('PremiseNumber'),
                'PremiseName': this.parent.getControlValue('PremiseName'),
                'ProductCode': this.parent.getControlValue('ProductCode'),
                'ProductDesc': this.parent.getControlValue('ProductDesc'),
                'PremiseRowID': this.parent.getControlValue('Premise'),
                'currentContractType': this.pageParams.CurrentContractType
            }
        });
        this.parent.navigate('ShowPremiseLevel', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVISITSUMMARY);
    }

    public cmdEventHistory_onclick(): void {
        this.parent.navigate('Premise', InternalGridSearchServiceModuleRoutes.ICABSCMCUSTOMERCONTACTHISTORYGRID);
    }

    public cmdContract_onclick(): void {
        this.parent.navigate('Premise', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
            ContractNumber: this.parent.getControlValue('ContractNumber'),
            ContractName: this.parent.getControlValue('ContractName'),
            currentContractTypeURLParameter: this.pageParams.CurrentContractType,
            parentMode: 'Premise'
        });
    }

    public DrivingChargeInd_onclick(): void {
        this.logger.log('DrivingChargeInd', this.riExchange.riInputElement.checked(this.uiForm, 'DrivingChargeInd'));
        if (this.riMaintenance.CurrentMode === MntConst.eModeAdd || this.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            this.uiDisplay.tdDrivingChargeValue = this.riExchange.riInputElement.checked(this.uiForm, 'DrivingChargeInd');
            this.uiDisplay.tdDrivingChargeValueLab = this.uiDisplay.tdDrivingChargeValue;
        }
    }

    //************************************************************************************
    //* LookUps (onkeydown actions)                                                      *
    //************************************************************************************
    public SalesAreaCode_onkeydown(obj: any): void {
        if (obj.keyCode === 34) {
            this.SalesAreaCodeSearch();
        }
    }

    private navigateToTransDataURL(htmlPage: string): void {
        let url = htmlPage
            + '?Business_Code=' + this.PBusinessCode
            + '&Contract_Number=' + this.parent.getControlValue('ContractNumber')
            + '&Premise_Number=' + this.parent.getControlValue('PremiseNumber');

        // go to premise access program
        this.parent.navigate('TRANS', url); //TODO

        // Force the page back to the first tab
        this.riTab.TabDraw();
    }

    // subprocedure to link to a Local development program
    public LinkToPremiseAccess(): void {
        this.navigateToTransDataURL('/wsscripts/TransPremAccessMaintiCABS.html');
    }

    public LinkToTechs(): void {
        this.navigateToTransDataURL('/wsscripts/TransPremPrefTechiCABS.html');
    }

    public LinkToDates(): void {
        this.navigateToTransDataURL('/wsscripts/TransAccessDateRest.html');
    }

    public LinkToTimes(): void {
        this.navigateToTransDataURL('/wsscripts/TransAccessTimeRest.html');
    }

    public SalesAreaCodeSearch(): void {
        this.parent.ellipsis.SalesAreaSearchComponent.childparams.parentMode = 'LookUp-Premise';
        this.parent.ellipsis.SalesAreaSearchComponent.childparams.ContractTypeCode = this.pageParams.CurrentContractType;
        this.parent.openModal('SalesAreaSearchComponent');
    }

    public PremiseAddressLine4_onfocusout(): void {
        if (this.pageParams.vSCAddressLine4Required && this.pageParams.vSCEnableValidatePostcodeSuburb) {
            this.cmdGetAddress_onclick();
        }
    }

    public PremiseAddressLine5_onfocusout(): void {
        if (this.pageParams.vSCAddressLine5Required && this.pageParams.vSCEnableValidatePostcodeSuburb) {
            this.cmdGetAddress_onclick();
        }
    }

    public PremisePostcode_onfocusout(): void {
        if (this.pageParams.vSCPostCodeRequired) {
            this.cmdGetAddress_onclick();
        }
        this.PremisePostcode_ondeactivate();
    }

    public PremisePostcode_onchange(): void {
        let ctrl = this.uiForm.controls;
        //TODO - Verify the if condition
        if (this.pageParams.SCEnablePostcodeDefaulting && this.pageParams.vSCEnableDatabasePAF && ctrl.PremisePostcode.value.trim() !== '') {
            this.riMaintenance.BusinessObject = 'iCABSGetPostCodeTownAndState.p';
            this.riMaintenance.clear();
            this.riMaintenance.PostDataAdd('Function', 'GetPostCodeTownAndState', MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('BusinessCode', this.riExchange.ClientSideValues.Fetch('BusinessCode'), MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('Postcode', ctrl.PremisePostcode.value, MntConst.eTypeCode);
            this.riMaintenance.PostDataAdd('State', ctrl.PremiseAddressLine5.value, MntConst.eTypeText);
            this.riMaintenance.PostDataAdd('Town', ctrl.PremiseAddressLine4.value, MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('UniqueRecordFound', MntConst.eTypeCheckBox);
            this.riMaintenance.ReturnDataAdd('Postcode', MntConst.eTypeCode);
            this.riMaintenance.ReturnDataAdd('State', MntConst.eTypeText);
            this.riMaintenance.ReturnDataAdd('Town', MntConst.eTypeText);
            this.riMaintenance.Execute(this, function (data: any): any {
                if (!data['UniqueRecordFound']) {
                    setTimeout(() => { this.parent.PostCodeSearch.openModal(); }, 200);
                } else {
                    this.parent.setControlValue('PremisePostcode', data['Postcode']);
                    this.parent.setControlValue('PremiseAddressLine5', data['State']);
                    this.parent.setControlValue('PremiseAddressLine4', data['Town']);
                }
            });
        }
    }

    //*************************************************************************************************************************
    //*************** S P E E D S C R I P T   L O G I C ************************************************************************
    public SetSCVariables(): void {
        //Functionality not required
        this.pageParams.SCEnableHopewiserPAF = this.pageParams.vSCEnableHopewiserPAF;
        this.pageParams.SCEnableDatabasePAF = this.pageParams.vSCEnableDatabasePAF;
        this.pageParams.SCAddressLine3Logical = this.pageParams.vSCAddressLine3Logical;
        this.pageParams.SCAddressLine4Required = this.pageParams.vSCAddressLine4Required;
        this.pageParams.SCAddressLine5Required = this.pageParams.vSCAddressLine5Required;
        this.pageParams.SCPostCodeRequired = this.pageParams.vSCPostCodeRequired;
        this.pageParams.SCPostCodeMustExistInPAF = this.pageParams.vSCPostCodeMustExistInPAF;
        this.pageParams.SCRunPAFSearchOnFirstAddressLine = this.pageParams.vSCRunPAFSearchOn1stAddressLine;
        this.pageParams.SCServiceReceiptRequired = this.pageParams.vSCServiceReceiptRequired;
    }

    public SetHTMLPageSettings(param: any): any {
        //Hide/Show fields and set variables depending on whether System Chars are required or not
        this.uiDisplay.labelDiscountCode = this.pageParams.vEnableDiscountCode;
        this.uiDisplay.DiscountCode = this.pageParams.vEnableDiscountCode;
        this.uiDisplay.DiscountDesc = this.pageParams.vEnableDiscountCode;
        this.uiDisplay.trPremiseAddressLine3 = this.pageParams.vEnableAddressLine3;
        this.uiDisplay.trPremiseDirectoryName = this.pageParams.vEnableMapGridReference;
        this.uiDisplay.trRetainServiceWeekday = this.pageParams.vEnableRetentionOfServiceWeekDay;
        this.uiDisplay.trServiceReceiptRequired = this.pageParams.vSCServiceReceiptRequired;

        this.parent.setControlValue('NationalAccountChecked', this.pageParams.vEnableNationalAccountWarning);
        this.parent.setControlValue('SCEnableDrivingCharges', this.pageParams.vSCEnableDrivingCharges);

        this.uiDisplay.tdDrivingChargeInd = (this.pageParams.vSCEnableDrivingCharges && this.pageParams.CurrentContractType !== 'P');
        this.uiDisplay.tdDrivingChargeIndLab = this.uiDisplay.tdDrivingChargeInd;

        this.logger.log('SetHTMLPageSettings Controls', this.pageParams);
    }

    //TODO - Verify below function & CheckPostcode()
    public PostcodeDefaultingEnabled(param: any): any {
        if (this.pageParams.vEnablePostcodeDefaulting) {  /* Only do the following if (Postcode Defaulting is enabled */
            if (this.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                if (this.riExchange.riInputElement.HasChanged(this.uiForm, 'ContractNumber')
                    || this.parent.getControlValue('PremiseName') === '') {
                    if (this.parent.getControlValue('PremisePostcode') !== '' && this.pageParams.CurrentContractType === 'C') {
                        this.CheckPostcode();
                    }
                }
                //Again if (the new postcode already exists in iCABS) { this may be a renegotiated premises, if (so this.Reneg Screen
                if (this.pageParams.CurrentContractType === 'C' && this.riExchange.riInputElement.HasChanged(this.uiForm, 'PremisePostcode')) {
                    this.CheckPostcode();
                }
            }
        }
    }
}
