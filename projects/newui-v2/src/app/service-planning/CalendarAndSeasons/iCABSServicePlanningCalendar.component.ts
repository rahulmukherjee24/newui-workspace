import { Component, OnInit, OnDestroy, Injector, ViewChild, AfterViewInit, HostListener } from '@angular/core';
import { QueryParams } from '@shared/services/http-params-wrapper';

import { BaseComponent } from '@base/BaseComponent';
import { PageIdentifier } from '@base/PageIdentifier';
import { InternalGridSearchServiceModuleRoutes, InternalMaintenanceServiceModuleRoutes, ServiceDeliveryModuleRoutes, AppModuleRoutes } from '@base/PageRoutes';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { ServiceCoverSearchComponent } from '@internal/search/iCABSAServiceCoverSearch';
import { BranchServiceAreaSearchComponent } from '@internal/search/iCABSBBranchServiceAreaSearch';
import { ContractSearchComponent } from '@internal/search/iCABSAContractSearch';
import { PremiseSearchComponent } from '@internal/search/iCABSAPremiseSearch';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { PaginationComponent } from '@shared/components/pagination/pagination';

@Component({
    templateUrl: 'iCABSServicePlanningCalendar.html'
})

export class ServicePlanningCalendarComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    private xhr: Object = {
        module: 'planning',
        method: 'service-planning/maintenance',
        operation: 'Service/iCABSServicePlanningCalendar'
    };
    public pageId: string = '';
    public legends: Array<any> = [
        { key: 'SP_COLOUR_UP_TO_DATE', label: 'Up To Date', color: '#FFFFFF' },
        { key: 'SP_COLOUR_UNPLANNED', label: 'Unplanned (Late)', color: '#FFCCCC' },
        { key: 'SP_COLOUR_PLANNED_NOT_COMPLETED', label: 'Planned', color: '#ccffcc' },
        { key: 'SP_COLOUR_INPLANNING_NOT_CURRENT', label: 'In Planning', color: '#87CEFA' },
        { key: 'SP_COLOUR_VISITED', label: 'Visited', color: '#66CCCC' }
    ];
    public itemsPerPage: number = 10;
    public totalRecords: number = 1;
    public controls: Array<any> = [
        { name: 'BranchNumberName', disabled: true, required: true, type: MntConst.eTypeText }, // BranchNumber and BranchName
        { name: 'BranchServiceAreaCode', required: true, type: MntConst.eTypeTextFree, commonValidator: true },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'StartDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'EndDate', required: true, type: MntConst.eTypeDate },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseNumber', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'PremiseDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ProductCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'DisplayLines', required: true, value: '250', type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'ProductSaleGroupCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ProductSaleGroupDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ShowDetails' },
        // not in html
        { name: 'BusinessCode', type: MntConst.eTypeCode },
        { name: 'WeekNumber' },
        { name: 'PlannedVisitDuration' },
        { name: 'FirstWeekDay' },
        { name: 'EmployeeCode' },
        { name: 'BranchNumber', disabled: true, type: MntConst.eTypeCode }
    ];

    public ellipsismodalConfig: {
        backdrop: 'static',
        keyboard: true
    };
    public branchServiceAreaEllipsis: any = {
        component: BranchServiceAreaSearchComponent,
        inputParams: {
            'parentMode': 'LookUp-Emp'
        },
        dataReceived: ((data: any) => {
            if (data) {
                this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
                this.setControlValue('EmployeeSurname', data.EmployeeSurname);
            }
        })
    };
    public contractSearchEllipsisConfig: any = {
        component: ContractSearchComponent,
        inputParams: {
            'parentMode': 'LookUp-All'
        },
        dataReceived: ((data: any) => {
            if (data) {
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('ContractDesc', data.ContractName);
                this.clearFeilds('ContractNumber');
                this.premiseSearchEllipsisConfig['inputParams']['ContractNumber'] = data.ContractNumber;
                this.premiseSearchEllipsisConfig['inputParams']['ContractName'] = data.ContractName;
                this.serviceCoverEllipsis['inputParams']['ContractNumber'] = data.ContractNumber;
                this.serviceCoverEllipsis['inputParams']['ContractName'] = data.ContractName;
            }
        })
    };
    public premiseSearchEllipsisConfig: any = {
        component: PremiseSearchComponent,
        inputParams: {
            'parentMode': 'LookUp'
        },
        dataReceived: ((data: any) => {
            if (data) {
                this.setControlValue('PremiseNumber', data.PremiseNumber);
                this.setControlValue('PremiseDesc', data.PremiseName);
                this.clearFeilds('PremiseNumber');
                this.serviceCoverEllipsis['inputParams']['PremiseNumber'] = data.PremiseNumber;
                this.serviceCoverEllipsis['inputParams']['PremiseName'] = data.PremiseName;
            }
        })
    };
    public serviceCoverEllipsis: any = {
        component: ServiceCoverSearchComponent,
        inputParams: {
            'parentMode': 'LookUpBasic'
        },
        dataReceived: ((data) => {
            if (data) {
                this.setControlValue('ProductCode', data.ProductCode);
                this.setControlValue('ProductDesc', data.ProductDesc);
            }
        })
    };
    public productSalesGroupSearchConfig: any = {
        ellipsisTitle: 'Product Sales Group Search',
        configParams: {
            table: '',
            shouldShowAdd: false,
            parentMode: 'LookUp'
        },
        httpConfig: {
            operation: 'Business/iCABSBProductSalesGroupSearch',
            module: 'contract-admin',
            method: 'contract-management/search'
        },
        tableColumn: [
            { title: 'Code', name: 'ProductSaleGroupCode', type: MntConst.eTypeCode, size: 10 },
            { title: 'Description', name: 'ProductSaleGroupDesc', type: MntConst.eTypeText, required: 'Required', size: 40 }
        ],
        dataReceived: ((data: any) => {
            if (data) {
                this.setControlValue('ProductSaleGroupCode', data.ProductSaleGroupCode);
                this.setControlValue('ProductSaleGroupDesc', data.ProductSaleGroupDesc);
            }
        })
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSERVICEPLANNINGCALENDAR;
        this.browserTitle = 'Service Planning - Calendar';
        this.pageTitle = 'Service Planning';
    }
    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterViewInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.buildGrid();
        } else {
            this.setControlValue('BranchNumber', this.utils.getBranchCode());
            this.setControlValue('BranchNumberName', this.utils.getBranchText());
            if (this.parentMode.toString() === 'ServicePlanning') {
                this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
                this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname'));
                this.setControlValue('StartDate', this.riExchange.getParentHTMLValue('StartDate'));
                this.setControlValue('EndDate', this.riExchange.getParentHTMLValue('EndDate'));
                this.setControlValue('WeekNumber', this.riExchange.getParentHTMLValue('WeekNumber'));
                this.setControlValue('PlannedVisitDuration', this.riExchange.getParentHTMLValue('PlannedVisitDuration'));
                this.setControlValue('FirstWeekDay', this.riExchange.getParentHTMLValue('FirstWeekDay'));
            }
            this.pageParams.gridCurrentPage = 1;
        }
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.FunctionUpdateSupport = true;
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // Listener for keydown events
    @HostListener('document:keydown', ['$event']) document_onkeydown(event: any): void {
        switch (event.keyCode) {
            // GoTo Current WeekEndingDate
            case 106: // * in num pad
                let currentDate: Date = new Date();
                let startDate: Date = new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 8));
                this.setControlValue('StartDate', this.globalize.parseDateToFixedFormat(startDate) as string);
                let endDate: Date = new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 7));
                this.setControlValue('EndDate', this.globalize.parseDateToFixedFormat(endDate) as string);
                this.riGrid.RefreshRequired();
                break;
            case 107: // + in num pad
                this.setControlValue('StartDate', this.utils.addDays(this.getControlValue('StartDate'), 7));
                this.setControlValue('EndDate', this.utils.addDays(this.getControlValue('EndDate'), 7));
                this.riGrid.RefreshRequired();
                break;
            // - 7 Days
            case 109: // - in num pad -
                this.setControlValue('StartDate', this.utils.addDays(this.getControlValue('StartDate'), -7));
                this.setControlValue('EndDate', this.utils.addDays(this.getControlValue('EndDate'), -7));
                this.riGrid.RefreshRequired();
                break;
        }
    }

    //Grid
    public buildGrid(): void {
        let dateDiff: any = '';
        this.riGrid.Clear();
        this.riGrid.AddColumn('EmployeeCode', 'PlanVisit', 'EmployeeCode', MntConst.eTypeText, 6);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
        dateDiff = this.utils.dateDiffInDays(this.getControlValue('StartDate'), this.getControlValue('EndDate')) + 1;
        for (let i = 0; i < dateDiff; i++) {
            this.riGrid.AddColumn('ServiceDate' + i, 'Planning', 'ServiceDate' + i, MntConst.eTypeText, 40);
            this.riGrid.AddColumnAlign('ServiceDate' + i, MntConst.eAlignmentCenter);
        }
        this.riGrid.Complete();
        this.riGridBeforeExecute();
    }
    public riGridBeforeExecute(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.LanguageCode, this.riExchange.LanguageCode());
        search.set(this.serviceConstants.BranchNumber, this.utils.getBranchCode());
        search.set(this.serviceConstants.BranchServiceAreaCode, this.getControlValue('BranchServiceAreaCode'));
        search.set('ProductSaleGroupCode', this.getControlValue('ProductSaleGroupCode'));
        search.set('DateFrom', this.getControlValue('StartDate'));
        search.set('DateTo', this.getControlValue('EndDate'));
        search.set('ShowDetails', this.getControlValue('ShowDetails') ? 'True' : 'False');
        search.set(this.serviceConstants.ContractNumber, this.getControlValue('ContractNumber'));
        search.set(this.serviceConstants.PremiseNumber, this.getControlValue('PremiseNumber'));
        search.set(this.serviceConstants.ProductCode, this.getControlValue('ProductCode'));
        search.set('DisplayLines', this.getControlValue('DisplayLines'));
        search.set(this.serviceConstants.GridCacheRefresh, true);
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.PageSize, this.itemsPerPage.toString());
        search.set(this.serviceConstants.PageCurrent, this.pageParams.gridCurrentPage);
        search.set(this.serviceConstants.GridHandle, this.utils.gridHandle);
        search.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);

        search.set('riSortOrder', this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhr['method'], this.xhr['module'], this.xhr['operation'], search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.itemsPerPage : 1;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.Execute(data);
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 100);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public riGridAfterExecute(): void {
        if (this.riGrid.Details.GetAttribute('EmployeeCode', 'additionalproperty')) {
            let additionalData: Array<any>;
            additionalData = this.riGrid.Details.GetAttribute('EmployeeCode', 'additionalproperty').split('|');
            if (this.legends.length > 0) this.legends.length = 0;
            this.legends.push({ key: 'SP_COLOUR_UP_TO_DATE', label: 'Up To Date', color: '#FFFFFF' });
            this.legends.push({ key: 'SP_COLOUR_UNPLANNED', label: 'Unplanned (Late)', color: '#FFCCCC' });
            if (additionalData[2] === 'Yes') {
                this.legends.push({ key: 'SP_COLOUR_CONFIRMED', label: 'Appointment Confirmed', color: '#f4a433' });
            }
            this.legends.push({ key: 'SP_COLOUR_PLANNED_NOT_COMPLETED', label: 'Planned', color: '#ccffcc' });
            this.legends.push({ key: 'SP_COLOUR_INPLANNING_NOT_CURRENT', label: 'In Planning', color: '#87CEFA' });
            this.legends.push({ key: 'SP_COLOUR_VISITED', label: 'Visited', color: '#66CCCC' });
            if (additionalData[3] === 'Yes') {
                this.legends.push({ key: 'SP_COLOUR_CALLOUT', label: 'Callout', color: '#FFCCCC' });
            }
            if (additionalData[4] === 'Yes') {
                this.legends.push({ key: 'SP_COLOUR_HIGHLIGHT', label: 'Highlight', color: '#FFFF33' });
            }
            if (additionalData[5] === 'Yes') {
                this.legends.push({ key: 'SP_COLOUR_CONFIRMED', label: 'Filter Results', color: '#000099' });
            }
            if (additionalData[6] === 'Yes') {
                this.legends.push({ key: 'SP_COLOUR_DIARY', label: 'Diary Clash', color: 'red' });
            }
        }
    }
    //Grid Refresh
    public btnRefresh(): void {
        if (this.uiForm.valid) {
            if (this.getControlValue('GridPageSize')) {
                this.itemsPerPage = this.getControlValue('GridPageSize');
            }
            this.riGrid.Mode = MntConst.eModeNormal;
            this.riGrid.RefreshRequired();
            this.buildGrid();
        }
    }
    // pagination current page
    public getCurrentPage(currentPage: any): void {
        if (this.pageParams.gridCurrentPage !== currentPage.value) {
            this.pageParams.gridCurrentPage = currentPage.value;
            this.riGrid.RefreshRequired();
            this.riGridBeforeExecute();
        }
    }

    public onDblClick(event: any): void {
        if (this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'additionalproperty')) {
            let additionalInfo: any;
            additionalInfo = this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'additionalproperty').split('|');
            if (additionalInfo[1] !== 'x') {
                if (additionalInfo[1] === 'e') {
                    this.setControlValue('EmployeeCode', additionalInfo[0]);
                    this.setAttribute('EmployeeRowID', additionalInfo[0]);
                    this.navigate('ServiceVisitCalendar', InternalGridSearchServiceModuleRoutes.ICABSADIARYYEARGRID);

                } else {
                    let search: QueryParams = this.getURLSearchParamObject(),
                        formData: any = {};
                    search.set(this.serviceConstants.Action, '6');
                    search.set('BranchNumber', this.utils.getBranchCode());
                    search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
                    search.set('PlanVisitROWID', additionalInfo[0]);
                    formData[this.serviceConstants.Function] = 'ValidateVisit';
                    this.ajaxSource.next(this.ajaxconstant.START);
                    this.httpService.makePostRequest(this.xhr['method'], this.xhr['module'], this.xhr['operation'], search, formData)
                        .subscribe(
                            (data) => {
                                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                                if (data.ErrorMessageDesc && data.ErrorMessageDesc !== '.') {
                                    this.modalAdvService.emitError(new ICabsModalVO(data.ErrorMessageDesc));
                                } else {
                                    this.navigate('Calendar', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEPLANNINGDETAILHG,
                                        {
                                            PlanVisitRowID: additionalInfo[0],
                                            WeekNumber: this.getControlValue('WeekNumber'),
                                            EndDate: this.getControlValue('EndDate')
                                        }
                                    );
                                }
                            },
                            (error) => {
                                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                            });
                }
            }
        }
    }

    public workListClick(): void {
        this.navigate('ServicePlanning', AppModuleRoutes.SERVICEDELIVERY + ServiceDeliveryModuleRoutes.ICABSWORKLISTCONFIRM, {
            'BranchNumber': this.getControlValue('BranchNumberName'),
            'DateFrom': this.getControlValue('StartDate'),
            'DateTo': this.getControlValue('EndDate')
        });
    }
    public defaultRoutinesClick(): void {
        let search: QueryParams = this.getURLSearchParamObject(),
            formData: any = {};
        search.set(this.serviceConstants.Action, '6');
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('StartDate', this.getControlValue('StartDate'));
        search.set('UpToDate', this.getControlValue('EndDate'));

        formData[this.serviceConstants.ActionType] = 'DefaultRoutines';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhr['method'], this.xhr['module'], this.xhr['operation'], search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.ErrorMessageDesc) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.ErrorMessageDesc));
                        this.btnRefresh();
                    }
                    if (data.errorMessage) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage));
                        this.btnRefresh();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }
    public branchServiceAreaFetch(): void {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let search: QueryParams = this.getURLSearchParamObject(),
                formData: any = {};
            search.set(this.serviceConstants.Action, '6');
            search.set('BranchNumber', this.utils.getBranchCode());
            search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
            formData[this.serviceConstants.Function] = 'GetDescriptions';
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.xhr['method'], this.xhr['module'], this.xhr['operation'], search, formData)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data) {
                            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        } else {
            this.setControlValue('EmployeeSurname', '');
        }
    }
    public productSaleGroupFetch(): void {
        if (this.getControlValue('ProductSaleGroupCode')) {
            let search: QueryParams = this.getURLSearchParamObject(),
                formData: any = {};
            search.set(this.serviceConstants.Action, '6');
            search.set('BranchNumber', this.utils.getBranchCode());
            search.set('ProductSaleGroupCode', this.getControlValue('ProductSaleGroupCode'));
            formData[this.serviceConstants.Function] = 'GetDescriptions';
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.xhr['method'], this.xhr['module'], this.xhr['operation'], search, formData)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data) {
                            this.setControlValue('ProductSaleGroupDesc', data.ProductSaleGroupDesc);
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        } else {
            this.setControlValue('ProductSaleGroupDesc', '');
        }
    }
    public clearFeilds(data: string): void {
        switch (data) {
            case 'ContractNumber':
                this.setControlValue('PremiseNumber', '');
                this.setControlValue('PremiseDesc', '');
                this.setControlValue('ProductCode', '');
                this.setControlValue('ProductDesc', '');
                break;
            case 'PremiseNumber':
                this.setControlValue('ProductCode', '');
                this.setControlValue('ProductDesc', '');
                break;
        }
    }
    public fillFeilds(data: string): void {
        switch (data) {
            case 'ContractNumber':
                if (this.getControlValue('ContractNumber') === '') {
                    this.setControlValue('ContractDesc', '');
                }
                this.premiseSearchEllipsisConfig['inputParams']['ContractNumber'] = this.getControlValue('ContractNumber');
                this.serviceCoverEllipsis['inputParams']['ContractNumber'] = this.getControlValue('ContractNumber');
                break;
            case 'PremiseNumber':
                if (this.getControlValue('PremiseNumber') === '') {
                    this.setControlValue('PremiseDesc', '');
                }
                this.serviceCoverEllipsis['inputParams']['PremiseNumber'] = this.getControlValue('PremiseNumber');
                break;
        }
    }
}
