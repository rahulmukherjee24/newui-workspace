import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from './../../../shared/services/riMaintenancehelper';

@Component({
    templateUrl: 'iCABSSeServicePlanningSplitServiceMaintenanceHg.html'
})

export class ServicePlanningSplitServiceMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private queryParams: any = {
        operation: 'Service/iCABSSeServicePlanningSplitServiceMaintenanceHg',
        module: 'planning',
        method: 'service-planning/maintenance'
    };
    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, required: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, required: true },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, required: true },
        { name: 'ProductDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'PlanVisitNumber', type: MntConst.eTypeInteger, required: true },
        { name: 'VisitTypeCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'PlanVisitSystemDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'OriginalVisitDueDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'PlannedVisitDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'SplitToDate', type: MntConst.eTypeDate, required: true },
        { name: 'OriginalVisitDuration', type: MntConst.eTypeText, disabled: true },
        { name: 'PlannedVisitDuration', type: MntConst.eTypeText, disabled: true },
        { name: 'RemainingDuration', type: MntConst.eTypeText, required: true },
        { name: 'ServicedDuration', type: MntConst.eTypeText, disabled: true },
        { name: 'PlanVisitStatusCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'ServiceCoverNumber', type: MntConst.eTypeInteger, required: true },
        { name: 'KeyedStartTime', type: MntConst.eTypeTime },
        { name: 'KeyedEndTime', type: MntConst.eTypeTime },
        { name: 'IncludeKeyedServiceTimeInd', type: MntConst.eTypeCheckBox },
        { name: 'ServiceCoverRowID', type: MntConst.eTypeText },
        { name: 'PlanVisitRowID' }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNINGSPLITSERVICEMAINTENANCEHG;
        this.browserTitle = this.pageTitle = 'Split Service Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.getvbTimeSeparator();
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // lookup to get time seperator
    private getvbTimeSeparator(): void {
        let lookupIP: Array<any>;
        lookupIP = [{
            'table': 'riCountry',
            'query': {
                'BusinessCode': this.businessCode(),
                'riCountryCode': this.countryCode()
            },
            'fields': ['riTimeSeparator']
        }];
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupIP).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    if (data && data[0].length > 0) {
                        if (data[0][0])
                            this.pageParams.vbTimeSeparator = data[0][0].riTimeSeparator;
                    } else
                        this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                }
            }).catch(error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private windowOnLoad(): void {
        switch (this.parentMode) {
            case 'ServicePlanning':
                this.disableControl('ContractNumber', true);
                this.disableControl('PremiseNumber', true);
                this.disableControl('ProductCode', true);
                this.disableControl('ServiceCoverNumber', true);
                this.disableControl('PlanVisitNumber', true);
                this.disableControl('PlannedVisitDate', true);
                this.riExchange.getParentHTMLValue('ContractNumber');
                this.riExchange.getParentHTMLValue('PremiseNumber');
                this.riExchange.getParentHTMLValue('ProductCode');
                this.riExchange.getParentHTMLValue('ServiceCoverNumber');
                this.riExchange.getParentHTMLValue('PlanVisitNumber');
                this.riExchange.getParentHTMLValue('PlanVisitRowID');
                this.beforeFetch();
                break;
            case 'WorklistEntry':
                this.disableControl('ContractNumber', true);
                this.disableControl('PremiseNumber', true);
                this.disableControl('ProductCode', true);
                this.disableControl('ServiceCoverNumber', true);
                this.disableControl('PlanVisitNumber', true);
                this.disableControl('PlannedVisitDate', true);
                this.riExchange.getParentHTMLValue('ContractNumber');
                this.riExchange.getParentHTMLValue('PremiseNumber');
                this.riExchange.getParentHTMLValue('ProductCode');
                this.riExchange.getParentHTMLValue('ServiceCoverNumber');
                this.riExchange.getParentHTMLValue('PlanVisitNumber');
                this.riExchange.getParentHTMLValue('PlanVisitRowID');
                this.riExchange.getParentHTMLValue('IncludeKeyedServiceTimeInd');
                this.riExchange.getParentHTMLValue('KeyedStartTime');
                this.riExchange.getParentHTMLValue('KeyedEndTime');
                this.beforeFetch();
                break;
        }
    }

    // fetch function to populate data
    private beforeFetch(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.ContractNumber, this.getControlValue('ContractNumber'));
        search.set(this.serviceConstants.PremiseNumber, this.getControlValue('PremiseNumber'));
        search.set(this.serviceConstants.ProductCode, this.getControlValue('ProductCode'));
        search.set('ServiceCoverNumber', this.getControlValue('ServiceCoverNumber'));
        search.set('PlanVisitROWID', this.getControlValue('PlanVisitRowID'));
        search.set('PlanVisitNumber', this.getControlValue('PlanVisitNumber'));
        search.set('KeyedStartTime', this.getControlValue('KeyedStartTime'));
        search.set('KeyedEndTime', this.getControlValue('KeyedEndTime'));
        search.set('IncludeKeyedServiceTimeInd', this.getControlValue('IncludeKeyedServiceTimeInd'));
        search.set(this.serviceConstants.Action, '0');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.uiForm.patchValue(data);
                    this.lookUpdata();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //lookup for primary fields
    private lookUpdata(): void {
        let lookupIP: Array<any>;
        lookupIP = [{
            'table': 'Contract',
            'query': {
                'BusinessCode': this.businessCode(),
                'ContractNumber': this.getControlValue('ContractNumber')
            },
            'fields': ['ContractName']
        },
        {
            'table': 'Premise',
            'query': {
                'BusinessCode': this.businessCode(),
                'ContractNumber': this.getControlValue('ContractNumber'),
                'PremiseNumber': this.getControlValue('PremiseNumber')
            },
            'fields': ['PremiseName']
        },
        {
            'table': 'Product',
            'query': {
                'BusinessCode': this.businessCode(),
                'ProductCode': this.getControlValue('ProductCode')
            },
            'fields': ['ProductDesc']
        },
        {
            'table': 'PlanVisitStatus ',
            'query': {
                'PlanVisitStatusCode': this.getControlValue('PlanVisitStatusCode')
            },
            'fields': ['PlanVisitSystemDesc']
        }];
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupIP).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    if (data && data.length > 0) {
                        if (data[0][0])
                            this.setControlValue('ContractName', data[0][0].ContractName || '');
                        if (data[1][0])
                            this.setControlValue('PremiseName', data[1][0].PremiseName || '');
                        if (data[2][0])
                            this.setControlValue('ProductDesc', data[2][0].ProductDesc || '');
                        if (data[3][0])
                            this.setControlValue('PlanVisitSystemDesc', data[3][0].PlanVisitSystemDesc || '');
                        this.afterFetch();
                    } else
                        this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                }
            }).catch(error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //post fetch
    private afterFetch(): void {
        if (this.getControlValue('ServicedDuration') !== '')
            this.pageParams.isServicedDuration = true;
        else
            this.pageParams.isServicedDuration = false;
    }

    //post save navigate back
    private afterSave(): void {
        if (this.parentMode === 'ServicePlanning' || this.parentMode === 'WorklistEntry')
            this.location.back();
    }

    //save post call
    private promptConfirm(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        formData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        formData[this.serviceConstants.ProductCode] = this.getControlValue('ProductCode');
        formData['ServiceCoverNumber'] = this.getControlValue('ServiceCoverNumber');
        formData['PlanVisitNumber'] = this.getControlValue('PlanVisitNumber');
        formData['KeyedStartTime'] = this.getControlValue('KeyedStartTime');
        formData['KeyedEndTime'] = this.getControlValue('KeyedEndTime');
        formData['RemainingDuration'] = this.getControlValue('RemainingDuration');
        formData['ServicedDuration'] = this.getControlValue('ServicedDuration');
        formData['OriginalVisitDuration'] = this.getControlValue('OriginalVisitDuration');
        formData['PlannedVisitDuration'] = this.getControlValue('PlannedVisitDuration');
        formData['SplitToDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('SplitToDate'));
        formData['OriginalVisitDueDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('OriginalVisitDueDate'));
        formData['PlannedVisitDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('PlannedVisitDate'));
        formData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
        formData['PlanVisitStatusCode'] = this.getControlValue('PlanVisitStatusCode');
        formData['IncludeKeyedServiceTimeInd'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('IncludeKeyedServiceTimeInd'));
        formData['PlanVisitROWID'] = this.getControlValue('PlanVisitRowID');
        formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.formPristine();
                    this.afterSave();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //Remaining duration change event
    public remainingDurationOnchange(): void {
        let isvbError: boolean = false;
        let vbTimeFormat, vbRemainingDurationStr, remainingDuration, vbMRemainingDurationStr, modifiedStr: string;
        let vbRemainingDuration, vbRemainingDurationlen, vbDurationHours, vbDurationMinutes, strIndex: number;
        let reg = new RegExp('^[\\d\\s' + this.pageParams.vbTimeSeparator + ']+$');
        vbRemainingDurationStr = this.getControlValue('RemainingDuration');
        if (reg.test(vbRemainingDurationStr)) {
            vbTimeFormat = '00' + this.pageParams.vbTimeSeparator + '##';
            modifiedStr = vbRemainingDurationStr;
            do {
                modifiedStr = modifiedStr.replace(this.pageParams.vbTimeSeparator, '');
                strIndex = modifiedStr.indexOf(this.pageParams.vbTimeSeparator);
            } while (strIndex !== -1);
            vbMRemainingDurationStr = modifiedStr;
            if (!isNaN(vbMRemainingDurationStr)) {
                vbRemainingDuration = parseInt(vbMRemainingDurationStr, 10);
                vbRemainingDurationlen = vbMRemainingDurationStr.length;
                if (isNaN(vbRemainingDuration))
                    isvbError = true;
                if (!isvbError && ((vbRemainingDurationlen < 4) || (vbRemainingDurationlen > 7)))
                    isvbError = true;
                else if (!isvbError) {
                    let rDurationMin: number = Math.floor(vbRemainingDuration / 100);
                    let rDurationSec: number = Math.floor(vbRemainingDuration % 100);
                    let rDurationMinDis: string = rDurationMin.toString();
                    let rDurationSecDis: string = rDurationSec.toString();
                    if (rDurationSec >= 0)
                        rDurationSecDis = rDurationSec > 9 ? rDurationSecDis : '0' + rDurationSecDis;
                    if (rDurationMin >= 0)
                        rDurationMinDis = rDurationMin > 9 ? rDurationMinDis : '0' + rDurationMinDis;
                    this.setControlValue('RemainingDuration', rDurationMinDis + this.pageParams.vbTimeSeparator + rDurationSecDis);
                    if (!isvbError && (vbRemainingDuration === 0))
                        isvbError = true;
                    if (!isvbError) {
                        vbDurationHours = vbRemainingDuration / 100;
                        vbDurationMinutes = vbRemainingDuration % 100;
                        if (vbDurationMinutes > 59) {
                            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.vbDurationMinutes));
                            isvbError = true;
                        }
                    }
                }
            } else
                this.setControlValue('RemainingDuration', '');
            if (isvbError)
                this.setControlValue('RemainingDuration', '');
        } else
            this.setControlValue('RemainingDuration', '');
    }

    //split date change event
    public onSplitDateChange(data: any): void {
        this.uiForm.controls['SplitToDate'].markAsDirty();
    }

    public onCancel(): void {
        if (this.parentMode === 'ServicePlanning' || this.parentMode === 'WorklistEntry') {
            this.formPristine();
            this.location.back();
        }
    }

    public onSave(): void {
        if (this.riExchange.validateForm(this.uiForm))
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.promptConfirm.bind(this)));
    }

}
