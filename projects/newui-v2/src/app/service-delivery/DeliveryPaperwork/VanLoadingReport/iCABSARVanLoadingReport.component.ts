import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, Injector, AfterContentInit, ViewChild, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import { PageIdentifier } from '../../../base/PageIdentifier';
import { BaseComponent } from '../../../../app/base/BaseComponent';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { DatepickerComponent } from './../../../../shared/components/datepicker/datepicker';
import { BranchServiceAreaSearchComponent } from './../../../internal/search/iCABSBBranchServiceAreaSearch';

@Component({
    templateUrl: 'iCABSARVanLoadingReport.html'
})

export class VanLoadingReportComponent extends BaseComponent implements AfterContentInit, OnInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('fromDate') fromDatePicker: DatepickerComponent;
    @ViewChild('toDate') toDatePicker: DatepickerComponent;
    private lookUpSubscription: Subscription;
    private muleConfig: Object = {
        method: 'service-delivery/grid',
        module: 'vehicle',
        operation: 'ApplicationReport/iCABSARVanLoadingReport'
    };
    public pageId: string = '';
    public reportMessage: string = '';
    public fromDateValue: Date = new Date();
    public toDateValue: Date = new Date();
    public controls: Array<any> = [
        { name: 'ServiceBranchNumberDesc', disabled: true, type: MntConst.eTypeTextFree },
        { name: 'ServicePlanStatusCode', disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaCode', disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', disabled: true, required: false, type: MntConst.eTypeTextFree },
        { name: 'FromDate', disabled: false, required: true, type: MntConst.eTypeDate },
        { name: 'ToDate', disabled: false, required: true, type: MntConst.eTypeDate },
        { name: 'fromDateValue', disabled: false },
        { name: 'toDateValue', disabled: false }
    ];
    public modalConfig: Object = {
        backdrop: 'static',
        keyboard: true
    };
    public branchServiceAreaSearchComponent = BranchServiceAreaSearchComponent;
    public inputParams: any = {
        'parentMode': 'LookUp',
        'ServiceBranchNumber': this.utils.getBranchText().split(' - ')[0],
        'BranchName': this.utils.getBranchText().split(' - ')[1]
    };
    public servicePlanStatus: any = {
        isDisabled: false,
        isRequired: true,
        isTriggerValidate: false,
        active: { id: 'P', text: '' },
        displayFields: ['ServicePlanStatusCode', 'ServicePlanStatusDesc'],
        receivedDropDownData: ((e) => {
            this.setControlValue('ServicePlanStatusCode', e.ServicePlanStatusCode);
            this.uiForm.controls['ServicePlanStatusCode'].markAsDirty();
        }),
        onError: ((e) => {
            this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
        })
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARVANLOADINGREPORT;
        this.browserTitle = this.pageTitle = 'Van Loading Report';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.lookUpSubscription) {
            this.lookUpSubscription.unsubscribe();
        }
    }

    ngAfterContentInit(): void {
        this.setControlValue('ServiceBranchNumberDesc', this.utils.getBranchText());
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.LanguageCode, this.riExchange.LanguageCode());
        this.servicePlanStatus['inputParams'] = {
            method: 'service-planning/search',
            module: 'planning',
            operation: 'System/iCABSSServicePlanStatusLanguageSearch',
            search: search
        };
    }

    /* Not Ideal but defaulted ServicePlanStatus To 'P' - Confirmed */
    public servicePlanStatusData(): void {
        let lookupIP: Array<any> = [{
            'table': 'ServicePlanStatusLang',
            'query': {
                'LanguageCode': this.riExchange.LanguageCode(),
                'ServicePlanStatusCode': 'P'
            },
            'fields': ['ServicePlanStatusDesc']
        }];
        this.servicePlanStatus.isTriggerValidate = true;
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data[0].length > 0) {
                this.servicePlanStatus.active = {
                    id: 'P',
                    text: 'P' + ' - ' + data[0][0].ServicePlanStatusDesc
                };
                this.setControlValue('ServicePlanStatusCode', this.servicePlanStatus.active.id);
                this.formPristine();
            }
        });
    }
    public datePickerSelectedValue(data: any, handle: string): void {
        switch (handle) {
            case 'FromDate':
                this.setControlValue('FromDate', data.value);
                this.fromDatePicker.validateDateField();
                this.uiForm.controls['FromDate'].markAsDirty();
                break;
            case 'ToDate':
                this.setControlValue('ToDate', data.value);
                this.toDatePicker.validateDateField();
                this.uiForm.controls['ToDate'].markAsDirty();
                break;
        }
    }
    // Submit Report Generation
    public submitReportGeneration(): void {
        if (this.uiForm.valid) {
            let date: Date = new Date(),
                time: any = this.globalize.parseTimeToFixedFormat(date.getHours() + ':' + date.getMinutes()),
                queryPost: QueryParams = this.getURLSearchParamObject(),
                dateString = this.globalize.parseDateToFixedFormat(date) as String;
            queryPost.set(this.serviceConstants.Action, '2');
            queryPost.set('Description', 'Van Loading Report');
            queryPost.set('ProgramName', 'iCABSVanLoadingReportGeneration.p');
            queryPost.set('StartDate', dateString.toString());
            queryPost.set('StartTime', time.toString());
            queryPost.set('Report', 'report');
            queryPost.set('ParameterName', ['BusinessCode', 'ServiceBranchNumber', 'BranchServiceAreaCode', 'ServicePlanStatusCode', 'FromDate', 'ToDate'].join(this.serviceConstants.PipeDelimeter));
            queryPost.set('ParameterValue', [this.utils.getBusinessCode(), this.utils.getBranchCode(), this.getControlValue('BranchServiceAreaCode'), this.getControlValue('ServicePlanStatusCode'), this.getControlValue('FromDate'), this.getControlValue('ToDate')].join(this.serviceConstants.PipeDelimeter));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.muleConfig['method'], this.muleConfig['module'], this.muleConfig['operation'], queryPost)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (!data.fullError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage));
                        } else {
                            this.reportMessage = data.fullError;
                            this.formPristine();
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }
    // Set Branch Area Code
    public setbranchServiceAreaCode(data: any): void {
        this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
        this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
        this.uiForm.controls['BranchServiceAreaCode'].markAsDirty();
        this.uiForm.controls['BranchServiceAreaDesc'].markAsDirty();
    }
}
