import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, OnInit, Injector, OnDestroy } from '@angular/core';

import * as moment from 'moment';

import { BaseComponent } from '../../base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { MntConst } from './../../../shared/services/riMaintenancehelper';

@Component({
    templateUrl: 'iCABSSInstallReceipt.html'
})
export class InstallReceiptComponent extends BaseComponent implements OnInit, OnDestroy {
    //API variables
    private queryParams: Object = {
        method: 'service-delivery/maintenance',
        module: 'installations',
        operation: 'Sales/iCABSSInstallReceipt'
    };
    private isSCEnableCompanyCode: boolean = true;

    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'BusinessDesc', disabled: true },
        { name: 'CompanyCode' },
        { name: 'BranchNumber' },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'ReportType', value: 'Processed' },
        { name: 'ReceiptType', value: 'All' },
        { name: 'RepDest', value: 'direct' }
    ];
    public branchCodeList: Array<any>;
    //Dropdown
    public dropdown: any = {
        company: {
            params: {
                parentMode: 'LookUp',
                businessCode: this.businessCode(),
                countryCode: this.countryCode()
            },
            active: { id: '', text: '' },
            type: 'Company'
        },
        branch: {
            params: {
                'parentMode': 'LookUp-NegBranch'
            },
            active: { id: '', text: '' },
            type: 'Branch'
        }
    };
    //Datepicker
    public datepickerType: any = {
        dateFrom: 'DateFrom',
        dateTo: 'DateTo'
    };
    public isHideCompany: boolean = false;
    public isTRInformation: boolean = false;
    public reportGenMsg: string = '';

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSINSTALLRECEIPT;
        this.browserTitle = this.pageTitle = MessageConstant.PageSpecificMessage.sInstallReceipt.pageTitle;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.getSysCharDtetails();
        this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        let dt: Date = new Date();
        this.setControlValue('BusinessDesc', this.utils.getBusinessText(this.businessCode(), this.countryCode()));
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('DateFrom', this.utils.addDays(dt, -1));
        this.setControlValue('DateTo', this.utils.addDays(dt, -1));
        //Set default selected dropdown values
        this.dropdown['branch']['active'] = { id: this.utils.getBranchCode(), 'text': this.utils.getBranchText() };
    }

    //Start: Syschar functionality
    private getSysCharDtetails(): void {
        let sysCharList: number[], sysCharIP: any;
        sysCharList = [
            this.sysCharConstants.SystemCharEnableCompanyCode //130
        ];
        sysCharIP = {
            module: this.queryParams['module'],
            operation: this.queryParams['operation'],
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.speedScript.sysCharPromise(sysCharIP).then((data) => {
            if (!this.handleSuccessError(data) && data.records && data.records.length > 0) {
                this.isSCEnableCompanyCode = data.records[0].Required;
                //Show/hide company
                this.isHideCompany = this.isSCEnableCompanyCode;
            }
        }).catch((error) => this.handleError(error));
    }
    //End: Syschar functionality

    //Start: API call functionality
    private handleError(error: any): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
    }

    private handleSuccessError(data: any): boolean {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        if (data.hasError) {
            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
        }
        return data.hasError;
    }

    private submitReportRequest(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: any = {}, dt: any = new Date(), parameterList: any = {}, parameterName: any = [], parameterValue: any = [];

        search.set(this.serviceConstants.Action, '0');

        parameterList['BusinessCode'] = this.businessCode();
        parameterList['BranchNumber'] = this.getControlValue('BranchNumber');
        parameterList['DateFrom'] = this.getControlValue('DateFrom');
        parameterList['DateTo'] = this.getControlValue('DateTo');
        parameterList['RepManDest'] = this.utils.getReportManDest(this.getControlValue('RepDest'));
        parameterList['CompanyCode'] = this.getControlValue('CompanyCode');
        parameterList['Receipttype'] = this.getControlValue('ReceiptType');
        parameterList['Reporttype'] = this.getControlValue('ReportType');

        for (let key in parameterList) {
            if (key) {
                parameterName.push(key);
                parameterValue.push(parameterList[key]);
            }
        }
        parameterName = parameterName.join(this.serviceConstants.PipeDelimeter);
        parameterValue = parameterValue.join(this.serviceConstants.PipeDelimeter);

        formData['Description'] = 'Installation/Removal/Receipts';
        formData['ProgramName'] = 'iCABSInstallationReceipts.p';
        formData['StartDate'] = this.globalize.parseDateToFixedFormat(dt);
        formData['StartTime'] = this.globalize.parseTimeToFixedFormat(moment(dt).format('HH:mm'));
        formData['Report'] = 'report';
        formData['ParameterName'] = parameterName;
        formData['ParameterValue'] = parameterValue;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'],
            this.queryParams['operation'], search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isTRInformation = true;
                    this.reportGenMsg = data.fullError;
                }, (error) => this.handleError(error));
    }
    //End: API call functionality

    //Start: Dropdown functionality
    public onChangeDD(e: any, type: any): void {
        switch (type) {
            case this.dropdown['company']['type']:
                this.setControlValue('CompanyCode', e.CompanyCode);
                break;
            case this.dropdown['branch']['type']:
                this.setControlValue('BranchNumber', e.BranchNumber);
                break;
        }
    }
    //Start: Dropdown functionality

    //Start: Date picker functionality
    public onSelecteDatePicker(e: any, type: string): void {
        switch (type) {
            case this.datepickerType['dateFrom']:
                this.setControlValue('DateFrom', e.value);
                break;
            case this.datepickerType['dateTo']:
                this.setControlValue('DateTo', e.value);
                break;
        }
    }
    //End: Date picker functionality

    public onClickSubmit(): void {
        if (this.uiForm.valid) {
            this.submitReportRequest();
        }
    }
}
