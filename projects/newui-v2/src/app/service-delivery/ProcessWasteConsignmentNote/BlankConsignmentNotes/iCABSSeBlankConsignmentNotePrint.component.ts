import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, OnInit, Injector, OnDestroy, ViewChild, AfterContentInit } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { DropdownStaticComponent } from '../../../../shared/components/dropdown-static/dropdownstatic';

@Component({
    templateUrl: 'iCABSSeBlankConsignmentNotePrint.html'
})

export class BlankConsignmentNotePrintComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('filterRegAuthorityDropdown') filterRegAuthorityDropdown: DropdownStaticComponent;
    @ViewChild('filterWasteTransferTypeDropdown') filterWasteTransferTypeDropdown: DropdownStaticComponent;
    // URL Query Parameters
    private queryParams: Object = {
        operation: 'Service/iCABSSeBlankConsignmentNotePrint',
        module: 'waste',
        method: 'service-delivery/maintenance'
    };
    private lookUpSubscription: Subscription;
    public filterRegAuthorityList: Array<Object> = [];
    public filterWasteTransferTypeList: Array<Object> = [];
    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'FilterRegAuthority' },
        { name: 'FilterWasteTransferType' },
        { name: 'PrintCopies', type: MntConst.eTypeInteger },
        { name: 'SchedulePrintCopies', type: MntConst.eTypeInteger },
        { name: 'AuthorityPrintCopies', type: MntConst.eTypeInteger },
        { name: 'UsePrintTool', type: MntConst.eTypeCheckBox },
        { name: 'WasteTransferTypeList' },
        { name: 'WasteTransferTypeDescList' }
    ];
    public scheduleRequiredInd: string;
    public authorityCopyRequiredInd: string;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEBLANKCONSIGNMENTNOTEPRINT;
        this.browserTitle = this.pageTitle = 'Blank Consignment Note Print';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnLoad();
    }

    ngAfterContentInit(): void {
        this.scheduleRequiredInd = 'yes';
        this.setControlValue('UsePrintTool', true);
    }

    ngOnDestroy(): void {
        if (this.lookUpSubscription)
            this.lookUpSubscription.unsubscribe();
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        this.setControlValue('PrintCopies', '1');
        this.setControlValue('SchedulePrintCopies', '1');
        this.setControlValue('AuthorityPrintCopies', '1');
        this.fetchLookUp();
    }

    private fetchLookUp(): void {
        let dataArray: any;
        let dataArraylength: number;
        let obj: Object = {};
        let searchParams: QueryParams;
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '0');
        searchParams.set('RegulatoryAuthorityNumber', '');
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        dataArray = data.records;
                        dataArraylength = dataArray.length;
                        if (dataArraylength > 0) {
                            for (let i = 0; i < dataArraylength; i++) {
                                obj = {
                                    text: dataArray[i]['RegulatoryAuthority.RegulatoryAuthorityName'],
                                    value: dataArray[i]['RegulatoryAuthority.RegulatoryAuthorityNumber']
                                };
                                this.filterRegAuthorityList.push(obj);
                            }
                            this.filterRegAuthorityDropdown.selectedItem = this.filterRegAuthorityList[0]['value'];
                            this.filterRegAuthorityOnChange(this.filterRegAuthorityDropdown.selectedItem);
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                }
            );

    }

    //On changing Reg Authority dropdown
    public filterRegAuthorityOnChange(event: any): void {
        let obj: Object = {}, postData: Object = {};
        this.filterWasteTransferTypeList = [];
        let searchParams: QueryParams;
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        postData['Function'] = 'GetWasteTransferType';
        postData['RegulatoryAuthority'] = event;
        //making post request
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        this.setControlValue('WasteTransferTypeList', data.WasteTransferTypeList);
                        this.setControlValue('WasteTransferTypeDescList', data.WasteTransferTypeDescList);
                        obj = {
                            text: data.WasteTransferTypeDescList,
                            value: data.WasteTransferTypeList
                        };
                        if (obj['text']) {
                            this.filterWasteTransferTypeList.push(obj);
                            this.setControlValue('FilterWasteTransferType', data.WasteTransferTypeList);
                            this.filterWasteTransferTypeDropdown.selectedItem = this.getControlValue('FilterWasteTransferType');
                            this.filterWasteTransferTypeOnChange(this.getControlValue('FilterWasteTransferType'));
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    //On changing waste transfer type dropdown
    public filterWasteTransferTypeOnChange(event: any): void {
        let searchParams: QueryParams, postData: Object = {};
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        postData['Function'] = 'DisplayCopies';
        postData['RegulatoryAuthority'] = this.filterRegAuthorityDropdown.selectedItem;
        postData['WasteTransferTypeCode'] = event;
        //making post request
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        this.scheduleRequiredInd = data.ScheduleRequiredInd;
                        this.authorityCopyRequiredInd = data.AuthorityCopyRequiredInd;
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public cmdPrintCopiesOnClick(): void {
        let printCopies: any;
        printCopies = this.getControlValue('PrintCopies');
        if (printCopies)
            this.printBlankDocument('WasteConsignmentNote', printCopies);
    }
    public cmdPrintScheduleCopiesOnClick(): void {
        let schedulePrintCopies: any;
        schedulePrintCopies = this.getControlValue('SchedulePrintCopies');
        if (schedulePrintCopies)
            this.printBlankDocument('Schedule', schedulePrintCopies);
    }
    public cmdPrintAuthorityCopiesOnClick(): void {
        let authorityPrintCopies: any;
        authorityPrintCopies = this.getControlValue('AuthorityPrintCopies');
        if (authorityPrintCopies)
            this.printBlankDocument('AuthorityCopy', authorityPrintCopies);
    }

    //On clicking of print button
    public printBlankDocument(ipDocumentType: string, ipNumCopies: number): void {
        let searchParams: QueryParams, postData: Object = {};
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        postData['Function'] = 'PrintBlank';
        postData['BusinessCode'] = this.businessCode();
        postData['BranchNumber'] = this.utils.getBranchCode();
        postData['RegulatoryAuthority'] = this.filterRegAuthorityDropdown.selectedItem;
        postData['WasteTransferTypeCode'] = this.filterWasteTransferTypeDropdown.selectedItem;
        postData['DocumentType'] = ipDocumentType;
        postData['NumPrintCopies'] = ipNumCopies;
        //making post request
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else if (this.getControlValue('UsePrintTool'))
                        window.open('icabs-wcnprint://localhost/print?duplications=' + data.Duplications + '&url=' + window['encodeURI'](data.BlankWasteConsignmentNotes), '_blank');
                    else
                        window.open(data.BlankWasteConsignmentNotes, '_blank');
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }
}
