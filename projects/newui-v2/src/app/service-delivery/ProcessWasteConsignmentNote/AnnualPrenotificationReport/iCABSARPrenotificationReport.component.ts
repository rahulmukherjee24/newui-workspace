import { Component, OnInit, AfterViewInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { BranchServiceAreaSearchComponent } from './../../../internal/search/iCABSBBranchServiceAreaSearch';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';

@Component({
    templateUrl: 'iCABSARPrenotificationReport.html'
})

export class ARPrenotificationReportComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('branchServiceAreaCode') branchServiceAreaCode;
    private getReportManDest: any;
    private delimeter: string = '';
    private xhrParams: any = {
        operation: 'ApplicationReport/iCABSARPrenotificationReport',
        module: 'waste',
        method: 'service-delivery/maintenance'
    };
    private serviceSubscription: Subscription;
    private regulatorySubscription: Subscription;
    public isMandatory: boolean = true;
    public isBranchServiceSearchModal: boolean = false;
    public pageId: string = '';
    public batchSubmittedText: string;
    public isShowBatchSubmitted: boolean = false;
    public controls = [
        { name: 'BranchServiceAreaCode', readonly: false, disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeText },
        { name: 'RegulatoryAuthorityNumber', readonly: false, disabled: false, required: true, type: MntConst.eTypeInteger },
        { name: 'RegulatoryAuthorityName', type: MntConst.eTypeText },
        { name: 'RepDest', value: 'direct' },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true }
    ];

    public fieldRequired: any = {
        'BranchNumber': false
    };

    public dropdownConfig: any = {
        branchSearch: {
            isDisabled: true,
            inputParams: {},
            selected: { id: '', text: '' }
        }
    };

    public ellipsisConfig: any = {
        branchService: {
            isShowCloseButton: true,
            iSShowHeader: true,
            childConfigParams: {
                'parentMode': 'LookUp-Emp'
            },
            component: BranchServiceAreaSearchComponent
        },
        regulatoryAuthorityNumber: {
            autoOpen: false,
            ellipsisTitle: 'Regulatory Authority Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp',
                extraParams: {
                    'search.sortby': 'SeasonalTemplateNumber',
                    'BranchNumber': this.utils.getBranchCode()
                }
            },
            httpConfig: {
                operation: 'Business/iCABSBRegulatoryAuthoritySearch',
                module: 'waste',
                method: 'service-delivery/search'
            },
            tableColumn: [
                { title: 'Regulatory Authority Number', name: 'RegulatoryAuthorityNumber' },
                { title: 'Name', name: 'RegulatoryAuthorityName' },
                { title: 'Max Weight Per Day (KG)', name: 'MaximumWeightPerDay' },
                { title: 'Requires Premises Registration', name: 'RequiresPremiseRegistrationInd' },
                { title: 'Requires Service Cover Waste', name: 'RequiresServiceCoverWasteInd' },
                { title: 'Requires Waste Transfer Notes', name: 'RequiresWasteTransferNotesInd' },
                { title: 'Waste Regulatory Carrier Name', name: 'WasteRegulatoryCarrierName' },
                { title: 'Waste Regulatory Registration Number', name: 'WasteRegulatoryRegistrationNum' }
            ],
            disable: false
        }
    };

    ngOnInit(): void {
        super.ngOnInit();
        this.oNLoad();
    }

    ngOnDestroy(): void {
        if (this.serviceSubscription) {
            this.serviceSubscription.unsubscribe();
        }
        if (this.regulatorySubscription) {
            this.regulatorySubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }

    ngAfterViewInit(): void {
        this.isBranchServiceSearchModal = true;
    }

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARPRENOTIFICATIONREPORT;
        this.browserTitle = this.pageTitle = 'Annual Prenotification Report';
    }

    private oNLoad(): void {
        this.getReportManDest = this.utils.getReportManDest(this.getControlValue('RepDest'));
        this.dropdownConfig.branchSearch.selected = { id: this.utils.getBranchCode(), text: this.utils.getBranchText() };
        this.disableControl('EmployeeSurname', true);
        this.disableControl('RegulatoryAuthorityName', true);
        this.branchServiceAreaCode.nativeElement.focus();
        this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(new Date(new Date().getFullYear() + 1, 0, 1)));
        this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(new Date(new Date().getFullYear() + 1, 11, 31)));
    }

    private submitReportRequest(): void {
        let strDescription: any = 'Annual Prenotification Report';
        let strProgramName: any = 'iCABSPrenotificationReportGeneration.p';
        let strStartDate: any;
        let strStartTime: any;
        let strParamName: any;
        let strParamValue: any;
        strParamName = 'BusinessCode' + this.delimeter +
            'BranchNumber' + this.delimeter +
            'BranchServiceAreaCode' + this.delimeter +
            'RegulatoryAuthorityNumber' + this.delimeter +
            'DateFrom' + this.delimeter +
            'DateTo' + this.delimeter +
            'RepManDest' + this.delimeter;
        strParamValue = this.utils.getBusinessCode() + this.delimeter +
            this.utils.getBranchCode() + this.delimeter +
            this.getControlValue('BranchServiceAreaCode') + this.delimeter +
            this.getControlValue('RegulatoryAuthorityNumber') + this.delimeter +
            this.getControlValue('DateFrom') + this.delimeter +
            this.getControlValue('DateTo') + this.delimeter +
            this.getReportManDest + this.delimeter;
        let date: Date = new Date();
        strStartDate = this.globalize.parseDateToFixedFormat(date);
        let formattedTime: any = Math.round(date.getTime() / (1000 * 60 * 60));
        strStartTime = this.globalize.parseTimeToFixedFormat(formattedTime);
        this.ajaxSource.next(this.ajaxconstant.START);
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        searchParams.set(this.serviceConstants.Action, '2');
        let bodyParams: any = {};
        bodyParams['Description'] = strDescription;
        bodyParams['ProgramName'] = strProgramName;
        bodyParams['StartDate'] = strStartDate;
        bodyParams['StartTime'] = strStartTime;
        bodyParams['Report'] = 'report';
        bodyParams['ParameterName'] = strParamName;
        bodyParams['ParameterValue'] = strParamValue;
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.isShowBatchSubmitted = true;
                this.batchSubmittedText = data.fullError;
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            });
    }

    public onChangeDateFrom(value: any): void {
        if (value) {
            this.setControlValue('DateFrom', value.value);
        }
    }

    public onChangeDateTo(value: any): void {
        if (value) {
            this.setControlValue('DateTo', value.value);
        }
    }

    public onBranchServiceDataReceived(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
    }
    public onRegulatoryAuthorityDataReceived(data: any): void {
        if (data) {
            this.setControlValue('RegulatoryAuthorityNumber', data.RegulatoryAuthorityNumber);
            this.setControlValue('RegulatoryAuthorityName', data.RegulatoryAuthorityName);
        }
    }

    public onChangeMenu(): void {
        this.getReportManDest = this.utils.getReportManDest(this.getControlValue('RepDest'));
    }

    public onBlurServiceBranch(): void {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let postSearchParams: any = this.getURLSearchParamObject();
            postSearchParams.set(this.serviceConstants.Action, '6');
            let postParams: any = {};
            postParams.PostDesc = 'BranchServiceArea';
            postParams.BusinessCode = this.utils.getBusinessCode();
            postParams.BranchNumber = this.utils.getBranchCode();
            postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.serviceSubscription = this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, postSearchParams, postParams)
                .subscribe(
                (data) => {
                    if ((data['hasError'])) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                        this.setControlValue('EmployeeSurname', '');
                    } else {
                        if (data['EmployeeSurname']) {
                            this.setControlValue('EmployeeSurname', data['EmployeeSurname']);
                        }
                        else {
                            this.setControlValue('EmployeeSurname', '');
                        }
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
        }
        else {
            this.setControlValue('EmployeeSurname', '');
        }
    }

    public onChangeRegulatoryAuthorityNumber(): void {
        if (this.getControlValue('RegulatoryAuthorityNumber')) {
            let postSearchParams = this.getURLSearchParamObject();
            postSearchParams.set(this.serviceConstants.Action, '6');
            let postParams: any = {};
            postParams.PostDesc = 'RegulatoryAuthority';
            postParams.BusinessCode = this.utils.getBusinessCode();
            postParams.RegulatoryAuthorityNumber = this.getControlValue('RegulatoryAuthorityNumber');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.regulatorySubscription = this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, postSearchParams, postParams)
                .subscribe(
                (data) => {
                    if ((data['hasError'])) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                        this.setControlValue('RegulatoryAuthorityNumber', '');
                        this.setControlValue('RegulatoryAuthorityName', '');
                    } else {
                        if (data['RegulatoryAuthorityName']) {
                            this.setControlValue('RegulatoryAuthorityName', data['RegulatoryAuthorityName']);
                        }
                        else {
                            this.setControlValue('RegulatoryAuthorityNumber', '');
                            this.setControlValue('RegulatoryAuthorityName', '');
                        }
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
        }
        else {
            this.setControlValue('RegulatoryAuthorityNumber', '');
            this.setControlValue('RegulatoryAuthorityName', '');
        }
    }

    public onClickSubmit(): void {
        if ((this.utils.getBranchCode()) && (this.getControlValue('BranchServiceAreaCode'))
            && (this.getControlValue('RegulatoryAuthorityNumber')) && !(this.riExchange.riInputElement.isError(this.uiForm, 'DateFrom')) && !(this.riExchange.riInputElement.isError(this.uiForm, 'DateTo'))) {
            this.submitReportRequest();
        }

    }
}


