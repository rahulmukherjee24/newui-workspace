import { Component, OnInit, Injector, ViewChild, OnDestroy, HostListener } from '@angular/core';

import { Subscription } from 'rxjs';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { GridAdvancedComponent } from '../../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { AppModuleRoutes, ServiceDeliveryModuleRoutes } from '../../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSSeWasteConsignmentNoteGenerate.html'
})

export class WasteConsignmentNoteGenerateComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    private vEndofWeekDay: number;
    private gsnSubscription: Subscription;
    private gridSubscription: Subscription;
    private vEndofWeekDate: any;
    private pageCurrent: number = 1;
    private search: any = this.getURLSearchParamObject();
    private xhrParams: any = {
        operation: 'Service/iCABSSeWasteConsignmentNoteGenerate',
        module: 'waste',
        method: 'service-delivery/batch'
    };
    public isDisableGSN: boolean = false;
    public pageSize = 10;
    public batchSubmittedText: string;
    public isShowBatchSubmitted: boolean = false;
    public isDatePickerRequired: boolean = true;
    public itemsPerPage: number = 10;
    public totalRecords: number;
    public pageId: string = '';
    public controls = [
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'BranchServiceAreaList', type: MntConst.eTypeTextFree },
        { name: 'GenerateAll', type: MntConst.eTypeCheckBox },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true },
        { name: 'GenerateAll', type: MntConst.eTypeCheckBox },
        { name: 'BranchServiceAreaList_Machine' },
        { name: 'BusinessCode' },
        { name: 'BranchNumber' }
    ];

    ngOnInit(): void {
        super.ngOnInit();
        this.getSystemParameters();
    }

    ngOnDestroy(): void {
        if (this.gsnSubscription) {
            this.gsnSubscription.unsubscribe();
        }
        if (this.gridSubscription) {
            this.gridSubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEWASTECONSIGNMENTNOTEGENERATE;
        this.pageTitle = 'Generate Waste Consignment Notes';
        this.browserTitle = 'Waste Consignment Note Generation';
    }

    private getSystemParameters(): void {
        let lookUpSys: any = [{
            'table': 'SystemParameter',
            'query': {},
            'fields': ['SystemParameterEndOfWeekDay']
        }];
        this.LookUp.lookUpRecord(lookUpSys).subscribe((data) => {
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                if (data[0][0].SystemParameterEndOfWeekDay < 7) {
                    this.vEndofWeekDay = data[0][0].SystemParameterEndOfWeekDay;
                } else {
                    this.vEndofWeekDay = 1;
                }
                let today: any = new Date();
                this.vEndofWeekDate = this.utils.addDays(today, ((7 - today.getDay()) + this.vEndofWeekDay - 1));
                let tempDate: any = this.vEndofWeekDate.toString();
                tempDate = (this.globalize.parseDateStringToDate(tempDate)).toString();
                this.vEndofWeekDate = new Date(tempDate);
                if (this.isReturning()) {
                    this.buildGrid();
                } else {
                    this.windowOnload();
                }
            }
        });
    }


    private windowOnload(): void {
        this.disableControl('BranchName', true);
        this.disableControl('BranchServiceAreaList', true);

        this.setControlValue('GenerateAll', true);
        this.setControlValue('BusinessCode', this.utils.getBusinessCode());
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchTextOnly(this.utils.getBranchCode()));

        let formatteddateFrom: any = this.globalize.formatDateToLocaleFormat(this.vEndofWeekDate);
        let tempDtFrom: Date = this.utils.addDays(formatteddateFrom, 1);
        this.setControlValue('DateFrom', tempDtFrom);
        let tempDtTo: Date = this.utils.addDays(tempDtFrom, 6);
        this.setControlValue('DateTo', tempDtTo);
    }

    //fn to execute grid on page load
    private riGridBeforeExecute(): void {
        this.riGrid.RefreshRequired();
        this.search.set(this.serviceConstants.Action, '2');

        this.search.set('BranchNumber', this.utils.getBranchCode());
        this.search.set('DateFrom', this.globalize.parseDateToFixedFormat(this.getControlValue('DateFrom')));
        this.search.set('DateTo', this.globalize.parseDateToFixedFormat(this.getControlValue('DateTo')));
        this.search.set('BranchServiceAreaList', this.getControlValue('BranchServiceAreaList_Machine'));
        this.search.set('GenerateAll', this.getControlValue('GenerateAll'));

        this.search.set(this.serviceConstants.PageSize, (this.itemsPerPage).toString());
        this.search.set(this.serviceConstants.PageCurrent, this.pageCurrent.toString());
        this.search.set(this.serviceConstants.GridMode, '0');
        this.search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());

        this.search.set('riSortOrder', 'Descending');
        this.search.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        this.xhrParams.search = this.search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.xhrParams.search)
            .subscribe(
                (data) => {
                    if (data) {
                        if (data) {
                            try {
                                this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                                this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                                if (data['hasError']) {
                                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                                } else {
                                    this.riGrid.Execute(data);
                                }

                            } catch (e) {
                                this.logger.log('Problem in grid load', e);
                            }
                        }
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                error => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.totalRecords = 1;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('BranchServiceAreaCode', 'WCNGenerate', 'BranchServiceAreaCode', MntConst.eTypeCode, 5);
        this.riGrid.AddColumn('EmployeeSurname', 'WCNGenerate', 'EmployeeSurname', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('UngeneratedCount', 'WCNGenerate', 'UngeneratedCounted', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumn('WillGenerate', 'WCNGenerate', 'WillGenerate', MntConst.eTypeImage, 3);
        this.riGrid.AddColumn('GeneratedCount', 'WCNGenerate', 'GeneratedCounted', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumn('Print', 'WCNGenerate', 'Print', MntConst.eTypeImage, 3);
        this.riGrid.AddColumn('PrintAuthority', 'WCNGenerate', 'PrintAuthority', MntConst.eTypeImage, 3);
        this.riGrid.AddColumn('PrintSchedule', 'WCNGenerate', 'PrintSchedule', MntConst.eTypeImage, 3);

        this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Print', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PrintAuthority', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PrintSchedule', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
        this.riGridBeforeExecute();
    }

    private addDays(date: Date, days: number): any {
        let d: any = this.globalize.parseDateToFixedFormat(date.toString());
        let newDate: Date = new Date(d.toString());
        newDate.setDate(newDate.getDate() + days);
        let adddate: any = this.globalize.parseDateToFixedFormat(newDate);
        return adddate;
    }


    // fn  for double click event inside grid
    private selectedRowFocus(event: any, rsrcElement: any): void {
        let oTR: any = rsrcElement.parentElement.parentElement.parentElement;
        switch (rsrcElement.parentElement.parentElement.getAttribute('name')) {
            case 'WillGenerate':
                let postSearchParams = this.getURLSearchParamObject();
                postSearchParams.set(this.serviceConstants.Action, '6');
                let postParams: any = {};
                postParams.Function = 'ToggleGenerate';
                postParams.BranchNumber = this.utils.getBranchCode();
                postParams.BranchServiceAreaCode = this.riGrid.Details.GetValue('BranchServiceAreaCode');
                postParams.BranchServiceAreaList = this.getControlValue('BranchServiceAreaList_Machine');

                this.ajaxSource.next(this.ajaxconstant.START);
                this.gridSubscription = this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, postSearchParams, postParams)
                    .subscribe(
                        (data) => {
                            if ((data['hasError'])) {
                                this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));

                            } else {
                                this.setControlValue('BranchServiceAreaList', data['HumanServiceAreaList']);
                                this.setControlValue('BranchServiceAreaList_Machine', data['BranchServiceAreaList']);
                                this.refresh();

                            }
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        },
                        (error) => {
                            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        });
                break;
            case 'Print':
            case 'PrintAuthority':
            case 'PrintSchedule':
                if (event.srcElement.getAttribute('value')) {
                    if (event.srcElement.getAttribute('AdditionalProperty') === 'DrillDown') {
                        this.setAttribute('BranchServiceAreaCode', this.riGrid.Details.GetValue('BranchServiceAreaCode'));
                        this.setAttribute('BranchServiceAreaDesc', this.riGrid.Details.GetValue('EmployeeSurname'));
                        this.navigate('BranchServiceArea', AppModuleRoutes.SERVICEDELIVERY + ServiceDeliveryModuleRoutes.ICABSSEWASTECONSIGNMENTNOTEGENERATEDETAIL, {});
                    } else {
                        this.search = this.getURLSearchParamObject();
                        this.search.set(this.serviceConstants.Action, '6');
                        this.search.set('id', event.srcElement.getAttribute('AdditionalProperty'));
                        this.isRequesting = true;
                        this.httpService.fetchReportURLGET({
                            operation: 'datastore',
                            search: this.search
                        }).then(data => {
                            this.isRequesting = false;
                        }).catch(error => {
                            this.globalNotifications.displayMessage(error);
                        });
                    }
                }
        }
    }


    public refresh(): void {
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'DateFrom') && !this.riExchange.riInputElement.isError(this.uiForm, 'DateTo')) {
            this.riGrid.RefreshRequired();
            this.buildGrid();
        }
    }

    public getCurrentPage(currentPage: any): void {
        this.pageCurrent = currentPage.value;
        this.riGridBeforeExecute();
    }

    public onChangeDateFrom(value: any): void {
        if (value) {
            this.setControlValue('DateFrom', value.value);
        }
    }

    public generateOnChange(): void {
        this.refresh();
    }

    public onChangeDateTo(value: any): void {
        if (value) {
            this.setControlValue('DateTo', value.value);
        }
        this.refresh();
    }

    public riGridAfterExecute(): void {
        setTimeout(() => {
            if (!this.getControlValue('GenerateAll')) {
                if (this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.innerHTML) {
                    if (this.riGrid.HTMLGridBody.children[0].children[0].children[0].getAttribute('additionalproperty')) {
                        this.setControlValue('BranchServiceAreaList', this.riGrid.HTMLGridBody.children[0].children[0].children[0].getAttribute('additionalproperty'));
                    }
                    else {
                        this.setControlValue('BranchServiceAreaList', null);
                    }
                    if (this.riGrid.HTMLGridBody.children[0].children[1].getAttribute('additionalproperty')) {
                        this.setControlValue('BranchServiceAreaList_Machine', this.riGrid.HTMLGridBody.children[0].children[1].getAttribute('additionalproperty'));
                    }
                    else {
                        this.setControlValue('BranchServiceAreaList_Machine', null);
                    }
                }
                else {
                    this.setControlValue('BranchServiceAreaList', null);
                    this.setControlValue('BranchServiceAreaList_Machine', null);
                }
            }
        }, 0);

    }

    public riGridBodyOnDBLClick(ev: any): void {
        this.selectedRowFocus(ev, ev.srcElement);
    }

    // fn to generate batch process
    public onClickGSN(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams.Function = 'SubmitBatchProcess';
        postParams.BranchNumber = this.utils.getBranchCode();
        postParams.GenerateAll = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('GenerateAll'));
        postParams.DateFrom = this.globalize.parseDateToFixedFormat(this.getControlValue('DateFrom'));
        postParams.DateTo = this.globalize.parseDateToFixedFormat(this.getControlValue('DateTo'));
        postParams.BranchServiceAreaList = this.getControlValue('BranchServiceAreaList_Machine');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.gsnSubscription = this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, postSearchParams, postParams)
            .subscribe(
                (data) => {
                    if ((data['hasError'])) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));

                    } else {
                        this.isShowBatchSubmitted = true;
                        this.batchSubmittedText = data.BatchProcessInformation;

                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
        this.isDisableGSN = true;
    }

    //function used for changing values of date picker and refreshing grid on key down of plus minus in num pad

    @HostListener('document:keydown', ['$event']) document_onkeydown(ev: any): void {
        //let dtFrom = new Date(this.getControlValue('DateFrom'));
        let dtFrom: any = this.getControlValue('DateFrom');
        let dtTo: any = this.getControlValue('DateTo');
        switch (ev.keyCode) {

            case 106:
                let datetFrom: Date = this.addDays(this.vEndofWeekDate, 1);
                let dateTo: Date = this.addDays(this.vEndofWeekDate, 7);
                this.setControlValue('DateFrom', datetFrom);
                this.setControlValue('DateTo', dateTo);
                this.riGrid.RefreshRequired();
                break;
            case 107:
                dtFrom = this.addDays(dtFrom, 7);
                dtTo = this.addDays(dtTo, 7);
                this.setControlValue('DateFrom', dtFrom);
                this.setControlValue('DateTo', dtTo);
                this.riGrid.RefreshRequired();
                break;
            case 109:
                dtFrom = this.addDays(dtFrom, -7);
                dtTo = this.addDays(dtTo, -7);
                this.setControlValue('DateFrom', dtFrom);
                this.setControlValue('DateTo', dtTo);
                this.riGrid.RefreshRequired();
                break;
        }
    }


}
