import { QueryParams } from '@shared/services/http-params-wrapper';
/**
 * The WasteConsignmentNoteGenerateDetailComponent to display note generation detail
 * @author icabs ui team
 * @version 1.0
 * @since  01/02/2018
 */
import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { GridAdvancedComponent } from '../../../../shared/components/grid-advanced/grid-advanced';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { PaginationComponent } from '../../../../shared/components/pagination/pagination';
import { DatepickerComponent } from '../../../../shared/components/datepicker/datepicker';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { GlobalizeService } from '../../../../shared/services/globalize.service';


@Component({
    templateUrl: 'iCABSSeWasteConsignmentNoteGenerateDetail.html'
})

export class WasteConsignmentNoteGenerateDetailComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('fromDateChild') public fromDateChild: DatepickerComponent;
    @ViewChild('toDateChild') public toDateChild: DatepickerComponent;
    @ViewChild('riGrid') public riGrid: GridAdvancedComponent;
    @ViewChild('wsPagination') wsPagination: PaginationComponent;

    private inputParams: any = {
        method: 'service-delivery/batch',
        module: 'waste',
        operation: 'Service/iCABSSeWasteConsignmentNoteGenerateDetail'
    };

    public itemsPerPage: number = 10;
    public pageCurrent: number = 1;
    public totalRecords: number = 1;
    public pageId: string = '';
    public currDate: any = null;
    public currDate1: any = null;
    public SelectedLine: any = {};
    public controls: Array<Object> = [
        { name: 'BranchNumber', type: MntConst.eTypeInteger },
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', type: MntConst.eTypeText },
        { name: 'DateFrom', type: MntConst.eTypeDate },
        { name: 'DateTo', type: MntConst.eTypeDate },
        { name: 'UsePrintTool' }
    ];

    //Lifecycles/Constructor methods definition
    constructor(injector: Injector, public globalize: GlobalizeService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEWASTECONSIGNMENTNOTEGENERATEDETAIL;
        this.browserTitle = 'Generate Service Listing';
        this.pageTitle = 'Generate Waste Consignment Notes';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.initData();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * This method used to initialize variables
     * @param void
     * @return void
     */
    private initData(): void {
        this.setCurrentContractType();
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.disableControl('BranchNumber', true);
        this.disableControl('BranchName', true);
        this.disableControl('BranchServiceAreaCode', true);
        this.disableControl('BranchServiceAreaDesc', true);
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchText());
        this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentAttributeValue('BranchServiceAreaCode'));
        this.setControlValue('BranchServiceAreaDesc', this.riExchange.getParentAttributeValue('BranchServiceAreaDesc'));
        this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(this.riExchange.getParentHTMLValue('DateFrom')));
        this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(this.riExchange.getParentHTMLValue('DateTo')));
        this.currDate = this.globalize.parseDateToFixedFormat(this.riExchange.getParentHTMLValue('DateFrom'));
        this.currDate1 = this.globalize.parseDateToFixedFormat(this.riExchange.getParentHTMLValue('DateTo'));
        this.setControlValue('UsePrintTool', true);
        this.buildGrid();
        this.riGridBeforeExecute();
    }

    /**
     * This method is used to configure grid(add columns/alignment setting etc)
     * @param void
     * @return void
     */
    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('RegulatoryAuthorityNumber', 'WCNGenerateDetail', 'RegulatoryAuthorityNumber', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumn('RegulatoryAuthorityName', 'WCNGenerateDetail', 'RegulatoryAuthorityName', MntConst.eTypeCode, 20);
        this.riGrid.AddColumn('WasteTransferTypeCode', 'WCNGenerateDetail', 'WasteTransferTypeCode', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumn('WasteTransferTypeDesc', 'WCNGenerateDetail', 'WasteTransferTypeDesc', MntConst.eTypeCode, 20);
        this.riGrid.AddColumn('GeneratedCount', 'WCNGenerateDetail', 'GeneratedCounted', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumn('Print', 'WCNGenerateDetail', 'Print', MntConst.eTypeImage, 3);
        this.riGrid.AddColumn('PrintAuthority', 'WCNGenerateDetail', 'PrintAuthority', MntConst.eTypeImage, 3);
        this.riGrid.AddColumn('PrintSchedule', 'WCNGenerateDetail', 'PrintSchedule', MntConst.eTypeImage, 3);

        this.riGrid.AddColumnAlign('RegulatoryAuthorityNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('WasteTransferTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Print', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PrintAuthority', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PrintSchedule', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
    }

    /**
     * This method is called before execute grid
     * @param void
     * @return void
     */
    public riGridBeforeExecute(): void {
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'DateFrom') && !this.riExchange.riInputElement.isError(this.uiForm, 'DateTo')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set('BranchNumber', this.getControlValue('BranchNumber'));
            search.set('DateFrom', this.getControlValue('DateFrom'));
            search.set('DateTo', this.getControlValue('DateTo'));
            search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
            search.set(this.serviceConstants.GridMode, '0');
            search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
            search.set(this.serviceConstants.GridCacheRefresh, 'True');
            search.set(this.serviceConstants.PageSize, this.itemsPerPage.toString());
            search.set(this.serviceConstants.PageCurrent, this.pageCurrent.toString());
            search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
            search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
            search.set(this.serviceConstants.Action, '2');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.inputParams.method, this.inputParams.module,
                this.inputParams.operation, search)
                .subscribe((data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    }
                    else {
                        try {
                            this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                            this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                            this.riGrid.UpdateHeader = true;
                            this.riGrid.UpdateBody = true;
                            this.riGrid.UpdateFooter = false;
                            if (this.riGrid.Update) {
                                this.riGrid.StartRow = this.SelectedLine.Row;
                                this.riGrid.StartColumn = 0;
                                this.riGrid.RowID = this.SelectedLine.RowID;
                                this.riGrid.UpdateHeader = false;
                                this.riGrid.UpdateBody = true;
                                this.riGrid.UpdateFooter = false;
                            }
                            this.riGrid.Execute(data);

                        } catch (e) {
                            this.logger.warn(e);
                        }
                    }
                },
                    (error) => {
                        this.totalRecords = 1;
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        }
    }

    /**
     * This method is called refresh button click
     * @param void
     * @return void
     */
    public refresh(): void {
        this.riGridBeforeExecute();
    }
    private setCurrentContractType(): void {
        this.pageParams.currentContractType = this.riExchange.getCurrentContractType();
        this.pageParams.currentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
    }

    /**
     * This method is used to capture from date change value
     * @param dtObj date picker selected value
     * @return void
     */
    public fromDateSelectedValue(dtObj: any): void {
        this.setControlValue('DateFrom', dtObj.value);
        this.riGridBeforeExecute();
    }

    /**
     * This method is used to capture to date change value
     * @param dtObj date picker selected value
     * @return void
     */
    public toDateSelectedValue(dtObj: any): void {
        this.setControlValue('DateTo', dtObj.value);
        this.riGridBeforeExecute();
    }

    /**
     * This method is called on grid double click
     * @param void
     * @return void
     */
    public riGridBodyOndblClick(data: any): void {
        this.gridFocus(data.srcElement.parentElement.parentElement.parentElement.children[0].children[0].children[0]);
        switch (this.riGrid.CurrentColumnName) {
            case 'Print':
            case 'PrintAuthority':
            case 'PrintSchedule':
                if (this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'AdditionalProperty')) {
                    switch (this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'AdditionalProperty')) {
                        case 'DrillDown':
                            //Same screen direction is not required
                            //riExchange.Mode = "DrillDown"
                            //window.location = "/wsscripts/riHTMLWrapper.p?riFileName=Service/iCABSSeWasteConsignmentNoteGenerateDetail.htm"
                            break;
                        default:
                            if (this.getControlValue('UsePrintTool')) {
                                //ToDo Pending with Mark
                                /*If UsePrintTool.checked Then

                                Dim cmdLine
                                cmdLine = """C:\\Program Files\\riWebBrowser\\iCABS Print.exe"" " _
                                & " --duplications " & window.event.srcElement.getAttribute("RowID") _
                                & " --url " & window.event.srcElement.getAttribute("AdditionalProperty")
                                Dim Shell : set Shell = CreateObject("WScript.Shell")
                                Call Shell.Run(cmdLine, 1, false)*/
                                window.open('icabs-wcnprint://localhost/print?duplications=' + this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'rowid') + '&url=' + encodeURI(this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'AdditionalProperty')), '_blank');
                            } else {
                                window.open(this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'AdditionalProperty'), '_blank');
                            }
                    }
                }
        }
    }

    /**
     * This method is called pagination click
     * @param currentPage is the current page number
     * @return void
     */
    public getCurrentPage(currentPage: any): void {
        this.pageCurrent = currentPage.value;
        this.riGridBeforeExecute();
    }

    public gridFocus(rsrcElement: any): void {
        let oTR: any = rsrcElement;
        this.SelectedLine.RowID = oTR.RowID;
        this.SelectedLine.Row = oTR.parentElement.parentElement.sectionRowIndex;
    }

}


