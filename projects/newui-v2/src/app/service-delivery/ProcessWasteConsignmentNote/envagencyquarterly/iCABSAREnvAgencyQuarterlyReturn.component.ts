import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { PaginationComponent } from '../../../../shared/components/pagination/pagination';
import { BranchServiceAreaSearchComponent } from '../../../../app/internal/search/iCABSBBranchServiceAreaSearch';
import { GridAdvancedComponent } from '../../../../shared/components/grid-advanced/grid-advanced';
import { RefreshComponent } from '../../../../shared/components/refresh/refresh';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { AjaxConstant } from '../../../../shared/constants/AjaxConstants';
import { ScreenNotReadyComponent } from '../../../../shared/components/screenNotReady';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';


@Component({
    templateUrl: 'iCABSAREnvAgencyQuarterlyReturn.html'
})

export class EnvAgencyQuarterlyReturnComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('eqrGridPagination') eqrGridPagination: PaginationComponent;
    @ViewChild('refreshBtn') public refreshBtn: RefreshComponent;

    private queryParams: any = {
        operation: 'ApplicationReport/iCABSAREnvAgencyQuarterlyReturn',
        module: 'waste',
        method: 'service-delivery/grid'
    };
    private riCacheRefresh: string = 'True';
    private riGridHandle: string = this.utils.randomSixDigitString();

    public pageId: string = '';
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public ellipsis: any = {
        branchServiceArea: {
            disabled: false,
            showCloseButton: true,
            showHeader: true,
            showAddNew: false,
            autoOpenSearch: false,
            parentMode: 'LookUp-Emp',
            component: BranchServiceAreaSearchComponent
        },
        regulatoryAuthorityNumber: {
            autoOpen: false,
            ellipsisTitle: 'Regulatory Authority Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp',
                extraParams: {
                    'search.sortby': 'SeasonalTemplateNumber',
                    'BranchNumber': this.utils.getBranchCode()
                }
            },
            httpConfig: {
                operation: 'Business/iCABSBRegulatoryAuthoritySearch',
                module: 'waste',
                method: 'service-delivery/search'
            },
            tableColumn: [
                { title: 'Regulatory Authority Number', name: 'RegulatoryAuthorityNumber' },
                { title: 'Name', name: 'RegulatoryAuthorityName' },
                { title: 'Max Weight Per Day (KG)', name: 'MaximumWeightPerDay' },
                { title: 'Requires Premises Registration', name: 'RequiresPremiseRegistrationInd' },
                { title: 'Requires Service Cover Waste', name: 'RequiresServiceCoverWasteInd' },
                { title: 'Requires Waste Transfer Notes', name: 'RequiresWasteTransferNotesInd' },
                { title: 'Waste Regulatory Carrier Name', name: 'WasteRegulatoryCarrierName' },
                { title: 'Waste Regulatory Registration Number', name: 'WasteRegulatoryRegistrationNum' }
            ],
            disable: false
        }
    };
    public screenNotReadyComponent = ScreenNotReadyComponent;
    public itemsPerPage: number = 10;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public currDate: Date = null;
    public currDate1: Date = null;

    public controls: Array<Object> = [
        { name: 'BranchName', disabled: false, required: false, value: '', type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: false, required: false, value: '', type: MntConst.eTypeInteger },
        { name: 'BranchServiceAreaCode', disabled: false, required: false, value: '', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: false, required: true, value: '', type: MntConst.eTypeText },
        { name: 'RegulatoryAuthorityNumber', disabled: false, required: false, value: '', type: MntConst.eTypeInteger },
        { name: 'RegulatoryAuthorityName', disabled: false, required: false, value: '', type: MntConst.eTypeText },
        { name: 'DateFrom', disabled: false, required: false, value: '', type: MntConst.eTypeDate },
        { name: 'DateTo', disabled: false, required: false, value: '', type: MntConst.eTypeDate }
    ];

    //Component constructor
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARENVAGENCYQUARTERLYRETURN;
        this.browserTitle = this.pageTitle = 'Environment Agency Quarterly Returns';
    }

    //Lifecycle methods angular starts
    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    ngAfterViewInit(): void {
        this.initData();
    }

    /**
     * Method set DateFrom from Datepicker
     * @param:  event selected date value
     * @return: void
     */
    public formSelectedValue(event: any): void {
        this.setControlValue('DateFrom', event.value);
    }

    /**
     * Method set DateTo from Datepicker
     * @param:  event selected date value
     * @return: void
     */
    public toDateSelectedValue(event: any): void {
        this.setControlValue('DateTo', event.value);
    }

    /**
     * Method to set up grid column
     * @param:  void
     * @return: void
     */
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('BlankColumn', 'EnvAgencyQuarterlyReturn', 'BlankColumn', MntConst.eTypeText, 1);
        this.riGrid.AddColumn('WasteConsignmentNoteNumber', 'EnvAgencyQuarterlyReturn', 'WasteConsignmentNoteNumber', MntConst.eTypeCode, 20);
        this.riGrid.AddColumn('M', 'EnvAgencyQuarterlyReturn', 'M', MntConst.eTypeCode, 1);
        this.riGrid.AddColumn('VisitDueDate', 'EnvAgencyQuarterlyReturn', 'VisitDueDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('WasteRegulatoryPremiseRef', 'EnvAgencyQuarterlyReturn', 'WasteRegulatoryPremiseRef', MntConst.eTypeText, 15);
        this.riGrid.AddColumn('PremisePostcode', 'EnvAgencyQuarterlyReturn', 'PremisePostcode', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('PrimaryEWCCode', 'EnvAgencyQuarterlyReturn', 'PrimaryEWCCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('WasteHazardCodeList', 'EnvAgencyQuarterlyReturn', 'WasteHazardCodeList', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('WastePhysicalFormDescription', 'EnvAgencyQuarterlyReturn', 'WastePhysicalFormDescription', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('TotalWeight', 'EnvAgencyQuarterlyReturn', 'TotalWeight', MntConst.eTypeDecimal2, 10);
        this.riGrid.AddColumn('WasteOperationCode', 'EnvAgencyQuarterlyReturn', 'WasteOperationCode', MntConst.eTypeText, 8);
        this.riGrid.AddColumn('BlankColumn2', 'EnvAgencyQuarterlyReturn', 'BlankColumn2', MntConst.eTypeText, 1);
        this.riGrid.AddColumn('DepotNumber', 'EnvAgencyQuarterlyReturn', 'DepotNumber', MntConst.eTypeInteger, 1);
        this.riGrid.AddColumnScreen('BlankColumn', false);
        this.riGrid.AddColumnScreen('BlankColumn2', false);
        this.riGrid.AddColumnScreen('DepotNumber', false);
        this.riGrid.AddColumnScreen('M', false);
        this.riGrid.AddColumnScreen('M', false);
        this.riGrid.Complete();
    }

    /**
     * Method to call on pageload
     * @param:   void
     * @return: void
     */
    private initData(): void {
        this.setDefaultDate();
        this.riGrid.DefaultBorderColor = 'ADD8E6';
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchText());
        this.riExchange.riInputElement.Disable(this.uiForm, 'BranchName');
        this.riExchange.riInputElement.Disable(this.uiForm, 'EmployeeSurname');
        this.riExchange.riInputElement.Disable(this.uiForm, 'RegulatoryAuthorityName');
        this.buildGrid();
        this.beforeExecute();
    }

    /**
     * Method to call on pagination click
     * @param:   currentPage is the page number as per user clicks
     * @return: void
     */
    public getCurrentPage(currentPage: any): void {
        this.riCacheRefresh = 'False';
        this.currentPage = currentPage.value;
        this.beforeExecute();
    }

    /**
     * Method is called on refresh click
     * @param: void
     * @return: void
     */
    public refresh(): void {
        this.riCacheRefresh = 'True';
        this.beforeExecute();
    }

    /**
     * Method is called before grid execute
     * @param: void
     * @return: void
     */
    private beforeExecute(): void {
        if (this.getControlValue('DateFrom') && this.getControlValue('DateTo')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.countryCode());
            //set parameters
            search.set('BranchNumber', this.getControlValue('BranchNumber'));
            search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
            search.set('RegulatoryAuthorityNumber', this.getControlValue('RegulatoryAuthorityNumber'));
            search.set('SelectDateFrom', this.getControlValue('DateFrom'));
            search.set('SelectDateTo', this.getControlValue('DateTo'));
            if (this.riCacheRefresh === 'True') {
                search.set('riCacheRefresh', this.riCacheRefresh);
            }
            search.set(this.serviceConstants.GridMode, this.utils.randomSixDigitString());
            search.set(this.serviceConstants.GridHandle, this.riGridHandle);
            // set grid building parameters
            search.set(this.serviceConstants.PageSize, this.itemsPerPage.toString());
            search.set(this.serviceConstants.PageCurrent, this.currentPage.toString());
            this.queryParams.search = search;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(AjaxConstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            try {
                                this.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                                this.totalItems = data.pageData ? data.pageData.lastPageNumber * this.itemsPerPage : 1;
                                this.riGrid.UpdateHeader = true;
                                this.riGrid.UpdateBody = true;
                                this.riGrid.UpdateFooter = true;
                                this.riGrid.Execute(data);
                            } catch (e) {
                                this.logger.warn(e);
                            }
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(AjaxConstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    }
                );
        }
    }

    /**
     * Method to set default dates
     * @param:   void
     * @returns: void
     */
    private setDefaultDate(): void {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        //set parameters
        postData[this.serviceConstants.Function] = 'DefaultDates';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(AjaxConstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        try {
                            this.setControlValue('DateFrom', data.SelectDateFrom);
                            this.setControlValue('DateTo', data.SelectDateTo);
                            this.currDate = data.SelectDateFrom;
                            this.currDate1 = data.SelectDateTo;
                        } catch (e) {
                            this.logger.warn(e);
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(AjaxConstant.COMPLETE);
                }
            );
    }

    /**
     * Method to set BranchServiceAreaCode from ellipsis
     * @param:   void
     * @returns: void
     */
    public setServiceAreaReturnData(obj: any): void {
        this.setControlValue('BranchServiceAreaCode', obj.BranchServiceAreaCode);
        this.setControlValue('EmployeeSurname', obj.EmployeeSurname);
    }

    /**
     * Method is called BranchServiceAreaCode on change
     * @param:   void
     * @returns: void
     */
    public branchServiceAreaCodeOnChange(): void {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            //set parameters
            postData[this.serviceConstants.Function] = 'GetEmployeeSurname';
            postData['BranchNumber'] = this.getControlValue('BranchNumber');
            postData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.errorMessage) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            this.setControlValue('EmployeeSurname', '');
                        } else {
                            try {
                                if (data.EmployeeSurname) {
                                    this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                                } else {
                                    this.setControlValue('EmployeeSurname', '');
                                }
                            } catch (e) {
                                this.logger.warn(e);
                            }
                        }
                    },
                    (error) => {
                        this.setControlValue('EmployeeSurname', '');
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    }
                );
        } else {
            this.setControlValue('EmployeeSurname', '');
        }
    }

    public onRegulatoryAuthorityDataReceived(data: any): void {
        if (data) {
            this.setControlValue('RegulatoryAuthorityNumber', data.RegulatoryAuthorityNumber);
            this.setControlValue('RegulatoryAuthorityName', data.RegulatoryAuthorityName);
        }
    }

    /**
     * Method is called regulatoryAuthorityNumber on change
     * @param:   void
     * @returns: void
     */
    public regulatoryAuthorityNumberOnChange(): void {
        if (this.getControlValue('RegulatoryAuthorityNumber')) {
            let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            //set parameters
            postData[this.serviceConstants.Function] = 'GetRegulatoryAuthorityName';
            postData['RegulatoryAuthorityNumber'] = this.getControlValue('RegulatoryAuthorityNumber');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            this.setControlValue('RegulatoryAuthorityName', '');
                        } else {
                            try {
                                if (data.RegulatoryAuthorityName) {
                                    this.setControlValue('RegulatoryAuthorityName', data.RegulatoryAuthorityName);
                                } else {
                                    this.setControlValue('RegulatoryAuthorityName', '');
                                }
                            } catch (e) {
                                this.logger.warn(e);
                            }
                        }
                    },
                    (error) => {
                        this.setControlValue('RegulatoryAuthorityName', '');
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    }
                );
        } else {
            this.setControlValue('RegulatoryAuthorityName', '');
        }
    }

    public onGridRowDblClick(event: any): void {
        //Need to check
    }
    public onGridRowClick(event: any): void {
        //Need to check
    }
}
