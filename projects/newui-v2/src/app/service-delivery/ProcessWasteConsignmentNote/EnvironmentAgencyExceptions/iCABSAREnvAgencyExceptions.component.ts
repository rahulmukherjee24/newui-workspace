import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';

import { PageIdentifier } from '../../../base/PageIdentifier';
import { BaseComponent } from '../../../../app/base/BaseComponent';
import { BranchServiceAreaSearchComponent } from './../../../internal/search/iCABSBBranchServiceAreaSearch';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from '../../../../shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { IGenericEllipsisControl } from '@shared/components/ellipsis-generic/ellipsis-generic';
import { IExportOptions } from '@app/base/ExportConfig';

@Component({
    templateUrl: 'iCABSAREnvAgencyExceptions.html'
})

export class EnvAgencyExceptionsComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private headerParams: any = {
        method: 'service-delivery/grid',
        module: 'waste',
        operation: 'ApplicationReport/iCABSAREnvAgencyExceptions'
    };
    public pageId: string = '';
    public controls = [
        { name: 'BranchNumber', readonly: true, disabled: true, value: this.utils.getBranchText(), type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'SelectDateFrompicker', type: MntConst.eTypeDate, required: true },
        { name: 'SelectDateTopicker', type: MntConst.eTypeDate, required: true },
        { name: 'RegulatoryAuthorityNumber', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'RegulatoryAuthorityName', type: MntConst.eTypeText, disabled: true }
    ];
    public searchModalRoute: string = '';
    public branchServiceAreaSearchComponent = BranchServiceAreaSearchComponent;
    public isVisitError: boolean = false;
    public isCacheRefresh: boolean = false;
    public curPage: any = 1;
    public totalRecords: number;
    public pageSize: any = 10;
    public gridHandle: any = '';
    public DateObj: any = {
        SelectDateFrom: new Date(),
        SelectDateTo: new Date()
    };
    public branchServiceAreaSearchParams: any = {
        parentMode: 'LookUp-Emp',
        showAddNew: false,
        ServiceBranchNumber: this.utils.getBranchText()
    };
    public regulatoryAuthorityNumber: IGenericEllipsisControl = {
        autoOpen: false,
        ellipsisTitle: 'Regulatory Authority Search',
        configParams: {
            table: '',
            shouldShowAdd: false,
            parentMode: 'LookUp',
            extraParams: {
                'search.sortby': 'SeasonalTemplateNumber',
                'BranchNumber': this.utils.getBranchCode()
            }
        },
        httpConfig: {
            operation: 'Business/iCABSBRegulatoryAuthoritySearch',
            module: 'waste',
            method: 'service-delivery/search'
        },
        tableColumn: [
            { title: 'Regulatory Authority Number', name: 'RegulatoryAuthorityNumber' },
            { title: 'Name', name: 'RegulatoryAuthorityName' },
            { title: 'Max Weight Per Day (KG)', name: 'MaximumWeightPerDay' },
            { title: 'Requires Premises Registration', name: 'RequiresPremiseRegistrationInd' },
            { title: 'Requires Service Cover Waste', name: 'RequiresServiceCoverWasteInd' },
            { title: 'Requires Waste Transfer Notes', name: 'RequiresWasteTransferNotesInd' },
            { title: 'Waste Regulatory Carrier Name', name: 'WasteRegulatoryCarrierName' },
            { title: 'Waste Regulatory Registration Number', name: 'WasteRegulatoryRegistrationNum' }
        ],
        disable: false
    };
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public expCfg: IExportOptions = {};

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARENVAGENCYEXCEPTIONS;
        this.pageTitle = 'Environment Agency Exceptions';
        this.browserTitle = this.pageTitle;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    ngOnDestroy(): void {
        this.serviceConstants = null;
    }
    ngAfterContentInit(): void {
        this.riGrid.PageSize = 10;
        this.riGrid.DefaultBorderColor = 'DDDDDD';
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateFooter = true;
        this.riGrid.UpdateBody = true;
        this.setDate();
        this.buildGrid();
    }

    private setDate(): void {
        this.DateObj.SelectDateFrom = '';
        this.DateObj.SelectDateTo = '';
        let urlParams: QueryParams = this.getURLSearchParamObject();
        let formData: any = {
            Function: 'DefaultDates'
        };
        let date: any = new Date();
        urlParams.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, urlParams, formData)
            .subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('SelectDateFrompicker', this.utils.formatDate(data.SelectDateFrom));
                    this.setControlValue('SelectDateTopicker', this.utils.formatDate(data.SelectDateTo));
                }
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }
    private buildGrid(): any {
        this.riGrid.Clear();
        this.riGrid.AddColumn('BlankColumn','EnvAgencyExceptions','BlankColumn',MntConst.eTypeText,1);
        this.riGrid.AddColumnScreen('BlankColumn',false);
        this.riGrid.AddColumn('WasteConsignmentNoteNumber', 'EnvAgencyExceptions', 'WasteConsignmentNoteNumber', MntConst.eTypeCode, 20);
        this.riGrid.AddColumn('M','EnvAgencyExceptions','M',MntConst.eTypeCode,1);
        this.riGrid.AddColumnScreen('M',false);
        this.riGrid.AddColumn('VisitDueDate', 'EnvAgencyExceptions', 'VisitDueDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('Reason', 'EnvAgencyExceptions', 'Reason', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('WasteRegulatoryPremiseRef', 'EnvAgencyExceptions', 'WasteRegulatoryPremiseRef', MntConst.eTypeText, 15);
        this.riGrid.AddColumn('PremisePostcode', 'EnvAgencyExceptions', 'PremisePostcode', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('PrimaryEWCCode', 'EnvAgencyExceptions', 'PrimaryEWCCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('WasteHazardCodeList', 'EnvAgencyExceptions', 'WasteHazardCodeList', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('WastePhysicalFormDescription', 'EnvAgencyExceptions', 'WastePhysicalFormDescription', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('TotalWeight', 'EnvAgencyExceptions', 'TotalWeight', MntConst.eTypeDecimal2, 10);
        this.riGrid.AddColumn('WasteOperationCode', 'EnvAgencyExceptions', 'WasteOperationCode', MntConst.eTypeText, 8);
        this.riGrid.AddColumn('ContractPremise', 'EnvAgencyExceptions', 'ContractPremise', MntConst.eTypeText, 14);
        this.riGrid.AddColumn('PremiseAddressLine1', 'EnvAgencyExceptions', 'PremiseAddressLine1', MntConst.eTypeText, 40);
        this.riGrid.Complete();
        this.expCfg.dateColumns = this.riGrid.getColumnIndexListFromFull(['VisitDueDate']);
    }
    private riGridBeforeExecute(): void {
        if (this.uiForm.valid) {
            let date: any = new Date(),
                search: QueryParams = this.getURLSearchParamObject();
            this.isVisitError = false;
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'TotalVisits', false);
            this.riGrid.UpdateBody = true;
            this.riGrid.UpdateFooter = true;
            this.riGrid.UpdateHeader = true;
            search.set('BranchNumber', this.utils.getBranchCode());
            search.set(this.serviceConstants.Action, '2');
            search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
            search.set('RegulatoryAuthorityNumber', this.getControlValue('RegulatoryAuthorityNumber'));
            search.set('SelectDateFrom', this.getControlValue('SelectDateFrompicker'));
            search.set('SelectDateTo', this.getControlValue('SelectDateTopicker'));
            search.set('riGridMode', '0');
            if (this.isCacheRefresh) {
                search.set('riCacheRefresh', 'True');
                this.isCacheRefresh = false;
            }
            search.set('riGridHandle', this.gridHandle);
            search.set('riAppName', 'riControlGrid');
            search.set('HeaderClickedColumn', '');
            search.set('riSortOrder', 'Ascending');
            search.set(this.serviceConstants.GridPageSize, this.pageSize);
            search.set(this.serviceConstants.GridPageCurrent, this.curPage);
            search.set('DTE', this.utils.formatDate(date));
            search.set('TME', date.getTime());
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search).subscribe(
                (data) => {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.curPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageSize : 1;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.Execute(data);
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
        }
    }

    public onBranchServiceAreaSearchReceived(obj: any): void {
        if (obj) {
            this.setControlValue('BranchServiceAreaCode', obj.BranchServiceAreaCode);
            this.setControlValue('EmployeeSurname', obj.EmployeeSurname);
        }
    }
    public onRegulatoryAuthoritySearchReceived(obj: any): void {
        if (obj) {
            this.setControlValue('RegulatoryAuthorityNumber', obj.RegulatoryAuthorityNumber);
            this.setControlValue('RegulatoryAuthorityName', obj.RegulatoryAuthorityName);
        }
    }
    public getCurrentPage(currentPage: any): void {
        this.curPage = currentPage.value;
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateRow = true;
        this.riGrid.UpdateFooter = true;
        this.riGridBeforeExecute();
    }
    public refresh(): void {
        this.isCacheRefresh = true;
        this.gridHandle = this.utils.randomSixDigitString();
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
    public getEmployeeSurname(): void {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let search: QueryParams = this.getURLSearchParamObject(),
                formData: any = {
                    Function: 'GetEmployeeSurname'
                };
            search.set(this.serviceConstants.Action, '6');
            search.set('BranchNumber', this.utils.getBranchCode());
            search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            this.setControlValue('EmployeeSurname', '');
                        } else {
                            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        } else {
            this.setControlValue('EmployeeSurname', '');
        }
    }
    public getRegulatoryAuthorityName(): void {
        if (this.getControlValue('RegulatoryAuthorityNumber')) {
            let search: QueryParams = this.getURLSearchParamObject(),
                formData: any = {
                    Function: 'GetRegulatoryAuthorityName'
                };
            search.set(this.serviceConstants.Action, '6');
            search.set('BranchNumber', this.utils.getBranchCode());
            search.set('RegulatoryAuthorityNumber', this.getControlValue('RegulatoryAuthorityNumber'));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            this.setControlValue('RegulatoryAuthorityName', '');
                        } else {
                            this.setControlValue('RegulatoryAuthorityName', data.RegulatoryAuthorityName);
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        } else {
            this.setControlValue('RegulatoryAuthorityName', '');
        }
    }
}
