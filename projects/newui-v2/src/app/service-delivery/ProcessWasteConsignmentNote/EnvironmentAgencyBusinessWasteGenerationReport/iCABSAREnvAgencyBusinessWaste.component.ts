import { QueryParams } from '@shared/services/http-params-wrapper';
/**
 * The SeServiceAreaDetailGridComponent  to display service area grid
 * @author icabs ui team
 * @version 1.0
 * @since   11/01/2018
 */
import { Component } from '@angular/core';
import { OnInit, Injector, OnDestroy, ViewChild } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { BranchSearchComponent } from './../../../internal/search/iCABSBBranchSearch';
import { EllipsisComponent } from '../../../../shared/components/ellipsis/ellipsis';
import { PremiseSearchComponent } from './../../../internal/search/iCABSAPremiseSearch';
import { ContractSearchComponent } from './../../../internal/search/iCABSAContractSearch';
import { AjaxConstant } from '../../../../shared/constants/AjaxConstants';
import { DatepickerComponent } from '../../../../shared/components/datepicker/datepicker';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { DropdownStaticComponent } from '../../../../shared/components/dropdown-static/dropdownstatic';

@Component({
    templateUrl: 'iCABSAREnvAgencyBusinessWaste.html'
})

export class AREnvAgencyBusinessWasteComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('newBranchNumberSearch') newBranchNumberSearch: BranchSearchComponent;
    @ViewChild('contractSearchEllipsis') public contractSearchEllipsis: EllipsisComponent;
    @ViewChild('premiseSearchEllipsis') public premiseSearchEllipsis: EllipsisComponent;
    @ViewChild('datePickerDateFrom') public datePickerDateFrom: DatepickerComponent;
    @ViewChild('datePickerDateTo') public datePickerDateTo: DatepickerComponent;
    @ViewChild('repDestDropdown') public repDestDropdown: DropdownStaticComponent;

    private queryParams: any = {
        operation: 'ApplicationReport/iCABSAREnvAgencyBusinessWaste',
        module: 'waste',
        method: 'service-delivery/grid'
    };
    public searchModalRoute: string = '';
    public isBranchDisable: boolean = false;
    public pageId: string = '';
    public search: QueryParams;
    public currDate: Date;
    public currDate1: Date;
    public inputParams: any = { 'parentMode': 'NewBranch' };
    public contractSearchComponent = ContractSearchComponent;
    public premiseSearchComponent: any = PremiseSearchComponent;
    public isThInformationHide: boolean = true;
    public contractSearchParams: any = {
        'parentMode': 'LookUp',
        'showAddNew': false
    };
    public inputParamsPremise: any = {
        'parentMode': 'LookUp'
    };
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public repDestOptions: Array<any> = [{ value: 'direct', text: 'Report Viewer' }];
    public controls: Array<Object> = [
        { name: 'BusinessCode', disabled: false, required: false, value: '' },
        { name: 'BusinessDesc', disabled: false, required: false, value: '' },
        { name: 'ContractNumber', disabled: false, required: false, value: '', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ContractName', disabled: false, required: true, value: '', type: MntConst.eTypeText },
        { name: 'PremiseNumber', disabled: false, required: false, value: '', type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: false, required: false, value: '', type: MntConst.eTypeText },
        { name: 'DateFrom', disabled: false, required: false, value: '', type: MntConst.eTypeDate },
        { name: 'DateTo', disabled: false, required: false, value: '', type: MntConst.eTypeDate },
        { name: 'BranchNumber', disabled: false, required: false, value: '' },
        { name: 'BranchName', disabled: false, required: false, value: '' },
        { name: 'RepDest', disabled: false, required: false, value: '' }
    ];
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARENVAGENCYBUSINESSWASTE;
        this.browserTitle = this.pageTitle = 'Environment Agency Business Waste Generation Report';
    }
    ngOnInit(): void {
        super.ngOnInit();
        this.initData();
    }
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * Method to call on pageload
     * @param:  void
     * @returns: void
     */
    private initData(): void {
        let dt: Date = new Date(), branchArr: Array<string>;
        this.riExchange.riInputElement.Disable(this.uiForm, 'BusinessDesc');
        this.riExchange.riInputElement.Disable(this.uiForm, 'BranchName');
        this.riExchange.riInputElement.Disable(this.uiForm, 'ContractName');
        this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseName');
        this.isBranchDisable = false;
        branchArr = this.utils.getBranchText().split(' - ');
        this.newBranchNumberSearch.active = { id: this.utils.getBranchCode(), text: this.utils.getBranchCode() + ' - ' + branchArr[1] };
        this.setControlValue('BusinessCode', this.businessCode());
        this.setControlValue('BusinessDesc', this.utils.getBusinessText(this.businessCode(), this.countryCode()));
        this.setControlValue('BranchName', branchArr[1]);
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.currDate = new Date(dt.getFullYear(), dt.getMonth(), 1);
        this.currDate1 = new Date(dt.getFullYear(), dt.getMonth() + 1, 0);
        this.setControlValue('DateFrom', this.currDate.getDay() + '/' + dt.getMonth() + '/' + dt.getFullYear());
        this.setControlValue('DateTo', this.currDate1.getDay() + '/' + dt.getMonth() + 1 + '/' + dt.getFullYear());
        this.inputParamsPremise.currentContractType = this.riExchange.getCurrentContractType();
        this.inputParamsPremise.currentContractTypeURLParameter = this.riExchange.getCurrentContractTypeUrlParam();
        this.contractSearchParams.currentContractType = this.riExchange.getCurrentContractType();
        this.contractSearchParams.currentContractTypeURLParameter = this.riExchange.getCurrentContractTypeUrlParam();
    }

    /**
     * Datepicker select method
     */
    public formSelectedValue(event: any): void {
        this.setControlValue('DateFrom', event.value);
    }

    /**
     * Datepicker select method
     */
    public toDateSelectedValue(event: any): void {
        this.setControlValue('DateTo', event.value);
    }
    /**
     * Method to get selected dropdown report
     * @param: event-report dropdown selected value
     * @return: void
     */
    public selectedReport(report: any): void {
        this.setControlValue('RepDest', report);
    }

    /**
     * Method to get branchcode onchange dropdown
     * @param: obj-  branch dropdown selected value
     * @return: void
     */
    public onBranchDataReceived(obj: any): void {
        this.setControlValue('BranchNumber', obj.BranchNumber);
        this.setControlValue('BranchName', obj.BranchName);
    }
    /**
     * Method to get premise data
     * @param: obj:  premise ellipsis received value
     * @return: void
     */
    public onPremiseDataReceived(obj: any): void {
        this.setControlValue('PremiseNumber', obj.PremiseNumber);
        this.setControlValue('PremiseName', obj.PremiseName);
        this.populateDescription();
    }

    /**
     * Method to get contract data
     * @param: obj:  contract ellipsis received value
     * @return: void
     */
    public onContractDataReceived(obj: any): void {
        this.inputParamsPremise.ContractNumber = obj.ContractNumber;
        this.inputParamsPremise.ContractName = obj.ContractName;
        this.setControlValue('ContractNumber', obj.ContractNumber);
        this.setControlValue('ContractName', obj.ContractName);
        this.populateDescription();
    }

    /**
     * Method to populate description
     * @param: void
     * @return: void
     */
    public populateDescription(): void {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject();
        postData['Mode'] = 'SetDisplayFields';
        if (this.getControlValue('BranchNumber'))
            postData['BranchNumber'] = this.getControlValue('BranchNumber');
        if (this.getControlValue('ContractNumber'))
            postData['ContractNumber'] = this.getControlValue('ContractNumber');
        if (this.getControlValue('PremiseNumber'))
            postData['PremiseNumber'] = this.getControlValue('PremiseNumber');
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(AjaxConstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                try {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                    } else {
                        this.setControlValue('ContractName', data.ContractName);
                        this.setControlValue('PremiseName', data.PremiseName);
                        this.setControlValue('BranchName', data.BranchName);
                        this.inputParamsPremise.ContractNumber = this.getControlValue('ContractNumber');
                        this.inputParamsPremise.ContractName = this.getControlValue('ContractName');
                    }
                } catch (error) {
                    this.logger.warn(error);
                }
            },
            (error) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * Method is called on change value of contract number
     * @param: void
     * @return: void
     */
    public contractChange(): void {
        if (this.getControlValue('ContractNumber')) {
            this.setControlValue('ContractNumber', this.utils.numberPadding(this.getControlValue('ContractNumber'), 8));
            this.populateDescription();
        } else {
            this.setControlValue('ContractName', '');
        }
    }

    /**
     * Method is called on change value of Premise Number
     * @param: void
     * @return: void
     */
    public premiseChange(): void {
        if (this.getControlValue('PremiseNumber') && this.getControlValue('ContractNumber')) {
            this.populateDescription();
        } else {
            this.setControlValue('PremiseName', '');
        }
    }

    /**
     * Method is called on submit button click
     * @param: void
     * @return: void
     */
    public cmdSubmitOnclick(): void {
        if (this.getControlValue('DateFrom') && this.getControlValue('BusinessCode') && this.getControlValue('DateTo') && this.getControlValue('BranchNumber')) {
            this.submitReportRequest();
        }
    }

    /**
     * Method is triggered service call on submit button click
     * @param: void
     * @return: void
     */
    public submitReportRequest(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        if (this.getControlValue('ContractNumber'))
            search.set('ContractNumber', this.getControlValue('ContractNumber'));
        if (this.getControlValue('PremiseNumber'))
            search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        search.set('DateFrom', this.getControlValue('DateFrom'));
        search.set('DateTo', this.getControlValue('DateTo'));
        search.set('BranchNumber', this.getControlValue('BranchNumber'));
        search.set('RepManDest', 'batch|ReportID');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                try {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                    }
                    else {
                        this.isThInformationHide = false;
                        document.querySelector('#thInformation').innerHTML = data.ReturnHTML;
                    }
                } catch (error) {
                    this.logger.warn(error);
                }

            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }
}
//End class

