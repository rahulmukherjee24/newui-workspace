import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, OnInit, OnDestroy, Injector, ViewChild, ChangeDetectorRef, EventEmitter } from '@angular/core';

import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { MessageConstant } from '../../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { EmployeeSearchComponent } from './../../../internal/search/iCABSBEmployeeSearch';
import { ContractSearchComponent } from '../../../internal/search/iCABSAContractSearch';

@Component({
    templateUrl: 'iCABSSeHCARiskAssessmentGrid.html'
})

export class HCARiskAssessmentGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private queryParams: any = {
        operation: 'Service/iCABSSeHCARiskAssessmentGrid',
        module: 'pda',
        method: 'service-delivery/maintenance'
    };
    private errorMessageDesc: string = '';
    private currentContractTypeURLParameter: string = '';

    public setFocusEmployeeCode = new EventEmitter<boolean>();
    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'EmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeText },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', type: MntConst.eTypeText }
    ];
    public gridConfig: any = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 1
    };
    public ellipsisParams: any = {
        employee: {
            isShowCloseButton: true,
            isShowHeader: true,
            isDisabled: false,
            childConfigParams: {
                parentMode: 'LookUp'
            },
            contentComponent: EmployeeSearchComponent
        },
        contractName: {
            isShowCloseButton: true,
            isShowHeader: true,
            isDisabled: false,
            childConfigParams: {
                parentMode: 'LookUp'
            },
            contentComponent: ContractSearchComponent
        },
        common: {
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            }
        }
    };

    constructor(injector: Injector, private ref: ChangeDetectorRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEHCARISKASSESSMENTGRID;

        this.browserTitle = this.pageTitle = 'Premises Site Risk Assessment Update';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // Initializes data into different controls on page load
    private onWindowLoad(): void {
        this.buildGrid();
        this.disableControl('EmployeeSurname', true);
        this.disableControl('ContractName', true);
        setTimeout(() => {
            this.setFocusEmployeeCode.emit(true);
        }, 0);
    }

    // Builds the structure of the grid
    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('EmployeeCode', 'Update', 'EmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EmployeeSurname', 'Update', 'EmployeeSurname', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContractNumber', 'Update', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNumber', 'Update', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'Update', 'PremiseName', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('RiskAssessmentText', 'Update', 'RiskAssessmentText', MntConst.eTypeText, 100);

        this.riGrid.AddColumn('Updated', 'Update', 'Updated', MntConst.eTypeImage, 1, false, 'Click here to update');
        this.riGrid.AddColumnAlign('Updated', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('EmployeeCode', true);
        this.riGrid.AddColumnOrderable('ContractNumber', true);

        this.riGrid.Complete();
    }

    // Populate data into the grid
    private populateGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject();

        search.set('BranchNumber', this.utils.getBranchCode());
        search.set(this.serviceConstants.EmployeeCode, this.getControlValue('EmployeeCode'));
        search.set(this.serviceConstants.ContractNumber, this.getControlValue('ContractNumber'));

        // set grid building parameters
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.PageSize, this.gridConfig.pageSize.toString());
        search.set(this.serviceConstants.PageCurrent, this.gridConfig.currentPage.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.Action, '2');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGrid.RefreshRequired();
                    this.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                    this.riGrid.Execute(data);
                    this.ref.detectChanges();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // Populate EmployeeSurname & ContractName
    private populateDescriptions(): void {
        let formData: any = {};
        let search: QueryParams = this.getURLSearchParamObject();

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'GetDescriptions';
        formData['BranchNumber'] = this.utils.getBranchCode();
        if (this.getControlValue('EmployeeCode')) {
            formData[this.serviceConstants.EmployeeCode] = this.getControlValue('EmployeeCode');
        }
        if (this.getControlValue('ContractNumber')) {
            formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                        this.setControlValue('ContractName', data.ContractName);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    // Updates the pagelevel attributes on grid row activity
    private onUpdateFocus(rsrcElement: any): void {
        let oTR = rsrcElement.parentElement.parentElement.parentElement;
        rsrcElement.focus();

        this.setAttribute('HCARiskAssessmentRowID', this.riGrid.Details.GetAttribute('Updated', 'RowID'));
        this.setAttribute('Row', oTR.sectionRowIndex);

        if (this.riGrid.Details.GetAttribute('EmployeeCode', 'AdditionalProperty') === 'No Error') {
            this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('Updated', 'AdditionalProperty'));
            switch (this.riGrid.Details.GetAttribute('ContractNumber', 'AdditionalProperty')) {
                case 'C':
                    this.currentContractTypeURLParameter = '';
                    break;
                case 'J':
                    this.currentContractTypeURLParameter = '<job>';
                    break;
                case 'P':
                    this.currentContractTypeURLParameter = '<product>';
                    break;
            }
        }
        else {
            this.errorMessageDesc = this.riGrid.Details.GetAttribute('EmployeeCode', 'AdditionalProperty');
        }
    }

    // Callback to retrieve the current page on user clicks
    public getCurrentPage(currentPage: any): void {
        this.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh();
    }

    // Refresh the grid data on user click
    public onRiGridRefresh(): void {
        if (this.gridConfig.currentPage <= 0) {
            this.gridConfig.currentPage = 1;
        }
        this.populateGrid();
    }

    // Populate data from ellipsis
    public onEllipsisDataReceived(type: string, data: any): void {
        switch (type) {
            case 'Employee':
                this.setControlValue('EmployeeCode', data.EmployeeCode || '');
                this.setControlValue('EmployeeSurname', data.EmployeeSurname || '');
                break;
            case 'Contract':
                this.setControlValue('ContractNumber', data.ContractNumber || '');
                this.setControlValue('ContractName', data.ContractName || '');
                break;
            default:
        }
    }

    // EmployeeCode OnChange event
    public onEmployeeCodeChange(event: Event): void {
        if (this.getControlValue('EmployeeCode').toString().trim().length === 0) {
            this.setControlValue('EmployeeSurname', '');
        }
        else {
            this.populateDescriptions();
        }
    }

    // public ContractNumber OnChange event
    public onContractNumberChange(event: Event): void {
        if (this.getControlValue('ContractNumber').toString().trim().length === 0) {
            this.setControlValue('ContractName', '');
        }
        else {
            this.populateDescriptions();
        }
    }

    // Grid body on double click
    public onRiGridBodyDblClick(event: any): void {
        if (this.riGrid.CurrentColumnName === 'Updated') {
            this.onUpdateFocus(event.srcElement);

            if (this.errorMessageDesc !== '') {
                this.modalAdvService.emitError(new ICabsModalVO(this.errorMessageDesc));
                this.errorMessageDesc = '';
            }
            else {
                // ToDo
                // this.navigate('Update', '', { CurrentContractTypeURLParameter: this.currentContractTypeURLParameter })
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotDeveloped));
            }
        }
    }

    // Handles grid sort functionality
    public onRiGridSort(event: any): void {
        this.onRiGridRefresh();
    }
}
