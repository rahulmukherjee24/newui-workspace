import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { AppModuleRoutes, ContractManagementModuleRoutes, InternalMaintenanceServiceModuleRoutes } from '../../../base/PageRoutes';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { BranchSearchComponent } from '../../../../app/internal/search/iCABSBBranchSearch';
import { PaginationComponent } from '../../../../shared/components/pagination/pagination';
import { GridAdvancedComponent } from '../../../../shared/components/grid-advanced/grid-advanced';
import { EmployeeSearchComponent } from './../../../internal/search/iCABSBEmployeeSearch';
import { OccupationSearchComponent } from './../../../internal/search/iCABSSOccupationSearch.component';
import { DropdownStaticComponent } from '../../../../shared/components/dropdown-static/dropdownstatic';
import { IExportOptions } from '@app/base/ExportConfig';

@Component({
    templateUrl: 'iCABSSeServiceVisitRejectionsGrid.html'
})

export class ServiceVisitRejectionsGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;
    @ViewChild('visitTypeSearch') public visitTypeSearch;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riPagination') riPagination: PaginationComponent;
    @ViewChild('showTypeOptionDropdown') showTypeOptionDropdown: DropdownStaticComponent;

    // URL Query Parameters
    private queryParams: Object = {
        operation: 'Service/iCABSSeServiceVisitRejectionsGrid',
        module: 'service',
        method: 'service-delivery/maintenance'
    };

    public parentMode: string;
    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger },
        { name: 'BranchNumber', type: MntConst.eTypeInteger },
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true },
        { name: 'EmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'OccupationCode', type: MntConst.eTypeCode },
        { name: 'OccupationDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'VisitTypeCode', type: MntConst.eTypeCode },
        { name: 'VisitTypeDesc', type: MntConst.eTypeText },
        { name: 'BranchServiceArea' },
        { name: 'ShowType' }
    ];

    //Branch Search
    public inputParams: any = {
        branchParams: {
            'parentMode': 'LookUp'
        }
    };

    //Visit type Search
    public dropDown: any = {
        menu: [],
        VisitTypeCode: {
            isRequired: false,
            triggerValidate: false,
            isDisabled: false,
            active: { id: '', text: '' },
            arrData: [],
            inputParams: {
                parentMode: 'LookUp'
            }
        }
    };

    // Ellipsis Component
    public ellipsisConfig: any = {
        employee: {
            showCloseButton: true,
            showHeader: true,
            disabled: false,
            childConfigParams: {
                parentMode: 'LookUp'
            },
            component: EmployeeSearchComponent
        },
        occupation: {
            showHeader: true,
            showCloseButton: true,
            disabled: false,
            childConfigParams: {
                'parentMode': 'LookUp-Service'
            },
            component: OccupationSearchComponent
        }
    };

    //Show type
    public showText: Array<any> = [
        'All',
        'Contracts',
        'Jobs',
        'PNOL'
    ];
    public showValue: Array<any> = [
        'All',
        'Contract',
        'Job',
        'PNOL'
    ];

    //Pagination component
    public gridParams: any = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0
    };

    //Global variables
    public Mode: string;
    public isContractNumber: boolean = false;
    public isHidePagination: boolean = true;
    public reqPrepUsed: string;
    public reqInfestation: string;
    public showTypeOptionList: Array<Object> = [];
    public isFlag: boolean = false;
    public contractRowID: any;
    public premiseRowID: any;
    public serviceCoverRowID: any;
    public isRequiredfield: boolean = false;
    public validateUserValue: boolean = false;
    public branchSelected: Object = {
        id: '',
        text: ''
    };
    public exportConfig: IExportOptions = {};

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEVISITREJECTIONSGRID;
        this.browserTitle = this.pageTitle = 'Service Visit Rejections';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.parentMode = this.riExchange.getParentMode();
        this.getSysCharDetails();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    ngAfterViewInit(): void {
        if (this.riExchange.URLParameterContains('Allbranches')) {
            this.branchSearchDropDown.isDisabled = false;
        }
        this.branchSearchDropDown.active = {
            id: this.utils.getBranchCode(),
            text: this.utils.getBranchText()
        };
        this.setControlValue('BranchNumber', this.branchSearchDropDown.active.id);
        this.setControlValue('BranchName', this.branchSearchDropDown.active.text);
        this.visitTypeSearch.triggerDataFetch(this.dropDown.VisitTypeCode.inputParams);
        this.showTypeOptionDropdown.selectedItem = 'All';
        if (this.showValue.length > 0) {
            for (let i = 0; i < this.showValue.length; i++) {
                this.showTypeOptionList.push({
                    text: this.showText[i],
                    value: this.showValue[i]
                });
            }
        }
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.showTypeOptionDropdown.selectedItem = this.pageParams.showTypeDropdownValue;
            if (!this.branchSearchDropDown.isDisabled) {
                this.branchSelected = {
                    id: this.getControlValue('BranchNumber'),
                    text: this.getControlValue('BranchNumber') + ' - ' + this.getControlValue('BranchName')
                };
            }
            this.dropDown.VisitTypeCode.active = {
                id: this.getControlValue('VisitTypeCode'),
                text: this.getControlValue('VisitTypeDesc')
            };
            if (this.parentMode === 'Debrief' || this.parentMode === 'DebriefAction') {
                this.disableControl('EmployeeCode', true);
                this.disableControl('OccupationCode', true);
                this.disableControl('VisitTypeCode', true);
                this.disableControl('DateFrom', true);
                this.disableControl('DateTo', true);
                this.disableControl('ContractNumber', true);
                this.disableControl('PremiseNumber', true);
                this.dropDown.VisitTypeCode.isDisabled = true;
                this.ellipsisConfig.employee.disabled = true;
                this.ellipsisConfig.occupation.disabled = true;
                if (this.parentMode === 'DebriefAction')
                    this.Mode = 'Debrief';
            }
            this.gridParams['pageCurrent'] = this.pageParams.gridCurrentPage;
            this.buildGrid();
            this.isReturningFlag = false;
        }
        else
            this.windowOnLoad();
    }

    private setTimeFunction(): any {
        this.setVariables();
        setTimeout(() => {
            if ((this.getControlValue('DateFrom') !== '') && (this.getControlValue('DateTo') !== '')) {
                this.buildGrid();
                this.isFlag = false;
            }
        }, 3000);
    }

    private getSysCharDetails(): any {
        let sysCharNumbers: number[] = [
            this.sysCharConstants.SystemCharEnableOutcards,
            this.sysCharConstants.SystemCharShowWasteConsNumInVisitEntry,
            this.sysCharConstants.SystemCharEnableVisitTriggerVerification
        ];
        let sysCharIp: Object = {
            module: this.queryParams['module'],
            operation: this.queryParams['operation'],
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharNumbers.toString()
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.speedScript.sysChar(sysCharIp).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    let record: any = data.records;
                    let recordlength: number = record.length;
                    if (data && recordlength > 0) {
                        this.pageParams.vSCEnableOutcards = record[0]['Required'];
                        this.pageParams.vSCShowConsNote = record[1]['Required'];
                        this.pageParams.vSCEnableVerification = record[2]['Required'];
                    }
                    if (this.pageParams.vSCEnableOutcards) {
                        this.showTypeOptionList.push({
                            text: 'Outcards',
                            value: 'Outcard'
                        });
                    }
                    if (this.pageParams.vSCEnableVerification) {
                        this.showTypeOptionList.push({
                            text: 'Verification',
                            value: 'Verification'
                        });
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private windowOnLoad(): void {
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.FunctionUpdateSupport = true;
        let firstDay: any;
        let lastDay: any;
        let date = new Date();
        if (this.parentMode === 'Debrief' || this.parentMode === 'DebriefAction') {
            this.isContractNumber = true;
            this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode') || this.riExchange.getParentAttributeValue('EmployeeCode'));
            this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname') || this.riExchange.getParentAttributeValue('EmployeeSurname'));
            this.setControlValue('ContractNumber', this.riExchange.getParentAttributeValue('ContractNumber'));
            this.setControlValue('PremiseNumber', this.riExchange.getParentAttributeValue('PremiseNumber'));
            this.setControlValue('BranchServiceArea', this.riExchange.getParentAttributeValue('BranchServiceAreaCode'));
            if (this.parentMode === 'DebriefAction') {
                if (this.riExchange.getParentAttributeValue('PlannedVisitDate')) {
                    this.setControlValue('DateFrom', this.riExchange.getParentAttributeValue('PlannedVisitDate'));
                    this.setControlValue('DateTo', this.riExchange.getParentAttributeValue('PlannedVisitDate'));
                }
                else {
                    setTimeout(() => {
                        this.utils.setDatePickerCustomError('DateFrom', true);
                        this.utils.setDatePickerCustomError('DateTo', true);
                    }, 0);
                }
                this.Mode = 'Debrief';
                this.setTimeFunction();
            }
            else {
                if (this.riExchange.getParentAttributeValue('ActualVisitDate')) {
                    this.setControlValue('DateFrom', this.riExchange.getParentHTMLValue('ActualVisitDate'));
                    this.setControlValue('DateTo', this.riExchange.getParentHTMLValue('ActualVisitDate'));
                }
                else {
                    setTimeout(() => {
                        this.utils.setDatePickerCustomError('DateFrom', true);
                        this.utils.setDatePickerCustomError('DateTo', true);
                    }, 0);
                }
                this.setTimeFunction();
            }
            this.disableControl('EmployeeCode', true);
            this.disableControl('OccupationCode', true);
            this.disableControl('VisitTypeCode', true);
            this.disableControl('DateFrom', true);
            this.disableControl('DateTo', true);
            this.disableControl('ContractNumber', true);
            this.disableControl('PremiseNumber', true);
            this.dropDown.VisitTypeCode.isDisabled = true;
            this.ellipsisConfig.employee.disabled = true;
            this.ellipsisConfig.occupation.disabled = true;
        }
        else {
            firstDay = this.globalize.parseDateToFixedFormat(new Date(date.getFullYear(), date.getMonth(), 1)).toString();
            lastDay = this.globalize.parseDateToFixedFormat(this.utils.TodayAsDDMMYYYY()).toString();
            this.setControlValue('DateFrom', this.globalize.parseDateStringToDate(firstDay));
            this.setControlValue('DateTo', this.globalize.parseDateStringToDate(lastDay));
        }
        this.setVariables();
    }

    private setVariables(): void {
        let searchParams: QueryParams;
        let postData: Object = {};
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        postData['ActionType'] = 'SetVariables';
        //making post request
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        this.reqPrepUsed = data.ReqPrepUsed;
                        this.reqInfestation = data.ReqInfestation;
                        this.isFlag = true;
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private buildGrid(): void {
        this.riGrid.PageSize = 10;
        this.riGrid.AddColumn('gridEmployeeCode', 'VisitRejections', 'gridEmployeeCode', MntConst.eTypeCode, 8, true);
        this.riGrid.AddColumnAlign('gridEmployeeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceDateStart', 'VisitRejections', 'ServiceDateStart', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ActivityStatusErrorMessage', 'VisitRejections', 'ActivityStatusErrorMessage', MntConst.eTypeText, 30);
        if (this.pageParams.vSCShowConsNote) {
            this.riGrid.AddColumn('ConsNoteNumber', 'VisitRejections', 'ConsNoteNumber', MntConst.eTypeCode, 8);
            this.riGrid.AddColumnAlign('ConsNoteNumber', MntConst.eAlignmentCenter);
        }
        if (this.pageParams.vSCEnableOutcards) {
            this.riGrid.AddColumn('OutcardNumber', 'VisitRejections', 'OutcardNumber', MntConst.eTypeCode, 8);
            this.riGrid.AddColumnAlign('OutcardNumber', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('ContractNumber', 'VisitRejections', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseNumber', 'VisitRejections', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ProductCode', 'VisitRejections', 'ProductCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchServiceArea', 'VisitRejections', 'BranchServiceArea', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('BranchServiceArea', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('VisitTypeCode', 'VisitRejections', 'VisitTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('StandardDuration', 'VisitRejections', 'StandardDuration', MntConst.eTypeTime, 6);
        this.riGrid.AddColumnAlign('StandardDuration', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('OvertimeDuration', 'VisitRejections', 'OvertimeDuration', MntConst.eTypeTime, 6);
        this.riGrid.AddColumnAlign('OvertimeDuration', MntConst.eAlignmentCenter);
        if (this.reqPrepUsed === 'yes')
            this.riGrid.AddColumn('PrepUsedText', 'VisitRejections', 'PrepUsedText', MntConst.eTypeText, 5);
        if (this.reqInfestation === 'yes')
            this.riGrid.AddColumn('InfestationText', 'VisitRejections', 'InfestationText', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('StatusDesc', 'VisitRejections', 'StatusDesc', MntConst.eTypeText, 10);
        this.riGrid.Complete();
        this.loadData();
        this.dateCollist = this.riGrid.getColumnIndexListFromFull([
            'ServiceDateStart'
        ]);
        this.durationCollist = this.riGrid.getColumnIndexListFromFull([
            'StandardDuration',
            'OvertimeDuration'
        ]);

        if (this.dateCollist.length) {
            this.exportConfig.dateColumns = this.dateCollist;
        }
        if (this.durationCollist.length) {
            this.exportConfig.durationColumns = this.durationCollist;
        }
    }

    public loadData(): void {
        this.riGrid.UpdateBody = true;
        this.riGrid.UpdateBody = true;
        this.riGrid.UpdateHeader = true;
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set('BranchNumber', this.getControlValue('BranchNumber'));
        searchParams.set('ContractNumber', this.getControlValue('ContractNumber'));
        searchParams.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        searchParams.set('DateFrom', this.getControlValue('DateFrom'));
        searchParams.set('DateTo', this.getControlValue('DateTo'));
        searchParams.set('ShowType', this.showTypeOptionDropdown.selectedItem);
        searchParams.set('EmployeeCode', this.getControlValue('EmployeeCode'));
        searchParams.set('OccupationCode', this.getControlValue('OccupationCode'));
        searchParams.set('VisitTypeCode', this.getControlValue('VisitTypeCode'));
        searchParams.set('ReqPrepUsed', this.utils.convertResponseValueToCheckboxInput(this.reqPrepUsed).toString());
        searchParams.set('ReqInfestation', this.utils.convertResponseValueToCheckboxInput(this.reqInfestation).toString());
        searchParams.set('Mode', this.Mode);
        searchParams.set('BranchServiceArea', this.getControlValue('BranchServiceArea'));
        searchParams.set(this.serviceConstants.PageSize, this.gridParams['itemsPerPage']);
        searchParams.set(this.serviceConstants.PageCurrent, this.gridParams['pageCurrent']);
        searchParams.set(this.serviceConstants.GridMode, this.gridParams['riGridMode']);
        searchParams.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        let sortOrder = 'Descending';
        if (!this.riGrid.DescendingSort)
            sortOrder = 'Ascending';
        searchParams.set('riSortOrder', sortOrder);
        searchParams.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        this.gridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                        setTimeout(() => {
                            this.riPagination.setPage(this.gridParams['pageCurrent']);
                        }, 500);
                        this.gridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.Execute(data);
                        if (data.pageData && (data.pageData.lastPageNumber * 10) > 0)
                            this.isHidePagination = false;
                        else
                            this.isHidePagination = true;
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                }
            );
    }

    public onGridDblClick(data: any): void {
        this.pageParams.showTypeDropdownValue = this.showTypeOptionDropdown.selectedItem;
        this.pageParams.gridCurrentPage = this.gridParams['pageCurrent'];
        let currentContractTypeCode: any;
        let URLParameter: any;
        this.contractRowID = this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty');
        this.premiseRowID = this.riGrid.Details.GetAttribute('PremiseNumber', 'additionalproperty');
        this.serviceCoverRowID = this.riGrid.Details.GetAttribute('ProductCode', 'additionalproperty');
        currentContractTypeCode = this.riGrid.Details.GetAttribute('ServiceDateStart', 'additionalproperty');
        switch (currentContractTypeCode) {
            case 'C':
                URLParameter = '';
                break;
            case 'J':
                URLParameter = '<job>';
                break;
            case 'P':
                URLParameter = '<product>';
                break;
            default:
                break;
        }
        switch (this.riGrid.CurrentColumnName) {
            case 'gridEmployeeCode':
                this.setAttribute('PDAICABSActivityRowID', this.riGrid.Details.GetAttribute('gridEmployeeCode', 'rowID'));
                this.navigate('ServiceVisitRejections', InternalMaintenanceServiceModuleRoutes.ICABSSEPDAICABSACTIVITYMAINTENANCE);
                break;
            case 'ContractNumber':
                if (this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'drilldown')) {
                    this.navigate('VisitRejection', AppModuleRoutes.CONTRACTMANAGEMENT + ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE_SUB, {
                        ContractRowID: this.contractRowID,
                        CurrentContractTypeURLParameter: URLParameter
                    });
                }
                break;
            case 'PremiseNumber':
                if (this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'drilldown')) {
                    this.navigate('VisitRejection', AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESMAINTENANCE + ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE_SUB, {
                        PremiseRowID: this.premiseRowID,
                        contractTypeCode: currentContractTypeCode
                    });
                }
                break;
            case 'ProductCode':
                if (this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'drilldown')) {
                    this.navigate('VisitRejection', this.ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                        ServiceCoverRowID: this.serviceCoverRowID,
                        currentContractType: currentContractTypeCode,
                        CurrentContractTypeURLParameter: URLParameter
                    });
                }
                break;
        }
    }

    public onBranchDataReceived(data: any): void {
        if (data['BranchNumber']) {
            this.setControlValue('BranchNumber', data['BranchNumber']);
            this.setControlValue('BranchName', data['BranchName']);
        } else {
            this.setControlValue('BranchNumber', '');
            this.setControlValue('BranchName', '');
        }
    }

    public visitTypeSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('VisitTypeCode', value.VisitTypeCode || value.value);
            this.setControlValue('VisitTypeDesc', value.VisitTypeDesc || value.value);
        }
        this.uiForm.markAsDirty();
    }

    public visitTypeDataRecieved(event: any): any {
        let len: number;
        len = event.length || 0;
        this.dropDown.VisitTypeCode.arrData = [];
        for (let i = 0; i < len; i++) {
            this.dropDown.VisitTypeCode.arrData.push({ code: event[i]['VisitType.VisitTypeCode'], desc: event[i]['VisitType.VisitTypeDesc'] });
        }
    }

    public onEmployeeDataReceived(data: any): void {
        if (data) {
            this.setControlValue('EmployeeCode', data['EmployeeCode']);
            this.setControlValue('EmployeeSurname', data['EmployeeSurname']);
        }
    }

    public onOccupationDataReceived(data: any): void {
        if (data) {
            this.setControlValue('OccupationCode', data['OccupationCode']);
            this.setControlValue('OccupationDesc', data['OccupationDesc']);
        }
    }

    public datePickerFromValue(value: any): void {
        if (value && value.value)
            this.setControlValue('DateFrom', value.value);
        let fullYearDate = new Date();
        let fullYear = fullYearDate.getFullYear();
        let onChangeDate = new Date(value.value), month: any = '' + onChangeDate.getMonth(), day: any = '' + onChangeDate.getDate(), year: any = onChangeDate.getFullYear();
        if (year < fullYear) {
            month = parseInt(month, 10);
            month = month + 4;
            if (month > 12) {
                month = month % 12;
                year = year + 1;
            }
            let onChangeToDate = this.globalize.parseDateStringToDate([day, month, year].join('/'));
            this.setControlValue('DateTo', onChangeToDate);
        }
        else {
            let lastDay = this.globalize.parseDateToFixedFormat(this.utils.TodayAsDDMMYYYY()).toString();
            this.setControlValue('DateTo', this.globalize.parseDateStringToDate(lastDay));
        }
    }

    public datePickerToValue(value: any): void {
        if (value && value.value)
            this.setControlValue('DateTo', value.value);
    }

    public showTypeOnChange(event: any): void {
        this.showTypeOptionDropdown.selectedItem = event;
    }

    public onEmployeeCodeChange(event: any): void {
        if (this.getControlValue('EmployeeCode')) {
            let searchParams: QueryParams;
            let postData: Object = {};
            searchParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            searchParams.set('ActionType', 'GetEmployeeSurname');
            postData['BranchNumber'] = this.getControlValue('BranchNumber');
            postData['EmployeeCode'] = this.getControlValue('EmployeeCode');
            //making post request
            this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
                searchParams, postData).subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            this.setControlValue('EmployeeCode', '');
                            this.setControlValue('EmployeeSurname', '');
                        }
                        else
                            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
        else {
            this.setControlValue('EmployeeCode', '');
            this.setControlValue('EmployeeSurname', '');
        }
    }

    public onOccupationCodeChange(event: any): void {
        if (this.getControlValue('OccupationCode')) {
            let searchParams: QueryParams;
            let postData: Object = {};
            searchParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            searchParams.set('ActionType', 'GetOccupationDesc');
            postData['OccupationCode'] = this.getControlValue('OccupationCode');
            //making post request
            this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
                searchParams, postData).subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            this.setControlValue('OccupationCode', '');
                            this.setControlValue('OccupationDesc', '');
                        }
                        else
                            this.setControlValue('OccupationDesc', data.OccupationDesc);
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
        else {
            this.setControlValue('OccupationCode', '');
            this.setControlValue('OccupationDesc', '');
        }
    }

    public getCurrentPage(currentPage: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.gridParams['pageCurrent'] = currentPage.value;
            this.riGrid.UpdateHeader = true;
            this.riGrid.UpdateRow = true;
            this.riGrid.UpdateFooter = true;
            this.loadData();
        }
    }

    public riGridSort(event: any): void {
        this.loadData();
    }

    public refresh(): void {
        if ((this.getControlValue('DateFrom') !== '') && (this.getControlValue('DateTo') !== '')) {
            if (this.isFlag) {
                this.buildGrid();
                this.isFlag = false;
            }
            else
                this.loadData();
        }
    }
}
