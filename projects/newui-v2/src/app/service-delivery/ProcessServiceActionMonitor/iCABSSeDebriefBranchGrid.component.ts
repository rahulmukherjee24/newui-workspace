import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, ViewChild, OnInit, Injector, OnDestroy, ElementRef, ChangeDetectorRef, Renderer } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { BranchServiceAreaSearchComponent } from '../../../app/internal/search/iCABSBBranchServiceAreaSearch';
import { EmployeeSearchComponent } from '../../../app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { AjaxConstant } from '../../../shared/constants/AjaxConstants';
import { PaginationComponent } from '../../../shared/components/pagination/pagination';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { ServiceDeliveryModuleRoutes, AppModuleRoutes } from '../../../app/base/PageRoutes';

@Component({
    templateUrl: 'iCABSSeDebriefBranchGrid.html'
})

export class SeDebriefBranchGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('branchServiceAreaCode') branchServiceAreaCode: ElementRef;

    private queryParams: any = {
        operation: 'Service/iCABSSeDebriefBranchGrid',
        module: 'sam',
        method: 'service-delivery/grid'
    };
    private blnCacheRefresh: boolean = true;
    public pageId: string = '';
    public controls: any = [
        { name: 'SupervisorEmployeeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'SupervisorEmployeeName', readonly: false, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', readonly: false, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'DebriefDate', readonly: false, disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'ActiveOnly', type: MntConst.eTypeText },
        { name: 'RAGStatusCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeText }
    ];
    public ellipsisConfig: any = {
        employee: {
            isShowCloseButton: true,
            isShowHeader: true,
            childConfigParams: {
                'parentMode': 'LookUp-Supervisor'
            },
            component: EmployeeSearchComponent
        },
        branchServiceArea: {
            isDisabled: false,
            isShowCloseButton: true,
            isShowHeader: true,
            parentMode: 'LookUp',
            isShowAddNew: false,
            component: BranchServiceAreaSearchComponent
        }
    };
    public gridConfig = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 1,
        action: '2'
    };
    public checkboxValue: boolean = true;

    constructor(injector: Injector, private _renderer: Renderer, private ref: ChangeDetectorRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEDEBRIEFBRANCHGRID;
        this.browserTitle = 'Service Action Monitor - Branch';
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            this.buildGrid();
            this.populateGrid();
        }
        else {
            this.pageParams.gridCurrentPage = 1;
            this.windowOnload();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.setControlValue('DebriefDate', this.globalize.parseDateToFixedFormat(new Date()));
        if (this.parentMode === 'DebriefOutstanding') {
            this.setControlValue('SupervisorEmployeeCode', this.riExchange.getParentAttributeValue('SupervisorEmployeeCode'));
            this.setControlValue('SupervisorEmployeeName', this.riExchange.getParentAttributeValue('SupervisorEmployeeName'));
        }
        else {
            this._renderer.invokeElementMethod(this.branchServiceAreaCode.nativeElement, 'focus', []);
        }
        this.buildGrid();
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        if (this.riGrid.Update) {
            this.riGrid.UpdateBody = true;
            this.riGrid.UpdateHeader = false;
            this.riGrid.UpdateFooter = false;
        }
        else {
            this.blnCacheRefresh = false;
        }

        this.riGrid.AddColumn('Supervisor', 'Grid', 'Supervisor', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('Supervisor', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchServiceAreaCode', 'Grid', 'BranchServiceAreaCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('EmployeeDetails', 'Grid', 'EmployeeDetails', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('EmployeeDetails', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('LastPremises', 'Grid', 'LastPremises', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('LastPremises', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('NextPremises', 'Grid', 'NextPremises', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('NextPremises', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('CurrentStatus', 'Grid', 'CurrentStatus', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('CurrentStatus', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('LastDebriefDate', 'Grid', 'LastDebriefDate', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('LastDebriefDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServicePlanStartDate', 'Grid', 'ServicePlanStartDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServicePlanStartDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('WorkSentToPDADate', 'Grid', 'WorkSentToPDADate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('WorkSentToPDADate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('NumberOfDaysWork', 'Grid', 'NumberOfDaysWork', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('NumberOfDaysWork', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('NumberOfOpenPlans', 'Grid', 'NumberOfOpenPlans', MntConst.eTypeInteger, 2);
        this.riGrid.AddColumnAlign('NumberOfOpenPlans', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('NumberOfPremises', 'Grid', 'NumberOfPremises', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('NumberOfPremises', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PercentageDone', 'Grid', 'PercentageDone', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('PercentageDone', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('IncompletePremises', 'Grid', 'IncompletePremises', MntConst.eTypeInteger, 2);
        this.riGrid.AddColumnAlign('IncompletePremises', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('Actions', 'Grid', 'Actions', MntConst.eTypeInteger, 2);
        this.riGrid.AddColumnAlign('Actions', MntConst.eAlignmentCenter);
        this.riGrid.Complete();
    }

    public populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set('BranchNumber', this.utils.getBranchCode());
        gridSearch.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        gridSearch.set('SupervisorEmployeeCode', this.getControlValue('SupervisorEmployeeCode'));
        gridSearch.set('RAGStatusCode', this.getControlValue('RAGStatusCode'));
        gridSearch.set('ActiveOnly', String(this.checkboxValue));

        gridSearch.set(this.serviceConstants.GridMode, '0');
        gridSearch.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        gridSearch.set(this.serviceConstants.GridCacheRefresh, this.blnCacheRefresh ? 'True' : 'False');
        gridSearch.set(this.serviceConstants.PageSize, this.gridConfig.pageSize.toString());
        gridSearch.set(this.serviceConstants.PageCurrent, this.pageParams.gridCurrentPage.toString());
        gridSearch.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridSearch.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());

        this.ajaxSource.next(AjaxConstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridSearch)
            .subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    } else {
                        this.riGrid.RefreshRequired();
                        this.riGridPagination.currentPage = this.pageParams.gridCurrentPage = e.pageData ? e.pageData.pageNumber : 1;
                        this.riGridPagination.totalItems = this.gridConfig.totalRecords = e.pageData ? e.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.Execute(e);
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 100);
                        this.ref.detectChanges();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public onRiGridRefresh(): void {
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.pageParams.gridCurrentPage = currentPage.value;
        this.onRiGridRefresh();
    }

    public gridFocus(rsrcElement: any): void {
        rsrcElement.focus();
        this.setAttribute('BranchServiceAreaCode', this.riGrid.Details.GetValue('BranchServiceAreaCode'));
        this.setAttribute('BranchServiceAreaDesc', this.riGrid.Details.GetAttribute('BranchServiceAreaCode', 'Title'));
        this.setAttribute('EmployeeCode', this.riGrid.Details.GetAttribute('BranchServiceAreaCode', 'AdditionalProperty'));
        this.setAttribute('EmployeeSurname', this.riGrid.Details.GetAttribute('NextPremises', 'AdditionalProperty'));
        this.setAttribute('DebriefFromDate', this.riGrid.Details.GetAttribute('ServicePlanStartDate', 'AdditionalProperty'));
        this.setAttribute('Row', this.riGrid.CurrentRow);
    }

    public riGridOnDblClick(event: any): void {
        if (event.srcElement.className === 'pointer') {
            this.gridFocus(event.srcElement);
            this.riExchange.Mode = 'DebriefBranch-All';
            if (this.riGrid.CurrentColumnName === 'Actions') {
                this.riExchange.Mode = 'DebriefBranch-Actions';
            }
            this.navigate(this.riExchange.Mode, AppModuleRoutes.SERVICEDELIVERY + ServiceDeliveryModuleRoutes.ICABSSEDEBRIEFEMPLOYEEGRID);
        }
    }

    public populateDescriptions(): void {
        let descSearch: QueryParams = this.getURLSearchParamObject();
        descSearch.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData['Function'] = 'GetDescriptions';
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['SupervisorEmployeeCode'] = this.getControlValue('SupervisorEmployeeCode');
        this.ajaxSource.next(AjaxConstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, descSearch, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.setControlValue('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode').toUpperCase());
                    this.setControlValue('BranchServiceAreaDesc', e.BranchServiceAreaDesc);
                    this.setControlValue('SupervisorEmployeeCode', this.getControlValue('SupervisorEmployeeCode').toUpperCase());
                    this.setControlValue('SupervisorEmployeeName', e.SupervisorEmployeeName);
                }
            },
            (error) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));

            });
    }

    public onCheckboxClicked(event: any): void {
        if (event) {
            this.checkboxValue = event.target.checked;
        }
    }

    public onEmployeeDataReceived(data: any): void {
        if (data) {
            this.setControlValue('SupervisorEmployeeCode', data.SupervisorEmployeeCode);
            this.setControlValue('SupervisorEmployeeName', data.SupervisorSurname);
        }
    }

    public onServiceAreaDataReceived(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
        }
    }

}
