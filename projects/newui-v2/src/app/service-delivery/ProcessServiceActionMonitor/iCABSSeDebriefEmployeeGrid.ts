import { QueryParams } from '@shared/services/http-params-wrapper';
import { OnInit, Injector, Component, OnDestroy, ViewChild, ElementRef } from '@angular/core';

import { Subscription } from 'rxjs';

import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { DebriefSearchComponent } from './../../internal/search/iCABSSeDebriefSearch.component';
import { InternalGridSearchApplicationModuleRoutes, AppModuleRoutes, ContractManagementModuleRoutes, InternalGridSearchApplicationModuleRoutesConstant, InternalGridSearchServiceModuleRoutes, ServiceDeliveryModuleRoutes, InternalMaintenanceServiceModuleRoutes } from './../../base/PageRoutes';
import { PremiseSearchComponent } from './../../internal/search/iCABSAPremiseSearch';
import { ContractSearchComponent } from './../../internal/search/iCABSAContractSearch';
import { EmployeeSearchComponent } from './../../internal/search/iCABSBEmployeeSearch';
import { BranchServiceAreaSearchComponent } from './../../internal/search/iCABSBBranchServiceAreaSearch';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { IExportOptions } from '@app/base/ExportConfig';

@Component({
    templateUrl: 'iCABSSeDebriefEmployeeGrid.html'
})

export class SeDebriefEmployeeGridComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('BranchServiceAreaCode') branchServiceAreaCode: ElementRef;

    private search: QueryParams = new QueryParams();
    private xhrParams: any = {
        operation: 'Service/iCABSSeDebriefEmployeeGrid',
        module: 'sam',
        method: 'service-delivery/maintenance'
    };
    private currentContractType: string = '';
    private subSysChar: Subscription;
    private isDisplayRowChanged: boolean = false;

    public branchServiceAreaComponent = BranchServiceAreaSearchComponent;
    public employeeSearchComponent = EmployeeSearchComponent;
    public contractSearchComponent = ContractSearchComponent;
    public premiseSearchComponent = PremiseSearchComponent;
    public debriefSearchComponent = DebriefSearchComponent;

    public branchServiceAreaSearchParams: any = {
        parentMode: 'LookUp'
    };
    public employeeSearchParams: any = {
        parentMode: 'LookUp'
    };
    public inputParamsContractSearch: any = {
        parentMode: 'LookUp-All',
        accountNumber: '',
        currentContractType: 'C',
        enableAccountSearch: false
    };
    public inputParamsAccountPremise: any = {
        parentMode: 'LookUp',
        currentContractType: 'C',
        ContractNumber: '',
        showAddNew: false
    };
    public debriefSearchParams: any = {
        parentMode: 'LookUp-SAM',
        EmployeeCode: ''
    };

    public pageId: string = '';
    public cmdShowText: string = 'Trip Information';
    public totalRecords: number = 1;
    public exportConfig: IExportOptions = {};
    public controls = [
        { name: 'BranchServiceAreaCode', disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'VehicleCheckedInd', disabled: true, required: false, type: MntConst.eTypeCheckBox },
        { name: 'EmployeeCode', disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ServicePlanNumber', disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'DebriefNumber', disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'VehicleStartMileage', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'VehicleEndMileage', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'DayLocationStart', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'DayLocationEnd', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'DayTimeStart', disabled: true, required: false, type: MntConst.eTypeTime },
        { name: 'DayTimeEnd', disabled: true, required: false, type: MntConst.eTypeTime },
        { name: 'LastSyncDate', disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'LastSyncTime', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ContractNumber', disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'PremiseNumber', disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'FilterStatus', disabled: false, required: false },
        { name: 'FilterAction', disabled: false, required: false },
        { name: 'NumberOfDaysWorked', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'NumberOfPremises', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'PercentageComplete', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'DisplayNumberOfRows', disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'DebriefFromDate', disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'DebriefToDate', disabled: false, required: false, type: MntConst.eTypeDate },
        { name: 'cmdLeadAlert', disabled: false, required: false },
        { name: 'cmdDebrief', disabled: false, required: false },
        { name: 'cmdShow', disabled: false, required: false },
        { name: 'cmdVisitMap', disabled: true, required: false }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEDEBRIEFEMPLOYEEGRID;
        this.browserTitle = 'Service Action Monitor - Employee';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.inputParamsContractSearch.currentContractType = this.riExchange.getCurrentContractType();
        this.inputParamsAccountPremise.currentContractType = this.riExchange.getCurrentContractType();
        if (this.isReturning()) {
            this.buildGrid();
            this.riGridBeforeExecute();
            return;
        } else {
            this.pageParams['currentPage'] = 1;
        }
        this.pageParams.tbFilterDetails1 = true;
        this.pageParams.tbFilterDetails2 = true;
        this.pageParams.tbSyncDetails1 = false;
        this.pageParams.tbSyncDetails2 = false;
        this.pageParams.gridHandle = this.utils.randomSixDigitString();
        this.loadSysChar();
    }

    ngOnDestroy(): void {
        if (this.subSysChar) {
            this.subSysChar.unsubscribe();
        }
        super.ngOnDestroy();
    }

    private loadSysChar(): void {
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharEnableRouteOptimisationSoftwareIntegration
        ];
        let sysCharIP = {
            module: this.xhrParams.module,
            operation: this.xhrParams.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };
        this.subSysChar = this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            let record: any = data.records;
            this.pageParams.vbEnableRouteOptimisation = record[0].Required;
            this.windowOnload();
        });
    }

    private windowOnload(): void {
        this.pageParams.blnCacheRefresh = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = 10;
        this.riGrid.FunctionUpdateSupport = true;
        this.setControlValue('FilterStatus', 'All');
        this.buildGrid();
        this.branchServiceAreaCode.nativeElement.focus();
        if (!this.pageParams.vbEnableRouteOptimisation) {
            this.pageParams.cmdVisitMap = false;
        } else {
            this.pageParams.cmdVisitMap = true;
            this.disableControl('cmdVisitMap', true);
        }
        let fromDate: Date = new Date();
        fromDate.setMonth(fromDate.getMonth() - 1);
        fromDate.setDate(1);
        this.setControlValue('DebriefFromDate', fromDate);
        this.setControlValue('DebriefToDate', this.globalize.parseDateToFixedFormat(new Date()));
        this.setControlValue('DisplayNumberOfRows', 10);
        this.disableControl('DebriefFromDate', true);
        if (this.parentMode === 'DebriefBranch-Actions') {
            this.setControlValue('FilterAction', 'Action');
        } else {
            this.setControlValue('FilterAction', 'All');
        }
        // Set default value for menu
        if (this.parentMode === 'DebriefBranch-All' || this.parentMode === 'DebriefBranch-Actions'
            || this.parentMode === 'DebriefSummary') {
            this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentAttributeValue('BranchServiceAreaCode'));
            this.setControlValue('BranchServiceAreaDesc', this.riExchange.getParentAttributeValue('BranchServiceAreaDesc'));
            this.setControlValue('EmployeeCode', this.riExchange.getParentAttributeValue('EmployeeCode'));
            this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('EmployeeSurname'));
            this.setControlValue('DebriefFromDate', this.riExchange.getParentAttributeValue('DebriefFromDate'));
            if (this.parentMode === 'DebriefSummary') {
                this.setControlValue('DebriefNumber', this.riExchange.getParentAttributeValue('DebriefNumber'));
                this.setControlValue('DebriefToDate', this.riExchange.getParentAttributeValue('DebriefToDate'));
                this.disableControl('DebriefToDate', true);
            }
            this.disableControl('BranchServiceAreaCode', true);
            this.disableControl('EmployeeCode', true);
            this.disableControl('DebriefNumber', true);
            if (!this.fieldHasValue('DebriefFromDate')) {
                this.setControlValue('DebriefFromDate', fromDate);
            }
            this.populateDebriefDetails();
            this.riGridBeforeExecute();
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('PremiseSequenceNumber', 'Grid', 'PremiseSequenceNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseSequenceNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseNum', 'Grid', 'PremiseNum', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseNum', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseDetails', 'Grid', 'PremiseDetails', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseDetails', MntConst.eAlignmentLeft);
        //this.riGrid.AddColumn('GPS','Grid','GPS',MntConst.eTypeText,4);
        //this.riGrid.AddColumnAlign('GPS',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PlannedVisitDate', 'Grid', 'PlannedVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('PlannedVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('VisitStartTime', 'Grid', 'VisitStartTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('VisitStartTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('VisitEndTime', 'Grid', 'VisitEndTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('VisitEndTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServicePlanNum', 'Grid', 'ServicePlanNum', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServicePlanNum', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ActualVisitDate', 'Grid', 'ActualVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ActualVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ActualStartTime', 'Grid', 'ActualStartTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('ActualStartTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ActualEndTime', 'Grid', 'ActualEndTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('ActualEndTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('CurrentStatus', 'Grid', 'CurrentStatus', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('CurrentStatus', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('SignatureRequired', 'Grid', 'SignatureRequired', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('SignatureRequired', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BarcodeScanRequired', 'Grid', 'BarcodeScanRequired', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('BarcodeScanRequired', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('SignatureCaptured', 'Grid', 'SignatureCaptured', MntConst.eTypeImage, 10);
        this.riGrid.AddColumnAlign('SignatureCaptured', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseContactName', 'Grid', 'PremiseContactName', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('PremiseContactName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('SignatureApprovedInd', 'Grid', 'SignatureApprovedInd', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('SignatureApprovedInd', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('SignatureApprovedInd', true);
        this.riGrid.AddColumn('BarcodeScanCaptured', 'Grid', 'BarcodeScanCaptured', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('BarcodeScanCaptured', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ActionRequiredInd', 'Grid', 'ActionRequiredInd', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('ActionRequiredInd', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('ActionRequiredInd', true);
        this.riGrid.AddColumn('ActionCreatedDate', 'Grid', 'ActionCreatedDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ActionCreatedDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ActionCreatedTime', 'Grid', 'ActionCreatedTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('ActionCreatedTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PreviouslyMissed', 'Grid', 'PreviouslyMissed', MntConst.eTypeText, 4);
        this.riGrid.AddColumnAlign('PreviouslyMissed', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('VisitRejectionsNumber', 'Grid', 'VisitRejectionsNumber', MntConst.eTypeInteger, 1);
        this.riGrid.AddColumnAlign('VisitRejectionsNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('LocationAdded', 'Grid', 'LocationAdded', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('LocationAdded', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('Alerts', 'Grid', 'Alerts', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('Alerts', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('Leads', 'Grid', 'Leads', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('Leads', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('PremiseSequenceNumber', true);
        this.riGrid.AddColumnOrderable('ActualVisitDate', true);
        this.riGrid.Complete();

        this.exportConfig = {};
        this.dateCollist = this.riGrid.getColumnIndexList([
            'PlannedVisitDate',
            'ActualVisitDate',
            'ActionCreatedDate'
        ]);
        this.durationCollist = this.riGrid.getColumnIndexList([
            'VisitStartTime',
            'VisitEndTime',
            'ActualStartTime',
            'ActualEndTime',
            'ActionCreatedTime'
        ]);
        if (this.dateCollist.length) {
            this.exportConfig.dateColumns = this.dateCollist;
        }
        if (this.durationCollist.length) {
            this.exportConfig.durationColumns = this.durationCollist;
        }
    }

    private riGridBeforeExecute(): void {
        let fromDate = this.globalize.parseDateStringToDate(this.getControlValue('DebriefFromDate'));
        let toDate = this.globalize.parseDateStringToDate(this.getControlValue('DebriefToDate'));
        if (this.riExchange.riInputElement.isError(this.uiForm, 'DebriefFromDate')
            || this.riExchange.riInputElement.isError(this.uiForm, 'DebriefToDate')
            || (fromDate && toDate && fromDate > toDate)) {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'DebriefFromDate', true);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'DebriefToDate', true);
            return;
        }
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '2');
        this.search.set('BranchNumber', this.utils.getBranchCode());
        this.search.set('ContractNumber', this.getControlValue('ContractNumber'));
        this.search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        this.search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        this.search.set('EmployeeCode', this.getControlValue('EmployeeCode'));
        this.search.set('DebriefNumber', this.getControlValue('DebriefNumber'));
        this.search.set('DebriefFromDate', this.getControlValue('DebriefFromDate'));
        this.search.set('DebriefToDate', this.getControlValue('DebriefToDate'));
        this.search.set('FilterAction', this.getControlValue('FilterAction'));
        this.search.set('FilterStatus', this.getControlValue('FilterStatus'));
        this.search.set('ServicePlanNumber', this.getControlValue('ServicePlanNumber'));
        this.search.set(this.serviceConstants.GridMode, '0');
        this.search.set(this.serviceConstants.GridHandle, this.pageParams.gridHandle);
        this.search.set(this.serviceConstants.GridCacheRefresh, this.pageParams.blnCacheRefresh);
        this.search.set(this.serviceConstants.PageSize, this.getControlValue('DisplayNumberOfRows'));
        this.search.set(this.serviceConstants.GridPageCurrent, this.pageParams['currentPage'].toString());
        this.search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        this.search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, this.search)
            .subscribe(
                (data) => {
                    if (data) {
                        if (!this.riGrid.Update) {
                            this.pageParams['currentPage'] = data.pageData ? data.pageData.pageNumber : 1;
                            this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        }
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            if (this.riGrid.Update) {
                                this.riGrid.StartRow = this.getAttribute('Row');
                                this.riGrid.StartColumn = 0;
                                this.riGrid.RowID = this.getAttribute('RowID');
                                this.riGrid.UpdateHeader = false;
                                this.riGrid.UpdateBody = true;
                                this.riGrid.UpdateFooter = false;
                            } else {
                                this.pageParams.blnCacheRefresh = false;
                                this.riGrid.UpdateHeader = true;
                                this.riGrid.UpdateBody = true;
                                this.riGrid.UpdateFooter = true;
                            }
                            if (this.isReturning()) {
                                setTimeout(() => {
                                    this.riGridPagination.setPage(this.pageParams['currentPage']);
                                }, 500);
                            }
                            this.riGrid.Execute(data);
                        }
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                error => {
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                    this.totalRecords = 1;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private gridFocus(rsrcElement: any): void {
        rsrcElement.focus();
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('PremiseDetails', 'AdditionalProperty'));
        this.setAttribute('ContractNumber', this.riGrid.Details.GetAttribute('PlannedVisitDate', 'AdditionalProperty'));
        this.setAttribute('PremiseNumber', this.riGrid.Details.GetAttribute('ActualVisitDate', 'AdditionalProperty'));
        this.setAttribute('PlannedVisitDate', this.riGrid.Details.GetValue('PlannedVisitDate'));
        this.setAttribute('ActualVisitDate', this.riGrid.Details.GetValue('ActualVisitDate'));
        this.setAttribute('ActualStartTime', this.riGrid.Details.GetValue('ActualStartTime'));
        this.setAttribute('ActualEndTime', this.riGrid.Details.GetValue('ActualEndTime'));
        this.setAttribute('ServicePlanNumber', this.riGrid.Details.GetValue('ServicePlanNum'));
    }

    private upDateGridIndicators(functionName: string, rsrcElement: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '6');
        this.setAttribute('Row', rsrcElement.parentElement.parentElement.sectionRowIndex);
        this.setAttribute('RowID', rsrcElement.getAttribute('RowID'));
        let formData = {
            Function: functionName,
            Rowid: rsrcElement.getAttribute('RowID')
        };
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.pageParams.blnCacheRefresh = true;
                        this.riGridBeforeExecute();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                });
    }

    private populateDescriptions(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '6');
        let formData = {
            Function: 'GetDescriptions',
            BranchNumber: this.utils.getBranchCode()
        };
        if (this.fieldHasValue('EmployeeCode')) {
            formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        }
        if (this.fieldHasValue('BranchServiceAreaCode')) {
            formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        }
        if (this.fieldHasValue('DebriefFromDate')) {
            formData['DebriefFromDate'] = this.getControlValue('DebriefFromDate');
        }

        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('BranchServiceAreaCode', data['BranchServiceAreaCode']);
                        this.setControlValue('BranchServiceAreaDesc', data['BranchServiceAreaDesc']);
                        this.setControlValue('EmployeeCode', data['EmployeeCode']);
                        this.setControlValue('EmployeeSurname', data['EmployeeSurname']);
                        this.setControlValue('DebriefFromDate', data['DebriefFromDate']);
                        this.populateDebriefDetails();
                        this.debriefSearchParams.EmployeeCode = this.getControlValue('EmployeeCode');
                        this.pageParams.gridHandle = this.utils.randomSixDigitString();
                        this.pageParams.blnCacheRefresh = true;
                        this.riGrid.RefreshRequired();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                });
    }

    private populateDebriefDetails(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '6');
        let formData = {
            Function: 'GetDebriefDetails',
            BranchNumber: this.utils.getBranchCode(),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            EmployeeCode: this.getControlValue('EmployeeCode'),
            DebriefNumber: this.getControlValue('DebriefNumber'),
            DebriefFromDate: this.getControlValue('DebriefFromDate'),
            DebriefToDate: this.getControlValue('DebriefToDate')
        };
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('DebriefFromDate', data['DebriefFromDate']);
                        this.setControlValue('DebriefToDate', data['DebriefToDate']);
                        this.setControlValue('NumberOfDaysWorked', data['NumberOfDaysWorked']);
                        this.setControlValue('NumberOfPremises', data['NumberOfPremises']);
                        this.setControlValue('PercentageComplete', data['PercentageComplete']);
                        this.setControlValue('VehicleCheckedInd', data['VehicleCheckedInd']);
                        this.setControlValue('VehicleStartMileage', data['VehicleStartMileage']);
                        this.setControlValue('VehicleEndMileage', data['VehicleEndMileage']);
                        this.setControlValue('DayLocationStart', data['DayLocationStart']);
                        this.setControlValue('DayLocationEnd', data['DayLocationEnd']);
                        this.setControlValue('DayTimeStart', data['DayTimeStart']);
                        this.setControlValue('DayTimeEnd', data['DayTimeEnd']);
                        this.setControlValue('LastSyncDate', data['LastSyncDate']);
                        this.setControlValue('LastSyncTime', data['LastSyncTime']);
                        this.pageParams.gridHandle = this.utils.randomSixDigitString();
                        this.pageParams.blnCacheRefresh = true;
                        this.riGrid.RefreshRequired();
                        let fromDate = this.globalize.parseDateStringToDate(this.getControlValue('DebriefFromDate'));
                        let toDate = this.globalize.parseDateStringToDate(this.getControlValue('DebriefToDate'));
                        if (fromDate > toDate) {
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'DebriefFromDate', true);
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'DebriefToDate', true);
                        } else {
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'DebriefFromDate', false);
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'DebriefToDate', false);
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                });
    }

    private populateContract(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '6');
        let formData = {
            Function: 'GetContract',
            ContractNumber: this.getControlValue('ContractNumber')
        };
        if (this.fieldHasValue('PremiseNumber')) {
            formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
        }
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('ContractName', data['ContractName']);
                        this.setControlValue('PremiseName', data['PremiseName']);
                        this.pageParams.gridHandle = this.utils.randomSixDigitString();
                        this.pageParams.blnCacheRefresh = true;
                        this.riGrid.RefreshRequired();
                    }
                    this.inputParamsAccountPremise.ContractNumber = this.getControlValue('ContractNumber');
                    this.inputParamsAccountPremise.ContractName = this.getControlValue('ContractName');
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                });
    }

    private checkValidServicePlan(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '6');
        let formData = {
            Function: 'CheckValidServicePlan',
            BranchNumber: this.utils.getBranchCode(),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            ServicePlanNumber: this.getControlValue('ServicePlanNumber')
        };
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.disableControl('cmdVisitMap', true);
                    } else {
                        if (this.UCase(data['CheckValidServicePlan']) === 'YES') {
                            this.disableControl('cmdVisitMap', false);
                        } else {
                            this.disableControl('cmdVisitMap', true);
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                });
    }

    public tbodyGridOnDblClick(event: any): void {
        if (event.srcElement.style.cursor !== true) {  //hand
            this.gridFocus(event.srcElement);
            this.pageParams.contractType = this.riGrid.Details.GetAttribute('PremiseSequenceNumber', 'AdditionalProperty');
            switch (this.riGrid.CurrentColumnName) {
                case 'PremiseNum':
                    this.navigate('ServicePlanning', AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESMAINTENANCE + ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE_SUB,
                        {
                            currentContractType: this.pageParams.contractType,
                            PremiseRowID: this.getAttribute('PremiseRowID')
                        });
                    break;
                case 'CurrentStatus':
                    this.navigate('General', InternalGridSearchApplicationModuleRoutes.ICABSSEPREMISEPDAICABSACTIVITYGRID,
                        {
                            ContractNumber: this.getAttribute('ContractNumber'),
                            PremiseNumber: this.getAttribute('PremiseNumber'),
                            PlannedVisitDate: this.getAttribute('PlannedVisitDate'),
                            ActualVisitDate: this.getAttribute('ActualVisitDate'),
                            ActualStartTime: this.getAttribute('ActualStartTime'),
                            ActualEndTime: this.getAttribute('ActualEndTime'),
                            ServicePlanNumber: this.getAttribute('ServicePlanNumber')
                        });
                    break;
                case 'BarcodeScanCaptured':
                    /**
                     * Opern page iCABSSeScannedBarcodes with parent mode DebriefBranchGrid
                     */
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotCovered));
                    break;
                case 'VisitRejectionsNumber':
                    this.navigate('Debrief', AppModuleRoutes.SERVICEDELIVERY + ServiceDeliveryModuleRoutes.ICABSSESERVICEVISITREJECTIONSGRID,
                        {
                            EmployeeCode: this.getControlValue('EmployeeCode'),
                            EmployeeSurname: this.getControlValue('EmployeeSurname'),
                            ContractNumber: this.getAttribute('ContractNumber'),
                            PremiseNumber: this.getAttribute('PremiseNumber'),
                            ActualVisitDate: this.getAttribute('ActualVisitDate'),
                            BranchServiceArea: this.getControlValue('BranchServiceAreaCode')
                        });
                    break;
                case 'LocationAdded':
                    this.navigate('Debrief', InternalGridSearchServiceModuleRoutes.ICABSSEHCANEWLOCATIONGRID);
                    break;
                case 'Alerts':
                case 'Leads':
                    this.navigate('Debrief', InternalGridSearchApplicationModuleRoutes.ICABSCMHCALEADGRID,
                        {
                            DebriefFromDate: this.getControlValue('DebriefFromDate'),
                            DebriefToDate: this.getControlValue('DebriefToDate'),
                            ActualVisitDate: this.getAttribute('ActualVisitDate')
                        });
                    break;
                case 'SignatureApprovedInd':
                    this.upDateGridIndicators('UpdateSignatureApprovedInd', event.srcElement.parentElement);
                    break;
                case 'ActionRequiredInd':
                    this.upDateGridIndicators('UpdateActionRequiredInd', event.srcElement.parentElement);
                    break;
            }
        }
    }

    public branchServiceAreaCodeOnchange(): void {
        if (this.fieldHasValue('BranchServiceAreaCode')) {
            this.setControlValue('EmployeeCode', '');
            this.populateDescriptions();
        } else {
            this.setControlValue('BranchServiceAreaDesc', '');
        }
    }

    public employeeCodeOnchange(): void {
        if (this.fieldHasValue('EmployeeCode')) {
            this.setControlValue('BranchServiceAreaCode', '');
            this.populateDescriptions();
        } else {
            this.setControlValue('EmployeeSurname', '');
            this.debriefSearchParams.EmployeeCode = '';
        }
    }

    public debriefFromDateOnchange(): void {
        if (this.fieldHasValue('DebriefFromDate') && !this.fieldHasValue('DebriefNumber')) {
            this.populateDebriefDetails();
        }
    }

    public debriefToDateOnchange(): void {
        if (this.fieldHasValue('DebriefToDate') && !this.fieldHasValue('DebriefNumber')) {
            this.populateDebriefDetails();
        }
    }

    public debriefNumberOnchange(): void {
        if (this.fieldHasValue('DebriefNumber')) {
            this.populateDebriefDetails();
            this.disableControl('DebriefFromDate', true);
            this.disableControl('DebriefToDate', true);
        } else {
            this.disableControl('DebriefFromDate', false);
            this.disableControl('DebriefToDate', false);
        }
    }

    public displayNumberOfRowsOnchange(): void {
        this.riGrid.PageSize = this.getControlValue('DisplayNumberOfRows');
        this.isDisplayRowChanged = true;
        this.riGrid.RefreshRequired();
    }

    public cmdDebriefOnclick(): void {
        this.navigate('Debrief', InternalMaintenanceServiceModuleRoutes.ICABSSEDEBRIEFMAINTENANCE);
    }

    public cmdLeadAlertOnclick(): void {
        this.navigate('Debrief-All', AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSCMHCALEADGRID,
            {
                DebriefFromDate: this.getControlValue('DebriefFromDate'),
                DebriefToDate: this.getControlValue('DebriefToDate'),
                ActualVisitDate: this.getAttribute('ActualVisitDate')
            });
    }

    public cmdShowOnclick(): void {
        if (this.pageParams.tbFilterDetails1 === true) {
            this.cmdShowText = 'Filter Options';
            this.pageParams.tbFilterDetails1 = false;
            this.pageParams.tbFilterDetails2 = false;
            this.pageParams.tbSyncDetails1 = true;
            this.pageParams.tbSyncDetails2 = true;
        } else {
            this.cmdShowText = 'Trip Information';
            this.pageParams.tbFilterDetails1 = true;
            this.pageParams.tbFilterDetails2 = true;
            this.pageParams.tbSyncDetails1 = false;
            this.pageParams.tbSyncDetails2 = false;
        }
    }

    public contractNumberOnchange(): void {
        if (!this.fieldHasValue('ContractNumber')) {
            this.setControlValue('ContractName', '');
            this.setControlValue('PremiseNumber', '');
            this.setControlValue('PremiseName', '');
            this.inputParamsAccountPremise.ContractNumber = '';
            this.inputParamsAccountPremise.ContractName = '';
        } else {
            this.populateContract();
        }
    }

    public premiseNumberOnchange(): void {
        if (!this.fieldHasValue('PremiseNumber')) {
            this.setControlValue('PremiseName', '');
        } else {
            this.populateContract();
        }
    }

    public cmdVisitMapOnclick(): void {
        /**
         * Opern page iCABSSeVisitMap with parent mode DebriefEmployeeGrid
         */
        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotCovered));
    }

    public servicePlanNumberOnChange(): void {
        if (this.pageParams.vbEnableRouteOptimisation) {
            if (this.fieldHasValue('ServicePlanNumber')) {
                this.checkValidServicePlan();
            } else {
                this.disableControl('cmdVisitMap', true);
            }

        }
    }

    public refresh(): void {
        if (this.isDisplayRowChanged) {
            this.isDisplayRowChanged = false;
        }
        this.pageParams.blnCacheRefresh = true;
        this.riGridBeforeExecute();
    }

    public riGridSort(event: any): void {
        this.riGridBeforeExecute();
    }

    public getCurrentPage(event: any): void {
        if (this.pageParams['currentPage'] !== event.value) {
            this.pageParams['currentPage'] = event.value;
            this.riGridBeforeExecute();
        }
    }

    public setBranchServiceArea(event: any): void {
        this.riExchange.riInputElement.SetValue(this.uiForm, 'BranchServiceAreaCode', event.BranchServiceAreaCode);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'BranchServiceAreaDesc', event.BranchServiceAreaDesc);
    }

    public employeeOnchange(obj: any): void {
        this.setControlValue('EmployeeCode', obj.EmployeeCode);
        this.setControlValue('EmployeeSurname', obj.EmployeeSurname);
        this.debriefSearchParams.EmployeeCode = obj.EmployeeCode;
    }

    public setContractNumber(obj: any): void {
        this.setControlValue('ContractNumber', obj.ContractNumber);
        this.setControlValue('ContractName', obj.ContractName);
        this.inputParamsAccountPremise.ContractNumber = obj.ContractNumber;
        this.inputParamsAccountPremise.ContractName = obj.ContractName;
    }

    public onPremiseSearchDataReceived(obj: any): void {
        this.setControlValue('PremiseNumber', obj.PremiseNumber);
        this.setControlValue('PremiseName', obj.PremiseName);
    }

    public debriefOnchange(obj: any): void {
        this.setControlValue('DebriefNumber', obj.DebriefNumber);
        this.setControlValue('DebriefFromDate', obj.DebriefFromDate);
        this.setControlValue('DebriefToDate', obj.DebriefToDate);
    }
}
