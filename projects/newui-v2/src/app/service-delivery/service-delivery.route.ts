import { ServiceDocketDataEntryComponent } from './VisitMaintenance/iCABSSeServiceDocketDataEntry';
import { ServiceDeliveryModuleRoutes } from './../base/PageRoutes';
import { ServicePlanDeliveryNoteGridComponent } from './DeliveryPaperwork/iCABSSeServicePlanDeliveryNoteGrid.component';
import { SeDebriefEmployeeGridComponent } from './ProcessServiceActionMonitor/iCABSSeDebriefEmployeeGrid';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceDeliveryRootComponent } from './service-delivery.component';
import { TechnicianWorkSummaryComponent } from './PDAReturns/TechWorkGrid/iCABSSeTechnicianWorkSummaryGrid.component';
import { TechnicianSyncSummaryGridComponent } from './PDAReturns/iCABSSeTechnicianSyncSummaryGrid.component';
import { HCARiskAssessmentGridComponent } from './PDAReturns/RiskAssessment/iCABSSeHCARiskAssessmentGrid.component';
import { ServicePlanDeliveryNoteProductComponent } from './DeliveryPaperwork/SingleServiceReceipt/iCABSSeServicePlanDeliveryNotebyProduct.component';
import { InstallReceiptComponent } from './DeliveryPaperwork/iCABSSInstallReceipt.component';
import { WorkListConfirmComponent } from './DeliveryPaperwork/Work-list/iCABSWorkListConfirm.component';
import { SePESVisitGridComponent } from './PDAReturns/iCABSSePESVisitGrid.component';
import { ProductSalesDeliveriesDueGridComponent } from './ReportsPlanning/iCABSAProductSalesDeliveriesDueGrid.component';
import { BranchServiceAreaGridComponent } from './TableMaintenanceBusiness/ServiceArea/iCABSBBranchServiceAreaGrid.component';
import { ServiceCallTypeGridComponent } from './ReportsWorkload/ServiceCallTypeBranch/iCABSServiceCallTypeGrid.component';
import { ProductLanguageMaintenanceComponent } from './Translation/Product Language/iCABSBProductLanguageMaintenance.component';
import { ValidLinkedProductsGridComponent } from './TableMaintenanceProduct/ValidLinkedProducts/iCABSBValidLinkedProductsGrid.component';
import { ARPrenotificationReportComponent } from './ProcessWasteConsignmentNote/AnnualPrenotificationReport/iCABSARPrenotificationReport.component';
import { RouteAwayGuardService } from './../../shared/services/route-away-guard.service';
import { DailyPrenotificationReportComponent } from './ProcessWasteConsignmentNote/DailyPrenotificationReport/iCABSARDailyPrenotificationReport.component';
import { SeDebriefBranchGridComponent } from './ProcessServiceActionMonitor/iCABSSeDebriefBranchGrid.component';
import { VanLoadingReportComponent } from './DeliveryPaperwork/VanLoadingReport/iCABSARVanLoadingReport.component';
import { BlankConsignmentNotePrintComponent } from './ProcessWasteConsignmentNote/BlankConsignmentNotes/iCABSSeBlankConsignmentNotePrint.component';
import { GroupServiceVisitEntryGridComponent } from './VisitMaintenance/GroupVisitMaintenance/iCABSSeGroupServiceVisitEntryGrid.component';
import { PDAWorkListEntryGridComponent } from './VisitMaintenance/iCABSSePDAWorkListEntryGrid.component';
import { ReturnedPaperWorkGridComponent } from './../contract-management/LettersAndLabels/Non-ReturnedPaperworkAudit/iCABSARReturnedPaperWorkGrid.component';
import { ServiceVisitRejectionsGridComponent } from './PDAReturns/VisitRejections/iCABSSeServiceVisitRejectionsGrid';
import { AREnvAgencyBusinessWasteComponent } from './ProcessWasteConsignmentNote/EnvironmentAgencyBusinessWasteGenerationReport/iCABSAREnvAgencyBusinessWaste.component';
import { ServiceAreaAllocationGridComponent } from './ProcessAreaMaintenance/ServiceAreaAllocation/iCABSBServiceAreaAllocationGrid.component';
import { EnvAgencyExceptionsComponent } from './ProcessWasteConsignmentNote/EnvironmentAgencyExceptions/iCABSAREnvAgencyExceptions.component';
import { EnvAgencyQuarterlyReturnComponent } from './ProcessWasteConsignmentNote/envagencyquarterly/iCABSAREnvAgencyQuarterlyReturn.component';
import { WasteTransferNotesPrintComponent } from './LettersAndLabels/WasteTransferNotes/iCABSARWasteTransferNotesPrint.component';
import { WasteConsignmentNoteGenerateComponent } from './ProcessWasteConsignmentNote/WasteConsignmentNoteGeneration/iCABSSeWasteConsignmentNoteGenerate.component';
import { VisitActionMaintenanceComponent } from './TableMaintenanceBusiness/VisitAction/iCABSBVisitActionMaintenance.component';
import { WasteConsignmentNoteGenerateDetailComponent } from './ProcessWasteConsignmentNote/WasteConsignmentNoteGeneration/iCABSSeWasteConsignmentNoteGenerateDetail.component';
import { BranchServiceAreaEmployeeGridComponent } from './../internal/grid-search/iCABSBBranchServiceAreaEmployeeGrid.component';

const routes: Routes = [
    {
        path: '', component: ServiceDeliveryRootComponent, children: [
            { path: 'pdareturns/techniciansyncsummary', component: TechnicianSyncSummaryGridComponent },
            { path: 'techWorkGridService/TechnicianWorkSummaryGrid', component: TechnicianWorkSummaryComponent },
            { path: 'paperwork/servicelisting', component: ServicePlanDeliveryNoteGridComponent },
            { path: 'paperwork/singleServiceReceipt', component: ServicePlanDeliveryNoteProductComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSERVICECALLTYPEGRIDBUSINESS, component: ServiceCallTypeGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSERVICECALLTYPEGRIDREGION, component: ServiceCallTypeGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSERVICECALLTYPEGRIDBRANCH, component: ServiceCallTypeGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSERVICECALLTYPEGRIDSERVICEAREA, component: ServiceCallTypeGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSESERVICEDOCKETDATAENTRY, component: ServiceDocketDataEntryComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSEPESVISITGRID, component: SePESVisitGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSWORKLISTCONFIRM, component: WorkListConfirmComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSAPRODUCTSALESDELIVERIESDUEGRID, component: ProductSalesDeliveriesDueGridComponent },
            { path: 'installReceipt', component: InstallReceiptComponent },
            { path: ServiceDeliveryModuleRoutes.iCABSARENVAGENCYEXCEPTIONS, component: EnvAgencyExceptionsComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSARPRENOTIFICATIONREPORT, component: ARPrenotificationReportComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSBBRANCHSERVICEAREAGRID, component: BranchServiceAreaGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSEDEBRIEFBRANCHGRID, component: SeDebriefBranchGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSEDEBRIEFEMPLOYEEGRID, component: SeDebriefEmployeeGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSEHCARISKASSESSMENTGRID, component: HCARiskAssessmentGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSBVALIDLINKEDPRODUCTSGRID, component: ValidLinkedProductsGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSARDAILYPRENOTIFICATIONREPORT, component: DailyPrenotificationReportComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSBPRODUCTLANGUAGEMAINTENANCE, component: ProductLanguageMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: ServiceDeliveryModuleRoutes.ICABSARVANLOADINGREPORT, component: VanLoadingReportComponent, canDeactivate: [RouteAwayGuardService] },
            { path: ServiceDeliveryModuleRoutes.ICABSSEBLANKCONSIGNMENTNOTEPRINT, component: BlankConsignmentNotePrintComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSEGROUPSERVICEVISITENTRYGRID, component: GroupServiceVisitEntryGridComponent, canDeactivate: [RouteAwayGuardService] },
            { path: ServiceDeliveryModuleRoutes.ICABSSEPDAWORKLISTENTRYGRID, component: PDAWorkListEntryGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSARRETURNEDPAPERWORKGRID, component: ReturnedPaperWorkGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSESERVICEVISITREJECTIONSGRID, component: ServiceVisitRejectionsGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSESERVICEVISITREJECTIONSGRID_ALL, component: ServiceVisitRejectionsGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSARENVAGENCYBUSINESSWASTE, component: AREnvAgencyBusinessWasteComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSBSERVICEAREAALLOCATIONGRID, component: ServiceAreaAllocationGridComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSARWASTETRANSFERNOTESPRINT, component: WasteTransferNotesPrintComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSARENVAGENCYQUARTERLYRETURN, component: EnvAgencyQuarterlyReturnComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSSEWASTECONSIGNMENTNOTEGENERATE, component: WasteConsignmentNoteGenerateComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSBVISITACTIONMAINTENANCE, component: VisitActionMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: ServiceDeliveryModuleRoutes.ICABSSEWASTECONSIGNMENTNOTEGENERATEDETAIL, component: WasteConsignmentNoteGenerateDetailComponent },
            { path: ServiceDeliveryModuleRoutes.ICABSBBRANCHSERVICEAREAEMPLOYEEGRID, component: BranchServiceAreaEmployeeGridComponent }
        ], data: { domain: 'SERVICE DELIVERY' }
    }
];

export const ServiceDeliveryRouteDefinitions: ModuleWithProviders = RouterModule.forChild(routes);

