import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { BaseComponent } from './../../../base/BaseComponent';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { ScreenNotReadyComponent } from '../../../../shared/components/screenNotReady';
@Component({
    templateUrl: 'iCABSBVisitActionMaintenance.html'
})

export class VisitActionMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    public pageId: string = '';
    public controls = [
        { name: 'VisitTypeCode', required: true, type: MntConst.eTypeCode },
        { name: 'VisitTypeDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'SystemVisitActionCode', required: true, type: MntConst.eTypeCode, commonValidator: true },
        { name: 'SystemVisitActionDesc', disabled: true, type: MntConst.eTypeText }
    ];

    public dropdown: any = {
        visitActionSearch: {
            inputParams: {
                parentMode: 'Search',
                VisitTypeCode: ''
            },
            activeSelected: {
                id: '',
                text: ''
            },
            isRequired: true,
            isFirstItemSelected: false,
            isTriggerValidate: false
        },
        visitTypeSearch: {
            inputParams: {
                parentMode: 'LookUp'
            },
            activeSelected: {
                id: '',
                text: ''
            },
            isRequired: true,
            isFirstItemSelected: false,
            isTriggerValidate: false
        }
    };
    public systemVisitActionCodeComponent: any = ScreenNotReadyComponent;
    public queryParams: any = {
        operation: 'Business/iCABSBVisitActionMaintenance',
        module: 'service',
        method: 'service-delivery/admin'
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBVISITACTIONMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Visit Action Maintenance';
    }
    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            this.populateUIFromFormData();
        } else {
            this.windowOnload();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.routeAwayGlobals.resetRouteAwayFlags();
    }

    private windowOnload(): void {
        this.setCurrentContractType();
        switch (this.parentMode) {
            case 'Search':
            case 'SearchAdd':
                this.riExchange.getParentHTMLValue('VisitTypeCode');
                this.riExchange.getParentHTMLValue('VisitTypeDesc');
                if (this.getControlValue('VisitTypeCode') && this.getControlValue('VisitTypeDesc')) {
                    this.dropdown.visitActionSearch.activeSelected = {
                        id: this.getControlValue('VisitTypeCode'),
                        text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
                    };
                }
                this.setControlValue('SystemVisitActionCode', this.riExchange.getParentHTMLValue('SystemVisitActionCode') || '');
                this.pageParams.visitTypeCode = this.getControlValue('VisitTypeCode');
                this.pageParams.systemVisitActionCode = this.getControlValue('SystemVisitActionCode');
                this.pageParams.getVisitTypeCode = this.riExchange.getParentHTMLValue('VisitTypeCode') ? true : false;
                break;
        }
        switch (this.parentMode) {
            case 'Search':
                this.riMaintenanceFetchRecord();
                break;
            case 'SearchAdd':
                this.riMaintenanceFetchRecord();
                this.riMaintenanceAddMode();
                break;
            default:
                this.riMaintenanceFetchRecord();
                break;
        }
        this.riMaintenanceExecMode(this.pageParams.riMaintenanceCurrentMode);

    }

    private setCurrentContractType(): void {
        this.pageParams.currentContractType = this.riExchange.getCurrentContractType();
        this.pageParams.currentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
    }

    private riMaintenanceFetchRecord(): void {
        this.pageParams.riMaintenanceCurrentMode = MntConst.eModeFetch;
        if (this.pageParams.visitTypeCode && this.pageParams.systemVisitActionCode) {
            this.setControlValue('VisitTypeCode', this.pageParams.visitTypeCode);
            this.setControlValue('SystemVisitActionCode', this.pageParams.systemVisitActionCode);
        }
        if (this.getControlValue('VisitTypeCode') && this.getControlValue('SystemVisitActionCode')) { this.riMaintenanceAddTableCommit(); }
    }

    private riMaintenanceAddTableCommit(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        search.set('VisitTypeCode', this.getControlValue('VisitTypeCode') || '');
        search.set('SystemVisitActionCode', this.getControlValue('SystemVisitActionCode') || '');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
            .subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        this.setControlValue('SystemVisitActionCode', '');
                        this.setControlValue('SystemVisitActionDesc', '');
                    } else {
                        this.pageParams.rOWID = e.ttVisitAction;
                        this.setControlValue('SystemVisitActionCode', e.SystemVisitActionCode);
                        this.setControlValue('SystemVisitActionDesc', e.SystemVisitActionDesc);
                        this.riMaintenanceAddVirtualTableCommit();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.setControlValue('SystemVisitActionCode', '');
                    this.setControlValue('SystemVisitActionDesc', '');
                });
    }

    private riMaintenanceAddVirtualTableCommit(): void {
        if (this.getControlValue('VisitTypeCode') && this.getControlValue('SystemVisitActionCode')) {
            let lookupIP = [
                {
                    'table': 'VisitType',
                    'query': {
                        'BusinessCode': this.businessCode(),
                        'Languagecode': this.riExchange.LanguageCode(),
                        'VisitTypeCode': this.getControlValue('VisitTypeCode')
                    },
                    'fields': ['VisitTypeDesc']
                },
                {
                    'table': 'SystemVisitActionLang',
                    'query': {
                        'BusinessCode': this.businessCode(),
                        'BranchNumber': this.utils.getBranchCode(),
                        'Languagecode': this.riExchange.LanguageCode(),
                        'SystemVisitActionCode': this.getControlValue('SystemVisitActionCode')
                    },
                    'fields': ['SystemVisitActionDesc']
                }
            ];

            this.ajaxSource.next(this.ajaxconstant.START);
            this.LookUp.lookUpPromise(lookupIP).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    if (data && data.length > 0) {
                        if (data[0] && data[0].length > 0 && data[0][0]) {
                            if (this.getControlValue('VisitTypeCode')) {
                                this.setControlValue('VisitTypeDesc', data[0][0].VisitTypeDesc || '');
                                this.pageParams.visitTypeROWID = data[0][0].ttVisitType;
                                this.dropdown.visitActionSearch.activeSelected = {
                                    id: this.getControlValue('VisitTypeCode'),
                                    text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
                                };
                                this.dropdown.visitTypeSearch.activeSelected = {
                                    id: this.getControlValue('VisitTypeCode'),
                                    text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
                                };
                            } else {
                                this.setControlValue('VisitTypeDesc', '');
                            }
                        } else {
                            this.setControlValue('VisitTypeCode', '');
                            this.setControlValue('VisitTypeDesc', '');
                        }
                        if (data[1] && data[1].length > 0 && data[1][0]) {
                            if (this.getControlValue('SystemVisitActionCode')) {
                                this.pageParams.systemVisitActionROWID = data[1][0].ttSystemVisitActionLang;
                                this.setControlValue('SystemVisitActionDesc', data[1][0].SystemVisitActionDesc || '');
                            } else {
                                this.setControlValue('SystemVisitActionDesc', '');
                            }
                        } else {
                            this.setControlValue('SystemVisitActionCode', '');
                            this.setControlValue('SystemVisitActionDesc', '');
                        }
                        this.pageParams.visitTypeDesc = this.getControlValue('VisitTypeDesc');
                        this.pageParams.systemVisitActionDesc = this.getControlValue('SystemVisitActionDesc');

                        if (this.pageParams.riMaintenanceCurrentMode === MntConst.eModeFetch) { this.setDefaultFormData(); }
                    } else {
                        this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                    }
                }
            }).catch(error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        } else {
            this.setControlValue('SystemVisitActionCode', '');
            this.setControlValue('SystemVisitActionDesc', '');
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
        }
    }

    private setDefaultFormData(): void {
        this.pageParams.visitTypeCode = this.getControlValue('VisitTypeCode');
        this.pageParams.systemVisitActionCode = this.getControlValue('SystemVisitActionCode');
    }

    private resetControl(): void {
        this.riExchange.resetCtrl(this.controls);
        this.riExchange.renderForm(this.uiForm, this.controls);
        this.dropdown.visitTypeSearch.activeSelected = {
            id: '',
            text: ''
        };
        if (this.dropdown.visitActionSearch.activeSelected.id) {
            this.setControlValue('VisitTypeCode', this.pageParams.visitTypeCode);
            this.setControlValue('VisitTypeDesc', this.pageParams.visitTypeDesc);
        }
        this.markAsPristine();
    }

    private markAsPristine(): void {
        this.controls.forEach((i) => {
            this.uiForm.controls[i.name].markAsPristine();
            this.uiForm.controls[i.name].markAsUntouched();
        });
        this.dropdown.visitActionSearch.isRequired = false;
        this.dropdown.visitTypeSearch.isRequired = false;
        this.dropdown.visitTypeSearch.isTriggerValidate = false;
        this.dropdown.visitActionSearch.isTriggerValidate = false;
    }

    private riMaintenanceExecMode(mode: string): void {
        switch (mode) {
            case 'eModeFetch':
                this.pageParams.isEnableSave = false;
                break;
            case 'eModeAdd':
                this.pageParams.isEnableSave = true;
                break;
            case 'eModeSaveAdd':
                this.confirm();
                break;
            case 'eModeDelete':
                this.confirmDelete();
                break;
        }
    }

    private riMaintenanceExecContinue(mode: string): void {
        switch (mode) {
            case 'eModeSaveAdd':
                this.riMaintenanceAfterSave();
                break;
            case 'eModeDelete':
                this.riMaintenanceAfterDelete();
                break;
        }
    }

    private riMaintenanceCancelEvent(mode: string): void {
        switch (mode) {
            case 'eModeAdd':
            case 'eModeSaveAdd':
                this.pageParams.isEnableSave = false;
                break;
            case 'eModeDelete':
                this.pageParams.isEnableSave = false;
                break;
        }
    }

    private riMaintenanceAfterSave(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.pageParams.actionAfterSave);

        let postDataAdd: Object = {};
        postDataAdd['VisitTypeCode'] = this.getControlValue('VisitTypeCode') || '';
        postDataAdd['SystemVisitActionCode'] = this.getControlValue('SystemVisitActionCode') || '';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postDataAdd)
            .subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    } else {
                        this.markAsPristine();
                        this.pageParams.rOWID = e.ttVisitAction;
                        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully);
                        modalVO.closeCallback = this.riMaintenanceAfterSaveAdd.bind(this);
                        this.modalAdvService.emitMessage(modalVO);
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private riMaintenanceAfterDelete(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.pageParams.actionAfterDelete);

        let formdata: Object = {};
        formdata['ROWID'] = this.pageParams.rOWID;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formdata)
            .subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        this.riMaintenanceFetchRecord();
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeleted));
                        this.pageParams.riMaintenanceCurrentMode = MntConst.eModeFetch;
                        this.resetControl();
                        this.setDefaultFormData();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.riMaintenanceFetchRecord();
                });
    }

    private riMaintenanceAfterSaveAdd(): void {
        this.setDefaultFormData();
        this.pageParams.isEnableSave = false;
        this.pageParams.riMaintenanceCurrentMode = MntConst.eModeFetch;
    }

    private riMaintenanceAddMode(): void {
        this.pageParams.riMaintenanceCurrentMode = MntConst.eModeAdd;
    }

    private riMaintenanceDeleteMode(): void {
        this.pageParams.riMaintenanceCurrentMode = MntConst.eModeDelete;
    }

    private confirm(): any {
        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.confirmed.bind(this));
        this.modalAdvService.emitPrompt(modalVO);
    }

    private confirmDelete(): any {
        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.confirmed.bind(this), this.cancelDelete.bind(this));
        this.modalAdvService.emitPrompt(modalVO);
    }
    private confirmed(obj: any): any {
        this.riMaintenanceExecContinue(this.pageParams.riMaintenanceCurrentMode);
    }
    private cancelDelete(obj: any): any {
        this.pageParams.riMaintenanceCurrentMode = MntConst.eModeFetch;
    }
    // validate uiForm
    private validateUiForm(): boolean {
        this.riExchange.validateForm(this.uiForm);
        if (this.getControlValue('VisitTypeCode')) {
            this.dropdown.visitActionSearch.isRequired = false;
            this.dropdown.visitTypeSearch.isRequired = false;
            this.dropdown.visitTypeSearch.isTriggerValidate = false;
            this.dropdown.visitActionSearch.isTriggerValidate = false;
        } else {
            this.dropdown.visitActionSearch.isRequired = true;
            this.dropdown.visitTypeSearch.isRequired = true;
            this.dropdown.visitTypeSearch.isTriggerValidate = true;
            this.dropdown.visitActionSearch.isTriggerValidate = true;
        }
        if (this.getControlValue('VisitTypeCode') && this.getControlValue('SystemVisitActionCode')) {
            return true;
        } else {
            return false;
        }
    }

    /*
    Method: cancel():
    Params:
    Details: Cancels your current action
    */
    public cancel(): void {
        this.resetControl();
        this.riMaintenanceFetchRecord();
        this.riMaintenanceExecMode(this.pageParams.riMaintenanceCurrentMode);
    }

    public add(): void {
        this.setDefaultFormData();
        this.resetControl();
        if (!this.pageParams.getVisitTypeCode) {
            this.setControlValue('VisitTypeCode', '');
        }
        this.setControlValue('SystemVisitActionCode', '');
        this.riMaintenanceAddMode();
        this.riMaintenanceExecMode(this.pageParams.riMaintenanceCurrentMode);
    }

    public save(): void {
        if (this.validateUiForm()) {
            switch (this.pageParams.riMaintenanceCurrentMode) {
                case 'eModeAdd':
                case 'eModeSaveAdd':
                    this.pageParams.actionAfterSave = 1;
                    this.pageParams.riMaintenanceCurrentMode = MntConst.eModeSaveAdd;
                    this.riMaintenanceExecMode(this.pageParams.riMaintenanceCurrentMode);
                    break;
            }
        }
    }

    public delete(): void {
        if (this.validateUiForm()) {
            this.riMaintenanceDeleteMode();
            this.pageParams.actionAfterDelete = 3;
            this.riMaintenanceExecMode(this.pageParams.riMaintenanceCurrentMode);
        } else {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.NoRecordSelected));
        }
    }

    public onvisitActionSearchDropdownDataRecieved(data: any): void {
        this.setControlValue('VisitTypeCode', data['VisitAction.SystemVisitActionCode']);
        this.setControlValue('VisitTypeDesc', data['SystemVisitActionLang.SystemVisitActionDesc']);
        this.clearControls(['VisitTypeCode', 'VisitTypeDesc']);
        this.uiForm.markAsDirty();
    }
    public onvisitTypeSearchDropdownDataRecieved(data: any): void {
        this.setControlValue('VisitTypeCode', data.VisitTypeCode);
        this.setControlValue('VisitTypeDesc', data.VisitTypeDesc);
        this.clearControls(['VisitTypeCode', 'VisitTypeDesc']);
        this.uiForm.markAsDirty();
    }

    public systemVisitActionCodeOnChange(): void {
        if (this.pageParams.riMaintenanceCurrentMode === MntConst.eModeFetch) {
            this.setDefaultFormData();
            this.riMaintenanceFetchRecord();
        } else {
            this.riMaintenanceAddVirtualTableCommit();
        }
    }
}
