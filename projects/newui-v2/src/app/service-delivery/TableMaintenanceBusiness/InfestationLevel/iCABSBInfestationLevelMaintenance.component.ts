import { Component, OnInit, Injector, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { InternalGridSearchServiceModuleRoutes } from './../../../base/PageRoutes';
import { EllipsisComponent } from './../../../../shared/components/ellipsis/ellipsis';
import { InfestationLevelSearchComponent } from './../../../internal/search/iCABSBInfestationLevelSearch.component';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBInfestationLevelMaintenance.html'
})

export class InfestationLevelMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('infestationLevelSearch') infestationLevelSearch: EllipsisComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private queryPost: QueryParams = this.getURLSearchParamObject();
    private muleConfig = {
        method: 'service-delivery/admin',
        module: 'service',
        operation: 'Business/iCABSBInfestationLevelMaintenance'
    };

    public pageId: string = '';
    public controls = [
        { name: 'InfestationGroupCode', required: true, disabled: false, type: MntConst.eTypeCode },
        { name: 'InfestationGroupDesc', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'InfestationLevelCode', required: true, disabled: false, type: MntConst.eTypeCode },
        { name: 'InfestationLevelDesc', required: true, disabled: true, type: MntConst.eTypeText },
        { name: 'Level', required: false, disabled: true, type: MntConst.eTypeText, value: '0' },
        { name: 'LevelScore', required: true, disabled: true, type: MntConst.eTypeInteger },
        { name: 'SortOrder', required: false, disabled: true, type: MntConst.eTypeInteger },
        { name: 'PassToPDAInd', required: false, disabled: true },
        { name: 'ExtraInputInd', required: false, disabled: true },
        { name: 'PassToTabletInd', required: false, disabled: true }
    ];
    public searchConfigs: any = {
        infestationGroupSearch: {
            isRequired: true,
            isDisabled: false,
            isTriggerValidate: false,
            params: {
                operation: 'Business/iCABSBInfestationGroupSearch',
                module: 'service',
                method: 'service-delivery/search'
            },
            displayFields: ['InfestationGroupCode', 'InfestationGroupDesc'],
            active: {
                id: '',
                text: ''
            }
        },
        infestationLevelSearch: {
            isDisabled: false,
            isAutoOpen: false,
            params: {
                parentMode: 'Search',
                InfestationGroupCode: '',
                InfestationGroupDesc: '',
                showAddNew: true
            },
            component: InfestationLevelSearchComponent
        }
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBINFESTATIONLEVELMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Infestation Level Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.setFormMode(this.c_s_MODE_UPDATE);
            this.searchConfigs.infestationLevelSearch.isAutoOpen = false;
            this.enableControls(['InfestationGroupCode', 'InfestationGroupDesc']);
            this.uiForm.controls['InfestationLevelCode'].disable();
            this.searchConfigs.infestationGroupSearch.active = {
                id: this.getControlValue('InfestationGroupCode'),
                text: this.getControlValue('InfestationGroupCode') + ' - ' + this.getControlValue('InfestationGroupDesc')
            };
            this.searchConfigs.infestationGroupSearch.isDisabled = true;
        } else {
            this.setFormMode(this.c_s_MODE_SELECT);
            this.searchConfigs.infestationLevelSearch.isAutoOpen = true;
        }
        this.searchConfigs.infestationLevelSearch.params.InfestationGroupCode = this.getControlValue('InfestationGroupCode');
        this.searchConfigs.infestationLevelSearch.params.InfestationGroupDesc = this.getControlValue('InfestationGroupDesc');
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // Fetching Visit Type Data
    private fetchInfestationGroupData(): void {
        if (this.formMode !== this.c_s_MODE_ADD) {
            this.formPristine();
            let queryGet: QueryParams = this.getURLSearchParamObject();
            queryGet.set(this.serviceConstants.Action, '0');
            queryGet.set(this.serviceConstants.BusinessCode, this.businessCode());
            queryGet.set(this.serviceConstants.InfestationGroupCode, this.getControlValue('InfestationGroupCode'));
            queryGet.set(this.serviceConstants.Mode, 'InfestationGroup');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryGet)
                .subscribe(
                    (data) => {
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            this.setFormMode(this.c_s_MODE_SELECT);
                            this.pageParams.mode = this.formMode;
                        } else {
                            this.pageParams.InfestationGroupCode = data.InfestationGroupCode;
                            this.pageParams.InfestationGroupDesc = data.InfestationGroupDesc;
                            // this.pageParams.PassToPDAInd = data.PassToPDAInd;
                            this.setControlValue('InfestationGroupCode', data.InfestationGroupCode);
                            this.setControlValue('InfestationGroupDesc', data.InfestationGroupDesc);
                            this.searchConfigs.infestationLevelSearch.params.InfestationGroupCode = this.getControlValue('InfestationGroupCode');
                            this.searchConfigs.infestationLevelSearch.params.InfestationGroupDesc = this.getControlValue('InfestationGroupDesc');
                            this.searchConfigs.infestationGroupSearch.active = {
                                id: this.getControlValue('InfestationGroupCode'),
                                text: this.getControlValue('InfestationGroupCode') + ' - ' + this.getControlValue('InfestationGroupDesc')
                            };
                            this.searchConfigs.infestationGroupSearch.isDisabled = true;
                        }
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }

    // Saving Infestation Level Data
    private saveInfestationLevel(): void {
        let formdata: any = {};
        if (this.formMode === this.c_s_MODE_ADD) {
            this.queryPost.set(this.serviceConstants.Action, '1');
            formdata = {
                InfestationGroupCode: this.getControlValue('InfestationGroupCode'),
                InfestationLevelCode: this.getControlValue('InfestationLevelCode'),
                InfestationLevelDesc: this.getControlValue('InfestationLevelDesc'),
                Level: this.getControlValue('Level'),
                LevelScore: this.getControlValue('LevelScore'),
                SortOrder: this.getControlValue('SortOrder'),
                PassToPDAInd: this.getControlValue('PassToPDAInd'),
                ExtraInputInd: this.getControlValue('ExtraInputInd'),
                PassToTabletInd: this.getControlValue('PassToTabletInd')
            };
        } else {
            this.queryPost.set(this.serviceConstants.Action, '2');
            formdata = {
                ROWID: this.pageParams.ttInfestationLevel,
                InfestationLevelDesc: this.getControlValue('InfestationLevelDesc'),
                Level: this.getControlValue('Level'),
                LevelScore: this.getControlValue('LevelScore'),
                SortOrder: this.getControlValue('SortOrder'),
                PassToPDAInd: this.getControlValue('PassToPDAInd'),
                ExtraInputInd: this.getControlValue('ExtraInputInd'),
                PassToTabletInd: this.getControlValue('PassToTabletInd')
            };
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                        this.pageParams = data;
                        this.searchConfigs.infestationLevelSearch.isAutoOpen = false;
                        this.searchConfigs.infestationLevelSearch.isDisabled = false;
                        this.searchConfigs.infestationGroupSearch.isDisabled = true;
                        this.uiForm.controls['InfestationLevelCode'].disable();
                        this.setFormMode(this.c_s_MODE_UPDATE);
                        this.formPristine();
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    // Deleting Infestation Level Data
    private deleteInfestationLevel(): void {
        this.queryPost.set(this.serviceConstants.Action, '3');
        let formdata: any = {
            BusinessCode: this.businessCode(),
            ROWID: this.pageParams.ttInfestationLevel
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully));
                        this.pageParams = {};
                        this.uiForm.reset();
                        this.setFormMode(this.c_s_MODE_SELECT);
                        this.disableControls(['InfestationGroupCode', 'InfestationLevelCode']);
                        this.uiForm.controls['InfestationGroupCode'].enable();
                        this.uiForm.controls['InfestationLevelCode'].enable();
                        this.searchConfigs.infestationGroupSearch.isDisabled = false;
                        this.searchConfigs.infestationLevelSearch.isDisabled = false;
                        this.searchConfigs.infestationLevelSearch.params.InfestationGroupCode = this.getControlValue('InfestationGroupCode');
                        this.searchConfigs.infestationLevelSearch.params.InfestationGroupDesc = this.getControlValue('InfestationGroupDesc');
                        this.searchConfigs.infestationGroupSearch.active = {
                            id: '',
                            text: ''
                        };
                        this.setFormMode(this.c_s_MODE_SELECT);
                        this.formPristine();
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    // Marking Controls as Touched
    private uiFormMarkAsTouched(): void {
        for (let control of this.controls) {
            this.uiForm.controls[control.name].markAsTouched();
        }
    }

    // Fetching Visit Type Data
    public fetchInfestationLevelData(): void {
        if (this.formMode !== this.c_s_MODE_ADD) {
            this.formPristine();
            let queryGet: QueryParams = this.getURLSearchParamObject();
            queryGet.set(this.serviceConstants.Action, '0');
            queryGet.set(this.serviceConstants.BusinessCode, this.businessCode());
            queryGet.set(this.serviceConstants.InfestationGroupCode, this.getControlValue('InfestationGroupCode'));
            queryGet.set(this.serviceConstants.InfestationLevelCode, this.getControlValue('InfestationLevelCode'));
            queryGet.set(this.serviceConstants.Mode, 'InfestationLevel');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryGet)
                .subscribe(
                    (data) => {
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            this.setFormMode(this.c_s_MODE_SELECT);
                            this.pageParams.mode = this.formMode;
                        } else {
                            this.setFormMode(this.c_s_MODE_UPDATE);
                            this.pageParams.InfestationGroupCode = data.InfestationGroupCode;
                            this.pageParams.PassToPDAInd = data.PassToPDAInd;
                            this.pageParams.InfestationLevelCode = data.InfestationLevelCode;
                            this.pageParams.InfestationLevelDesc = data.InfestationLevelDesc;
                            this.pageParams.ExtraInputInd = data.ExtraInputInd;
                            this.pageParams.SortOrder = data.SortOrder;
                            this.pageParams.PassToTabletInd = data.PassToTabletInd;
                            this.pageParams.Level = data.Level;
                            this.pageParams.LevelScore = data.LevelScore;
                            this.pageParams.ttInfestationLevel = data.ttInfestationLevel;

                            this.setControlValue('InfestationGroupCode', data.InfestationGroupCode);
                            this.setControlValue('PassToPDAInd', data.PassToPDAInd);
                            this.setControlValue('InfestationLevelCode', data.InfestationLevelCode);
                            this.setControlValue('InfestationLevelDesc', data.InfestationLevelDesc);
                            this.setControlValue('ExtraInputInd', data.ExtraInputInd);
                            this.setControlValue('SortOrder', data.SortOrder);
                            this.setControlValue('PassToTabletInd', data.PassToTabletInd);
                            this.setControlValue('Level', data.Level);
                            this.setControlValue('LevelScore', data.LevelScore);
                            this.fetchInfestationGroupData();

                            this.enableControls(['InfestationGroupCode', 'InfestationGroupDesc']);
                            this.uiForm.controls['InfestationLevelCode'].disable();
                            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'InfestationLevelDesc', true);
                            this.fetchInfestationGroupData();
                        }
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }

    public setInfestationGroupData(data: any): void {
        if (data && data.InfestationGroupCode) {
            this.setControlValue('InfestationGroupCode', data.InfestationGroupCode);
            this.setControlValue('InfestationGroupDesc', data.InfestationGroupDesc);
            this.searchConfigs.infestationLevelSearch.params.InfestationGroupCode = this.getControlValue('InfestationGroupCode');
            this.searchConfigs.infestationLevelSearch.params.InfestationGroupDesc = this.getControlValue('InfestationGroupDesc');
            this.searchConfigs.infestationGroupSearch.active = {
                id: this.getControlValue('InfestationGroupCode'),
                text: this.getControlValue('InfestationGroupCode') + ' - ' + this.getControlValue('InfestationGroupDesc')
            };
            if (this.formMode === this.c_s_MODE_ADD) {
                this.uiForm.controls['InfestationGroupCode'].markAsDirty();
            }
        }
    }

    public setInfestationLevelData(data: any): void {
        if (data) {
            if (data.AddMode) {
                this.addNewInfestationLevel();
            } else {
                if (data.InfestationGroupCode) {
                    this.setControlValue('InfestationGroupCode', data.InfestationGroupCode);
                }
                this.setControlValue('InfestationLevelCode', data.InfestationLevelCode);
                this.fetchInfestationLevelData();
            }
        }
    }

    // Add New Infestation Level
    public addNewInfestationLevel(): void {
        this.setFormMode(this.c_s_MODE_ADD);
        this.uiForm.reset();
        this.pageParams = {};
        this.enableControls(['InfestationGroupDesc']);
        this.searchConfigs.infestationLevelSearch.params.InfestationGroupCode = '';
        this.searchConfigs.infestationLevelSearch.params.InfestationGroupDesc = '';
        this.searchConfigs.infestationGroupSearch.isDisabled = false;
        this.searchConfigs.infestationGroupSearch.isRequired = true;
        this.searchConfigs.infestationGroupSearch.active = {
            id: '',
            text: ''
        };
        this.searchConfigs.infestationLevelSearch.isDisabled = true;
        this.setControlValue('Level', '0');
        this.uiForm.controls['InfestationLevelDesc'].enable();
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'InfestationLevelDesc', true);
    }

    public promptModalForSave(): void {
        if (this.uiForm.valid) {
            let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveInfestationLevel.bind(this));
            this.modalAdvService.emitPrompt(modalVO);
        } else {
            if (this.uiForm.controls['InfestationGroupCode'].invalid) {
                this.searchConfigs.infestationGroupSearch.isTriggerValidate = true;
            } else {
                this.searchConfigs.infestationGroupSearch.isTriggerValidate = false;
            }
            this.uiFormMarkAsTouched();
        }
    }

    public promptModalForDelete(): void {
        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteInfestationLevel.bind(this));
        this.modalAdvService.emitPrompt(modalVO);
    }

    // Cancel Button operation
    public cancelInfestationLevel(): void {
        if (this.formMode !== this.c_s_MODE_SELECT) {
            if (this.formMode === this.c_s_MODE_ADD) {
                this.uiForm.reset();
                this.disableControls(['InfestationGroupCode', 'InfestationLevelCode']);
                this.searchConfigs.infestationLevelSearch.params.InfestationGroupCode = '';
                this.searchConfigs.infestationLevelSearch.params.InfestationGroupDesc = '';
                this.searchConfigs.infestationGroupSearch.active = {
                    id: '',
                    text: ''
                };
                this.searchConfigs.infestationGroupSearch.isRequired = false;
                this.searchConfigs.infestationGroupSearch.isTriggerValidate = false;
                this.searchConfigs.infestationLevelSearch.isAutoOpen = true;
                this.searchConfigs.infestationLevelSearch.isDisabled = false;
                this.setFormMode(this.c_s_MODE_SELECT);
            } else {
                this.setFormMode(this.c_s_MODE_UPDATE);
                this.setControlValue('InfestationGroupCode', this.pageParams.InfestationGroupCode);
                this.setControlValue('InfestationGroupDesc', this.pageParams.InfestationGroupDesc);
                this.setControlValue('InfestationLevelCode', this.pageParams.InfestationLevelCode);
                this.setControlValue('InfestationLevelDesc', this.pageParams.InfestationLevelDesc);
                this.setControlValue('Level', this.pageParams.Level);
                this.setControlValue('LevelScore', this.pageParams.LevelScore);
                this.setControlValue('SortOrder', this.pageParams.SortOrder);
                this.setControlValue('PassToPDAInd', this.pageParams.PassToPDAInd);
                this.setControlValue('ExtraInputInd', this.pageParams.ExtraInputInd);
                this.setControlValue('PassToTabletInd', this.pageParams.PassToTabletInd);
            }
        }
        this.formPristine();
    }

    // Option Dropdown selection
    public selectedOption(optionValue: string): void {
        switch (optionValue) {
            case 'Add':
                this.navigate('ContractInfestationTolerance', InternalGridSearchServiceModuleRoutes.ICABSSINFESTATIONTOLERANCEGRID);
                break;
        }
    }
}
