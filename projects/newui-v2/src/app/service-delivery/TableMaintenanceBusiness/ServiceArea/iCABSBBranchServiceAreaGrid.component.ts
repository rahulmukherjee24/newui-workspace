import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, OnInit, OnDestroy, Injector, ViewChild } from '@angular/core';

import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { BaseComponent } from '../../../../app/base/BaseComponent';
import { InternalMaintenanceServiceModuleRoutes } from '../../../base/PageRoutes';


@Component({
    templateUrl: 'iCABSBBranchServiceAreaGrid.html'
})

export class BranchServiceAreaGridComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private queryParams: any = {
        operation: 'Business/iCABSBBranchServiceAreaGrid',
        module: 'service',
        method: 'service-delivery/maintenance'
    };

    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'SearchType', value: 'Code' },
        { name: 'SearchValue' },
        { name: 'ActiveOnly' },
        { name: 'BranchNumber', disabled: true },
        { name: 'BranchName', disabled: true },
        { name: 'menu' },
        { name: 'BusinessCode', disabled: true },
        { name: 'BusinessDesc', disabled: true }
    ];

    public pageSize: number = 10;
    public pageCurrent: number = 1;
    public totalRecords: number = 0;
    public itemsTotal: number = 0;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBBRANCHSERVICEAREAGRID;
        this.pageTitle = this.browserTitle = 'Branch Service Area';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.setControlValue('BusinessCode', this.businessCode());
        this.setControlValue('BusinessDesc', this.utils.getBusinessText());
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchTextOnly());
        this.setControlValue('ActiveOnly', 'true');
        this.windowOnload();
        this.refresh();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public windowOnload(): void {
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = 10;
        this.buildGrid();
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('BranchServiceAreaCode', 'BranchServiceArea', 'BranchServiceAreaCode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumn('BranchServiceAreaDesc', 'BranchServiceArea', 'BranchServiceAreaDesc', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('EmployeeCode', 'BranchServiceArea', 'EmployeeCode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumn('EmployeeSurname', 'BranchServiceArea', 'EmployeeSurname', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('LiveArea', 'BranchServiceArea', 'LiveArea', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnOrderable('BranchServiceAreaCode', true);
        this.riGrid.AddColumnOrderable('EmployeeCode', true);
        this.riGrid.Complete();
    }

    public riGridBeforeExecute(): void {
        let gridParams: QueryParams = new QueryParams();
        gridParams.set(this.serviceConstants.Action, '2');
        gridParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        gridParams.set(this.serviceConstants.CountryCode, this.countryCode());
        gridParams.set('BranchNumber', this.utils.getBranchCode());
        gridParams.set('ActiveOnly', this.getControlValue('ActiveOnly'));
        gridParams.set('SearchType', this.getControlValue('SearchType'));
        gridParams.set('SearchValue', this.getControlValue('SearchValue'));
        gridParams.set(this.serviceConstants.GridMode, '0');
        gridParams.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        gridParams.set(this.serviceConstants.PageSize, this.pageSize.toString());
        gridParams.set(this.serviceConstants.PageCurrent, this.pageCurrent.toString());
        gridParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridParams.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridParams)
            .subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e['errorMessage'], e['fullerror']));
                        return;
                    }
                    this.pageCurrent = e.pageData ? e.pageData.pageNumber : 1;
                    this.totalRecords = e.pageData ? e.pageData.lastPageNumber * this.pageSize : 1;
                    this.riGrid.RefreshRequired();
                    this.riGrid.Execute(e);
                    this.itemsTotal = this.totalRecords;
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public refresh(): void {
        this.riGridBeforeExecute();
    }

    public riGridSort(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public menuOnchange(event: any): void {
        if (event.target.value === 'Add') {
            this.setControlValue('menu', '');
            /* navigate to page : iCABSBBranchServiceAreaMaintenance.html*/
            this.navigate('BranchServiceAreaAdd', InternalMaintenanceServiceModuleRoutes.ICABSBBRANCHSERVICEAREAMAINTENANCE, {
                parentMode: 'BranchServiceAreaAdd'
            });
        }
    }

    public riGridBodyOndblClick(): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'BranchServiceAreaCode':
                /* navigate to page : iCABSBBranchServiceAreaMaintenance.html*/
                this.navigate('BranchServiceAreaUpdate', InternalMaintenanceServiceModuleRoutes.ICABSBBRANCHSERVICEAREAMAINTENANCE, {
                    parentMode: 'BranchServiceAreaUpdate',
                    rowID: this.riGrid.Details.GetAttribute('BranchServiceAreaCode', 'rowID')
                });
                break;
        }
    }

    public riGridAfterExecute(): void {
        if (this.riGrid.HTMLGridBody) {
            if (this.riGrid.HTMLGridBody.children[0]) {
                if (this.riGrid.HTMLGridBody.children[0].children[0]) {
                    if (this.riGrid.HTMLGridBody.children[0].children[0].children[0]) {
                        this.gridFocus(this.riGrid.HTMLGridBody.children[0].children[0].children[0]);
                    }
                }
            }
        }
    }

    public gridFocus(rsrcElement: any): void {
        let oTR: any;
        if (rsrcElement) {
            oTR = rsrcElement.parentElement.parentElement.parentElement;
            this.setAttribute('Sequence', rsrcElement.value);
            this.setAttribute('RowID', rsrcElement.RowID);
            this.setAttribute('Row', oTR.sectionRowIndex);
            rsrcElement.focus();
        }
    }

    public getCurrentPage(event: any): void {
        this.pageCurrent = event.value;
        this.riGridBeforeExecute();
    }
}
