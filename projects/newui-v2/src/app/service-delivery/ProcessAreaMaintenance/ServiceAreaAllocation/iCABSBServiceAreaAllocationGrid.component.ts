import { Component, Injector, OnInit, OnDestroy, AfterContentInit, ViewChild } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';

import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { EllipsisComponent } from './../../../../shared/components/ellipsis/ellipsis';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { BIReportsRoutes, ContractManagementModuleRoutes, InternalGridSearchSalesModuleRoutes } from '@app/base/PageRoutes';
import { DropdownStaticComponent } from './../../../../shared/components/dropdown-static/dropdownstatic';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { ServiceTypeSearchComponent } from './../../../internal/search/iCABSBServiceTypeSearch.component';
import { ProductServiceGroupSearchComponent } from './../../../internal/search/iCABSBProductServiceGroupSearch.component';
import { BranchServiceAreaSearchComponent } from './../../../internal/search/iCABSBBranchServiceAreaSearch';
import { IExportOptions } from '@app/base/ExportConfig';

@Component({
    templateUrl: 'iCABSBServiceAreaAllocationGrid.html'
})

export class ServiceAreaAllocationGridComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('serviceTypeSearchDropDown') serviceTypeSearchDropDown: ServiceTypeSearchComponent;
    @ViewChild('productServiceGroupSearchDropDown') productServiceGroupSearchDropDown: ProductServiceGroupSearchComponent;
    @ViewChild('portfolioStatus') portfolioStatus: DropdownStaticComponent;
    @ViewChild('contractType') contractType: DropdownStaticComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('branchserviceareacodeEllipsis') branchserviceareacodeEllipsis: EllipsisComponent;

    private method: string = 'service-delivery/maintenance';
    private module: string = 'service';
    private operation: string = 'Business/iCABSBServiceAreaAllocationGrid';
    public enableVisitCycles: boolean;
    public enableWed: boolean;
    private blnCacheRefresh: boolean = true;
    private currentContractType: string;
    private currentContractTypeActual: string;
    private isFirstTimeportfolioStatus: boolean = true;
    private iscontractType: boolean = true;

    public ellipsis: any = {};
    public pageId: string = '';
    public totalRecords: number = 0;
    public curPage: number = 1;
    public isVisiblePagination: boolean = false;
    public contractTypeOptions: Array<any> = [];
    public portfolioStatusOptions: Array<any> = [];
    public exportConfig: IExportOptions = {};
    public inputParams: any = {
        params: {
            parentMode: 'LookUp'
        }
    };
    public controls = [
        { name: 'BranchServiceAreaCode' },
        { name: 'BranchServiceAreaDesc', disabled: true },
        { name: 'ServiceVisitFrequency' },
        { name: 'VisitCycleInWeeks' },
        { name: 'NextServiceVisitDateFrom', type: MntConst.eTypeDate },
        { name: 'NextServiceVisitDateTo', type: MntConst.eTypeDate },
        { name: 'ServiceTypeCode' },
        { name: 'ServiceTypeDesc' },
        { name: 'ProductServiceGroupCode' },
        { name: 'ProductServiceGroupDesc' },
        { name: 'PostcodeFilter' },
        { name: 'TownFilter' },
        { name: 'StateFilter' },
        { name: 'ContractTypeCode' },
        { name: 'PortfolioStatusType' },
        { name: 'SelectedPremiseNumber', disabled: true },
        { name: 'SelectedPremiseName', disabled: true },
        { name: 'SelectedProductService', disabled: true },
        { name: 'SelectedProductCode', disabled: true },
        { name: 'SelectedWED', disabled: true },
        { name: 'SelectedServiceType', disabled: true },
        { name: 'SelectedFrequency', disabled: true },
        { name: 'SelectedVisitCycle', disabled: true },
        { name: 'SelectedQuantity', disabled: true },
        { name: 'SelectedNextVisit', disabled: true, type: MntConst.eTypeDate },
        { name: 'SelectedPostcode', disabled: true },
        { name: 'SelectedServiceAreaCode' },
        { name: 'SelectedServiceAreaSeq' },
        { name: 'SelectedAnniversary', type: MntConst.eTypeDate },
        { name: 'ExcludeSelectedArea' },
        { name: 'ServiceCoverRowID' },
        { name: 'ServiceCoverRowID' },
        { name: 'EmployeeSurname' }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBSERVICEAREAALLOCATIONGRID;
        this.browserTitle = 'Service Area Allocation';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.productServiceGroupSearchDropDown.isRequired = false;
        this.getSysCharDtetails();
        this.createContractTypeOptions();
        this.createOptions();
        if (this.isReturning()) {
            if (this.pageParams.serviceTypeCode) {
                this.serviceTypeSearchDropDown.active = { id: this.pageParams.serviceTypeCode, text: this.pageParams.serviceTypeCode + ' - ' + this.pageParams.serviceTypeDesc };
            }
            if (this.pageParams.productServiceGroupCode) {
                this.productServiceGroupSearchDropDown.active = { id: this.pageParams.productServiceGroupCode, text: this.pageParams.productServiceGroupCode + ' - ' + this.pageParams.productServiceGroupDesc };
            }
            this.setControlValue('SelectedServiceAreaCode', this.pageParams.SelectedServiceAreaCode);
            this.setControlValue('SelectedServiceAreaSeq', this.pageParams.SelectedServiceAreaSeq);
            this.setControlValue('SelectedAnniversary', this.pageParams.SelectedAnniversary);
            this.iscontractType = false;
        }
        this.ellipsis = {
            branchServiceAreaCode: {
                childConfigParams: {
                    'parentMode': 'LookUp'
                },
                contentComponent: BranchServiceAreaSearchComponent,
                searchModalRoute: ''
            }
        };
    }

    public ngAfterContentInit(): void {
        if (!this.isReturning()) {
            this.portfolioStatus.selectedItem = 'Current';
            this.setControlValue('PortfolioStatusType', 'Current');
        } else {
            this.contractType.selectedItem = this.pageParams.contractType;
            this.portfolioStatus.selectedItem = this.pageParams.portfolioStatusType;
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /** to get the syschar records
     * @param void
     * @return void
     */
    private getSysCharDtetails(): any {
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharEnableWeeklyVisitPattern, //930
            this.sysCharConstants.SystemCharEnableWED //3370
        ];
        let sysCharIP: any = {
            module: this.module,
            operation: this.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };
        this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            if (data && data.records) {
                let record = data.records;
                this.enableVisitCycles = record[0]['Required'];
                this.enableWed = record[1]['Required'];
            } else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
            }
        });
    }

    /**
     * to create the options for the dropdowns
     * @param void
     * @return void
     */
    private createOptions(): void {
        this.portfolioStatusOptions = [
            { text: 'All', value: 'All' },
            { text: 'Current Portfolio', value: 'Current' },
            { text: 'Non Current Portfolio', value: 'NonCurrent' }
        ];
    }

    /**
     * to create the options for the ContractType dropdown
     * @param void
     * @return void
     */
    private createContractTypeOptions(): void {
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams[this.serviceConstants.Function] = 'GetBusinessContractTypes';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.contractTypeOptions.push({ text: 'All', value: 'All' });
                        this.pageParams.contractType = 'All';
                        let contractTypeOptionsArray: Array<string> = data.BusinessContractTypes.toString().split(',');
                        let i: number = 0, newServicePlanStatusArray: any = {}, lngth: number = 0, txt: string, desc: string;
                        lngth = contractTypeOptionsArray.length;
                        for (let i = 0; i < lngth; i++) {
                            switch (contractTypeOptionsArray[i]) {
                                case 'C':
                                    this.contractTypeOptions.push({ text: 'Contracts', value: 'C' });
                                    break;
                                case 'J':
                                    this.contractTypeOptions.push({ text: 'Jobs', value: 'J' });
                                    break;
                                case 'P':
                                    this.contractTypeOptions.push({ text: 'Product Sales', value: 'P' });
                                    break;
                            }
                        }
                    }
                    if (!this.isReturning()) {
                        this.contractType.selectedItem = 'All';
                        this.setControlValue('ContractTypeCode', 'All');
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    /**
     * add the columns of the grid
     * @param void
     * @return void
     */
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('BranchServiceAreaCode', 'Rezone', 'BranchServiceAreaCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'Rezone', 'BranchServiceAreaSeqNo', MntConst.eTypeInteger, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseNumber', 'Rezone', 'PremiseNumber', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseName', 'Rezone', 'PremiseName', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('ProductCode', 'Rezone', 'ProductCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceVisitFrequency', 'Rezone', 'ServiceVisitFrequency', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        if (this.enableVisitCycles) {
            this.riGrid.AddColumn('VisitCycleInWeeks', 'Rezone', 'VisitCycleInWeeks', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnAlign('VisitCycleInWeeks', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('NextServiceVisitDate', 'Rezone', 'NextServiceVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('NextServiceVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceVisitAnnivDate', 'Rezone', 'ServiceVisitAnnivDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServiceVisitAnnivDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceQuantity', 'Rezone', 'ServiceQuantity', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentRight);
        if (this.enableWed) {
            this.riGrid.AddColumn('ServiceWED', 'Rezone', 'ServiceWED', MntConst.eTypeDecimal1, 8);
            this.riGrid.AddColumnAlign('ServiceWED', MntConst.eAlignmentRight);
        }
        this.riGrid.AddColumn('ServiceAnnualValue', 'Rezone', 'ServiceAnnualValue', MntConst.eTypeTextFree, 10);
        this.riGrid.AddColumnAlign('ServiceAnnualValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('ServiceTypeCode', 'Rezone', 'ServiceTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('VisitsUnits', 'Rezone', 'VisitsUnits', MntConst.eTypeImage, 2);
        this.riGrid.AddColumnAlign('VisitsUnits', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ProductServiceGroupCode', 'Rezone', 'ProductServiceGroupCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('ProductServiceGroupCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremisePostcode', 'Rezone', 'PremisePostcode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('PremisePostcode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('PremiseState', 'Rezone', 'PremiseState', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('PremiseState', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('PremiseTown', 'Rezone', 'PremiseTown', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('PremiseTown', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('Selected', 'Rezone', 'Selected', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('Selected', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);
        this.riGrid.AddColumnOrderable('BranchServiceAreaCode', true);
        this.riGrid.AddColumnOrderable('ProductCode', true);
        this.riGrid.AddColumnOrderable('ServiceTypeCode', true);
        this.riGrid.AddColumnOrderable('VisitsUnits', true);
        this.riGrid.AddColumnOrderable('ProductServiceGroupCode', true);
        this.riGrid.AddColumnOrderable('PremisePostcode', true);
        this.riGrid.Complete();
        this.riGridBeforeExecute();

        this.exportConfig = {};
        this.dateCollist = this.riGrid.getColumnIndexList([
            'NextServiceVisitDate',
            'ServiceVisitAnnivDate'
        ]);
        if (this.dateCollist.length) {
            this.exportConfig.dateColumns = this.dateCollist;
        }
    }


    /**
     * This method is to fetch the grid records
     * @param void
     * @return void
     */
    private riGridBeforeExecute(): void {
        let search = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode') || '');
        search.set('ServiceVisitFrequency', this.getControlValue('ServiceVisitFrequency') || '');
        search.set('VisitCycleInWeeks', this.getControlValue('VisitCycleInWeeks') || '');
        search.set('NextServiceVisitDateFrom', this.getControlValue('NextServiceVisitDateFrom') || '');
        search.set('NextServiceVisitDateTo', this.getControlValue('NextServiceVisitDateTo') || '');
        search.set('ServiceTypeCode', this.getControlValue('ServiceTypeCode') || '');
        search.set('ProductServiceGroupCode', this.getControlValue('ProductServiceGroupCode') || '');
        search.set('PostcodeFilter', this.getControlValue('PostcodeFilter') || '');
        search.set('ContractTypeCode', this.getControlValue('ContractTypeCode') || 'All');
        search.set('PortfolioStatusType', this.getControlValue('PortfolioStatusType') || 'Current');
        search.set('ServiceCoverRowID', this.getControlValue('ServiceCoverRowID') || '');
        search.set('SelectedPostcode', this.getControlValue('SelectedPostcode') || '');
        search.set('TownFilter', this.getControlValue('TownFilter') || '');
        if (this.riGrid.Update) {
            search.set('ROWID', this.getControlValue('ServiceCoverRowID') || '');
        }
        search.set('StateFilter', this.getControlValue('StateFilter') || '');
        if (this.getControlValue('ExcludeSelectedArea'))
            search.set('ExcludeSelectedArea', this.getControlValue('SelectedServiceAreaCode') || '');
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        if (this.blnCacheRefresh) {
            search.set(this.serviceConstants.GridCacheRefresh, 'true');
        } else {
            search.set(this.serviceConstants.GridCacheRefresh, 'false');
        }
        search.set(this.serviceConstants.PageSize, '10');
        search.set(this.serviceConstants.GridPageCurrent, this.curPage.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.method, this.module, this.operation, search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        if (this.riGrid.Update) {
                            this.riGrid.StartRow = this.getAttribute('Row');
                            this.riGrid.StartColumn = 0;
                            this.riGrid.RowID = this.getAttribute('ServiceCoverRowID');
                            this.riGrid.UpdateHeader = false;
                            this.riGrid.UpdateFooter = false;
                        } else {
                            this.curPage = data.pageData.pageNumber || 1;
                            this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                            this.isVisiblePagination = true;
                            if (!this.totalRecords) {
                                this.isVisiblePagination = false;
                            }
                            this.blnCacheRefresh = false;
                            this.riGrid.UpdateHeader = true;
                        }
                        this.riGrid.UpdateBody = true;
                        this.riGrid.Execute(data);
                        this.riGrid.Update = false;
                    }
                },
                error => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                    this.totalRecords = 0;
                    this.isVisiblePagination = false;
                });
    }

    private rezoneFocus(rsrcElement: any): void {
        rsrcElement.focus();
        this.setAttribute('SelectedServiceArea', this.riGrid.Details.GetValue('BranchServiceAreaCode'));
        this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute('Selected', 'rowid'));
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('PremiseNumber', 'rowid'));
        this.setAttribute('Row', this.riGrid.CurrentRow);
        this.currentContractTypeActual = this.riGrid.Details.GetAttribute('BranchServiceAreaCode', 'additionalproperty');
        switch (this.currentContractTypeActual) {
            case 'C':
                this.currentContractType = '';
                break;
            case 'J':
                this.currentContractType = '<job>';
                break;
            case 'P':
                this.currentContractType = '<product>';
                break;
        }
    }

    public doLookUpCallForProductGroup(): void {
        let lookupIP = [
            {
                'table': 'ProductServiceGroup',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ProductServiceGroupCode': this.getControlValue('ProductServiceGroupCode')
                },
                'fields': ['ProductServiceGroupDesc']
            }
        ];
        this.LookUp.lookUpPromise(lookupIP).then((data) => {
            let record = data[0];
            if (record.length > 0) {
                record = record[0];
                this.setControlValue('ProductServiceGroupDesc', record.ProductServiceGroupDesc);
            } else {
                this.setControlValue('ProductServiceGroupDesc', '');
            }
            this.productServiceGroupSearchDropDown.active = { id: this.getControlValue('ProductServiceGroupCode'), text: this.getControlValue('ProductServiceGroupCode') + ' - ' + this.getControlValue('ProductServiceGroupDesc') };
            this.pageParams.productServiceGroupCode = this.getControlValue('ProductServiceGroupCode');
            this.pageParams.productServiceGroupDesc = this.getControlValue('ProductServiceGroupDesc');
        });

    }

    public riGridBodyOnClick(event: Object): void {
        if (this.riGrid.CurrentColumnName === 'Selected' && !this.riGrid.RefreshRequiredStatus()) {
            if (this.getControlValue('ServiceCoverRowID')) {
                this.setControlValue('SelectedServiceAreaCode', this.riGrid.Details.GetValue('BranchServiceAreaCode'));
                this.setControlValue('SelectedServiceAreaSeq', this.riGrid.Details.GetValue('BranchServiceAreaSeqNo'));
                this.setControlValue('SelectedAnniversary', this.riGrid.Details.GetValue('ServiceVisitAnnivDate'));
            } else {
                this.rezoneFocus(event['srcElement']['parentElement']);
                this.setControlValue('ProductServiceGroupCode', this.riGrid.Details.GetValue('ProductServiceGroupCode'));
                this.setControlValue('ProductServiceGroupDesc', this.riGrid.Details.GetAttribute('ProductServiceGroupCode', 'ToolTip'));
                this.doLookUpCallForProductGroup();
                // this.productServiceGroupSearchDropDown.active = { id: this.getControlValue('ProductServiceGroupCode'), text: this.getControlValue('ProductServiceGroupCode') + ' -' };
                this.setControlValue('ServiceCoverRowID', this.riGrid.Details.GetAttribute('Selected', 'rowid'));
                this.setControlValue('PostcodeFilter', this.riGrid.Details.GetValue('PremisePostcode'));
                this.setControlValue('SelectedServiceAreaCode', this.riGrid.Details.GetValue('BranchServiceAreaCode'));
                this.setControlValue('SelectedServiceAreaSeq', this.riGrid.Details.GetValue('BranchServiceAreaSeqNo'));
                this.setControlValue('SelectedPremiseNumber', this.riGrid.Details.GetValue('PremiseNumber'));
                this.setControlValue('SelectedPremiseName', this.riGrid.Details.GetValue('PremiseName'));
                this.setControlValue('SelectedProductCode', this.riGrid.Details.GetValue('ProductCode'));
                this.setControlValue('SelectedFrequency', this.riGrid.Details.GetValue('ServiceVisitFrequency'));
                this.setControlValue('SelectedVisitCycle', this.riGrid.Details.GetValue('VisitCycleInWeeks'));
                this.setControlValue('SelectedQuantity', this.riGrid.Details.GetValue('ServiceQuantity'));
                this.setControlValue('SelectedServiceType', this.riGrid.Details.GetValue('ServiceTypeCode'));
                this.setControlValue('SelectedAnniversary', this.riGrid.Details.GetValue('ServiceVisitAnnivDate'));
                this.setControlValue('SelectedProductService', this.riGrid.Details.GetValue('ProductServiceGroupCode'));
                this.setControlValue('SelectedPostcode', this.riGrid.Details.GetValue('PremisePostcode'));
                this.setControlValue('SelectedNextVisit', this.riGrid.Details.GetValue('NextServiceVisitDate'));
                this.setControlValue('SelectedContractType', this.currentContractType);
                if (this.enableWed)
                    this.setControlValue('SelectedWED', this.riGrid.Details.GetValue('ServiceWED'));
                this.setControlValue('BranchServiceAreaCode', '');
                this.setControlValue('BranchServiceAreaDesc', '');
                this.blnCacheRefresh = true;
                this.riGrid.Update = true;
                this.riGridBeforeExecute();
            }
            this.pageParams.SelectedServiceAreaCode = this.getControlValue('SelectedServiceAreaCode');
            this.pageParams.SelectedServiceAreaSeq = this.getControlValue('SelectedServiceAreaSeq');
            this.pageParams.SelectedAnniversary = this.getControlValue('SelectedAnniversary');
        }
    }

    /**
     * This method will run after double clicking on a grid row
     * @param doubleclickevent
     * @return void
     */
    public riGridBodyOnDblClick(event: any): void {
        let columnName: string = this.riGrid.CurrentColumnName;
        if (columnName === 'PremiseNumber' || columnName === 'ProductCode' || columnName === 'VisitsUnits') {
            this.rezoneFocus(event.srcElement.parentElement);
            switch (columnName) {
                case 'PremiseNumber':
                    this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                        contractTypeCode: this.currentContractTypeActual,
                        PremiseRowID: this.riGrid.Details.GetAttribute('PremiseNumber', 'additionalproperty')
                    });
                    break;
                case 'ProductCode':
                    if (this.currentContractType === '<product>') {
                        this.navigate('ServicePlanning', InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE, {
                            currentContractTypeURLParameter: this.currentContractType
                        });
                    } else {
                        this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                            currentContractTypeURLParameter: this.currentContractType
                        });
                    }
                    break;
                case 'VisitsUnits':
                    this.navigate('BranchServiceArea', BIReportsRoutes.ICABSSESTATICVISITGRIDYEARBRANCH,
                        {
                            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
                            EmployeeSurname: this.getControlValue('EmployeeSurname')
                        });
                    break;
            }
        }
    }

    public onClickSaveButton(): void {
        this.setRequiredStatus('SelectedServiceAreaCode', true);
        this.setRequiredStatus('SelectedServiceAreaSeq', true);
        this.setRequiredStatus('SelectedAnniversary', true);
        if (this.riExchange.validateForm(this.uiForm)) {
            let searchParams: any = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            let postParams: any = {};
            postParams[this.serviceConstants.Function] = 'UpdateServiceCover';
            postParams.ServiceCoverRowID = this.getControlValue('ServiceCoverRowID');
            postParams.SelectedServiceAreaCode = this.getControlValue('SelectedServiceAreaCode');
            postParams.SelectedServiceAreaSeq = this.getControlValue('SelectedServiceAreaSeq');
            postParams.SelectedAnniversary = this.getControlValue('SelectedAnniversary');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.formPristine();
                            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                            this.pageParams.SelectedServiceAreaCode = this.getControlValue('SelectedServiceAreaCode');
                            this.pageParams.SelectedServiceAreaSeq = this.getControlValue('SelectedServiceAreaSeq');
                            this.pageParams.SelectedAnniversary = this.getControlValue('SelectedAnniversary');
                        }
                    },
                    (error) => {
                        this.errorService.emitError(error);
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        }
    }

    public populateDescriptions(): void {
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams[this.serviceConstants.Function] = 'GetDescriptions';
        postParams.BranchNumber = this.utils.getBranchCode();
        postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        postParams.ServiceTypeCode = this.getControlValue('ServiceTypeCode');
        postParams.ServiceCoverRowID = this.getControlValue('ProductServiceGroupCode');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    public onClickClearButton(): void {
        this.formPristine();
        this.setRequiredStatus('SelectedServiceAreaCode', false);
        this.setRequiredStatus('SelectedServiceAreaSeq', false);
        this.setRequiredStatus('SelectedAnniversary', false);
        this.riGrid.Clear();
        this.isVisiblePagination = false;
        this.uiForm.reset();
        this.serviceTypeSearchDropDown.active = { id: '', text: '' };
        this.productServiceGroupSearchDropDown.active = { id: '', text: '' };
        this.pageParams.SelectedServiceAreaCode = '';
        this.pageParams.SelectedServiceAreaSeq = '';
        this.pageParams.SelectedAnniversary = '';
    }

    public onClickViewButton(): void {
        if (!this.getControlValue('ServiceCoverRowID')) {
            this.modalAdvService.emitMessage(new ICabsModalVO('No Service Cover Selected'));
        } else {
            if (this.currentContractType === '<product>') {
                this.navigate('StaticVisit', InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE, {
                    currentContractTypeURLParameter: this.currentContractType
                });
            } else {
                this.navigate('StaticVisit', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                    currentContractTypeURLParameter: this.currentContractType
                });
            }
        }
    }

    /**
     * grid refresh
     * @param void
     * @return void
     */
    public refresh(): void {
        this.riGrid.RefreshRequired();
        if (this.riExchange.validateForm(this.uiForm)) {
            this.buildGrid();
        }
    }

    public riGridSort(event: any): void {
        this.riGridBeforeExecute();
    }

    public getCurrentPage(currentPage: any): void {
        this.curPage = currentPage.value;
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateRow = true;
        this.riGrid.UpdateFooter = true;
        this.riGridBeforeExecute();
    }

    public onreceivedServiceType(data: any): void {
        this.uiForm.controls['ServiceTypeCode'].markAsDirty();
        this.setControlValue('ServiceTypeCode', data.ServiceTypeCode);
        this.setControlValue('ServiceTypeDesc', data.ServiceTypeDesc);
        this.pageParams.serviceTypeCode = data.ServiceTypeCode;
        this.pageParams.serviceTypeDesc = data.ServiceTypeDesc;
    }

    public onreceivedProductServiceGroup(data: any): void {
        this.uiForm.controls['ProductServiceGroupCode'].markAsDirty();
        this.setControlValue('ProductServiceGroupCode', data.ProductServiceGroupCode);
        this.setControlValue('ProductServiceGroupDesc', data.ProductServiceGroupDesc);
        this.pageParams.productServiceGroupCode = data.ProductServiceGroupCode;
        this.pageParams.productServiceGroupDesc = data.ProductServiceGroupDesc;
    }

    /**
     * On change of the ContractType
     * @param void
     * @return void
     */
    public onContractTypeChange(data: any): void {
        if (!this.iscontractType || !this.isReturning()) {
            this.setControlValue('ContractTypeCode', data);
            this.pageParams.contractType = data;
        }
        this.iscontractType = false;
    }

    /**
     * after clicking on the portfolioStatus dropdown
     * @param void
     * @return void
     */
    public onPortfolioStatusChange(data: any): void {
        if (!this.isFirstTimeportfolioStatus || !this.isReturning()) {
            this.setControlValue('PortfolioStatusType', data);
            this.pageParams.portfolioStatusType = data;
        }
        this.isFirstTimeportfolioStatus = false;
    }

    public onRecieveBranchServiceAreaCode(data: any): void {
        this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
        this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
    }

    public selectedAnniversaryOnChange(data: any): void {
        if (data) {
            this.uiForm.controls['SelectedAnniversary'].markAsDirty();
        }
    }
}
