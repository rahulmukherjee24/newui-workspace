import { QueryParams } from '@shared/services/http-params-wrapper';
import { AfterContentInit, Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { BaseComponent } from '@app/base/BaseComponent';
import { EmployeeSearchComponent } from '@internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { IExportOptions } from '@app/base/ExportConfig';
import { InternalGridSearchServiceModuleRoutes, InternalMaintenanceServiceModuleRoutes } from '@base/PageRoutes';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
@Component({
    templateUrl: 'iCABSSePDAWorkListEntryGrid.html',
    styles: [`
       .meansPausedVisit{
           background-color:#ADFF2F
        }
    `]
})

export class PDAWorkListEntryGridComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('serviceEntryPagination') serviceEntryPagination: PaginationComponent;
    private xhr: Object = {
        module: 'pda',
        method: 'service-delivery/maintenance',
        operation: 'Service/iCABSSePDAWorkListEntryGrid'
    };
    public exportConfig: IExportOptions = {};
    public pageId: string = '';
    public itemsPerPage: number = 10;
    public totalRecords: number = 1;
    public currentPage: number = 1;
    public controls: Array<any> = [
        { name: 'EmployeeCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'TotalMileage', disabled: true, type: MntConst.eTypeInteger },
        { name: 'TotalDuration', disabled: true, type: MntConst.eTypeTime },
        { name: 'menu' },
        //not in HTML
        { name: 'TemporaryWorkingDay' },
        { name: 'ContinueAddActivity', value: 'false' },
        { name: 'PassEmployeeCode' },
        { name: 'PassEmployeeName' },
        { name: 'PassDiaryDate' },
        { name: 'PassDiaryStartTime' },
        { name: 'PassDiaryEndTime' },
        { name: 'PassWONumber' },
        { name: 'PassDiaryEntryNumber' },
        { name: 'DiaryDate' },
        { name: 'PDAICABSActivityRowID' }
    ];
    public readonly today: Date = new Date();
    public ellipsismodalConfig: {
        backdrop: 'static',
        keyboard: true
    };
    public employeeSearchEllipsis: any = {
        component: EmployeeSearchComponent,
        inputParams: {
            'parentMode': 'LookUp-Service'
        },
        dataReceived: ((data: any) => {
            if (data) {
                this.setControlValue('EmployeeCode', data.EmployeeCode);
                this.setControlValue('EmployeeSurname', data.EmployeeSurName);
                this.setControlValue('PassEmployeeCode', data.EmployeeCode);
                this.setControlValue('PassEmployeeName', data.EmployeeSurname);
            }
        })
    };
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPDAWORKLISTENTRYGRID;
        this.browserTitle = this.pageTitle = 'Service Worklist Entry';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    public ngAfterContentInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.setControlValue('menu', '');
            this.currentPage = this.pageParams.gridCurrentPage;
            this.riGrid.DescendingSort = false;
            this.buildGrid();
            this.riGridBeforeExecute();
            this.isReturningFlag = false;
        } else {
            this.pageParams.gridCurrentPage = 1;
            switch (this.parentMode) {
                case 'WorkListCopy':
                    //Pull value from Parent Page
                    this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
                    this.setControlValue('DateFrom', this.riExchange.getParentHTMLValue('DateFrom'));
                    this.setControlValue('DateTo', this.riExchange.getParentHTMLValue('DateTo'));
                    this.employeeCodeOnchange();
                    break;
                case 'ProductivityReview':
                    this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
                    this.setControlValue('DateFrom', this.riExchange.getParentHTMLValue('DateFrom'));
                    this.setControlValue('DateTo', this.riExchange.getParentHTMLValue('DateTo'));
                    this.employeeCodeOnchange();
                    break;
                default:
                    // This should set the date to two Saturdays ago.
                    this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(this.today));
                    this.setDateTo();
                    break;
            }
        }
        this.riGrid.DefaultBorderColor = 'ADD8E6';
        this.riGrid.HighlightBar = true;
        this.riGrid.DescendingSort = false;
        this.buildGrid();
    }
    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public employeeCodeOnchange(): void {
        if (this.getControlValue('EmployeeCode') && this.uiForm.valid) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '0');
            search.set('PostDesc', 'Employee');
            search.set('EmployeeCode', this.getControlValue('EmployeeCode'));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.xhr['method'], this.xhr['module'], this.xhr['operation'], search)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data) {
                            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                            this.setControlValue('PassEmployeeCode', this.getControlValue('EmployeeCode'));
                            this.setControlValue('PassEmployeeName', data.EmployeeSurname);
                            this.riGridBeforeExecute();
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        } else {
            this.setControlValue('EmployeeSurname', '');
        }
    }

    public setDateTo(): void {
        if (this.getControlValue('DateFrom')) {
            this.setControlValue('DateTo', this.utils.addDays(this.getControlValue('DateFrom'), 7));
        }
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('EmployeeCode', 'ServiceWorkList', 'EmployeeCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumn('EmployeeName', 'ServiceWorkList', 'EmployeesName', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('ServiceVisitDate', 'ServiceWorkList', 'ServiceVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('ServiceTimeStart', 'ServiceWorkList', 'ServiceTimeStart', MntConst.eTypeTime, 10);
        this.riGrid.AddColumn('ServiceTimeEnd', 'ServiceWorkList', 'ServiceTimeEnd', MntConst.eTypeTime, 10);
        this.riGrid.AddColumn('ActivityType', 'ServiceWorkList', 'ActivityType', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('ContractNumber', 'ServiceWorkList', 'ContractNumber', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('PremiseNumber', 'ServiceWorkList', 'PremiseNumber', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('PremisesName', 'ServiceWorkList', 'PremisesName', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('ProductCode', 'ServiceWorkList', 'ProductCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumn('VisitTypeCode', 'ServiceWorkList', 'VisitTypeCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumn('Quantity', 'ServiceWorkList', 'Quantity', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('StandardTreatmentTime', 'ServiceWorkList', 'StandardTreatmentTime', MntConst.eTypeText, 6);
        this.riGrid.AddColumn('VisitDuration', 'ServiceWorkList', 'VisitDuration', MntConst.eTypeTime, 10);
        this.riGrid.AddColumn('DurationPercentage', 'ServiceWorkList', 'DurationPercentage', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumn('PremiseContactSignature', 'ServiceWorkList', 'PremiseContactSignature', MntConst.eTypeImage, 1);
        this.riGrid.AddColumn('AttemptedPrintCode', 'ServiceWorkList', 'AttemptedPrintCode', MntConst.eTypeCode, 1);
        this.riGrid.AddColumn('KeyedStartTime', 'ServiceWorklist', 'KeyedStartTime', MntConst.eTypeTime, 6);
        this.riGrid.AddColumn('KeyedEndTime', 'ServiceWorklist', 'KeyedEndTime', MntConst.eTypeTime, 6);
        this.riGrid.AddColumn('Mileage', 'ServiceWorklist', 'Mileage', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('PrivateWorkMileage', 'ServiceWorklist', 'PrivateWorkMileage', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('Recommendation', 'ServiceWorkList', 'Recommendation', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('PDALog', 'ServiceWorkList', 'PDALog', MntConst.eTypeImage, 1, false);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('EmployeeName', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceTimeStart', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceTimeEnd', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Quantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('StandardTreatmentTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitDuration', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('DurationPercentage', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PrivateWorkMileage', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('PDALog', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Recommendation', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('KeyedStartTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('KeyedEndTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremiseContactSignature', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('AttemptedPrintCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('ServiceVisitDate', true, true);

        this.riGrid.Complete();

        this.exportConfig = {};
        this.dateCollist = this.riGrid.getColumnIndexListFromFull([
            'ServiceVisitDate'
        ]);
        this.durationCollist = this.riGrid.getColumnIndexListFromFull([
            'ServiceTimeStart',
            'ServiceTimeEnd',
            'VisitDuration',
            'KeyedStartTime',
            'KeyedEndTime'
        ]);
        if (this.dateCollist.length) {
            this.exportConfig.dateColumns = this.dateCollist;
        }
        if (this.durationCollist.length) {
            this.exportConfig.durationColumns = this.durationCollist;
        }
    }
    public riGridBeforeExecute(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set('EmployeeCode', this.getControlValue('EmployeeCode'));
        search.set('DateFrom', this.getControlValue('DateFrom'));
        search.set('DateTo', this.getControlValue('DateTo'));
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.PageSize, this.itemsPerPage.toString());
        search.set(this.serviceConstants.PageCurrent, this.currentPage.toString());
        search.set(this.serviceConstants.GridHandle, this.utils.gridHandle);
        search.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        search.set('riSortOrder', this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhr['method'], this.xhr['module'], this.xhr['operation'], search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                        setTimeout(() => {
                            this.serviceEntryPagination.setPage(this.currentPage);
                        }, 500);
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.itemsPerPage : 1;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }
    public riGridAfterExecute(): void {
        this.setControlValue('TotalMileage', this.riGrid.Details.GetAttribute('ServiceTimeStart', 'additionalproperty'));
        this.setControlValue('TotalDuration', this.riGrid.Details.GetAttribute('ServiceTimeEnd', 'additionalproperty'));
    }
    //Grid Refresh
    public btnRefresh(): void {
        if (this.uiForm.valid) {
            this.currentPage = 1;
            this.riGrid.Mode = MntConst.eModeNormal;
            this.riGrid.RefreshRequired();
            this.riGridBeforeExecute();
        }
    }
    // pagination current page
    public getCurrentPage(currentPage: any): void {
        if (this.currentPage !== currentPage.value) {
            this.currentPage = currentPage.value;
            this.riGrid.RefreshRequired();
            this.riGridBeforeExecute();
        }
    }
    public riGridSort(event: any): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
    public onDblClick(event: any): void {
        this.pageParams.gridCurrentPage = this.currentPage;
        switch (this.riGrid.CurrentColumnName) {
            case 'Recommendation':
                if (this.riGrid.CurrentColumnValue !== '') {
                    this.navigate('TechWorkSummary', InternalGridSearchServiceModuleRoutes.ICABSARECOMMENDATIONGRID.URL_1, {
                        'ContractNumber': this.riGrid.Details.GetValue('ContractNumber'),
                        'PremiseNumber': this.riGrid.Details.GetValue('PremiseNumber'),
                        'ProductCode': this.riGrid.Details.GetValue('ProductCode'),
                        'ServiceDateFrom': this.getControlValue('DateFrom'),
                        'ServiceDateTo': this.getControlValue('DateTo')
                    });
                }
                break;
            case 'ContractNumber':
            case 'ActivityType':
                if (this.riGrid.Details.GetValue('ContractNumber').substr(0, 4) === 'WONO') {
                    this.setControlValue('PassWONumber', this.riGrid.Details.GetValue('ContractNumber').substr(5, this.riGrid.Details.GetValue('ContractNumber').length));
                    this.navigate('ServiceVisit-WorkOrderGrid', InternalMaintenanceServiceModuleRoutes.ICABSCMWORKORDERMAINTENANCE, {
                        'PassWONumber': this.riGrid.Details.GetValue('ContractNumber').substr(5, this.riGrid.Details.GetValue('ContractNumber').length)
                    });
                } else if (this.riGrid.Details.GetValue('ContractNumber').substr(0, 4) === 'DINO') {
                    this.setControlValue('PassDiaryEntryNumber', this.riGrid.Details.GetValue('ContractNumber').substr(5, this.riGrid.Details.GetValue('ContractNumber').length));
                    this.setControlValue('DiaryDate', this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('ServiceVisitDate')));
                    this.navigate('ServiceActivityUpdate', InternalMaintenanceServiceModuleRoutes.ICABSADIARYENTRY, {});
                } else if (this.riGrid.Details.GetValue('ContractNumber').substr(0, 12) !== '') {
                    this.setAttribute('ServiceVisitRowID', this.riGrid.Details.GetAttribute('ServiceVisitDate', 'additionalproperty'));
                    this.setAttribute('PDAInd', this.riGrid.Details.GetAttribute('PDALog', 'additionalproperty'));
                } else {
                    this.setControlValue('PDAICABSActivityRowID', this.riGrid.Details.GetAttribute('ServiceVisitDate', 'additionalproperty'));
                    this.navigate('ServiceVisit', InternalMaintenanceServiceModuleRoutes.ICABSSEPDAICABSACTIVITYMAINTENANCE, {
                        'PDAICABSActivityRowID': this.riGrid.Details.GetAttribute('ServiceVisitDate', 'additionalproperty')
                    });
                }
                break;
        }
    }

    public clearWorkingInformation(event: any): void {
        this.setControlValue('TemporaryWorkingDay', '');
        this.setControlValue('ContinueAddActivity', 'false');
    }
    //menu navigation
    public addDiary(): void {
        if (this.getControlValue('EmployeeCode') !== '') {
            this.setControlValue('PassDiaryEntryNumber', '');
            this.setControlValue('DiaryDate', '');
            this.navigate('DiaryGridAdd', InternalMaintenanceServiceModuleRoutes.ICABSADIARYENTRY, {
            });
        } else {
            this.setControlValue('menu', '');
        }
    }
    public addWorkOrder(): void {
        if (this.getControlValue('EmployeeCode') !== '') {
            this.setControlValue('PassDiaryEntryNumber', this.globalize.parseDateToFixedFormat(this.today));
            this.setControlValue('PassDiaryStartTime', '09:00');
            this.setControlValue('PassDiaryEndTime', '10:00');
            this.navigate('WorkOrderGridAdd', InternalMaintenanceServiceModuleRoutes.ICABSCMWORKORDERMAINTENANCE);
        } else {
            this.setControlValue('menu', '');
        }
    }

    public onChangeMenu(e: any): void {
        this.pageParams.gridCurrentPage = this.currentPage;
        switch (e.target.value) {
            case 'AddDiary':
                this.addDiary();
                break;
            case 'AddWorkOrder':
                this.addWorkOrder();
                break;
        }
    }
}
