import { QueryParams } from '@shared/services/http-params-wrapper';
import { Component, OnInit, OnDestroy, AfterContentInit, Injector, ViewChild, EventEmitter, ElementRef } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseComponent } from './../../../base/BaseComponent';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { EllipsisComponent } from './../../../../shared/components/ellipsis/ellipsis';
import { ContractManagementModuleRoutes, InternalMaintenanceServiceModuleRoutes, InternalGridSearchApplicationModuleRoutes, InternalMaintenanceApplicationModuleRoutes } from './../../../base/PageRoutes';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from './../../../../shared/components/pagination/pagination';
import { BranchServiceAreaSearchComponent } from './../../../internal/search/iCABSBBranchServiceAreaSearch';
import { EmployeeSearchComponent } from './../../../internal/search/iCABSBEmployeeSearch';
import { ServicePlanSearchComponent } from './../../../internal/search/iCABSSeServicePlanSearch.component';
import { VariableService } from './../../../../shared/services/variable.service';

@Component({
    templateUrl: 'iCABSSeGroupServiceVisitEntryGrid.html'
})

export class GroupServiceVisitEntryGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('branchServiceAreaCodeEllipsis') branchServiceAreaCodeEllipsis: EllipsisComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private queryParams: any = {
        module: 'service',
        method: 'service-delivery/maintenance',
        operation: 'Service/iCABSSeGroupServiceVisitEntryGrid'
    };
    private canDeactivateObservable: Observable<boolean>;
    private navigateOnGridClick: boolean = false;
    public setFocusOnBranchServiceAreaCode = new EventEmitter<boolean>();
    public setFocusOnEmployeeCode = new EventEmitter<boolean>();
    public setFocusOnVisitReference1 = new EventEmitter<boolean>();
    public setFocusOnManualVisitReasonCode = new EventEmitter<boolean>();
    private isOnBlur: boolean = false;
    public isProcessedClick: boolean = false;
    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'BranchServiceAreaCode', required: true, type: MntConst.eTypeCode, commonValidator: true },
        { name: 'BranchServiceAreaDesc', required: true, disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceDate', required: true, type: MntConst.eTypeDate, commonValidator: true },
        { name: 'ServicePlanNumber', required: true, type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'EmployeeCode', required: false, type: MntConst.eTypeCode, commonValidator: true },
        { name: 'EmployeeSurname', required: true, disabled: true, type: MntConst.eTypeText },
        { name: 'ServicePlanStartDate', required: false, disabled: true, type: MntConst.eTypeDate },
        { name: 'ServicePlanEndDate', required: false, disabled: true, type: MntConst.eTypeDate },
        { name: 'VisitReference1', required: false, type: MntConst.eTypeText, commonValidator: true },
        { name: 'ManualVisitReasonCode', required: false, type: MntConst.eTypeText, commonValidator: true },
        { name: 'ManualVisitReasonDesc', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'AcceptAll', required: false, type: MntConst.eTypeCheckBox },
        // hidden
        { name: 'TotalCancelled', required: false },
        { name: 'CancelRowid', required: false }
    ];

    public ellipsis: any = {
        servicePlanNumber: {
            childConfigParams: {
                parentMode: 'LookUp'
            },
            contentComponent: ServicePlanSearchComponent,
            isDisabled: false
        },
        branchServiceAreaCode: {
            childConfigParams: {
                parentMode: 'LookUp-GroupVisitEntry'
            },
            contentComponent: BranchServiceAreaSearchComponent,
            isDisabled: false
        },
        employeeCode: {
            childConfigParams: {
                parentMode: 'LookUp-Service-All'
            },
            contentComponent: EmployeeSearchComponent,
            isDisabled: false
        }
    };

    public dropdown: any = {
        manualVisitReasonSearch: {
            params: {
                method: 'service-delivery/search',
                module: 'manual-service',
                operation: 'Business/iCABSBManualVisitReasonSearch'
            },
            displayFields: ['ManualVisitReasonCode', 'ManualVisitReasonDesc'],
            activeSelected: {
                id: '',
                text: ''
            },
            isDisabled: false,
            isRequired: true,
            isFirstItemSelected: false,
            isTriggerValidate: false
        }
    };

    constructor(injector: Injector, private el: ElementRef, private variableService: VariableService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEGROUPSERVICEVISITENTRYGRID;
        this.browserTitle = this.pageTitle = 'Group Service Visit Entry';
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.navigateOnGridClick = false;
            this.buildGrid();
            this.setConfigParams();
            this.riGridBeforeExecute();
        } else {
            this.pageParams.isAllSelectClick = false;
            this.pageParams.riGridHandle = this.utils.randomSixDigitString();
            this.pageParams.curPage = 1;
            this.windowOnload();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.routeAwayGlobals.resetRouteAwayFlags();
    }

    ngAfterContentInit(): void {
        if (!this.getControlValue('BranchServiceAreaCode') && !this.isReturning()) this.branchServiceAreaCodeEllipsis.openModal();
    }

    private windowOnload(): void {
        this.setCurrentContractType();
        this.getSysCharDtetails();

        this.setFocusOnBranchServiceAreaCode.emit(true);

        this.setControlValue('AcceptAll', false);
        this.pageParams.tdAcceptAll = false;
        this.pageParams.vSelectAll = false;
        this.pageParams.vClearAll = false;
        this.pageParams.vApplyDates = false;
        this.pageParams.vProcessSelection = false;
        this.pageParams.vRemoveCancelVisits = false;

        this.setAttribute('AbandonCancel', false);
    }

    /*Spped script - syschar dependency fetch*/
    private getSysCharDtetails(): void {
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharEnableEntryOfVisitRefInVisitEntry, // 2040 - Enable Entry Of Visit Reference In Visit Entry
            this.sysCharConstants.SystemCharEnableServiceCoverDisplayLevel, // 3360 - Enable Service Cover Display Level
            this.sysCharConstants.SystemCharEnableManualVisitReasonCode, // 3100 - Enable Manual Visit Reason Code
            this.sysCharConstants.SystemCharEnableVisitDetail // 2490 - Enable Visit Details
        ];
        let sysCharIP = {
            module: this.queryParams.module,
            operation: this.queryParams.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };
        this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            let record = data.records;
            this.pageParams.vSCEnableVisitRef = record[0]['Required']; // 2040
            this.pageParams.vSCEnableServiceCoverDispLev = record[1]['Required']; // 3360
            this.pageParams.vSCEnableManualVisitReasonCode = record[2]['Required']; // 3100
            this.pageParams.vSCEnableVisitDetail = record[3]['Required']; // 2490
            this.pageParams.vVisitRefChange = false;

            if (this.pageParams.vSCEnableVisitRef) this.pageParams.trVisitReference = true;
            else this.pageParams.trVisitReference = false;

            if (this.pageParams.vSCEnableManualVisitReasonCode) this.pageParams.trManualVisitReasonCode = true;
            else this.pageParams.trManualVisitReasonCode = false;

            this.riCountryLookUpCall();
            this.sysCharCheck();
        });
    }
    // lookUp Table Data Fetch
    private riCountryLookUpCall(): void {
        let lookupIP: Array<any> = [
            {
                'table': 'riCountry',
                'query': {
                    'riCountrySelected': true
                },
                'fields': ['riTimeSeparator']
            }
        ];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupIP).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                if (data && data.length > 0) {
                    if (data[0] && data[0].length > 0) {
                        if (data[0][0].riTimeSeparator) this.pageParams.vbTimeSeparator = data[0][0].riTimeSeparator;
                    }
                } else {
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                }
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    private riErrorMessageLookUpCall(code: number): string {
        this.pageParams.ErrorMessageDescription = '';
        let lookupIP: Array<any> = [
            {
                'table': 'ErrorMessage',
                'query': {
                    'ErrorMessageCode': code
                },
                'fields': ['ErrorMessageDescription']
            }
        ];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupIP).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data && data.length > 0) {
                if (data[0] && data[0].length > 0 && data[0][0].ErrorMessageDescription) {
                    this.pageParams.ErrorMessageDescription = data[0][0].ErrorMessageDescription;
                    this.modalAdvService.emitMessage(new ICabsModalVO(this.pageParams.ErrorMessageDescription));
                }
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
        return this.pageParams.ErrorMessageDescription;
    }

    // set current page data
    private setCurrentContractType(): void {
        this.pageParams.currentContractType = this.riExchange.getCurrentContractType();
        this.pageParams.currentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
    }

    // set ellipsis/dropdown input configParams
    private setConfigParams(): void {
        this.ellipsis.servicePlanNumber.childConfigParams.branchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        this.ellipsis.servicePlanNumber.childConfigParams.employeeSurname = this.getControlValue('EmployeeSurname');
        if (this.getControlValue('ManualVisitReasonDesc')) {
            this.dropdown.manualVisitReasonSearch.activeSelected = {
                id: this.getControlValue('ManualVisitReasonCode'),
                text: this.getControlValue('ManualVisitReasonCode') + ' - ' + this.getControlValue('ManualVisitReasonDesc')
            };
        }
    }

    // Create grid staructure
    private buildGrid(): void {
        this.riGrid.FunctionTabSupport = true;
        this.riGrid.FunctionUpdateSupport = true;
        this.riGrid.DescendingSort = false;
        this.pageParams.totalRecords = 0;
        this.pageParams.pageSize = 10;
        if (this.pageParams.refreshClicked) {
            this.pageParams.requestCacheRefresh =  true;
            this.pageParams.refreshClicked = false;
        } else {
            this.pageParams.requestCacheRefresh =  !this.isReturning();
        }
        this.riGrid.Clear();

        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'ServiceVisit', 'BranchServiceAreaSeqNo', MntConst.eTypeInteger, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractPremise', 'ServiceVisit', 'ContractPremise', MntConst.eTypeText, 16, true, 'Go to Premise Maintenance');
        this.riGrid.AddColumnAlign('ContractPremise', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'ServiceVisit', 'PremiseName', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ProductCode', 'ServiceVisit', 'ProductCode', MntConst.eTypeCode, 10, true, 'Go to Service Cover Maintenance');
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        if (this.pageParams.vSCEnableServiceCoverDispLev) {
            this.riGrid.AddColumn('DisplayNo', 'ServiceVisit', 'DisplayNo', MntConst.eTypeText, 6, false, '');
            this.riGrid.AddColumnAlign('DisplayNo', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('Location', 'ServiceVisit', 'Location', MntConst.eTypeText, 14);
            this.riGrid.AddColumnAlign('Location', MntConst.eAlignmentLeft);
        }

        this.riGrid.AddColumn('ServiceVisitFrequency', 'ServiceVisit', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PlanVisitNumber', 'ServiceVisit', 'PlanVisitNumber', MntConst.eTypeInteger, 5, false);
        this.riGrid.AddColumnAlign('PlanVisitNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitTypeCode', 'ServiceVisit', 'VisitTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('gridEmployeeCode', 'ServiceVisit', 'gridEmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('gridEmployeeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServicedQuantity', 'ServiceVisit', 'ServicedQuantity', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('ServicedQuantity', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServicePlanSequenceNumber', 'ServiceVisit', 'ServicePlanSequenceNumber', MntConst.eTypeInteger, 5, true, 'Go to Service Visit Maintenance');
        this.riGrid.AddColumnAlign('ServicePlanSequenceNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceDateStart', 'ServiceVisit', 'ServiceDateStart', MntConst.eTypeDate, 10, false);
        this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);

        if (this.pageParams.vSCEnableServiceCoverDispLev) {
            this.riGrid.AddColumnUpdateSupport('ServiceDateStart', true);
        }

        if (!this.pageParams.vSCEnableServiceCoverDispLev) {
            this.riGrid.AddColumn('ServiceDateEnd', 'ServiceVisit', 'ServiceDateEnd', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('ServiceDateEnd', MntConst.eAlignmentCenter);
        }

        if (this.pageParams.vSCEnableServiceCoverDispLev || this.pageParams.vSCEnableVisitDetail) {
            this.riGrid.AddColumn('ServiceVisitTime', 'ServiceVisit', 'ServiceVisitTime', MntConst.eTypeTime, 6, false);
            this.riGrid.AddColumnAlign('ServiceVisitTime', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnUpdateSupport('ServiceVisitTime', true);
        }

        if (this.pageParams.vSCEnableVisitRef) {
            this.riGrid.AddColumn('VisitReference', 'ServiceVisit', 'VisitReference', MntConst.eTypeCode, 10);
            this.riGrid.AddColumnAlign('VisitReference', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnUpdateSupport('VisitReference', true);
        }

        if (this.pageParams.vSCEnableVisitDetail) {
            this.riGrid.AddColumn('VisitDetail', 'ServiceVisit', 'VisitDetail', MntConst.eTypeImage, 1, false, '');
            this.riGrid.AddColumnAlign('VisitDetail', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('Processed', 'ServiceVisit', 'Processed', MntConst.eTypeImage, 1, false, '');
        this.riGrid.AddColumnAlign('Processed', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Accept', 'ServiceVisit', 'Accept', MntConst.eTypeImage, 1, false, '');
        this.riGrid.AddColumnAlign('Accept', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('RePlan', 'ServiceVisit', 'RePlan', MntConst.eTypeImage, 1, false, '');
        this.riGrid.AddColumnAlign('RePlan', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Cancel', 'ServiceVisit', 'Cancel', MntConst.eTypeImage, 1, false, '');
        this.riGrid.AddColumnAlign('Cancel', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('UpdateServicedQuantity', 'ServiceVisit', 'ServicedQuantity', MntConst.eTypeInteger, 6, false);
        this.riGrid.AddColumnAlign('UpdateServicedQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('UpdateServicedQuantity', true);

        if (this.pageParams.vWasteCollected) {
            this.riGrid.AddColumn('WasteCollected', 'ServiceVisit', 'WasteCollected', MntConst.eTypeImage, 1, false, 'Click here if waste is collected');
            this.riGrid.AddColumnAlign('WasteCollected', MntConst.eAlignmentCenter);
        }

        if (this.pageParams.vSCEnableManualVisitReasonCode) {
            this.riGrid.AddColumn('ManualVisitReasonCode1', 'ServiceVisit', 'ManualVisitReasonCode1', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('ManualVisitReasonCode1', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumnOrderable('ServicePlanSequenceNumber', true);
        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);
        this.riGrid.AddColumnOrderable('ServiceVisitFrequency', true);
        this.riGrid.AddColumnOrderable('Processed', true);
        this.riGrid.Complete();
    }

    // Check SysChar and see whether or not to show WasteCollected column
    private sysCharCheck(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');

        let postData: Object = {
            Function: 'SysCharCheck'
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                if (data.WasteCollected === 'yes') this.pageParams.vWasteCollected = true;
                else this.pageParams.vWasteCollected = false;
                this.buildGrid();
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    private populateDateFields(): void {
        if (this.getControlValue('BranchServiceAreaCode') && this.getControlValue('ServicePlanNumber')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');

            let postData: Object = {
                Function: 'GetDates',
                BranchNumber: this.utils.getBranchCode(),
                BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
                ServicePlanNumber: this.getControlValue('ServicePlanNumber'),
                ServicePlanStartDate: this.getControlValue('ServicePlanStartDate'),
                ServicePlanEndDate: this.getControlValue('ServicePlanEndDate'),
                ServiceDate: this.getControlValue('ServiceDate')
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    if (data.ServicePlanStartDate) this.setControlValue('ServicePlanStartDate', data.ServicePlanStartDate);
                    if (data.ServicePlanEndDate) this.setControlValue('ServicePlanEndDate', data.ServicePlanEndDate);
                    if (data.ServiceDate) this.setControlValue('ServiceDate', data.ServiceDate);
                    this.riGrid.RefreshRequired();
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        } else {
            this.setControlValue('ServicePlanNumber', '');
            this.setControlValue('ServicePlanStartDate', '');
            this.setControlValue('ServicePlanEndDate', '');
            this.setControlValue('ServiceDate', '');
        }
        this.resetAcceptAll(true);
    }

    // Reset the "Accept All" checkbox
    private resetAcceptAll(clearGrid: boolean): void {
        this.setControlValue('AcceptAll', false);
        this.disableControl('AcceptAll', false);
        if (clearGrid) this.riGrid.ResetGrid();
    }

    private serviceVisitProcess(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');

        let postData: Object = {
            Function: 'CreateServiceVisit',
            PlanVisitRowID: this.getAttribute('PlanVisitRowID'),
            ServiceDate: this.getControlValue('ServiceDate'),
            EmployeeCode: this.getControlValue('EmployeeCode'),
            UpdatedQuantity: this.riGrid.Details.GetValue('UpdateServicedQuantity'),
            ServiceVisitTime: this.riGrid.Details.GetValue('ServiceVisitTime') || '',
            GridServiceDate: this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('ServiceDateStart'))
        };
        if (this.pageParams.vSCEnableVisitRef) {
            if (this.riGrid.Details.GetValue('VisitReference')) postData['VisitReference'] = this.riGrid.Details.GetValue('VisitReference');
            else postData['VisitReference'] = this.getControlValue('VisitReference1');
        }
        if (this.pageParams.vSCEnableManualVisitReasonCode) {
            postData['ManualVisitReasonCode'] = this.getControlValue('ManualVisitReasonCode');
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError)
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            else {
                this.pageParams.vUpdateSelection = true;
                this.riGrid.Update = true;
                this.riGridUpdateExecute();
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    private warnDates(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');

        let postData: Object = {
            Function: 'WarnDates'
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.ErrorMessageDesc) {
                let modalVO: ICabsModalVO = new ICabsModalVO(data.ErrorMessageDesc);
                modalVO.closeCallback = this.warningConfirm.bind(this);
                this.modalAdvService.emitMessage(modalVO);
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    private warningConfirm(data: Object): void {
        let messageText: string = MessageConstant.PageSpecificMessage.iCABSSeGroupServiceVisitEntryGrid.visitSelected;
        let modalVO: ICabsModalVO = new ICabsModalVO(messageText, null, this.confirmVisitSelectedEvent.bind(this));
        setTimeout(function (): void {
            this.modalAdvService.emitPrompt(modalVO);
        }.bind(this), 0);
    }

    private confirmVisitSelectedEvent(data: Object): void {
        if (this.pageParams.trVisitReference && !this.getControlValue('VisitReference1')) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'VisitReference1', true);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'VisitReference1', true);
        }
        if (this.pageParams.vSCEnableManualVisitReasonCode && !this.getControlValue('ManualVisitReasonCode')) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ManualVisitReasonCode', true);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'ManualVisitReasonCode', true);
            this.dropdown.manualVisitReasonSearch.isTriggerValidate = true;
        }
        if ((!this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode') && !this.pageParams.vSCEnableVisitRef)
            || (this.pageParams.vSCEnableVisitRef
                && !this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode')
                && !this.riExchange.riInputElement.isError(this.uiForm, 'VisitReference1')
                && ((this.pageParams.vSCEnableManualVisitReasonCode && this.getControlValue('ManualVisitReasonCode')) || !this.pageParams.vSCEnableManualVisitReasonCode)
            )) {
            if (parseInt(this.getControlValue('TotalCancelled'), 10) > 0) {
                this.navigate('GroupVisitEntry', InternalMaintenanceApplicationModuleRoutes.ICABSAVISITCANCELLATIONMAINTENANCE);
                this.pageParams.vRemoveCancelVisits = true;
                this.pageParams.requestCacheRefresh = true;
                this.riGridBeforeExecute();
            }
            this.pageParams.vProcessSelection = true;
            if (!this.getAttribute('AbandonCancel') || parseInt(this.getControlValue('TotalCancelled'), 10) === 0) {
                this.riGrid.Update = false;
                this.riGridUpdateExecute();
            }
        }
    }

    /*------- riGrid Routines started -----*/
    private riExchangeUpdateHTMLDocument(): void {
        if (!this.pageParams.isAllSelectClick && !this.pageParams.vClearAll) {
            this.pageParams.requestCacheRefresh = true;
        }
        this.riGrid.Update = false;
        this.riGridBeforeExecute();
    }

    private riGridUpdateExecute(): void {
        let gridQueryParams: QueryParams = this.getURLSearchParamObject();
        gridQueryParams.set(this.serviceConstants.Action, '2');
        let postData: Object = {};
        if (this.pageParams.vUpdateSelection) {
            postData['Selection'] = this.riGrid.CurrentColumnName;
            postData['ROWID'] = this.getAttribute('PlanVisitRowID');
        }
        let temp: any = (!this.pageParams.vUpdateSelection && !this.pageParams.vProcessSelection && (!this.pageParams.vWasteCollected || this.isOnBlur));
        this.isOnBlur = false;
        if (temp) {
            postData['BranchServiceAreaSeqNo'] = this.riGrid.Details.GetValue('BranchServiceAreaSeqNo');
            postData['ContractPremise'] = this.riGrid.Details.GetValue('ContractPremise');
            postData['PremiseName'] = this.riGrid.Details.GetValue('PremiseName');
            postData['ProductCode'] = this.riGrid.Details.GetValue('ProductCode');
            postData['DisplayNo'] = this.riGrid.Details.GetValue('DisplayNo');
            postData['Location'] = this.riGrid.Details.GetValue('Location');
            postData['PlanVisitNumber'] = this.riGrid.Details.GetValue('PlanVisitNumber');
            postData['VisitTypeCode'] = this.riGrid.Details.GetValue('VisitTypeCode');
            postData['ServiceVisitFrequency'] = this.riGrid.Details.GetValue('ServiceVisitFrequency');
            postData['gridEmployeeCode'] = this.riGrid.Details.GetValue('gridEmployeeCode');
            postData['ServicedQuantity'] = this.riGrid.Details.GetValue('ServicedQuantity');
            postData['ServicePlanSequenceNumber'] = this.riGrid.Details.GetValue('ServicePlanSequenceNumber');
            postData['ServiceDateStartRowID'] = this.riGrid.Details.GetAttribute('ServiceDateStart', 'rowid'); // Fix for IUI-18933
            postData['ServiceDateStart'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('ServiceDateStart'));
            postData['ServiceVisitTime'] = this.riGrid.Details.GetValue('ServiceVisitTime');
            postData['UpdateServicedQuantity'] = this.riGrid.Details.GetValue('UpdateServicedQuantity');
            postData['ManualVisitReasonCode1'] = this.riGrid.Details.GetValue('ManualVisitReasonCode1'); // Fix for IUI-18933
            postData['ContractPremiseRowID'] = this.riGrid.Details.GetAttribute('ContractPremise', 'rowid');
            postData['ProductCodeRowID'] = this.riGrid.Details.GetAttribute('ProductCode', 'rowid');
            postData['ServicePlanSequenceNumberRowID'] = this.riGrid.Details.GetAttribute('ServicePlanSequenceNumber', 'rowid');
            postData['VisitReferenceRowID'] = this.riGrid.Details.GetAttribute('VisitReference', 'rowid');
            postData['UpdateServicedQuantityRowID'] = this.riGrid.Details.GetAttribute('UpdateServicedQuantity', 'rowid');
        }
        postData['PlanVisitRowID'] = this.getAttribute('PlanVisitRowID') || '';
        if (this.pageParams.vWasteCollected && !this.isProcessedClick) { postData['ROWID'] = this.getAttribute('PlanVisitRowID') || ''; } // Fix for IUI-19353

        postData['BranchNumber'] = this.utils.getBranchCode();
        postData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        postData['ServicePlanNumber'] = this.getControlValue('ServicePlanNumber');
        postData['ServiceDate'] = this.getControlValue('ServiceDate');
        postData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        postData['ManualVisitReasonCode'] = this.getControlValue('ManualVisitReasonCode');
        if (temp) {
            postData['VisitReference'] = this.getControlValue('VisitReference1') ? this.riGrid.Details.GetValue('VisitReference') + ',' + this.getControlValue('VisitReference1') : this.riGrid.Details.GetValue('VisitReference'); // Fix for IUI-18933
        } else {
            postData['VisitReference'] = this.getControlValue('VisitReference1'); // Fix for IUI-19064
        }

        postData['WasteCollected'] = this.pageParams.vWasteCollected;
        postData['UpdateSelection'] = this.pageParams.vUpdateSelection;
        postData['ProcessSelection'] = this.pageParams.vProcessSelection;
        postData['SelectAll'] = this.pageParams.vSelectAll;
        postData['ClearAll'] = this.pageParams.vClearAll;
        postData['ApplyDates'] = this.pageParams.vApplyDates;
        postData['RemoveCancelVisits'] = this.pageParams.vRemoveCancelVisits;
        postData['ManualVisitReason'] = this.pageParams.vSCEnableManualVisitReasonCode;

        if (this.pageParams.vProcessSelection) {
            postData[this.serviceConstants.PageSize] = this.pageParams.pageSize.toString();
            postData[this.serviceConstants.PageCurrent] = this.pageParams.curPage.toString();
        }
        postData[this.serviceConstants.GridMode] = temp ? '3' : '0';
        postData[this.serviceConstants.GridHandle] = this.pageParams.riGridHandle;
        postData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        postData[this.serviceConstants.GridHTMLPage] = 'Service/iCABSSeGroupServiceVisitEntryGrid.htm';
        postData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridQueryParams, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.isProcessedClick = false;
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGrid.Mode = MntConst.eModeNormal;
                    if (this.pageParams.vProcessSelection || this.pageParams.vUpdateSelection || this.pageParams.vWasteCollected) {
                        // Removing page number calculation to fix IUI-18846
                        if (data.footer && data.footer.rows && data.footer.rows.length > 0) this.pageParams.riGridFooterData = data.footer.rows[0].text;
                        this.riGrid.RefreshRequired();
                        if (this.riGrid.Update) {
                            this.riGrid.StartRow = this.getAttribute('Row');
                            this.riGrid.StartColumn = 0;
                            this.riGrid.RowID = this.getAttribute('PlanVisitRowID');
                            this.riGrid.UpdateHeader = false;
                            this.riGrid.UpdateBody = true;
                            this.riGrid.UpdateFooter = true;
                        }
                        this.riGrid.Execute(data);
                    }
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private riGridBeforeExecute(): void {
        let gridQueryParams: QueryParams = this.getURLSearchParamObject();
        gridQueryParams.set(this.serviceConstants.Action, '2');
        gridQueryParams.set('BranchNumber', this.utils.getBranchCode());
        gridQueryParams.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        gridQueryParams.set('ServicePlanNumber', this.getControlValue('ServicePlanNumber'));
        gridQueryParams.set('ServiceDate', this.getControlValue('ServiceDate'));
        gridQueryParams.set('EmployeeCode', this.getControlValue('EmployeeCode'));
        gridQueryParams.set('WasteCollected', this.pageParams.vWasteCollected);
        gridQueryParams.set('UpdateSelection', this.pageParams.vUpdateSelection);
        gridQueryParams.set('ProcessSelection', this.pageParams.vProcessSelection);
        gridQueryParams.set('SelectAll', this.pageParams.vSelectAll);
        gridQueryParams.set('ClearAll', this.pageParams.vClearAll);
        gridQueryParams.set('ApplyDates', this.pageParams.vApplyDates);
        gridQueryParams.set('RemoveCancelVisits', this.pageParams.vRemoveCancelVisits);
        gridQueryParams.set('ManualVisitReason', this.pageParams.vSCEnableManualVisitReasonCode);
        gridQueryParams.set('ManualVisitReasonCode', this.getControlValue('ManualVisitReasonCode'));
        gridQueryParams.set('VisitReference', this.getControlValue('VisitReference1'));
        gridQueryParams.set('PlanVisitRowID', this.getAttribute('PlanVisitRowID') || '');
        gridQueryParams.set(this.serviceConstants.PageSize, this.pageParams.pageSize.toString());
        gridQueryParams.set(this.serviceConstants.PageCurrent, this.pageParams.curPage.toString());
        gridQueryParams.set(this.serviceConstants.GridCacheRefresh, this.pageParams.requestCacheRefresh);
        gridQueryParams.set(this.serviceConstants.GridMode, '0');
        gridQueryParams.set(this.serviceConstants.GridHandle, this.pageParams.riGridHandle);
        gridQueryParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridQueryParams.set(this.serviceConstants.GridHTMLPage, 'Service/iCABSSeGroupServiceVisitEntryGrid.htm');
        gridQueryParams.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridQueryParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    this.riGrid.ResetGrid();
                } else {
                    this.pageParams.curPage = data.pageData.pageNumber || 1;
                    this.pageParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.pageSize : 1;
                    this.riGrid.RefreshRequired();
                    if (this.pageParams.vUpdateSelection) {
                        this.pageParams.vbPostData = gridQueryParams;
                        this.pageParams.Selection = event.srcElement;
                    }
                    if (this.riGrid.Update) {
                        this.riGrid.StartRow = this.getAttribute('Row');
                        this.riGrid.StartColumn = 0;
                        this.riGrid.RowID = this.getAttribute('PlanVisitRowID');
                        this.riGrid.UpdateHeader = false;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = false;
                    }
                    this.riGrid.Execute(data);
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private serviceVisitFocus(rsrcElement: any): void {
        rsrcElement.focus();
        this.setAttribute('Row', this.riGrid.CurrentRow);
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('ContractPremise', 'rowid'));
        this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'rowid')); //ProductCode

        if (this.pageParams.vSCEnableServiceCoverDispLev) {
            this.setAttribute('PlanVisitRowID', this.riGrid.Details.GetAttribute('PlanVisitNumber', 'additionalproperty')); // PlanVisitNumber
            this.setControlValue('PlanVisitRowID', this.riGrid.Details.GetAttribute('PlanVisitNumber', 'additionalproperty'));
            this.pageParams.vPlanVisitRowID = this.riGrid.Details.GetAttribute('PlanVisitNumber', 'additionalproperty');
            this.setAttribute('VisitTypeCode', this.riGrid.Details.GetAttribute('VisitTypeCode', 'additionalproperty'));
            this.setAttribute('Processed', this.riGrid.Details.GetAttribute('BranchServiceAreaSeqNo', 'additionalproperty'));
        } else {
            this.setAttribute('PlanVisitRowID', this.riGrid.Details.GetAttribute('PlanVisitNumber', 'additionalproperty')); // PlanVisitNumber
            this.setControlValue('PlanVisitRowID', this.riGrid.Details.GetAttribute('PlanVisitNumber', 'additionalproperty'));
        }

        this.setAttribute('Row', this.riGrid.CurrentRow);

        if (this.riGrid.Details.GetAttribute('ServicePlanSequenceNumber', 'rowid') !== '1') {
            this.setAttribute('ServiceVisitRowID', this.riGrid.Details.GetAttribute('ServicePlanSequenceNumber', 'rowid'));
        } else {
            this.setAttribute('ServiceVisitRowID', '');
        }

        switch (this.riGrid.Details.GetAttribute('ServicePlanSequenceNumber', 'additionalproperty')) {
            case 'C':
                this.pageParams.CurrentContractTypeURLParameter = '';
                break;
            case 'J':
                this.pageParams.CurrentContractTypeURLParameter = '<job>';
                break;
            case 'P':
                this.pageParams.CurrentContractTypeURLParameter = '<product>';
                break;
        }
        this.pageParams.contractTypeCode = this.riGrid.Details.GetAttribute('ServicePlanSequenceNumber', 'additionalproperty');
    }

    public riGridAfterExecute(event: any): void {
        let tfootGroupServiceVisit: string;
        if (this.riGrid.HTMLGridFooter && this.riGrid.HTMLGridFooter.children.length > 0) tfootGroupServiceVisit = this.riGrid.HTMLGridFooter.children[0].children[0].children[0].innerHTML;
        if (this.pageParams.riGridFooterData) tfootGroupServiceVisit = this.pageParams.riGridFooterData;
        if (this.pageParams.vProcessSelection) this.pageParams.blnProcessed = true;
        else this.pageParams.blnProcessed = false;
        if ((!this.riGrid.Update && this.riGrid.Mode !== '3') || this.pageParams.vUpdateSelection) {
            if (tfootGroupServiceVisit) {
                let TotalInfo: Array<any> = tfootGroupServiceVisit.split('|');
                tfootGroupServiceVisit = '';
                this.setControlValue('TotalCancelled', TotalInfo[1]);
                this.setControlValue('CancelRowid', TotalInfo[2]);
            }
        }
        if (this.pageParams.blnProcessed) {
            this.pageParams.requestCacheRefresh = true;
            this.riGrid.RefreshRequired();
            this.riGridBeforeExecute();
        } else {
            this.pageParams.vProcessSelection = false;
            this.pageParams.requestCacheRefresh = false;
            this.riGrid.RefreshRequired();
        }
        this.pageParams.vUpdateSelection = false;
        this.pageParams.vProcessSelection = false;
        this.pageParams.vSelectAll = false;
        //this.pageParams.vClearAll = false;
        this.pageParams.vApplyDates = false;
        this.setAttribute('AbandonCancel', false);
        this.pageParams.vRemoveCancelVisits = false;
    }

    public riGridBodyColumnOnBlur(event: any): void {
        this.pageParams.vUpdateSelection = false;
        this.isOnBlur = true;
        this.riGrid.Update = true;
        this.riGridUpdateExecute();
    }

    public riGridBodyColumnLostFocus(): void {
        let vbNegative: boolean = false;
        let vbError: boolean = false;
        let vbTimeFormat: string = '##00' + this.pageParams.vbTimeSeparator + '##';
        let vbMsgResult: string;
        let vbDurationMinutes: number;
        if (this.pageParams.vSCEnableServiceCoverDispLev && !this.pageParams.vSCEnableVisitDetail) {
            let vbTimeTaken: number = this.riGrid.Details.GetValue('ServiceVisitTime').replace(this.pageParams.vbTimeSeparator, '');
            if (!isFinite(vbTimeTaken)) {
                vbError = true;
                this.riErrorMessageLookUpCall(2320);
            } else if (!vbError) {
                let vbTimeGridValue: string | boolean = this.globalize.parseTimeToFixedFormat(vbTimeTaken.toString());
                this.riGrid.Details.SetValue('ServiceVisitTime', vbTimeGridValue);
                if (!vbError && vbTimeTaken <= 0) {
                    vbError = true;
                    this.riErrorMessageLookUpCall(2257);
                }
                if (!vbError) {
                    vbDurationMinutes = parseInt(vbTimeTaken.toString().substr(-2), 10);
                    if (vbDurationMinutes < 0) {
                        vbDurationMinutes = vbDurationMinutes * -1;
                        vbNegative = true;
                    }
                    if (vbDurationMinutes > 59) {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.iCABSSeGroupServiceVisitEntryGrid.vbMsgResult));
                        vbError = true;
                    }
                }
            }
            if (vbError) {
                this.riGrid.Details.Focus('ServiceVisitTime');
            }
        }
    }

    public riGridBodyOnClick(event: Object): void {
        this.serviceVisitFocus(event['srcElement']);
        // Fix for IUI-18848
        if (this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName)) {
            switch (this.riGrid.CurrentColumnName) {
                case 'CheckedDetails':
                    if (this.pageParams.vSCEnableManualVisitReasonCode && !this.getControlValue('ManualVisitReasonCode')) {
                        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ManualVisitReasonCode', true);
                        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'ManualVisitReasonCode', true);
                        this.dropdown.manualVisitReasonSearch.isTriggerValidate = true;
                    } else {
                        if (
                            (this.pageParams.vSCEnableVisitRef
                                && !this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode')
                                && !this.riExchange.riInputElement.isError(this.uiForm, 'VisitReference1')
                                && ((this.pageParams.vSCEnableManualVisitReasonCode && !this.getControlValue('ManualVisitReasonCode')) || !this.pageParams.vSCEnableManualVisitReasonCode))
                            || (this.pageParams.vSCEnableVisitRef
                                && !this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode')
                                && this.riGrid.Details.GetValue('VisitReference')
                                && ((this.pageParams.vSCEnableManualVisitReasonCode && !this.getControlValue('ManualVisitReasonCode')) || !this.pageParams.vSCEnableManualVisitReasonCode))
                            || (!this.pageParams.vSCEnableVisitRef
                                && !this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode')
                                && ((this.pageParams.vSCEnableManualVisitReasonCode && !this.getControlValue('ManualVisitReasonCode')) || !this.pageParams.vSCEnableManualVisitReasonCode))
                        ) {

                            if (this.riGrid.Details.GetValue('VisitReference')) {
                                this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'VisitReference1', false);
                                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'VisitReference1', false);
                            }
                            let search: QueryParams = this.getURLSearchParamObject();
                            search.set(this.serviceConstants.Action, '6');
                            let postData: Object = {
                                Function: 'CreateServiceVisit',
                                PlanVisitRowID: this.getAttribute('PlanVisitRowID'),
                                ServiceDate: this.getControlValue('ServiceDate'),
                                EmployeeCode: this.getControlValue('EmployeeCode'),
                                UpdatedQuantity: this.riGrid.Details.GetValue('UpdateServicedQuantity')
                            };
                            if (this.pageParams.vSCEnableVisitRef) {
                                if (this.riGrid.Details.GetValue('VisitReference'))
                                    postData['VisitReference'] = this.riGrid.Details.GetValue('VisitReference');
                                else
                                    postData['VisitReference'] = this.getControlValue('VisitReference1');
                            }
                            if (this.pageParams.vSCEnableManualVisitReasonCode) {
                                postData['ManualVisitReasonCode'] = this.getControlValue('ManualVisitReasonCode');
                            }
                            this.ajaxSource.next(this.ajaxconstant.START);
                            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
                                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                                if (data.hasError) this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                                else this.riGrid.RefreshRequired();
                            }, (error) => {
                                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                            });

                        } else {
                            if (this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode')) {
                                this.setFocusOnEmployeeCode.emit(true);
                            } else {
                                if (this.pageParams.vSCEnableVisitRef) this.setFocusOnVisitReference1.emit(true);
                            }
                        }
                    }
                    break;
                case 'WasteCollected':
                    let search: QueryParams = this.getURLSearchParamObject();
                    search.set(this.serviceConstants.Action, '6');

                    let postData: Object = {
                        Function: 'WasteCollected',
                        ServiceVisitRowID: this.getAttribute('ServiceVisitRowID')
                    };
                    this.ajaxSource.next(this.ajaxconstant.START);
                    this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.riGrid.Update = true;
                            this.riGridUpdateExecute();
                        }
                    }, (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
                    break;
                case 'Accept':
                case 'RePlan':
                case 'Cancel':
                    this.pageParams.vUpdateSelection = true;
                    this.riGrid.Update = true;
                    this.riGridUpdateExecute();
                    break;
                case 'Processed':
                    if (this.pageParams.vSCEnableManualVisitReasonCode && !this.getControlValue('ManualVisitReasonCode')) {
                        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ManualVisitReasonCode', true);
                        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'ManualVisitReasonCode', true);
                        this.dropdown.manualVisitReasonSearch.isTriggerValidate = true;
                        this.setFocusOnManualVisitReasonCode.emit(true);
                    } else {
                        if (
                            (this.pageParams.vSCEnableVisitRef
                                && !this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode')
                                && !this.riExchange.riInputElement.isError(this.uiForm, 'VisitReference1')
                                && ((this.pageParams.vSCEnableManualVisitReasonCode && this.getControlValue('ManualVisitReasonCode')) || !this.pageParams.vSCEnableManualVisitReasonCode))
                            || (this.pageParams.vSCEnableVisitRef
                                && !this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode')
                                && this.riGrid.Details.GetValue('VisitReference')
                                && ((this.pageParams.vSCEnableManualVisitReasonCode && this.getControlValue('ManualVisitReasonCode')) || !this.pageParams.vSCEnableManualVisitReasonCode))
                            || (!this.pageParams.vSCEnableVisitRef
                                && !this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode')
                                && ((this.pageParams.vSCEnableManualVisitReasonCode && this.getControlValue('ManualVisitReasonCode'))
                                    || !this.pageParams.vSCEnableManualVisitReasonCode))
                        ) {
                            if (this.riGrid.Details.GetValue('VisitReference')) {
                                this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'VisitReference1', false);
                                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'VisitReference1', false);
                            }
                            this.serviceVisitProcess();
                        } else {
                            if (this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode')) {
                                this.setFocusOnEmployeeCode.emit(true);
                            } else {
                                if (this.pageParams.vSCEnableVisitRef && this.riExchange.riInputElement.isError(this.uiForm, 'VisitReference1')) this.setFocusOnVisitReference1.emit(true);
                                else if (this.pageParams.vSCEnableManualVisitReasonCode && this.getControlValue('ManualVisitReasonCode')) {
                                    this.setFocusOnManualVisitReasonCode.emit(true);
                                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ManualVisitReasonCode', true);
                                    this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'ManualVisitReasonCode', true);
                                    this.dropdown.manualVisitReasonSearch.isTriggerValidate = true;
                                }
                            }
                        }
                    }
                    break;
                case 'VisitReference':
                    this.visitReferenceOnChange();
                    break;

            }
        }
    }

    public riGridBodyOnDblClick(event: Object): void {
        this.serviceVisitFocus(event['srcElement']);
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractPremise':
                this.navigateOnGridClick = true; // Fix for IUI-18931
                this.navigate('GroupServiceVisit', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                    PremiseROWID: this.getAttribute('PremiseRowID'),
                    contracttypecode: this.pageParams.contractTypeCode
                });
                break;
            case 'ProductCode':
                this.navigateOnGridClick = true;
                this.navigate('GroupServiceVisit', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                    CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                    ServiceCoverROWID: this.getAttribute('ServiceCoverRowID'),
                    currentContractType: this.pageParams.contractTypeCode
                });
                break;
            case 'ServicePlanSequenceNumber':
                this.navigateOnGridClick = true;
                this.navigate('GroupServiceVisit', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE, {
                    CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                    ServiceVisitRowID: this.getAttribute('ServiceVisitRowID'),
                    ServiceCoverRowID: this.getAttribute('ServiceCoverRowID'),
                    PlanVisitRowID: this.getAttribute('PlanVisitRowID'),
                    currentContractType: this.pageParams.contractTypeCode
                });
                break;
            case 'DisplayNo':
                this.navigateOnGridClick = true;
                if (this.riGrid.Details.GetValue('DisplayNo') === '00') {
                    this.navigate('GroupServiceVisit', InternalGridSearchApplicationModuleRoutes.ICABSSASERVICECOVERDISPLAYGRID, {
                        CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                        BranchServiceAreaCodeServiceCoverRowID: this.getAttribute('ServiceCoverRowID')
                    });
                }
                break;
            case 'VisitDetail':
                this.navigateOnGridClick = true;
                if (this.riGrid.Details.GetAttribute('ServiceDateStart', 'additionalproperty') === 'ServiceVisit') {
                    this.setAttribute('grdGroupServiceVisitSystemInvoiceNumber', this.getAttribute('ServiceVisitRowID'));
                    this.navigate('VisitDetail', ContractManagementModuleRoutes.ICABSASERVICEVISITDETAILSUMMARY);
                } else {
                    this.setAttribute('PlanVisitRowID', this.riGrid.Details.GetAttribute('PlanVisitNumber', 'additionalproperty'));
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotCovered));
                }
                break;
        }
    }

    public riGridBodyColumnFocus(event: any): void {
        if (event.srcElement && (event.srcElement.tagName === 'INPUT')) {
            switch (this.riGrid.CurrentColumnName) {
                case 'UpdateServicedQuantity':
                    this.serviceVisitFocus(event.srcElement);
                    break;
                case 'ServiceVisitTime':
                    this.serviceVisitFocus(event.srcElement);
                    break;
            }
        }
    }

    public getCurrentPage(event: Object): void {
        this.pageParams.curPage = event['value'];
        this.riExchangeUpdateHTMLDocument();
    }

    public refresh(): void {
        this.pageParams.requestCacheRefresh = true;
        this.pageParams.refreshClicked = true;
        this.pageParams.isAllSelectClick = true;
        this.pageParams.vClearAll = false;
        this.riExchange.validateForm(this.uiForm);
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'BranchServiceAreaCode')
            && !this.riExchange.riInputElement.isError(this.uiForm, 'ServicePlanNumber')
            && (!this.riExchange.riInputElement.isError(this.uiForm, 'ServiceDate') || this.getControlValue('ServiceDate'))) {
            if (this.pageParams.curPage <= 0) this.pageParams.curPage = 1;
            this.buildGrid();
            this.riExchangeUpdateHTMLDocument();
        }
    }

    public riGridSort(event: Object): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    /* =================== Page UI onChange Rutines ============*/
    public branchServiceAreaCodeOnChange(event: any): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');

        let postData: Object = {
            Function: 'GetEmployee',
            BranchNumber: this.utils.getBranchCode(),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            BranchServiceAreaDesc: this.getControlValue('BranchServiceAreaDesc'),
            EmployeeCode: this.getControlValue('EmployeeCode'),
            EmployeeSurname: this.getControlValue('EmployeeSurname')
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                if (data.EmployeeSurname) this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                if (data.EmployeeCode) this.setControlValue('EmployeeCode', data.EmployeeCode);
                if (data.BranchServiceAreaDesc) this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
                this.setConfigParams();
                this.riGrid.RefreshRequired();
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
        this.setControlValue('ServicePlanNumber', '');
        this.servicePlanNumberOnChange();
    }

    public branchServiceAreaCodeOnReceive(data: any): void {
        if (data.BranchServiceAreaCode) this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
        if (data.BranchServiceAreaDesc) this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
        if (data.EmployeeCode) this.setControlValue('EmployeeCode', data.EmployeeCode);
        if (data.EmployeeSurname) this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        this.setConfigParams();
    }

    public servicePlanNumberOnChange(): void {
        this.populateDateFields();
    }

    public employeeCodeOnReceive(data: any): void {
        if (data.EmployeeCode) this.setControlValue('EmployeeCode', data.EmployeeCode);
        if (data.EmployeeSurname) this.setControlValue('EmployeeSurname', data.EmployeeSurname);
    }

    public employeeCodeOnChange(): void {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let postData: Object = {
                Function: 'GetServiceEmployee',
                BranchNumber: this.utils.getBranchCode(),
                BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
                EmployeeCode: this.getControlValue('EmployeeCode')
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    this.riExchange.riInputElement.markAsError(this.uiForm, 'EmployeeCode');
                } else {
                    if (data.EmployeeCode) this.setControlValue('EmployeeCode', data.EmployeeCode);
                    if (data.EmployeeSurname) this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                    this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'EmployeeCode', false);
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        } else {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'BranchServiceAreaCode', true);
            this.setFocusOnBranchServiceAreaCode.emit(true);
        }
    }

    public servicePlanNumberOnReceive(data: any): void {
        if (data.ServicePlanNumber) this.setControlValue('ServicePlanNumber', data.ServicePlanNumber);
        this.servicePlanNumberOnChange();
    }

    public visitReferenceOnChange(): void {
        this.pageParams.vVisitRefChange = true;
    }

    public onManualVisitReceived(data: Object): void {
        this.setControlValue('ManualVisitReasonCode', data['ManualVisitReasonCode']);
        this.setControlValue('ManualVisitReasonDesc', data['ManualVisitReasonDesc']);
        this.dropdown.manualVisitReasonSearch.isTriggerValidate = false;
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ManualVisitReasonCode', false);
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'ManualVisitReasonCode', false);
    }

    public manualVisitreasonCodeOnChange(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');

        let postData: Object = {
            Function: 'GetManualVisitReasonDesc',
            ManualVisitReasonCode: this.getControlValue('ManualVisitReasonCode')
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                if (data.ManualVisitReasonCode) this.setControlValue('ManualVisitReasonCode', data.ManualVisitReasonCode);
                if (data.ManualVisitReasonDesc) this.setControlValue('ManualVisitReasonDesc', data.ManualVisitReasonDesc);
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    public acceptAllOnClick(): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'VisitReference1', true);
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'VisitReference1', true);
        if ((!this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode') && !this.pageParams.vSCEnableVisitRef)
            || (this.pageParams.vSCEnableVisitRef
                && !this.riExchange.riInputElement.isError(this.uiForm, 'EmployeeCode')
                && !this.riExchange.riInputElement.isError(this.uiForm, 'VisitReference1')
                && (this.pageParams.vSCEnableManualVisitReasonCode && !this.getControlValue('ManualVisitReasonCode')))
            || !this.pageParams.vSCEnableManualVisitReasonCode) {

            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');

            let postData: Object = {
                Function: 'AcceptAllPlanVisits',
                BranchNumber: this.utils.getBranchCode(),
                BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
                ServicePlanNumber: this.getControlValue('ServicePlanNumber'),
                ServiceDate: this.getControlValue('ServiceDate'),
                AcceptAllStatus: this.getControlValue('AcceptAllStatus'),
                EmployeeCode: this.getControlValue('EmployeeCode'),
                VisitReference1: this.getControlValue('VisitReference1'),
                UpdatedQuantity: this.riGrid.Details.GetValue('UpdateServicedQuantity')
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGrid.Update = false;
                    this.riGridBeforeExecute();
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        } else {
            this.setFocusOnEmployeeCode.emit(true);
            this.setControlValue('AcceptAll', !this.getControlValue('AcceptAll'));
        }
    }

    public serviceDateOnChange(data: any): void {
        if (data.value) this.setControlValue('ServiceDate', data.value);
        this.resetAcceptAll(false);
    }

    public visitReference1OnChange(): void {
        this.pageParams.vVisitRefChange = true;
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'VisitReference1') || !this.getControlValue('VisitReference1')) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'VisitReference1', true);
            this.uiForm.controls['VisitReference1'].markAsDirty();
        }
        if (this.riExchange.riInputElement.isError(this.uiForm, 'VisitReference1') && this.getControlValue('VisitReference1')
            && (!this.getControlValue('VisitReference1').toString().includes('"') && !this.getControlValue('VisitReference1').toString().startsWith('?'))) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'VisitReference1', false);
            this.uiForm.controls['VisitReference1'].markAsPristine();
        }
    }

    public cmdSelectUnprocessedOnClick(): void {
        this.pageParams.requestCacheRefresh = false;
        this.pageParams.vSelectAll = true;
        this.riGridBeforeExecute();
    }
    public cmdProcessSelectionOnClick(): void {
        this.isProcessedClick = true;
        this.warnDates();
    }

    public cmdClearAllOnClick(): void {
        this.pageParams.vClearAll = true;
        this.pageParams.isAllSelectClick = false;
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ServiceDate', true);
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'ServiceDate', true);
        this.setControlValue('ServiceDate', '');
        this.riGridBeforeExecute();
    }

    public cmdApplyDatesOnClick(): void {
        this.pageParams.isAllSelectClick = true;
        this.pageParams.vApplyDates = true;
        this.riGridBeforeExecute();
    }
    /*
    *  Alerts user when user is moving away without saving the changes. //CR implementation
    */
    public canDeactivate(): Observable<boolean> {
        this.routeAwayGlobals.setSaveEnabledFlag(this.pageParams.vVisitRefChange);
        let messageText: string = MessageConstant.PageSpecificMessage.iCABSSeGroupServiceVisitEntryGrid.unloadTestText;
        this.canDeactivateObservable = new Observable((observer) => {
            let modalVO: ICabsModalVO = new ICabsModalVO(messageText, null);
            if (this.pageParams.vVisitRefChange && !this.navigateOnGridClick) {  // Fix for IUI-18931
                modalVO.cancelCallback = () => {
                    this.cancelEvent({}, observer);
                };
                modalVO.confirmCallback = () => {
                    observer.next(true);
                };
                this.modalAdvService.emitPrompt(modalVO);
                return;
            }
            observer.next(true);
        });
        return this.canDeactivateObservable;
    }

    public cancelEvent(event: any, observer: any): void {
        if (this.variableService.getBackClick() === true) {
            this.variableService.setBackClick(false);
            this.location.go(this.utils.getCurrentUrl());
        }
        observer.next(false);
        setTimeout(() => {
            this.router.navigate([], { skipLocationChange: true, preserveQueryParams: true });
        }, 0);
    }

}
