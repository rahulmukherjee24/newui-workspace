import { Component, OnInit, Injector, ViewChild, EventEmitter, OnDestroy } from '@angular/core';
import { QueryParams } from '@shared/services/http-params-wrapper';

import { BaseComponent } from '@base/BaseComponent';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';

import { BranchServiceAreaSearchComponent } from '@internal/search/iCABSBBranchServiceAreaSearch';

@Component({
    templateUrl: 'iCABSARWasteTransferNotesPrint.html'
})

export class WasteTransferNotesPrintComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private queryParams: any = {
        module: 'waste',
        method: 'service-delivery/maintenance',
        operation: 'ApplicationReport/iCABSARWasteTransferNotesPrint'
    };
    private callBuildGrid: boolean;

    public controls: Array<any> = [
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'BranchServiceAreaCode', disabled: false, type: MntConst.eTypeCode, commonValidator: true },
        { name: 'DateFrom', required: true, disabled: false, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, disabled: false, type: MntConst.eTypeDate },
        { name: 'EmployeeSurName', disabled: true, type: MntConst.eTypeTextFree },
        { name: 'PlanVisitsDue', disabled: false, type: MntConst.eTypeCheckBox },
        { name: 'RegulatoryAuthorityName', disabled: false, type: MntConst.eTypeText, commonValidator: true },
        { name: 'RegulatoryAuthorityNumber', disabled: false, type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'SelectedReportNumber', disabled: false },
        { name: 'SelectedRowid', disabled: false }
    ];
    public dropdown: any = {
        regulatoryAuthorityNumber: {
            params: {
                'parentMode': 'LookUp'
            },
            active: {
                id: '',
                text: ''
            }
        }
    };
    public pageId: string = '';
    public ellipsis: any = {
        branchServiceAreaCode: {
            childConfigParams: {
                parentMode: 'LookUp-Emp'
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: BranchServiceAreaSearchComponent
        }
    };
    public reportGenMsg: string;
    public setFocusOnBranchServiceAreaCode = new EventEmitter<boolean>();
    public ttRegulatoryAuthority: Array<any> = [];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARWASTETRANSFERNOTESPRINT;
        this.browserTitle = this.pageTitle = 'Waste Transfer Notes';
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.buildGrid();
        } else {
            this.windowOnload();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.setCurrentContractType();

        this.pageParams.vSCEnableDurations = true;
        this.setControlValue('BranchNumber', this.utils.getBranchCode());

        let current: Date = new Date();
        if (current.getMonth() === 11) {
            this.setControlValue('DateFrom', new Date(current.getFullYear() + 1, 0, 1));
            this.setControlValue('DateTo', new Date(current.getFullYear() + 2, 0, 0));
        } else {
            this.setControlValue('DateFrom', new Date(current.getFullYear(), current.getMonth() + 1, 1));
            this.setControlValue('DateTo', new Date(current.getFullYear(), current.getMonth() + 2, 0));
        }
        this.setButtonDisabled(true);
        this.setControlValue('BranchServiceAreaCode', '');
        this.setControlValue('PlanVisitsDue', true);
        this.setFocusOnBranchServiceAreaCode.emit(true);
        this.lookUpCall();
        this.buildGrid();
    }

    // set current page data
    private setCurrentContractType(): void {
        this.pageParams.currentContractType = this.riExchange.getCurrentContractType();
        this.pageParams.currentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
    }
    // lookUp Table Data Fetch
    private lookUpCall(): void {
        let lookupIP: Array<any> = [
            {
                'table': 'RegulatoryAuthority',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['RegulatoryAuthorityNumber', 'RegulatoryAuthorityName']
            },
            {
                'table': 'Branch',
                'query': {
                    'BranchNumber': this.utils.getBranchCode()
                },
                'fields': ['BranchName']
            }
        ];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupIP).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                if (data && data.length > 0) {
                    if (data[0] && data[0].length > 0) {
                        data[0].forEach(element => {
                            this.ttRegulatoryAuthority.push({
                                'RegulatoryAuthorityNumber': element.RegulatoryAuthorityNumber,
                                'RegulatoryAuthorityName': element.RegulatoryAuthorityName
                            });
                        });
                        this.utils.sortByKey(this.ttRegulatoryAuthority, 'RegulatoryAuthorityName');
                    }
                    if (data[1] && data[1].length > 0) {
                        if (data[1][0].BranchName) this.setControlValue('BranchName', data[1][0].BranchName);
                    }
                } else {
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                }
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    private setButtonDisabled(lDisabled: boolean): void {
        setTimeout(() => {
            this.pageParams.cmdGenerate = lDisabled;
            this.pageParams.cmdPrint = lDisabled;
        }, 0);
    }

    private showReport(lSingleReport: boolean): void {
        let cFromBranchServiceAreaCode: string;
        let cToBranchServiceAreaCode: string;
        let cFromRegulatoryAuthorityNumber: string;
        let cToRegulatoryAuthorityNumber: string;
        let cPlanVisits: string;
        let cFromReportNumber: string;
        let cToReportNumber: string;

        if (!this.getControlValue('BranchServiceAreaCode')) {
            cFromBranchServiceAreaCode = '!';
            cToBranchServiceAreaCode = 'ZZZZZZZZZ';
        } else {
            cFromBranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
            cToBranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        }

        if (this.getControlValue('PlanVisitsDue')) cPlanVisits = '1';
        else cPlanVisits = '0';

        if (!this.getControlValue('RegulatoryAuthorityNumber')) {
            cFromRegulatoryAuthorityNumber = '0';
            cToRegulatoryAuthorityNumber = '999999';
        } else {
            cFromRegulatoryAuthorityNumber = this.getControlValue('RegulatoryAuthorityNumber') || '0';
            cToRegulatoryAuthorityNumber = this.getControlValue('RegulatoryAuthorityNumber') || '0';
        }

        if (lSingleReport) {
            cFromReportNumber = this.getControlValue('SelectedReportNumber');
            cToReportNumber = this.getControlValue('SelectedReportNumber');
        } else {
            cFromReportNumber = '0';
            cToReportNumber = '99999999';
        }
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        let postData: Object = {
            BranchNumber: this.utils.getBranchCode(),
            FromBranchServiceAreaCode: cFromBranchServiceAreaCode,
            ToBranchServiceAreaCode: cToBranchServiceAreaCode,
            PlanVisitsDueInd: cPlanVisits,
            FromDate: this.getControlValue('DateFrom'),
            ToDate: this.getControlValue('DateTo'),
            FromRegulatoryAuthorityNumber: cFromRegulatoryAuthorityNumber,
            ToRegulatoryAuthorityNumber: cToRegulatoryAuthorityNumber,
            FromReportNumber: cFromReportNumber,
            ToReportNumber: cToReportNumber
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                let url: string = data.url;
                window.open(url, '_blank');
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    private clientSideValidation(): boolean {
        let clientSideValidation: boolean = false;
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'DateFrom')
            && !this.riExchange.riInputElement.isError(this.uiForm, 'DateTo')) {
            clientSideValidation = true;
        }
        return clientSideValidation;
    }

    /* ----------- Grid Routines ----------*/
    // Create grid staructure
    private buildGrid(): void {
        this.pageParams.totalRecords = 0;
        this.pageParams.pageSize = 10;
        this.pageParams.curPage = 1;
        this.pageParams.requestCacheRefresh = true;
        this.pageParams.riGridHandle = this.utils.randomSixDigitString();

        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.FunctionUpdateSupport = true;
        this.riGrid.Clear();
        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.riGrid.AddColumn('BranchServiceAreaCode', 'Letter', 'BranchServiceAreaCode', MntConst.eTypeCode, 4);
            this.riGrid.AddColumn('EmployeeSurname', 'Letter', 'EmployeeSurname', MntConst.eTypeCode, 30);
        }

        this.riGrid.AddColumn('ContractNumber', 'Letter', 'ContractNumber', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseNumber', 'Letter', 'PremiseNumber', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseName', 'Letter', 'PremiseName', MntConst.eTypeTextFree, 30);
        this.riGrid.AddColumn('LetterDate', 'Letter', 'LetterDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('LetterDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ExpiryDate', 'Letter', 'ExpiryDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ExpiryDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('Printed', 'Letter', 'Printed', MntConst.eTypeImage, 1, true);

        this.riGrid.Complete();
    }

    private riGridBeforeExecute(): void {
        if (this.clientSideValidation()) {
            let gridQueryParams: QueryParams = this.getURLSearchParamObject();
            gridQueryParams.set(this.serviceConstants.Action, '2');

            gridQueryParams.set('BranchNumber', this.utils.getBranchCode());
            gridQueryParams.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
            gridQueryParams.set('DateFrom', this.getControlValue('DateFrom'));
            gridQueryParams.set('DateTo', this.getControlValue('DateTo'));
            gridQueryParams.set('PlanVisitsDue', this.getControlValue('PlanVisitsDue'));
            gridQueryParams.set('RegulatoryAuthorityNumber', this.getControlValue('RegulatoryAuthorityNumber') || '0');

            gridQueryParams.set(this.serviceConstants.PageSize, this.pageParams.pageSize.toString());
            gridQueryParams.set(this.serviceConstants.PageCurrent, this.pageParams.curPage.toString());
            gridQueryParams.set(this.serviceConstants.GridCacheRefresh, this.pageParams.requestCacheRefresh);
            gridQueryParams.set(this.serviceConstants.GridMode, '0');
            gridQueryParams.set(this.serviceConstants.GridHandle, this.pageParams.riGridHandle);
            gridQueryParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
            gridQueryParams.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridQueryParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.riGrid.ResetGrid();
                    } else {
                        this.pageParams.curPage = data.pageData.pageNumber || 1;
                        this.pageParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.pageSize : 1;
                        this.riGrid.RefreshRequired();
                        if (this.riGrid.Update) {
                            this.riGrid.StartRow = this.getAttribute('Row');
                            this.riGrid.StartColumn = 0;
                            this.riGrid.RowID = this.getAttribute('PlanVisitRowID');
                            this.riGrid.UpdateHeader = false;
                            this.riGrid.UpdateBody = true;
                            this.riGrid.UpdateFooter = false;
                        }
                        this.riGrid.Execute(data);
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    private selectedRowFocus(rsrcElement: any): void {
        this.setAttribute('SelectedRowid', this.riGrid.Details.GetAttribute('ContractNumber', 'rowid'));
        this.setAttribute('SelectedReportNumber', this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty'));
        this.setControlValue('SelectedReportNumber', this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty'));
    }

    public riGridAfterExecute(event: any): void {
        this.setButtonDisabled(false);
    }

    public riGridBodyOnClick(event: Object): void {
        this.selectedRowFocus(event['srcElement']);
    }

    public riGridBodyOnDblClick(event: Object): void {
        this.selectedRowFocus(event['srcElement']);
        if (this.riGrid.CurrentColumnName === 'Printed' && this.riGrid.Details.GetValue('Printed') === 'sp') {
            this.showReport(true);
        }
    }


    public getCurrentPage(event: Object): void {
        this.pageParams.curPage = event['value'];
        this.riGridBeforeExecute();
    }

    public refresh(): void {
        this.riExchange.validateForm(this.uiForm);
        if (this.uiForm.status) {
            if (this.pageParams.curPage <= 0) this.pageParams.curPage = 1;
            if (this.callBuildGrid) {
                this.buildGrid();
                this.callBuildGrid = false;
            }
            this.riGridBeforeExecute();
        }
    }

    public riGridSort(event: Object): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    /* =================== Page UI Routines ============*/
    public branchServiceAreaCodeOnChange(data: any): void {
        this.riGrid.RefreshRequired();
        this.callBuildGrid = true;
        if (this.getControlValue('BranchServiceAreaCode')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let postData: Object = {
                Function: 'GetEmployeeSurname',
                BranchNumber: this.utils.getBranchCode(),
                BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode')
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('EmployeeSurName', data.EmployeeSurname || '');
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        } else {
            this.setControlValue('EmployeeSurName', '');
        }
    }

    public regulatoryAuthorityNumberOnChange(data: any): void {
        this.riGrid.RefreshRequired();
        this.setButtonDisabled(true);
    }

    public branchServiceAreaCodeOnReceived(obj: any, val: boolean): void {
        if (obj) {
            this.setControlValue('BranchServiceAreaCode', obj.BranchServiceAreaCode || '');
            this.setControlValue('EmployeeSurName', obj.EmployeeSurname || '');
            this.riGrid.RefreshRequired();
            this.callBuildGrid = true;
        }
    }

    public cmdGenerateOnClick(): void {
        let cPlanVisits: boolean;
        if (this.clientSideValidation()) {
            if (this.getControlValue('PlanVisitsDue')) {
                cPlanVisits = true;
            } else {
                cPlanVisits = false;
            }

            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let postData: Object = {
                Function: 'GenerateBatch',
                BranchNumber: this.utils.getBranchCode(),
                BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
                DateFrom: this.getControlValue('DateFrom'),
                DateTo: this.getControlValue('DateTo'),
                PlanVisitsDue: this.getControlValue('PlanVisitsDue'),
                RegulatoryAuthorityNumber: this.getControlValue('RegulatoryAuthorityNumber') || '0'
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.pageParams.isTRInformation = true;
                    this.reportGenMsg = data.ReturnHTML;
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        }
    }
    public cmdPrintOnClick(): void {
        if (this.clientSideValidation()) {
            this.showReport(false);
        }
    }
}
