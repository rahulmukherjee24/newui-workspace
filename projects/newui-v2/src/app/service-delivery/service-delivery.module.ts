import { VisitActionMaintenanceComponent } from './TableMaintenanceBusiness/VisitAction/iCABSBVisitActionMaintenance.component';
import { EnvAgencyQuarterlyReturnComponent } from './ProcessWasteConsignmentNote/envagencyquarterly/iCABSAREnvAgencyQuarterlyReturn.component';
import { ServiceAreaAllocationGridComponent } from './ProcessAreaMaintenance/ServiceAreaAllocation/iCABSBServiceAreaAllocationGrid.component';
import { ServiceVisitRejectionsGridComponent } from './PDAReturns/VisitRejections/iCABSSeServiceVisitRejectionsGrid';
import { ReturnedPaperWorkGridComponent } from './../contract-management/LettersAndLabels/Non-ReturnedPaperworkAudit/iCABSARReturnedPaperWorkGrid.component';
import { PDAWorkListEntryGridComponent } from './VisitMaintenance/iCABSSePDAWorkListEntryGrid.component';
import { GroupServiceVisitEntryGridComponent } from './VisitMaintenance/GroupVisitMaintenance/iCABSSeGroupServiceVisitEntryGrid.component';
import { BlankConsignmentNotePrintComponent } from './ProcessWasteConsignmentNote/BlankConsignmentNotes/iCABSSeBlankConsignmentNotePrint.component';
import { VanLoadingReportComponent } from './DeliveryPaperwork/VanLoadingReport/iCABSARVanLoadingReport.component';
import { ProductLanguageMaintenanceComponent } from './Translation/Product Language/iCABSBProductLanguageMaintenance.component';
import { DailyPrenotificationReportComponent } from './ProcessWasteConsignmentNote/DailyPrenotificationReport/iCABSARDailyPrenotificationReport.component';
import { ValidLinkedProductsGridComponent } from './TableMaintenanceProduct/ValidLinkedProducts/iCABSBValidLinkedProductsGrid.component';
import { ARPrenotificationReportComponent } from './ProcessWasteConsignmentNote/AnnualPrenotificationReport/iCABSARPrenotificationReport.component';
import { EnvAgencyExceptionsComponent } from './ProcessWasteConsignmentNote/EnvironmentAgencyExceptions/iCABSAREnvAgencyExceptions.component';
import { InstallReceiptComponent } from './DeliveryPaperwork/iCABSSInstallReceipt.component';
import { ProductSalesDeliveriesDueGridComponent } from './ReportsPlanning/iCABSAProductSalesDeliveriesDueGrid.component';
import { SeDebriefEmployeeGridComponent } from './ProcessServiceActionMonitor/iCABSSeDebriefEmployeeGrid';
import { SePESVisitGridComponent } from './PDAReturns/iCABSSePESVisitGrid.component';
import { ServiceCallTypeGridComponent } from './ReportsWorkload/ServiceCallTypeBranch/iCABSServiceCallTypeGrid.component';
import { ServiceDocketDataEntryComponent } from './VisitMaintenance/iCABSSeServiceDocketDataEntry';
import { SeDebriefBranchGridComponent } from './ProcessServiceActionMonitor/iCABSSeDebriefBranchGrid.component';
import { WorkListConfirmComponent } from './DeliveryPaperwork/Work-list/iCABSWorkListConfirm.component';
import { ServicePlanDeliveryNoteProductComponent } from './DeliveryPaperwork/SingleServiceReceipt/iCABSSeServicePlanDeliveryNotebyProduct.component';
import { ServicePlanDeliveryNoteGridComponent } from './DeliveryPaperwork/iCABSSeServicePlanDeliveryNoteGrid.component';
import { HCARiskAssessmentGridComponent } from './PDAReturns/RiskAssessment/iCABSSeHCARiskAssessmentGrid.component';
import { TechnicianWorkSummaryComponent } from './PDAReturns/TechWorkGrid/iCABSSeTechnicianWorkSummaryGrid.component';
import { TechnicianSyncSummaryGridComponent } from './PDAReturns/iCABSSeTechnicianSyncSummaryGrid.component';
import { ServiceDeliveryRouteDefinitions } from './service-delivery.route';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { BranchServiceAreaGridComponent } from './TableMaintenanceBusiness/ServiceArea/iCABSBBranchServiceAreaGrid.component';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { ServiceDeliveryRootComponent } from './service-delivery.component';
import { SharedModule } from '@shared/shared.module';

import { AREnvAgencyBusinessWasteComponent } from './ProcessWasteConsignmentNote/EnvironmentAgencyBusinessWasteGenerationReport/iCABSAREnvAgencyBusinessWaste.component';
import { WasteConsignmentNoteGenerateComponent } from './ProcessWasteConsignmentNote/WasteConsignmentNoteGeneration/iCABSSeWasteConsignmentNoteGenerate.component';
import { WasteConsignmentNoteGenerateDetailComponent } from './ProcessWasteConsignmentNote/WasteConsignmentNoteGeneration/iCABSSeWasteConsignmentNoteGenerateDetail.component';
import { WasteTransferNotesPrintComponent } from './LettersAndLabels/WasteTransferNotes/iCABSARWasteTransferNotesPrint.component';
import { BranchServiceAreaEmployeeGridComponent } from '@internal/grid-search/iCABSBBranchServiceAreaEmployeeGrid.component';

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchEllipsisModule,
        InternalSearchModule,
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        ServiceDeliveryRouteDefinitions,
        SharedModule
    ],
    declarations: [
        ServiceDeliveryRootComponent,
        TechnicianSyncSummaryGridComponent,
        TechnicianWorkSummaryComponent,
        HCARiskAssessmentGridComponent,
        ServicePlanDeliveryNoteGridComponent,
        ServicePlanDeliveryNoteProductComponent,
        WorkListConfirmComponent,
        SeDebriefBranchGridComponent,
        ServiceDocketDataEntryComponent,
        ServiceCallTypeGridComponent,
        SePESVisitGridComponent,
        SeDebriefEmployeeGridComponent,
        ProductSalesDeliveriesDueGridComponent,
        InstallReceiptComponent,
        EnvAgencyExceptionsComponent,
        ARPrenotificationReportComponent,
        BranchServiceAreaGridComponent,
        ValidLinkedProductsGridComponent,
        DailyPrenotificationReportComponent,
        ProductLanguageMaintenanceComponent,
        VanLoadingReportComponent,
        BlankConsignmentNotePrintComponent,
        GroupServiceVisitEntryGridComponent,
        PDAWorkListEntryGridComponent,
        ReturnedPaperWorkGridComponent,
        ServiceVisitRejectionsGridComponent,
        AREnvAgencyBusinessWasteComponent,
        ServiceAreaAllocationGridComponent,
        WasteTransferNotesPrintComponent,
        EnvAgencyQuarterlyReturnComponent,
        WasteConsignmentNoteGenerateComponent,
        VisitActionMaintenanceComponent,
        WasteConsignmentNoteGenerateDetailComponent,
        BranchServiceAreaEmployeeGridComponent
    ]
})
export class ServiceDeliveryModule { }
