export class UplusComponent {
    public static readonly List: Array<any> = [
        { name: 'Add a New Contract Pest, Washroom & Ambius Admin', link: 'https://youtu.be/dCg8hCatlX4' },
        { name: 'Pest: Adding a New Premises', link: 'https://youtu.be/2-FYF6Ea7Ts' },
        { name: 'Contact Centre Search Pt 1 Customer Support', link: 'https://youtu.be/J1xk_TFwxCU' },
        { name: 'Contact Centre Search Pt 2 Customer Support', link: 'https://youtu.be/SbBFSaxwPLM' },
        { name: 'Contact Centre Search Pt 3 Customer Support', link: 'https://youtu.be/Q9dbnrQM49I' },
        { name: 'Add a new Service Cover - Pest', link: 'https://youtu.be/4qXtnMUJZXA' },
        { name: 'Add a new Service Cover - Ambius', link: 'https://youtu.be/V_63UQhZNqw' },
        { name: 'Add a new Service Cover - Washrooms', link: 'https://youtu.be/7MIZ7b78kHU' }
    ];
}
