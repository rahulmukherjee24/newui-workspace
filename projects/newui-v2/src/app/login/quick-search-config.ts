import { QueryParams } from './../../shared/services/http-params-wrapper';
import { HttpService } from './../../shared/services/http-service';
import { MntConst } from './../../shared/services/riMaintenancehelper';
import { Utils } from './../../shared/services/utility';
import { ServiceConstants } from './../../shared/constants/service.constants';

export class QuickSearch {
    private countryCode: any;
    private businessCode: any;
    private branchCode: any;
    public method: string = 'contract-management/search';
    public module: string = 'contract';
    public operation: string = 'Application/iCABSAContractSearch';
    private readonly title: string = 'Contract Search';
    private readonly pageTitle: string = '';
    private readonly rows: Array<any> = [];
    public columns: Array<any> = [];
    public inputParams: any = {};
    public isViewVisible: boolean = true;

    constructor(private util: Utils, private searchService: HttpService, private serviceConstants: ServiceConstants, private search: QueryParams) {
        this.countryCode = this.util.getCountryCode();
        this.businessCode = this.util.getBusinessCode();
        this.branchCode = this.util.getBranchCode();
    }

    public setSearchParams(selectedOption: any, searchValue: any): QueryParams {
        let ret: any = {};
        this.search = new QueryParams();
        this.serviceConstants = new ServiceConstants();
        switch (selectedOption) {
            case 'nb':
                this.search.set('ContractName', searchValue);
                this.search.set('search.op.ContractName', 'BEGINS');
                break;
            case 'nm':
                this.search.set('ContractName', searchValue);
                this.search.set('search.op.ContractName', 'CONTAINS');
                break;
            case 'num':
                this.search.set('ContractNumber', searchValue);
                this.search.set('search.op.ContractNumber', 'GE');
                break;
        }
        this.search.set(this.serviceConstants.Action, '0');
        this.search.set(this.serviceConstants.CountryCode, this.util.getCountryCode());
        this.search.set(this.serviceConstants.BusinessCode, this.util.getBusinessCode());
        this.search.set('NegBranchNumber', this.util.getBranchCode());
        this.search.set('PortfolioStatusCode', 'All');
        this.search.set('pageSize', '10');
        this.search.set('email', 'rahul.mukherjee@rentokil-initial.com');

        this.inputParams.module = this.module;
        this.inputParams.method = this.method;
        this.inputParams.operation = this.operation;
        this.inputParams.search = this.search;
        this.inputParams.isViewVisible = this.isViewVisible;
        this.buildTableColumns();
        ret['inputParams'] = this.inputParams;
        ret['columns'] = this.columns;
        return ret;
    }

    public buildTableColumns(): void {
        this.columns = [
            { title: 'Prefix', name: 'ContractTypePrefix', type: MntConst.eTypeCode },
            { title: 'Number', name: 'ContractNumber', type: MntConst.eTypeLink },
            { title: 'Name', name: 'ContractName', type: MntConst.eTypeText },
            { title: 'Commence Date', name: 'ContractCommenceDate', type: MntConst.eTypeDate },
            { title: 'Status', name: 'PortfolioStatusDesc', type: MntConst.eTypeText },
            { title: 'Expiry Date', name: 'ContractExpiryDate', type: MntConst.eTypeDate },
            { title: 'Account', name: 'AccountName', type: MntConst.eTypeText }
        ];
    }
}
