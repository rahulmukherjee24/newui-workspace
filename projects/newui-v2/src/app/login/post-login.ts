
import { QueryParams } from '@shared/services/http-params-wrapper';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { ContractManagementModuleRoutes, AppModuleRoutes, CCMModuleRoutes } from './../base/PageRoutes';
import { TableComponent } from './../../shared/components/table/table';
import { Utils } from './../../shared/services/utility';
import { CBBService } from './../../shared/services/cbb.service';
import { ServiceConstants } from './../../shared/constants/service.constants';
import { HttpService } from './../../shared/services/http-service';
import { QuickSearch } from './quick-search-config';
import { QuickLinks } from './quickLinks';
import { CallCenterActionTypes } from './../actions/call-centre-search';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy, ViewChild, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../shared/services/auth.service';
import { ErrorService } from '../../shared/services/error.service';
import { GlobalConstant } from '../../shared/constants/global.constant';
import { ComponentInteractionService } from '../../shared/services/component-interaction.service';
import { ContractActionTypes } from '@app/actions/contract';

@Component({
    selector: 'icabs-post-login',
    templateUrl: 'post-login.html'
})

export class PostLoginComponent implements OnInit, OnDestroy {
    @ViewChild('errorModal') public errorModal;
    @ViewChild('contractTable') contractTable: TableComponent;

    public authJson: any;
    public muleJson: any;
    public displayNavBar: boolean;
    public showErrorHeader: boolean = true;
    public componentInteractionSubscription: Subscription;
    public errorSubscription: Subscription;
    public routerSubscription: Subscription;
    public name: string;
    public rowArr: Array<any> = [];
    public colArr: Array<any> = [];
    public search: QueryParams;
    public filter: Array<any> = [
        { options: 'Name begins', value: 'nb' },
        { options: 'Name matches', value: 'nm' },
        { options: 'Number', value: 'num' }
    ];
    public searchFilter: any = this.filter[0].value;
    public contractSearchType: Array<any> = [];
    public statusList: Array<any> = [];
    public searchValue: any;
    public isQuickSearchVisible: boolean = false;
    public isSearchValue: boolean = false;
    public columns: Array<any> = [];
    public isDataLoaded: boolean = false;
    public isRequesting: boolean = false;

    constructor(private _authService: AuthService, private _router: Router, private _ls: LocalStorageService, private _componentInteractionService: ComponentInteractionService, private _zone: NgZone, private _errorService: ErrorService, private sessionStorage: SessionStorageService, private titleService: Title, private store: Store<any>, private serviceConstants: ServiceConstants, private searchService: HttpService, private cbbService: CBBService, private util: Utils) {
        this.store.dispatch({
            type: CallCenterActionTypes.LAST_VISITED_TAB,
            payload: 0
        });

        this.componentInteractionSubscription = this._componentInteractionService.getObservableSource().subscribe(msg => {
            if (msg !== 0) {

                this._zone.run(() => {
                    this.displayNavBar = msg;
                });
            }
        });
        this.routerSubscription = this._router.events.subscribe(event => {
            if (event['url'] === '/postlogin') {
                this._componentInteractionService.emitMessage(true);
            }
        });
    }

    ngOnInit(): void {
        this.displayNavBar = true;
        this.titleService.setTitle(GlobalConstant.Configuration.AppName);
        this._authService.handleClientLoad(false, false);
        if (!this._authService.isSignedIn()) {
            this._componentInteractionService.emitMessage(false);
            this._authService.signOut();
            this._router.navigate(['/application/login']);
            return;
        }

        //this._authService.getUserAccessDetails();

        if (!this._authService.oauthResponse) {
            this.authJson = this._ls.retrieve('OAUTH');
        } else {
            this.authJson = this._authService.oauthResponse;
        }

        this._errorService.emitError(0);
        this.errorSubscription = this._errorService.getObservableSource().subscribe(error => {
            if (error !== 0) {
                this._zone.run(() => {
                    if (error.redirect) {
                        setTimeout(() => {
                            //this.errorModal.show({ error: error });
                            this._authService.clearData();
                            this._authService.signOut();
                            this._router.navigate(['/application/login']);
                        }, 1000);
                    } else {
                        this.errorModal.show({ error: error });
                    }
                });
            }
        });

        this.muleJson = this._authService.getMuleResponse();

        // Remove Nav Stack
        this.sessionStorage.clear('NAVIGATION_STACK');
        this.store.dispatch({
            type: ContractActionTypes.CLEAR_ALL,
            payload: ''
        });
        this.name = this._authService.displayName || this._ls.retrieve('DISPLAYNAME');
        this.isSearchValue = true;
    }

    ngOnDestroy(): void {
        if (this.componentInteractionSubscription) {
            this.componentInteractionSubscription.unsubscribe();
        }

        if (this.errorSubscription) {
            this.errorSubscription.unsubscribe();
        }
        if (this.routerSubscription) {
            this.routerSubscription.unsubscribe();
        }
        //this.riExchange.releaseReference(this);
    }

    public get quickLinks(): any {
        return QuickLinks.List;
    }

    public onRefresh(): void {
        this.isSearchValue = false;
        if (JSON.stringify(QuickLinks.List).indexOf(AppModuleRoutes.CCM + CCMModuleRoutes.ICABSCMCALLCENTREGRID) !== -1 && this.searchValue && this.searchFilter) {
            this.isSearchValue = true;
            this.isRequesting = true;
            let quickSearchObj = new QuickSearch(this.util, this.searchService, this.serviceConstants, this.search);
            let quickSearchParams: any = quickSearchObj.setSearchParams(this.searchFilter, this.searchValue);
            this.columns = quickSearchParams.columns;
            this.contractTable.loadTableData(quickSearchParams.inputParams);
        }
    }

    public linkClicked(event: any): void {
        this._router.navigate([ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE], { queryParams: { parentMode: 'Search', ContractNumber: event.row.ContractNumber, CurrentContractType: event.row.ContractTypePrefix } });
    }

    public nextPage(event: any): void {
        event.preventDefault();
        this.isRequesting = true;
        this.contractTable.nextPage();
    }

    public prevPage(event: any): void {
        event.preventDefault();
        this.isRequesting = true;
        this.contractTable.prevPage();
    }

    public tableDataLoaded(data: any): void {
        this.isDataLoaded = true;
        this.isRequesting = false;
    }

}
