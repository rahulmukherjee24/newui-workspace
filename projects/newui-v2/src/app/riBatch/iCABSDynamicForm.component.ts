import { Component, OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';
import { environment } from './../../environments/environment';
import { Subscription } from 'rxjs';

import { Utils } from '@shared/services/utility';
import { GlobalNotificationsService } from '@shared/services/global.notifications.service';

import { IForm, IFormSchema } from '@app/base/GenericFormType';
import { MessageConstant } from '@shared/constants/message.constant';
import { EFolderTypes, EFormTypeConstants } from './GenericForm.constants';

@Component({
    templateUrl: 'iCABSDynamicForm.html'
})

export class ICABSDynamicFormComponent implements OnInit, AfterContentInit, OnDestroy {
    protected isRequesting: boolean = false;
    activatedRouteSubscription: Subscription;
    baseURL = environment['FIREBASE_AU_NZ_FORM_URL'];
    pageTitle: string = '';
    menuAccessList: IFormSchema[] = [{ name: 'Option', programURL: '', configPath: ''}];
    reportName: IFormSchema = this.menuAccessList[0];
    response: IForm;

    constructor(private activatedRoute: ActivatedRoute, private http: HttpClient, private utils: Utils, private notification: GlobalNotificationsService) { }

    public ngOnInit(): void {
        this.activatedRouteSubscription = this.activatedRoute.queryParams.subscribe(
            (param: Params) => {
                if (param) {
                    this.pageTitle = param.pageType ;
                    this.utils.setTitle(this.pageTitle);
            }
        });
    }

    public ngAfterContentInit(): void {
        this.isRequesting = true;
        this.fetchStoreFile(EFormTypeConstants[this.pageTitle.replace(/[^a-zA-Z0-9]/g,'').toUpperCase()] , EFolderTypes.INDEX)
            .then((data: IFormSchema[]) => {
                let menuList: IFormSchema[] = [];
                data.forEach( report  => {
                    if (this.utils.hasMenuAccessList(report.programURL))
                        menuList.push(report);
                });
                menuList = this.utils.sortByKey(menuList, 'name', false , true);
                this.menuAccessList = [...this.menuAccessList, ...menuList];
                this.isRequesting = false;
            })
            .catch(err => {
                this.isRequesting = false;
                this.notification.displayMessage({ errormessage : this.pageTitle + ' ' + err.statusText});
            });
    }

    public ngOnDestroy(): void {
        if (this.activatedRouteSubscription) {
            this.activatedRouteSubscription.unsubscribe();
        }
    }

    public fetchStoreFile(fileName: string, type: string): any {
        return this.http.get<IFormSchema[] | IForm >( this.baseURL + type + '/' + fileName.toLowerCase() + '.json').toPromise();
    }

    public onReportSelection(): void {
        this.response = null;
        if (this.reportName.name === 'Option') {
            return;
        }
        if (this.reportName.configPath) {
            this.fetchStoreFile(this.reportName.configPath, EFolderTypes.FORM)
            .then((data: IForm) => {
                this.response = data;
            })
            .catch(error => {
                this.notification.displayMessage({ errorMessage: 'Check logs'});
            });
        } else {
            this.notification.displayMessage({ errormessage : MessageConstant.Message.PageNotCovered });
        }
    }
}
