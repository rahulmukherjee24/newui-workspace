import { Component, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { SharedModule } from '@shared/shared.module';
import { ICABSDynamicFormComponent } from './iCABSDynamicForm.component';
import { ICABSGenericFormComponent } from './iCABSGenericForm.component';
import { RiBatchModuleRoutes } from '@app/base/PageRoutes';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class RiBatchProcessComponent {
}

@NgModule({
    imports: [
        SharedModule,
        HttpClientModule,
        RouterModule.forChild([
            {    path: '', component: RiBatchProcessComponent, children: [
                    {   path: RiBatchModuleRoutes.ICABSRIBATCHBRANCHFUNCTION , component: ICABSDynamicFormComponent },
                    {   path: RiBatchModuleRoutes.ICABSRIBATCHFINANCIALACCOUNTS , component: ICABSDynamicFormComponent },
                    {   path: RiBatchModuleRoutes.ICABSRIBATCHSTOCKCONTROL , component: ICABSDynamicFormComponent },
                    {   path: RiBatchModuleRoutes.ICABSRIBATCHDISPLAYANDLOCATIONS , component: ICABSDynamicFormComponent }
                ], data: { domain: 'RIBATCH' }
            }
        ])
    ],
    declarations: [
        RiBatchProcessComponent,
        ICABSDynamicFormComponent,
        ICABSGenericFormComponent
    ]

})

export class RiBatchModule {
}
