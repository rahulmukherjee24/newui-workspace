export enum EFormTypeConstants {
    BRANCHFUNCTIONSEXTRACTS = 'branchfunctionsextracts',
    DISPLAYSLOCATIONSEXTRACTS = 'displays&locationsextracts',
    FINANCIALACCOUNTSEXTRACTS = 'financialaccountsextracts',
    STOCKCONTROLEXTRACTS = 'stockcontrolextracts'
}

export enum EControlTypes {
    DATEPICKER = 'datepicker',
    TEXT = 'text',
    EMAIL = 'email',
    CHECKBOX = 'checkbox',
    DROPDOWN = 'dropdown',
    BUTTON = 'button'
}

export enum EDefaultTypes {
    USEREMAIL = 'userEmail',
    USERNAME = 'userName',
    CURRENTDATE = 'currentDate',
    FIRSTDATEOFMONTH = 'firstDateOfMonth',
    LASTDATEOFMONTH = 'lastDateOfMonth',
    CURRENTYEAR = 'currentYear',
    GETMONTH = 'getMonth',
    CURRENTMONTH = 'currentMonth',
    GETBRANCHLIST = 'getBranchList',
    BUSINESSCODE = 'businessCode',
    BUSINESSDESC = 'businessDesc'
}

export enum EFolderTypes {
    FORM = 'form',
    INDEX = 'index'
}

export enum EServiceExtraParamType {
    BRANCHNUMBER = 'BRANCHNUMBER',
    LANGUAGECODE = 'LANGUAGECODE'
}

export class GenericFormConstants {
    public static dateMonth = { 'January': '01', 'February': '02', 'March': '03', 'April': '04', 'May': '05', 'June': '06', 'July': '07', 'August': '08', 'September': '09', 'October': '10', 'November': '11', 'December': '12' };
}
