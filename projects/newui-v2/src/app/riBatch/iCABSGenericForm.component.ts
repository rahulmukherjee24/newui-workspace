import { Component, Input, ViewChild, AfterContentInit, Injector } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LocalStorageService } from 'ngx-webstorage';
import { HttpRequestOptions, HttpServiceUrl } from '@app/base/httpRequestOptions';
import { HTTP_METHOD, RestService } from '@shared/services/rest-service';
import { CBBService } from '@shared/services/cbb.service';
import { GlobalNotificationsService } from '@shared/services/global.notifications.service';
import { StaticUtils } from '@shared/services/static.utility';
import { Utils } from '@shared/services/utility';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ServiceConstants } from '@shared/constants/service.constants';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { IControl, IForm, IList, IServiceType } from '@app/base/GenericFormType';
import { EControlTypes, EDefaultTypes , EServiceExtraParamType, GenericFormConstants } from './GenericForm.constants';

@Component({
    selector: 'icabs-generic-form',
    templateUrl: 'iCABSGenericForm.html',
    styles: [
        `
        .control-container {
            display: flex;
            flex-wrap: wrap;
        }
        :host .hidden {
            display: none;
        }
        :host /deep/ .message-box.info-label {
            background-color: #cfe2ff;
            border: 1px solid #b6d4fe;
            color: #084298;
            padding: 0px;
            font-size: 15px;
            line-height: 25px;
            text-align: inherit;
            font-weight: inherit;
        }
        `
    ]
})

export class ICABSGenericFormComponent implements AfterContentInit {
    @ViewChild('uiForm') uiForm: NgForm;
    @Input() response: IForm;

    reportTitle: string = '';
    serviceDetails: IServiceType;

    controls: IControl[];
    name: string = '';
    datepicker = {};
    buttonList: IControl[];
    isRequesting: boolean = false;
    private requestOptions: HttpRequestOptions;


    constructor(
        private utils: Utils,
        private cbbService: CBBService,
        private ls: LocalStorageService,
        private serviceConstants: ServiceConstants,
        public injector: Injector,
        private restService: RestService,
        private notification: GlobalNotificationsService
        ) {
    }

    ngAfterContentInit(): void {
        this.reportTitle = this.response.title;
        this.serviceDetails = this.response.service;
        this.controls = StaticUtils.deepCopy(this.response.controls);
        this.buttonList = this.response.controls.filter( control => EControlTypes[control.type.toUpperCase()] === EControlTypes.BUTTON );
        this.renderForm();
    }

    private renderForm(): void {
        this.isRequesting = true;
        this.controls.forEach( control => {
            this.setDefaultValue(control);
        });
        this.isRequesting = false;
    }

    private setDefaultValue(dfObj: IControl): void {
        switch (dfObj.type.toLowerCase()) {
            case EControlTypes.DATEPICKER:
                this.setDatePicker(dfObj);
            break;
            case EControlTypes.DROPDOWN:
                this.setDropdown(dfObj);
            break;
            default:
                this.setFixedString(dfObj);
        }
    }

    private setFixedString(dfObj: IControl): void {
        const date = new Date();
        switch (EDefaultTypes[dfObj.default.toUpperCase()]) {
            case EDefaultTypes.USEREMAIL:
                dfObj.value = this.utils.getEmail();
                break;
            case EDefaultTypes.USERNAME:
                dfObj.value = this.utils.getUserName();
                break;
            case EDefaultTypes.CURRENTYEAR:
                dfObj.value = date.getFullYear() > 9 ? date.getFullYear().toString() : ('0' + date.getFullYear());
                break;
            case EDefaultTypes.GETMONTH:
                dfObj.value = dfObj.value || (date.getMonth() + 1) > 9 ? (date.getMonth() + 1).toString() : ('0' + (date.getMonth() + 1));
                break;
            case EDefaultTypes.BUSINESSCODE:
                dfObj.value = this.utils.getBusinessCode();
                break;
            case EDefaultTypes.BUSINESSDESC:
                dfObj.value = this.utils.getBusinessText();
                break;
            default:
                dfObj.value = dfObj.hasOwnProperty('value') ? dfObj.value : '';
        }
    }

    private setDropdown(dfObj: IControl): void {
        const date = new Date();
        switch (EDefaultTypes[dfObj.default.toUpperCase()]) {
            case EDefaultTypes.CURRENTMONTH:
                Object.assign(dfObj, { ['list'] : this.formatter(GenericFormConstants.dateMonth)});
                dfObj.value = dfObj.value || (date.getMonth() + 1) > 9 ? (date.getMonth() + 1).toString() : ('0' + (date.getMonth() + 1));
            break;
            case EDefaultTypes.GETBRANCHLIST:
                const branchList = this.cbbService.getBranchListByCountryAndBusiness(this.utils.getCountryCode(), this.utils.getBusinessCode());
                if (dfObj.list) {
                    dfObj.list.value = [...dfObj.list.value,...this.formatter(branchList, 'text' , 'value').value];
                } else {
                    Object.assign(dfObj, { ['list'] : this.formatter(branchList, 'text' , 'value') });
                }
                dfObj.value = dfObj.hasOwnProperty('value') ? dfObj.value : this.utils.getBranchCode();
            break;
        }
    }

    private setDatePicker(dfObj: IControl): void {
        const date = new Date();
        if (typeof dfObj.default === 'string') {
            switch (EDefaultTypes[dfObj.default.toUpperCase()]) {
                case EDefaultTypes.CURRENTDATE:
                    dfObj.value = date;
                    break;
                case EDefaultTypes.FIRSTDATEOFMONTH:
                    dfObj.value = new Date(date.getFullYear(), date.getMonth(), 1);
                    break;
                case EDefaultTypes.LASTDATEOFMONTH:
                    dfObj.value = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    break;
            }
        } else {
            dfObj.value = dfObj.default['delta'] > 0 ? this.utils.addDays(new Date(), dfObj.default['delta']) : this.utils.removeDays(new Date(), Math.abs(dfObj.default['delta']));
        }
    }

    private formatter(list: Array<any> | Record<string,string>, key?: string, value?: string): IList {
        const dropdownList: IList = { value: [] };
        if (Array.isArray(list) && key && value) {
            dropdownList.value = list.map( listItem => {
                return { 'text': listItem[key], 'value': listItem[value]};
            });
        } else {
            dropdownList.value = Object.keys(list).map( listItem => {
                return  { 'text': listItem, 'value' : list[listItem] };
            });
        }
        return dropdownList;
    }

    public updateDatePickerValue(e: { value: string, trigger: boolean }, control: string, required: boolean): void {
        Object.assign(this.datepicker, { [control]: { value: e.value, required } });
    }

    public formSubmit(): void {
        let form = {};
        let datePickerValid = true;
        if (Object.keys(this.datepicker).length !== 0) {
            Object.keys(this.datepicker).forEach(control => {
                datePickerValid = !(this.datepicker[control].required && this.datepicker[control].value === '') && datePickerValid;
                Object.assign(form, { [control]: this.datepicker[control].value });
            });
        }
        if (!this.uiForm.form.invalid && datePickerValid) {
            form = {
                ...form,
                ...this.uiForm.form.getRawValue()
            };
            this.processForm(form);
        }
    }

    private processForm(formData: Record<string,string>): void {
        const queryParams = new QueryParams();
        queryParams.set(this.serviceConstants.BusinessCode, formData[this.serviceConstants.BusinessCode] || this.utils.getBusinessCode());
        queryParams.set(this.serviceConstants.CountryCode, formData[this.serviceConstants.CountryCode] || this.utils.getCountryCode());
        queryParams.set(this.serviceConstants.Action, this.serviceDetails.action || 6);
        queryParams.set(this.serviceConstants.email, this.utils.getEmail());

        this.requestOptions = HttpRequestOptions
            .create(this.injector, HttpServiceUrl.ICABS_PROXY)
            .addPathParam('create/batch')
            .addQueryParams(queryParams)
            .addBodyParams(formData)
            .addMethodType(HTTP_METHOD.METHOD_TYPE_POST)
            .addPageHeader(this.serviceConstants.Module, this.serviceDetails.module)
            .addPageHeader(this.serviceConstants.Operation, this.serviceDetails.operation);

        if (this.serviceDetails.hasOwnProperty('extraParameters')) {
                this.serviceDetails.extraParameters.forEach(extraControl => {
                    this.requestOptions.addBodyParam(extraControl , formData[extraControl] || this.getExtraControl(extraControl));
                });
        }

        this.executeBatch();
    }

    private executeBatch(): void {
        this.isRequesting = true;
        this.restService.executeRequest(this.requestOptions).then(data => {
            this.isRequesting = false;
            if (data.hasOwnProperty('BatchProcessInformation')) {
                this.notification.displayMessage(data['BatchProcessInformation'],CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
            } else if (data.hasOwnProperty('errorMessage') || data.hasOwnProperty('fullError')) {
                this.notification.displayMessage(data);
            }
        }).catch((error: any) => {
            this.isRequesting = false;
            this.notification.displayMessage({ errorMessage: 'Check logs'});
        });
    }

    private getExtraControl(controlName: string): string {
        switch (controlName.toUpperCase()) {
            case EServiceExtraParamType.BRANCHNUMBER:
                return this.utils.getBranchCode();
            case EServiceExtraParamType.LANGUAGECODE:
                return this.ls.retrieve('LanguageCode') || 'ENG';
        }
    }

    public onButtonClick(val: string): void {
        if (val.toLowerCase().includes('reset')) {
            this.controls = StaticUtils.deepCopy(this.response.controls);
            this.renderForm();
        }
    }

    public get controlType(): typeof EControlTypes {
        return EControlTypes;
    }

}
