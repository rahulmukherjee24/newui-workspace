import { RouteAwayGuardService } from './../../shared/services/route-away-guard.service';
import { UserAuthorityBranchGridComponent } from './grid-search/iCABSBUserAuthorityBranchGrid.component';
import { ServicePlanningEmployeeTimeGridComponent } from './grid-search/iCABSSeServicePlanningEmployeeTimeGridHg.component';
import { ServiceAreaSequenceGridComponent } from './grid-search/iCABSSeServiceAreaSequenceGrid.component';
import { PremiseActivityGridComponent } from './grid-search/iCABSSePremiseActivityGrid.component';
import { SalesStatisticsSericeValueDetailAdjustGridComponent } from './grid-search/iCABSSSalesStatisticsServiceValueDetailAdjustGrid.component';
import { ServicePlanSummaryDetailGridComponent } from './grid-search/iCABSSeServicePlanSummaryDetailGrid.component';
import { ServicePlanGridComponent } from './grid-search/iCABSSeServicePlanGrid.component';
import { ServicePatternGridComponent } from './grid-search/iCABSSeServicePatternGrid.component';
import { ContractApprovalGridComponent } from './grid-search/iCABSSdlContractApprovalGrid.component';
import { SContactTypeDetailPropertiesGridComponent } from './grid-search/iCABSSContactTypeDetailPropertiesGrid.component';
import { PremiseAccessTimesGridComponent } from './grid-search/iCABSPremiseAccessTimesGrid';
import { ContactTypeDetailEscalateGridComponent } from './grid-search/iCABSSContactTypeDetailEscalateGrid.component';
import { SalesAreaPostCodeComponent } from './grid-search/iCABSBSalesAreaPostCodeGrid.component';
import { PlanVisitGridDayComponent } from './grid-search/iCABSAPlanVisitGridDay';
import { StaticVisitGridDayComponent } from './grid-search/iCABSAStaticVisitGridDay.component';
import { LinkedProductsGridComponent } from './grid-search/iCABSALinkedProductsGrid.component';
import { PipelineQuoteGridComponent } from './grid-search/iCABSSPipelineQuoteGrid.component';
import { QuoteGridComponent } from './grid-search/iCABSSSOQuoteGrid';
import { InternalGridSearchSalesModuleRoutesConstant } from '../base/PageRoutes';
import { InternalSearchModule } from './search.module';
import { InternalSearchEllipsisModule } from './search-ellipsis.module';
import { SearchEllipsisDropdownModule } from './search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from './search-ellipsis-business.module';
import { SharedModule } from './../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Component, ViewChild, ViewContainerRef, NgModule } from '@angular/core';
import { ProRataChargeBranchGridComponent } from './grid-search/iCABSAProRataChargeBranchGrid.component';
import { ApprovalBusinessComponent } from './grid-search/approval-business/iCABSSApprovalBusiness.component';

@Component({
    template: `<router-outlet></router-outlet>
    <icabs-modal #errorModal="child" [(showHeader)]="showErrorHeader" [config]="{backdrop: 'static'}"></icabs-modal>`
})

export class InternalGridSearchSalesComponent {
    @ViewChild('errorModal') public errorModal;
    public showErrorHeader: boolean = true;
    constructor(viewContainerRef: ViewContainerRef) {
    }
}


@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        RouterModule.forChild([
            {
                path: '', component: InternalGridSearchSalesComponent, children: [
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSSOQUOTEGRID, component: QuoteGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSPIPELINEQUOTEGRID, component: PipelineQuoteGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSALINKEDPRODUCTSGRID, component: LinkedProductsGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSASTATICVISITGRIDDAY, component: StaticVisitGridDayComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSAPLANVISITGRIDDAY, component: PlanVisitGridDayComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSBSALESAREAPOSTCODEGRID, component: SalesAreaPostCodeComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSCONTACTTYPEDETAILESCALATEGRID, component: ContactTypeDetailEscalateGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSPREMISEACCESSTIMESGRID, component: PremiseAccessTimesGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSCONTACTTYPEDETAILPROPERTIESGRID, component: SContactTypeDetailPropertiesGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSAPRORATACHARGEBRANCHGRID, component: ProRataChargeBranchGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSDLCONTRACTAPPROVALGRID, component: ContractApprovalGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEPATTERNGRID, component: ServicePatternGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEPLANGRID, component: ServicePlanGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEPLANSUMMARYDETAILGRID, component: ServicePlanSummaryDetailGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSSALESSTATISTICSSERVICEVALUEDETAILADJUSTGRID, component: SalesStatisticsSericeValueDetailAdjustGridComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSEPREMISEACTIVITYGRID, component: PremiseActivityGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEAREASEQUENCEGRID, component: ServiceAreaSequenceGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEPLANNINGEMPLOYEETIMEGRIDHG, component: ServicePlanningEmployeeTimeGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSBUSERAUTHORITYBRANCHGRID, component: UserAuthorityBranchGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSAPPROVALBUSINESS, component: ApprovalBusinessComponent }
                ]
            }
        ])
    ],

    declarations: [
        InternalGridSearchSalesComponent,
        QuoteGridComponent,
        PipelineQuoteGridComponent,
        LinkedProductsGridComponent,
        StaticVisitGridDayComponent,
        PlanVisitGridDayComponent,
        SalesAreaPostCodeComponent,
        ContactTypeDetailEscalateGridComponent,
        PremiseAccessTimesGridComponent,
        SContactTypeDetailPropertiesGridComponent,
        ProRataChargeBranchGridComponent,
        ContractApprovalGridComponent,
        ServicePatternGridComponent,
        ServicePlanGridComponent,
        ServicePlanSummaryDetailGridComponent,
        PremiseActivityGridComponent,
        ServiceAreaSequenceGridComponent,
        SalesStatisticsSericeValueDetailAdjustGridComponent,
        ServicePlanningEmployeeTimeGridComponent,
        UserAuthorityBranchGridComponent,
        ApprovalBusinessComponent
    ]
})

export class InternalGridSearchSalesModule { }
