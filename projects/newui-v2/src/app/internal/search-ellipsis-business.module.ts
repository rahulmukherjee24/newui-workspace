import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModuleRoutes, InternalGridSearchApplicationModuleRoutesConstant } from './../base/PageRoutes';

import { ClosedTemplateSearchComponent } from './search/iCABSBClosedTemplateSearch.component';
import { BusinessOriginDetailLanguageSearchComponent } from './search/iCABSBBusinessOriginDetailLanguageSearch';
import { ContractDurationComponent } from './search/iCABSBContractDurationSearch';
import { ProductDetailSearchComponent } from './search/iCABSBProductDetailSearch.component';
import { CampaignSearchComponent } from './search/iCABSBCampaignSearch/iCABSBCampaignSearch';
import { SalesAreaSearchComponent } from './search/iCABSBSalesAreaSearch.component';
import { InvoiceFeeSearchComponent } from './search/iCABSBInvoiceFeeSearch';
import { ICABSBAPICodeSearchComponent } from './search/iCABSBAPICodeSearchComponent';
import { ApiRateSearchComponent } from './search/iCABSBApiRateSearch';
import { ProductExpenseSearchComponent } from './search/iCABSBProductExpenseSearch';
import { BusinessTierSearchComponent } from './search/iCABSBTierSearchComponent';
import { RegulatoryAuthoritySearchComponent } from './search/iCABSBRegulatoryAuthoritySearch.component';
import { ProductCoverSearchComponent } from './search/iCABSBProductCoverSearch.component';
import { CalendarTemplateSearchComponent } from './search/iCABSBCalendarTemplateSearch.component';
import { InvoiceRunDateSelectPrintComponent } from './search/iCABSBInvoiceRunDateSelectPrint.component';
import { InvoiceRunDateSelectPrint2Component } from './search/iCABSBInvoiceRunDateSelectPrint2.component';
import { ProductSearchGridComponent } from './search/iCABSBProductSearch';
import { LostBusinessDetailSearchComponent } from './search/iCABSBLostBusinessDetailSearch.component';
import { ListGroupSearchComponent } from './grid-search/iCABSBListGroupSearch.component';
import { ProductAttributeValuesSearchComponent } from './search/iCABSBProductAttributeValuesSearch.component';
import { InfestationLevelSearchComponent } from './search/iCABSBInfestationLevelSearch.component';
import { SeasonalTemplateSearchComponent } from './search/iCABSBSeasonalTemplateSearch';
import { PrepChargeRateSearchComponent } from './search/iCABSBPrepChargeRateSearch.component';
import { BranchPcodeServiceGrpEntrySearchComponent } from './search/iCABSBBranchPcodeServiceGrpEntrySearch.component';
import { PreparationSearchComponent } from './search/iCABSBPreparationSearch.component';
import { VisitActionSearchComponent, VisitActionSearchDropDownComponent } from './search/iCABSBVisitActionSearch.component';
import { BranchProdServiceGrpEntrySearchComponent } from './search/iCABSBBranchProdServiceGrpEntrySearch.component';
import { ExpenseCodeSearchComponent } from './search/iCABSBExpenseCodeSearch.component';

@NgModule({
    exports: [
        ClosedTemplateSearchComponent,
        BusinessOriginDetailLanguageSearchComponent,
        ContractDurationComponent,
        ProductDetailSearchComponent,
        CampaignSearchComponent,
        SalesAreaSearchComponent,
        InvoiceFeeSearchComponent,
        ICABSBAPICodeSearchComponent,
        ApiRateSearchComponent,
        ProductExpenseSearchComponent,
        BusinessTierSearchComponent,
        RegulatoryAuthoritySearchComponent,
        ProductCoverSearchComponent,
        CalendarTemplateSearchComponent,
        InvoiceRunDateSelectPrintComponent,
        InvoiceRunDateSelectPrint2Component,
        ProductSearchGridComponent,
        LostBusinessDetailSearchComponent,
        ListGroupSearchComponent,
        ProductAttributeValuesSearchComponent,
        SeasonalTemplateSearchComponent,
        PrepChargeRateSearchComponent,
        BranchPcodeServiceGrpEntrySearchComponent,
        PreparationSearchComponent,
        VisitActionSearchComponent,
        VisitActionSearchDropDownComponent,
        BranchProdServiceGrpEntrySearchComponent,
        ExpenseCodeSearchComponent,
        InfestationLevelSearchComponent
    ],
    declarations: [
        ClosedTemplateSearchComponent,
        BusinessOriginDetailLanguageSearchComponent,
        ContractDurationComponent,
        ProductDetailSearchComponent,
        CampaignSearchComponent,
        SalesAreaSearchComponent,
        InvoiceFeeSearchComponent,
        ICABSBAPICodeSearchComponent,
        ApiRateSearchComponent,
        ProductExpenseSearchComponent,
        BusinessTierSearchComponent,
        RegulatoryAuthoritySearchComponent,
        ProductCoverSearchComponent,
        CalendarTemplateSearchComponent,
        InvoiceRunDateSelectPrintComponent,
        InvoiceRunDateSelectPrint2Component,
        ProductSearchGridComponent,
        LostBusinessDetailSearchComponent,
        ListGroupSearchComponent,
        ProductAttributeValuesSearchComponent,
        InfestationLevelSearchComponent,
        SeasonalTemplateSearchComponent,
        PrepChargeRateSearchComponent,
        BranchPcodeServiceGrpEntrySearchComponent,
        PreparationSearchComponent,
        VisitActionSearchComponent,
        VisitActionSearchDropDownComponent,
        BranchProdServiceGrpEntrySearchComponent,
        ExpenseCodeSearchComponent
    ],
    imports: [
        SharedModule,
        RouterModule.forChild([
            { path: 'application/campaignsearchcomponent', component: CampaignSearchComponent },
            { path: 'application/invoicefeesearch', component: InvoiceFeeSearchComponent },
            { path: 'application/apiRateSearch', component: ApiRateSearchComponent },
            { path: 'application/productExpenseSearch', component: ProductExpenseSearchComponent },
            { path: 'application/CalendarTemplateSearch', component: CalendarTemplateSearchComponent },
            { path: 'application/invoiceRunDate/print', component: InvoiceRunDateSelectPrintComponent },
            { path: InternalSearchModuleRoutes.ICABSBINVOICERUNDATESELECTPRINT2, component: InvoiceRunDateSelectPrint2Component },
            { path: InternalSearchModuleRoutes.ICABSBCLOSEDTEMPLATESEARCH, component: ClosedTemplateSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBPRODUCTSEARCH, component: ProductSearchGridComponent },
            { path: InternalSearchModuleRoutes.ICABSBLOSTBUSINESSDETAILSEARCH, component: LostBusinessDetailSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBINFESTATIONLEVELSEARCH, component: InfestationLevelSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBPRODUCTDETAILSEARCH, component: ProductDetailSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBPRODUCTCOVERSEARCH, component: ProductCoverSearchComponent },
            { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSBLISTGROUPSEARCH, component: ListGroupSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBSEASONALTEMPLATESEARCH, component: SeasonalTemplateSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBBRANCHPCODESERVICEGRPENTRYSEARCH, component: BranchPcodeServiceGrpEntrySearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBVISITACTIONSEARCH, component: VisitActionSearchComponent }
        ])
    ],
    entryComponents: [
        ClosedTemplateSearchComponent,
        BusinessOriginDetailLanguageSearchComponent,
        ContractDurationComponent,
        ProductDetailSearchComponent,
        CampaignSearchComponent,
        SalesAreaSearchComponent,
        InvoiceFeeSearchComponent,
        ICABSBAPICodeSearchComponent,
        ApiRateSearchComponent,
        ProductExpenseSearchComponent,
        BusinessTierSearchComponent,
        RegulatoryAuthoritySearchComponent,
        ProductCoverSearchComponent,
        CalendarTemplateSearchComponent,
        InvoiceRunDateSelectPrintComponent,
        InvoiceRunDateSelectPrint2Component,
        ProductSearchGridComponent,
        LostBusinessDetailSearchComponent,
        ListGroupSearchComponent,
        ProductAttributeValuesSearchComponent,
        SeasonalTemplateSearchComponent,
        PrepChargeRateSearchComponent,
        BranchPcodeServiceGrpEntrySearchComponent,
        PreparationSearchComponent,
        VisitActionSearchComponent,
        VisitActionSearchDropDownComponent,
        BranchProdServiceGrpEntrySearchComponent,
        ExpenseCodeSearchComponent,
        InfestationLevelSearchComponent
    ]
})

export class SearchEllipsisBusinessModule { }

