import { Component, ViewChild, ViewContainerRef, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { InternalSearchModule } from './search.module';
import { SharedModule } from './../../shared/shared.module';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { InternalGridSearchSalesModuleRoutesConstant } from '../base/PageRoutes';

import { ProductSalesSCDetailMntComponent } from './grid-search/iCABSAProductSalesSCDetailMaintenance/iCABSAProductSalesSCDetailMaintenance.component';
import { PlanVisitGridYearComponent } from './grid-search/iCABSAPlanVisitGridYear';
import { ProRateChargeSearchComponent } from './grid-search/iCABSAProRataChargeSummary.component';
import { StaticVisitGridYearComponent } from './grid-search/iCABSAStaticVisitGridYear/iCABSAStaticVisitGridYear.component';
import { ContractHistoryGridComponent } from './grid-search/iCABSAContractHistoryGrid';
import { EmptyPremiseLocationSearchGridComponent } from './grid-search/iCABSAEmptyPremiseLocationSearchGrid';
import { ContractInvoiceTurnoverGridComponent } from './grid-search/iCABSAContractInvoiceTurnoverGrid.component';
import { ServiceCoverComponent } from './grid-search/iCABSSSOServiceCoverGrid.component';
import { PremiseServiceSummaryComponent } from './grid-search/iCABSAPremiseServiceSummary.component';
import { SCMProspectConvGridComponent } from './grid-search/iCABSCMProspectConvGrid.component';
import { PipelineServiceCoverGridComponent } from './grid-search/iCABSSPipelineServiceCoverGrid.component';
import { PipelinePremiseGridComponent } from './grid-search/iCABSSPipelinePremiseGrid.component';
import { SOPremiseGridComponent } from './grid-search/iCABSSSOPremiseGrid';
import { DLHistoryGridComponent } from './grid-search/iCABSSdlHistoryGrid';
import { RelatedVisitGridComponent } from './grid-search/iCABSARelatedVisitGrid.component';
import { InvoiceRangeUpdateGridComponent } from './grid-search/iCABSBInvoiceRangeUpdateGrid';
import { SalesStatisticsServiceValueGridComponent } from './grid-search/iCABSSSalesStatisticsServiceValueGrid.component';
import { ServicePlanningSummaryGridComponent } from './grid-search/iCABSSeServicePlanningSummaryGrid.component';

import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';


@Component({
    template: `<router-outlet></router-outlet>
    <icabs-modal #errorModal="child" [(showHeader)]="showErrorHeader" [config]="{backdrop: 'static'}"></icabs-modal>`
})

export class InternalGridSalesComponent {
    @ViewChild('errorModal') public errorModal;
    public showErrorHeader: boolean = true;
    constructor(viewContainerRef: ViewContainerRef) {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        RouterModule.forChild([
            {
                path: '', component: InternalGridSalesComponent, children: [
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSAPRODUCTSALESSCDETAILMAINTENANCE, component: ProductSalesSCDetailMntComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSAPLANVISITGRIDYEAR, component: PlanVisitGridYearComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSAPRORATACHARGESUMMARY, component: ProRateChargeSearchComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSASTATICVISITGRIDYEAR, component: StaticVisitGridYearComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSACONTRACTHISTORYGRID, component: ContractHistoryGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSAEMPTYPREMISELOCATIONSEARCHGRID, component: EmptyPremiseLocationSearchGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSACONTRACTINVOICETURNOVERGRID, component: ContractInvoiceTurnoverGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSSOSERVICECOVERGRID, component: ServiceCoverComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSAPREMISESERVICESUMMARY, component: PremiseServiceSummaryComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSCMPROSPECTCONVGRID, component: SCMProspectConvGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSPIPELINESERVICECOVERGRID, component: PipelineServiceCoverGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSPIPELINEPREMISEGRID, component: PipelinePremiseGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSSOPREMISEGRID, component: SOPremiseGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSDLHISTORYGRID, component: DLHistoryGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSARELATEDVISITGRID, component: RelatedVisitGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSBINVOICERANGEUPDATEGRID, component: InvoiceRangeUpdateGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSSALESSTATISTICSSERVICEVALUEGRID.CONTRACT, component: SalesStatisticsServiceValueGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSSALESSTATISTICSSERVICEVALUEGRID.JOB, component: SalesStatisticsServiceValueGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSSALESSTATISTICSSERVICEVALUEGRID.PRODUCT, component: SalesStatisticsServiceValueGridComponent },
                    { path: InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEPLANNINGSUMMARYGRID, component: ServicePlanningSummaryGridComponent }


                ]
            }
        ])
    ],
    declarations: [
        InternalGridSalesComponent,
        ProductSalesSCDetailMntComponent,
        PlanVisitGridYearComponent,
        ProRateChargeSearchComponent,
        StaticVisitGridYearComponent,
        ContractHistoryGridComponent,
        EmptyPremiseLocationSearchGridComponent,
        ContractInvoiceTurnoverGridComponent,
        ServiceCoverComponent,
        PremiseServiceSummaryComponent,
        SCMProspectConvGridComponent,
        PipelineServiceCoverGridComponent,
        PipelinePremiseGridComponent,
        SOPremiseGridComponent,
        DLHistoryGridComponent,
        RelatedVisitGridComponent,
        InvoiceRangeUpdateGridComponent,
        SalesStatisticsServiceValueGridComponent,
        ServicePlanningSummaryGridComponent
    ]
})

export class InternalGridSalesModule { }
