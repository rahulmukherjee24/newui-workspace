import { Component, ViewChild, ViewContainerRef, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { InternalGridSearchServiceModuleRoutesConstant } from '../base/PageRoutes';
import { InternalSearchModule } from './search.module';
import { SharedModule } from './../../shared/shared.module';

import { ServiceVisitEntryGridComponent } from './grid-search/iCABSSeServiceVisitEntryGrid';
import { ServiceWorkListGridComponent } from './grid-search/iCABSSeServiceWorkListGrid.component';
import { ServiceActivityUpdateGridComponent } from './grid-search/iCABSSeServiceActivityUpdateGrid.component';
import { SummaryWorkloadRezoneMoveGridComponent } from './grid-search/iCABSSeSummaryWorkloadRezoneMoveGrid.component';
import { DebriefActionsGridComponent } from './grid-search/iCABSSeDebriefActionsGrid.component';
import { SpecialInstructionsGridComponent } from './grid-search/iCABSSeHCASpecialInstructionsGrid.component';
import { ServiceAreaRezoneRejectionsGridComponent } from './grid-search/iCABSSeServiceAreaRezoneRejectionsGrid.component';
import { ServiceVisitEntitlementEntryGridComponent } from './grid-search/iCABSSeServiceVisitEntitlementEntryGrid.component';
import { CustomerSignatureSummaryComponent } from './grid-search/iCABSSeCustomerSignatureSummary.component';
import { CustomerQuarterlyReturnsPrintComponent } from './grid-search/iCABSARCustomerQuarterlyReturnsPrint.component';
import { ServiceVisitReleaseDetailGridComponent } from './grid-search/iCABSSeServiceVisitReleaseDetailGrid.component';
import { ServiceWorkListDateGridComponent } from './grid-search/iCABSSeServiceWorkListDateGrid.component';

import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';

@Component({
    template: `<router-outlet></router-outlet>
    <icabs-modal #errorModal="child" [(showHeader)]="showErrorHeader" [config]="{backdrop: 'static'}"></icabs-modal>`
})

export class InternalGridServiceMenuComponent {
    @ViewChild('errorModal') public errorModal;
    public showErrorHeader: boolean = true;
    constructor(viewContainerRef: ViewContainerRef) {
    }
}


@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        RouterModule.forChild([
            {
                path: '', component: InternalGridServiceMenuComponent, children: [
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEVISITENTRYGRID, component: ServiceVisitEntryGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEWORKLISTGRID, component: ServiceWorkListGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEACTIVITYUPDATEGRID, component: ServiceActivityUpdateGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEWORKLISTDATEGRID, component: ServiceWorkListDateGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESUMMARYWORKLOADREZONEMOVEGRID.URL_1, component: SummaryWorkloadRezoneMoveGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESUMMARYWORKLOADREZONEMOVEGRID.URL_2, component: SummaryWorkloadRezoneMoveGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESUMMARYWORKLOADREZONEMOVEGRID.URL_3, component: SummaryWorkloadRezoneMoveGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSEDEBRIEFACTIONSGRID, component: DebriefActionsGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSEHCASPECIALINSTRUCTIONSGRID, component: SpecialInstructionsGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEAREAREZONEREJECTIONSGRID, component: ServiceAreaRezoneRejectionsGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEVISITENTITLEMENTENTRYGRID, component: ServiceVisitEntitlementEntryGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSECUSTOMERSIGNATURESUMMARY, component: CustomerSignatureSummaryComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSECUSTOMERSIGNATURESUMMARYBRANCH, component: CustomerSignatureSummaryComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSECUSTOMERSIGNATURESUMMARYREGION, component: CustomerSignatureSummaryComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSARCUSTOMERQUARTERLYRETURNSPRINT, component: CustomerQuarterlyReturnsPrintComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEVISITRELEASEDETAILGRID, component: ServiceVisitReleaseDetailGridComponent }
                ]
            }
        ])
    ],
    declarations: [
        InternalGridServiceMenuComponent,
        ServiceVisitEntryGridComponent,
        ServiceWorkListGridComponent,
        ServiceActivityUpdateGridComponent,
        ServiceWorkListDateGridComponent,
        SummaryWorkloadRezoneMoveGridComponent,
        DebriefActionsGridComponent,
        SpecialInstructionsGridComponent,
        ServiceAreaRezoneRejectionsGridComponent,
        ServiceVisitEntitlementEntryGridComponent,
        CustomerSignatureSummaryComponent,
        CustomerQuarterlyReturnsPrintComponent,
        ServiceVisitReleaseDetailGridComponent
    ]
})

export class InternalGridServiceMenuModule { }
