import { DespatchGridComponent } from './grid-search/iCABSSeDespatchGrid.component';
import { PlanVisitGridComponent } from './grid-search/iCABSSePlanVisitGrid.component';
import { ICABSAServiceVisitDisplayGridComponent } from './grid-search/iCABSAServiceVisitDisplayGrid.component';
import { NotificationGroupGridComponent } from './grid-search/iCABSSNotificationGroupGrid.component';
import { ServiceCoverUpdateableGridComponent } from './grid-search/iCABSAServiceCoverUpdateableGrid';
import { ContactTypeDetailAssigneeGridComponent } from './grid-search/iCABSSContactTypeDetailAssigneeGrid.component';
import { CallCentreGridCallLogDetailViewComponent } from './grid-search/iCABSCMCallCentreGridCallLogDetailView.component';
import { ClosedTemplateDateGridComponent } from './grid-search/iCABSAClosedTemplateDateGrid.component';
import { InvoiceGroupAddressAmendmentGridComponent } from './grid-search/iCABSAInvoiceGroupAddressAmendmentGrid.component';
import { LinkedPremiseSummaryGridComponent } from './grid-search/iCABSALinkedPremiseSummaryGrid.component';
import { CallAnalysisTicketGridComponent } from './grid-search/iCABSCMCallAnalysisTicketGrid.component';
import { CreditApprovalGridComponent } from './grid-search/iCABSACreditApprovalGrid.component';
import { CallCentreGridNotepadComponent } from './grid-search/iCABSCMCallCentreGridNotepad';
import { RouteAwayGuardService } from './../../shared/services/route-away-guard.service';
import { ServiceCoverDisplayGridComponent } from './grid-search/iCABSAServiceCoverDisplayGrid.component';
import { InvoiceHeaderGridComponent } from './grid-search/iCABSAInvoiceHeaderGrid';
import { InternalGridSearchApplicationModuleRoutesConstant } from './../base/PageRoutes';
import { ApplyAPIContractGridComponent } from './grid-search/iCABSAApplyAPIContractGrid.component';
import { SearchEllipsisBusinessModule } from './search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from './search-ellipsis-dropdown.module';
import { InternalSearchEllipsisModule } from './search-ellipsis.module';
import { InternalSearchModule } from './search.module';
import { SharedModule } from './../../shared/shared.module';

import { HttpClientModule } from '@angular/common/http';
import { Component, ViewChild, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
    template: `<router-outlet></router-outlet>
    <icabs-modal #errorModal="child" [(showHeader)]="showErrorHeader" [config]="{backdrop: 'static'}"></icabs-modal>`
})

export class InternalGridSearchApplicationComponent {
    @ViewChild('errorModal') public errorModal;
    public showErrorHeader: boolean = true;
    constructor() {
    }
}


@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        RouterModule.forChild([
            {
                path: '', component: InternalGridSearchApplicationComponent, children: [
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSAAPPLYAPICONTRACTGRID, component: ApplyAPIContractGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSAINVOICEHEADERGRID, component: InvoiceHeaderGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSSASERVICECOVERDISPLAYGRID, component: ServiceCoverDisplayGridComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCALLCENTREGRIDNOTEPAD, component: CallCentreGridNotepadComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSACREDITAPPROVALGRID, component: CreditApprovalGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCALLANALYSISTICKETGRID, component: CallAnalysisTicketGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSALINKEDPREMISESUMMARYGRID, component: LinkedPremiseSummaryGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSAINVOICEGROUPADDRESSAMENDMENTGRID, component: InvoiceGroupAddressAmendmentGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSCLOSEDTEMPLATEDATEGRID, component: ClosedTemplateDateGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCALLCENTREGRIDCALLLOGDETAILVIEW, component: CallCentreGridCallLogDetailViewComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSSEPLANVISITGRID, component: PlanVisitGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSSCONTACTTYPEDETAILASSIGNEEGRID, component: ContactTypeDetailAssigneeGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSSEDESPATCHGRID, component: DespatchGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICECOVERUPDATEABLEGRID, component: ServiceCoverUpdateableGridComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICECOVERUPDATEABLEGRID, component: ServiceCoverUpdateableGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSSNOTIFICATIONGROUPGRID, component: NotificationGroupGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICEVISITDISPLAYGRID, component: ICABSAServiceVisitDisplayGridComponent }
                ]
            }
        ])
    ],

    declarations: [
        InternalGridSearchApplicationComponent,
        ApplyAPIContractGridComponent,
        InvoiceHeaderGridComponent,
        ServiceCoverDisplayGridComponent,
        CallCentreGridNotepadComponent,
        CreditApprovalGridComponent,
        CallAnalysisTicketGridComponent,
        LinkedPremiseSummaryGridComponent,
        InvoiceGroupAddressAmendmentGridComponent,
        ClosedTemplateDateGridComponent,
        CallCentreGridCallLogDetailViewComponent,
        PlanVisitGridComponent,
        ContactTypeDetailAssigneeGridComponent,
        DespatchGridComponent,
        ServiceCoverUpdateableGridComponent,
        NotificationGroupGridComponent,
        ICABSAServiceVisitDisplayGridComponent
    ]
})

/*export class InternalGridSearchModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: InternalGridSearchModule,
            providers: [
                RouteAwayGuardService, RouteAwayGlobals
            ]
        };
    }
}*/

export class InternalGridSearchApplicationModule { }
