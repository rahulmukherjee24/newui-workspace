import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { HistoryTypeLanguageSearchComponent } from './search/iCABSSHistoryTypeLanguageSearch.component';
import { InvoiceChargeSearchComponent } from './search/iCABSAInvoiceChargeSearch.component';
import { ServicePlanningInfoComponent } from './search/iCABSSeServicePlanningInfo.component';
import { AccountSearchComponent } from './search/iCABSASAccountSearch';
import { AccountGroupSearchComponent } from './search/iCABSAAccountGroupSearch';
import { GroupAccountNumberComponent } from './search/iCABSSGroupAccountNumberSearch';
import { ServicePlanSearchComponent } from './search/iCABSSeServicePlanSearch.component';
import { AccountHistoryDetailComponent } from './search/iCABSAAccountHistoryDetail';
import { CustomerTypeSearchComponent } from './search/iCABSSCustomerTypeSearch';
import { ServiceCoverLocationEffectDateSearchComponent } from './search/iCABSAServiceCoverLocationEffectDateSearch.component';
import { InvoiceHeaderAddressComponent } from './search/iCABSAInvoiceHeaderAddressDetails.component';
import { AccountBusinessTypeSearchComponent } from './search/iCABSSAccountBusinessTypeSearch/iCABSSAccountBusinessTypeSearch';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { ServiceCoverSearchComponent } from './search/iCABSAServiceCoverSearch';
import { BatchProgramScheduleSearchComponent } from './search/riMBatchProgramScheduleSearch';
import { JobServiceCoverSearchComponent } from './search/iCABSCMNatAxJobServiceCoverSearch';
import { InvoiceGroupSearchComponent } from './search/iCABSAInvoiceGroupSearch.component';
import { OccupationSearchComponent } from './search/iCABSSOccupationSearch.component';
import { DebriefSearchComponent } from './search/iCABSSeDebriefSearch.component';
import { PremiseLocationSearchComponent } from './search/iCABSAPremiseLocationSearch.component';
import { ProRataChargeStatusLanguageSearchComponent } from './search/iCABSSProRataChargeStatusLanguageSearch';
import { InvoiceSearchComponent } from './search/iCABSInvoiceSearch.component';
import { InternalSearchModuleRoutes } from './../base/PageRoutes';
import { AUPostcodeSearchComponent } from './grid-search/iCABSAAUPostcodeSearch';
import { AccountPremiseSearchComponent } from './grid-search/iCABSAAccountPremiseSearchGrid';
import { AccountHistoryGridComponent } from './grid-search/iCABSAAccountHistoryGrid';
import { InvoiceGroupGridComponent } from './grid-search/iCABSAInvoiceGroupGrid';
import { ScreenNotReadyComponent } from '../../shared/components/screenNotReady';
import { LostBusinessContactSearchComponent } from './search/iCABSALostBusinessContactSearch.component';
import { ProspectSearchComponent } from './search/iCABSCMProspectSearch.component';
import { PremiseLocationAllocationGridComponent } from './search/iCABSAPremiseLocationAllocationGrid';
import { ServiceCoverDetailsComponent } from './search/iCABSAServiceCoverDetailSearch.component';
import { ServiceSummaryDetailComponent } from './search/iCABSAServiceSummaryDetail';
import { MarktSelectSearchComponent } from './search/iCABSMarktSelectSearch.component';
import { PlanVisitSearchComponent } from './search/iCABSSePlanVisitSearch.component';
import { PNOLPremiseSearchGridComponent } from './grid-search/iCABSAPNOLPremiseSearchGrid.component';
import { LostBusinessRequestSearchComponent } from './search/iCABSALostBusinessRequestSearch.component';
import { CallCentreReviewGridMultiComponent } from './search/iCABSCMCallCentreReviewGridMulti';
import { LostBusinessContactOutcomeLanguageSearchComponent } from './search/iCABSSLostBusinessContactOutcomeLanguageSearch.component';
import { ContactMediumLanguageSearchComponent } from './search/iCABSSContactMediumLanguageSearch.component';
import { SystemInvoiceCreditReasonSearchComponent } from './search/iCABSSSystemInvoiceCreditReasonSearch.component';
import { SystemInvoiceFormatLanguageSearchComponent } from './search/iCABSSSystemInvoiceFormatLanguageSearch.component';
import { SICSearchComponent } from './search/iCABSSSICSearch.component';
import { LanguageSearchComponent } from './search/riMGLanguageSearch.component';
import { InfestationSearchComponent } from './search/iCABSSeInfestationSearch.component';
import { ServiceVisitSearchComponent } from './search/iCABSSeServiceVisitSearch.component';
import { PrepUsedSearchComponent } from './search/iCABSSePrepUsedSearch.component';
import { SystemPDAActivityTypeLanguageSearchComponent } from './search/iCABSSSystemPDAActivityTypeLanguageSearch.component';
import { PDAInfestationSearchComponent } from './search/iCABSSePDAiCABSInfestationSearch.component';
import { PDAiCABSPrepUsedSearchComponent } from './search/iCABSSePDAiCABSPrepUsedSearch.component';
import { PrepUsedMaintenanceComponent } from './search/iCABSSePDAiCABSPrepUsedMaintenance.component';
import { MUserSearchComponent } from './search/riMUserSearch.component';
import { UserInformationSearchComponent } from './search/riMUserInformationSearch.component';
import { ContactTypeDetailSearchComponent } from './search/iCABSSContactTypeDetailSearch.component';
import { NgModule } from '@angular/core';

@NgModule({
    exports: [
        PlanVisitSearchComponent,
        InvoiceChargeSearchComponent,
        AccountSearchComponent,
        AccountGroupSearchComponent,
        GroupAccountNumberComponent,
        ServicePlanningInfoComponent,
        AccountHistoryDetailComponent,
        ContactMediumLanguageSearchComponent,
        ServicePlanSearchComponent,
        DebriefSearchComponent,
        PDAiCABSPrepUsedSearchComponent,
        PrepUsedMaintenanceComponent,
        InvoiceHeaderAddressComponent,
        AccountBusinessTypeSearchComponent,
        ServiceCoverSearchComponent,
        BatchProgramScheduleSearchComponent,
        JobServiceCoverSearchComponent,
        ProRataChargeStatusLanguageSearchComponent,
        InvoiceGroupSearchComponent,
        ServiceCoverLocationEffectDateSearchComponent,
        CustomerTypeSearchComponent,
        OccupationSearchComponent,
        PremiseLocationSearchComponent,
        ProRataChargeStatusLanguageSearchComponent,
        InvoiceSearchComponent,
        HistoryTypeLanguageSearchComponent,
        AUPostcodeSearchComponent,
        AccountPremiseSearchComponent,
        AccountHistoryGridComponent,
        InvoiceGroupGridComponent,
        LostBusinessContactSearchComponent,
        ScreenNotReadyComponent,
        ProspectSearchComponent,
        PremiseLocationAllocationGridComponent,
        ServiceCoverDetailsComponent,
        ServiceSummaryDetailComponent,
        MarktSelectSearchComponent,
        PNOLPremiseSearchGridComponent,
        LostBusinessRequestSearchComponent,
        ContactTypeDetailSearchComponent,
        LostBusinessContactOutcomeLanguageSearchComponent,
        SystemInvoiceCreditReasonSearchComponent,
        SystemInvoiceFormatLanguageSearchComponent,
        SICSearchComponent,
        LanguageSearchComponent,
        InfestationSearchComponent,
        ServiceVisitSearchComponent,
        PrepUsedSearchComponent,
        SystemPDAActivityTypeLanguageSearchComponent,
        PDAInfestationSearchComponent,
        MUserSearchComponent,
        UserInformationSearchComponent,
        CallCentreReviewGridMultiComponent
    ],
    imports: [
        SharedModule,
        RouterModule.forChild([
            { path: 'application/accountsearch', component: AccountSearchComponent },
            { path: 'application/accountgroupsearch', component: AccountGroupSearchComponent },
            { path: 'application/groupaccountnumbersearch', component: GroupAccountNumberComponent },
            { path: 'application/accountHistory/detail', component: AccountHistoryDetailComponent },
            { path: 'application/accountbusinesstypesearch', component: AccountBusinessTypeSearchComponent },
            { path: 'application/serviceCoverSearch', component: ServiceCoverSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSACMNATAXJOBSERVICECOVERSEARCH, component: JobServiceCoverSearchComponent },
            { path: 'application/ProRataChargeStatusLanguageSearch', component: ProRataChargeStatusLanguageSearchComponent },
            { path: 'application/invoiceGroupSearch', component: InvoiceGroupSearchComponent },
            { path: 'application/ServiceCoverLocationEffectDateSearchComponent', component: ServiceCoverLocationEffectDateSearchComponent },
            { path: 'application/occupationSearch', component: OccupationSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSEDEBRIEFSEARCH, component: DebriefSearchComponent },
            { path: 'application/ProRataChargeStatusLanguageSearch', component: ProRataChargeStatusLanguageSearchComponent },
            { path: 'application/CustomerTypeSearch', component: CustomerTypeSearchComponent },
            { path: 'application/PremiseLocationSearch', component: PremiseLocationSearchComponent },
            { path: 'application/ProRataChargeStatusLanguageSearch', component: ProRataChargeStatusLanguageSearchComponent },
            { path: 'application/InvoiceSearch', component: InvoiceSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSHISTORYTYPELANGUAGESEARCH, component: HistoryTypeLanguageSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSESERVICEPLANSEARCH, component: ServicePlanSearchComponent },
            { path: 'contractmanagement/account/invoiceCharge', component: InvoiceChargeSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSEPLANVISITSEARCH, component: PlanVisitSearchComponent },
            { path: 'application/aupostcode/search', component: AUPostcodeSearchComponent },
            { path: 'application/accountpremise', component: AccountPremiseSearchComponent },
            { path: 'application/accounthistorygrid', component: AccountHistoryGridComponent },
            { path: InternalSearchModuleRoutes.ICABSALOSTBUSINESSCONTACTSEARCH, component: LostBusinessContactSearchComponent },
            { path: 'application/invoicegroupgrid', component: InvoiceGroupGridComponent },
            { path: InternalSearchModuleRoutes.ICABSCMPROSPECTSEARCH, component: ProspectSearchComponent },
            { path: 'application/premiseLocationAllocation', component: PremiseLocationAllocationGridComponent, canDeactivate: [RouteAwayGuardService] },
            { path: 'application/ServiceCoverDetails', component: ServiceCoverDetailsComponent },
            { path: InternalSearchModuleRoutes.ICABSSESERVICEPLANNINGINFO, component: ServicePlanningInfoComponent },
            { path: InternalSearchModuleRoutes.ICABSASERVICESUMMARYDETAIL, component: ServiceSummaryDetailComponent },
            { path: InternalSearchModuleRoutes.ICABSALOSTBUSINESSREQUESTSEARCH, component: LostBusinessRequestSearchComponent },
            { path: 'application/premiseSearchGrid', component: PNOLPremiseSearchGridComponent },
            { path: InternalSearchModuleRoutes.ICABSCMCALLCENTREREVIEWGRIDMULTI, component: CallCentreReviewGridMultiComponent },
            { path: InternalSearchModuleRoutes.ICABSSLOSTBUSINESSCONTACTOUTCOMELANGUAGESEARCH, component: LostBusinessContactOutcomeLanguageSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSCONTACTMEDIUMLANGUAGESEARCH, component: ContactMediumLanguageSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSSYSTEMINVOICECREDITREASONSEARCH, component: SystemInvoiceCreditReasonSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSSYSTEMINVOICEFORMATLANGUAGESEARCH, component: SystemInvoiceFormatLanguageSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSSICSEARCH, component: SICSearchComponent },
            { path: InternalSearchModuleRoutes.RIMGLANGUAGESEARCH, component: LanguageSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSEINFESTATIONSEARCH, component: InfestationSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSESERVICEVISITSEARCH, component: ServiceVisitSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSEPREPUSEDSEARCH, component: PrepUsedSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSSYSTEMPDAACTIVITYTYPELANGUAGESEARCH, component: SystemPDAActivityTypeLanguageSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSEPDAICABSPREPUSEDSEARCH, component: PDAiCABSPrepUsedSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSEPDAICABSPREPUSED, component: PrepUsedMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: InternalSearchModuleRoutes.ICABSSEPDAICABSINFESTATIONSEARCH, component: PDAInfestationSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSSEPDAICABSINFESTATIONSEARCH, component: PDAInfestationSearchComponent },
            { path: InternalSearchModuleRoutes.RIMUSERSEARCH, component: MUserSearchComponent }
        ])
    ],
    declarations: [
        InvoiceChargeSearchComponent,
        AccountSearchComponent,
        AccountGroupSearchComponent,
        GroupAccountNumberComponent,
        ServicePlanningInfoComponent,
        PDAiCABSPrepUsedSearchComponent,
        PrepUsedMaintenanceComponent,
        AccountHistoryDetailComponent,
        ServicePlanSearchComponent,
        ContactMediumLanguageSearchComponent,
        CallCentreReviewGridMultiComponent,
        PlanVisitSearchComponent,
        DebriefSearchComponent,
        InvoiceHeaderAddressComponent,
        AccountBusinessTypeSearchComponent,
        ServiceCoverSearchComponent,
        BatchProgramScheduleSearchComponent,
        JobServiceCoverSearchComponent,
        ProRataChargeStatusLanguageSearchComponent,
        InvoiceGroupSearchComponent,
        ServiceCoverLocationEffectDateSearchComponent,
        CustomerTypeSearchComponent,
        OccupationSearchComponent,
        PremiseLocationSearchComponent,
        ProRataChargeStatusLanguageSearchComponent,
        InvoiceSearchComponent,
        HistoryTypeLanguageSearchComponent,
        AUPostcodeSearchComponent,
        AccountPremiseSearchComponent,
        AccountHistoryGridComponent,
        InvoiceGroupGridComponent,
        LostBusinessContactSearchComponent,
        ScreenNotReadyComponent,
        ProspectSearchComponent,
        PremiseLocationAllocationGridComponent,
        ServiceCoverDetailsComponent,
        ServiceSummaryDetailComponent,
        MarktSelectSearchComponent,
        PNOLPremiseSearchGridComponent,
        LostBusinessRequestSearchComponent,
        ContactTypeDetailSearchComponent,
        LostBusinessContactOutcomeLanguageSearchComponent,
        SystemInvoiceCreditReasonSearchComponent,
        SystemInvoiceFormatLanguageSearchComponent,
        SICSearchComponent,
        LanguageSearchComponent,
        InfestationSearchComponent,
        ServiceVisitSearchComponent,
        PrepUsedSearchComponent,
        SystemPDAActivityTypeLanguageSearchComponent,
        PDAInfestationSearchComponent,
        MUserSearchComponent,
        UserInformationSearchComponent
    ],
    entryComponents: [
        InvoiceChargeSearchComponent,
        ServicePlanningInfoComponent,
        AccountSearchComponent,
        AccountGroupSearchComponent,
        GroupAccountNumberComponent,
        AccountHistoryDetailComponent,
        ServicePlanSearchComponent,
        PlanVisitSearchComponent,
        PDAiCABSPrepUsedSearchComponent,
        PrepUsedMaintenanceComponent,
        DebriefSearchComponent,
        InvoiceHeaderAddressComponent,
        ContactMediumLanguageSearchComponent,
        AccountBusinessTypeSearchComponent,
        CallCentreReviewGridMultiComponent,
        ServiceCoverSearchComponent,
        BatchProgramScheduleSearchComponent,
        JobServiceCoverSearchComponent,
        ProRataChargeStatusLanguageSearchComponent,
        InvoiceGroupSearchComponent,
        ServiceCoverLocationEffectDateSearchComponent,
        CustomerTypeSearchComponent,
        OccupationSearchComponent,
        PremiseLocationSearchComponent,
        ProRataChargeStatusLanguageSearchComponent,
        InvoiceSearchComponent,
        HistoryTypeLanguageSearchComponent,
        AUPostcodeSearchComponent,
        AccountPremiseSearchComponent,
        AccountHistoryGridComponent,
        InvoiceGroupGridComponent,
        LostBusinessContactSearchComponent,
        ScreenNotReadyComponent,
        ProspectSearchComponent,
        PremiseLocationAllocationGridComponent,
        ServiceCoverDetailsComponent,
        ServiceSummaryDetailComponent,
        MarktSelectSearchComponent,
        PNOLPremiseSearchGridComponent,
        LostBusinessRequestSearchComponent,
        ContactTypeDetailSearchComponent,
        LostBusinessContactOutcomeLanguageSearchComponent,
        SystemInvoiceCreditReasonSearchComponent,
        SystemInvoiceFormatLanguageSearchComponent,
        SICSearchComponent,
        LanguageSearchComponent,
        InfestationSearchComponent,
        ServiceVisitSearchComponent,
        PrepUsedSearchComponent,
        SystemPDAActivityTypeLanguageSearchComponent,
        PDAInfestationSearchComponent,
        MUserSearchComponent,
        UserInformationSearchComponent
    ]
})

export class InternalSearchEllipsisModule { }
