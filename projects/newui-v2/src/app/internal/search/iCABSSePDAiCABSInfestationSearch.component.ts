import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';

import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from './../../base/BaseComponent';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceServiceModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSSePDAiCABSInfestationSearch.html'
})

export class PDAInfestationSearchComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('infestationSearchPagination') infestationSearchPagination: PaginationComponent;

    public pageId: string = '';
    public itemsPerPage: number = 10;
    public pageCurrent: number = 1;
    public itemsTotal: number = 0;

    public controls: Array<any> = [
        { name: 'ContractNumber', disabled: true },
        { name: 'ContractName', disabled: true },
        { name: 'PremiseNumber', disabled: true },
        { name: 'PremiseName', disabled: true },
        { name: 'ProductCode', disabled: true },
        { name: 'ProductDesc', disabled: true },
        { name: 'menu' },
        { name: 'PDAICABSActivityRowID' },
        { name: 'ProductDetailCode' },
        { name: 'InfestationGroupCode' },
        { name: 'InfestationLocationCode' }
    ];
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPDAICABSINFESTATIONSEARCH;
        this.pageTitle = this.browserTitle = 'Infestation Search';
    }
    ngOnInit(): void {
        super.ngOnInit();
        this.buildGrid();
        this.refresh();
        this.windowOnload();
    }
    ngAfterViewInit(): void {
        this.setControlValue('menu', '');
    }
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
        this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
        this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
        this.setControlValue('ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));
        this.setControlValue('PDAICABSActivityRowID', this.riExchange.getParentHTMLValue('PDAICABSActivityRowID'));
    }
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('ProductDetailCode', 'Infest', 'ProductDetailCode', MntConst.eTypeCode, 6, false, '');
        this.riGrid.AddColumn('ProductDetailDesc', 'Infest', 'ProductDetailDesc', MntConst.eTypeText, 20, false, '');
        this.riGrid.AddColumn('InfestationGroupCode', 'Infest', 'InfestationGroupCode', MntConst.eTypeCode, 6, false, '');
        this.riGrid.AddColumn('InfestationGroupDesc', 'Infest', 'InfestationGroupDesc', MntConst.eTypeText, 20, false, '');
        this.riGrid.AddColumn('InfestationLocationCode', 'Infest', 'InfestationLocationCode', MntConst.eTypeCode, 6, false, '');
        this.riGrid.AddColumn('InfestationLocationDesc', 'Infest', 'InfestationLocationDesc', MntConst.eTypeText, 20, false, '');
        this.riGrid.AddColumn('InfestationLevelCode', 'Infest', 'InfestationLevelCode', MntConst.eTypeCode, 6, false, '');
        this.riGrid.AddColumn('InfestationLevelDesc', 'Infest', 'InfestationLevelDesc', MntConst.eTypeText, 20, false, '');
        this.riGrid.AddColumn('InfestationHML', 'Infest', 'InfestationHML', MntConst.eTypeText, 10, false, '');
        this.riGrid.AddColumnAlign('InfestationHML', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
    }

    private riGridBeforeExecute(): void {
        let gridParams = this.getURLSearchParamObject();
        let queryParams: any = {
            operation: 'Service/iCABSSePDAiCABSInfestationSearch',
            module: 'pda',
            method: 'service-delivery/search'
        };
        gridParams.set(this.serviceConstants.Action, '2');
        gridParams.set('PDAICABSActivityRowID', this.riExchange.getParentHTMLValue('PDAICABSActivityRowID'));
        gridParams.set(this.serviceConstants.GridMode, '0');
        gridParams.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        gridParams.set(this.serviceConstants.PageSize, this.itemsPerPage.toString());
        gridParams.set(this.serviceConstants.PageCurrent, this.pageCurrent.toString());
        gridParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridParams.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(queryParams.method, queryParams.module, queryParams.operation, gridParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullerror']));
                        return;
                    }
                    this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                    this.itemsTotal = data.pageData ? data.pageData.lastPageNumber * this.itemsPerPage : 1;

                    this.riGrid.RefreshRequired();

                    this.riGrid.Execute(data);
                    this.infestationSearchPagination.totalItems = this.itemsTotal;
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error));
                });
    }

    public riGridBodyOnClick(): void {
        this.setControlValue('ProductDetailCode', this.riGrid.Details.GetValue('ProductDetailCode'));
        this.setControlValue('InfestationGroupCode', this.riGrid.Details.GetValue('InfestationGroupCode'));
        this.setControlValue('InfestationLocationCode', this.riGrid.Details.GetValue('InfestationLocationCode'));
    }

    public riGridBodyOnDblClick(): void {
        this.setControlValue('ProductDetailCode', this.riGrid.Details.GetValue('ProductDetailCode'));
        this.setControlValue('InfestationGroupCode', this.riGrid.Details.GetValue('InfestationGroupCode'));
        this.setControlValue('InfestationLocationCode', this.riGrid.Details.GetValue('InfestationLocationCode'));
        if (this.riGrid.CurrentColumnName === 'ProductDetailCode') {
            this.navigate('SearchInfest', InternalMaintenanceServiceModuleRoutes.ICABSSEPDAICABSINFESTATIONMAINTENANCE);
        }
    }

    public getCurrentPage(event: any): void {
        this.pageCurrent = event.value;
        this.riGridBeforeExecute();
    }
    public refresh(): void {
        this.riGridBeforeExecute();
    }

    public menuOnchange(): void {
        if (this.getControlValue('menu') === 'AddInfest') {
            this.navigate('SearchAdd', InternalMaintenanceServiceModuleRoutes.ICABSSEPDAICABSINFESTATIONMAINTENANCE);
        }
    }
}
