
import { Component, OnInit, Input } from '@angular/core';
import { LocaleTranslationService } from '../../../shared/services/translation.service';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

@Component({

    templateUrl: 'iCABSAInvoiceHeaderAddressDetails.html'
})
export class InvoiceHeaderAddressComponent implements OnInit {

    @Input() inputParams: any;
    public storeSubscription: Subscription;
    public translateSubscription: Subscription;

    constructor(
        store: Store<any>,
        private localeTranslateService: LocaleTranslationService) {
        this.storeSubscription = store.select('contract').subscribe(data => {
            if (data !== null && data['data'] && !(Object.keys(data['data']).length === 0 && data['data'].constructor === Object)) {
            }

        });

    }

    ngOnInit(): void {
        this.localeTranslateService.setUpTranslation();

    }

}
