import { AfterContentInit, Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { Location } from '@angular/common';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { ModalAdvService } from '@shared/components/modal-adv/modal-adv.service';
import { PageIdentifier } from '@base/PageIdentifier';
import { PreparationSearchComponent } from './iCABSBPreparationSearch.component';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSSePDAiCABSPrepUsedMaintenance.html',
    providers: [CommonLookUpUtilsService]
})

export class PrepUsedMaintenanceComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('promptModal') public promptModal;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private ActivityRowID: string;
    private PrepUsedRowID: string;
    private prepData: Object = {};
    private prepList: Array<string>;

    public pageId: string;
    public pageMode: string = MntConst.eModeUpdate;
    public promptConfirmContent: string;

    private xhrParams: Record<string, string> = {
        method: 'service-delivery/maintenance',
        module: 'pda',
        operation: 'Service/iCABSSePDAiCABSPrepUsedMaintenance'
    };

    public ellipsis: any = {
        prepCodeEllipsis: {
            inputParams: {
                parentMode: 'LookUp',
                showAddNew: false
            },
            contentComponent: PreparationSearchComponent,
            isEllipsisDisabled: false
        }
    };

    public controls: IControls[] = [
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true, required: true },
        { name: 'ContractNumber', type: MntConst.eTypeCode, required: true },
        { name: 'PremiseName', type: MntConst.eTypeTextFree, disabled: true, required: true },
        { name: 'PremiseNumber', type: MntConst.eTypeAutoNumber, required: true },
        { name: 'PrepBatchNumber', disabled: false, type: MntConst.eTypeText },
        { name: 'PrepCode', type: MntConst.eTypeCode },
        { name: 'PrepDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'PrepUsedText', type: MntConst.eTypeText },
        { name: 'PrepValue', type: MntConst.eTypeCurrency, required: true },
        { name: 'PrepVolume', type: MntConst.eTypeDecimal2, required: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, required: true },
        { name: 'ProductDesc', type: MntConst.eTypeText, disabled: true, required: true },
        { name: 'RoomClearanceSpan', disabled: true, type: MntConst.eTypeText },
        { name: 'RoomClearanceTime', disabled: true, type: MntConst.eTypeText }
    ];

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService, private location: Location, private modalAdvService: ModalAdvService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPDAICABSPREPUSEDMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Preparation Used Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.prepList = ['PrepVolume', 'PrepValue', 'PrepUsedText', 'PrepBatchNumber', 'RoomClearanceTime', 'RoomClearanceSpan'];
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();

        if (this.parentMode === 'Search' || this.parentMode === 'SearchAdd') {
            this.setControlValue('ContractNumber', this.riExchange.getParentAttributeValue('ContractNumber'));
            this.setControlValue('ContractName', this.riExchange.getParentAttributeValue('ContractName'));
            this.setControlValue('PremiseNumber', this.riExchange.getParentAttributeValue('PremiseNumber'));
            this.setControlValue('PremiseName', this.riExchange.getParentAttributeValue('PremiseName'));
            this.setControlValue('ProductCode', this.riExchange.getParentAttributeValue('ProductCode'));
            this.setControlValue('ProductDesc', this.riExchange.getParentAttributeValue('ProductDesc'));
            this.ActivityRowID = this.riExchange.getParentHTMLValue('Activity');
        }

        if (this.parentMode === 'Search') {
            this.controlChange(true);
            this.PrepUsedRowID = this.riExchange.getParentHTMLValue('PrepUsed');
            this.setControlValue('PrepCode', this.riExchange.getParentAttributeValue('PrepCode'));
            this.setControlValue('PrepDesc', this.riExchange.getParentAttributeValue('PrepDesc'));
            this.getPrepData();
        } else {
            this.setRequiredStatus('PrepCode', true);
            this.pageMode = MntConst.eModeAdd;
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private getPrepData(): void {
        if (this.ActivityRowID && this.PrepUsedRowID) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, 0);
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            let postObj: Object = {};

            postObj['PDAICABSPrepUsedROWID'] = this.PrepUsedRowID;
            postObj['PDAICABSActivityRowID'] = this.ActivityRowID;
            postObj['PrepCode'] = this.getControlValue('PrepCode');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, postObj).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.errorMessage) {
                        this.displayMessage(data.errorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    } else {
                        this.prepData = data;
                        this.setData();
                    }
                }).catch(error => {
                    this.displayMessage(error);
                });
        }
    }

    private setData(): void {
        if (this.prepData) {
            this.prepList.forEach(item => {
                this.setControlValue(item, this.prepData[item]);
            });
        } else {
            this.prepList.forEach(item => {
                this.setControlValue(item, '');
            });
        }
        if (this.parentMode === 'SearchAdd') {
            if (this.pageMode === MntConst.eModeAdd) {
                this.setControlValue('PrepCode', '');
                this.setControlValue('PrepDesc', '');
            } else {
                this.setControlValue('PrepCode', this.prepData['PrepCode']);
                this.setControlValue('PrepDesc', this.prepData['PrepDesc']);
            }
        }
    }

    public confirmed(): void {
        if (this.pageMode === MntConst.eModeDelete) {
            this.deleteRecord();
        }
        else {
            this.saveRecord();
        }
    }

    private saveRecord(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.pageMode === MntConst.eModeAdd ? 1 : 2);
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        let postObj: Object = {};

        if (this.pageMode === MntConst.eModeUpdate) {
            postObj['PDAICABSPrepUsedROWID'] = this.PrepUsedRowID;
        }
        postObj['ContractNumber'] = this.getControlValue('ContractNumber');
        postObj['PremiseNumber'] = this.getControlValue('PremiseNumber');
        postObj['ProductCode'] = this.getControlValue('ProductCode');
        postObj['PrepCode'] = this.getControlValue('PrepCode');
        postObj['PrepVolume'] = this.getControlValue('PrepVolume');
        postObj['PrepValue'] = this.getControlValue('PrepValue');
        postObj['PrepUsedText'] = this.getControlValue('PrepUsedText');
        postObj['PrepBatchNumber'] = this.getControlValue('PrepBatchNumber');
        postObj['RoomClearanceTime'] = this.getControlValue('RoomClearanceTime');
        postObj['RoomClearanceSpan'] = this.getControlValue('RoomClearanceSpan');
        postObj['PDAICABSActivityRowID'] = this.ActivityRowID;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, postObj).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.displayMessage(data.errorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                } else {
                    this.prepData = {};
                    this.prepList.forEach(item => {
                        this.prepData[item] = this.getControlValue(item);
                    });
                    this.prepData['PrepCode'] = this.getControlValue('PrepCode');
                    this.prepData['PrepDesc'] = this.getControlValue('PrepDesc');
                    if (this.pageMode === MntConst.eModeAdd) {
                        this.PrepUsedRowID = data.PDAICABSPrepUsed;
                        this.controlChange(true);
                    }
                    this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                    this.pageMode = MntConst.eModeUpdate;
                    this.formPristine();
                }
            }).catch(error => {
                this.displayMessage(error);
            });
    }

    private deleteRecord(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, 3);
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        let postObj: Object = {};

        postObj['PDAICABSPrepUsedROWID'] = this.PrepUsedRowID;
        postObj['PDAICABSActivityRowID'] = this.ActivityRowID;
        postObj['ContractNumber'] = this.getControlValue('ContractNumber');
        postObj['PremiseNumber'] = this.getControlValue('PremiseNumber');
        postObj['ProductCode'] = this.getControlValue('ProductCode');
        postObj['PrepCode'] = this.getControlValue('PrepCode');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, postObj).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage) {
                    this.displayMessage(data.errorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                } else {
                    event.preventDefault();
                    this.location.back();
                }
            });
    }

    private deleteCanceled(): void {
        this.pageMode = MntConst.eModeUpdate;
    }

    public onPrepVolChange(): void {
        let prepCode = this.getControlValue('PrepCode');
        let prepVol = this.getControlValue('PrepVolume');
        if (!prepVol) {
            return;
        }
        if (this.ActivityRowID && prepCode) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, 6);
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            let postObj: Object = {};

            postObj['PDAICABSActivityRowID'] = this.ActivityRowID;
            postObj['PrepCode'] = prepCode;
            postObj['PrepVolume'] = prepVol;
            postObj['Function'] = 'GetPrepValue';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, postObj).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.errorMessage) {
                        this.displayMessage(data.errorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    } else {
                        this.setControlValue('PrepValue', this.globalize.formatCurrencyToLocaleFormat(data.PrepValue));
                    }
                }).catch(error => {
                    this.displayMessage(error);
                });
        } else {
            this.displayMessage(MessageConstant.Message.RecordNotFound, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
            this.markControlAsTouched('PrepCode');
        }

    }

    public onPrepCodeDataReceived(data: any): void {
        if (data) {
            this.setControlValue('PrepCode', data.PrepCode);
            this.setControlValue('PrepDesc', data.PrepDesc);
            this.uiForm.markAsDirty();
        }
    }

    public onPrepCodeChange(): void {
        let prepCode = this.getControlValue('PrepCode');
        if (prepCode) {
            this.commonLookup.getPrepDesc(prepCode).then(data => {
                if (data && data[0] && data[0].length) {

                    this.setControlValue('PrepDesc', data[0][0]['PrepDesc']);
                } else {
                    this.setControlValue('PrepCode', '');
                }
            }).catch(error => {
                this.displayMessage(error);
            });
        } else {
            this.setControlValue('PrepDesc', '');
        }
    }

    public onBtnClicked(action: string): void {
        switch (action) {
            case 'Save':
                if (this.riExchange.validateForm(this.uiForm)) {
                    this.promptModal.show();
                    this.promptConfirmContent = MessageConstant.Message.ConfirmRecord;
                }
                break;
            case 'Delete':
                this.pageMode = MntConst.eModeDelete;
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteRecord.bind(this), this.deleteCanceled.bind(this)));
                this.promptConfirmContent = MessageConstant.Message.DeleteRecord;
                break;
            case 'Cancel':
                this.setData();
                this.formPristine();
                break;
        }
    }

    private controlChange(status: boolean): void {
        this.disableControl('PrepCode', status);
        this.ellipsis.prepCodeEllipsis.isEllipsisDisabled = status;
    }
}
