import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { InternalGridSearchServiceModuleRoutes } from './../../base/PageRoutes';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '../../../shared/services/riMaintenancehelper';

@Component({
    templateUrl: 'iCABSSeServicePlanningInfo.html'
})

export class ServicePlanningInfoComponent extends BaseComponent implements OnInit, OnDestroy {
    private queryParams: any = {
        operation: 'Service/iCABSSeServicePlanningInfo',
        module: 'planning',
        method: 'service-planning/maintenance'
    };

    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', type: MntConst.eTypeText },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger },
        { name: 'PremiseName', type: MntConst.eTypeText },
        { name: 'ProductCode', type: MntConst.eTypeCode },
        { name: 'ProductDesc', type: MntConst.eTypeText },
        { name: 'ContractExpiryDate', type: MntConst.eTypeDate },
        { name: 'ServiceSpecialInstructions', type: MntConst.eTypeTextFree },
        { name: 'WindowStart01', type: MntConst.eTypeTime },
        { name: 'WindowEnd01', type: MntConst.eTypeTime },
        { name: 'WindowStart08', type: MntConst.eTypeTime },
        { name: 'WindowEnd08', type: MntConst.eTypeTime },
        { name: 'WindowStart02', type: MntConst.eTypeTime },
        { name: 'WindowEnd02', type: MntConst.eTypeTime },
        { name: 'WindowStart09', type: MntConst.eTypeTime },
        { name: 'WindowEnd09', type: MntConst.eTypeTime },
        { name: 'WindowStart03', type: MntConst.eTypeTime },
        { name: 'WindowEnd03', type: MntConst.eTypeTime },
        { name: 'WindowStart10', type: MntConst.eTypeTime },
        { name: 'WindowEnd10', type: MntConst.eTypeTime },
        { name: 'WindowStart04', type: MntConst.eTypeTime },
        { name: 'WindowEnd04', type: MntConst.eTypeTime },
        { name: 'WindowStart11', type: MntConst.eTypeTime },
        { name: 'WindowEnd11', type: MntConst.eTypeTime },
        { name: 'WindowStart05', type: MntConst.eTypeTime },
        { name: 'WindowEnd05', type: MntConst.eTypeTime },
        { name: 'WindowStart12', type: MntConst.eTypeTime },
        { name: 'WindowEnd12', type: MntConst.eTypeTime },
        { name: 'WindowStart06', type: MntConst.eTypeTime },
        { name: 'WindowEnd06', type: MntConst.eTypeTime },
        { name: 'WindowStart13', type: MntConst.eTypeTime },
        { name: 'WindowEnd13', type: MntConst.eTypeTime },
        { name: 'WindowStart07', type: MntConst.eTypeTime },
        { name: 'WindowEnd07', type: MntConst.eTypeTime },
        { name: 'WindowStart14', type: MntConst.eTypeTime },
        { name: 'WindowEnd14', type: MntConst.eTypeTime },
        // Hidden field
        { name: 'ServiceCoverRowID', type: MntConst.eTypeText },
        { name: 'CustomerServiceInfo', type: MntConst.eTypeText }
    ];
    public tabs: any = {
        tab0: { active: true },
        tab1: { active: false },
        tab2: { active: false }
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNINGINFO;

        this.browserTitle = this.pageTitle = 'Service Cover Information';
        this.pageParams.isShowContractHasExpired = false;
        this.pageParams.isShowCustomerInfo = false;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // Initializes data into different controls on page load
    private onWindowLoad(): void {
        this.setControlValue('ServiceCoverRowID', this.riExchange.getParentAttributeValue('ServiceCoverRowID'));

        if (this.getControlValue('ServiceCoverRowID')) {
            this.populateDetails();
        } else {
            this.uiForm.disable();
        }
    }

    private populateDetails(): void {
        let search: QueryParams = this.getURLSearchParamObject();

        search.set('ServiceCoverROWID', this.getControlValue('ServiceCoverRowID'));
        search.set(this.serviceConstants.Action, '0');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    for (let key in data) {
                        if (key) {
                            if (data[key].length > 0) {
                                this.setControlValue(key, data[key]);
                            }
                            this.disableControl(key, true);
                        }
                    }
                    this.afterFetch();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private afterFetch(): void {
        if (this.getControlValue('ContractExpiryDate')) {
            if (new Date(this.getControlValue('ContractExpiryDate')).getTime() <= new Date().getTime()) {
                this.pageParams.isShowContractHasExpired = true;
            }
        }

        if (this.getControlValue('CustomerServiceInfo') && this.getControlValue('CustomerServiceInfo') === 'yes') {
            this.pageParams.isShowCustomerInfo = true;
        }
    }

    public onCustomerInfoClick(): void {
        this.navigate('ServiceCover', InternalGridSearchServiceModuleRoutes.ICABSACUSTOMERINFORMATIONSUMMARY.URL_2, {
            'ContractNumber': this.getControlValue('ContractNumber'),
            'ContractName': this.getControlValue('ContractName')
        });
    }

    public onTabClick(index: number): void {
        if (this.tabs['tab' + index].active) {
            return;
        }

        for (let key in this.tabs) {
            if (!key) {
                continue;
            }

            if (key === 'tab' + index) {
                this.tabs[key].active = true;
                continue;
            }

            this.tabs[key].active = false;
        }
    }
}
