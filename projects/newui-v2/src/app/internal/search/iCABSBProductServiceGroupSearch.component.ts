import { DropdownConstants } from './../../base/ComponentConstants';
import { Component, OnInit, Input, EventEmitter, ViewChild, Output, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Subscription } from 'rxjs';

import { RiExchange } from './../../../shared/services/riExchange';
import { Utils } from '../../../shared/services/utility';
import { DropdownComponent } from '../../../shared/components/dropdown/dropdown';
import { HttpService } from '../../../shared/services/http-service';
import { ServiceConstants } from '../../../shared/constants/service.constants';
import { SearchDropdown } from '@app/base/search-dropdown';

@Component({
    selector: 'icabs-sbproductservice-groupsearch',
    templateUrl: 'iCABSBProductServiceGroupSearch.html'
})

export class ProductServiceGroupSearchComponent extends SearchDropdown implements OnInit, OnDestroy {
    @ViewChild('productServiceDropDown') public productServiceDropDown: DropdownComponent;
    @Input() public inputParams: any;
    @Input() public isDisabled: boolean = false;
    @Input() public active: any;
    @Input() public isRequired: boolean = true;
    @Input() public isTriggerValidate: boolean;
    @Output() receivedProductServiceGroupSearch: EventEmitter<any> = new EventEmitter();
    @Output() getDefaultInfo = new EventEmitter();
    @Input() public isMultiSelect: boolean;

    private errorMessage: any;
    private subFetchData: Subscription;

    public displayFields: Array<string> = ['ProductServiceGroupCode', 'ProductServiceGroupDesc'];

    constructor(
        private serviceConstants: ServiceConstants,
        private httpService: HttpService,
        private utils: Utils,
        private riExchange: RiExchange) {
        super();
    }

    ngOnInit(): void {
        this.fetchData();
    }

    ngOnDestroy(): void {
        if (this.subFetchData) {
            this.subFetchData.unsubscribe();
        }
    }

    /*
    *Get Dropdown Data On Load
    */
    public fetchData(): void {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let xhr: any = {
            module: 'product',
            method: 'service-delivery/search',
            operation: 'Business/iCABSBProductServiceGroupSearch',
            search: search
        };

        this.subFetchData = this.httpService.makeGetRequest(xhr.method, xhr.module, xhr.operation, xhr.search).subscribe(data => {
            if (!data.records) { return; }
            let requestdata: Array<any> = data.records;
            this.responseData = data.records;
            let newRowIndex: any;
            for (let i = 0; i < requestdata.length; i++) {
                if (requestdata[i].ProductServiceGroupCode === this.active.id) {
                    newRowIndex = i;
                }
            }
            this.getDefaultInfo.emit({ totalRecords: requestdata ? requestdata.length : 0, firstRow: (requestdata && requestdata.length > 0) ? requestdata[0] : {}, newRow: (requestdata && requestdata.length > 0) ? requestdata[newRowIndex] : {} });
            this.productServiceDropDown.updateComponent(data.records);
            if (this.preValue) {
                this.setValue(this.preValue);
                this.preValue = '';
            }
        }, error => {
            this.errorMessage = error as any;
        });
    }

    /*
   *Return Dropdown Data on Selection
   */
    public onDataReceived(obj: any): void {
        let selectedRecords = obj.value;
        let returnString: string = '';
        let returnObj: any;
        switch (this.inputParams.params.parentMode) {
            case 'LookUp':
                returnObj = {
                    'ProductServiceGroupCode': selectedRecords.ProductServiceGroupCode,
                    'ProductServiceGroupDesc': selectedRecords.ProductServiceGroupDesc
                };
                break;
            case 'LookUp-String':
                returnString = this.inputParams.params.ProductServiceGroupString.trim();
                if (returnString) {
                    returnString = returnString + ',' + selectedRecords.ProductServiceGroupCode;
                }
                else {
                    returnString = selectedRecords.ProductServiceGroupCode;
                }
                returnObj = {
                    'ProductServiceGroupString': returnString,
                    ProductServiceGroupDesc: selectedRecords.ProductServiceGroupDesc
                };
                break;
            case 'GeneralSearch-Lookup3':
                returnString = this.inputParams.params.SearchValue3.trim();
                if (returnString) {
                    returnString = returnString + ',' + selectedRecords.ProductServiceGroupCode;
                }
                else {
                    returnString = selectedRecords.ProductServiceGroupCode;
                }
                returnObj = {
                    'SearchValue3': returnString
                };
                break;
            case 'LookUpMultiple':
                let strServiceTypes = this.inputParams.params.ProductServiceGroupCode.trim();
                if (strServiceTypes) {
                    strServiceTypes = strServiceTypes + ',' + selectedRecords.ProductServiceGroupCode;
                }
                else {
                    strServiceTypes = selectedRecords.ProductServiceGroupCode;
                }
                returnObj = {
                    'ProductServiceGroupCode': strServiceTypes,
                    'ProductServiceGroupDesc': selectedRecords.ProductServiceGroupDesc
                };
                break;
            default:
                returnObj = {
                    'ProductServiceGroupCode': selectedRecords.ProductServiceGroupCode,
                    'ProductServiceGroupDesc': selectedRecords.ProductServiceGroupDesc
                };
        }
        if (this.isMultiSelect) {
            returnObj[DropdownConstants.c_s_KEY_MULTISELECTVALUE] = obj.value[DropdownConstants.c_s_KEY_MULTISELECTVALUE];
        }
        this.receivedProductServiceGroupSearch.emit(returnObj);
    }
}
