import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { TableComponent } from './../../../shared/components/table/table';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { InternalMaintenanceServiceModuleRoutes } from '../../../app/base/PageRoutes';

@Component({
    templateUrl: 'iCABSSePrepUsedSearch.html'
})

export class PrepUsedSearchComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riTable') riTable: TableComponent;
    /**
     * Private Variables
     */
    //Page Level Variable
    private serviceVisitRowID: string = '';
    //API Variables
    private queryParams: Object = {
        operation: 'Service/iCABSSePrepUsedSearch',
        module: 'preps',
        method: 'service-delivery/search'
    };
    /**
     * Public Variables
     */
    //Page Level Variables
    public pageId: string = '';
    public isDisplayMenu: boolean = false;
    //Form Variables
    public controls: Array<Object> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, disabled: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeAutoNumber, disabled: true },
        { name: 'PremiseName', type: MntConst.eTypeTextFree, disabled: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'ProductDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'menu', type: '' }
    ];
    //Table Config Variables
    public prepUsedTableConfig: any = {
        itemsPerPage: '10',
        pageStart: '1'
    };
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPREPUSEDSEARCH;
        this.pageTitle = 'Prep Used Details';
        this.browserTitle = 'Prep Used Search';
    }
    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.setControlValue('menu', '');
        }
        this.windowOnload();
    }
    ngOnDestory(): void {
        super.ngOnDestroy();
    }
    private windowOnload(): void {
        this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));

        this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));

        this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
        this.setControlValue('ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));

        this.serviceVisitRowID = this.riExchange.getParentHTMLValue('ServiceVisit');

        if (this.parentMode === 'ServiceVisit') {
            this.isDisplayMenu = true;
        }
        this.loadTableData();
    }
    /**
     * Method to used to create table fields
     * @params: params: void
     * @return: : void
     */
    private buildTableColumns(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('PrepCode', MntConst.eTypeCode, 'Key', 'Prep Code', 10);
        this.riTable.AddTableField('PrepDesc', MntConst.eTypeText, 'Virtual', 'Description', 30);
        this.riTable.AddTableField('MeasureBy', MntConst.eTypeText, 'Virtual', 'Measure By', 20);
        this.riTable.AddTableField('PrepVolume', MntConst.eTypeDecimal2, 'Required', 'Volume', 10);
        this.riTable.AddTableField('PrepValue', MntConst.eTypeCurrency, 'Required', 'Prep Value', 10);
    }
    /**
     * Method to used to load table data
     * @params: params: void
     * @return: : void
     */
    private loadTableData(): void {
        let search: any = this.getURLSearchParamObject(), searchParams: any = {};
        search.set(this.serviceConstants.Action, '0');
        search.set('search.sortby', 'PrepCode');
        search.set('ProductCode', this.getControlValue('ProductCode'));
        search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        search.set('ContractNumber', this.getControlValue('ContractNumber'));
        search.set('ServiceVisitRowID', this.serviceVisitRowID);

        searchParams.search = search;
        searchParams.module = this.queryParams['module'];
        searchParams.method = this.queryParams['method'];
        searchParams.operation = this.queryParams['operation'];
        this.buildTableColumns();
        this.riTable.loadTableData(searchParams);
    }
    /**
     * Method to get executed when user click on the individual row
     * @params: params: void
     * @return: : void
     */
    public selectedPrepUsedData(event: any): void {
        let vntReturnData: any, prepUsedDataParam: any;
        vntReturnData = event.row;
        switch (this.parentMode) {
            case 'ServiceVisit':
                prepUsedDataParam = {
                    parentMod: 'Search',
                    PrepUsedRowID: vntReturnData.ttPrepUsed,
                    ContractNumber: this.getControlValue('ContractNumber'),
                    PremiseNumber: this.getControlValue('PremiseNumber'),
                    ProductCode: this.getControlValue('ProductCode'),
                    PrepCode: vntReturnData.PrepCode,
                    ServiceVisitRowID: this.serviceVisitRowID
                };
                this.navigate('Search', InternalMaintenanceServiceModuleRoutes.ICABSSEPREPUSEDMAINTENANCE, prepUsedDataParam);
                break;
            default:
                if (this.parentMode === 'LookUp') {
                    this.riExchange.setParentHTMLValue('PrepCode', vntReturnData.PrepCode);
                } else {
                    this.riExchange.setParentHTMLValue('PrepCode', vntReturnData.PrepCode);
                }
                break;
        }
    }
    /**
     * Method to get executed when user select option menu
     * @params: params: void
     * @return: : void
     */
    public prepMenuOnchange(value: string): void {
        if (value === 'AddPrep') {
            this.navigate('SearchAdd', InternalMaintenanceServiceModuleRoutes.ICABSSEPREPUSEDMAINTENANCE, {
                ServiceVisitRowID: this.serviceVisitRowID
            });
        }
    }
}
