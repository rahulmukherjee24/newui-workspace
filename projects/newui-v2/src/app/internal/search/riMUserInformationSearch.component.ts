import { Component, OnInit, ViewChild, NgZone, OnDestroy, AfterContentInit } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BehaviorSubject, Subscription } from 'rxjs';

import { PageIdentifier } from './../../base/PageIdentifier';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { Utils } from './../../../shared/services/utility';
import { HttpService } from './../../../shared/services/http-service';
import { AjaxObservableConstant } from './../../../shared/constants/ajax-observable.constant';
import { AjaxConstant } from './../../../shared/constants/AjaxConstants';
import { ErrorConstant } from './../../../shared/constants/error.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { ModalAdvService } from './../../../shared/components/modal-adv/modal-adv.service';

@Component({
    templateUrl: 'riMUserInformationSearch.html'
})

export class UserInformationSearchComponent extends SelectedDataEvent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    private headerParams: any = {
        method: 'it-functions/ri-model',
        module: 'user',
        operation: 'Model/riMUserInformationSearch'
    };
    private ajaxSource = new BehaviorSubject<any>(0);
    private ajaxSource$;
    public inputParams: any = {
        showAddNew: false
    };
    private ajaxSubscription: Subscription;
    private isFirstCallCompleted: boolean = false;
    public pageId: string;
    public pageTitle: string;
    public gridParams: any = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 0,
        cacheRefresh: false,
        gridHandle: 0
    };
    public isRequesting: boolean = false;

    constructor(private serviceConstants: ServiceConstants,
        private utils: Utils,
        private httpService: HttpService,
        private modalAdvService: ModalAdvService,
        private ajaxconstant: AjaxObservableConstant,
        private zone: NgZone
    ) {
        super();
        this.pageId = PageIdentifier.RIMUSERSEARCH;
        this.pageTitle = 'User Search';
        this.ajaxSource$ = this.ajaxSource.asObservable();

        this.ajaxSubscription = this.ajaxSource$.subscribe(event => {
            if (event !== 0) {
                this.zone.run(() => {
                    switch (event) {
                        case AjaxConstant.START:
                            this.isRequesting = true;
                            break;
                        case AjaxConstant.COMPLETE:
                            this.isRequesting = false;
                            break;
                    }
                });
            }
        });
    }

    ngOnInit(): void {
        this.ajaxSource$ = this.ajaxSource.asObservable();
        this.gridParams.gridHandle = this.utils.randomSixDigitString();
    }

    ngAfterContentInit(): void {
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.FunctionUpdateSupport = true;
        this.riGrid.FunctionTabSupport = true;
        this.riGrid.PageSize = this.gridParams.pageSize;
        this.riGrid.DescendingSort = true;
        this.buildGrid();
        this.onGridRefresh();
    }

    ngOnDestroy(): void {
        if (this.ajaxSubscription) {
            this.ajaxSubscription.unsubscribe();
        }
    }
    // This function builds the grid structure
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('UserCode', 'GridUserInformationSearch', 'UserCode', MntConst.eTypeText, 10, false);
        this.riGrid.AddColumnTabSupport('UserCode', true);
        this.riGrid.AddColumn('UserName', 'GridUserInformationSearch', 'UserName', MntConst.eTypeText, 35, true);
        this.riGrid.AddColumnTabSupport('UserName', true);
        this.riGrid.AddColumn('Email', 'GridUserInformationSearch', 'Email', MntConst.eTypeText, 50, false);
        this.riGrid.AddColumnTabSupport('Email', true);
        this.riGrid.AddColumn('DateLastLoggedOn', 'GridUserInformationSearch', 'DateLastLoggedOn', MntConst.eTypeDate, 8, false);
        this.riGrid.AddColumnTabSupport('DateLastLoggedOn', true);
        this.riGrid.AddColumn('UserTypeCode', 'GridUserInformationSearch', 'UserTypeCode', MntConst.eTypeText, 3, false);
        this.riGrid.AddColumnTabSupport('UserTypeCode', true);
        this.riGrid.AddColumn('UserTypeDescription', 'GridUserInformationSearch', 'UserTypeDescription', MntConst.eTypeText, 20, false);
        this.riGrid.AddColumnTabSupport('UserTypeDescription', true);
        this.riGrid.AddColumn('ApprovalLevelCode', 'GridUserInformationSearch', 'ApprovalLevelCode', MntConst.eTypeText, 2, false);
        this.riGrid.AddColumnTabSupport('ApprovalLevelCode', true);
        this.riGrid.AddColumn('Inactive', 'GridUserInformationSearch', 'Inactive', MntConst.eTypeImage, 6, false);
        this.riGrid.AddColumnTabSupport('Inactive', true);
        this.riGrid.AddColumn('riDeveloper', 'GridUserInformationSearch', 'riDeveloper', MntConst.eTypeImage, 6, false);
        this.riGrid.AddColumnTabSupport('riDeveloper', true);
        this.riGrid.Complete();
        this.riGrid.RefreshRequired();
    }
    // This function triggers grid service
    private loadGridData(): void {
        let searchParams: QueryParams = new QueryParams();
        searchParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        searchParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set('LookupMode', this.inputParams['ParentMode'] || this.inputParams['parentMode']);
        searchParams.set('BranchNumber', this.inputParams['BranchNumber']);
        searchParams.set(this.serviceConstants.GridMode, '0');
        searchParams.set(this.serviceConstants.GridHandle, this.gridParams.gridHandle);
        searchParams.set(this.serviceConstants.GridPageSize, this.riGrid.PageSize.toString());
        searchParams.set(this.serviceConstants.PageCurrent, this.gridParams.currentPage);
        searchParams.set(this.serviceConstants.GridHeaderClickedColumn, '');
        searchParams.set(this.serviceConstants.GridSortOrder, this.riGrid.DescendingSort ? 'Descending' : 'Ascending');
        if (this.gridParams.cacheRefresh) {
            searchParams.set(this.serviceConstants.GridCacheRefresh, this.gridParams.cacheRefresh);
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrGet(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            searchParams).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                    return;
                }
                this.gridParams.currentPage = data.pageData.pageNumber;
                this.gridParams.totalRecords = data.pageData.lastPageNumber * 10 || 10;
                this.riGrid.Execute(data);
            }).catch(error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
            });
    }
    // This function is called on the click of refresh button
    public onGridRefresh(): void {
        this.gridParams.cacheRefresh = true;
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }
    // This function is called when pagination changes
    public onCurrentPageRecieved(data: any): void {
        this.gridParams.currentPage = data.value;
        this.gridParams.cacheRefresh = false;
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }
    // This function is called when grid row is double clicked
    public onBodyDoubleClick(): void {
        let returnData: any = {};
        if (!this.inputParams.hasOwnProperty('parentMode')) {
            return;
        }
        switch (this.inputParams.parentMode) {
            case 'riBatchProgamSchedule':
                returnData['riBPSUserCode'] = this.riGrid.Details.GetValue('UserCode');
                returnData['UserName'] = this.riGrid.Details.GetValue('UserName');
                break;
            case 'LookUp':
                returnData['UserCode'] = this.riGrid.Details.GetValue('UserCode');
                returnData['UserName'] = this.riGrid.Details.GetValue('UserName');
                break;
            case 'LookUp-Advantage':
                returnData['FilterUserCode'] = this.riGrid.Details.GetValue('UserCode');
                returnData['UserName'] = this.riGrid.Details.GetValue('UserName');
                returnData['EmployeeCode'] = this.riGrid.Details.GetAttribute('UserCode', 'additionalproperty');
                break;
            case 'LookUp-Dash':
                returnData['UserCode'] = this.riGrid.Details.GetValue('UserCode');
                returnData['UserName'] = this.riGrid.Details.GetValue('UserName');
                break;
            case 'LookUp-Email':
                returnData['UserCode'] = this.riGrid.Details.GetValue('UserCode');
                returnData['UserName'] = this.riGrid.Details.GetValue('UserName');
                returnData['EmailAddress'] = this.riGrid.Details.GetValue('Email');
                break;
            case 'AuditStaticMove':
                returnData['ViewByValue'] = this.riGrid.Details.GetValue('UserCode');
                returnData['ViewByValueDesc'] = this.riGrid.Details.GetValue('UserName');
                break;
            case 'Search2':
                returnData['UserCode2'] = this.riGrid.Details.GetValue('UserCode');
                returnData['UserName'] = this.riGrid.Details.GetValue('UserName');
                break;
            default:
                returnData['UserCode'] = this.riGrid.Details.GetValue('UserCode');
        }
        this.emitSelectedData(returnData);
    }
    // This function is called input params changes
    public updateView(params: any): void {
        this.inputParams = params;
        if (!this.isFirstCallCompleted) {
            this.onGridRefresh();
        }
        this.isFirstCallCompleted = true;
    }
    // This function is called when add new button is clicked
    public onAddNew(): void {
        this.emitSelectedData('AddModeOn');
    }
}
