import { Component, OnDestroy, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Utils } from './../../../shared/services/utility';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { PageIdentifier } from './../../base/PageIdentifier';
import { TableComponent } from './../../../shared/components/table/table';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';

@Component({
    templateUrl: 'riMGLanguageSearch.html'
})

export class LanguageSearchComponent extends SelectedDataEvent implements OnDestroy {
    @ViewChild('riMGLanguageSearchTable') riMGLanguageSearchTable: TableComponent;
    public itemsPerPage: string = '10';
    public pageId: string;
    public inputParams: any = {
        method: 'it-functions/ri-model',
        operation: 'Model/riMGLanguageSearch',
        module: 'configuration'
    };

    public columns: Array<any> = [
        { title: 'Code', name: 'LanguageCode', type: MntConst.eTypeCode },
        { title: 'Description', name: 'LanguageDescription', type: MntConst.eTypeText },
        { title: 'Default', name: 'LanguageDefault', type: MntConst.eTypeCheckBox }
    ];

    constructor(
        private serviceConstants: ServiceConstants,
        private utils: Utils
    ) {
        super();
        this.pageId = PageIdentifier.ICABSSSICSEARCH;
    }

    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
    }

    private getTablePageData(): void {
        let search: any = new QueryParams();
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set(this.serviceConstants.Action, '0');
        this.inputParams.search = search;
        this.riMGLanguageSearchTable.loadTableData(this.inputParams);
    }

    public updateView(params?: any): void {
        if (params && params.parentMode) {
            this.inputParams.parentMode = params.parentMode;
        }
        this.getTablePageData();
    }

    /* Sends data to parent page ellipsis */
    public selectedData(event: any): void {
        let returnObj = {};
        switch (this.inputParams.parentMode) {
            case 'LookUp':
                returnObj = {
                    'LanguageCode': event.row.LanguageCode,
                    'LanguageDescription': event.row.LanguageDescription
                };
                break;
            case 'LookUp-MarketAttrGrid':
                returnObj = {
                    'LanguageCodeValue': event.row.LanguageCode,
                    'LanguageDesc': event.row.LanguageDescription
                };
                break;
            default:
                returnObj = {
                    'LanguageCodeValue': event.row.LanguageCode
                };

        }
        this.emitSelectedData(returnObj);
    }
}
