import { Component, OnInit, Injector, ViewChild, OnDestroy, NgZone, AfterContentInit } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Rx';

import { PageIdentifier } from './../../base/PageIdentifier';
import { InternalMaintenanceServiceModuleRoutes } from './../../base/PageRoutes';
import { Utils } from './../../../shared/services/utility';
import { RiExchange } from './../../../shared/services/riExchange';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { TableComponent } from './../../../shared/components/table/table';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';
import { HttpService } from './../../../shared/services/http-service';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { ModalAdvService } from './../../../shared/components/modal-adv/modal-adv.service';
import { AjaxObservableConstant } from './../../../shared/constants/ajax-observable.constant';

@Component({
    templateUrl: 'iCABSSeServiceVisitSearch.html'
})

export class ServiceVisitSearchComponent extends SelectedDataEvent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riTable') riTable: TableComponent;
    private parentMode: string;
    private queryParams: Object = {
        operation: 'Service/iCABSSeServiceVisitSearch',
        module: 'service',
        method: 'service-delivery/search'
    };

    // Subscription variable
    public ajaxSource = new BehaviorSubject<any>(0);
    public ajaxSource$: Observable<any>;
    public ajaxSubscription: Subscription;
    public activatedRouteSubscription: Subscription;
    public isRequesting: boolean = false;

    //Inject classes
    public httpService: HttpService;
    public ajaxconstant: AjaxObservableConstant;
    public zone: NgZone;
    public router: Router;
    public formBuilder: FormBuilder;
    public utils: Utils;
    public riExchange: RiExchange;
    public serviceConstants: ServiceConstants;
    public activatedRoute: ActivatedRoute;

    //Variables declared to use all classes
    public URLparam: any;
    public pageTitle: string;
    public itemsPerPage: number = 10;
    public uiForm: FormGroup;
    public pageId: string = '';
    public numberLabel: string = '';
    public pageParams: any = {
        'vbBusinessCode': '',
        'vbContractNumber': '',
        'vbPremiseNumber': '',
        'vbProductCode': '',
        'vbServiceCoverNumber': ''

    };
    public inputParams: any = {};
    public controls: Array<Object> = [
        { name: 'ContractNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true },
        { name: 'PremiseNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: true },
        { name: 'ProductCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ProductDesc', disabled: true },
        { name: 'menu' },
        { name: 'ServiceCoverRowID' },
        { name: 'ServiceVisitRowID' }
    ];

    constructor(injector: Injector, private modalAdvService: ModalAdvService) {
        super();
        this.injectServices(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEVISITSEARCH;
        this.pageTitle = 'Service Visit Details';
        this.activatedRouteSubscription = this.activatedRoute.queryParams.subscribe(
            (param: any) => {
                if (param) {
                    this.riExchange.setRouterParams(param);
                }
            });
    }

    ngOnInit(): void {
        this.uiForm = this.formBuilder.group({});
        this.riExchange.renderForm(this.uiForm, this.controls);
        this.parentMode = this.riExchange.getParentMode();
        if (this.parentMode === 'ServiceCover')
            this.utils.setTitle('Service Visit Search');
    }

    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
        this.formBuilder = null;
        this.riExchange = null;
    }

    ngAfterContentInit(): void {
        this.windowOnLoad();
    }

    private windowOnLoad(): void {
        this.URLparam = this.riExchange.getParentHTMLValue('currentContractTypeURLParameter');
        this.numberLabel = this.riExchange.getCurrentContractTypeLabel() + ' Number';
        this.riExchange.riInputElement.SetValue(this.uiForm, 'ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        this.riExchange.riInputElement.SetValue(this.uiForm, 'ContractName', this.riExchange.getParentHTMLValue('ContractName'));
        this.riExchange.riInputElement.SetValue(this.uiForm, 'PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        this.riExchange.riInputElement.SetValue(this.uiForm, 'PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
        this.riExchange.riInputElement.SetValue(this.uiForm, 'ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
        this.riExchange.riInputElement.SetValue(this.uiForm, 'ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));
        this.riExchange.riInputElement.SetValue(this.uiForm, 'ServiceCoverRowID', this.riExchange.getParentHTMLValue('ServiceCoverRowID'));
        this.fetchServiceCover();
    }

    private fetchServiceCover(): void {
        let searchParams: QueryParams = new QueryParams();
        searchParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        searchParams.set(this.serviceConstants.Action, '6');
        let bodyParams: any = {};
        bodyParams['Function'] = 'RowIDReturnsPrimaryKeyFields';
        bodyParams['ServiceCoverRowID'] = this.riExchange.riInputElement.GetValue(this.uiForm, 'ServiceCoverRowID');
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.pageParams.vbBusinessCode = data.BusinessCode;
                    this.pageParams.vbContractNumber = data.ContractNumber;
                    this.pageParams.vbPremiseNumber = data.PremiseNumber;
                    this.pageParams.vbProductCode = data.ProductCode;
                    this.pageParams.vbServiceCoverNumber = data.ServiceCoverNumber;
                    this.buildTableColumns();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    public injectServices(injector: Injector): void {
        this.formBuilder = injector.get(FormBuilder);
        this.utils = injector.get(Utils);
        this.serviceConstants = injector.get(ServiceConstants);
        this.riExchange = injector.get(RiExchange);
        this.activatedRoute = injector.get(ActivatedRoute);
        this.router = injector.get(Router);
        this.ajaxSource$ = this.ajaxSource.asObservable();
        this.httpService = injector.get(HttpService);
        this.ajaxconstant = injector.get(AjaxObservableConstant);
    }

    public buildTableColumns(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('ServiceDateStart', MntConst.eTypeDate, 'Required', 'Service Date', 10);
        this.riTable.AddTableField('VisitTypeCode', MntConst.eTypeCode, 'Required', 'Visit Type', 2);
        this.riTable.AddTableField('EmployeeSurname', MntConst.eTypeText, 'Virtual', 'Employee', 25);
        this.riTable.AddTableField('ServicedQuantity', MntConst.eTypeInteger, 'Required', 'Quantity', 5);
        this.riTable.AddTableField('SharedInd', MntConst.eTypeCheckBox, 'Optional', 'Shared', 1);
        this.buildTable();
    }

    public buildTable(): void {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set('ContractNumber', this.pageParams.vbContractNumber);
        search.set('PremiseNumber', this.pageParams.vbPremiseNumber);
        search.set('ProductCode', this.pageParams.vbProductCode);
        search.set('ServiceCoverNumber', this.pageParams.vbServiceCoverNumber);
        search.set('search.sortby', 'ServiceDateStart');
        this.inputParams.parentMode = this.parentMode;
        this.inputParams.search = search;
        this.inputParams.module = this.queryParams['module'];
        this.inputParams.method = this.queryParams['method'];
        this.inputParams.operation = this.queryParams['operation'];
        this.riTable.loadTableData(this.inputParams);
    }

    public tableRowClick(event: any): void {
        let vntReturnData: any, returnObj: Object;
        vntReturnData = event.row;
        switch (this.parentMode) {
            case 'ServiceCover':
                returnObj = {
                    'EmployeeSurname': vntReturnData.EmployeeSurname,
                    'ServiceDateStart': vntReturnData.ServiceDateStart,
                    'ServicedQuantity': vntReturnData.ServicedQuantity,
                    'VisitTypeCode': vntReturnData.VisitTypeCode
                };
                this.router.navigate([InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE], {
                    queryParams: {
                        parentMode: 'Search',
                        CurrentContractTypeURLParameter: this.riExchange.getCurrentContractTypeLabel(),
                        ServiceVisitRowID: vntReturnData.ttServiceVisit,
                        ServiceCoverRowID: this.riExchange.riInputElement.GetValue(this.uiForm, 'ServiceCoverRowID')
                    }
                });
                break;
            default:
                break;
        }
        this.emitSelectedData(returnObj);
    }

    public onServiceVisitClick(): void {
        this.router.navigate([InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE], {
            queryParams: {
                parentMode: 'SearchAdd',
                CurrentContractTypeURLParameter: this.riExchange.getCurrentContractTypeLabel(),
                ServiceCoverRowID: this.riExchange.riInputElement.GetValue(this.uiForm, 'ServiceCoverRowID')
            }
        });
    }

    public selectedOption(event: string): void {
        switch (event) {
            case 'AddVisit':
                this.onServiceVisitClick();
                break;
        }
    }

    public updateView(params: any): void {
        if (params.parentMode)
            this.parentMode = params.parentMode;
        this.buildTableColumns();
    }
}
