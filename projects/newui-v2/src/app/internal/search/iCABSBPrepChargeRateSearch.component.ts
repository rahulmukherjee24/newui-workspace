import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { PageIdentifier } from './../../base/PageIdentifier';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { Utils } from './../../../shared/services/utility';
import { RiExchange } from '../../../shared/services/riExchange';
import { TableComponent } from './../../../shared/components/table/table';
import { MntConst } from './../../../shared/services/riMaintenancehelper';

@Component({
    templateUrl: 'iCABSBPrepChargeRateSearch.html'
})

export class PrepChargeRateSearchComponent extends SelectedDataEvent implements OnInit, OnDestroy {
    @ViewChild('prepChargeRateSearch') prepChargeRateSearch: TableComponent;
    private utils: Utils;
    private serviceConstants: ServiceConstants;
    private formBuilder: FormBuilder;
    private riExchange: RiExchange;
    public uiForm: FormGroup;
    public pageId: string = '';
    public itemsPerPage: string = '10';
    public page: number = 1;
    public inputParams: Object;
    public enableAddNew = true;
    public showAddNew = false;
    public pageTitle: string;
    public isPrepVisible: boolean = false;
    public controls: Array<any> = [
        { name: 'PrepCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'PrepDesc', disabled: true, type: MntConst.eTypeText }
    ];
    // URL Query Parameters
    public queryParams: any = {
        operation: 'Business/iCABSBPrepChargeRateSearch',
        module: 'rates',
        method: 'bill-to-cash/search'
    };
    constructor(injector: Injector) {
        super();
        this.pageId = PageIdentifier.ICABSBPREPCHARGERATESEARCH;
        this.injectServices(injector);
    }
    ngOnInit(): void {
        this.uiForm = this.formBuilder.group({});
        this.riExchange.renderForm(this.uiForm, this.controls);
    }
    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
        this.formBuilder = null;
        this.riExchange = null;
    }
    private injectServices(injector: Injector): void {
        this.formBuilder = injector.get(FormBuilder);
        this.utils = injector.get(Utils);
        this.serviceConstants = injector.get(ServiceConstants);
        this.riExchange = injector.get(RiExchange);
    }
    private windowOnload(): void {
        this.riExchange.riInputElement.SetValue(this.uiForm, 'PrepCode', (this.inputParams['PrepCode']) ? this.inputParams['PrepCode'] : '');
        this.riExchange.riInputElement.SetValue(this.uiForm, 'PrepDesc', (this.inputParams['PrepDesc']) ? this.inputParams['PrepDesc'] : '');
        if (this.riExchange.riInputElement.GetValue(this.uiForm, 'PrepCode')) {
            this.isPrepVisible = true;
            this.pageTitle = 'Preparation Detail';
        } else {
            this.isPrepVisible = false;
            this.pageTitle = 'Preparation Charge Rate Search';
        }
    }
    //This method is responsible to load table data
    private loadTableData(): void {
        let search: QueryParams = new QueryParams(), searchParams: any = {};

        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (!this.riExchange.riInputElement.GetValue(this.uiForm, 'PrepCode')) {
            search.set('search.sortby', 'PrepCode');
        } else {
            search.set('search.sortby', 'ChargeRateCode');
            search.set('PrepCode', this.inputParams['PrepCode']);
        }
        searchParams.search = search;
        searchParams.module = this.queryParams.module;
        searchParams.method = this.queryParams.method;
        searchParams.operation = this.queryParams.operation;

        this.buildTableColumns();
        this.prepChargeRateSearch.loadTableData(searchParams);
    }
    //This methos is responsible to show columns for the table
    private buildTableColumns(): void {
        this.prepChargeRateSearch.clearTable();
        if (!this.riExchange.riInputElement.GetValue(this.uiForm, 'PrepCode')) {
            this.prepChargeRateSearch.AddTableField('PrepCode', MntConst.eTypeCode, 'Key', 'Prep Code', 5);
            this.prepChargeRateSearch.AddTableField('PrepDesc', MntConst.eTypeText, 'Virtual', 'Prep Desc', 20);
        }
        this.prepChargeRateSearch.AddTableField('ChargeRateCode', MntConst.eTypeCode, 'Key', 'Charge Rate', 3);
        this.prepChargeRateSearch.AddTableField('ChargeRateDesc', MntConst.eTypeText, 'Virtual', 'Charge Rate Desc', 20);
        this.prepChargeRateSearch.AddTableField('PrepPriceEffectDate', MntConst.eTypeDate, 'Required', 'Effect Date', 10);

    }
    public updateView(params?: any): void {
        this.inputParams = params;
        if (this.inputParams && this.inputParams['showAddNew'] === true) {
            this.showAddNew = true;
        } else {
            this.showAddNew = false;
        }
        this.windowOnload();
        this.loadTableData();
    }
    //When clicked on perticular row of the table, this method get executed
    public selectedData(event: any): void {
        let vntReturnData: any; let returnObj: Object;
        vntReturnData = event.row;
        returnObj = { 'ChargeRateCode': vntReturnData.ChargeRateCode, 'PrepPriceEffectDate': vntReturnData.PrepPriceEffectDate };
        if (!this.riExchange.riInputElement.GetValue(this.uiForm, 'PrepCode')) {
            returnObj = Object.assign(returnObj, { 'PrepCode': vntReturnData.PrepCode });
        }
        this.emitSelectedData(returnObj);
    }
    public onAddNew(data: number): void {
        let returnObj = {
            'AddMode': true
        };
        this.emitSelectedData(returnObj);
    }
}
