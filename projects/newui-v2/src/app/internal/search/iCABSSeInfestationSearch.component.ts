
import { Component, OnInit, Injector, OnDestroy, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { TableComponent } from './../../../shared/components/table/table';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { InternalMaintenanceServiceModuleRoutes } from '../../../app/base/PageRoutes';

@Component({
    templateUrl: 'iCABSSeInfestationSearch.html'
})

export class InfestationSearchComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('infestationSearchtable') infestationSearchtable: TableComponent;
    public itemsPerPage: string = '10';
    public page: number = 1;
    public pageId: string = '';
    public isDisplayMenu: boolean = false;
    public serviceVisitRowID: any = '';
    public controls = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, disabled: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeAutoNumber, disabled: true },
        { name: 'PremiseName', type: MntConst.eTypeTextFree, disabled: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'ProductDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'menu', type: '' }
    ];
    // URL Query Parameters
    public queryParams: any = {
        operation: 'Service/iCABSSeInfestationSearch',
        module: 'service',
        method: 'service-delivery/search'
    };
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEINFESTATIONSEARCH;
        this.pageTitle = 'Infestation Details';
        this.browserTitle = 'Infestation Search';

    }
    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnload();
        this.loadTableData();
    }
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    private windowOnload(): void {
        this.setControlValue('menu','');
        this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));

        this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));

        this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
        this.setControlValue('ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));
        this.serviceVisitRowID = this.riExchange.GetParentRowID(this.uiForm, 'ServiceVisit');

        if (!this.serviceVisitRowID)
            this.serviceVisitRowID = this.riExchange.getParentHTMLValue('ServiceVisit');
        this.setAttribute('ServiceVisitRowID', this.serviceVisitRowID);
        if (this.parentMode === 'ServiceVisit') {
            this.isDisplayMenu = true;
        }
    }
    //This function used to create table fields
    private buildTableColumns(): void {
        this.infestationSearchtable.clearTable();
        this.infestationSearchtable.AddTableField('InfestationNumber', MntConst.eTypeInteger, 'Key', 'Infestation Number', 10);
        this.infestationSearchtable.AddTableField('ProductDetailDesc', MntConst.eTypeText, 'Virtual', 'Detail', 20);
        this.infestationSearchtable.AddTableField('InfestationLevelDesc', MntConst.eTypeText, 'Virtual', 'Level', 20);
        this.infestationSearchtable.AddTableField('InfestationLocationDesc', MntConst.eTypeText, 'Virtual', 'Location', 20);
        this.infestationSearchtable.AddTableField('TreatmentPlanID', MntConst.eTypeText, 'Key', 'Treatment Plan', 10);

    }
    //This function used to load table data
    private loadTableData(): void {
        let search: QueryParams = new QueryParams(), searchParams: any = {};

        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set('search.sortby', 'InfestationNumber');
        search.set('ProductCode', this.getControlValue('ProductCode'));
        search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        search.set('ContractNumber', this.getControlValue('ContractNumber'));
        search.set('ServiceVisitRowID', this.serviceVisitRowID);

        searchParams.search = search;
        searchParams.module = this.queryParams.module;
        searchParams.method = this.queryParams.method;
        searchParams.operation = this.queryParams.operation;
        this.buildTableColumns();
        this.infestationSearchtable.loadTableData(searchParams);
    }
    //This function get executed when user click on the individual row
    public selectedData(event: any): void {
        let vntReturnData: any;
        switch (this.parentMode) {
            case 'ServiceVisit':
                vntReturnData = event.row;
                this.setAttribute('InfestationNumber', vntReturnData.InfestationNumber);
                this.setAttribute('InfestationROWID', vntReturnData.ttInfestation);
                this.navigate('Search', InternalMaintenanceServiceModuleRoutes.ICABSSEINFESTATIONMAINTENANCE);
                break;
            default:
                vntReturnData = event.row;
                if (this.parentMode === 'LookUp') {
                    this.riExchange.setParentHTMLValue('InfestationNumber', vntReturnData.InfestationNumber);
                } else {
                    this.riExchange.setParentHTMLValue('InfestationNumber', vntReturnData.InfestationNumber);
                }
                break;
        }
    }
    public menuOnchange(value: string): void {
        if (value === 'AddInfestation') {
            this.navigate('SearchAdd', InternalMaintenanceServiceModuleRoutes.ICABSSEINFESTATIONMAINTENANCE);
        }
    }

}
