import { Component, OnInit, ViewChild, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Router } from '@angular/router';

import { BehaviorSubject, Subscription } from 'rxjs';

import { PageIdentifier } from './../../base/PageIdentifier';
import { RiExchange } from './../../../shared/services/riExchange';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { Utils } from './../../../shared/services/utility';
import { HttpService } from './../../../shared/services/http-service';
import { AjaxObservableConstant } from './../../../shared/constants/ajax-observable.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { ModalAdvService } from './../../../shared/components/modal-adv/modal-adv.service';
import { InternalSearchModuleRoutes } from './../../base/PageRoutes';
import { AjaxConstant } from './../../../shared/constants/AjaxConstants';

@Component({
    templateUrl: 'riMUserSearch.html'
})

export class MUserSearchComponent extends SelectedDataEvent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private headerParams: any = {
        method: 'it-functions/ri-model',
        module: 'user',
        operation: 'Model/riMUserSearch'
    };
    private ajaxSource = new BehaviorSubject<any>(0);
    private ajaxSource$;
    private messageProperties: ICabsModalVO = new ICabsModalVO();
    public inputParams: any = {
        showAddNew: false
    };
    private ajaxSubscription: Subscription;

    public pageId: string;
    public pageTitle: string;
    public controls: any = {
        FilterSelect: [{ value: 'UserCode' }],
        InactiveUsers: [],
        Begins: [],
        NonRelatedEmployee: []
    };
    public uiForm: FormGroup;
    public gridParams: any = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 0,
        cacheRefresh: false,
        gridHandle: 0
    };
    public isRequesting: boolean = false;

    constructor(private riExchange: RiExchange,
        private formBuilder: FormBuilder,
        private serviceConstants: ServiceConstants,
        private utils: Utils,
        private httpService: HttpService,
        private modalAdvService: ModalAdvService,
        private ajaxconstant: AjaxObservableConstant,
        private router: Router,
        private zone: NgZone
    ) {
        super();
        this.pageId = PageIdentifier.RIMUSERSEARCH;
        this.pageTitle = 'User Search';
        this.ajaxSource$ = this.ajaxSource.asObservable();

        this.ajaxSubscription = this.ajaxSource$.subscribe(event => {
            if (event !== 0) {
                this.zone.run(() => {
                    switch (event) {
                        case AjaxConstant.START:
                            this.isRequesting = true;
                            break;
                        case AjaxConstant.COMPLETE:
                            this.isRequesting = false;
                            break;
                    }
                });
            }
        });
    }

    public ngOnInit(): void {
        this.ajaxSource$ = this.ajaxSource.asObservable();
        this.uiForm = this.formBuilder.group(this.controls);

        if ('/' + InternalSearchModuleRoutes.RIMUSERSEARCH + '?fromMenu=true' === this.router.url) {
            this.utils.setTitle('User Search');
        }
    }

    public ngAfterViewInit(): void {
        this.riExchange.riInputElement.SetValue(this.uiForm, 'FilterSelect', 'UserCode');

        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = this.gridParams.pageSize;

        this.buildGrid();
    }

    public ngOnDestroy(): void {
        if (this.ajaxSubscription) {
            this.ajaxSubscription.unsubscribe();
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('UserCode', 'UserCode', 'UserCode', MntConst.eTypeText, 20, true);
        this.riGrid.AddColumnAlign('UserCode', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('UserName', 'UserName', 'UserName', MntConst.eTypeText, 40, true);
        this.riGrid.AddColumnAlign('UserName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('Email', 'Email', 'Email', MntConst.eTypeText, 40, false);
        this.riGrid.AddColumnAlign('Email', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('UserTypeCode', 'UserTypeCode', 'UserTypeCode', MntConst.eTypeText, 10, false);
        this.riGrid.AddColumnAlign('UserTypeCode', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('Active', 'Active', 'Active', MntConst.eTypeImage, 5, false);
        this.riGrid.AddColumnAlign('UserTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Employee', 'Employee', 'Employee', MntConst.eTypeText, 20, false);
        this.riGrid.AddColumnAlign('Employee', MntConst.eAlignmentLeft);

        this.riGrid.Complete();
    }

    private loadGridData(): void {
        let searchParams: QueryParams = new QueryParams();

        searchParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        searchParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set('FilterSelect', this.riExchange.riInputElement.GetValue(this.uiForm, 'FilterSelect'));
        searchParams.set('InactiveUsers', this.riExchange.riInputElement.GetValue(this.uiForm, 'InactiveUsers') || '');
        searchParams.set('NonRelatedEmployee', this.riExchange.riInputElement.GetValue(this.uiForm, 'NonRelatedEmployee') || '');
        searchParams.set('Begins', this.riExchange.riInputElement.GetValue(this.uiForm, 'Begins') || '');
        searchParams.set(this.serviceConstants.GridMode, '0');
        searchParams.set(this.serviceConstants.GridHandle, this.gridParams.gridHandle);
        searchParams.set(this.serviceConstants.GridPageSize, this.riGrid.PageSize.toString());
        searchParams.set(this.serviceConstants.PageCurrent, this.gridParams.currentPage);
        searchParams.set(this.serviceConstants.GridHeaderClickedColumn, '');
        searchParams.set(this.serviceConstants.GridSortOrder, this.riGrid.DescendingSort ? 'Descending' : 'Ascending');
        if (this.gridParams.cacheRefresh) {
            searchParams.set(this.serviceConstants.GridCacheRefresh, this.gridParams.cacheRefresh);
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrGet(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            searchParams).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage) {
                    this.displayMessage('Error', data.errorMessage, data.fullError);
                    return;
                }
                this.gridParams.currentPage = data.pageData.pageNumber;
                this.gridParams.totalRecords = data.pageData.lastPageNumber * 10 || 10;
                this.riGrid.Execute(data);
            }).catch(error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage('Error', error.errorMessage, error.fullError || '');
            });
    }

    private displayMessage(type: string, message: string, fullError?: string): void {
        this.messageProperties = new ICabsModalVO();
        this.messageProperties.msg = message;
        this.messageProperties.fullError = fullError;

        this.modalAdvService['emit' + type](this.messageProperties);
    }

    public onGridRefresh(): void {
        this.gridParams.cacheRefresh = true;
        this.gridParams.gridHandle = this.utils.randomSixDigitString();
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    public onCurrentPageRecieved(data: any): void {
        this.gridParams.currentPage = data.value;
        this.gridParams.cacheRefresh = false;
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    public onBodyDoubleClick(): void {
        let returnData: any = {};
        if (!this.inputParams.hasOwnProperty('parentMode')) {
            return;
        }

        switch (this.inputParams.parentMode) {
            case 'LookUp':
                returnData['UserCode'] = this.riGrid.Details.GetValue('UserCode');
                break;
            case 'Search':
                returnData['UserCode'] = this.riGrid.Details.GetValue('UserCode');
                returnData['UserName'] = this.riGrid.Details.GetValue('UserName');
                break;
            case 'Search2':
                returnData['UserCode2'] = this.riGrid.Details.GetValue('UserCode');
                returnData['UserName'] = this.riGrid.Details.GetValue('UserName');
                break;
            case 'GeoLocSearch':
                returnData['GeoLocCreatedUserCodeFilter'] = this.riGrid.Details.GetValue('UserCode');
                break;
        }

        this.emitSelectedData(returnData);
    }

    public updateView(params: any): void {
        if (params.hasOwnProperty('parentMode')) {
            this.inputParams['parentMode'] = params.parentMode;
        }

        if (params.hasOwnProperty('showAddNew')) {
            this.inputParams['showAddNew'] = params.showAddNew;
        }
    }

    public onAddNew(): void {
        this.emitSelectedData('AddModeOn');
    }
}
