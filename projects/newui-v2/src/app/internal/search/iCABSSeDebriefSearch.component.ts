import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Utils } from './../../../shared/services/utility';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { EmployeeSearchComponent } from '../../../app/internal/search/iCABSBEmployeeSearch';
import { TableComponent } from './../../../shared/components/table/table';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';
import { RiExchange } from '../../../shared/services/riExchange';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from './../../base/PageIdentifier';

@Component({
    templateUrl: 'iCABSSeDebriefSearch.html'
})

export class DebriefSearchComponent extends SelectedDataEvent implements OnInit, OnDestroy {
    @ViewChild('riTable') riTable: TableComponent;

    private riExchange: RiExchange;
    private utils: Utils;
    private serviceConstants: ServiceConstants;
    private formBuilder: FormBuilder;
    private queryParams: any = {
        operation: 'Service/iCABSSeDebriefSearch',
        module: 'sam',
        method: 'service-delivery/search'
    };
    public uiForm: FormGroup;
    public pageId: string = '';
    public controls: any = [
        { name: 'EmployeeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: 'true', type: MntConst.eTypeText }
    ];
    public ellipsisConfig: any = {
        employee: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: EmployeeSearchComponent
        }
    };
    public isEmployeeDisp: boolean = true;
    public parentMode: any;
    public itemsPerPage: number = 10;
    public page: number = 1;

    constructor(injector: Injector) {
        super();
        this.injectService(injector);
        this.pageId = PageIdentifier.ICABSSEDEBRIEFSEARCH;
    }

    ngOnInit(): void {
        this.uiForm = this.formBuilder.group({});
        this.riExchange.renderForm(this.uiForm, this.controls);
    }

    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
        this.formBuilder = null;
        this.riExchange = null;
    }

    public injectService(injector: Injector): void {
        this.formBuilder = injector.get(FormBuilder);
        this.utils = injector.get(Utils);
        this.serviceConstants = injector.get(ServiceConstants);
        this.riExchange = injector.get(RiExchange);
    }

    public updateView(params: any): void {
        this.parentMode = params.parentMode;
        switch (params.parentMode) {
            case 'LookUp-SAM':
                this.riExchange.riInputElement.SetValue(this.uiForm, 'EmployeeCode', params.EmployeeCode ? params.EmployeeCode : '' );
                this.riExchange.riInputElement.Disable(this.uiForm, 'EmployeeCode');
                this.isEmployeeDisp = false;
                break;
            default:
                this.isEmployeeDisp = true;
                break;
        }
        this.riExchange.riInputElement.Disable(this.uiForm, 'EmployeeSurname');
        this.buildTable();
    }

    public buildTable(): void {
        this.riTable.clearTable();
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (this.riExchange.riInputElement.GetValue(this.uiForm, 'EmployeeCode')) {
            search.set(this.serviceConstants.EmployeeCode, this.riExchange.riInputElement.GetValue(this.uiForm, 'EmployeeCode'));
        } else {
            this.riTable.AddTableField('EmployeeCode', MntConst.eTypeCode, 'Required', 'Employee', 6);
            this.riTable.AddTableField('EmployeeSurname', MntConst.eTypeText, 'Virtual', 'Employee Name', 25);
        }
        this.riTable.AddTableField('DebriefNumber', MntConst.eTypeInteger, 'Key', 'Debrief Number', 7);
        this.riTable.AddTableField('DebriefFromDate', MntConst.eTypeDate, 'Required', 'From', 10);
        this.riTable.AddTableField('DebriefDate', MntConst.eTypeDate, 'Required', 'To', 10);
        this.riTable.AddTableField('DebriefTime', MntConst.eTypeTime, 'Required', 'Time', 10);

        let inputParams: any = {};
        inputParams.search = search;
        inputParams.module = this.queryParams.module;
        inputParams.method = this.queryParams.method;
        inputParams.operation = this.queryParams.operation;
        this.riTable.loadTableData(inputParams);
    }

    public onRiTableBodyRecordSelected(event: any): void {
        let returnObj: any;
        if (this.parentMode === 'LookUp-SAM') {
            returnObj = {
                'DebriefNumber': event.row.DebriefNumber,
                'DebriefFromDate': event.row.DebriefFromDate,
                'DebriefToDate': event.row.DebriefDate
            };
        }
        else {
            returnObj = {
                'DebriefNumber': event.row.DebriefNumber
            };
        }
        this.emitSelectedData(returnObj);
    }

    public onEmployeeDataReceived(data: any): void {
        if (data) {
            this.riExchange.riInputElement.SetValue(this.uiForm, 'EmployeeCode', data.EmployeeCode);
            this.riExchange.riInputElement.SetValue(this.uiForm, 'EmployeeSurname', data.EmployeeSurname);
        }
    }
}
