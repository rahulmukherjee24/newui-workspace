import { Component, OnInit, Injector, ViewChild, OnDestroy, Renderer, ElementRef, ChangeDetectorRef } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { EmployeeSearchComponent } from '../../internal/search/iCABSBEmployeeSearch';


@Component({
    templateUrl: 'iCABSSeFollowUpGrid.html'
})

export class FollowUpGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('employeeCode') employeeCode: ElementRef;

    private queryParams: any = {
        operation: 'Service/iCABSSeFollowUpGrid',
        module: 'follow-ups',
        method: 'service-planning/grid'
    };
    public pageId: string = '';
    public controls = [
        { name: 'BranchNumber', disabled: true, required: true, type: MntConst.eTypeInteger },
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'EmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'PostcodeFilter', type: MntConst.eTypeCode },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate }
    ];
    public gridConfig = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 1,
        action: '2'
    };
    public ellipsisConfig = {
        employee: {
            isShowCloseButton: true,
            isShowHeader: true,
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: EmployeeSearchComponent
        }
    };

    constructor(injector: Injector, private _renderer: Renderer, private ref: ChangeDetectorRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEFOLLOWUPGRID;
        this.browserTitle = 'Follow Up Report';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageTitle = 'Follow Up Report';
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        let branchCodeWithDesc: string = this.utils.getBranchText(this.utils.getBranchCode());
        let index: number = branchCodeWithDesc.indexOf('-');
        let branchDesc: string = branchCodeWithDesc.substring(index + 1);
        this.setControlValue('BranchName', branchDesc);
        this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(new Date(new Date().getFullYear(), new Date().getMonth(), 1)));
        this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)));
        this._renderer.invokeElementMethod(this.employeeCode.nativeElement, 'focus', []);
        this.buildGrid();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.AddColumn('EmployeeCode', 'FollowUp', 'EmployeeCode', MntConst.eTypeCode, 6, false);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('EmployeeCode', true);
        this.riGrid.AddColumn('EmployeeName', 'FollowUp', 'EmployeeName', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('FollowUpDate', 'FollowUp', 'FollowUpDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('FollowUpDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('FollowUpDate', true);
        this.riGrid.AddColumn('CustomerName', 'FollowUp', 'CustomerName', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('Postcode', 'FollowUp', 'Postcode', MntConst.eTypeTextFree, 10);
        this.riGrid.AddColumnOrderable('Postcode', true);
        this.riGrid.AddColumn('ContractNumber', 'FollowUp', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseNumber', 'FollowUp', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ProductCode', 'FollowUp', 'ProductCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (this.getControlValue('DateFrom') && this.getControlValue('DateTo')) {
            let gridSearch: QueryParams = this.getURLSearchParamObject();
            gridSearch.set('BranchNumber', this.getControlValue('BranchNumber'));
            gridSearch.set('PostcodeFilter', this.getControlValue('PostcodeFilter'));
            gridSearch.set('DateFrom', this.globalize.parseDateToFixedFormat(this.getControlValue('DateFrom')).toString());
            gridSearch.set('DateTo', this.globalize.parseDateToFixedFormat(this.getControlValue('DateTo')).toString());
            gridSearch.set('EmployeeCode', this.getControlValue('EmployeeCode'));
            let sortOrder = 'Descending';
            if (!this.riGrid.DescendingSort) {
                sortOrder = 'Ascending';
            }
            gridSearch.set(this.serviceConstants.GridMode, '0');
            gridSearch.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
            gridSearch.set(this.serviceConstants.PageSize, this.gridConfig.pageSize.toString());
            gridSearch.set(this.serviceConstants.PageCurrent, this.gridConfig.currentPage.toString());
            gridSearch.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
            gridSearch.set(this.serviceConstants.GridSortOrder, sortOrder);
            gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());
            this.ajaxSource.next(this.ajaxconstant.START);
            this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridSearch)
                .subscribe(
                    (e) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (e.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        } else {
                            this.riGrid.RefreshRequired();
                            this.riGridPagination.currentPage = this.gridConfig.currentPage = e.pageData ? e.pageData.pageNumber : 1;
                            this.riGridPagination.totalItems = this.gridConfig.totalRecords = e.pageData ? e.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                            this.riGrid.Execute(e);
                            this.ref.detectChanges();
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }

    // Refresh the grid data on user click
    public onRiGridRefresh(): void {
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    // Callback to retrieve the current page on user clicks
    public getCurrentPage(currentPage: any): void {
        this.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh();
    }

    // Handles grid sort functionality
    public riGridSort(event: any): void {
        this.onRiGridRefresh();
    }

    public onEmployeeDataReceived(data: any): void {
        if (data) {
            this.setControlValue('EmployeeCode', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
    }

    public onEmployeeCodeChange(event: any): void {
        if (!event.srcElement.value) {
            this.setControlValue('EmployeeSurname', '');
        } else {
            let employeeSearchParams: QueryParams = this.getURLSearchParamObject();
            employeeSearchParams.set(this.serviceConstants.Action, '6');
            let formData: any = {};
            formData['Function'] = 'GetEmployeeSurname';
            formData['EmployeeCode'] = event.srcElement.value;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, employeeSearchParams, formData).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    } else {
                        this.setControlValue('EmployeeSurname', e.EmployeeSurname);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }
}
