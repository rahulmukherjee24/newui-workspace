/**
 * The SeServiceAreaDetailGridComponent  to display service area grid
 * @author icabs ui team
 * @version 1.0
 * @since   01/03/2017
 */
import { OnInit, Injector, Component, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { BranchServiceAreaSearchComponent } from './../../internal/search/iCABSBBranchServiceAreaSearch';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { DropdownStaticComponent } from './../../../shared/components/dropdown-static/dropdownstatic';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { BIReportsRoutes, InternalGridSearchSalesModuleRoutes, InternalMaintenanceServiceModuleRoutes } from '@app/base/PageRoutes';
import { DatepickerComponent } from './../../../shared/components/datepicker/datepicker';
import { VariableService } from './../../../shared/services/variable.service';
import { IExportOptions } from '@app/base/ExportConfig';

@Component({
    templateUrl: 'iCABSSeServiceAreaDetailGrid.html'
})

// Class definition starts here
export class SeServiceAreaDetailGridComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('selectDayDropdown') selectDayDropdown: DropdownStaticComponent;
    @ViewChild('portfolioStatusTypeDropdown') portfolioStatusTypeDropdown: DropdownStaticComponent;
    @ViewChild('entitlementProdeDropdown') entitlementProdeDropdown: DropdownStaticComponent;
    @ViewChild('menuDropdown') menuDropdown: DropdownStaticComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('effectiveDateChild') public effectiveDateChild: DatepickerComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private systemChars: Object = {
        SCEnableEntitlement: false,
        SCEnableWED: false
    };
    private isBApplyChange: boolean = false;
    private isBCheckFrequency: boolean = false;
    private isBlnGridHasData: boolean = false;
    private gridCacheRefresh: string = 'True';
    private gridSubscription: Subscription;
    private queryParams: any = {
        operation: 'Service/iCABSSeServiceAreaDetailGrid',
        module: 'structure',
        method: 'service-planning/grid'
    };

    public canDeactivateObservable: Observable<boolean>;
    public branchServiceAreaSearchComponent: any = BranchServiceAreaSearchComponent;
    public selectContractTypeCodeOptions: Array<any> = [];
    public menuOptions: Array<any> = [];
    public fieldDisabled: any = {
        SelectContractTypeCode: false,
        LastChangeEffectDate: false,
        menu: false,
        PortfolioStatusType: false,
        EntitlementProd: false
    };
    public portfolioStatusTypeOptions: Array<any> = [];
    public entitlementProdOptions: Array<any> = [];
    public hideObj: any = {
        trEntitlement: true,
        LastChangeEffectDate: true,
        TotalWEDs: false,
        tdSaveRequired: true,
        tdErrors: true,
        tdNoChanges: true,
        trDisplayErrors: true,
        ExportChanges: false,
        ImportChanges: false,
        ApplyChanges: false
    };
    public fieldRequired: any = {
        LastChangeEffectDate: false
    };
    public currDate: any = null;
    public pageSize = 10;
    public totalRecords: number = 0;
    public pageId: string = '';
    public inputParamsBranchServiceAreaSearch: any = {
        parentMode: 'LookUp-Emp'
    };
    public curPage: number = 1;
    public exportConfig: IExportOptions = {};
    public controls = [
        { name: 'BranchServiceAreaCode', disabled: false, value: '', required: false, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: false, value: '', required: false, type: MntConst.eTypeText },
        { name: 'ContractTypeCode', disabled: false, value: '', required: false, type: MntConst.eTypeText },
        { name: 'PremisePostcode', disabled: false, value: '', required: false, type: MntConst.eTypeCode },
        { name: 'PortfolioStatusType', disabled: false, value: '', required: false, type: MntConst.eTypeCode },
        { name: 'PremiseTown', disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'EntitlementProd', disabled: false, value: '', required: false, type: MntConst.eTypeText },
        { name: 'SeqNumberFrom', disabled: false, value: '', required: false, type: MntConst.eTypeInteger },
        { name: 'DisplayErrors', disabled: false, value: false, required: false },
        { name: 'TotalPremises', disabled: false, value: '', required: false, type: MntConst.eTypeInteger },
        { name: 'TotalExchanges', disabled: false, value: '', required: false, type: MntConst.eTypeInteger },
        { name: 'TotalHours', disabled: false, value: '', required: false, type: MntConst.eTypeText },
        { name: 'TotalWEDs', disabled: false, value: '', required: false, type: MntConst.eTypeDecimal1 },
        { name: 'LastChangeEffectDate', value: '', disabled: false, required: false, type: MntConst.eTypeDate },
        { name: 'UploadedError', disabled: false, value: '', required: false },
        { name: 'FileUploaded', disabled: false, value: '', required: false },
        { name: 'riGridHandle', disabled: false, value: '', required: false }
    ];

    //Lifecycle/Constructor methods definition
    constructor(injector: Injector, private el: ElementRef, private variableService: VariableService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEAREADETAILGRID;
        this.browserTitle = this.pageTitle = 'Service Area Detail';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.setSystemCharacters();
    }
    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.gridSubscription) {
            this.gridSubscription.unsubscribe();
        }
    }

    /**
     * This method used to set system characters in Component variables
     * @param void
     * @return void
     */
    private setSystemCharacters(): void {
        let sysCharList: Array<number> = [
            this.sysCharConstants.SystemCharEnableEntitlement,
            this.sysCharConstants.SystemCharEnableWED
        ];
        let sysNumbers: string = sysCharList.join(',');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.fetchSysChar(sysNumbers).subscribe((data) => {
            try {
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    this.systemChars['SCEnableEntitlement'] = data.records[0].Required;
                    this.systemChars['SCEnableWED'] = data.records[1].Required;
                }
            } catch (error) {
                this.logger.warn(error);
            }
            this.initData();
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    /**
     * This method to service call for system characters
     * @param any
     * @return any observable to subsribe system characters response
     */
    private fetchSysChar(sysCharNumbers: any): any {
        let querySysChar: QueryParams = this.getURLSearchParamObject();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.SystemCharNumber, sysCharNumbers);
        return this.httpService.sysCharRequest(querySysChar);
    }

    /**
     * This method is used to initialize all parameters during page load
     * @param void
     * @return void
     */
    private initData(): void {
        this.setCurrentContractType();
        this.buildMenuOptions();
        this.createOtherOptions();
        this.hideObj['trEntitlement'] = true;
        if (this.systemChars['SCEnableEntitlement'] === true) {
            this.hideObj['trEntitlement'] = false;
        }
        this.disableControl('EmployeeSurname', true);
        this.disableControl('TotalExchanges', true);
        this.disableControl('TotalPremises', true);
        this.disableControl('TotalHours', true);
        this.disableControl('TotalWEDs', true);
        this.hideObj['TotalWEDs'] = true;
        if (this.systemChars['SCEnableWED'] === true) {
            this.hideObj['TotalWEDs'] = false;
        }
        this.el.nativeElement.querySelector('#BranchServiceAreaCode').focus();
        this.branchServiceAreaCodeOndeactivate();
        this.buildMenu();
        this.buildGrid();
        if (this.isReturning()) {
            this.refresh();
        }
        this.isBApplyChange = false;
        this.setControlValue('riGridHandle', this.utils.randomSixDigitString());
    }

    private setCurrentContractType(): void {
        this.pageParams.currentContractType = this.riExchange.getCurrentContractType();
        this.pageParams.currentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
    }

    /**
     * This method is used to populate ContractTypeCode dropdown from service call
     * @param void
     * @return void
     */
    private buildMenuOptions(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let postData: Object = {}, businessContractArray: Array<string> = [];
        postData[this.serviceConstants.Function] = 'GetBusinessContractTypes';
        search.set(this.serviceConstants.Action, '6');
        this.selectContractTypeCodeOptions.push({ value: 'All', text: 'All' });
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    businessContractArray = data.BusinessContractTypes.split(',');
                    for (let cType of businessContractArray) {
                        switch (cType) {
                            case 'C':
                                this.selectContractTypeCodeOptions.push({ value: 'C', text: 'Contracts' });
                                break;
                            case 'J':
                                this.selectContractTypeCodeOptions.push({ value: 'J', text: 'Jobs' });
                                break;
                            case 'P':
                                this.selectContractTypeCodeOptions.push({ value: 'P', text: 'Product Sales' });
                                break;
                        }
                    }
                    if (this.isReturning() === false) {
                        this.pageParams.ContractTypeCode = 'C';
                        this.setControlValue('ContractTypeCode', 'C');
                    }
                    this.selectDayDropdown.selectedItem = this.pageParams.ContractTypeCode;
                    this.contractTypeCodeOnchange();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is used to configure grid(add columns/alignment setting etc)
     * @param void
     * @return void
     */
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('BranchServiceArea', 'ServiceCover', 'AccountNumber', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'ServiceCover', 'BranchServiceAreaSeqNo', MntConst.eTypeText, 7, true);
        this.riGrid.AddColumn('Postcode', 'ServiceCover', 'Postcode', MntConst.eTypeCode, 9, true);
        this.riGrid.AddColumn('PremiseAddressLine4', 'ServiceCover', 'PremiseAddressLine4', MntConst.eTypeCode, 30);
        this.riGrid.AddColumn('ContractPremise', 'ServiceCover', 'ContractPremise', MntConst.eTypeText, 16);
        this.riGrid.AddColumn('PremiseName', 'ServiceCover', 'PremiseName', MntConst.eTypeText, 14);
        this.riGrid.AddColumn('PremiseAddressLine1', 'ServiceCover', 'PremiseAddressLine1', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('PremiseContactName', 'ServiceCover', 'PremiseContactName', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('PremiseTelephoneNumber', 'ServiceCover', 'PremiseTelephoneNumber', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('ProductCode', 'ServiceCover', 'ProductCode', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumn('ServiceCoverNumber', 'ServiceCover', 'ServiceCoverNumber', MntConst.eTypeInteger, 7);
        this.riGrid.AddColumn('ServiceDetail', 'ServiceCover', 'ServiceDetail', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('ServiceVisitFrequency', 'ServiceCover', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ServiceQuantity', 'ServiceCover', 'ServiceQuantity', MntConst.eTypeInteger, 5);
        if (this.systemChars['SCEnableWED'] === true) {
            this.riGrid.AddColumn('WEDValue', 'ServiceCover', 'WEDValue', MntConst.eTypeDecimal1, 5);
        }
        this.riGrid.AddColumn('StandardTreatmentTime', 'ServiceCover', 'StandardTreatmentTime', MntConst.eTypeTime, 8);
        this.riGrid.AddColumn('VisitValue', 'ServiceCover', 'VisitValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumn('ServiceVisitAnnivDate', 'ServiceCover', 'ServiceVisitAnnivDate', MntConst.eTypeDate, 5);
        this.riGrid.AddColumn('ServiceTypeCode', 'ServiceCover', 'ServiceTypeCode', MntConst.eTypeCode, 5);
        this.riGrid.AddColumn('TermDate', 'ServiceCover', 'TermDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('AnnualCalendarTemplateNumber', 'ServiceCover', 'AnnualCalendarTemplateNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ClosedCalendarTemplateNumber', 'ServiceCover', 'ClosedCalendarTemplateNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('StaticVisitCount', 'ServiceCover', 'StaticVisitCount', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('PlannedVisitCount', 'ServiceCover', 'PlannedVisitCount', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitDifference', 'ServiceCover', 'VisitDifference', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ServiceSpecialInstructions', 'ServiceCover', 'ServiceSpecialInstructions', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('Uploaded', 'ServiceCover', 'Uploaded', MntConst.eTypeImage, 1);
        this.riGrid.AddColumn('ErrorDesc', 'ServiceCover', 'ErrorDesc', MntConst.eTypeText, 30);

        this.riGrid.AddColumnAlign('BranchServiceArea', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Postcode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('PremiseAddressLine4', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('ContractPremise', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('PremiseAddressLine1', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('PremiseTelephoneNumber', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('ServiceCoverNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceDetail', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);

        if (this.systemChars['SCEnableWED'] === true) {
            this.riGrid.AddColumnAlign('WEDValue', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumnAlign('StandardTreatmentTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitValue', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceVisitAnnivDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('AnnualCalendarTemplateNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ClosedCalendarTemplateNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('StaticVisitCount', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PlannedVisitCount', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitDifference', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('TermDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceSpecialInstructions', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('Uploaded', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ErrorDesc', MntConst.eAlignmentLeft);

        this.riGrid.AddColumnScreen('PremiseContactName', false);
        this.riGrid.AddColumnScreen('PremiseTelephoneNumber', false);
        this.riGrid.AddColumnScreen('ServiceCoverNumber', false);
        this.riGrid.AddColumnScreen('ServiceDetail', false);
        this.riGrid.AddColumnScreen('StandardTreatmentTime', false);
        this.riGrid.AddColumnScreen('VisitValue', false);
        this.riGrid.AddColumnScreen('ServiceTypeCode', false);
        this.riGrid.AddColumnScreen('StaticVisitCount', false);
        this.riGrid.AddColumnScreen('PlannedVisitCount', false);
        this.riGrid.AddColumnScreen('VisitDifference', false);
        this.riGrid.AddColumnScreen('TermDate', false);
        this.riGrid.AddColumnScreen('ServiceSpecialInstructions', false);
        this.riGrid.AddColumnScreen('ErrorDesc', false);

        this.riGrid.AddColumnOrderable('BranchServiceArea', true);
        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);
        this.riGrid.AddColumnOrderable('Postcode', true);
        this.riGrid.AddColumnOrderable('ContractPremise', true);
        this.riGrid.AddColumnOrderable('ServiceVisitFrequency', true, true);
        this.riGrid.AddColumnOrderable('PremiseAddressLine4', true);
        this.riGrid.AddColumnOrderable('ServiceVisitAnnivDate', true);
        this.riGrid.AddColumnOrderable('PremiseName', true);
        this.riGrid.AddColumnOrderable('ProductCode', true);
        this.riGrid.Complete();

        this.exportConfig = {};
        this.dateCollist = this.riGrid.getColumnIndexListFromFull([
            'ServiceVisitAnnivDate',
            'TermDate'
        ]);
        if (this.dateCollist.length) {
            this.exportConfig.dateColumns = this.dateCollist;
        }
    }

    /**
     * This method is used to enable filters
     * @param void
     * @return void
     */
    private activateFilters(): void {
        this.disableControl('PremiseTown', false);
        this.disableControl('SeqNumberFrom', false);
    }

    /**
     * This method is used to build Menu
     * @param void
     * @return void
     */
    private buildMenu(): void {
        this.menuOptions = [{ text: 'Options', value: '' }];
        switch (this.getControlValue('ContractTypeCode')) {
            case 'All':
            case 'C':
            case 'J':
                this.menuOptions.push({ text: 'Static Visits', value: 'StaticVisits' });
                break;
        }
        this.menuOptions.push({ text: 'Planned Visits', value: 'PlanVisits' });
        //Not in icabsnewui scope as per TCs
        this.menuOptions.push({ text: 'Area Static Visits', value: 'AreaStaticVisits' });
        //this.menuOptions.push({ text: 'Visit Variance', value: 'VisitVariance' });
    }

    /**
     * This method is used to build createOther Options
     * @param void
     * @return void
     */
    private createOtherOptions(): void {
        this.portfolioStatusTypeOptions.push({ text: 'All', value: 'All' });
        this.portfolioStatusTypeOptions.push({ text: 'Current Portfolio', value: 'Current' });
        this.portfolioStatusTypeOptions.push({ text: 'Non Current Portfolio', value: 'NonCurrent' });
        if (this.isReturning() === false) {
            this.pageParams.PortfolioStatusType = 'Current';
            this.setControlValue('PortfolioStatusType', 'Current');
        }
        this.portfolioStatusTypeDropdown.selectedItem = this.pageParams.PortfolioStatusType;

        this.entitlementProdOptions.push({ text: 'Include', value: 'Include' });
        this.entitlementProdOptions.push({ text: 'Exclude', value: 'Exclude' });
        if (this.isReturning() === false) {
            this.pageParams.EntitlementProd = 'Exclude';
            this.setControlValue('EntitlementProd', 'Exclude');
        }
        this.entitlementProdeDropdown.selectedItem = this.pageParams.EntitlementProd;
    }

    private serviceCoverFocus(rsrcElement: any): void {
        let oTR: any = rsrcElement.parentElement.parentElement.parentElement, baseColumn: number = 0;
        rsrcElement.focus();
        switch (this.riGrid.CurrentColumnName) {
            case 'Postcode':
                this.attributes['PremiseRowID'] = oTR.children[baseColumn + 2].children[0].getAttribute('RowID');
                break;
            default:
                this.attributes['ServiceCoverRowID'] = oTR.children[baseColumn + 7].children[0].getAttribute('RowID');
                break;
        }
        this.attributes['RowIndex'] = oTR.sectionRowIndex;
        switch (oTR.children[3].getAttribute('AdditionalProperty')) {
            case 'C':
                this.pageParams['CurrentContractTypeURLParameter'] = '';
                this.pageParams['currentContractType'] = 'C';
                break;
            case 'J':
                this.pageParams['CurrentContractTypeURLParameter'] = '<job>';
                this.pageParams['currentContractType'] = 'J';
                break;
            case 'P':
                this.pageParams['CurrentContractTypeURLParameter'] = '<product>';
                this.pageParams['currentContractType'] = 'P';
                break;
        }
    }

    /**
     * This method is used to capture last date change value
     * @param dtObj date picker selected value
     * @return void
     */
    public effectiveDateSelectedValue(dtObj: any): void {
        this.isBCheckFrequency = false;
        this.setControlValue('LastChangeEffectDate', dtObj.value);
        if (dtObj.value) {
            switch (this.getControlValue('UploadedError')) {
                case 'Continue':
                    this.hideObj['tdSaveRequired'] = false;
                    this.hideObj['tdErrors'] = true;
                    this.hideObj['trDisplayErrors'] = true;
                    this.hideObj['tdNoChanges'] = true;
                    this.hideObj['LastChangeEffectDate'] = false;
                    break;
                case 'Error':
                    this.hideObj['tdSaveRequired'] = true;
                    this.hideObj['tdErrors'] = false;
                    this.hideObj['trDisplayErrors'] = false;
                    this.hideObj['tdNoChanges'] = true;
                    this.hideObj['LastChangeEffectDate'] = true;
                    this.setControlValue('LastChangeEffectDate', '');
                    break;
                default:
                    this.hideObj['tdSaveRequired'] = true;
                    this.hideObj['tdErrors'] = true;
                    this.hideObj['trDisplayErrors'] = true;
                    this.hideObj['tdNoChanges'] = false;
                    this.hideObj['LastChangeEffectDate'] = true;
                    this.setControlValue('LastChangeEffectDate', '');
                    break;
            }
        }

    }

    /**
     * This method is used to capture service area
     * @param value BranchServiceAreaCode object from ellipsis
     * @return void
     */
    public onBranchServiceArea(obj: any): void {
        this.setControlValue('BranchServiceAreaCode', obj.BranchServiceAreaCode);
        this.setControlValue('EmployeeSurname', obj.EmployeeSurname);
    }

    /**
     * This method is called onblur BranchServiceAreaCode
     * @param void
     * @return void
     */
    public branchServiceAreaCodeOndeactivate(): void {
        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.setControlValue('EmployeeSurname', '');
        }
        this.activateFilters();
        this.buildGrid();
    }

    /**
     * This method is used to capture ContractTypeCode
     * @param ctypeObjVal ContractTypeCode selected value
     * @return void
     */
    public selectContractTypeCodeOptionsChange(ctypeObjVal: any): void {
        this.setControlValue('ContractTypeCode', ctypeObjVal);
        this.pageParams.ContractTypeCode = ctypeObjVal;
        this.contractTypeCodeOnchange();
    }

    /**
     * This method is called on change of menu options
     * @param mObjVal menu selected value
     * @return void
     */
    public menuOptionsChange(mObjVal: any): void {
        switch (mObjVal) {
            case 'AreaStaticVisits':
                this.navigate('BranchServiceArea', BIReportsRoutes.ICABSSESTATICVISITGRIDYEARBRANCH,
                    {
                        BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
                        EmployeeSurname: this.getControlValue('EmployeeSurname')
                    });
                break;
            case 'StaticVisits':
                this.navigate('BranchServiceArea', InternalGridSearchSalesModuleRoutes.ICABSSESERVICEPATTERNGRID, { StaticVisits: 'yes' });
                break;
            case 'PlanVisits':
                this.navigate('BranchServiceArea', InternalGridSearchSalesModuleRoutes.ICABSSESERVICEPATTERNGRID);
                break;
            case 'VisitVariance':
                this.modalAdvService.emitMessage(new ICabsModalVO('/wsscripts/riHTMLWrapper.p?riFileName=Service/iCABSSeVisitVarianceGrid.htm' + MessageConstant.Message.PageNotDeveloped));
                break;
        }
    }

    /**
     * This method is called refresh button click
     * @param void
     * @return void
     */
    public refresh(): void {
        this.riGridBeforeExecute();
    }

    /**
     * This method is called pagination click
     * @param currentPage is the current page number
     * @return void
     */
    public getCurrentPage(currentPage: any): void {
        this.curPage = currentPage.value;
        this.riGrid.RefreshRequired();
        this.gridCacheRefresh = 'False';
        this.riGridBeforeExecute();
    }

    /**
     * This method is used to capture PortfolioStatusType
     * @param statusType PortfolioStatusType selected value
     * @return void
     */
    public portfolioStatusTypeOptionsChange(statusType: any): void {
        this.setControlValue('PortfolioStatusType', statusType);
        this.pageParams.PortfolioStatusType = statusType;
    }

    /**
     * This method is used to capture EntitlementProd
     * @param entitlementProd EntitlementProd selected value
     * @return void
     */
    public entitlementProdOptionsChange(entitlementProd: any): void {
        this.setControlValue('EntitlementProd', entitlementProd);
        this.pageParams.EntitlementProd = entitlementProd;
    }

    /**
     * This method is called before execute grid
     * @param void
     * @return void
     */
    public riGridBeforeExecute(): void {
        if (this.riGrid.Mode === MntConst.eModeUpdate) {
            this.hideObj['tdSaveRequired'] = false;
        }

        if (this.hideObj['tdSaveRequired'] === false && this.riGrid.Mode === MntConst.eModeUpdate && !this.isBApplyChange && !this.isBCheckFrequency) {
            let msgObj: any = new ICabsModalVO(MessageConstant.PageSpecificMessage.serviceAreaDetailGrid.confirmWarning, null, this.confirmOk.bind(this), this.cancelOk.bind(this));
            msgObj.title = MessageConstant.PageSpecificMessage.serviceAreaDetailGrid.confirmTitle;
            this.modalAdvService.emitPrompt(msgObj);
        }

        this.setControlValue('UploadedError', '');
        let search: QueryParams = this.getURLSearchParamObject();
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('SeqNumberFrom', this.getControlValue('SeqNumberFrom'));
        search.set('PremisePostcode', this.getControlValue('PremisePostcode'));
        search.set('PremiseTown', this.getControlValue('PremiseTown'));
        search.set('ContractTypeCode', this.getControlValue('ContractTypeCode'));
        search.set('PortfolioStatusType', this.getControlValue('PortfolioStatusType'));
        search.set('EntitlementProd', this.getControlValue('EntitlementProd'));
        search.set('TotalPremises', this.getControlValue('TotalPremises'));
        search.set('TotalExchanges', this.getControlValue('TotalExchanges'));
        search.set('TotalWeds', this.getControlValue('TotalWeds'));
        search.set('TotalHours', this.getControlValue('TotalHours'));
        search.set('DisplayErrorsOnly', this.getControlValue('DisplayErrors'));
        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.PageSize, this.pageSize.toString());
        search.set(this.serviceConstants.PageCurrent, this.curPage.toString());
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.getControlValue('riGridHandle'));
        search.set(this.serviceConstants.GridCacheRefresh, this.gridCacheRefresh);
        this.gridCacheRefresh = 'True';
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        let sortOrder: string = 'Descending';
        if (!this.riGrid.DescendingSort) sortOrder = 'Ascending';
        search.set('riSortOrder', sortOrder);
        if (this.isBApplyChange) {
            search.set('LastChangeEffectDate', this.getControlValue('LastChangeEffectDate'));
            search.set(this.serviceConstants.Function, 'ApplyChanges');
        } else if (this.isBCheckFrequency) {
            search.set('LastChangeEffectDate', this.getControlValue('LastChangeEffectDate'));
            search.set(this.serviceConstants.Function, 'CheckFrequency');
        } else {
            search.set('FileUploaded', this.getControlValue('FileUploaded'));
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.gridSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data) {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.curPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageSize : 1;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.Execute(data);
                    }

                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    /**
     * This method is called after confirm button click
     * @param void
     * @return void
     */
    public confirmOk(): void {
        this.gridCacheRefresh = 'False';
    }

    /**
     * This method is called after cancel click
     * @param void
     * @return void
     */
    public cancelOk(): void {
        this.hideObj['tdSaveRequired'] = true;
        this.hideObj['tdErrors'] = true;
        this.hideObj['trDisplayErrors'] = true;
        this.hideObj['tdNoChanges'] = true;
        this.hideObj['LastChangeEffectDate'] = true;
        this.setControlValue('LastChangeEffectDate', '');
    }

    /**
     * This method is used to capture DisplayErrors
     * @param de DisplayErrors checked value
     * @return void
     */
    public changeDisplayErrors(de: any): void {
        this.setControlValue('DisplayErrors', de);
    }

    /**
     * This method is called to sort based on header click
     * @param void
     * @return void
     */
    public riGridSort(): void {
        this.gridCacheRefresh = 'False';
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    /**
     * This method is called after grid execute
     * @param void
     * @return void
     */
    public riGridAfterExecute(): void {
        let intStartColumn: number = 0, intUnitsColumn = 0, intPremisesColumn = 0, intHoursColumn = 0, intWEDColumn = 0;
        intStartColumn = 4;
        intUnitsColumn = intStartColumn;
        intPremisesColumn = intStartColumn + 1;
        intHoursColumn = intStartColumn + 2;
        intWEDColumn = intStartColumn + 4;
        setTimeout(() => {
            if (this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.innerHTML) {
                this.setControlValue('TotalExchanges', this.riGrid.HTMLGridBody.children[0].children[intUnitsColumn].getAttribute('additionalproperty'));
                this.setControlValue('TotalPremises', this.riGrid.HTMLGridBody.children[0].children[intPremisesColumn].getAttribute('additionalproperty'));
                this.setControlValue('TotalHours', this.riGrid.HTMLGridBody.children[0].children[intHoursColumn].getAttribute('AdditionalProperty'));
                if (this.systemChars['SCEnableWED'] === true) {
                    this.setControlValue('TotalWEDs', this.riGrid.HTMLGridBody.children[0].children[intWEDColumn].getAttribute('AdditionalProperty'));
                }
            } else {
                this.setControlValue('TotalExchanges', '0');
                this.setControlValue('TotalPremises', '0');
                this.setControlValue('TotalHours', '00:00');
                if (this.systemChars['SCEnableWED'] === true) {
                    this.setControlValue('TotalWEDs', '0.0');
                }
            }
            this.isBlnGridHasData = (this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.children.length > 0);
            if (this.isBlnGridHasData) {
                this.setControlValue('UploadedError', this.riGrid.Details.GetAttribute('Postcode', 'AdditionalProperty'));
                this.setControlValue('riGridHandle', this.riGrid.Details.GetAttribute('SequenceNumber', 'AdditionalProperty'));
            }
            this.isBCheckFrequency = false;
            if (this.isBApplyChange) {
                this.hideObj['tdSaveRequired'] = true;
                this.hideObj['LastChangeEffectDate'] = true;
                this.setControlValue('LastChangeEffectDate', '');
                this.isBApplyChange = false;
            }
        }, 0);
    }

    /**
     * This method is called on branchServiceArea change
     * @param void
     * @return void
     */
    public branchServiceAreaCodeOnchange(): void {
        let search: QueryParams = this.getURLSearchParamObject(), postData: Object = {};
        postData[this.serviceConstants.Function] = 'SetEmployeeSurname';
        search.set(this.serviceConstants.Action, '6');
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                }

            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is called on contractTypeCode change
     * @param void
     * @return void
     */
    public contractTypeCodeOnchange(): void {
        this.buildMenu();
    }

    public riGridBodyOndblClick(event: any): void {
        this.serviceCoverFocus(event.srcElement);
        switch (this.riGrid.CurrentColumnName) {
            case 'BranchServiceAreaSeqNo':
                this.navigate('ServiceAreaSequence', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEAREASEQUENCEMAINTENANCE, {
                    ServiceCoverRowID: this.attributes['ServiceCoverRowID'],
                    CurrentContractTypeURLParameter: this.pageParams['CurrentContractTypeURLParameter'],
                    RowIndex: this.attributes['RowIndex']
                });
                break;
            case 'ProductCode':
                if (this.pageParams['CurrentContractTypeURLParameter'] === '<product>') {
                    this.navigate('ServiceAreaDetail', InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE, {
                        CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                        ServiceCoverRowID: this.attributes['ServiceCoverRowID'],
                        RowIndex: this.attributes['RowIndex'],
                        currentContractType: this.pageParams['currentContractType']
                    });

                } else {
                    this.navigate('ServiceAreaSequence', this.ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                        CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                        ServiceCoverRowID: this.attributes['ServiceCoverRowID'],
                        RowIndex: this.attributes['RowIndex'],
                        currentContractType: this.pageParams['currentContractType']
                    });

                }
                break;
            case 'Postcode':
                this.navigate('ServiceAreaSequence', this.ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE,
                    {
                        CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                        PremiseRowID: this.attributes['PremiseRowID'],
                        contractTypeCode: this.pageParams['currentContractType']
                    });

                break;
        }
    }

    public riGridBodyOnKeyDown(event: any): void {
        switch (event.keyCode) {
            case 38:
                event.returnValue = 0;
                if (event.srcElement.parentElement.parentElement.parentElement.previousSibling) {
                    if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children) {
                        if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex]) {
                            if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]) {
                                this.serviceCoverFocus(event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]);
                            }
                        }
                    }

                }
                break;
            case 40:
            case 9:
                event.returnValue = 0;
                if (event.srcElement.parentElement.parentElement.parentElement.nextSibling) {
                    if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children) {
                        if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex]) {
                            if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]) {
                                this.serviceCoverFocus(event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]);
                            }
                        }
                    }

                }
                break;
        }
    }

    /**
     * This method is called when leaving the screen
     * @param void
     * @return Observable containing current state
     */
    public canDeactivate(): Observable<boolean> {
        let isShownWarning: boolean = !this.hideObj['tdSaveRequired'];
        this.routeAwayGlobals.setSaveEnabledFlag(isShownWarning);
        let messageText: string = MessageConstant.PageSpecificMessage.serviceAreaDetailGrid.confirmWarningExit;
        this.canDeactivateObservable = new Observable((observer) => {
            let modalVO: ICabsModalVO = new ICabsModalVO(messageText, null);
            modalVO.title = MessageConstant.PageSpecificMessage.serviceAreaDetailGrid.warningTitle;
            if (isShownWarning) {
                modalVO.cancelCallback = () => {
                    this.cancelEvent(observer);
                };
                modalVO.confirmCallback = () => {
                    observer.next(true);
                };
                this.modalAdvService.emitPrompt(modalVO);
                return;
            }
            observer.next(true);
        });
        return this.canDeactivateObservable;
    }

    public cancelEvent(observer: any): void {
        if (this.variableService.getBackClick() === true) {
            this.variableService.setBackClick(false);
            this.location.go(this.utils.getCurrentUrl());
        }
        observer.next(false);
        setTimeout(() => {
            this.router.navigate([], { skipLocationChange: true, preserveQueryParams: true });
        }, 0);
    }

    public btnApplyChangesOnclick(): void {
        if (!this.hideObj['LastChangeEffectDate']) {
            if (this.getControlValue('LastChangeEffectDate')) {
                let msgObj: any = new ICabsModalVO(MessageConstant.PageSpecificMessage.serviceAreaDetailGrid.confirmMsg, null, this.confirmChangeOk.bind(this), this.cancelChangeOk.bind(this));
                msgObj.title = MessageConstant.PageSpecificMessage.serviceAreaDetailGrid.ConfirmChangeTitle;
                this.modalAdvService.emitPrompt(msgObj);
            }
        }
    }
    /**
     * This method is called after ok button click
     * @param void
     * @return void
     */

    public confirmChangeOk(): void {
        this.isBApplyChange = true;
        this.gridCacheRefresh = 'True';
        this.buildGrid();
        this.refresh();
    }

    /**
     * This method is called after cancel button click
     * @param void
     * @return void
     */
    public cancelChangeOk(): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'LastChangeEffectDate', true);
        this.effectiveDateChild.setFocus = true;
    }
}
// Class END
