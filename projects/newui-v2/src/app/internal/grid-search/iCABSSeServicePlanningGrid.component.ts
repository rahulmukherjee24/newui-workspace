import { StaticUtils } from '@shared/services/static.utility';
import { environment } from './../../../environments/environment';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { LocalStorageService } from 'ngx-webstorage';
import { SysCharConstants } from './../../../shared/constants/syscharservice.constant';

import { Component, Injector, OnInit, OnDestroy, ViewChild, EventEmitter, HostListener } from '@angular/core';

import { BaseComponent } from '../../base/BaseComponent';
import { BranchServiceAreaSearchComponent } from './../search/iCABSBBranchServiceAreaSearch';
import { ContractManagementModuleRoutes, InternalGridSearchSalesModuleRoutes, InternalMaintenanceServiceModuleRoutes, InternalGridSearchServiceModuleRoutes, InternalMaintenanceApplicationModuleRoutes, AppModuleRoutes, GoogleMapPagesModuleRoutes } from './../../base/PageRoutes';
import { ContractSearchComponent } from './../search/iCABSAContractSearch';
import { ErrorConstant } from './../../../shared/constants/error.constant';
import { GlobalConstant } from './../../../shared/constants/global.constant';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from './../../base/PageIdentifier';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { ServicePlanningGridHelper } from './iCABSSeServicePlanningGridHelper.service';
import { GridSearchModuleConstants } from './grid-search.constant';
import { WebWorkerService } from './../../../shared/web-worker/web-worker';
import { AppWebWorker } from './../../../shared/web-worker/app-worker';
import { HttpWorker } from './../../../shared/web-worker/http-worker';
import { AuthService } from './../../../shared/services/auth.service';

/**
 * @todo -
 * showButtonsForItalyOnly - This method can be made to be called once
 *
 */

@Component({
    templateUrl: 'iCABSSeServicePlanningGrid.html',
    providers: [ServicePlanningGridHelper]
})
export class ServicePlanningGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;


    private addElementNumber: number = 0;
    private addElementNumber2: number = 0;
    private currentContractTypeURLParameter: string = '';
    private isBuildGridRequired: boolean = false;
    private isLookupReady: boolean = false;
    private isPlanningDairyOptionReady: boolean = false;
    private isSysCharReady: boolean = false;
    private gridFooterDataLatest: string;
    private isGridSeqNoUpdateRequired: boolean = false;
    //API variables
    private queryParams = {
        method: 'service-planning/maintenance',
        module: 'planning',
        operation: 'Service/iCABSSeServicePlanningGrid'
    };
    public checkedBoxCount: number = 0;
    public btnMessageOnDisable: string = MessageConstant.PageSpecificMessage.seServicePlanningGrid.btnMessageOnDisable;

    public customErrorMessage: string = '';
    public pageId: string;
    //Form variables
    public controls: Array<any> = [
        //Search Fields block 1
        { name: 'BranchServiceAreaCode', type: MntConst.eAlignmentLeft },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'StartDate', required: true, disabled: true, type: MntConst.eTypeDate },
        { name: 'EndDate', required: true, disabled: true, type: MntConst.eTypeDate },
        { name: 'WeekNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'GridPageSize', required: true, type: MntConst.eTypeInteger },
        { name: 'CManualDates' },
        //Search Fields block 2
        { name: 'NegBranchNumber' },
        { name: 'BranchName' },
        { name: 'FromDate', type: MntConst.eTypeDate },
        { name: 'UpToDate', required: true, type: MntConst.eTypeDate },
        { name: 'ServicePlanNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'VisitTypeFilter' },
        { name: 'InServiceTypeCode' },
        { name: 'ServiceTypeDesc' },
        { name: 'SequenceNumber', type: MntConst.eTypeInteger },
        { name: 'SequenceGroupNo' },
        { name: 'SeqNoTo', type: MntConst.eTypeInteger },
        { name: 'DisplayTimes' },
        { name: 'ViewDays' },
        { name: 'ContractTypeFilter' },
        { name: 'ContractNumberSearch', type: MntConst.eAlignmentLeft },
        { name: 'DisplayAverageWeight' },
        { name: 'PlanningStatus' },
        { name: 'ContractNameSearch', type: MntConst.eTypeText, commonValidator: true },
        { name: 'DisplayServiceType' },
        { name: 'DisplayFilter', value: 'All' },
        { name: 'TownSearch', type: MntConst.eTypeText, commonValidator: true },
        { name: 'ConfApptOnly' },
        //Grid Top block fields
        { name: 'UnplannedNoOfCalls', disabled: true, type: MntConst.eTypeInteger },
        { name: 'UnplannedNoOfExchanges', disabled: true, type: MntConst.eTypeInteger },
        { name: 'UnplannedWED', type: MntConst.eTypeDecimal1 },
        { name: 'UnplannedTime', disabled: true, type: MntConst.eTypeText },
        { name: 'UnplannedNettValue', disabled: true, type: MntConst.eTypeCurrency },
        //Grid Bottom block fields - Filter Visits
        { name: 'SubtotalNoOfCalls', disabled: true, type: MntConst.eTypeInteger },
        { name: 'SubtotalNoOfExchanges', disabled: true, type: MntConst.eTypeInteger },
        { name: 'SubtotalWED', type: MntConst.eTypeDecimal1 },
        { name: 'SubtotalTime', disabled: true, type: MntConst.eTypeText },
        { name: 'SubtotalNettValue', disabled: true, type: MntConst.eTypeCurrency },
        //Grid Bottom block fields - Total Visits
        { name: 'TotalNoOfCalls', disabled: true, type: MntConst.eTypeInteger },
        { name: 'TotalNoOfExchanges', disabled: true, type: MntConst.eTypeInteger },
        { name: 'TotalWED', type: MntConst.eTypeDecimal1 },
        { name: 'TotalTime', disabled: true, type: MntConst.eTypeText },
        { name: 'TotalNettValue', disabled: true, type: MntConst.eTypeCurrency },
        //Last block fields
        { name: 'SeqNumberFrom', type: MntConst.eTypeInteger },
        { name: 'SeqNumberTo', type: MntConst.eTypeInteger },
        { name: 'PlanDays', value: '0' },
        //Hidden fields
        { name: 'BranchNumber' },
        { name: 'CancelRowid' },
        { name: 'ContractNumber' },
        { name: 'ContractName' },
        { name: 'PremiseNumber' },
        { name: 'PremiseName' },
        { name: 'ProductCode' },
        { name: 'ProductDesc' },
        { name: 'GetWarnMessage' },
        { name: 'RefreshGrid' },
        { name: 'ODateFrom' },
        { name: 'ODateTo' },
        { name: 'BranchServiceAreaCount' },
        { name: 'EmployeeCode' },
        { name: 'EmployeeCode1' },
        { name: 'EmployeeCode2' },
        { name: 'EmployeeSurname0' },
        { name: 'EmployeeSurname1' },
        { name: 'EmployeeSurname2' },
        // Additional Weight Fields
        { name: 'InPlanningWeight', type: MntConst.eTypeText },
        { name: 'LoadPercentage', type: MntConst.eTypeText },
        { name: 'OverweightInd' },
        // Added for multi-tech diary
        { name: 'EmployeeCodes' }
    ];

    public brServicePlanConstants: any;
    //Grid Component variables
    public pageSize: number = 11;
    public totalRecords: number = 10;
    public isHidePagination: boolean = true;
    public isTicked: boolean = false;

    //Ellipsis variables
    public ellipsConf: any = {
        serviceArea: {
            childConfigParams: {
                'parentMode': 'LookUpSendForename1'
            },
            contentComponent: BranchServiceAreaSearchComponent
        },
        contractNumber: {
            childConfigParams: {
                'parentMode': 'LookUp-ContractNumberSearch'
            },
            contentComponent: ContractSearchComponent
        }
    };
    public modalConfig: Object = {
        backdrop: 'static',
        keyboard: true
    };
    public isShowHeader: boolean = true;
    public isShowCloseButton: boolean = true;

    //Dropdown
    public dropdown: any = {
        negBranch: {
            params: {
                'parentMode': 'LookUp-NegBranch'
            },
            active: { id: '', text: '' },
            type: 'NegBranchNumber',
            isFocus: false
        },
        serviceType: {
            params: {
                'parentMode': 'LookUpC'
            },
            active: { id: '', text: '' },
            type: 'InServiceTypeCode',
            isFocus: false
        },
        planningStatus: {
            type: 'PlanningStatus'
        },
        viewDays: {
            type: 'ViewDays'
        },
        listVisitTypeFilter: [],
        listPlanningStatus: [],
        listViewDays: [],
        listContractTypeFilter: []
    };

    //Datepicker
    public datepickerType: any = {
        startDate: 'StartDate',
        endDate: 'EndDate'
    };

    //Page Business logis
    public setFocus: any = {
        branchServiceAreaCode: new EventEmitter<boolean>(),
        startDate: new EventEmitter<boolean>(false),
        endDate: new EventEmitter<boolean>(false)
    };
    public cmdPlanCancel: String = MessageConstant.PageSpecificMessage.seServicePlanningGrid.cancelBtnLabel1;
    public SystemCharEnableNAVStockReservation: boolean = false; //ITA-832

    // Caching Latest Week Number Data
    private latestWeekCache: Record<string, Record<string, string>> = {};

    public reserveStockRoute: string = '';

    constructor(injector: Injector, public helper: ServicePlanningGridHelper, private _ls: LocalStorageService, private _authService: AuthService, public sysCharConstants: SysCharConstants) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNINGGRID;
        this.browserTitle = MessageConstant.PageSpecificMessage.seServicePlanningGrid.browserTitle;
        this.pageTitle = MessageConstant.PageSpecificMessage.seServicePlanningGrid.pageTitle1;
    }


    ngOnInit(): void {
        super.ngOnInit();
        this.brServicePlanConstants = GridSearchModuleConstants.branchServicePlanningSummary;
        if (this.isReturning()) {
            this.helper.buildMenuOptions(this.pageParams, this.dropdown);
            this.populateUIFromFormData();
            this.applyStateRetation();
        } else {
            this.pageParams.curPage = 1;
            this.helper.init(this.pageParams);
            this.getSysCharDtetails();
            this.doLookup();
            this.setPlanningDiaryOptions();
            this.setControlValue('GridPageSize', 11);
        }
        this.showButtonsForItalyOnly(); //ITA-832
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.setFocus.branchServiceAreaCode) {
            this.setFocus.branchServiceAreaCode.unsubscribe();
        }
        this.setFocus.branchServiceAreaCode = null;
        if (this.setFocus.startDate) {
            this.setFocus.startDate.unsubscribe();
        }
        this.setFocus.startDate = null;
        if (this.setFocus.endDate) {
            this.setFocus.endDate.unsubscribe();
        }
        this.setFocus.endDate = null;
        this.setFocus = null;
    }

    @HostListener('window:keydown', ['$event'])
    keyboardInput(e: KeyboardEvent): void {
        this.onKeyDownDocumnt(e);
    }

    //Start: State retaintion functionality
    private applyStateRetation(): void {
        if (this.getControlValue('NegBranchNumber') && this.getControlValue('BranchName')) {
            this.dropdown.negBranch.active = { id: this.getControlValue('NegBranchNumber'), text: this.getControlValue('NegBranchNumber') + ' - ' + this.getControlValue('BranchName') };
        }
        if (this.getControlValue('InServiceTypeCode') && this.getControlValue('ServiceTypeDesc')) {
            this.dropdown.serviceType.active = { id: this.getControlValue('InServiceTypeCode'), text: this.getControlValue('InServiceTypeCode') + ' - ' + this.getControlValue('ServiceTypeDesc') };
        }
        //Fields
        if (this.pageParams.vEnableWED) {
            this.disableControl('UnplannedWED', true);
            this.disableControl('SubtotalWED', true);
            this.disableControl('TotalWED', true);
        }
        this.disableControl('StartDate', this.pageParams.isStartDateDisabled);
        this.disableControl('EndDate', this.pageParams.isEndDateDisabled);
        //Grid
        this.initGrid();
        this.buildGrid();
        //this.refresh();
        //Apply Parent Mode
        switch (this.parentMode) {
            case ServicePlanningGridHelper.SERVICE_PLAN:
                this.cmdPlanCancel = MessageConstant.PageSpecificMessage.seServicePlanningGrid.cancelBtnLabel2;
                this.pageTitle = MessageConstant.PageSpecificMessage.seServicePlanningGrid.pageTitle2;
                this.disableControl('BranchServiceAreaCode', true);
                this.riGridBeforeExecute();
                break;
            case ServicePlanningGridHelper.PRODUCTIVITY_REVIEW:
                this.riGridBeforeExecute();
                break;
            default:
                this.refresh();
        }
    }
    //End: State retaintion functionality

    //Start: Syschar functionality
    private getSysCharDtetails(): void {
        let sysCharIP: any = this.helper.getSysCharReqParam(this.sysCharConstants, this.queryParams);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.speedScript.sysCharPromise(sysCharIP).then((data) => {
            if (!this.handleSuccessError(data) && data.records && data.records.length > 0) {
                this.helper.processSysCharRes(data.records, this.pageParams);
            }
            this.isSysCharReady = true;
            this.windowOnload();
        }).catch((error) => this.handleError(error));
    }
    //End: Syschar functionality

    //Start: Loockup functionality
    private doLookup(): void {
        let reqData: any = this.helper.getLoockupReq();
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(reqData).then((data) => {
            if (!this.handleSuccessError(data)) {
                if (data && data.length > 0) {
                    this.helper.processPlanVisitStatusDescLookupRes(data, this.pageParams);
                    this.helper.processContractTypeLookupRes(data, this.pageParams);
                    this.helper.processSystemParameterLookupRes(data, this.pageParams);
                    this.helper.processSAreaSeqGroupLookupRes(data, this.pageParams);
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(ErrorConstant.Message.RecordNotFound));
                }
            }
            this.isLookupReady = true;
            this.windowOnload();
        }
        ).catch((error) => this.handleError(error));

        this.LookUp.i_GetBusinessRegistryValue(this.utils.getBusinessCode(), 'AI Rhythm', 'Undo_Selection_Button_Disabled', new Date())
            .then((data: any): any => {
                if (data) {
                    this.pageParams.isUndoSelection = data.split(',').includes(this.utils.getBranchCode().toString());
                }
            });
    }
    //End: Loockup functionality

    private showButtonsForItalyOnly(): any {  //ITA-832 [if 4390 available then show buttons else hide]
        let querySysChar: QueryParams = this.getURLSearchParamObject();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.SystemCharNumber, this.sysCharConstants.SystemCharEnableNAVStockReservation.toString());

        this.httpService.sysCharRequest(querySysChar)
            .subscribe((response) => {
                this.SystemCharEnableNAVStockReservation = (response && response.records && response.records[0]['Required']) ? true : false;
                this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'BranchServiceAreaCode', this.SystemCharEnableNAVStockReservation);

            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private windowOnload(): void {
        let upToDate: any;

        if (this.isSysCharReady && this.isLookupReady && this.isPlanningDairyOptionReady) {
            this.helper.buildMenuOptions(this.pageParams, this.dropdown);
            this.setInitialSelectedValueOfDD();

            this.setControlValue('DisplayTimes', this.pageParams.vDisplayTimes);
            this.setControlValue('DisplayAverageWeight', this.pageParams.vDisplayAverageWeight || this.pageParams.vEnableAdditionalWeight);
            this.setControlValue('DisplayServiceType', this.pageParams.vDisplayServiceType);

            //Grid
            this.initGrid();

            //Fields
            if (this.pageParams.vEnableWED) {
                this.disableControl('UnplannedWED', true);
                this.disableControl('SubtotalWED', true);
                this.disableControl('TotalWED', true);
            }
            this.setControlValue('BranchNumber', this.utils.getBranchCode());

            //Apply Parent Mode
            switch (this.parentMode) {
                case ServicePlanningGridHelper.SERVICE_PLAN:
                    this.cmdPlanCancel = MessageConstant.PageSpecificMessage.seServicePlanningGrid.cancelBtnLabel2;
                    this.pageTitle = MessageConstant.PageSpecificMessage.seServicePlanningGrid.pageTitle2;
                    this.dropdown.negBranch.isFocus = true;
                    this.disableControl('BranchServiceAreaCode', true);
                    if (this.riExchange.getParentAttributeValue('BranchServiceAreaCode')) {
                        this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentAttributeValue('BranchServiceAreaCode'));
                        this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('EmployeeSurname'));
                    } else {
                        this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
                        this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname'));
                    }
                    this.setControlValue('ServicePlanNumber', this.riExchange.getParentAttributeValue('ServicePlanNumber'));
                    this.setControlValue('StartDate', this.riExchange.getParentAttributeValue('ServicePlanStartDate'));
                    this.setControlValue('EndDate', this.riExchange.getParentAttributeValue('ServicePlanEndDate'));
                    upToDate = this.riExchange.getParentAttributeValue('ServicePlanStartDate');
                    this.setControlValue('SubtotalNoOfCalls', this.riExchange.getParentAttributeValue('ServicePlanNoOfCalls'));
                    this.setControlValue('SubtotalNoOfExchanges', this.riExchange.getParentAttributeValue('ServicePlanNoOfExchanges'));
                    this.setControlValue('SubtotalTime', this.riExchange.getParentAttributeValue('ServicePlanTime'));
                    this.setControlValue('SubtotalNettValue', this.riExchange.getParentAttributeValue('ServicePlanNettValue'));
                    this.setControlValue('TotalNoOfCalls', this.riExchange.getParentAttributeValue('ServicePlanNoOfCalls'));
                    this.setControlValue('TotalNoOfExchanges', this.riExchange.getParentAttributeValue('ServicePlanNoOfExchanges'));
                    this.setControlValue('TotalTime', this.riExchange.getParentAttributeValue('ServicePlanTime'));
                    this.setControlValue('TotalNettValue', this.riExchange.getParentAttributeValue('ServicePlanNettValue'));
                    this.setControlValue('UpToDate', this.helper.dateAdd(upToDate, 6));
                    //Show/Hide
                    this.pageParams.isFromDate = false;
                    this.pageParams.isUpToDate = false;
                    this.pageParams.isServicePlanNumber = true;
                    this.pageParams.isTrNewPlan = false;
                    this.pageParams.isTrAdjustPlan = true;
                    this.pageParams.isUnplannedTotals = false;
                    this.pageParams.isConfApptOnly = false;
                    this.pageParams.isDefaultRoutines = false;
                    //Disable Fields
                    this.pageParams.isUndoSelection = true;
                    this.buildGrid();
                    this.riGridBeforeExecute();
                    break;
                case ServicePlanningGridHelper.PRODUCTIVITY_REVIEW:
                    this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
                    this.setControlValue('StartDate', this.helper.dateAdd(this.pageParams.vEndofWeekDate, 1));
                    this.setControlValue('EndDate', this.helper.dateAdd(this.pageParams.vEndofWeekDate, 7));
                    this.setControlValue('UpToDate', this.riExchange.getParentAttributeValue('StartDate'));
                    this.onChangeBranchServiceAreaCode();
                    this.buildGrid();
                    this.riGridBeforeExecute();
                    break;
                default:
                    this.pageParams.isPlusMinus = true;
                    this.setFocus.branchServiceAreaCode.emit(true);
                    this.setControlValue('StartDate', this.helper.dateAdd(this.pageParams.vEndofWeekDate, 1));
                    this.setControlValue('EndDate', this.helper.dateAdd(this.pageParams.vEndofWeekDate, 7));
                    this.setControlValue('UpToDate', this.helper.dateAdd(this.pageParams.vEndofWeekDate, 7));
                    this.setControlValue('ServicePlanNumber', '0');
                    this.pageParams.isTrNewPlan = true;
                    this.pageParams.isTrAdjustPlan = false;
                    this.pageParams.isCmdSummaryDisabled = true;
                    this.buildGrid();
                    break;
            }
            this.getLatestWeekNumber();
            this.riGrid.RefreshRequired();
        }
    }

    public onReserveStock(e?: any): void {
        if (!this.getControlValue('BranchServiceAreaCode').length) {
            this.reserveStockRoute = '';
            if (e) { e.preventDefault(); }
        } else {
            this.reserveStockRoute = '#/' + InternalGridSearchServiceModuleRoutes.ICABSABRANCHSESERVICEPLANSUMMARYRESERVESTOCK + '?cbb_countrycode=' + this.utils.getCountryCode() + '&cbb_businesscode=' + this.utils.getBusinessCode() + '&cbb_branchcode=' + this.utils.getBranchCode() + '&startdate=' + this.getControlValue('StartDate') + '&enddate=' + this.getControlValue('EndDate') + '&branchserviceareacode=' + this.getControlValue('BranchServiceAreaCode');
        }
    }
    /**
     * Used to set the dropdown initial selected value at page load.
     */
    private setInitialSelectedValueOfDD(): void {
        //Set dropdown default value
        this.setControlValue('VisitTypeFilter', 'All');
        if (this.riExchange.getParentMode() === ServicePlanningGridHelper.SERVICE_PLAN) {
            this.setControlValue('PlanningStatus', 'AllStatus');
        } else {
            this.setControlValue('PlanningStatus', 'All');
        }
        if (this.riExchange.getParentMode() === ServicePlanningGridHelper.SERVICE_PLAN) {
            this.setControlValue('ViewDays', 'AllThisWeek');
        } else {
            this.setControlValue('ViewDays', 'All');
        }
        this.setControlValue('ContractTypeFilter', 'All');
    }

    //Start: API call functionality
    private handleError(error: any): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
    }

    private handleSuccessError(data: any): boolean {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        if (data.hasError) {
            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
        }
        return data.hasError;
    }

    private setPlanningDiaryOptions(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: any = {};
        search.set(this.serviceConstants.Action, '6');

        formData.methodtype = 'maintenance';
        formData.ActionType = 'GetPlanningDiaryOptions';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe((data) => {
                if (!this.handleSuccessError(data) && data.ErrorMessageDesc) {
                    this.modalAdvService.emitMessage(new ICabsModalVO(data.ErrorMessageDesc));
                } else {
                    this.pageParams.vDisplayTimes = (data.DisplayTimes === GlobalConstant.Configuration.Yes.toLowerCase());
                    this.pageParams.vDisplayAverageWeight = (data.DisplayAverageWeight === GlobalConstant.Configuration.Yes.toLowerCase());
                    this.pageParams.vDisplayServiceType = (data.DisplayServiceType === GlobalConstant.Configuration.Yes.toLowerCase());
                }
                this.isPlanningDairyOptionReady = true;
                this.windowOnload();
            }, (error) => this.handleError(error));
    }

    /**
     * Used to compare week number at server side.
     */
    private compareWeekNumber(): any {
        let startWeek: number = StaticUtils.getWeek(this.getControlValue('StartDate'));
        return startWeek === StaticUtils.getWeek(this.getControlValue('EndDate'))
            ? (() => {
                this.setControlValue('WeekNumber', startWeek);
                return false;
            })() : (() => {
                this.globalNotifications.displayMessage(
                    MessageConstant.PageSpecificMessage.seServicePlanningGrid.iui7808CompareWeekNumber
                );
                this.setControlValue('WeekNumber', '');
                this.setFocus.startDate.emit(true);
                return true;
            })();
    }

    /**
     * Used to fetch last week number.
     */
    private getLatestWeekNumber(): any {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {}, returnData: boolean = false;
        let formDataString: string = '';

        search.set(this.serviceConstants.Action, '6');

        formData['ActionType'] = 'GetLatestWeekNumber';
        formData['BranchNumber'] = this.getControlValue('BranchNumber');
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode').trim();
        formData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
        formData['EndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate'));
        formData['UpToDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('UpToDate'));
        formData['GetWarnMessage'] = this.getControlValue('GetWarnMessage');

        formDataString = Object.keys(formData).map(key => {
            return formData[key];
        }).join('');

        if (this.latestWeekCache.hasOwnProperty(formDataString)) {
            return this.handleLatestWeekNumberData(formDataString);
        }

        this.httpService.xhrPost(
            this.queryParams.method,
            this.queryParams.module,
            this.queryParams.operation,
            search,
            formData
        ).then(data => {
            this.handleLatestWeekNumberData(formDataString, data);
        }).catch(error => {
            this.handleError(error);
        });
        return returnData;
    }

    /**
     * Added Method To Handle The LatestWeekNumber Data
     */
    private handleLatestWeekNumberData(cacheKey: string, data?: Record<string, string>): boolean {
        data = data || this.latestWeekCache[cacheKey];

        if (!data) {
            return false;
        }

        this.setControlValue('GetWarnMessage', '');
        if (data.hasError || data.errorMessage || data.fullError) {
            this.modalAdvService.emitError(new ICabsModalVO(this.customErrorMessage + data.errorMessage ? data.errorMessage : '', data.fullError));
        } else {
            this.setControlValue('WeekNumber', data.WeekNumber);
            this.latestWeekCache[cacheKey] = data;
            if (data.WarningMessageDesc) {
                let modalVO: ICabsModalVO = new ICabsModalVO(data.WarningMessageDesc || 'Error Occurred');
                this.modalAdvService.emitMessage(modalVO);
                return;
            }
        }
    }

    private fetchUndoSelection(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
        search.set(this.serviceConstants.Action, '6');

        formData['ActionType'] = 'UndoSelection';
        formData['BranchNumber'] = this.getControlValue('BranchNumber');
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode').trim();
        formData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
        formData['EndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate'));
        formData['FromDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('FromDate'));
        formData['UpToDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('UpToDate'));
        formData['ServiceTypeCode'] = this.getControlValue('InServiceTypeCode');
        formData['NegBranchNumber'] = this.getControlValue('NegBranchNumber');
        formData['ContractNumberSearch'] = this.getControlValue('ContractNumberSearch');
        formData['ContractNameSearch'] = this.getControlValue('ContractNameSearch');
        formData['TownSearch'] = this.getControlValue('TownSearch');
        formData['SequenceNumber'] = this.getControlValue('SequenceNumber');
        formData['ContractTypeFilter'] = this.getControlValue('ContractTypeFilter');
        formData['DisplayFilter'] = this.getControlValue('DisplayFilter');
        formData['ConfApptOnly'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('ConfApptOnly'));
        formData['VisitTypeFilter'] = this.getControlValue('VisitTypeFilter');
        formData['ViewDays'] = this.getControlValue('ViewDays');
        formData['PlanningStatusFilter'] = this.getControlValue('PlanningStatus');
        if (this.pageParams.vSAreaSeqGroupAvail) {
            formData['SeqNoTo'] = this.getControlValue('SeqNoTo');
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData).then((data) => {
            this.handleSuccessError(data);
            this.pageParams.riGridHandle = this.utils.randomSixDigitString();
            this.pageParams.riCacheRefresh = true;
            this.riGrid.RefreshRequired();
            this.riGridBeforeExecute();
        }).catch((error) => this.handleError(error));
    }
    //End: API call functionality

    //Start: Grid Private functionality
    private initGrid(): void {
        this.riGrid.FunctionUpdateSupport = true;
        this.riGrid.PageSize = this.getControlValue('GridPageSize');
        this.pageSize = this.riGrid.PageSize;
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateFooter = true;
        this.riGrid.UpdateBody = true;
    }

    /**
     * Used to build the grid.
     */
    private buildGrid(): void {
        let branchServiceAreaCode: string = this.getControlValue('BranchServiceAreaCode');
        this.isBuildGridRequired = false;
        if (!this.isReturning()) { this.clearGrid(); }
        this.isHidePagination = true;
        if (this.helper.isAddColumnAreaCodeRequired(branchServiceAreaCode)) {
            this.riGrid.AddColumn('GBranchServiceAreaCode', 'ServiceCover', 'GBranchServiceAreaCode', MntConst.eAlignmentLeft, 4);
            this.riGrid.AddColumnAlign('GBranchServiceAreaCode', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'ServiceCover', 'BranchServiceAreaSeqNo', MntConst.eTypeText, 6, (this.parentMode === ServicePlanningGridHelper.SERVICE_PLAN));
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('BranchServiceAreaSeqNo', this.pageParams.vRouteOptimisation);

        this.riGrid.AddColumn('NextServiceVisitDate', 'ServiceCover', 'NextServiceVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('NextServiceVisitDate', MntConst.eAlignmentLeft);

        if (this.getControlValue('DisplayTimes')) {
            this.riGrid.AddColumn('GRoutingVisitStartTime', 'ServiceCover', 'GRoutingVisitStartTime', MntConst.eTypeText, 5); //ITA-660 [sprint-6.2]
            this.riGrid.AddColumnAlign('GRoutingVisitStartTime', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('ServiceTime', 'ServiceCover', 'ServiceTime', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('ServiceTime', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractNum', 'ServiceCover', 'ContractNum', MntConst.eAlignmentLeft, 10);
        this.riGrid.AddColumnAlign('ContractNum', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNum', 'ServiceCover', 'PremiseNum', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('PremiseNum', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('GPremiseName', 'ServiceCover', 'GPremiseName', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('GPremiseName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnOrderable('GPremiseName', true);

        this.riGrid.AddColumn('Address', 'ServiceCover', 'Address', MntConst.eTypeText, 40);
        this.riGrid.AddColumnScreen('Address', false);
        //this.riGrid.AddColumnExport('Address', true);

        this.riGrid.AddColumn('Town', 'ServiceCover', 'Town', MntConst.eTypeText, 15);
        this.riGrid.AddColumnAlign('Town', MntConst.eAlignmentLeft);

        if (this.pageParams.vEnablePostcodeDefaulting) {
            this.riGrid.AddColumn('Postcode', 'ServiceCover', 'Postcode', MntConst.eAlignmentLeft, 10);
            this.riGrid.AddColumnAlign('Postcode', MntConst.eAlignmentLeft);
        }

        this.riGrid.AddColumn('ProdCode', 'ServiceCover', 'ProdCode', MntConst.eAlignmentLeft, 10);
        this.riGrid.AddColumnAlign('ProdCode', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ServiceVisitFrequency', 'ServiceCover', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceQuantity', 'ServiceCover', 'ServiceQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('ServiceQuantity', true);

        if (this.getControlValue('DisplayAverageWeight')) {
            this.riGrid.AddColumn('AverageWeight', 'ServiceCover', 'AverageWeight', MntConst.eTypeDecimal2, 5);
            this.riGrid.AddColumnAlign('AverageWeight', MntConst.eAlignmentCenter);
        }

        if (this.pageParams.vEnableWED) {
            this.riGrid.AddColumn('WEDValue', 'ServiceCover', 'WEDValue', MntConst.eTypeDecimal1, 5);
            this.riGrid.AddColumnAlign('WEDValue', MntConst.eAlignmentCenter);
        }
        if (!this.pageParams.vEnableWED && this.getControlValue('DisplayServiceType')) {
            this.riGrid.AddColumn('ServiceTypeCode', 'ServiceCover', 'ServiceTypeCode', MntConst.eAlignmentLeft, 2);
            this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('VisitsDueAndCompleted', 'ServiceCover', 'VisitsDueAndCompleted', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('VisitsDueAndCompleted', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('LastRoutineVisitDate', 'ServiceCover', 'LastRoutineVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('LastRoutineVisitDate', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('VisitTypeCode', 'ServiceCover', 'VisitTypeCode', MntConst.eAlignmentLeft, 2);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('NotificationStatus', 'ServiceCover', 'NotificationStatus', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('NotificationStatus', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('M', 'ServiceCover', 'M', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('M', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('T', 'ServiceCover', 'T', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('T', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('W', 'ServiceCover', 'W', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('W', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Th', 'ServiceCover', 'Th', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('Th', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('F', 'ServiceCover', 'F', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('F', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Sa', 'ServiceCover', 'Sa', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('Sa', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Su', 'ServiceCover', 'Su', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('Su', MntConst.eAlignmentCenter);

        if (this.parentMode !== ServicePlanningGridHelper.SERVICE_PLAN) {
            this.riGrid.AddColumn('Planned', 'ServiceCover', 'Planned', MntConst.eTypeImage, 1);
            this.riGrid.AddColumnAlign('Planned', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('PlanCancel', 'ServiceCover', 'PlanCancel', MntConst.eTypeCheckBox, 1, false, '');
        this.riGrid.AddColumnAlign('PlanCancel', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PortfolioStatus', 'ServiceCover', 'PortfolioStatus', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('PortfolioStatus', MntConst.eAlignmentCenter);


        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);
        if (this.getControlValue('DisplayTimes')) {
            this.riGrid.AddColumnOrderable('GRoutingVisitStartTime', true);
        }
        this.riGrid.AddColumnOrderable('ContractNum', true);
        this.riGrid.AddColumnOrderable('ProdCode', true);
        this.riGrid.AddColumnOrderable('ServiceVisitFrequency', true);
        this.riGrid.AddColumnOrderable('NextServiceVisitDate', true, true);
        this.riGrid.AddColumnOrderable('VisitsDueAndCompleted', true);
        this.riGrid.AddColumnOrderable('PortfolioStatus', true);
        this.riGrid.AddColumnOrderable('Town', true);
        if (!this.pageParams.vEnableWED && this.getControlValue('DisplayServiceType')) {
            this.riGrid.AddColumnOrderable('ServiceTypeCode', true);
        }

        this.riGrid.AddColumnOrderable('ServiceTime', true);
        this.riGrid.AddColumnOrderable('VisitTypeCode', true);

        if (this.pageParams.vEnablePostcodeDefaulting) {
            this.riGrid.AddColumnOrderable('Postcode', true);
        }
        this.riGrid.Complete();
        // Retain state of grid header sort order
        if (this.isReturning()) {
            this.riGrid.HeaderClickedColumn = this.pageParams.riGridHeaderClickedColumn;
            this.riGrid.DescendingSort = this.pageParams.riGridSortOrder === 'Descending';
        }
    }


    private riGridBeforeExecute(): void {
        this.riGridBeforeExecuteValidation1();
    }

    private riGridBeforeExecuteValidation1(): void {
        let dateCheckValidation: any;

        dateCheckValidation = this.dateCheck();
        if (!dateCheckValidation && !this.compareWeekNumber()) {
            this.riGridBeforeExecuteValidation2();
        }
    }

    private riGridBeforeExecuteValidation2(): void {
        let sDate: any, eDate: Date, today: Date = new Date(), areaCode: any, allAreas: string;
        today.setHours(0, 0, 0, 0);
        eDate = new Date(this.getControlValue('EndDate'));
        if (this.getControlValue('CManualDates') && eDate) {
            if (eDate < today) {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.seServicePlanningGrid.iui7808Msg1));
                if (this.getControlValue('FromDate')) {
                    this.setControlValue('StartDate', this.globalize.parseDateStringToDate(this.getControlValue('FromDate')));
                }
                return;
            }
            sDate = this.getControlValue('StartDate');
            if (sDate) {
                sDate = this.globalize.parseDateStringToDate(sDate);
                if (eDate < sDate) {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.seServicePlanningGrid.iui7808Msg2));
                    if (eDate) {
                        this.setControlValue('EndDate', sDate);
                    }
                    return;
                }
            }
            this.setControlValue('GetWarnMessage', GlobalConstant.Configuration.Yes);
        }

        this.pageParams.blnRefreshRequired = false;
        areaCode = this.getControlValue('BranchServiceAreaCode');
        if (this.riGrid.Update) {
            this.pageParams.blnRefreshRequired = true;
            if (!areaCode) {
                areaCode = this.getAttribute('BranchServiceAreaCode');
            }
        }

        allAreas = this.getControlValue('BranchServiceAreaCode') ? GlobalConstant.Configuration.No : GlobalConstant.Configuration.Yes;

        if (!this.getLatestWeekNumber()) {
            if (this.isGridSeqNoUpdateRequired) {
                this.isGridSeqNoUpdateRequired = false;
                this.updateGridSeqNo(areaCode, allAreas);
            } else {
                if (this.isTicked) {
                    this.fetchGridDataOnTick(areaCode, allAreas);
                } else {
                    this.fetchGridData(areaCode, allAreas);
                }
            }
        }
    }

    private updateGridSeqNo(areaCode: any, allAreas: string): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};

        search.set(this.serviceConstants.Action, '2');

        //grid parameters
        formData[this.serviceConstants.GridMode] = '3';
        formData[this.serviceConstants.GridHandle] = this.pageParams.riGridHandle;
        //formData[this.serviceConstants.GridPageSize] = this.pageSize.toString();
        //formData[this.serviceConstants.GridPageCurrent] = this.curPage.toString();
        formData['riSortOrder'] = this.riGrid.SortOrder;
        formData['HeaderClickedColumn'] = this.riGrid.HeaderClickedColumn;
        //formData['riCacheRefresh'] = this.pageParams.riCacheRefresh;
        //this.pageParams.riCacheRefresh = false;

        // set parameters
        formData['ServiceQuantityRowID'] = (this.riGrid.CurrentColumnName.toLowerCase() === 'servicequantity') ? this.riGrid.Details.GetAttribute('ServiceQuantity', 'RowID') : '';
        formData['BranchServiceAreaSeqNoRowID'] = (this.riGrid.CurrentColumnName.toLowerCase() === 'servicequantity') ? '' : this.riGrid.Details.GetAttribute('BranchServiceAreaSeqNo', 'RowID');
        formData['BranchServiceAreaSeqNo'] = (this.riGrid.CurrentColumnName.toLowerCase() === 'servicequantity') ? '' : this.riGrid.Details.GetValue('BranchServiceAreaSeqNo');
        formData['NextServiceVisitDate'] = this.riGrid.Details.GetValue('NextServiceVisitDate');
        formData['ServiceTime'] = this.riGrid.Details.GetValue('ServiceTime');
        formData['ContractNum'] = this.riGrid.Details.GetValue('ContractNum');
        formData['PremiseNum'] = this.riGrid.Details.GetValue('PremiseNum');
        formData['GPremiseName'] = this.riGrid.Details.GetValue('GPremiseName');
        formData['Town'] = this.riGrid.Details.GetValue('Town');
        formData['Postcode'] = this.riGrid.Details.GetValue('Postcode');
        formData['ProdCode'] = this.riGrid.Details.GetValue('ProdCode');
        formData['ServiceVisitFrequency'] = this.riGrid.Details.GetValue('ServiceVisitFrequency');
        formData['ServiceQuantity'] = this.riGrid.Details.GetValue('ServiceQuantity');
        formData['ServiceTypeCode'] = this.riGrid.Details.GetValue('ServiceTypeCode');
        formData['VisitsDueAndCompleted'] = this.riGrid.Details.GetValue('VisitsDueAndCompleted');
        formData['LastRoutineVisitDate'] = this.riGrid.Details.GetValue('LastRoutineVisitDate');
        formData['VisitTypeCode'] = this.riGrid.Details.GetValue('VisitTypeCode');
        formData['PlanCancelRowID'] = this.riGrid.Details.GetAttribute('PlanCancel', 'RowID');
        formData['PlanCancel'] = this.riGrid.Details.GetValue('PlanCancel');

        formData['BranchNumber'] = this.getControlValue('BranchNumber');
        formData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
        formData['EndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate'));
        formData['FromDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('FromDate'));
        formData['UpToDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('UpToDate'));
        formData['VisitTypeFilter'] = this.getControlValue('VisitTypeFilter');
        formData['BranchServiceAreaCode'] = areaCode.trim();
        formData['AllBranchServiceAreas'] = allAreas;
        formData['ViewDays'] = this.getControlValue('ViewDays');
        formData['PlanningStatus'] = this.getControlValue('PlanningStatus');
        formData['NegBranchNumber'] = this.getControlValue('NegBranchNumber');
        formData['ServiceTypeCode'] = this.getControlValue('InServiceTypeCode');
        formData['SequenceNumber'] = this.getControlValue('SequenceNumber');
        formData['ContractNumber'] = this.getControlValue('ContractNumberSearch');
        formData['ContractName'] = this.getControlValue('ContractNameSearch');
        formData['TownName'] = this.getControlValue('TownSearch');
        formData['ServicePlanNumber'] = this.getControlValue('ServicePlanNumber');
        formData['ContractTypeFilter'] = this.getControlValue('ContractTypeFilter');
        formData['DisplayFilter'] = this.getControlValue('DisplayFilter');
        formData['CancelRowid'] = this.getControlValue('CancelRowid');
        formData['PlanFunction'] = this.utils.booleanToString(this.pageParams.isBlnPlanFunction);
        formData['UpdateNotification'] = this.utils.booleanToString(this.pageParams.isBlnUpdateNotification);
        formData['DisplayTimes'] = this.utils.booleanToString(this.getControlValue('DisplayTimes'));
        formData['DisplayAverageWeight'] = this.utils.booleanToString(this.getControlValue('DisplayAverageWeight'));
        formData['DisplayServiceType'] = this.utils.booleanToString(this.getControlValue('DisplayServiceType'));



        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData).subscribe(
            (data) => {
                // Update UI data on Grid Footer data change
                const gridFooterData = this.helper.getGridFooterData(data);
                if (gridFooterData) {
                    const arrInfo = gridFooterData.split('|');
                    this.setTotalCallsUnitsTimesNettValues(arrInfo);
                }
                this.handleSuccessError(data);
                this.riGrid.Mode = MntConst.eModeNormal;
            },
            (error) => {
                this.handleError(error);
                this.riGrid.Mode = MntConst.eModeNormal;
            });
    }

    //End: Grid Private functionality

    private onChangeNegBranchNumber(data?: any): void {
        if (data) {
            this.setControlValue('NegBranchNumber', data.BranchNumber);
            this.setControlValue('BranchName', data.BranchName);
        }
        this.isBuildGridRequired = true;
        this.pageParams.isVbErrorMessageFlagged = false;
    }

    private onChangeInServiceTypeCode(data?: any): void {
        if (data) {
            this.setControlValue('InServiceTypeCode', data.inServiceTypeCode);
            this.setControlValue('ServiceTypeDesc', data.ServiceTypeDesc);
        }
        this.isBuildGridRequired = true;
        this.pageParams.isVbErrorMessageFlagged = false;
    }

    private onChangeViewDays(): void {
        if (this.parentMode !== ServicePlanningGridHelper.SERVICE_PLAN) {
            if (this.getControlValue('ViewDays') !== 'All') {
                this.setControlValue('PlanningStatus', 'I');
            } else {
                this.setControlValue('PlanningStatus', 'All');
            }
        }
    }

    private onChangePlanningStatus(): void {
        if (this.parentMode !== ServicePlanningGridHelper.SERVICE_PLAN) {
            if (this.getControlValue('PlanningStatus') !== 'I') {
                this.setControlValue('ViewDays', 'All');
            } else {
                this.setControlValue('ViewDays', 'AllThisWeek');
            }
        }
    }
    private onDeactivateBranchServiceAreaCode(): void {
        if (this.pageParams.isVbErrorMessageFlagged) {
            this.setControlValue('BranchServiceAreaCode', '');
        }
        this.pageParams.isAllocate = false;
        this.pageParams.isCmdSummaryDisabled = true;
        if (this.getControlValue('BranchServiceAreaCode')) {
            this.pageParams.isAllocate = true;
            this.pageParams.isCmdSummaryDisabled = false;
            if (this.pageParams.vEnableTechDiary) {
                this.checkTechVisitDiary();
            }
        }
        this.pageParams.isVbErrorMessageFlagged = false;
    }




    private checkTechVisitDiary(): void {
        let branchServiceAreaCode: any = this.getControlValue('BranchServiceAreaCode'), count: number = 0;
        if (branchServiceAreaCode) {
            count = branchServiceAreaCode.split(',').length;
        }
        this.setControlValue('BranchServiceAreaCount', count);
        this.pageParams.isTdGotoDiary = (count > 3) ? false : true;
    }

    private setTotalCallsUnitsTimesNettValues(arrInfo?: Array<any>): void {
        const defaultValues = ['0', '0', '0', '00:00', '0', '0', '0', '00:00', '0', '0', '0', '0', '00:00', '0', '0', '0', '0', '0', 'Kg', '0', 'FALSE'];
        arrInfo = (arrInfo) ? arrInfo : defaultValues;
        this.setControlValue('UnplannedNoOfCalls', arrInfo[1]);
        this.setControlValue('UnplannedNoOfExchanges', arrInfo[2]);
        this.setControlValue('UnplannedTime', arrInfo[3]);
        this.setControlValue('UnplannedNettValue', arrInfo[4]);
        this.setControlValue('SubtotalNoOfCalls', arrInfo[5]);
        this.setControlValue('SubtotalNoOfExchanges', arrInfo[6]);
        this.setControlValue('SubtotalTime', arrInfo[7]);
        this.setControlValue('SubtotalNettValue', arrInfo[8]);
        this.setControlValue('TotalNoOfCalls', arrInfo[9]);
        this.setControlValue('TotalNoOfExchanges', arrInfo[10]);
        this.setControlValue('TotalTimeInteger', arrInfo[11]);
        this.setControlValue('TotalTime', arrInfo[12]);
        this.setControlValue('TotalNettValue', arrInfo[13]);
        if (this.pageParams.vEnableWED && this.pageParams.vEnableAdditionalWeight) {
            this.setControlValue('UnplannedWED', arrInfo[14]);
            this.setControlValue('SubtotalWED', arrInfo[15]);
            this.setControlValue('TotalWED', arrInfo[16]);
            this.setControlValue('InPlanningWeight', `${arrInfo[17]} ${arrInfo[18]}`);
            this.setControlValue('LoadPercentage', arrInfo[19]);
            this.setControlValue('OverweightInd', arrInfo[20]);
        } else if (this.pageParams.vEnableWED) {
            this.setControlValue('UnplannedWED', arrInfo[14]);
            this.setControlValue('SubtotalWED', arrInfo[15]);
            this.setControlValue('TotalWED', arrInfo[16]);
        } else if (this.pageParams.vEnableAdditionalWeight && arrInfo === defaultValues) {
            this.setControlValue('InPlanningWeight', `${arrInfo[17]} ${arrInfo[18]}`);
            this.setControlValue('LoadPercentage', arrInfo[19]);
            this.setControlValue('OverweightInd', arrInfo[20]);
        } else if (this.pageParams.vEnableAdditionalWeight) {
            this.setControlValue('InPlanningWeight', `${this.globalize.formatDecimalToLocaleFormat(arrInfo[14], 2)} ${arrInfo[15]}`);
            this.setControlValue('LoadPercentage', `${this.utils.round(arrInfo[16], 0)}`);
            this.setControlValue('OverweightInd', arrInfo[17]);
        }

        if (this.utils.stringToBoolean(this.getControlValue('OverweightInd').trim())) {
            this.uiForm.controls['InPlanningWeight'].setErrors({ overWeight: true });
            this.uiForm.controls['LoadPercentage'].setErrors({ overWeight: true });
        } else {
            this.uiForm.controls['InPlanningWeight'].setErrors(null);
            this.uiForm.controls['LoadPercentage'].setErrors(null);
        }
    }

    private setAttributes(rTR: any): Array<any> {
        let cProp: any;
        this.addElementNumber = 0;
        this.setAttribute('BranchServiceAreaCode', '');
        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.addElementNumber = 1;
            if (rTR.children && rTR.children.length > 0)
                this.setAttribute('BranchServiceAreaCode', this.riGrid.Details.GetValue('GBranchServiceAreaCode'));
        }
        (this.pageParams.vEnablePostcodeDefaulting) ? this.addElementNumber2 = 1 : this.addElementNumber = 0;
        // All values now passed in the following AdditionalProperty
        cProp = this.riGrid.Details.GetAttribute('ContractNum', 'AdditionalProperty');
        cProp = (cProp) ? cProp.split('^') : '';
        if (cProp) {
            this.setAttribute('ContractRowID', cProp[1]);
            this.setAttribute('PremiseRowID', cProp[2]);
            this.setAttribute('ServiceCoverRowID', cProp[3]);
            this.setAttribute('VisitTypeCode', this.riGrid.Details.GetValue('VisitTypeCode'));
            this.setAttribute('PlanVisitRowID', cProp[0]);
            this.setAttribute('Row', cProp[4]);
            this.setAttribute('ContractTypeCode', cProp[5]);
            this.setAttribute('NextServiceVisitDate', this.riGrid.Details.GetValue('NextServiceVisitDate'));
            this.setAttribute('PlanDate', this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'RowID'));
        }
        return cProp;
    }

    /**
     * On single click this function is called to set different values.
     * @param e
     */
    private updateEvents(e: Event): void {
        let objSrc: any, objTR: any, objSrcName: string, vRowid: any, cancelRowid: string;

        objSrc = e.srcElement;
        objTR = objSrc.parentElement;
        objSrcName = this.riGrid.CurrentColumnName;
        cancelRowid = this.getControlValue('CancelRowid');
        switch (objSrcName) {
            case 'PlanCancel':
                //Add the rows rowid to a variable so we only need to run the function once
                vRowid = this.riGrid.Details.GetAttribute('PlanCancel', 'RowID');
                cancelRowid = this.helper.manupulateCancelRowID(cancelRowid, vRowid);
                this.setControlValue('CancelRowid', cancelRowid);

                (e.target['checked']) ? this.checkedBoxCount++ : (this.checkedBoxCount < 0) ? this.checkedBoxCount = 0 : this.checkedBoxCount--;

                if (this.checkedBoxCount > 0) {
                    this.btnMessageOnDisable = '';
                } else {
                    this.btnMessageOnDisable = MessageConstant.PageSpecificMessage.seServicePlanningGrid.btnMessageOnDisable;
                }
                break;

            case 'NotificationStatus':
                if (this.parentMode !== ServicePlanningGridHelper.SERVICE_PLAN) {
                    this.setAttributes(objTR.parentElement);
                    this.pageParams.isBlnUpdateNotification = true;
                    this.riGrid.Update = true;
                    this.riGridBeforeExecute();
                }
                break;
            case 'M':
            case 'T':
            case 'W':
            case 'Th':
            case 'F':
            case 'Sa':
            case 'Su':
                if (this.parentMode !== ServicePlanningGridHelper.SERVICE_PLAN) {
                    if (this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'RowID') !== 1) {
                        this.pageParams.isVbErrorGrid = true; //assume there is an error and set to false if updated correctly
                        this.riGrid.Update = true;
                        this.fetchUpdatedGrid(objTR);
                    }
                }
                break;
        }
    }

    public fetchUpdatedGrid(objTR: any): void {
        if (this.isTicked) {
            setTimeout(() => {
                this.fetchUpdatedGrid(objTR);
            }, 1000);
        }
        this.pageParams.isBlnPlanFunction = true;
        //this.pageParams.vbPlanDate = this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'RowID');
        this.isTicked = true;
        this.setAttributes(objTR.parentElement.parentElement);
        this.customErrorMessage = 'Visit dated ' + this.riGrid.Details.GetValue('NextServiceVisitDate') + ' of ' + this.riGrid.Details.GetValue('ContractNum') + '/' + this.riGrid.Details.GetValue('PremiseNum') + '/' + this.riGrid.Details.GetValue('ProdCode') + '\n';
        this.riGridBeforeExecute();
    }
    /**
     * keyCode == 107 : Numpad + button
     * keyCode == 109 : Numpad - button
     * keyCode == 106 : Numpad * button
     * @param e
     */
    private onKeyDownDocumnt(e: any): void {
        let eDate: any, today: Date = new Date(), isValidKey: boolean = false;
        if (this.parentMode !== ServicePlanningGridHelper.SERVICE_PLAN && !this.getControlValue('CManualDates')) {
            let keyCode = e.keyCode;
            let isShiftKey = e.shiftKey;

            if (keyCode === 106 || (isShiftKey && keyCode === 56)) {
                //* GoTo Current WeekEndingDate
                this.setControlValue('StartDate', this.helper.dateAdd(this.pageParams.vEndofWeekDate, 1));
                this.setControlValue('EndDate', this.helper.dateAdd(this.pageParams.vEndofWeekDate, 7));
                this.setControlValue('UpToDate', this.getControlValue('EndDate'));
                if (this.getControlValue('FromDate'))
                    this.setControlValue('FromDate', this.globalize.parseDateStringToDate(this.getControlValue('StartDate')));
                isValidKey = true;
            } else if (keyCode === 107 || (isShiftKey && keyCode === 187)) {
                //+ 7 Days
                this.setControlValue('StartDate', this.helper.dateAdd(this.getControlValue('StartDate'), 7));
                this.setControlValue('EndDate', this.helper.dateAdd(this.getControlValue('EndDate'), 7));
                this.setControlValue('UpToDate', this.getControlValue('EndDate'));
                if (this.getControlValue('FromDate'))
                    this.setControlValue('FromDate', this.globalize.parseDateStringToDate(this.getControlValue('StartDate')));
                eDate = this.globalize.parseDateStringToDate(this.getControlValue('EndDate'));
                if (eDate < today) {
                    // #37671 AIH Removed logic to stop users going back
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.seServicePlanningGrid.planWeekPassed));
                }
                isValidKey = true;
            } else if (keyCode === 109 || (isShiftKey && keyCode === 189)) {
                //- 7 Days
                this.setControlValue('StartDate', this.helper.dateAdd(this.getControlValue('StartDate'), -7));
                this.setControlValue('EndDate', this.helper.dateAdd(this.getControlValue('EndDate'), -7));
                this.setControlValue('UpToDate', this.getControlValue('EndDate'));
                if (this.getControlValue('FromDate'))
                    this.setControlValue('FromDate', this.globalize.parseDateStringToDate(this.getControlValue('StartDate')));
                eDate = this.globalize.parseDateStringToDate(this.getControlValue('EndDate'));
                if (eDate < today) {
                    // #37671 AIH Removed logic to stop users going back
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.seServicePlanningGrid.planWeekPassed));
                }
                isValidKey = true;
            }

            if (isValidKey === true) {
                this.riGrid.RefreshRequired();
                this.riGrid.ResetGrid();
                this.clearGrid(true);
                this.setControlValue('GetWarnMessage', GlobalConstant.Configuration.Yes);
                this.getLatestWeekNumber();
                this.isHidePagination = true;
            }
        }
    }

    //Start: Ellipsis functionality
    public onServiceAreaEllipsisDataReceived(data: any): void {
        let code: any = '';
        if (data) {
            code = this.getControlValue('BranchServiceAreaCode');
            if (code) {
                code = code.split(',');
                code.push(data.BranchServiceAreaCode);
                code = code.map((val) => { return val.trim(); }).join();
            } else {
                code = data.BranchServiceAreaCode;
            }
            this.setControlValue('BranchServiceAreaCode', code);
            this.onChangeBranchServiceAreaCode();
        }
    }

    public onContractNumberEllipsisDataReceived(data: any): void {
        if (data) {
            this.setControlValue('ContractNumberSearch', data.ContractNumber);
        }
    }
    //End: Ellipsis functionality

    //Start: Date picker functionality
    public onSelecteDatePicker(type: string): void {
        switch (type) {
            case this.datepickerType['startDate']:
                this.setFocus.endDate.emit(true);
                this.buildGrid();
                this.riGrid.RefreshRequired();
                break;
            case this.datepickerType['endDate']:
                this.dateCheck();
                break;
        }
    }
    //End: Date picker functionality

    //Start: Dropdown functionality
    public onChangeDD(e: any, type: any): void {
        switch (type) {
            case this.dropdown.negBranch.type:
                this.onChangeNegBranchNumber(e);
                break;
            case this.dropdown.serviceType.type:
                this.onChangeInServiceTypeCode(e);
                break;
            case this.dropdown.planningStatus.type:
                this.onChangePlanningStatus();
                break;
            case this.dropdown.viewDays.type:
                this.onChangeViewDays();
                break;
        }
    }
    //Start: Dropdown functionality

    //Start: Grid functionality
    public riGridAfterExecute(): void {
        let gridFooterData: String, arrInfo: any[], arrInfoValue: string, errorMessageArr: any[], strErrorMessage: string;
        if (this.riGrid.HTMLGridFooter && this.riGrid.HTMLGridFooter.children && this.riGrid.HTMLGridFooter.children.length > 0) {
            gridFooterData = this.riGrid.HTMLGridFooter.children[0].children[0].children[0].innerHTML;
        }
        if (!this.riGrid.Update && this.riGrid.Mode !== '3' && !this.pageParams.isVbErrorGrid) {
            //Todo: Grid Footer not yet rady till now. So, need to implement later.
            this.setTotalCallsUnitsTimesNettValues();
            if (gridFooterData) {
                arrInfo = gridFooterData.split('|');
                this.setTotalCallsUnitsTimesNettValues(arrInfo);
                arrInfoValue = arrInfo[14];
                if (this.pageParams.vEnableWED) {
                    arrInfoValue = arrInfo[17];
                }
                this.pageParams.isBlnClosedCalendar = (arrInfoValue && arrInfoValue.toLowerCase() === GlobalConstant.Configuration.Yes.toLowerCase()) ? true : false;
                this.helper.getLegend(this.pageParams, this.riGrid);
            }
        }
        if (this.pageParams.isBlnPlanFunction) {
            //Todo: Grid Footer not yet rady till now. So, need to implement later.
            if (this.gridFooterDataLatest) {
                arrInfo = this.gridFooterDataLatest.split('|');
                strErrorMessage = arrInfo[arrInfo.length - 2];
                if (strErrorMessage) {
                    errorMessageArr = strErrorMessage.split(';');
                }
                if (errorMessageArr && errorMessageArr.length > 0) {
                    this.modalAdvService.emitMessage(new ICabsModalVO(errorMessageArr));
                }
            }
            this.pageParams.isVbErrorGrid = false;
        }
        this.pageParams.isBlnPlanFunction = false;
        //this.riGrid.Update = false;
        this.riGridAfterExecuteEx();
    }

    public riGridAfterExecuteEx(): void {
        if (this.pageParams.isBlnRefreshRequired) {
            this.riGrid.RefreshRequired();
        }
    }

    public getCurrentPage(data: any): void {
        if (this.riGrid.RefreshRequiredStatus()) {
            return;
        }
        this.isTicked = false;
        if (!this.isReturning() || this.pageParams.curPage !== data.value) {
            this.pageParams.curPage = data.value;
            this.riGrid.RefreshRequired();
            this.riGridBeforeExecute();
        }
    }

    public refresh(): void {
        this.isTicked = false;
        this.pageParams.riGridHandle = this.utils.randomSixDigitString();
        this.pageParams.riCacheRefresh = true;
        if (this.isBuildGridRequired) { this.buildGrid(); }
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public riGridSort(): void {
        this.pageParams.riGridSortOrder = this.riGrid.SortOrder;
        this.pageParams.riGridHeaderClickedColumn = this.riGrid.HeaderClickedColumn;
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public riGridBodyOnClick(e: Event): void {
        this.updateEvents(e);
    }

    public riGridBodyDblClick(e: Event): void {
        let addDisplayTime: number = 0, objSrc: any, objTR: any, objSrcName: string, contractTypeCode: any, val: any, cProp: any, additionProp: string, colName: string;

        objSrc = e.srcElement;
        objTR = objSrc.parentElement;
        objSrcName = this.riGrid.CurrentColumnName;
        if (this.getControlValue('DisplayTimes')) {
            addDisplayTime = 1;
        }

        switch (objSrcName) {
            case 'ContractNum':
            case 'PremiseNum':
            case 'ProdCode':
            case 'VisitTypeCode':
            case 'PortfolioStatus':
            case 'NextServiceVisitDate':
            case 'BranchServiceAreaSeqNo':
                if (objSrcName === 'BranchServiceAreaSeqNo' && this.pageParams.vRouteOptimisation && this.parentMode !== ServicePlanningGridHelper.SERVICE_PLAN) {
                    return;
                }

                additionProp = this.riGrid.Details.GetAttribute('PortfolioStatus', 'AdditionalProperty');
                if (objSrcName === 'PortfolioStatus' && additionProp) {
                    //'The source object PortfolioStatus is an image so needs to go up two levels to the TR
                    objTR = objSrc.parentElement.parentElement.parentElement;
                }

                cProp = this.setAttributes(objTR);
                contractTypeCode = this.getAttribute('ContractTypeCode');

                if (contractTypeCode === 'C') { this.currentContractTypeURLParameter = ''; }
                else if (contractTypeCode === 'J') { this.currentContractTypeURLParameter = '<job>'; }
                else if (contractTypeCode === 'P') { this.currentContractTypeURLParameter = '<product>'; }
                switch (objSrcName) {
                    case 'ContractNum': //iCABSAContractMaintenance
                        let urlPath: string = '';
                        if (contractTypeCode === 'C') {
                            urlPath = AppModuleRoutes.CONTRACTMANAGEMENT + ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE_SUB;
                        } else if (contractTypeCode === 'J') {
                            urlPath = AppModuleRoutes.CONTRACTMANAGEMENT + ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE_SUB;
                        } else {
                            urlPath = AppModuleRoutes.CONTRACTMANAGEMENT + ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE_SUB;
                        }
                        this.navigate(ServicePlanningGridHelper.SERVICE_PLANNING, urlPath, {
                            CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                            ContractRowID: cProp[1]
                        });
                        break;
                    case 'PremiseNum': //iCABSAPremiseMaintenance
                        this.navigate(ServicePlanningGridHelper.SERVICE_PLANNING, ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, { CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter, contractTypeCode: contractTypeCode });
                        break;
                    case 'ProdCode':
                        if (this.currentContractTypeURLParameter === '<product>') { //iCABSAProductSalesSCDetailMaintenance
                            this.navigate(ServicePlanningGridHelper.SERVICE_PLANNING, InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE, { CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter });
                        } else { //iCABSAServiceCoverMaintenance
                            let urlPath: string = '';
                            if (contractTypeCode === 'C') {
                                urlPath = ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT;
                            } else if (contractTypeCode === 'J') {
                                urlPath = ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCEJOB;
                            }
                            this.navigate(ServicePlanningGridHelper.SERVICE_PLANNING, urlPath, { CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter, currentContractType: contractTypeCode });
                        }
                        break;
                    case 'BranchServiceAreaSeqNo': //iCABSSeServiceVisitMaintenance
                        if (this.parentMode === ServicePlanningGridHelper.SERVICE_PLAN) {
                            this.navigate('ConfirmedPlan', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE, {
                                CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                                PlanVisitRowID: this.getAttribute('PlanVisitRowID'),
                                ServiceCoverRowID: this.getAttribute('ServiceCoverRowID')
                            });
                        }
                        break;
                    case 'VisitTypeCode':
                        this.navigate((this.parentMode === ServicePlanningGridHelper.SERVICE_PLAN) ? ServicePlanningGridHelper.SERVICE_PLAN : ServicePlanningGridHelper.SERVICE_PLANNING, InternalGridSearchServiceModuleRoutes.ICABSSESERVICEPLANNEDDATESGRID, { CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter });
                        break;
                    case 'NextServiceVisitDate': //iCABSAVisitAppointmentMaintenance
                        this.navigate((this.parentMode === ServicePlanningGridHelper.SERVICE_PLAN) ? ServicePlanningGridHelper.SERVICE_PLAN : ServicePlanningGridHelper.SERVICE_PLANNING, InternalMaintenanceApplicationModuleRoutes.ICABSAVISITAPPOINTMENTMAINTENANCE, {
                            CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                            PlanVisitRowID: cProp[0]
                        });
                        break;
                    case 'PortfolioStatus': //iCABSCMCustomerContactHistoryGrid
                        if (additionProp) {
                            //Contract Number
                            val = objTR.children[3 + this.addElementNumber + addDisplayTime].children[0].innerText;
                            if (val) {
                                val = val.trim().slice(-8);
                                this.setControlValue('ContractNumber', val);
                            }

                            //Contract Name
                            colName = this.riGrid.colArray[5 + this.addElementNumber + addDisplayTime].columnName;
                            val = this.riGrid.Details.GetAttribute(colName, 'AdditionalProperty');
                            this.setControlValue('ContractName', val);

                            //Premice Number
                            val = objTR.children[4 + this.addElementNumber + addDisplayTime].children[0].innerText;
                            this.setControlValue('PremiseNumber', val);

                            //Premice Name
                            val = objTR.children[5 + this.addElementNumber + addDisplayTime].children[0].innerText;
                            this.setControlValue('PremiseName', val);

                            //Product Code
                            val = objTR.children[7 + this.addElementNumber + addDisplayTime + this.addElementNumber2].children[0].innerText;
                            this.setControlValue('ProductCode', val);

                            //Product Desc
                            colName = this.riGrid.colArray[7 + this.addElementNumber + addDisplayTime + this.addElementNumber2].columnName;
                            val = this.riGrid.Details.GetAttribute(colName, 'AdditionalProperty');
                            this.setControlValue('ProductDesc', val);

                            switch (this.riGrid.Details.GetAttribute(objSrcName, 'RowID')) {
                                case 'Contract':
                                    this.setControlValue('PremiseNumber', '');
                                    this.setControlValue('PremiseName', '');
                                    this.setControlValue('ProductCode', '');
                                    this.setControlValue('ProductDesc', '');
                                    break;
                                case 'Premise':
                                    this.setControlValue('ProductCode', '');
                                    this.setControlValue('ProductDesc', '');
                                    break;
                            }
                            //iCABSCMCustomerContactHistoryGrid
                            this.navigate(ServicePlanningGridHelper.SERVICE_PLANNING, InternalGridSearchServiceModuleRoutes.ICABSCMCUSTOMERCONTACTHISTORYGRID,
                                {
                                    CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter
                                });
                        }
                        break;
                }
                break;
        }
    }

    public riGridBodyColumnBlur(): void {
        this.isGridSeqNoUpdateRequired = true;
        this.refresh();
    }
    //End: Grid functionality

    //Start: REST API Call functionality
    public onChangeBranchServiceAreaCode(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {}, dt: Date = new Date();
        if (this.getControlValue('BranchServiceAreaCode')) {
            this.setControlValue('EmployeeCode', '');
            this.setControlValue('EmployeeCode1', '');
            this.setControlValue('EmployeeCode2', '');
            this.setControlValue('EmployeeSurname0', '');
            this.setControlValue('EmployeeSurname1', '');
            this.setControlValue('EmployeeSurname2', '');
            this.setControlValue('EmployeeCodes' ,'');

            search.set(this.serviceConstants.Action, '6');

            formData['ActionType'] = 'GetEmployeeSurname';
            formData['BranchNumber'] = this.getControlValue('BranchNumber');
            formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode').trim();
            formData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
            formData['DTE'] = this.globalize.parseDateToFixedFormat(dt);
            formData['TME'] = dt.getTime();

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData).then((data) => {
                let vEmployeeCodes: any[] = [], vEmployeeSurnames: any[] = [], len: number;
                this.pageParams.isVbErrorMessageFlagged = false;
                if (this.handleSuccessError(data) || data.ErrorMessageDesc) {
                    this.setControlValue('BranchServiceAreaCode', '');
                    this.modalAdvService.emitError(new ICabsModalVO(data.ErrorMessageDesc));
                    this.pageParams.isVbErrorMessageFlagged = true;
                    data.EmployeeSurname = '';
                } else if (data.WarningMessageDesc) {
                    this.modalAdvService.emitMessage(new ICabsModalVO(data.WarningMessageDesc));
                }
                this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                if (data.EmployeeCodes && data.EmployeeSurname) {
                    this.setControlValue('EmployeeCodes', data.EmployeeCodes);
                    vEmployeeCodes = data.EmployeeCodes.split(',');
                    vEmployeeSurnames = data.EmployeeSurname.split(',');
                    len = vEmployeeCodes.length;
                    for (let index = 0; index < len; index++) {
                        switch (index) {
                            case 0:
                                this.setControlValue('EmployeeCode', vEmployeeCodes[0]);
                                this.setControlValue('EmployeeSurname0', vEmployeeSurnames[0]);
                                break;
                            case 1:
                                this.setControlValue('EmployeeCode1', vEmployeeCodes[1]);
                                this.setControlValue('EmployeeSurname1', vEmployeeSurnames[1]);
                                break;
                            case 2:
                                this.setControlValue('EmployeeCode2', vEmployeeCodes[2]);
                                this.setControlValue('EmployeeSurname2', vEmployeeSurnames[2]);
                                break;

                        }

                    }
                }
                this.onDeactivateBranchServiceAreaCode();
            }).catch((error) => this.handleError(error));
        } else {
            this.setControlValue('EmployeeSurname', '');
            this.onDeactivateBranchServiceAreaCode();
        }

        this.pageParams.isTdGotoDiary = false;
        if (this.pageParams.vEnableTechDiary && this.getControlValue('BranchServiceAreaCode')) {
            this.pageParams.isTdGotoDiary = true;
        }
        //Clear out total calls, units, times and nett values
        this.setTotalCallsUnitsTimesNettValues();
        this.riGrid.ResetGrid();
        this.buildGrid();
        this.riGrid.RefreshRequired();
        this.isHidePagination = true;
    }

    public onChangeSequenceGroupNo(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
        if (this.getControlValue('SequenceGroupNo')) {
            search.set(this.serviceConstants.Action, '6');

            formData['ActionType'] = 'GetSeqNoRange';
            formData['SequenceGroupNo'] = this.getControlValue('SequenceGroupNo');
            formData['SeqNoFrom'] = this.getControlValue('SeqNoFrom');
            formData['SeqNoTo'] = this.getControlValue('SeqNoTo');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData).then((data) => {
                this.pageParams.isVbErrorMessageFlagged = false;
                if (this.handleSuccessError(data)) {
                    this.setControlValue('SequenceNumber', '');
                    this.setControlValue('SeqNoTo', '');
                    this.pageParams.isVbErrorMessageFlagged = true;
                } else {
                    this.setControlValue('SequenceNumber', data.SeqNoFrom);
                    this.setControlValue('SeqNoTo', data.SeqNoTo);
                    this.buildGrid();
                    this.riGrid.RefreshRequired();
                }

            }).catch((error) => this.handleError(error));
        } else {
            this.setControlValue('SequenceNumber', '');
            this.setControlValue('SeqNoTo', '');
        }
    }

    public onClickCmdDefaultRoutines(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
        search.set(this.serviceConstants.Action, '6');

        formData['ActionType'] = 'DefaultRoutines';
        formData['BranchNumber'] = this.getControlValue('BranchNumber');
        formData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
        formData['EndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate'));
        formData['FromDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('FromDate'));
        formData['UpToDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('UpToDate'));
        formData['VisitTypeFilter'] = this.getControlValue('VisitTypeFilter');
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode').trim();
        formData['ViewDays'] = this.getControlValue('ViewDays');
        formData['NegBranchNumber'] = this.getControlValue('NegBranchNumber');
        formData['ServiceTypeCode'] = this.getControlValue('InServiceTypeCode');
        formData['SequenceNumber'] = this.getControlValue('SequenceNumber');
        formData['ContractTypeFilter'] = this.getControlValue('ContractTypeFilter');
        formData['DisplayFilter'] = this.getControlValue('DisplayFilter');
        formData['ContractNumberSearch'] = this.getControlValue('ContractNumberSearch');
        formData['ContractNameSearch'] = this.getControlValue('ContractNameSearch');
        formData['TownSearch'] = this.getControlValue('TownSearch');
        formData['ConfApptOnly'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('ConfApptOnly'));
        formData['PlanningStatusFilter'] = this.getControlValue('PlanningStatus');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData).then((data) => {
            this.handleSuccessError(data);
            this.pageParams.riGridHandle = this.utils.randomSixDigitString();
            this.pageParams.riCacheRefresh = true;
            this.riGrid.RefreshRequired();
            this.riGridBeforeExecute();
        }).catch((error) => this.handleError(error));
    }

    public onClickCmdPlanSubmit(): void {
        let isValid: boolean = true, search: QueryParams, formData: Object;
        this.uiForm.controls['SeqNumberFrom'].markAsTouched();
        this.uiForm.controls['SeqNumberTo'].markAsTouched();
        if (this.getControlValue('SeqNumberFrom') !== 0 && !this.getControlValue('SeqNumberFrom')) {
            this.riExchange.riInputElement.markAsError(this.uiForm, 'SeqNumberFrom');
            isValid = false;
        }
        if (this.getControlValue('SeqNumberTo') !== 0 && !this.getControlValue('SeqNumberTo')) {
            this.riExchange.riInputElement.markAsError(this.uiForm, 'SeqNumberTo');
            isValid = false;
        }
        if (isValid) {
            search = this.getURLSearchParamObject();
            formData = {};

            search.set(this.serviceConstants.Action, '6');

            formData['ActionType'] = 'PlanSubmit';
            formData['BranchNumber'] = this.getControlValue('BranchNumber');
            formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode').trim();
            formData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
            formData['EndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate'));
            formData['FromDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('FromDate'));
            formData['UpToDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('UpToDate'));
            formData['ServiceTypeCode'] = this.getControlValue('InServiceTypeCode');
            formData['NegBranchNumber'] = this.getControlValue('NegBranchNumber');
            formData['ContractNumberSearch'] = this.getControlValue('ContractNumberSearch');
            formData['ContractNameSearch'] = this.getControlValue('ContractNameSearch');
            formData['TownSearch'] = this.getControlValue('TownSearch');
            formData['SequenceNumber'] = this.getControlValue('SequenceNumber');
            formData['SeqNumberFrom'] = this.getControlValue('SeqNumberFrom');
            formData['SeqNumberTo'] = this.getControlValue('SeqNumberTo');
            formData['PlanDays'] = this.getControlValue('PlanDays');
            formData['ContractTypeFilter'] = this.getControlValue('ContractTypeFilter');
            formData['DisplayFilter'] = this.getControlValue('DisplayFilter');
            formData['ConfApptOnly'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('ConfApptOnly'));
            formData['VisitTypeFilter'] = this.getControlValue('VisitTypeFilter');
            formData['ViewDays'] = this.getControlValue('ViewDays');
            formData['PlanningStatusFilter'] = this.getControlValue('PlanningStatus');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);

                let errorList: Array<string> = [];
                if (data.ErrorMessageDesc || data.errorMessage) {
                    errorList.push(data.ErrorMessageDesc || data.errorMessage);
                }
                if (data.TimeWarningMessage) { errorList.push(data.TimeWarningMessage); }
                if (errorList.length > 0) {
                    this.modalAdvService.emitMessage(new ICabsModalVO(errorList));
                }
                this.pageParams.riGridHandle = this.utils.randomSixDigitString();
                this.pageParams.riCacheRefresh = true;
                this.riGrid.RefreshRequired();
                this.riGridBeforeExecute();

            }).catch((error) => this.handleError(error));
        }
    }

    public onClickCmdUndoSelection(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.PageSpecificMessage.seServicePlanningGrid.undoSelectionConfirm, null, this.fetchUndoSelection.bind(this)));
    }

    public onClickCmdGotoDiary(): void {
        let iCount: any = this.getControlValue('BranchServiceAreaCount');
        iCount = this.utils.CInt(iCount);
        if (iCount === 1) {
            this.navigate(ServicePlanningGridHelper.SERVICE_PLANNING, GoogleMapPagesModuleRoutes.ICABSATECHNICIANVISITDIARYGRID, {
                StartDate: this.getControlValue('StartDate'),
                EndDate: this.getControlValue('EndDate')
            });
        } else {
            this.navigate(ServicePlanningGridHelper.SERVICE_PLANNING, GoogleMapPagesModuleRoutes.ICABSAMULTITECHVISITDIARYGRID, {
                StartDate: this.getControlValue('StartDate'),
                EndDate: this.getControlValue('EndDate')
            });
        }
    }
    //End: REST API Call functionality

    public dateCheck(): boolean {
        let returnData: any = false, sDate: any, eDate: any, diff: number;
        sDate = this.getControlValue('StartDate');
        eDate = this.getControlValue('EndDate');
        if (sDate && eDate) {
            sDate = this.globalize.parseDateStringToDate(sDate);
            eDate = this.globalize.parseDateStringToDate(eDate);
            diff = this.utils.dateDiffInDays(sDate, eDate) + 1;
            if (diff > 7) {
                returnData = true;
                this.setControlValue('EndDate', this.utils.addDays(eDate, 6));
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.seServicePlanningGrid.iui7808DateCheckError));
                this.setFocus.startDate.emit(true);
                this.riGrid.ResetGrid();
                this.buildGrid();
                this.riGrid.RefreshRequired();
            } else {
                return this.compareWeekNumber();
            }
        }
        //false: no error, true: error
        return returnData;
    }

    public onChangeGridPageSize(): void {
        if (this.getControlValue('GridPageSize')) {
            this.riGrid.PageSize = this.utils.CInt(this.getControlValue('GridPageSize'));
            this.pageSize = this.riGrid.PageSize;
        }
    }

    //Start: Checkbox functionality
    public onClickCheckBox(): void {
        this.isBuildGridRequired = true;
    }

    public onClickConfApptOnly(e: any): void {
        this.pageParams.cmdDefaultRoutinesLbl = (e.target.checked) ? MessageConstant.PageSpecificMessage.seServicePlanningGrid.defaultAppointments : MessageConstant.PageSpecificMessage.seServicePlanningGrid.defaultRoutines;
    }

    public onClickCManualDates(e: any): void {
        if (e.target.checked) {
            this.clearGrid();
            this.riGrid.RefreshRequired();
            this.riGrid.ResetGrid();
            this.isHidePagination = true;
            this.riExchange.riInputElement.Enable(this.uiForm, 'StartDate');
            this.riExchange.riInputElement.Enable(this.uiForm, 'EndDate');
            this.pageParams.isStartDateDisabled = false;
            this.pageParams.isEndDateDisabled = false;
            this.setControlValue('ODateFrom', this.getControlValue('StartDate'));
            this.setControlValue('ODateTo', this.getControlValue('EndDate'));
            this.setControlValue('StartDate', '');
            this.setControlValue('EndDate', '');

        } else {
            this.clearGrid();
            this.riGrid.RefreshRequired();
            this.riGrid.ResetGrid();
            this.setControlValue('StartDate', this.getControlValue('ODateFrom'));
            this.setControlValue('EndDate', this.getControlValue('ODateTo'));
            this.riExchange.riInputElement.Disable(this.uiForm, 'StartDate');
            this.riExchange.riInputElement.Disable(this.uiForm, 'EndDate');
            this.pageParams.isStartDateDisabled = true;
            this.pageParams.isEndDateDisabled = true;
            this.setControlValue('ODateFrom', '');
            this.setControlValue('ODateTo', '');
            this.setFocus.branchServiceAreaCode.emit(true);
            this.setControlValue('StartDate', this.helper.dateAdd(this.pageParams.vEndofWeekDate, 1));
            this.setControlValue('EndDate', this.helper.dateAdd(this.pageParams.vEndofWeekDate, 7));
            this.setControlValue('UpToDate', this.helper.dateAdd(this.pageParams.vEndofWeekDate, 7));
            this.setControlValue('ServicePlanNumber', '0');
            this.pageParams.isTrNewPlan = true;
            this.pageParams.isTrAdjustPlan = false;
            this.buildGrid();
            this.getLatestWeekNumber();
            this.riGrid.RefreshRequired();
        }
    }
    //End: Checkbox functionality

    public onClickCmdShowFilter(): void {
        this.pageParams.isDivHidden = !this.pageParams.isDivHidden;
        this.pageParams.cmdShowFilterLbl = (this.pageParams.isDivHidden) ? MessageConstant.PageSpecificMessage.seServicePlanningGrid.showFilters : MessageConstant.PageSpecificMessage.seServicePlanningGrid.hideFilters;
    }

    public onClickCmdSummary(): void {
        /*if (this.pageParams.blnRefreshRequired) {
            this.riGrid.RefreshRequired();
            this.riGridBeforeExecute();
        }*/
        if (this.getControlValue('BranchServiceAreaCode')) {
            this.navigate(ServicePlanningGridHelper.SERVICE_PLANNING, InternalGridSearchSalesModuleRoutes.ICABSSESERVICEPLANNINGSUMMARYGRID, { CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter });
        }
    }

    public onClickCancel(): void {
        let mode: string;
        mode = (this.parentMode === ServicePlanningGridHelper.SERVICE_PLAN) ? 'Confirm' : 'Plan';
        this.navigate(mode, InternalMaintenanceApplicationModuleRoutes.ICABSAVISITCANCELLATIONMAINTENANCE, { 'CancelRowid': this.getControlValue('CancelRowid') });
    }

    public onClickBranchSummaryButton(): void {
        this.navigate(ServicePlanningGridHelper.BRANCH_SERVICE_PLANNING_SUMMARY, InternalGridSearchSalesModuleRoutes.ICABSSESERVICEPLANNINGSUMMARYGRID, { CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter });
    }
    /**
     * Function is used to fethc grid data from server.
     * @param areaCode
     * @param allAreas
     */
    private fetchGridDataOnTick(areaCode: any, allAreas: string): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {}, nextServiceVisitDate: any;
        search.set(this.serviceConstants.Action, '2');

        //grid parameters
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.riGridHandle;
        formData[this.serviceConstants.GridPageSize] = this.pageSize.toString();
        formData[this.serviceConstants.GridPageCurrent] = this.pageParams.curPage.toString();
        formData['riSortOrder'] = this.pageParams.riGridSortOrder;
        formData['HeaderClickedColumn'] = this.pageParams.riGridHeaderClickedColumn;
        formData['riCacheRefresh'] = true;

        // set parameters
        formData['BranchNumber'] = this.getControlValue('BranchNumber');
        formData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
        formData['EndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate'));
        formData['FromDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('FromDate'));
        formData['UpToDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('UpToDate'));
        formData['VisitTypeFilter'] = this.getControlValue('VisitTypeFilter');
        formData['BranchServiceAreaCode'] = areaCode.trim();
        formData['AllBranchServiceAreas'] = allAreas;
        formData['ViewDays'] = this.getControlValue('ViewDays');
        formData['PlanningStatus'] = this.getControlValue('PlanningStatus');
        formData['NegBranchNumber'] = this.getControlValue('NegBranchNumber');
        formData['ServiceTypeCode'] = this.getControlValue('InServiceTypeCode');
        formData['SequenceNumber'] = this.getControlValue('SequenceNumber');
        formData['ContractNumber'] = this.getControlValue('ContractNumberSearch');
        formData['ContractName'] = this.getControlValue('ContractNameSearch');
        formData['TownName'] = this.getControlValue('TownSearch');
        formData['ServicePlanNumber'] = this.getControlValue('ServicePlanNumber');
        formData['ContractTypeFilter'] = this.getControlValue('ContractTypeFilter');
        formData['DisplayFilter'] = this.getControlValue('DisplayFilter');
        formData['CancelRowid'] = this.getControlValue('CancelRowid');
        formData['PlanFunction'] = this.utils.booleanToString(this.pageParams.isBlnPlanFunction);
        formData['UpdateNotification'] = this.utils.booleanToString(this.pageParams.isBlnUpdateNotification);
        formData['DisplayTimes'] = this.utils.booleanToString(this.getControlValue('DisplayTimes'));
        formData['DisplayAverageWeight'] = this.utils.booleanToString(this.getControlValue('DisplayAverageWeight'));
        formData['DisplayServiceType'] = this.utils.booleanToString(this.getControlValue('DisplayServiceType'));

        if (this.pageParams.vSAreaSeqGroupAvail) {
            formData['SeqNoTo'] = this.getControlValue('SeqNoTo');
        }
        if (this.getControlValue('ConfApptOnly')) {
            formData['ConfApptOnly'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('ConfApptOnly'));
        }
        if (this.pageParams.isBlnPlanFunction) {
            formData['PlanDate'] = this.getAttribute('PlanDate');
        }

        if (this.getAttribute('Row') !== '') {
            formData['ROWID'] = this.getAttribute('ServiceCoverRowID');
            formData['VisitTypeCode'] = this.getAttribute('VisitTypeCode');
            nextServiceVisitDate = this.getAttribute('NextServiceVisitDate');
            if (nextServiceVisitDate) {
                nextServiceVisitDate = nextServiceVisitDate.split('*');
                nextServiceVisitDate = this.globalize.parseDateToFixedFormat(nextServiceVisitDate[0]);
            }
            formData['NextServiceVisitDate'] = nextServiceVisitDate;
            formData['RowCount'] = this.getAttribute('Row');
            formData['PlanVisitRowID'] = this.getAttribute('PlanVisitRowID');
        } else {
            this.setAttribute('Row', '0');
        }

        let googleData: any = this._ls.retrieve('GOOGLEDATA');
        let workerService: WebWorkerService;
        let worker: AppWebWorker = new AppWebWorker();
        workerService = new WebWorkerService(HttpWorker, environment);
        workerService.run(worker.delegatePromise, {
            url: this.queryParams.method,
            method: 'POST',
            token: this._ls.retrieve('token'),
            base: this._authService.getBaseURL(),
            search: {
                businessCode: this.utils.getBusinessCode(),
                countryCode: this.utils.getCountryCode(),
                action: '2',
                email: googleData.email
            },
            postHeaders: {
                module: this.queryParams.module,
                operation: this.queryParams.operation
            },
            formData: formData
        }, true);
        workerService.worker.onmessage = response => {
            let data: any = response.data.oResponse;
            try {
                //keep update footer data
                this.gridFooterDataLatest = this.helper.getGridFooterData(data);
                // Update Grid Footer in UI
                if (this.gridFooterDataLatest) {
                    const arrInfo = this.gridFooterDataLatest.split('|');
                    this.setTotalCallsUnitsTimesNettValues(arrInfo);
                }
                //Date need to reset after grid call
                this.pageParams.vbPlanDate = '';
                if (data.hasError || data.fullError || data.errorMessage) {
                    if (this.riGrid.Details.GetValue('ContractNum')) {
                        this.zone.run(() => {
                            this.modalAdvService.emitError(new ICabsModalVO(this.customErrorMessage + (data.errorMessage ? data.errorMessage : ''), data.fullError));
                        });
                    }
                    this.pageParams.isBlnPlanFunction = false;
                }
                else {
                    if (this.getAttribute('Row') !== '') {
                        this.riGrid.Update = true;
                        this.riGrid.StartRow = formData['RowCount'];
                        this.riGrid.RowID = formData['ROWID'];
                        this.riGrid.StartColumn = 0;
                        this.riGrid.UpdateHeader = false;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                    }
                    this.zone.run(() => {
                        this.riGrid.Execute(data);
                        if (data.pageData && (data.pageData.lastPageNumber * this.pageSize) > 0) {
                            this.isHidePagination = false;
                        } else {
                            this.isHidePagination = true;
                        }
                    });
                }
                this.isTicked = false;
                this.riGrid.Update = false;
            } catch (err) {
                if (this.isTicked) {
                    this.isTicked = false;
                }
                this.handleError(err);
            }
        };
    }

    private fetchGridData(areaCode: any, allAreas: string): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {}, nextServiceVisitDate: any;
        search.set(this.serviceConstants.Action, '2');

        //grid parameters
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.riGridHandle;
        formData[this.serviceConstants.GridPageSize] = this.pageSize.toString();
        formData[this.serviceConstants.GridPageCurrent] = this.pageParams.curPage.toString();
        formData['riSortOrder'] = this.pageParams.riGridSortOrder;
        formData['HeaderClickedColumn'] = this.pageParams.riGridHeaderClickedColumn;
        formData['riCacheRefresh'] = this.pageParams.riCacheRefresh;

        // set parameters
        formData['BranchNumber'] = this.getControlValue('BranchNumber');
        formData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
        formData['EndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate'));
        formData['FromDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('FromDate'));
        formData['UpToDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('UpToDate'));
        formData['VisitTypeFilter'] = this.getControlValue('VisitTypeFilter');
        formData['BranchServiceAreaCode'] = areaCode.trim();
        formData['AllBranchServiceAreas'] = allAreas;
        formData['ViewDays'] = this.getControlValue('ViewDays');
        formData['PlanningStatus'] = this.getControlValue('PlanningStatus');
        formData['NegBranchNumber'] = this.getControlValue('NegBranchNumber');
        formData['ServiceTypeCode'] = this.getControlValue('InServiceTypeCode');
        formData['SequenceNumber'] = this.getControlValue('SequenceNumber');
        formData['ContractNumber'] = this.getControlValue('ContractNumberSearch');
        formData['ContractName'] = this.getControlValue('ContractNameSearch');
        formData['TownName'] = this.getControlValue('TownSearch');
        formData['ServicePlanNumber'] = this.getControlValue('ServicePlanNumber');
        formData['ContractTypeFilter'] = this.getControlValue('ContractTypeFilter');
        formData['DisplayFilter'] = this.getControlValue('DisplayFilter');
        formData['CancelRowid'] = this.getControlValue('CancelRowid');
        formData['PlanFunction'] = this.utils.booleanToString(this.pageParams.isBlnPlanFunction);
        formData['UpdateNotification'] = this.utils.booleanToString(this.pageParams.isBlnUpdateNotification);
        formData['DisplayTimes'] = this.utils.booleanToString(this.getControlValue('DisplayTimes'));
        formData['DisplayAverageWeight'] = this.utils.booleanToString(this.getControlValue('DisplayAverageWeight'));
        formData['DisplayServiceType'] = this.utils.booleanToString(this.getControlValue('DisplayServiceType'));

        if (this.pageParams.vSAreaSeqGroupAvail) {
            formData['SeqNoTo'] = this.getControlValue('SeqNoTo');
        }
        if (this.getControlValue('ConfApptOnly')) {
            formData['ConfApptOnly'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('ConfApptOnly'));
        }
        if (this.pageParams.isBlnPlanFunction) {
            formData['PlanDate'] = this.pageParams.vbPlanDate;
        }

        if (this.riGrid.Update) {
            if (this.getAttribute('Row') !== '') {
                formData['ROWID'] = this.getAttribute('ServiceCoverRowID');
                formData['VisitTypeCode'] = this.getAttribute('VisitTypeCode');
                nextServiceVisitDate = this.riGrid.Details.GetValue('NextServiceVisitDate');
                if (nextServiceVisitDate) {
                    nextServiceVisitDate = nextServiceVisitDate.split('*');
                    nextServiceVisitDate = this.globalize.parseDateToFixedFormat(nextServiceVisitDate[0]);
                }
                formData['NextServiceVisitDate'] = nextServiceVisitDate;
                formData['RowCount'] = this.getAttribute('Row');
                formData['PlanVisitRowID'] = this.getAttribute('PlanVisitRowID');
            } else {
                this.setAttribute('Row', '0');
            }
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData).subscribe(
            (data) => {
                //keep update footer data
                this.gridFooterDataLatest = this.helper.getGridFooterData(data);
                //Date need to reset after grid call
                this.pageParams.vbPlanDate = '';
                if (!this.handleSuccessError(data)) {
                    if (this.riGrid.Update) {
                        if (this.getAttribute('Row') !== '') {
                            this.riGrid.StartRow = this.getAttribute('Row');
                            this.riGrid.RowID = this.getAttribute('ServiceCoverRowID');
                            this.riGrid.StartColumn = 0;
                            this.riGrid.UpdateHeader = false;
                            this.riGrid.UpdateBody = true;
                            this.riGrid.UpdateFooter = true;
                        }
                    } else {
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageSize : 1;
                        this.pageParams.curPage = data.pageData ? data.pageData.pageNumber : 1;
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.curPage);
                        }, 500);
                    }
                    this.riGrid.RefreshRequired();
                    this.riGrid.Execute(data);

                    if (data.pageData && (data.pageData.lastPageNumber * this.pageSize) > 0) {
                        this.isHidePagination = false;
                    } else {
                        this.isHidePagination = true;
                    }
                } else {
                    this.pageParams.isBlnPlanFunction = false;
                }
                this.riGrid.Update = false;
                //this.pageParams.riCacheRefresh = false;
            },
            (error) => {
                this.handleError(error);
            });
    }

    private clearGrid(donotClearColumns?: boolean): void {
        this.totalRecords = 1;
        this.pageParams.curPage = 1;
        if (!donotClearColumns) {
            this.riGrid.Clear();
        }
        this.riGrid.RefreshRequired();
    }
}
