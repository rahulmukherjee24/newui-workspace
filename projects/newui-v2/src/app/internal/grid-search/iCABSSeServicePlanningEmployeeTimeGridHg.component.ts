import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { ContractManagementModuleRoutes, InternalMaintenanceServiceModuleRoutes, InternalGridSearchSalesModuleRoutes } from './../../base/PageRoutes';
import { DropdownStaticComponent } from '../../../shared/components/dropdown-static/dropdownstatic';

@Component({
    templateUrl: 'iCABSSeServicePlanningEmployeeTimeGridHg.html'
})

export class ServicePlanningEmployeeTimeGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('option') option: DropdownStaticComponent;

    public pageId: string = '';
    private method: string = 'service-planning/maintenance';
    private module: string = 'planning';
    private operation: string = 'Service/iCABSSeServicePlanningEmployeeTimeGridHg';
    private currentContractType: string;
    private currentContractTypeActual: string;

    public isVisiblePagination: boolean = false;
    public totalRecords: number = 0;
    public pageSize: number = 10;
    public curPage: number = 1;
    public optionDropdownOptions: Array<any> = [];
    public legend: Array<any> = [];
    public controls = [
        { name: 'EmployeeCode', disabled: true },
        { name: 'EmployeeSurname', disabled: true },
        { name: 'StartDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'EndDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'PlannedVisitDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'PlanVisitRowID' },
        { name: 'WeekNumber', disabled: true },
        { name: 'ServicePlanNumber' },
        { name: 'Option' }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNINGEMPLOYEETIMEGRIDHG;
        this.browserTitle = 'Employee Time';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageTitle = 'Employee Time';
        this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.setAttribute('RowID', this.riExchange.getParentHTMLValue('EmployeeRowID'));
        this.setControlValue('ServicePlanNumber', this.riExchange.getParentHTMLValue('ServicePlanNumber'));
        this.setControlValue('StartDate', this.riExchange.getParentHTMLValue('StartDate'));
        this.setControlValue('EndDate', this.riExchange.getParentHTMLValue('EndDate'));
        this.setControlValue('WeekNumber', this.riExchange.getParentHTMLValue('WeekNumber'));
        this.setControlValue('PlanVisitRowID', this.riExchange.getParentHTMLValue('PlanVisitRowID'));
        this.setControlValue('PlannedVisitDate', this.riExchange.getParentHTMLValue('PlannedVisitDate'));
        this.createOptions();
        this.populateEmployeeDetails();
        this.legend = [{ label: 'Employee Times For Selected Visit', color: 'rgb(255, 255, 204)' }];
        this.buildGrid();
    }


    private createOptions(): void {
        this.optionDropdownOptions = [
            { text: 'Options', value: '' },
            { text: 'Add New Work', value: 'Add' }
        ];
    }

    private populateEmployeeDetails(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        search.set('ROWID', this.getAttribute('RowID'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.method, this.module, this.operation, search)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data) {
                    this.setControlValue('EmployeeCode', data.EmployeeCode);
                    this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                }
            },
            error => {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('PlannedVisitDate', 'ServicePlanningEmpTime', 'PlannedVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('StartTime', 'ServicePlanningEmpTime', 'StartTime', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('EndTime', 'ServicePlanningEmpTime', 'EndTime', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('VisitDuration', 'ServicePlanningEmpTime', 'VisitDuration', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('VisitValue', 'ServicePlanningEmpTime', 'VisitValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumn('ContractNumber', 'ServicePlanningEmpTime', 'ContractNumber', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('PremiseNumber', 'ServicePlanningEmpTime', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('PremiseName', 'ServicePlanningEmpTime', 'PremiseName', MntConst.eTypeText, 25);
        this.riGrid.AddColumn('ProductCode', 'ServicePlanningEmpTime', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.Complete();
        this.riGridBeforeExecute();
    }

    //fetch the grid records
    private riGridBeforeExecute(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set('EmployeeRowid', this.getAttribute('RowID'));
        search.set('PlanVisitRowID', this.getControlValue('PlanVisitRowID'));
        search.set('VisitDate', this.getControlValue('PlannedVisitDate'));
        search.set('ServicePlanNumber', this.getControlValue('ServicePlanNumber'));
        search.set('StartDate', this.getControlValue('StartDate'));
        search.set('EndDate', this.getControlValue('EndDate'));
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.GridCacheRefresh, 'True');
        search.set(this.serviceConstants.PageSize, '10');
        search.set(this.serviceConstants.GridPageCurrent, this.curPage.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.method, this.module, this.operation, search)
            .subscribe(
            (data) => {
                if (data['errorMessage']) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.curPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageSize : 1;
                    this.isVisiblePagination = true;
                    if (this.totalRecords === 0) {
                        this.isVisiblePagination = false;
                    }
                    this.riGrid.UpdateHeader = true;
                    this.riGrid.UpdateBody = true;
                    this.riGrid.UpdateFooter = true;
                    this.riGrid.Execute(data);
                }

                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            (error) => {
                this.errorService.emitError(error);
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private setAttributes(): void {
        this.currentContractTypeActual = this.riGrid.Details.GetAttribute('EndTime', 'additionalproperty');
        switch (this.currentContractTypeActual) {
            case 'C':
                this.currentContractType = '';
                break;
            case 'J':
                this.currentContractType = '<job>';
                break;
            case 'P':
                this.currentContractType = '<product>';
                break;
        }
        this.setAttribute('ContractRowID', this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty'));
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('PremiseNumber', 'additionalproperty'));
        this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'additionalproperty'));
        this.setAttribute('PlanVisitDetailRowID', this.riGrid.Details.GetAttribute('VisitDuration', 'additionalproperty'));
        this.setAttribute('Row', this.riGrid.CurrentRow);
    }

    // On double click of the grid row
    public riGridBodyOnDblClick(event: any): void {
        let columnName: string = this.riGrid.CurrentColumnName;
        this.setAttributes();
        switch (columnName) {
            case 'ContractNumber':
                this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                    currentContractTypeURLParameter: this.currentContractType,
                    ContractRowID: this.getAttribute('ContractRowID')
                });
                break;
            case 'PremiseNumber':
                this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    contractTypeCode: this.currentContractTypeActual,
                    PremiseRowID: this.getAttribute('PremiseRowID')
                });
                break;
            case 'ProductCode':
                if (this.currentContractType) {
                    this.navigate('ServicePlanning', InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE, {
                        contractTypeCode: this.currentContractType
                    });
                } else {
                    this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                        currentContractTypeURLParameter: this.currentContractType,
                        ServiceCoverRowID: this.getAttribute('ServiceCoverRowID')
                    });
                }
                break;
            case 'VisitDuration':
                this.navigate('ServicePlanDescription', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEPLANNINGEMPLOYEETIMEMAINTENANCEHG, {
                    Row: this.getAttribute('Row'),
                    PlanVisitDetailRowID: this.getAttribute('PlanVisitDetailRowID'),
                    PlanVisitRowID: this.riGrid.Details.GetAttribute('VisitValue', 'additionalproperty'),
                    EmployeeMaintenanceMode: 'Update'
                });
                break;
        }
    }

    //grid refresh
    public refresh(): void {
        this.buildGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.curPage = currentPage.value;
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateRow = true;
        this.riGrid.UpdateFooter = true;
        this.riGridBeforeExecute();
    }

    public onOptionChange(data: any): void {
        this.setControlValue('Option', data);
        if (data === 'Add') {
            this.navigate(this.parentMode, InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEPLANNINGEMPLOYEETIMEMAINTENANCEHG, {
                EmployeeMaintenanceMode: 'New'
            });
        }
    }
}
