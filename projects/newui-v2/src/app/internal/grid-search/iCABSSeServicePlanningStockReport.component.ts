import { Component, OnInit, OnDestroy } from '@angular/core';

import { PageIdentifier } from '../../base/PageIdentifier';
import { MessageConstant } from '../../../shared/constants/message.constant';

@Component({
    templateUrl: 'iCABSSeServicePlanningStockReport.html'
})
export class ServicePlanningStockReportComponent implements OnInit, OnDestroy {
    public pageId: string;
    public isShowHeader: boolean = true;
    public isShowCloseButton: boolean = true;
    public browserTitle: string;
    public pageTitle: string;

    constructor() {
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNINGGRID;
        this.browserTitle = MessageConstant.PageSpecificMessage.seServicePlanningGrid.browserTitle;
        this.pageTitle = this.browserTitle + MessageConstant.PageSpecificMessage.seServicePlanningGrid.stockReport;
    }

    ngOnInit(): void { return; }

    ngOnDestroy(): void { return; }
}
