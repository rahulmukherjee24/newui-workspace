import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { OnInit, ViewChild, Injector, Component, OnDestroy } from '@angular/core';

import { Observable, Subscription, ReplaySubject } from 'rxjs';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { BIReportsRoutes, InternalGridSearchServiceModuleRoutes } from './../../base/PageRoutes';
import { BranchServiceAreaSearchComponent } from './../search/iCABSBBranchServiceAreaSearch';
import { EmployeeSearchComponent } from './../search/iCABSBEmployeeSearch';
import { ProductServiceGroupSearchComponent } from './../search/iCABSBProductServiceGroupSearch.component';
import { GlobalNotificationsService } from '@shared/services/global.notifications.service';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';

@Component({
    templateUrl: 'iCABSSESummaryWorkloadGridMonthBranch.html',
    styles: [`
        :host /deep/ .gridtable.table-bordered>thead>tr>td,
        :host /deep/ .gridtable.table-bordered>tbody>tr>td,
        :host /deep/ .gridtable.table-bordered>tfoot>tr>td {
            padding: 2px 1px;
            font-size: 12px;
        }
        :host /deep/ .gridtable.table-bordered>thead>tr>th,
        :host /deep/ .gridtable.table-bordered>tbody>tr>th,
        :host /deep/ .gridtable.table-bordered>tfoot>tr>th {
            padding: 0;
            font-size: 12px;
        }
        :host /deep/ .gridtable.table-bordered>tbody>tr>td input.form-control {
            padding-right: 0;
            padding-left: 0;
            font-size: 10px;
            width: 50px!important;
        }
        :host /deep/ .gridtable.table-bordered>tbody>tr>td:nth-child(2) {
            text-align: center!important;
        }
    `]
})

export class SummaryWorkloadGridMonthBranchComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('ddProductServiceGroupSearch') ddProductServiceGroupSearch: ProductServiceGroupSearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riPagination') riPagination: PaginationComponent;
    private muleConfig: any = {
        method: 'service-planning/grid',
        module: 'areas',
        operation: 'Service/iCABSSESummaryWorkloadGridMonthBranch'
    };
    private subscriptionManager: Subscription;
    private oldFormData: Object = {};
    private isReturningStatus: Boolean = false;
    public gridConfig: any = {
        totalRecords: 0,
        gridCurPage: 1,
        pageSize: 10
    };
    public pageId: string = '';
    public legend: Array<any> = [];
    public totalLabel: string = 'Total Visits';
    public viewByOptions: Array<any> = [];
    public contractTypeOptions: Array<any> = [];
    public selectedYearOptions: Array<any> = [];
    public isPaginationEnabled: boolean = false;
    public productSearchGroupDesc: string;
    public isGridRequesting: boolean = false;
    public fieldVisibility: any = {
        trInterval: true,
        averageWeight: true,
        idWeight: true,
        totalWED: false,
        totalVisits: true
    };
    public ellipsisConfig: any = {
        branchServiceAreaCode: {
            autoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'LookUp-Emp',
                ServiceBranchNumber: this.utils.getBranchCode(),
                BranchName: this.utils.getBranchTextOnly(this.utils.getBranchCode()),
                currentContractType: this.riExchange.getCurrentContractType(),
                currentContractTypeURLParameter: this.riExchange.getCurrentContractTypeLabel()
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: BranchServiceAreaSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        },
        supervisorEmployeeCode: {
            isAutoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'ContactManagement-Originating',
                showAddNew: false,
                serviceBranchNumber: this.utils.getBranchCode()
            },
            contentComponent: EmployeeSearchComponent,
            showHeader: true,
            disabled: false
        }
    };

    public dropdownConfig = {
        productServiceGroupSearch: {
            params: {
                parentMode: 'LookUp-String',
                ProductServiceGroupString: '',
                SearchValue3: '',
                ProductServiceGroupCode: ''
            },
            active: { id: '', text: '' },
            isDisabled: false,
            isRequired: false
        }
    };

    public controls: Array<any> = [
        { name: 'BusinessCode', type: MntConst.eTypeCode },
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeText },
        { name: 'ProductServiceGroupString', type: MntConst.eTypeCode },
        { name: 'VisitFrequencyFilter', type: MntConst.eTypeInteger },
        { name: 'SelectedYear', type: MntConst.eTypeInteger },
        { name: 'TotalVisits', type: MntConst.eTypeInteger },
        { name: 'TotalUnits', type: MntConst.eTypeInteger },
        { name: 'TotalWED', type: MntConst.eTypeDecimal1 },
        { name: 'TotalTime', type: MntConst.eTypeText },
        { name: 'ProdServGroupString', type: MntConst.eTypeCode },
        { name: 'PostalBrickString', type: MntConst.eTypeCode },
        { name: 'VisitIntervalString', type: MntConst.eTypeCode },
        { name: 'AverageWeight', type: MntConst.eTypeDecimal2 },
        { name: 'SelectedArea', type: MntConst.eTypeCode },
        { name: 'SelectedDate', type: MntConst.eTypeDate },
        { name: 'SupervisorEmployeeCode', type: MntConst.eTypeCode },
        { name: 'SupervisorSurname', type: MntConst.eTypeText },
        { name: 'ViewTypeFilter', value: 'Visit' },
        { name: 'SelectedMonth' },
        { name: 'ViewByFilter', value: 'StaticDate' },
        { name: 'ContractTypeFilter' },
        { name: 'GridCacheKey' },
        { name: 'riGridHandle' }
    ];

    constructor(injector: Injector, private notification: GlobalNotificationsService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESUMMARYWORKLOADGRIDMONTHBRANCH;
        this.pageTitle = this.browserTitle = 'Branch Summary Workload';
        this.subscriptionManager = new Subscription();
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            this.isReturningStatus = true;
            this.gridConfig = this.pageParams.gridConfig;
            this.oldFormData = this.formData;
            if (this.pageParams['ProductServiceGroupSearch']) {
                this.dropdownConfig.productServiceGroupSearch.active = {
                    id: this.pageParams.ProductServiceGroupSearch['ProductServiceGroupString'],
                    text: this.pageParams.ProductServiceGroupSearch['ProductServiceGroupString'] + ' - ' + this.pageParams.ProductServiceGroupSearch['ProductServiceGroupDesc']
                };
            }
            this.windowOnload();
            setTimeout(() => {
                this.populateUIFromFormData();
                this.buildGrid();
                this.refresh();
            }, 0);
        }
        else {
            this.getSysCharValue();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.subscriptionManager) {
            this.subscriptionManager.unsubscribe();
        }
    }

    /**
     * Get syschar value from service and assign it into page params
     */
    private getSysCharValue(): void {
        this.fetchSysChar().subscribe(
            (e) => {
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    return false;
                }
                if (e.records && e.records.length > 0) {
                    this.pageParams.vDummy = e.records[0].Required ? e.records[0].Required : false;
                    this.pageParams.vEnableWeeklyVisitPattern = e.records[0].Logical ? e.records[0].Logical : false;
                    this.pageParams.vEnableWED = e.records[1].Required ? e.records[1].Required : false;
                }
                this.windowOnload();
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private fetchSysChar(): any {
        let querySysChar: QueryParams = this.getURLSearchParamObject();
        let sysCharNumbers = this.sysCharParameters();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.SystemCharNumber, sysCharNumbers);
        return this.httpService.sysCharRequest(querySysChar);
    }

    private sysCharParameters(): string {
        let sysCharList = [
            this.sysCharConstants.SystemCharEnableWeeklyVisitPattern,
            this.sysCharConstants.SystemCharEnableWED
        ];
        return sysCharList.join(',');
    }

    private buildMenuOptions(): void {
        this.viewByOptions = [];
        this.contractTypeOptions = [];
        this.viewByOptions.push({ value: 'StaticDate', text: 'Static Date' });
        this.viewByOptions.push({ value: 'PlanDueDate', text: 'Plan Due Date' });
        this.viewByOptions.push({ value: 'PlannedDate', text: 'Planned Date' });
        this.viewByOptions.push({ value: 'NotInConfirmedPlan', text: 'Not in Confirmed Plan' });
        this.viewByOptions.push({ value: 'PlanDueAndPlannedDates', text: 'Plan Due & Planned Dates' });

        //Build Menu Options for ContractType
        this.contractTypeOptions.push({ value: '', text: 'All' });
        this.contractTypeOptions.push({ value: 'C', text: 'Contract' });
        this.contractTypeOptions.push({ value: 'J', text: 'Job' });
        //possible need to do a syschar check to see if we should add Product Sale
        this.contractTypeOptions.push({ value: 'P', text: 'Product Sale' });
    }

    private buildYearOptions(): void {
        this.selectedYearOptions = [];
        let date: Date = new Date();
        for (let i = 1; i <= 7; i++) {
            let val: number = date.getFullYear() - 5 + i;
            this.selectedYearOptions.push({ value: val, text: val });
            if (date.getFullYear() === val) this.setControlValue('SelectedYear', val);
        }
    }

    private windowOnload(): void {
        this.buildMenuOptions();
        this.setPlanningDiaryOptions();

        if (this.pageParams.vEnableWeeklyVisitPattern) {
            this.fieldVisibility['trInterval'] = true;
        } else {
            this.fieldVisibility['trInterval'] = false;
        }

        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = this.gridConfig.pageSize;

        this.disableControl('BranchName', true);
        this.disableControl('EmployeeSurname', true);
        this.disableControl('TotalVisits', true);
        this.disableControl('TotalWED', true);
        this.disableControl('TotalUnits', true);
        this.disableControl('TotalTime', true);
        this.disableControl('AverageWeight', true);
        this.disableControl('ProdServGroupString', true);
        this.disableControl('PostalBrickString', true);
        this.disableControl('VisitIntervalString', true);
        this.disableControl('SelectedArea', true);
        this.disableControl('SelectedDate', true);
        this.disableControl('SupervisorSurname', true);

        this.buildYearOptions();
        this.setControlValue('BusinessCode', this.utils.getBusinessCode());
        this.setControlValue('BranchName', this.utils.getBranchText());
        this.setControlValue('SelectedMonth', new Date().getMonth() + 1);

        document.getElementById('BranchServiceAreaCode').focus();
        this.buildGrid();
        this.getLegendHtml();
        this.pageParams.gridConfig = this.gridConfig;
    }


    private getLegendHtml(): any {
        let postData = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        postData[this.serviceConstants.Function] = 'GetLegend';
        postData['BusinessCode'] = this.utils.getBusinessCode();

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.zone.run(() => {
                        let obj: any = data['HTMLLegend'];
                        this.legend = [];
                        if (obj) {
                            let legend = obj.split('bgcolor=');
                            for (let i = 0; i < legend.length; i++) {
                                let str = legend[i];
                                if (str.indexOf('"#') >= 0) {
                                    this.legend.push({
                                        color: str.substr(str.indexOf('"#') + 1, 7),
                                        label: str.substr(str.indexOf('<td>') + 4, str.substr(str.indexOf('<td>') + 4).indexOf('&'))
                                    });
                                }
                            }
                        }
                    });
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private setPlanningDiaryOptions(): any {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        search.set('ActionType', 'GetPlanningDiaryOptions');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    if (!data['DisplayAverageWeight'] || data['DisplayAverageWeight'].toLowerCase() === 'no') {
                        this.fieldVisibility['averageWeight'] = false;
                        this.fieldVisibility['idWeight'] = false;
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('ServiceArea', 'SummaryWorkload', 'ServiceArea', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('ServiceArea', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EmployeeName', 'SummaryWorkload', 'EmployeeName', MntConst.eTypeText, 40);

        //AIH changed to find and use last day of the selected month for the number od days to remove extra days
        let dtlastdate: Date = new Date(this.getControlValue('SelectedYear'), this.getControlValue('SelectedMonth'), 0);
        let vdayLast = dtlastdate.getDate() * 2;
        for (let iLoop = 1; iLoop <= vdayLast; iLoop++) {
            this.riGrid.AddColumn(iLoop.toString(), 'SummaryWorkload', iLoop.toString(), MntConst.eTypeText, 1);
            this.riGrid.AddColumnAlign(iLoop.toString(), MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('TotalVisits', 'SummaryWorkload', 'TotalVisits', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('TotalVisits', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('TotalCalls', 'SummaryWorkload', 'TotalCalls', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('TotalCalls', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('TotalTime', 'SummaryWorkload', 'TotalTime', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('TotalTime', MntConst.eAlignmentRight);

        this.riGrid.Complete();
    }

    //populate data into the grid and update the pagination control accordingly
    private riGridBeforeExecute(): any {
        this.isPaginationEnabled = true;
        let gridQueryParams: QueryParams = this.getURLSearchParamObject();
        gridQueryParams.set(this.serviceConstants.GridPageSize, this.gridConfig.pageSize.toString());
        gridQueryParams.set(this.serviceConstants.Action, '2');
        gridQueryParams.set(this.serviceConstants.GridMode, '0');
        gridQueryParams.set(this.serviceConstants.GridCacheRefresh, 'True');
        gridQueryParams.set(this.serviceConstants.GridHandle, this.utils.gridHandle);
        gridQueryParams.set(this.serviceConstants.GridSortOrder, 'Descending');
        gridQueryParams.set('Level', 'Branch');
        gridQueryParams.set('SelectedYear', this.getControlValue('SelectedYear'));
        gridQueryParams.set('SelectedMonth', this.getControlValue('SelectedMonth'));
        gridQueryParams.set('BranchNumber', this.utils.getBranchCode());
        gridQueryParams.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        gridQueryParams.set('SupervisorEmployeeCode', this.getControlValue('SupervisorEmployeeCode'));
        gridQueryParams.set('ViewByFilter', this.getControlValue('ViewByFilter'));
        gridQueryParams.set('ViewTypeFilter', this.getControlValue('ViewTypeFilter'));
        gridQueryParams.set('ProductServiceGroupString', this.getControlValue('ProductServiceGroupString') || '');
        gridQueryParams.set('ContractTypeFilter', this.getControlValue('ContractTypeFilter'));
        gridQueryParams.set('VisitFrequencyFilter', this.getControlValue('VisitFrequencyFilter'));
        gridQueryParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridQueryParams.set(this.serviceConstants.PageCurrent, this.gridConfig.gridCurPage.toString());

        this.ajaxSource.next(this.ajaxconstant.START);
        this.isGridRequesting = true;
        this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, gridQueryParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.isGridRequesting = false;
                if (data) {
                    if (data.hasError || data['error_description']) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.gridConfig.totalRecords = 0;
                        this.isPaginationEnabled = false;
                        this.riGrid.ResetGrid();
                    } else {
                        this.gridConfig.gridCurPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 0;
                        this.isPaginationEnabled = (this.gridConfig.totalRecords > 0) ? true : false;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.Execute(data);
                    }

                    this.pageParams.gridConfig = this.gridConfig;
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private getGridDay(): number {
        let val: any = (parseInt(this.riGrid.CurrentColumnName, 0) + 0.5) / 2;
        return this.utils.round(val, 0);
    }

    // Get page specific grid data from service end
    public getCurrentPage(data: any): void {
        this.gridConfig.gridCurPage = data.value;
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    // refresh the grid content
    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public clearGrid(): void {
        this.isPaginationEnabled = false;
        this.buildGrid();
    }

    /**
     * Grid body on Double click event
     */
    public riGridBodyOnDblClick(event: any): void {
        let locationUrl: string = '';
        let rowId: string = this.riGrid.Details.GetAttribute('ServiceArea', 'RowID');
        this.pageParams['gridCollapsed'] = this.riGrid.columnsCollpased;
        if ((!rowId) || (rowId === 'NULLROWID')) {
            return;
        }
        this.setAttribute('BusinessCodeSelectedServiceArea', rowId);
        let mode = 'SummaryWorkload';
        if (this.riGrid.CurrentColumnName === 'ServiceArea') {
            if (this.getControlValue('ViewByFilter') === 'StaticDate') {
                this.navigate('SummaryWorkload', BIReportsRoutes.ICABSSESTATICVISITGRIDYEARBRANCH,
                    {
                        BranchServiceAreaCode: this.riGrid.Details.GetValue('ServiceArea'),
                        EmployeeSurname: this.getControlValue('EmployeeSurname'),
                        ProductServiceGroupString: this.getControlValue('ProductServiceGroupString'),
                        VisitFrequencyFilter: this.getControlValue('VisitFrequencyFilter'),
                        ContractTypeFilter: this.getControlValue('ContractTypeFilter'),
                        ViewByFilter: this.getControlValue('ViewByFilter'),
                        SelectedDate: this.getControlValue('SelectedDate'),
                        SelectedMonth: this.getControlValue('SelectedMonth'),
                        SelectedYear: this.getControlValue('SelectedYear'),
                        DateType: this.getAttribute('BusinessCodeDateType'),
                        reroute: true
                    });
            } else {
                this.notification.displayMessage(MessageConstant.Message.PageNotDeveloped + ' - iCABSSEPlanVisitGridYearBranch.htm', CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
            }

        }
        else if (event.srcElement.getAttribute('AdditionalProperty') === 'Day1') {
            this.setAttribute('BusinessCodeDateType', 'Day');
            this.setAttribute('BusinessCodeSelectedDate', this.utils.formatDate(new Date(this.getControlValue('SelectedYear'), this.getControlValue('SelectedMonth') - 1, this.getGridDay())));
            this.setAttribute('BusinessCodeContractTypeFilter', this.getControlValue('ContractTypeFilter'));

            let gridCellSub: Subscription = this.getGridCellDetails(event).subscribe((e) => {
                setTimeout(() => {
                    if (this.getControlValue('ViewByFilter') === 'StaticDate') {
                        //Navigate to screen Service/iCABSSeSummaryWorkloadRezoneMoveGrid.htm' with <reroute>
                        if (this.riExchange.URLParameterContains('reroute')) {
                            locationUrl = InternalGridSearchServiceModuleRoutes.ICABSSESUMMARYWORKLOADREZONEMOVEGRID.URL_2;
                            this.navigate(mode, locationUrl, { 'reroute': true, 'productSearchGroupDesc': this.productSearchGroupDesc });
                        }
                        else {
                            locationUrl = InternalGridSearchServiceModuleRoutes.ICABSSESUMMARYWORKLOADREZONEMOVEGRID.URL_1;
                            this.navigate(mode, locationUrl, { 'productSearchGroupDesc': this.productSearchGroupDesc });
                        }
                    }
                    else {
                        this.navigate(mode, InternalGridSearchServiceModuleRoutes.ICABSSEPLANVISITGRIDDAYBRANCH, {
                            BranchServiceAreaCode: this.riGrid.Details.GetValue('ServiceArea'),
                            ProductServiceGroupString: this.getControlValue('ProductServiceGroupString'),
                            VisitFrequencyFilter: this.getControlValue('VisitFrequencyFilter'),
                            ContractTypeFilter: this.getControlValue('ContractTypeFilter'),
                            ViewByFilter: this.getControlValue('ViewByFilter'),
                            SelectedDate: this.getControlValue('SelectedDate'),
                            SelectedMonth: this.getControlValue('SelectedMonth'),
                            SelectedYear: this.getControlValue('SelectedYear'),
                            DateType: this.getAttribute('BusinessCodeDateType')
                        });
                    }
                }, 0);
            });
            this.subscriptionManager.add(gridCellSub);
        }
    }

    //Grid body on click event
    public riGridBodyOnClick(event: any): void {
        let rowId: string = this.riGrid.Details.GetAttribute('ServiceArea', 'RowID');
        if ((!rowId) || (rowId === 'NULLROWID')) {
            return;
        }

        if (event.srcElement.getAttribute('AdditionalProperty') === 'Day2') {
            this.getGridCellDetails(event);
        }
    }

    /**
     * Handle AfterExecute event of Grid
     */
    public riGridAfterExecute(): void {
        let totalString: Array<any> = [];
        this.setControlValue('TotalVisits', '');
        this.setControlValue('TotalWED', '');
        this.setControlValue('TotalUnits', '');
        this.setControlValue('TotalTime', '');
        this.setControlValue('AverageWeight', '');
        this.setControlValue('SelectedArea', '');
        this.setControlValue('SelectedDate', '');
        this.setControlValue('ProdServGroupString', '');
        this.setControlValue('PostalBrickString', '');
        this.setControlValue('VisitIntervalString', '');

        if (this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.children.length > 0) {
            let addValue: string = this.riGrid.HTMLGridBody.children[0].children[0].children[0].getAttribute('AdditionalProperty');
            if (addValue) {
                totalString = addValue.split('|');
                this.setControlValue('TotalVisits', totalString[1]);
                this.setControlValue('TotalWED', totalString[1]);
                this.setControlValue('TotalUnits', totalString[2]);
                this.setControlValue('TotalTime', totalString[3]);
                this.setControlValue('AverageWeight', totalString[4]);
                this.riExchange.riInputElement.isError(this.uiForm, 'TotalVisits');
                this.riExchange.riInputElement.isError(this.uiForm, 'TotalUnits');
                this.riExchange.riInputElement.isError(this.uiForm, 'TotalWED');

                if (this.getControlValue('ViewTypeFilter') === 'Premise') {
                    this.totalLabel = 'Total Premises';
                    this.fieldVisibility['totalWED'] = false;
                    this.fieldVisibility['totalVisits'] = true;
                }
                else if (this.getControlValue('ViewTypeFilter') === 'WED') {
                    this.totalLabel = 'Total W.E.D.';
                    this.fieldVisibility['totalWED'] = true;
                    this.fieldVisibility['totalVisits'] = false;
                }
                else {
                    this.totalLabel = 'Total Visits';
                    this.fieldVisibility['totalWED'] = false;
                    this.fieldVisibility['totalVisits'] = true;
                }
            }
        }

        //'Set GridHandle
        if (this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.children.length > 0) {
            this.setControlValue('riGridHandle', this.riGrid.Details.GetAttribute('TotalVisits', 'AdditionalProperty'));
            this.setControlValue('GridCacheKey', this.riGrid.Details.GetAttribute('TotalCalls', 'AdditionalProperty'));
            this.setControlValue('ProdServGroupString', '');
            this.setControlValue('PostalBrickString', '');
            this.setControlValue('VisitIntervalString', '');
        }

        if (this.isReturning() && this.isReturningStatus && this.oldFormData) {
            this.isReturningStatus = false;
            this.riGrid.columnsCollpased = this.pageParams['gridCollapsed'] || {};
            this.setControlValue('TotalVisits', this.oldFormData['TotalVisits']);
            this.setControlValue('TotalWED', this.oldFormData['TotalWED']);
            this.setControlValue('TotalUnits', this.oldFormData['TotalUnits']);
            this.setControlValue('TotalTime', this.oldFormData['TotalTime']);
            this.setControlValue('AverageWeight', this.oldFormData['AverageWeight']);
            this.setControlValue('SelectedArea', this.oldFormData['SelectedArea']);
            this.setControlValue('SelectedDate', this.oldFormData['SelectedDate']);
            this.setControlValue('ProdServGroupString', this.oldFormData['ProdServGroupString']);
            this.setControlValue('PostalBrickString', this.oldFormData['PostalBrickString']);
            this.setControlValue('VisitIntervalString', this.oldFormData['VisitIntervalString']);
        }
    }

    public getGridCellDetails(event: any): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        this.setAttribute('BranchServiceAreaCodeGridDay', this.getGridDay());
        this.setAttribute('BranchServiceAreaCodeServiceArea', this.riGrid.Details.GetValue('ServiceArea'));
        let postData = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');

        postData['Function'] = 'GetGridCellDetails';
        postData['GridCacheKey'] = this.getControlValue('GridCacheKey');
        postData['riGridHandle'] = this.getControlValue('riGridHandle');
        postData['SelectedYear'] = this.getControlValue('SelectedYear');
        postData['SelectedMonth'] = this.getControlValue('SelectedMonth');
        postData['GridDay'] = this.getAttribute('BranchServiceAreaCodeGridDay');
        postData['ServiceArea'] = this.getAttribute('BranchServiceAreaCodeServiceArea');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.setControlValue('ProdServGroupString', data['ProdServGroupString']);
                    this.setControlValue('PostalBrickString', data['PostalBrickString']);
                    this.setControlValue('VisitIntervalString', data['VisitIntervalString']);
                    this.setControlValue('SelectedArea', this.getAttribute('BranchServiceAreaCodeServiceArea'));
                    this.setControlValue('SelectedDate', this.utils.formatDate(this.getControlValue('SelectedYear') + '/' + this.getControlValue('SelectedMonth') + '/' + this.getAttribute('BranchServiceAreaCodeGridDay')));
                }

                this.setAttribute('BranchServiceAreaCodeGridDay', '');
                this.setAttribute('BranchServiceAreaCodeServiceArea', '');
                retObj.next(true);
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        return retObj;
    }

    public supervisorEmployeeCodeOnchange(): any {
        if (this.getControlValue('SupervisorEmployeeCode')) {
            let postData = {};
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');

            postData['PostDesc'] = 'GetSupervisorName';
            postData['SupervisorEmployeeCode'] = this.getControlValue('SupervisorEmployeeCode');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, search, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    }
                    else {
                        this.setControlValue('SupervisorSurname', data['SupervisorSurname']);
                    }
                    if (!this.getControlValue('SupervisorSurname')) this.setControlValue('SupervisorEmployeeCode', '');
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
        else {
            this.setControlValue('SupervisorSurname', '');
        }
    }

    public branchServiceAreaCodeOnchange(): any {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let postData = {};
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');

            postData['PostDesc'] = 'BranchServiceArea';
            postData['BranchNumber'] = this.utils.getBranchCode();
            postData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, search, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    }
                    else {
                        this.setControlValue('EmployeeSurname', data['EmployeeSurname']);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
        else {
            this.setControlValue('EmployeeSurname', '');
        }
    }

    public onEmployeeSearchDataReceived(data: any): void {
        this.setControlValue('SupervisorEmployeeCode', data.OriginatingEmployeeCode);
        this.setControlValue('SupervisorSurname', data.OriginatingEmployeeName);
    }

    public onBranchServiceAreaCodeDataReceived(data: any): void {
        this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
    }

    public onProductServiceGroupSearch(data: any): void {
        this.setControlValue('ProductServiceGroupString', data.ProductServiceGroupString || '');
        this.productSearchGroupDesc = data.ProductServiceGroupDesc;
        this.pageParams['ProductServiceGroupSearch'] = data;
    }
}
