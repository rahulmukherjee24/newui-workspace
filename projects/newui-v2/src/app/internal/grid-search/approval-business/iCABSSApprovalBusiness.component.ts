import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';

import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { InternalGridSearchSalesModuleRoutes } from '@app/base/PageRoutes';
import { MessageConstant } from '@shared/constants/message.constant';

@Component({
    templateUrl: 'iCABSSApprovalBusiness.html'
})

export class ApprovalBusinessComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private BusinessCode: string;
    private gridHandleString: string = this.utils.gridHandle;
    private xhrParams: Record<string, string> = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Sales/iCABSSApprovalBusiness'
    };
    public pageId: string;
    public controls: IControls[] = [
        { name: 'BusinessCode', disabled: true, type: MntConst.eTypeText },
        { name: 'BusinessDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'GroupCode', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'GroupDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'QuoteFilter' },
        { name: 'SOPQuoteType', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ViewBy', value: 'Area' },
        { name: 'accountTypeList' }
    ];
    public accountTypeList: Record<string, string>[] = [{
        value: '',
        key: 'All Account Types'
    }];
    public showPagination: boolean = false;

    constructor(injector: Injector) {
        super(injector);
        this.pageTitle = 'Business Sales Order Appoval';
        this.pageId = PageIdentifier.ICABSSAPPROVALBUSINESS;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.BusinessCode = this.businessCode();
        this.setControlValue('BusinessCode', this.BusinessCode);
        this.setControlValue('BusinessDesc', this.utils.getBusinessText(this.BusinessCode, this.countryCode()));
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (this.isReturning()) {
            this.accountTypeList = this.getControlValue('accountTypeList');
            this.loadGrid();

        } else {
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.pageSize = 10;
            this.pageParams.action = 2;
            this.pageParams.gridCacheRefresh = 'true';
            this.pageParams.totalRecords = 0;
            this.lookupForViewByData();
        }
        this.buildGrid();

    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private lookUpRecord(data: any, maxresults: any): any {
        let queryLookUp: QueryParams = this.getURLSearchParamObject();
        queryLookUp.set(this.serviceConstants.Action, '0');
        if (maxresults) {
            queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        }
        return this.httpService.lookUpRequest(queryLookUp, data);
    }

    private lookupForViewByData(): void {
        const lookupContactType: Array<any> = [
            {
                table: 'AccountType',
                query: { 'BusinessCode': this.BusinessCode },
                fields: ['BusinessCode', 'AccountTypeCode', 'AccountTypeDesc']
            },
            {
                table: 'AccountTypeLang',
                query: { 'BusinessCode': this.BusinessCode, 'LanguageCode': this.riExchange.LanguageCode() },
                fields: ['BusinessCode', 'AccountTypeCode', 'AccountTypeDesc', 'LanguageCode']
            }
        ];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.lookUpRecord(lookupContactType, 500).subscribe(
            (response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (!response['results'] || !response['results'].length || this.hasError(response)) {
                    this.displayMessage(response);
                }

                if (response['results'] && response['results'].length > 0) {
                    if (response['results'] && response['results'][0].length > 0) {
                        const AccountType: any[] = response['results'][0];
                        let AccountTypeLang: any[] = [];
                        if (response['results'].length > 1 && response['results'][1].length > 0) {
                            AccountTypeLang = response['results'][1];
                        }
                        AccountType.forEach(item => {
                            const filterData = AccountTypeLang.find(langObj => ((langObj.BusinessCode === item.BusinessCode)
                                && (langObj.AccountTypeCode === item.AccountTypeCode)));
                            this.accountTypeList.push({
                                value: item.AccountTypeCode,
                                key: (filterData) ? filterData.AccountTypeDesc : item.AccountTypeDesc
                            });
                        });
                        this.setControlValue('accountTypeList', this.accountTypeList); // for caching
                        this.setControlValue('QuoteFilter', this.accountTypeList[0].value);
                    }
                }

            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('GroupCode', 'SOP', 'GroupCode', MntConst.eTypeCode, 9);
        this.riGrid.AddColumn('GroupDesc', 'SOP', 'GroupDesc', MntConst.eTypeText, 20, false);
        this.riGrid.AddColumnAlign('GroupDesc', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('Input', 'SOP', 'Input', MntConst.eTypeInteger, 4, false);
        this.riGrid.AddColumnAlign('Input', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('ContractQuotedValue', 'SOP', 'ContractQuotedValue', MntConst.eTypeCurrency, 9, false);
        this.riGrid.AddColumnAlign('ContractQuotedValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('JobQuotedValue', 'SOP', 'JobQuotedValue', MntConst.eTypeCurrency, 9, false);
        this.riGrid.AddColumnAlign('JobQuotedValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('SalesAction', 'SOP', 'SalesAction', MntConst.eTypeInteger, 4, false);
        this.riGrid.AddColumnAlign('SalesAction', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('AdminAction', 'SOP', 'AdminAction', MntConst.eTypeInteger, 4, false);
        this.riGrid.AddColumnAlign('AdminAction', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('SubmittedContracts', 'SOP', 'SubmittedContracts', MntConst.eTypeInteger, 4, false);
        this.riGrid.AddColumnAlign('SubmittedContracts', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('ContractSubmittedValue', 'SOP', 'ContractSubmittedValue', MntConst.eTypeCurrency, 10, false);
        this.riGrid.AddColumnAlign('ContractSubmittedValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('SubmittedJobs', 'SOP', 'SubmittedJobs', MntConst.eTypeInteger, 9, false);
        this.riGrid.AddColumnAlign('SubmittedJobs', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('JobSubmittedValue', 'SOP', 'JobSubmittedValue', MntConst.eTypeCurrency, 10, false);
        this.riGrid.AddColumnAlign('JobSubmittedValue', MntConst.eAlignmentRight);
        this.riGrid.Complete();
    }

    public loadGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        let form: any = {};

        gridSearch.set(this.serviceConstants.Action, this.pageParams.action.toString());
        form['ApprovalGrid'] = 'Business';
        form['BusinessCode'] = this.BusinessCode;
        form['QuoteFilter'] = this.getControlValue('QuoteFilter');
        form[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        form[this.serviceConstants.GridHandle] = this.gridHandleString;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
        form[this.serviceConstants.Level] = 'Business';
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
        form[this.serviceConstants.PageSize] = this.pageParams.pageSize.toString();
        form[this.serviceConstants.ViewBy] = this.getControlValue('ViewBy');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridSearch, form)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);

                if (data['errorNumber'] || data['errorMessage']) {
                    this.displayMessage(data['errorMessage']);
                    return;
                }

                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.pageParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.pageSize : 1;
                if (this.isReturning()) {
                    setTimeout(() => {
                        this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                    }, 500);
                }
                this.riGrid.Execute(data);
                this.showPagination = true;

            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }

    public getCurrentPage(data: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridCacheRefresh = false;
            super.getCurrentPage(data);
        }
    }

    public onGridBodyDoubleClick(): void {
        if (this.getControlValue('ViewBy') === 'Region') {
            this.displayMessage(MessageConstant.Message.PageNotDeveloped);
            return;
        }
        if (this.riGrid.CurrentColumnName === 'GroupCode' && this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName) !== 'TOTALS') {
            this.navigate('ApprovalBusiness', InternalGridSearchSalesModuleRoutes.ICABSSDLCONTRACTAPPROVALGRID, { branchNumber: this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName), accountType: this.getControlValue('QuoteFilter') });
        }
    }

    public onRiGridRefresh(): void {
        this.loadGrid();
    }

    public gridReset(): void {
        this.riGrid.Clear();
        this.riGrid.RefreshRequired();
        this.buildGrid();
        this.pageParams.gridCurrentPage = 1;
        this.pageParams.totalRecords = 1;
        this.showPagination = false;
    }

}
