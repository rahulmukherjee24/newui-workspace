import { Component, OnInit, Injector, ViewChild, OnDestroy, EventEmitter } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ErrorConstant } from './../../../shared/constants/error.constant';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';

@Component({
    templateUrl: 'iCABSBUserAuthorityBranchGrid.html'
})

export class UserAuthorityBranchGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') public riGrid: GridAdvancedComponent;
    @ViewChild('riPagination') public riPagination: PaginationComponent;

    // URL Query Parameters
    private queryParams: any = {
        operation: 'Business/iCABSBUserAuthorityBranchGrid',
        module: 'user',
        method: 'people/grid'
    };
    private vbBranchRowID: any;
    private vbBranchRow: any;
    private vbBranchNumber: any;
    private vbOriDefaultBranchRowID: any;
    private vbRowID: any;
    public pageId: string = '';
    public setFocusOnExBranch = new EventEmitter<boolean>();

    //Pagination component
    public gridParams: any = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0
    };
    public isHidePagination: boolean = true;
    public controls: Array<any> = [
        { name: 'UserCode', type: MntConst.eTypeCode, required: true, disabled: true },
        { name: 'UserName', type: MntConst.eTypeText, required: true, disabled: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode, required: true },
        { name: 'BusinessDesc', type: MntConst.eTypeText, required: true, disabled: true },
        { name: 'ExistingBranchAuthorityInd', type: MntConst.eTypeCheckBox },
        { name: 'LiveBranchInd', type: MntConst.eTypeCheckBox }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBUSERAUTHORITYBRANCHGRID;
        this.browserTitle = this.pageTitle = 'User Authority - Branch';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        this.riExchange.getParentHTMLValue('BusinessCode');
        this.setControlValue('BusinessDesc', this.getControlValue('BusinessCode') + ' - ' + this.riExchange.getParentHTMLValue('BusinessDesc'));
        this.riExchange.getParentHTMLValue('UserCode');
        this.riExchange.getParentHTMLValue('UserName');
        this.buildGrid();
        this.setFocusOnExBranch.emit(true);
        this.setControlValue('LiveBranchInd', true);
    }

    //Build grid columns
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('BranchNumber', 'UserAuthorityBranch', 'BranchNumber', MntConst.eTypeCode, 3);
        this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchName', 'UserAuthorityBranch', 'BranchName', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('LiveBranch', 'UserAuthorityBranch', 'LiveBranch', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('LiveBranch', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchAuthorityExists', 'UserAuthorityBranch', 'BranchAuthorityExists', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('BranchAuthorityExists', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('DefaultBranch', 'UserAuthorityBranch', 'DefaultBranch', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('DefaultBranch', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('WriteAccess', 'UserAuthorityBranch', 'WriteAccess', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('WriteAccess', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('BranchNumber', true);
        this.riGrid.AddColumnOrderable('BranchName', true);
        this.riGrid.Complete();
    }

    //Load Grid data
    private loadGridData(): void {
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateBody = true;
        this.riGrid.UpdateFooter = true;
        let searchParams: QueryParams = new QueryParams();
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set(this.serviceConstants.BusinessCode, this.getControlValue('BusinessCode'));
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchParams.set('AuthorityUserCode', this.getControlValue('UserCode'));
        searchParams.set('ExistingBranchAuthorityInd', this.utils.convertResponseValueToCheckboxInput(this.getControlValue('ExistingBranchAuthorityInd')).toString());
        searchParams.set('LiveBranchInd', this.utils.convertResponseValueToCheckboxInput(this.getControlValue('LiveBranchInd')).toString());
        searchParams.set(this.serviceConstants.PageSize, this.gridParams['itemsPerPage']);
        searchParams.set(this.serviceConstants.PageCurrent, this.gridParams['pageCurrent']);
        searchParams.set(this.serviceConstants.GridMode, this.gridParams['riGridMode']);
        searchParams.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        let sortOrder = 'Descending';
        if (!this.riGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        searchParams.set('riSortOrder', sortOrder);
        searchParams.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        this.queryParams['search'] = searchParams;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'])
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.gridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    this.riGrid.UpdateHeader = true;
                    this.riGrid.UpdateBody = true;
                    this.riGrid.UpdateFooter = true;
                    this.riGrid.Execute(data);
                    if (data.pageData && (data.pageData.lastPageNumber * 10) > 0) {
                        this.isHidePagination = false;
                    } else {
                        this.isHidePagination = true;
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
            });
    }

    //API on BranchAuthorityExists Click
    private setBranchAuthorityExists(): void {
        let searchParams: QueryParams = new QueryParams();
        let postData: any = {};
        searchParams.set(this.serviceConstants.BusinessCode, this.getControlValue('BusinessCode'));
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchParams.set(this.serviceConstants.Action, '6');
        searchParams.set('AuthorityUserCode', this.getControlValue('UserCode'));
        searchParams.set('BranchNumber', this.vbBranchNumber);
        if (this.riGrid.Details.GetValue('BranchAuthorityExists') === 'yes') {
            searchParams.set('SetBranchAuthorityExists', 'No');
        } else {
            searchParams.set('SetBranchAuthorityExists', 'Yes');
        }
        postData[this.serviceConstants.Function] = 'SetBranchAuthorityExists';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.onGridUpdate(this.vbBranchRowID);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
            });
    }

    //API on DefaultBranch Click
    private setDefaultBranch(): void {
        let searchParams: QueryParams = new QueryParams();
        let postData: any = {};
        searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchParams.set(this.serviceConstants.Action, '6');
        searchParams.set('AuthorityUserCode', this.getControlValue('UserCode'));
        searchParams.set('BranchNumber', this.vbBranchNumber);
        searchParams.set('SetDefaultBranch', 'Yes');
        postData[this.serviceConstants.Function] = 'SetDefaultBranch';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.onGridUpdate(this.vbBranchRowID);
                    this.onGridUpdate(data.OriDefaultBranchRowID);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
            });
    }

    //API on WriteAccess Click
    private setWriteAccess(): void {
        let searchParams: QueryParams = new QueryParams();
        let postData: any = {};
        searchParams.set(this.serviceConstants.BusinessCode, this.getControlValue('BusinessCode'));
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchParams.set(this.serviceConstants.Action, '6');
        searchParams.set('AuthorityUserCode', this.getControlValue('UserCode'));
        searchParams.set('BranchNumber', this.vbBranchNumber);
        if (this.riGrid.Details.GetValue('WriteAccess') === 'yes') {
            searchParams.set('SetWriteAccess', 'No');
        } else {
            searchParams.set('SetWriteAccess', 'Yes');
        }
        postData[this.serviceConstants.Function] = 'SetWriteAccess';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.onGridUpdate(this.vbBranchRowID);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
            });
    }

    //API to update grid columns on Click
    private onGridUpdate(rowID: any): void {
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateBody = true;
        this.riGrid.UpdateFooter = true;
        let searchParams: QueryParams = new QueryParams();
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set(this.serviceConstants.BusinessCode, this.getControlValue('BusinessCode'));
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchParams.set('AuthorityUserCode', this.getControlValue('UserCode'));
        searchParams.set('ExistingBranchAuthorityInd', this.utils.convertResponseValueToCheckboxInput(this.getControlValue('ExistingBranchAuthorityInd')).toString());
        searchParams.set('LiveBranchInd', this.utils.convertResponseValueToCheckboxInput(this.getControlValue('LiveBranchInd')).toString());
        searchParams.set('ROWID', rowID);
        searchParams.set(this.serviceConstants.PageSize, this.gridParams['itemsPerPage']);
        searchParams.set(this.serviceConstants.PageCurrent, this.gridParams['pageCurrent']);
        searchParams.set(this.serviceConstants.GridMode, this.gridParams['riGridMode']);
        searchParams.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        let sortOrder = 'Descending';
        if (!this.riGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        searchParams.set('riSortOrder', sortOrder);
        searchParams.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        this.queryParams['search'] = searchParams;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'])
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.riGrid.RefreshRequired();
                    this.loadGridData();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
            }
            );
    }

    //pagination click
    public getCurrentPage(currentPage: any): void {
        this.gridParams['pageCurrent'] = currentPage.value;
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateRow = true;
        this.riGrid.UpdateFooter = true;
        this.loadGridData();
    }

    //Refresh click
    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    //Grid header click
    public riGridSort(event: any): void {
        this.riGrid.DescendingSort = false;
        this.loadGridData();
    }

    //Grid column on click
    public onGridClick(data: any): void {
        this.vbBranchNumber = this.riGrid.Details.GetValue('BranchNumber');
        this.vbBranchRowID = this.riGrid.Details.GetAttribute('BranchNumber', 'RowID');
        switch (this.riGrid.CurrentColumnName) {
            case 'BranchAuthorityExists':
                this.setBranchAuthorityExists();
                break;
            case 'DefaultBranch':
                if (this.riGrid.Details.GetValue('DefaultBranch') !== 'yes') {
                    this.setDefaultBranch();
                }
                break;
            case 'WriteAccess':
                this.setWriteAccess();
                break;
        }
    }

    //Create All Branch Authority - button click
    public onCmdAuthorityClick(): void {
        let searchParams: QueryParams = new QueryParams();
        let postData: any = {};
        searchParams.set(this.serviceConstants.BusinessCode, this.getControlValue('BusinessCode'));
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchParams.set(this.serviceConstants.Action, '6');
        searchParams.set('AuthorityUserCode', this.getControlValue('UserCode'));
        postData[this.serviceConstants.Function] = 'SetBranchAuthorityGroup';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGrid.RefreshRequired();
                    this.loadGridData();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
            });
    }
}
