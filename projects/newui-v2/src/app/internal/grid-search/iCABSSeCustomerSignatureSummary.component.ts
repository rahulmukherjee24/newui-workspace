import { Component, OnInit, OnDestroy, Injector, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { ReplaySubject } from 'rxjs';
import { Observable } from 'rxjs/Observable';

import { BaseComponent } from '../../base/BaseComponent';
import { PageIdentifier } from '../../base/PageIdentifier';
import { MessageConstant } from './../../../../src/shared/constants/message.constant';
import { ICabsModalVO } from './../../../../src/shared/components/modal-adv/modal-adv-vo';
import { MntConst } from './../../../../src/shared/services/riMaintenancehelper';
import { BranchSearchComponent } from './../../../../src/app/internal/search/iCABSBBranchSearch';
import { EmployeeSearchComponent } from './../../../../src/app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from './../../../../src/shared/components/grid-advanced/grid-advanced';
import { InternalGridSearchServiceModuleRoutes } from './../../../app/base/PageRoutes';
import { VariableService } from './../../../../src/shared/services/variable.service';
import { HttpService } from './../../../../src/shared/services/http-service';



@Component({
    templateUrl: 'iCABSSeCustomerSignatureSummary.html'
})

export class CustomerSignatureSummaryComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;
    @ViewChild('dateFrom') dateFrom;
    @ViewChild('employeeSearchEllipsis') employeeSearchEllipsis;
    @ViewChild('employeeSupervisionSearchEllipsis') employeeSupervisionSearchEllipsis;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private vNoReasonCount: number = 0;
    private queryParams: any = {
        method: 'service-delivery/grid',
        module: 'service',
        operation: 'Service/iCABSSeCustomerSignatureSummary'
    };
    private search: QueryParams = new QueryParams();
    private isDateCorrect: boolean = true;

    public pageId: string = '';
    public controls = [
        { name: 'BusinessCode', type: MntConst.eTypeCode },
        { name: 'BusinessDesc', type: MntConst.eTypeText },
        { name: 'BusinessCodeText', type: MntConst.eTypeText },
        { name: 'EmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeText },
        { name: 'RegionCode', type: MntConst.eTypeCode },
        { name: 'RegionDesc', type: MntConst.eTypeText },
        { name: 'BranchNumber', type: MntConst.eTypeText },
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'SupervisorEmployeeCode', type: MntConst.eTypeCode },
        { name: 'SupervisorSurname', type: MntConst.eTypeText },
        { name: 'Level', type: MntConst.eTypeCode },
        { name: 'DateFrom', type: MntConst.eTypeDate },
        { name: 'DateTo', type: MntConst.eTypeDate },
        { name: 'ViewBy', type: MntConst.eTypeDate },
        { name: 'branchNumberText' }
    ];
    public disableObject: any = {
        BusinessCode: true,
        BusinessDesc: true,
        BusinessCodeText: true,
        RegionCode: true,
        RegionDesc: true,
        BranchNumber: true,
        BranchName: true,
        EmployeeSurname: true,
        SupervisorSurname: true,
        employeeEllipse: null,
        employeeSupervisiorEllipse: null,
        EmployeeCode: null,
        SupervisorEmployeeCode: null,
        DateFrom: null,
        DateTo: null
    };
    public displayObject: any = {
        trBusiness: false,
        trRegion: false,
        trBranch: false,
        trViewBy: false,
        trScreen: true
    };
    public inputParams: any = {
        branchParams: {
            'parentMode': 'LookUp'
        },
        branchNumberSelected: {
            id: '',
            text: ''
        },
        employeeCode: {
            'parentMode': 'LookUp-Service-All'
        },
        supervisiorEmployee: {
            'parentMode': 'LookUp-Supervisor'
        }
    };
    public dateFromDt: any;
    public dateToDt: any;
    public employeeSearchComponent = EmployeeSearchComponent;
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };

    public isHidePagination: boolean = true;
    public gridParams: any = {
        totalRecords: 0,
        pageCurrent: 1,
        itemsPerPage: 10,
        riGridMode: 0
    };
    public isPaginationDisabled: boolean = false;
    public isRefreshDisabled: boolean = false;
    public businessDropdown: any;

    constructor(injector: Injector, private variableService: VariableService, private xhr: HttpService) {
        super(injector);
        if (window.location.href.includes('branch'))
            this.pageId = PageIdentifier.ICABSSECUSTOMERSIGNATURESUMMARYBRANCH;
        else if (window.location.href.includes('region'))
            this.pageId = PageIdentifier.ICABSSECUSTOMERSIGNATURESUMMARYREGION;
        else
            this.pageId = PageIdentifier.ICABSSECUSTOMERSIGNATURESUMMARY;
        this.pageTitle = this.browserTitle = 'Customer Signature Summary';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.noSignatureReason();
        this.riGrid.FunctionUpdateSupport = true;
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    //get all noSignatureReason and dependent fields.
    private noSignatureReason(): void {
        let lookupQuery: any, lookupQueryParams = new QueryParams();
        lookupQueryParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        lookupQueryParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        lookupQueryParams.set(this.serviceConstants.MaxResults, '100');
        lookupQueryParams.set(this.serviceConstants.Action, '0');
        lookupQuery = [{
            'table': 'NoSignatureReason',
            'query': { 'BusinessCode': this.utils.getBusinessCode() },
            'fields': ['ReasonNumber']
        }];
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.lookUpRequest(lookupQueryParams, lookupQuery).subscribe(
            noSignatureReason => {
                if (noSignatureReason.results && noSignatureReason.results[0] && noSignatureReason.results[0][0]) {
                    this.vNoReasonCount = noSignatureReason.results[0].length;
                }
                this.getReasonCodeAndDescription();
            },
            error => {
                this.displayObject.trScreen = false;
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    //get all noSignatureReason and dependent fields.
    private getReasonCodeAndDescription(): void {
        this.utils.getRegionCode().subscribe((data) => {
            this.setControlValue('RegionCode', data);
            let lookupQuery: any, lookupQueryParams = new QueryParams();
            lookupQueryParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            lookupQueryParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
            lookupQueryParams.set(this.serviceConstants.Action, '0');
            lookupQuery = [{
                'table': 'Region',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode(),
                    'RegionCode': data
                },
                'fields': ['RegionDesc']
            }];
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.lookUpRequest(lookupQueryParams, lookupQuery).subscribe(
                regionCodeResult => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (regionCodeResult.results && regionCodeResult.results[0] && regionCodeResult.results[0][0]) {
                        this.setControlValue('RegionDesc', regionCodeResult.results[0][0]['RegionDesc']);
                    }
                    this.windowOnload();
                },
                error => {
                    this.displayObject.trScreen = false;
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
        });
    }

    private getRegionDesc(regionCode: any): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        let lookupIP = [
            {
                'table': 'Region',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode(),
                    'RegionCode': regionCode
                },
                'fields': ['RegionDesc']
            }
        ];

        let queryLookUp = this.getURLSearchParamObject();
        queryLookUp.set(this.serviceConstants.Action, '0');
        queryLookUp.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        queryLookUp.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        queryLookUp.set(this.serviceConstants.MaxResults, '100');
        this.xhr.lookUpRequest(queryLookUp, lookupIP).subscribe(res => retObj.next(res.results[0][0].RegionDesc));
        return retObj;
    }

    private windowOnload(): void {
        this.displayObject.trScreen = false;
        this.setControlValue('BusinessCode', this.utils.getBusinessCode());
        this.setControlValue('BusinessDesc', this.utils.getBusinessDesc(this.utils.getBusinessCode()));
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchText());
        this.businessDropdown = [{ text: this.utils.getBusinessCode() + ' - ' + this.utils.getBusinessText(), value: this.utils.getBusinessCode() + ' - ' + this.utils.getBusinessText() }];
        this.setControlValue('BusinessCodeText', this.utils.getBusinessCode() + ' - ' + this.utils.getBusinessText());
        this.inputParams.branchNumberSelected = { id: this.utils.getBranchCode(), text: this.utils.getBusinessText() };
        this.setControlValue('branchNumberText', this.utils.getBranchText());
        let currentUrl: string = this.utils.getCurrentUrl();
        if (currentUrl.indexOf('business') > -1) {
            this.setControlValue('Level', 'Business');
            this.displayObject.trBusiness = false;
            this.displayObject.trRegion = true;
            this.displayObject.trBranch = true;
            this.displayObject.trViewBy = false;
            this.setControlValue('ViewBy', 'ByBranch');
        } else if (currentUrl.indexOf('branch') > -1) {
            this.setControlValue('Level', 'Branch');
            this.displayObject.trBusiness = true;
            this.displayObject.trRegion = true;
            this.displayObject.trBranch = false;
            this.displayObject.trViewBy = true;
            this.setControlValue('ViewBy', 'ByBranch');
        } else if (currentUrl.indexOf('region') > -1) {
            this.setControlValue('Level', 'Region');
            this.displayObject.trBusiness = true;
            this.displayObject.trRegion = false;
            this.displayObject.trBranch = true;
            this.displayObject.trViewBy = true;
            this.setControlValue('ViewBy', 'ByBranch');
            this.utils.getRegionCode().subscribe((data) => {
                let regionCode: any = data;
                this.setControlValue('RegionCode', regionCode);
                this.getRegionDesc(regionCode).subscribe((data) => {
                    this.setControlValue('RegionDesc', data);
                });
            });
        }
        let dateFrom = new Date();
        this.dateFromDt = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), 1);
        this.dateToDt = this.utils.formatDate(new Date());
        this.setControlValue('DateFrom', this.dateFromDt);
        this.setControlValue('DateTo', this.dateToDt);
        switch (this.riExchange.getParentMode()) {
            case 'Productivity':
                this.riExchange.getParentHTMLValue('EmployeeCode');
                this.riExchange.getParentHTMLValue('EmployeeSurname');
                this.riExchange.getParentHTMLValue('DateFrom');
                this.riExchange.getParentHTMLValue('DateTo');
                this.dateToDt = this.getControlValue('DateTo');
                this.dateFromDt = this.getControlValue('DateFrom');

                this.disableObject.EmployeeCode = true;
                this.disableObject.SupervisorEmployeeCode = true;
                this.disableObject.DateFrom = true;
                this.disableObject.DateTo = true;
                this.loadGridData();
                break;
            case 'DrillDownFromBusiness':
                this.setControlValue('BranchNumber', this.riExchange.getParentAttributeValue('BranchNumber'));
                this.setControlValue('BranchName', this.riExchange.getParentAttributeValue('BranchName'));
                this.setControlValue('RegionCode', this.riExchange.getParentAttributeValue('BranchNumber'));
                this.setControlValue('RegionDesc', this.riExchange.getParentAttributeValue('BranchName'));
                this.inputParams.branchNumberSelected = { id: this.getControlValue('BranchNumber'), text: this.getControlValue('BranchNumber') + ' - ' + this.getControlValue('BranchName') };
                if (this.riExchange.getParentAttributeValue('BranchNumber'))
                    this.setControlValue('branchNumberText', this.inputParams.branchNumberSelected['text']);
                this.riExchange.getParentHTMLValue('EmployeeCode');
                this.riExchange.getParentHTMLValue('EmployeeSurname');
                this.riExchange.getParentHTMLValue('SupervisorEmployeeCode');
                this.riExchange.getParentHTMLValue('SupervisorSurname');
                this.riExchange.getParentHTMLValue('DateFrom');
                this.riExchange.getParentHTMLValue('DateTo');
                this.dateToDt = this.getControlValue('DateTo');
                this.dateFromDt = this.getControlValue('DateFrom');
                if ((currentUrl.indexOf('region') > -1) && currentUrl.indexOf('fromMenu') > -1 && !this.isReturning()) {
                    this.dateFromDt = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), 1);
                    this.dateToDt = this.utils.formatDate(new Date());
                    this.setControlValue('DateFrom', this.dateFromDt);
                    this.setControlValue('DateTo', this.dateToDt);
                }
                this.isDateCorrect = true; //for returning same screen for branch isDateCorrect is set to true
                this.buildGrid();
                break;
        }
        if ((currentUrl.indexOf('branch') > -1) && currentUrl.indexOf('fromMenu') > -1 && !this.isReturning()) {
            this.dateFromDt = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), 1);
            this.dateToDt = this.utils.formatDate(new Date());
            this.setControlValue('DateFrom', this.dateFromDt);
            this.setControlValue('DateTo', this.dateToDt);
        }
        if ((currentUrl.indexOf('business') > -1) && currentUrl.indexOf('fromMenu') > -1 && !this.isReturning()) {
            this.dateFromDt = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), 1);
            this.dateToDt = this.utils.formatDate(new Date());
            this.setControlValue('DateFrom', this.dateFromDt);
            this.setControlValue('DateTo', this.dateToDt);
        }
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.dateFromDt = this.getControlValue('DateFrom');
            this.dateToDt = this.getControlValue('DateTo');
            this.isDateCorrect = true;
            this.buildGrid();
        }
    }

    private buildGrid(): void {
        if (!this.isDateCorrect) return;
        let level: string = this.getControlValue('Level');
        this.riGrid.Clear();
        switch (level) {
            case 'Branch':
                this.riGrid.AddColumn('gridEmployeeCode', 'CustSigSummary', 'gridEmployeeCode', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('gridEmployeeCode', MntConst.eAlignmentCenter);
                this.riGrid.AddColumnOrderable('gridEmployeeCode', true);

                this.riGrid.AddColumn('EmployeeName', 'CustSigSummary', 'EmployeeName', MntConst.eTypeText, 20);
                this.riGrid.AddColumnAlign('EmployeeName', MntConst.eAlignmentLeft);
                this.riGrid.AddColumnOrderable('EmployeeName', true);
                break;
            case 'Business':
                let viewBy = this.getControlValue('ViewBy');
                if (viewBy === 'ByRegion') {
                    this.riGrid.AddColumn('BranchNumber', 'CustSigSummary', 'BranchNumber', MntConst.eTypeCode, 3);
                    this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
                    this.riGrid.AddColumnOrderable('BranchNumber', true);

                    this.riGrid.AddColumn('BranchName', 'CustSigSummary', 'BranchName', MntConst.eTypeText, 20);
                    this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentLeft);
                    this.riGrid.AddColumnOrderable('BranchName', true);
                } else {
                    this.riGrid.AddColumn('BranchNumber', 'CustSigSummary', 'BranchNumber', MntConst.eTypeCode, 3);
                    this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
                    this.riGrid.AddColumnOrderable('BranchNumber', true);

                    this.riGrid.AddColumn('BranchName', 'CustSigSummary', 'BranchName', MntConst.eTypeText, 20);
                    this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentLeft);
                    this.riGrid.AddColumnOrderable('BranchName', true);
                }
                break;
            case 'Region':
                this.riGrid.AddColumn('BranchNumber', 'CustSigSummary', 'BranchNumber', MntConst.eTypeCode, 3);
                this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
                this.riGrid.AddColumnOrderable('BranchNumber', true);

                this.riGrid.AddColumn('BranchName', 'CustSigSummary', 'BranchName', MntConst.eTypeText, 20);
                this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentLeft);
                this.riGrid.AddColumnOrderable('BranchName', true);
                break;
        }

        this.riGrid.AddColumn('TotalVisits', 'CustSigSummary', 'TotalVisits', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('TotalVisits', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('TotalVisits', true);

        this.riGrid.AddColumn('TotalPNVisits', 'CustSigSummary', 'TotalPNVisits', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('TotalPNVisits', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('TotalPNVisits', true);

        this.riGrid.AddColumn('TotalSignatures', 'CustSigSummary', 'TotalSignatures', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('TotalSignatures', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('TotalSignatures', true);

        this.riGrid.AddColumn('PercentageCompliance', 'CustSigSummary', 'PercentageCompliance', MntConst.eTypeDecimal2, 6);
        this.riGrid.AddColumnAlign('PercentageCompliance', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('PercentageCompliance', true);

        for (let i: number = 1; i <= this.vNoReasonCount; i++) {
            this.riGrid.AddColumn('Noreason' + i.toString(), 'CustSigSummary', 'Noreason' + i.toString(), MntConst.eTypeInteger, 6);
            this.riGrid.AddColumnAlign('Noreason' + i.toString(), MntConst.eAlignmentRight);
            this.riGrid.AddColumnOrderable('Noreason' + i.toString(), true);
        }

        this.riGrid.AddColumn('TotalUnknown', 'CustSigSummary', 'TotalUnknown', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('TotalUnknown', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('TotalUnknown', true);

        this.riGrid.Complete();
        this.loadGridData();
    }

    private loadGridData(): void {
        this.riGrid.UpdateBody = true;
        this.riGrid.UpdateFooter = true;
        this.riGrid.UpdateHeader = true;

        this.search.set(this.serviceConstants.Action, '2');
        this.search.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.search.set(this.serviceConstants.CountryCode, this.countryCode());
        this.search.set(this.serviceConstants.PageSize, this.gridParams.itemsPerPage.toString());
        this.search.set(this.serviceConstants.PageCurrent, this.gridParams.pageCurrent.toString());
        this.search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        this.search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.search.set(this.serviceConstants.GridMode, this.gridParams.riGridMode);
        this.search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        this.search.set('Level', this.getControlValue('Level'));
        this.search.set('RegionCode', this.getControlValue('RegionCode'));
        this.search.set('BranchNumber', this.getControlValue('BranchNumber'));
        this.search.set('DateFrom', this.getControlValue('DateFrom'));
        this.search.set('DateTo', this.getControlValue('DateTo'));
        this.search.set('EmployeeCode', this.getControlValue('EmployeeCode'));
        this.search.set('SupervisorEmployeeCode', this.getControlValue('SupervisorEmployeeCode'));
        this.search.set('ViewBy', this.getControlValue('ViewBy'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module,
            this.queryParams.operation, this.search)
            .subscribe(
                (data) => {
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage));
                    else {
                        this.gridParams.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.UpdateFooter = true;
                        this.isRequesting = true;
                        this.riGrid.Execute(data);
                        if (data.pageData && (data.pageData.lastPageNumber * 10) > 0)
                            this.isHidePagination = false;
                        else
                            this.isHidePagination = true;
                    }
                    this.isRequesting = false;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.gridParams.totalRecords = 1;
                    this.handleError(error);
                });
    }

    //Start: API error handlint
    private handleError(error: any): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
    }

    private handleSuccessError(data: any): boolean {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        if (data.hasError) {
            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
        }
        return data.hasError;
    }

    //grid refresh functionality
    public refresh(): void {
        this.buildGrid();
    }

    public riGridSort(event: any): void {
        this.loadGridData();
    }

    public getCurrentPage(currentPage: any): void {
        this.gridParams.pageCurrent = currentPage.value;
        this.search.set(this.serviceConstants.PageCurrent, String(this.gridParams.pageCurrent));
        this.buildGrid();
    }

    public onBranchDataReceived(data: any): void {
        if (data['BranchNumber']) {
            this.setControlValue('BranchNumber', data['BranchNumber']);
            this.setControlValue('BranchName', data['BranchName']);
        } else {
            this.setControlValue('BranchNumber', '');
            this.setControlValue('BranchName', '');
        }
    }

    public startDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('DateFrom', value.value);
            if (value.value)
                this.isDateCorrect = true;
            else
                this.isDateCorrect = false;
        }
    }

    public endDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('DateTo', value.value);
            if (value.value)
                this.isDateCorrect = true;
            else
                this.isDateCorrect = false;
        }
    }

    //On employee code search ellipsis data return
    public onEmployeeDataReceived(data: any): void {
        this.setControlValue('EmployeeCode', data.EmployeeCode);
        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
    }

    //On employee supervisior search ellipsis data return
    public onEmployeeSupervisiorDataReceived(data: any): void {
        this.setControlValue('SupervisorEmployeeCode', data.SupervisorEmployeeCode);
        this.setControlValue('SupervisorSurname', data.SupervisorSurname);
    }

    //Employee code onchange functionalities
    public employeeCodeOnChange(): void {
        if (this.getControlValue('EmployeeCode')) {
            let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
            search.set(this.serviceConstants.Action, '6');

            formData['PostDesc'] = 'Employee';
            formData['BusinessCode'] = this.utils.getBusinessCode();
            formData['EmployeeCode'] = this.getControlValue('EmployeeCode');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData).subscribe((data) => {
                if (!this.handleSuccessError(data)) {
                    this.setControlValue('EmployeeSurname', data['EmployeeSurname']);
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            }, (error) => this.handleError(error));
        } else {
            this.setControlValue('EmployeeSurname', '');
        }
    }

    //Employee code onchange functionalities
    public supervisorEmployeeCodeOnChange(): void {
        if (this.getControlValue('SupervisorEmployeeCode')) {
            let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
            search.set(this.serviceConstants.Action, '6');

            formData['PostDesc'] = 'SupervisorEmployee';
            formData['BusinessCode'] = this.utils.getBusinessCode();
            formData['SupervisorEmployeeCode'] = this.getControlValue('SupervisorEmployeeCode');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData).subscribe((data) => {
                if (!this.handleSuccessError(data)) {
                    this.setControlValue('SupervisorSurname', data['SupervisorSurname']);
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            }, (error) => this.handleError(error));
        } else {
            this.setControlValue('SupervisorSurname', '');
        }
    }

    public riGridBodyOnDblClick(): void {
        let currentColumnName: any = this.riGrid.CurrentColumnName;
        let currentRowIndex: any = this.riGrid.CurrentRow;
        let rowData: any = this.riGrid.bodyArray[currentRowIndex];
        let paramsData: any = {
            CurrentContractTypeURLParameter: this.utils.getCurrentContractType(this.riExchange.routerParams['CurrentContractTypeURLParameter']),
            BranchNumber: this.riGrid.Details.GetValue('BranchNumber'),
            BranchName: this.riGrid.Details.GetValue('BranchName'),
            EmployeeCode: this.getControlValue('EmployeeCode'),
            EmployeeSurname: this.getControlValue('EmployeeSurname'),
            SupervisorEmployeeCode: this.getControlValue('SupervisorEmployeeCode'),
            SupervisorSurname: this.getControlValue('SupervisorSurname'),
            DateFrom: this.getControlValue('DateFrom'),
            DateTo: this.getControlValue('DateTo')
        };
        if (currentColumnName === 'BranchNumber' && this.riGrid.Details.GetValue('BranchNumber')) {
            if (this.getControlValue('ViewBy') === 'ByRegion') {
                this.navigate('DrillDownFromBusiness', InternalGridSearchServiceModuleRoutes.ICABSSECUSTOMERSIGNATURESUMMARYREGION, paramsData);
            } else {
                this.navigate('DrillDownFromBusiness', InternalGridSearchServiceModuleRoutes.ICABSSECUSTOMERSIGNATURESUMMARYBRANCH, paramsData);
            }

        }
        if (this.getControlValue('Level') === 'Branch' && currentColumnName !== 'EmployeeName' && currentColumnName !== 'PercentageCompliance' && currentColumnName !== 'gridEmployeeCode') {
            //GridFocusDetail functionalities
            paramsData['ViewByCode'] = this.riGrid.Details.GetAttribute(currentColumnName, 'RowId');
            paramsData['ViewByDesc'] = this.riGrid.Details.GetAttribute(currentColumnName, 'AdditionalProperty');

            if (this.riGrid.Details.GetValue('EmployeeName')) {
                paramsData['EmployeeCode'] = this.riGrid.Details.GetValue('gridEmployeeCode');
                paramsData['EmployeeSurname'] = this.riGrid.Details.GetValue('EmployeeName');
            } else {
                paramsData['EmployeeCode'] = '';
                paramsData['EmployeeSurname'] = this.riGrid.Details.GetValue('gridEmployeeCode');
            }
            this.navigate('DrillDownFromBusiness', InternalGridSearchServiceModuleRoutes.ICABSSECUSTOMERSIGNATUREDETAIL, paramsData);
        }
    }


}
