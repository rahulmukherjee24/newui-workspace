export interface ReserveStockSelectFilterInterface {
    CompanyCode?: string;
    DepotCode?: string;
    iCABSProduct?: string;
}

export interface ReserveStockColumnsInterface {
    colCode: string;
    title: string;
    sort?: string;
    isSorted: boolean;
    size?: number;
    name?: string;
}

export interface ReserveStockRowsInterface {
    AffectedAreas?: string;
    AvailableDate?: string;
    CompanyCode: string;
    DepotCode: string;
    iCABSProduct: string;
    iCABSProductDesc: string;
    NavProduct: string;
    QtrDifference: string;
    QtyAvailable: string;
    QtyRequested: string;
    QtyReserved: string;
}

export interface ReserveStockMetaDataInterface {
    branchName: string;
    branchNumber: string;
    fromDate: string;
    reportDateTime: string;
    toDate: string;
}
