import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, Injector, OnDestroy, ViewChild, AfterContentInit } from '@angular/core';

import { BaseComponent } from '@app/base/BaseComponent';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { GcpService } from './../../../../GCP/gcp-service';
import { SessionStorageService } from 'ngx-webstorage';

import { GridSearchModuleConstants } from './../grid-search.constant';
import { ReserveStockReportFormControlsInterface } from './../grid-search-interfaces/reserveStockReportControl.interface';
import { ReserveStockSelectFilterInterface, ReserveStockColumnsInterface, ReserveStockRowsInterface, ReserveStockMetaDataInterface } from './interfaces/BranchSummaryPlanReserveStock.interface';
import { TableComponent } from '@shared/components/table/table';

@Component({
    selector: 'icabs-branch-summary-plan-reserve-stock',
    templateUrl: './iCABSABranchSummaryPlanReserveStock.html'
})

export class BranchSummaryPlanReserveStockComponent extends BaseComponent implements OnInit, AfterViewInit, AfterContentInit, OnDestroy {
    @ViewChild('navErrorDataTable') public navErrorDataTable: TableComponent;

    public browserTitle: string;
    public columns: ReserveStockColumnsInterface[];
    public controls: ReserveStockReportFormControlsInterface[] = [
        { name: 'CompanyCode' },
        { name: 'DepotCode' },
        { name: 'iCABSProduct' },
        { name: 'ResvStockReportBranch', disabled: true, type: MntConst.eTypeText },
        { name: 'ResvStockReportCompanyCode' },
        { name: 'ResvStockReportEndDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'ResvStockReportProdCode', type: MntConst.eTypeText },
        { name: 'ResvStockReportProdDepot' },
        { name: 'ResvStockReportStartDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'ResvStockReportTime', disabled: true, type: MntConst.eTypeTime }
    ];
    public compCodeOptions: string[] = [];
    public depotOptions: string[] = [];
    public isEnableSort: boolean = false;
    public metaData: ReserveStockMetaDataInterface;
    public naverrorlist: any = {};
    public noRecordFound: boolean = false;
    public pageId: string;
    public pageTitle: string;
    public prodCodeOptions: string[] = [];
    public reserveStockRptConstant: any = GridSearchModuleConstants.reserveStockReport;
    public rows: ReserveStockRowsInterface[];
    public selectedColumn: string = this.reserveStockRptConstant.defaultSortColumn;
    public selectedFilters: ReserveStockSelectFilterInterface = { // order according to UI
        'iCABSProduct': '',
        'DepotCode': '',
        'CompanyCode': ''
    };

    constructor(injector: Injector,
        private http: HttpClient,
        private sessionStorage: SessionStorageService,
        private gcpService: GcpService) {
        super(injector);
        this.pageId = this.reserveStockRptConstant.pageId;
        this.browserTitle = this.pageTitle = this.reserveStockRptConstant.pageTitle;
    }

    ngAfterContentInit(): void {
        this.columns = [
            { 'title': 'Nav Company Code', 'sort': 'asc', 'name': 'CompanyCode', 'colCode': 'CompanyCode', 'isSorted': false, 'size': 8 },
            { 'title': 'Depot Code', 'sort': 'asc', 'name': 'DepotCode', 'colCode': 'DepotCode', 'isSorted': false, 'size': 5 },
            { 'title': 'iCABS Product', 'sort': 'asc', 'name': 'iCABSProduct', 'colCode': 'iCABSProduct', 'isSorted': false, 'size': 8 },
            { 'title': 'NAV Product Code', 'sort': 'asc', 'name': 'NavProduct', 'colCode': 'NavProduct', 'isSorted': false, 'size': 8 },
            { 'title': 'iCABS Descrption', 'sort': 'asc', 'name': 'iCABSProductDesc', 'colCode': 'iCABSProductDesc', 'isSorted': false, 'size': 20 },
            { 'title': 'Qty Available', 'sort': 'asc', 'name': 'QtyAvailable', 'colCode': 'QtyAvailable', 'isSorted': false, 'size': 8 },
            { 'title': 'Total Quantity Requested', 'sort': 'asc', 'name': 'QtyRequested', 'colCode': 'QtyRequested', 'isSorted': false, 'size': 8 },
            { 'title': 'Total Quantity Reserved', 'sort': 'asc', 'name': 'QtyReserved', 'colCode': 'QtyReserved', 'isSorted': false, 'size': 8 },
            { 'title': 'Difference', 'sort': 'dsc', 'name': 'QtrDifference', 'colCode': 'QtrDifference', 'isSorted': true, 'size': 8 },
            { 'title': 'ATP Date', 'sort': 'asc', 'name': 'AvailableDate', 'colCode': 'AvailableDate', 'isSorted': false, 'size': 8 },
            { 'title': 'Service Area Affected', 'sort': 'NA', 'name': 'AffectedAreas', 'colCode': 'AffectedAreas', 'isSorted': false }
        ];
    }

    ngAfterViewInit(): void {
        if (this.navErrorDataTable) {
            this.loadReserveStockReportData();
        }
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /*
     *this will exclude blank filters which are not selected by user
     *if nothing is selected then return origin blank filters
    */
    private checkAvailableFilters(filters: ReserveStockSelectFilterInterface): ReserveStockSelectFilterInterface {
        let availableFilters: ReserveStockSelectFilterInterface = {};
        for (const key in filters) {
            if (key && filters[key].length) {
                availableFilters[key] = filters[key];
            }
        }
        return availableFilters;
    }

    private defaultSort(a: any, b: any): any {
        return (a > b) ? 1 : (a < b) ? -1 : 0;
    }

    /*
     *this function will generate drop down options for all 3 filters once GCP response is available
     *it will remove duplicate values also before filling drop down options
     *it will sort also the drop down option values
    */
    private generateFilterOptions(sortedRows: ReserveStockRowsInterface[]): void {
        sortedRows.forEach((item, i) => {
            if (this.compCodeOptions.indexOf(item['CompanyCode'].toString()) < 0) { this.compCodeOptions.push(item['CompanyCode'].toString()); }
            if (this.depotOptions.indexOf(item['DepotCode'].toString()) < 0) { this.depotOptions.push(item['DepotCode'].toString()); }
            if (this.prodCodeOptions.indexOf(item['iCABSProduct'].toString()) < 0) { this.prodCodeOptions.push(item['iCABSProduct'].toString()); }
        });
        this.compCodeOptions.sort(this.defaultSort); // do not DRY
        this.depotOptions.sort(this.defaultSort);
        this.prodCodeOptions.sort(this.defaultSort);
    }

    private handleErrorOnAPI(): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.noRecordFound = true;
        this.isEnableSort = !this.noRecordFound;
        this.rows = [];
        this.navErrorDataTable.loadTableData('', false, this.rows); // Load data table to show 'record not found' message
    }

    private loadReserveStockReportData(): void {
        let searchParams: any = this.getURLSearchParamObject();

        const planToDate: any = this.globalize.parseDateToFixedFormat(this.riExchange.getParentHTMLValue('enddate') || this.activatedRoute.snapshot.queryParams['enddate']) || '';
        const planFromDate: any = this.globalize.parseDateToFixedFormat(this.riExchange.getParentHTMLValue('startdate') || this.activatedRoute.snapshot.queryParams['startdate']) || '';

        searchParams.set('branchServiceAreaList', this.riExchange.getParentHTMLValue('branchserviceareacode') || this.activatedRoute.snapshot.queryParams['branchserviceareacode'] || '');
        searchParams.set('cancelReservation', 'false');
        searchParams.set('confirmReservation', 'false');
        searchParams.set('planFromDate', planFromDate);
        searchParams.set('planToDate', planToDate);
        searchParams.set('branchNumber', this.riExchange.getParentHTMLValue('cbb_branchcode'));
        searchParams.set('email', this.utils.getEmail());
        searchParams.set('businessCode', this.riExchange.getParentHTMLValue('cbb_businesscode'));
        searchParams.set('countryCode', this.riExchange.getParentHTMLValue('cbb_countrycode'));

        const url = this.gcpService.getReserveStockUrl() + 'reserve/stock';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.getData(url, searchParams)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                // ITA-750 display nav errors
                if (response['naverrorlist'] && response['naverrorlist']['records'] && response['naverrorlist']['records'].length) {
                    this.naverrorlist = response['naverrorlist'];
                }

                if (response && response.data && response.data.records && response.data.records.length) {
                    const sortedRows = response.data.records;
                    sortedRows.sort(this.sortArrayBy()); // QtrDifference is default sort order
                    this.sessionStorage.store(this.reserveStockRptConstant.defaultSortColumn, sortedRows); // store in session storage for further usage
                    this.isEnableSort = (sortedRows && sortedRows.length) > 1 ? true : false; // enable only if rows has more than 1 row data
                    this.noRecordFound = false;
                    this.setMetaData(response.meta);
                    this.generateFilterOptions(sortedRows);
                    this.rows = sortedRows;
                    this.navErrorDataTable.loadTableData('', false, this.rows); // 960
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);

                } else {
                    // no record found
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setMetaData(response.meta);
                    this.handleErrorOnAPI();
                }
            },
                () => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setMetaData();
                    this.handleErrorOnAPI();
                });
    }

    private resetData(): ReserveStockRowsInterface[] {
        let sortedRows: ReserveStockRowsInterface[] = [];
        const sessionData: any = this.sessionStorage.retrieve(this.reserveStockRptConstant.defaultSortColumn.toLowerCase()) || null;

        if (sessionData) {
            sortedRows = sessionData;
            this.isEnableSort = (sortedRows && sortedRows.length) > 1 ? true : false;// enable only if rows has more than 1 row data
            this.noRecordFound = false;
            return sortedRows;
        }
        return [];
    }

    private setMetaData(data?: any): void {
        this.setControlValue('ResvStockReportBranch', (data && data.branchNumber) ? data.branchNumber : this.utils.getBranchText() || '');
        this.setControlValue('ResvStockReportEndDate', this.riExchange.getParentHTMLValue('enddate'));
        this.setControlValue('ResvStockReportStartDate', this.riExchange.getParentHTMLValue('startdate'));
        this.setControlValue('ResvStockReportTime', (data && data.reportDateTime) ? new Date(data.reportDateTime).toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, '$1') : '');
    }

    private sortArrayBy(field?: string): any {
        field = field || 'QtrDifference';
        return (a: any, b: any) => (a[field] > b[field]) ? 1 : (a[field] < b[field]) ? -1 : 0;
    }

    public onRefresh(): void {
        let filteredResult: ReserveStockRowsInterface[] = [];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.rows = [];

        filteredResult = this.resetData();
        const availableFilters = this.checkAvailableFilters(this.selectedFilters);

        if (Object.keys(availableFilters).length && filteredResult.length) {
            filteredResult = filteredResult.filter((item, i) => {
                let isMatch: boolean = false;
                for (let key in this.selectedFilters) {
                    if (key && this.selectedFilters[key].length) {
                        isMatch = item[key].toString() === this.selectedFilters[key].toString() ? true : false;
                        if (!isMatch) {
                            break;
                        }
                    }
                }
                return isMatch;
            });

        } else {
            filteredResult.sort(this.sortArrayBy()); // default sort on reset i.e. no filter selected
            this.selectedColumn = this.reserveStockRptConstant.defaultSortColumn;
        }

        this.noRecordFound = filteredResult.length ? false : true;

        if (filteredResult.length < 2) {
            this.selectedColumn = null;
            this.isEnableSort = false;

        } else {
            this.isEnableSort = true;
        }

        this.rows = filteredResult;
        this.navErrorDataTable.loadTableData('', false, this.rows);
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
    }

    public onSelect(filterType: string): void {
        this.selectedFilters[filterType] = this.getControlValue(filterType);
    }

}
