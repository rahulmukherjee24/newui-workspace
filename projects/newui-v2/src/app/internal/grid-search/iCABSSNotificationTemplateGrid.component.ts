import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { BaseComponent } from '@base/BaseComponent';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceModuleRoutes } from '@base/PageRoutes';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSSNotificationTemplateGrid.html'
})

export class NotificationTemplateGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private queryParams: any = {
        operation: 'System/iCABSSNotificationTemplateGrid',
        module: 'notification',
        method: 'ccm/maintenance'
    };

    public controls = [
        { name: 'NotifyTemplateCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'NotifyTemplateSystemDesc', type: MntConst.eTypeTextFree, disabled: true },
        { name: 'NotifyTemplateType', type: MntConst.eTypeTextFree, disabled: true }
    ];
    public pageId: string = '';

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSNOTIFICATIONTEMPLATEGRID;
        this.browserTitle = this.pageTitle = 'Notification Template Details - Maintenance Grid';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // Initializes data
    private onWindowLoad(): void {
        this.setControlValue('NotifyTemplateCode', this.riExchange.getParentHTMLValue('NotifyTemplateCode'));
        this.setControlValue('NotifyTemplateSystemDesc', this.riExchange.getParentHTMLValue('NotifyTemplateSystemDesc'));
        this.setControlValue('NotifyTemplateType', this.riExchange.getParentHTMLValue('NotifyTemplateType'));
        this.buildGrid();
        if (!this.isReturning()) {
            this.pageParams.gridConfig = {
                pageSize: 10,
                currentPage: 1,
                totalRecords: 1
            };
        }
        this.populateGrid();
    }

    // Builds the structure of the grid
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.HighlightBar = true;
        this.riGrid.AddColumn('LanguageCode', 'NotificationTemplateDetail', 'LanguageCode', MntConst.eTypeCode, 20);
        this.riGrid.AddColumn('NotifyText', 'NotificationTemplateDetail', 'NotifyText', MntConst.eTypeTextFree, 80);
        this.riGrid.Complete();
    }

    // Populate data into the grid
    private populateGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set('NotifyTemplateCode', this.getControlValue('NotifyTemplateCode'));
        // set grid building parameters
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.PageSize, this.pageParams.gridConfig.pageSize.toString());
        search.set(this.serviceConstants.PageCurrent, this.pageParams.gridConfig.currentPage.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.Action, '2');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrGet(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGrid.RefreshRequired();
                    this.pageParams.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                    this.riGrid.Execute(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    // Refresh the grid data on user click
    public onRiGridRefresh(event: any): void {
        if (this.pageParams.gridConfig.currentPage <= 0) {
            this.pageParams.gridConfig.currentPage = 1;
        }
        if (event) {
            this.riGrid.HeaderClickedColumn = '';
        }
        this.populateGrid();
    }

    // Callback to retrieve the current page on user clicks
    public onGetCurrentPage(currentPage: any): void {
        this.pageParams.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh(null);
    }

    // On Grid double click
    public onGridBodyDbClick(event: any): void {
        this.logger.log('onGridBodyDbClick', this.riGrid.CurrentColumnName, this.riGrid.Details.GetValue('LanguageCode'));
        if (this.riGrid.CurrentColumnName === 'LanguageCode') {
            this.navigate('Update', InternalMaintenanceModuleRoutes.ICABSSNOTIFICATIONTEMPLATEDETAILMAINTENANCE, { rowid: this.riGrid.Details.GetAttribute('LanguageCode', 'rowid') });
        }
    }

    // On Add Button click
    public onAddBtnClicked(event: any): void {
        this.navigate('Add', InternalMaintenanceModuleRoutes.ICABSSNOTIFICATIONTEMPLATEDETAILMAINTENANCE);
    }
}
