import { Component, Injector, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../base/BaseComponent';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from '../../../shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from '../../../shared/components/pagination/pagination';
import { BranchServiceAreaSearchComponent } from '../../internal/search/iCABSBBranchServiceAreaSearch';
import { DropdownStaticComponent } from '../../../shared/components/dropdown-static/dropdownstatic';

@Component({
    templateUrl: 'iCABSSEPlanVisitGridDayBranch.html'
})

export class PlanVisitGridDayBranchComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('contractTypeDropdown') contractTypeDropdown: DropdownStaticComponent;

    private isEnablePostcodeDefaulting: boolean = false;
    private isEnableAUPostcodeValidation: boolean = false;
    private displayAverageWeight: string = '';
    private queryParams: any = {
        operation: 'Service/iCABSSEPlanVisitGridDayBranch',
        module: 'plan-visits',
        method: 'service-planning/grid'
    };
    public pageId: string = '';
    public pageSize: number = 20;
    public filter: any = {
        isPostcodeDisplayed: false,
        isTownDisplayed: false,
        isContractTypeDisplayed: false,
        isDueDateDisplayed: false,
        isDueMonthDisplayed: false,
        isDueYearDisplayed: false
    };
    public gridConfig = {
        pageSize: 20,
        currentPage: 1,
        totalRecords: 1,
        action: '2'
    };
    public ellipsisConfig: any = {
        serviceArea: {
            isDisabled: false,
            childConfigParams: {
                parentMode: 'LookUp-PlanEmp'
            },
            contentComponent: BranchServiceAreaSearchComponent
        }
    };
    public controls: any = [
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'SelectedMonth', type: MntConst.eTypeInteger, readonly: true, disabled: true },
        { name: 'Postcode', type: MntConst.eTypeText, disabled: true, readonly: false },
        { name: 'SelectedYear', type: MntConst.eTypeInteger, readonly: true, disabled: true },
        { name: 'Town', type: MntConst.eTypeText, disabled: true, readonly: false },
        { name: 'ProductServiceGroupString', type: MntConst.eTypeCode, disabled: true, required: false },
        { name: 'VisitFrequencyFilter', type: MntConst.eTypeText, readonly: true, disabled: true },
        { name: 'GridPageSize', type: MntConst.eTypeInteger, required: true },
        { name: 'SelectedDate', type: MntConst.eTypeDate, disabled: true, readonly: true },
        { name: 'ViewByFilter', type: MntConst.eTypeText, disabled: true, readonly: true },
        { name: 'ContractTypeFilter', type: MntConst.eTypeText, disabled: true, readonly: true },
        { name: 'BranchServiceAreaPlan', type: MntConst.eTypeCode, required: true },
        { name: 'DestinationDate', type: MntConst.eTypeDate, required: true },
        { name: 'DateType', type: MntConst.eTypeText, readonly: true }

    ];

    constructor(injector: Injector, private ref: ChangeDetectorRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPLANVISITGRIDDAYBRANCH;
        this.browserTitle = this.pageTitle = 'Branch Summary Workload';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.loadSystemCharacters();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private loadSystemCharacters(): void {
        let sysCharList = [
            this.sysCharConstants.SystemCharEnablePostcodeDefaulting,
            this.sysCharConstants.SystemCharEnableAUPostcodeValidation
        ];
        let sysNumbers = sysCharList.join(',');
        this.fetchSysChar(sysNumbers).subscribe((data) => {
            if (data && data.records[0] && data.records.length > 0) {
                if (data.records[0].Required) {
                    this.isEnablePostcodeDefaulting = true;
                }
            }
            if (data && data.records[1] && data.records.length > 0) {
                if (data.records[1].Required) {
                    this.isEnableAUPostcodeValidation = true;
                }
            }
            this.setPlanningDiaryOptions();
            if (this.isEnableAUPostcodeValidation) {
                this.filter.isPostcodeDisplayed = true;
                this.filter.isTownDisplayed = true;
            }
            this.onWindowLoad();
        });
    }

    private fetchSysChar(sysCharNumber: any): any {
        let querySysChar: QueryParams = this.getURLSearchParamObject();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.SystemCharNumber, sysCharNumber);
        return this.httpService.sysCharRequest(querySysChar);
    }

    private setPlanningDiaryOptions(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData['ActionType'] = 'GetPlanningDiaryOptions';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.displayAverageWeight = e.DisplayAverageWeight;
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private onWindowLoad(): void {
        this.setControlValue('GridPageSize', 20);
        this.pageSize = 20;
        switch (this.parentMode) {
            case 'SummaryWorkload':
                let branchCode: string = this.utils.getBranchCode();
                this.setControlValue('BranchNumber', branchCode);
                let branchCodeWithDesc: string = this.utils.getBranchText(branchCode);
                let index: number = branchCodeWithDesc.indexOf('-');
                let branchDesc: string = branchCodeWithDesc.substring(index + 1);
                this.setControlValue('BranchName', branchDesc);
                this.disableControl('ProductServiceGroupString', false);
                this.disableControl('VisitFrequencyFilter', false);
                this.disableControl('ContractTypeFilter', false);
                this.filter.isContractTypeDisplayed = true;
                if (!this.isReturning()) {
                    this.setControlValue('ProductServiceGroupString', this.riExchange.getParentHTMLValue('ProductServiceGroupString'));
                    this.setControlValue('VisitFrequencyFilter', this.riExchange.getParentHTMLValue('VisitFrequencyFilter'));
                    this.setControlValue('ContractTypeFilter', this.riExchange.getParentHTMLValue('ContractTypeFilter'));
                    this.setControlValue('ViewByFilter', this.riExchange.getParentHTMLValue('ViewByFilter'));
                    this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
                    this.onBranchServiceAreaCodeChange();
                }
                break;

            case 'SummaryWorkloadYear':
                this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('BranchNumber'));
                this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('BranchName'));
                this.setControlValue('ProductServiceGroupString', this.riExchange.getParentHTMLValue('ProductServiceGroupString'));
                this.setControlValue('VisitFrequencyFilter', this.riExchange.getParentHTMLValue('VisitFrequencyFilter'));
                this.setControlValue('ContractTypeFilter', this.riExchange.getParentHTMLValue('ContractTypeFilter'));
                this.setControlValue('ViewByFilter', this.riExchange.getParentHTMLValue('ViewByFilter'));
                this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
                this.onBranchServiceAreaCodeChange();
                break;

            default:
                this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('BranchNumber'));
                this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('BranchName'));
                this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
                this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname'));
                this.setControlValue('ProductServiceGroupString', this.riExchange.getParentHTMLValue('ProductServiceGroupString'));
                this.setControlValue('VisitFrequencyFilter', this.riExchange.getParentHTMLValue('VisitFrequencyFilter'));
                this.setControlValue('Postcode', this.riExchange.getParentHTMLValue('Postcode'));
                this.setControlValue('Town', this.riExchange.getParentHTMLValue('Town'));
                break;
        }
        this.setControlValue('DateType', this.riExchange.getParentHTMLValue('DateType'));
        switch (this.getControlValue('DateType')) {
            case 'Day':
                this.setControlValue('SelectedDate', this.riExchange.getParentHTMLValue('SelectedDate'));
                this.filter.isDueDateDisplayed = true;
                break;
            case 'Month':
                this.setControlValue('SelectedMonth', this.riExchange.getParentHTMLValue('SelectedMonth'));
                this.setControlValue('SelectedYear', this.riExchange.getParentHTMLValue('SelectedYear'));
                this.filter.isDueMonthDisplayed = true;
                this.filter.isDueYearDisplayed = true;
        }
        this.setControlValue('DestinationDate', this.utils.formatDate(new Date()));
        this.buildGrid();
    }

    public buildGrid(): void {
        this.pageSize = this.getControlValue('GridPageSize');
        this.riGrid.Clear();
        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.riGrid.AddColumn('BranchServiceAreaCode', 'SummaryWorkload', 'BranchServiceAreaCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnOrderable('BranchServiceAreaCode', true);
            this.riGrid.AddColumn('EmployeeCode', 'SummaryWorkload', 'EmployeeCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'SummaryWorkload', 'BranchServiceAreaSeqNo', MntConst.eTypeInteger, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);

        if (this.getControlValue('DateType') === 'Month' || this.getControlValue('DateType') === 'YTDTotal' || this.getControlValue('DateType') === 'NetValue' || this.getControlValue('DateType') === 'YTDNetValue') {
            this.riGrid.AddColumn('StaticVisitDate', 'SummaryWorkload', 'StaticVisitDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('StaticVisitDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnOrderable('StaticVisitDate', true);
        }

        this.riGrid.AddColumn('ContractNumber', 'SummaryWorkload', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ContractNumber', true);

        this.riGrid.AddColumn('PremiseNumber', 'SummaryWorkload', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'SummaryWorkload', 'PremiseName', MntConst.eTypeText, 20);
        this.riGrid.AddColumnOrderable('PremiseName', true);

        this.riGrid.AddColumn('Address', 'SummaryWorkload', 'Address', MntConst.eTypeText, 40);
        this.riGrid.AddColumnScreen('Address', false);

        this.riGrid.AddColumn('PremisePostcode', 'SummaryWorkload', 'PremisePostcode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnOrderable('PremisePostcode', true);

        this.riGrid.AddColumn('ProductCode', 'SummaryWorkload', 'ProductCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ProductCode', true);

        this.riGrid.AddColumn('ServiceTypeCode', 'SummaryWorkload', 'ServiceTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ServiceTypeCode', true);

        this.riGrid.AddColumn('ServiceVisitFrequency', 'SummaryWorkload', 'ServiceVisitFrequency', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ServiceVisitFrequency', true);

        this.riGrid.AddColumn('ServiceQuantity', 'SummaryWorkload', 'ServiceQuantity', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);

        if (this.displayAverageWeight === 'yes') {
            this.riGrid.AddColumn('AverageWeight', 'SummaryWorkload', 'AverageWeight', MntConst.eTypeDecimal2, 5);
            this.riGrid.AddColumnAlign('AverageWeight', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('PlanQuantity', 'SummaryWorkload', 'PlanQuantity', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('PlanQuantity', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitTypeCode', 'SummaryWorkload', 'VisitTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PlanVisitStatusCode', 'SummaryWorkload', 'PlanVisitStatusCode', MntConst.eTypeText, 2);
        this.riGrid.AddColumnAlign('PlanVisitStatusCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('PlanVisitStatusCode', true);

        this.riGrid.AddColumn('LastVisitDate', 'SummaryWorkload', 'LastVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('LastVisitDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('LastVisitTypeCode', 'SummaryWorkload', 'LastVisitTypeCode', MntConst.eTypeCode, 3);
        this.riGrid.AddColumnAlign('LastVisitTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('NextVisitDate', 'SummaryWorkload', 'NextVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('NextVisitDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('NextVisitTypeCode', 'SummaryWorkload', 'NextVisitTypeCode', MntConst.eTypeCode, 3);
        this.riGrid.AddColumnAlign('NextVisitTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Reallocated', 'SummaryWorkload', 'Reallocated', MntConst.eTypeImage, 1, false);
        this.riGrid.AddColumnAlign('Reallocated', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
        this.populateGrid();
    }

    public populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set('Level', 'Day');
        gridSearch.set('ViewByFilter', this.getControlValue('ViewByFilter'));
        gridSearch.set('DateType', this.getControlValue('DateType'));
        gridSearch.set('Date', this.globalize.parseDateToFixedFormat(this.getControlValue('SelectedDate')).toString());
        gridSearch.set('VisitFrequencyFilter', this.getControlValue('VisitFrequencyFilter'));
        gridSearch.set('ContractTypeFilter', this.getControlValue('ContractTypeFilter'));
        gridSearch.set('SelectedMonth', this.getControlValue('SelectedMonth'));
        gridSearch.set('SelectedYear', this.getControlValue('SelectedYear'));
        gridSearch.set('BranchNumber', this.getControlValue('BranchNumber'));
        gridSearch.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        gridSearch.set('ProductServiceGroupString', this.getControlValue('ProductServiceGroupString'));
        gridSearch.set('Postcode', this.getControlValue('Postcode'));
        gridSearch.set('Town', this.getControlValue('Town'));

        gridSearch.set(this.serviceConstants.GridMode, '0');
        gridSearch.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        gridSearch.set(this.serviceConstants.PageSize, this.gridConfig.pageSize.toString());
        gridSearch.set(this.serviceConstants.PageCurrent, this.gridConfig.currentPage.toString());
        gridSearch.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridSearch.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());

        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridSearch)
            .subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.riGrid.RefreshRequired();
                    this.riGridPagination.currentPage = this.gridConfig.currentPage = e.pageData ? e.pageData.pageNumber : 1;
                    this.riGridPagination.totalItems = this.gridConfig.totalRecords = e.pageData ? e.pageData.lastPageNumber * this.pageSize : 1;
                    this.riGrid.Execute(e);
                    this.ref.detectChanges();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onRiGridRefresh(): void {
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh();
    }

    public onBranchServiceAreaDataReceived(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaPlan', data.BranchServiceAreaCode);
        }
    }

    public onRiGridBodyDoubleClick(event: any): void {
        let currentContractType: any = this.riGrid.Details.GetAttribute('BranchServiceAreaSeqNo', 'AdditionalProperty');
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractNumber':
                this.setAttribute('ContractRowID', this.riGrid.Details.GetAttribute('ContractNumber', 'AdditionalProperty'));
                let url = this.ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE;
                switch (currentContractType) {
                    case 'P':
                        url = this.ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE;
                        break;
                    case 'J':
                        url = this.ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE;
                        break;
                    case 'C':
                        url = this.ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE;
                        break;
                }
                this.navigate('StaticVisit', url,
                    {
                        ContractNumber: this.utils.mid(this.riGrid.Details.GetValue('ContractNumber'), 3)
                    });
                break;
            case 'PremiseNumber':
                this.navigate('StaticVisit', this.ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE,
                    {
                        contracttypecode: currentContractType,
                        PremiseRowID: this.riGrid.Details.GetAttribute('PremiseNumber', 'AdditionalProperty')
                    });
                break;
            case 'ProductCode':
                this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'AdditionalProperty'));
                this.navigate('StaticVisit', this.ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE,
                    {
                        currentContractType: currentContractType
                    });
                break;
            case 'Reallocated':
                if (this.getControlValue('BranchServiceAreaPlan') === '') {
                    this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'BranchServiceAreaPlan', true);
                    this.riExchange.riInputElement.markAsError(this.uiForm, 'BranchServiceAreaPlan');
                } else if (!this.getControlValue('DestinationDate')) {
                    this.riExchange.riInputElement.markAsError(this.uiForm, 'DestinationDate');
                } else {
                    let searchParams: QueryParams = this.getURLSearchParamObject();
                    searchParams.set(this.serviceConstants.Action, '0');
                    let formData: any = {};
                    formData[this.serviceConstants.Function] = 'ReallocateBranchServiceArea';
                    formData['BranchNumber'] = this.utils.getBranchCode();
                    formData['SelectedDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('SelectedDate'));
                    formData['PlanVisitRowID'] = this.riGrid.Details.GetAttribute('Reallocated', 'rowID');
                    formData['BranchServiceAreaPlan'] = this.getControlValue('BranchServiceAreaPlan');
                    formData['DestinationDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('DestinationDate'));
                    this.ajaxSource.next(this.ajaxconstant.START);
                    this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
                        (e) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            if (e.hasError) {
                                this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                            } else {
                                this.setControlValue('ErrorMessage', e.ErrorMessage);
                                let modalVO: ICabsModalVO = new ICabsModalVO(e.ErrorMessage);
                                modalVO.closeCallback = this.buildGrid.bind(this);
                                this.modalAdvService.emitMessage(modalVO);
                            }
                        },
                        (error) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        });
                }
                break;
        }
    }

    public onBranchServiceAreaCodeChange(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData['PostDesc'] = 'BranchServiceArea';
        formData['BranchNumber'] = this.getControlValue('BranchNumber');
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.setControlValue('EmployeeSurname', e.EmployeeSurname);
                    this.buildGrid();
                    this.onRiGridRefresh();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });

    }

    public onGridPageSizeChange(): void {
        if (this.getControlValue('GridPageSize')) {
            this.gridConfig.pageSize = this.getControlValue('GridPageSize');
        }
    }

    public onBranchServiceAreaPlanChange(event: any): void {
        if (!event.target.value) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'BranchServiceAreaPlan', true);
        } else {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'BranchServiceAreaPlan', false);
        }
    }
}
