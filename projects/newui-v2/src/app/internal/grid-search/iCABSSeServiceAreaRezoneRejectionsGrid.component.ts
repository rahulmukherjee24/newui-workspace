import { Component, OnInit, Injector, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ContractManagementModuleRoutes } from './../../base/PageRoutes';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../shared/constants/message.constant';

@Component({
    templateUrl: 'iCABSSeServiceAreaRezoneRejectionsGrid.html'
})

export class ServiceAreaRezoneRejectionsGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private queryParams: any = {
        operation: 'Service/iCABSSeServiceAreaRezoneRejectionsGrid',
        module: 'structure',
        method: 'service-planning/grid'
    };

    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true }
    ];

    constructor(injector: Injector, private ref: ChangeDetectorRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEAREAREZONEREJECTIONSGRID;
        this.pageTitle = this.browserTitle = 'Service Area Rezone Rejections';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // Initializes data into different controls on page load
    private onWindowLoad(): void {
        this.setControlValue('BranchName', this.utils.getBranchText());
        this.buildGrid();
        if (this.isReturning()) {
            this.populateGrid();
        } else {
            this.pageParams.gridConfig = {
                pageSize: 10,
                currentPage: 1,
                totalRecords: 1
            };
            this.populateGrid();
        }
    }

    // Builds the structure of the grid
    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('ContractNumber', 'AreaRezoneRejections', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNumber', 'AreaRezoneRejections', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'AreaRezoneRejections', 'PremiseName', MntConst.eTypeText, 25);
        this.riGrid.AddColumn('PremiseAddressLine1', 'AreaRezoneRejections', 'PremiseAddressLine1', MntConst.eTypeText, 25);

        this.riGrid.AddColumn('ProductCode', 'AreaRezoneRejections', 'ProductCode', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('BranchServiceAreaCode', 'AreaRezoneRejections', 'BranchServiceAreaCode', MntConst.eTypeCode, 6, false);
        this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EmployeeCode', 'AreaRezoneRejections', 'EmployeeCode', MntConst.eTypeCode, 6, false);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('OldBranchServiceAreaSeqNo', 'AreaRezoneRejections', 'OldBranchServiceAreaSeqNo', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('OldBranchServiceAreaSeqNo', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('NewBranchServiceAreaSeqNo', 'AreaRezoneRejections', 'NewBranchServiceAreaSeqNo', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('NewBranchServiceAreaSeqNo', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Updated', 'AreaRezoneRejections', 'Updated', MntConst.eTypeImage, 10, true);
        this.riGrid.AddColumnAlign('Updated', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
    }

    // Populate data into AreaRezoneRejections grid
    private populateGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.BranchNumber, this.utils.getBranchCode());

        // set grid building parameters
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.PageSize, this.pageParams.gridConfig.pageSize.toString());
        search.set(this.serviceConstants.PageCurrent, this.pageParams.gridConfig.currentPage.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.Action, '2');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGrid.RefreshRequired();
                    this.pageParams.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                    this.riGrid.Execute(data);
                    this.ref.detectChanges();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // Updates the pagelevel attributes on grid row activity
    private onGridFocus(): void {
        this.setAttribute('PESRezoneRowID', this.riGrid.Details.GetAttribute('NewBranchServiceAreaSeqNo', 'RowID'));
        this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'RowID'));
        this.setAttribute('Row', this.riGrid.CurrentRow);
    }

    // Refresh the grid data on user click
    public onRiGridRefresh(event: any): void {
        if (this.pageParams.gridConfig.currentPage <= 0) {
            this.pageParams.gridConfig.currentPage = 1;
        }
        if (event) {
            this.riGrid.HeaderClickedColumn = '';
        }
        this.populateGrid();
    }

    // Callback to retrieve the current page on user clicks
    public getCurrentPage(currentPage: any): void {
        this.pageParams.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh(null);
    }

    public onGridBodyClick(event: any): void {
        this.onGridFocus();
        switch (this.riGrid.CurrentColumnName) {
            case 'Updated':
                //this.navigate('RezoneRejections', iCABSSeServiceAreaRezoneRejectionsMaintenance.htm);
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.ScreenNotReady));
                break;
            case 'ProductCode':
                this.navigate('RezoneRejections', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE);
                break;
        }
    }
}
