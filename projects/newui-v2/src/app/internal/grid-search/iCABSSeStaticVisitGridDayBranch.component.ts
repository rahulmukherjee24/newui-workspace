import { Component, Injector, OnInit, OnDestroy, AfterContentInit, ViewChild } from '@angular/core';


import { ContractManagementModuleRoutes } from '@base/PageRoutes';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { GridAdvancedComponent } from '../../../shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { PageIdentifier } from '../../base/PageIdentifier';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { PaginationComponent } from '../../../shared/components/pagination/pagination';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { QueryParams } from '../../../shared/services/http-params-wrapper';
import { IXHRParams } from '@app/base/XhrParams';

@Component({
    templateUrl: 'iCABSSeStaticVisitGridDayBranch.html'
})

export class StaticVisitGridDayBranchComponent extends LightBaseComponent implements AfterContentInit, OnInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private xhrParams: IXHRParams = {
        operation: 'Application/iCABSAStaticVisitGridDay',
        module: 'plan-visits',
        method: 'service-planning/maintenance'
    };
    public pageId: string = '';
    public commonGridFunction: CommonGridFunction;
    public pageType: string = '';
    public contractTypeFilter = '';

    public filter: any = {
        isPostcodeDisplayed: false,
        isTownDisplayed: false,
        isDueDateDisplayed: false,
        isDueMonthDisplayed: false,
        isDueYearDisplayed: false
    };
    public controls: IControls[] = [
        { name: 'BranchNumber', value: this.utils.getBranchCode(), type: MntConst.eTypeInteger, disabled: true },
        { name: 'BranchName', value: this.utils.getBranchTextOnly(), type: MntConst.eTypeText, disabled: true },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'ProductServiceGroupString', type: MntConst.eTypeCode, disabled: true, required: false },
        { name: 'SelectedDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'VisitFrequencyFilter', type: MntConst.eTypeText, disabled: true },
        { name: 'SelectedMonth', type: MntConst.eTypeInteger, disabled: true },
        { name: 'SelectedYear', type: MntConst.eTypeInteger, disabled: true },
        { name: 'GridPageSize', value: 20, type: MntConst.eTypeInteger, required: true },
        { name: 'DateType', type: MntConst.eTypeText },
        { name: 'Postcode', type: MntConst.eTypeText, disabled: true },
        { name: 'Town', type: MntConst.eTypeText, disabled: true }
    ];

    constructor(injector: Injector, private sysCharConstants: SysCharConstants) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSSESTATICVISITGRIDDAYBRANCH;
        this.browserTitle = this.pageTitle = 'Branch Static Visits';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();

        if (this.isReturning()) {
            this.buildGrid();
            this.riGrid.HeaderClickedColumn = this.pageParams.headerGirdColumn || '';
            this.riGrid.DescendingSort = this.pageParams.GridSortOrder === 'Descending';
            this.commonGridFunction.onRiGridRefresh();
        } else {
            this.pageParams = {
                hasGridData: false,
                gridCacheRefresh: false,
                gridCurrentPage: 1,
                riGridSortOrder: 'Descending',
                gridConfig: {
                    itemsPerPage: 20,
                    totalItem: 1
                }
            };
        }
        this.loadSystemCharacters();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private loadSystemCharacters(): void {
        const syscharList: Array<number> = [
            this.sysCharConstants.SystemCharEnablePostcodeDefaulting,
            this.sysCharConstants.SystemCharEnableAUPostcodeValidation
        ];
        const query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                if (data.records[0].Required) {
                    this.pageParams.isEnablePostcodeDefaulting = true;
                }
                if (data.records[1].Required) {
                    this.pageParams.isEnableAUPostcodeValidation = true;
                    this.filter.isPostcodeDisplayed = true;
                    this.filter.isTownDisplayed = true;
                }
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });

    }

    private onWindowLoad(): void {
        if (this.parentMode === 'SummaryWorkload') {
            this.riExchange.getParentHTMLValue('ProductServiceGroupString');
            this.riExchange.getParentHTMLValue('VisitFrequencyFilter');
            this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
            this.branchServiceAreaCodeOnchange();
        } else {
            this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
            this.riExchange.getParentHTMLValue('EmployeeSurname');
            this.riExchange.getParentHTMLValue('ProductServiceGroupString');
            this.riExchange.getParentHTMLValue('VisitFrequencyFilter');
            this.riExchange.getParentHTMLValue('Postcode');
            this.riExchange.getParentHTMLValue('Town');
        }
        this.riExchange.getParentHTMLValue('DateType');

        switch (this.getControlValue('DateType')) {
            case 'Day':
                this.riExchange.getParentHTMLValue('SelectedDate');
                this.filter.isDueDateDisplayed = true;
                break;
            case 'Month':
            case 'YTDTotal':
            case 'NetValue':
            case 'YTDNetValue':
                this.riExchange.getParentHTMLValue('SelectedMonth');
                this.riExchange.getParentHTMLValue('SelectedYear');
                this.filter.isDueMonthDisplayed = true;
                this.filter.isDueYearDisplayed = true;
                break;
        }
        this.onRiGridRefresh();
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.riGrid.AddColumn('BranchServiceAreaCode', 'StaticVisit', 'BranchServiceAreaCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnOrderable('BranchServiceAreaCode', true);
            this.riGrid.AddColumn('EmployeeCode', 'StaticVisit', 'EmployeeCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'StaticVisit', 'BranchServiceAreaSeqNo', MntConst.eTypeInteger, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);

        if (this.getControlValue('DateType') === 'Month' || this.getControlValue('DateType') === 'YTDTotal' || this.getControlValue('DateType') === 'NetValue' || this.getControlValue('DateType') === 'YTDNetValue') {
            this.riGrid.AddColumn('StaticVisitDate', 'StaticVisit', 'StaticVisitDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('StaticVisitDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnOrderable('StaticVisitDate', true);
        }

        this.riGrid.AddColumn('ContractNumber', 'StaticVisit', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ContractNumber', true);

        this.riGrid.AddColumn('PremiseNumber', 'StaticVisit', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'StaticVisit', 'PremiseName', MntConst.eTypeText, 20);
        this.riGrid.AddColumnOrderable('PremiseName', true);

        if (this.pageParams.isEnablePostcodeDefaulting && !this.pageParams.isEnableAUPostcodeValidation) {
            this.riGrid.AddColumn('PremisePostcode', 'StaticVisit', 'PremisePostcode', MntConst.eTypeCode, 8);
        }

        if (this.pageParams.isEnableAUPostcodeValidation) {
            this.riGrid.AddColumn('AUPostcode', 'StaticVisit', 'AUPostcode', MntConst.eTypeCode, 8);
        }

        this.riGrid.AddColumn('ProductCode', 'StaticVisit', 'ProductCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ProductCode', true);

        this.riGrid.AddColumn('ServiceVisitFrequency', 'StaticVisit', 'ServiceVisitFrequency', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ServiceVisitFrequency', true);

        this.riGrid.AddColumn('ServiceQuantity', 'StaticVisit', 'ServiceQuantity', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceTypeCode', 'StaticVisit', 'ServiceTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ServiceTypeCode', true);

        this.riGrid.AddColumn('LastVisitDate', 'StaticVisit', 'LastVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('LastVisitDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('LastVisitTypeCode', 'StaticVisit', 'LastVisitTypeCode', MntConst.eTypeCode, 3);
        this.riGrid.AddColumnAlign('LastVisitTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('NextVisitDate', 'StaticVisit', 'NextVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('NextVisitDate', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        const formData: any = {};
        const searchParams: QueryParams = this.getURLSearchParamObject();
        formData[this.serviceConstants.Level] = 'BranchDay';
        formData['DateType'] = this.getControlValue('DateType');
        formData['Date'] = this.globalize.parseDateToFixedFormat(this.getControlValue('SelectedDate')).toString();
        formData['VisitFrequency'] = this.getControlValue('VisitFrequencyFilter');
        formData['SelectedMonth'] = this.getControlValue('SelectedMonth');
        formData['SelectedYear'] = this.getControlValue('SelectedYear');
        formData['BusinessCode'] = this.utils.getBusinessCode();
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['ProductServiceGroupString'] = this.getControlValue('ProductServiceGroupString');
        formData['PostCode'] = this.getControlValue('Postcode');
        formData['ContractTypeFilter'] = this.riExchange.getParentHTMLValue('ContractTypeFilter');
        formData['Town'] = this.getControlValue('Town');
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageSize] = this.getControlValue('GridPageSize');
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
        searchParams.set(this.serviceConstants.Action, '2');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, searchParams, formData).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.pageParams.gridConfig.itemsPerPage = this.getControlValue('GridPageSize');
                this.commonGridFunction.setPageData(data, this.pageParams.gridConfig);
            }).catch(error => {
                this.pageParams.gridConfig.hasGridData = false;
                this.displayMessage(error);
            });
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.buildGrid();
            this.pageParams.gridCacheRefresh = true;
            this.commonGridFunction.onRefreshClick();
        }
    }

    public getCurrentPage(currentPage: any): void {
        this.pageParams.gridCurrentPage = currentPage.value;
        this.onRiGridRefresh();
    }

    public onHeaderClick(): void {
        this.pageParams.headerGirdColumn = this.riGrid.HeaderClickedColumn;
        this.pageParams.GridSortOrder = this.riGrid.SortOrder;
        this.onRiGridRefresh();
    }

    public branchServiceAreaCodeOnchange(): any {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let postData = {};
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');

            postData['PostDesc'] = 'BranchServiceArea';
            postData['BranchNumber'] = this.utils.getBranchCode();
            postData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('service-planning/grid', 'areas',
                'Service/iCABSSESummaryWorkloadGridMonthBranch', search, postData).then(data => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('EmployeeSurname', data['EmployeeSurname']);
                }).catch(error => {
                    this.displayMessage(error);
                });
        }
        else {
            this.setControlValue('EmployeeSurname', '');
        }
    }

    private serviceVisit(): void {
        this.setAttribute('ContractRowID', this.riGrid.Details.GetAttribute('ContractNumber', 'additionalProperty'));
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('PremiseNumber', 'additionalProperty'));
        this.setAttribute('ServiceVisitRowID', this.riGrid.Details.GetAttribute('ProductCode', 'additionalProperty'));
        switch (this.riGrid.Details.GetAttribute('BranchServiceAreaSeqNo', 'additionalProperty')) {
            case 'C':
                this.pageParams.contractTypeCode = 'C';
                this.pageParams.CurrentContractTypeURLParameter = '';
                break;
            case 'J':
                this.pageParams.CurrentContractTypeURLParameter = '<job>';
                this.pageParams.contractTypeCode = 'J';
                break;
            case 'P':
                this.pageParams.CurrentContractTypeURLParameter = '<product>';
                this.pageParams.contractTypeCode = 'P';
                break;
        }
    }
    public onRiGridBodyDoubleClick(event: any): void {
        this.serviceVisit();
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractNumber':
                this.navigate('StaticVisit', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                    CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                    ContractRowID: this.getAttribute('ContractRowID'),
                    contractTypeCode: this.pageParams.contractTypeCode,
                    BusinessCode: this.utils.getBusinessCode(),
                    Action: 0
                });
                break;
            case 'PremiseNumber':
                this.navigate('StaticVisit', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    contracttypecode: this.pageParams.contractTypeCode,
                    PremiseROWID: this.getAttribute('PremiseRowID'),
                    BusinessCode: this.utils.getBusinessCode(),
                    Action: 0
                });
                break;
            case 'ProductCode':
                this.navigate('StaticVisit', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                    CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                    ServiceCoverRowID: this.getAttribute('ServiceVisitRowID'),
                    BusinessCode: this.utils.getBusinessCode(),
                    Action: 0
                });
                break;
        }
    }
}
