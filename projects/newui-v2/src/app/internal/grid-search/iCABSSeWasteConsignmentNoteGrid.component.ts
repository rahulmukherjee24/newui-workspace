import { Component, OnInit, Injector, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { QueryParams } from '@shared/services/http-params-wrapper';
import 'rxjs/add/operator/takeWhile';

import { BaseComponent } from '@base/BaseComponent';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';

@Component({
    templateUrl: 'iCABSSeWasteConsignmentNoteGrid.html'
})

export class WasteConsignmentNoteGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private isAlive: boolean = true;
    private queryParams: any = {
        operation: 'Service/iCABSSeWasteConsignmentNoteGrid',
        module: 'waste',
        method: 'service-delivery/maintenance'
    };
    private search = new QueryParams();
    private serviceCoverRowID: any;
    private wasteConsignmentNoteDetailRowID: string;

    public controls: any = [
        { name: 'ContractName', required: true, disabled: true },
        { name: 'ContractNumber', required: true, disabled: true },
        { name: 'PremiseName', required: true, disabled: true },
        { name: 'PremiseNumber', required: true, disabled: true },
        { name: 'SystemUniqueNumber' },
        { name: 'voidMode' },
        { name: 'WasteConsignmentNoteNumber', required: true, disabled: true },
        { name: 'WasteConsignmentVoid' }
    ];
    public gridConfig = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 1
    };
    public pageId: string = '';

    constructor(injector: Injector, private ref: ChangeDetectorRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEWASTECONSIGNMENTNOTEGRID;
        this.pageTitle = this.browserTitle = 'Waste Consignment Note Detail';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.doCleanUp();
    }

    /** Clean up the memory and unsubscribe */
    private doCleanUp(): void {
        this.serviceConstants = null;
        this.utils = null;
        this.ajaxSource = null;
        this.isAlive = false;
    }

    private windowOnload(): void {
        this.riExchange.getParentHTMLValue('ContractNumber');
        this.riExchange.getParentHTMLValue('ContractName');
        this.riExchange.getParentHTMLValue('PremiseNumber');
        this.riExchange.getParentHTMLValue('PremiseName');
        this.riExchange.getParentHTMLValue('WasteConsignmentNoteNumber');
        this.riExchange.getParentHTMLValue('voidMode');
        this.riExchange.getParentHTMLValue('SystemUniqueNumber');
        if (this.parentMode === 'ManualWasteConsignmentNote') {
            this.riExchange.getParentHTMLValue('WasteConsignmentVoid');
        }
        if (this.getControlValue('voidMode')) {
            this.utils.setTitle('Void Waste Consignment Note Detail');
        }
        this.buildGrid();
        this.populateGrid();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = this.gridConfig.pageSize;
        this.riGrid.AddColumn('productCode', 'WasteConsignmentNoteDetail', 'productCode', MntConst.eTypeDate, 6);
        this.riGrid.AddColumn('ProductDesc', 'WasteConsignmentNoteDetail', 'ProductDesc', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('EWCCode', 'WasteConsignmentNoteDetail', 'EWCCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumn('ServicedQuantity', 'WasteConsignmentNoteDetail', 'ServicedQuantity', MntConst.eTypeInteger, 7);
        this.riGrid.AddColumn('WasteDescription', 'WasteConsignmentNoteDetail', 'WasteDescription', MntConst.eTypeText, 50);
        this.riGrid.AddColumn('VisitRecorded', 'WasteConsignmentNoteDetail', 'VisitRecorded', MntConst.eTypeImage, 1);
        let WasteConsignmentVoid = this.getControlValue('WasteConsignmentVoid');
        this.riGrid.AddColumn('WasteConsignmentNoteDetailExists', 'WasteConsignmentNoteDetail', 'WasteConsignmentNoteDetailExists', MntConst.eTypeImage, 15, (this.getControlValue('WasteConsignmentVoid') === ''));
        this.riGrid.Complete();
    }

    // Populate data into the grid
    private populateGrid(): void {
        this.search = this.getURLSearchParamObject();
        this.search.set('ContractNumber', this.getControlValue('ContractNumber'));
        this.search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        this.search.set('SystemUniqueNumber', this.getControlValue('SystemUniqueNumber'));
        this.search.set(this.serviceConstants.GridMode, '0');
        this.search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        this.search.set(this.serviceConstants.PageSize, this.gridConfig.pageSize.toString());
        this.search.set(this.serviceConstants.PageCurrent, this.gridConfig.currentPage.toString());
        this.search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        let sortOrder = 'Descending';
        if (!this.riGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        this.search.set(this.serviceConstants.GridSortOrder, sortOrder);
        this.search.set(this.serviceConstants.Action, '2');
        this.queryParams.search = this.search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, this.queryParams.search)
            .takeWhile(() => this.isAlive).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.riGrid.RefreshRequired();
                        this.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                        this.riGrid.Execute(data);
                        this.ref.detectChanges();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private deleteWasteConsignmentNoteDetail(): void {
        this.search = this.getURLSearchParamObject();
        let formdata: Object = {};
        formdata[this.serviceConstants.Function] = 'DeleteWasteConsignmentNoteDetail';
        formdata['wasteConsignmentNoteDetailRowID'] = this.wasteConsignmentNoteDetailRowID;
        this.search.set(this.serviceConstants.Action, '6');
        this.queryParams.search = this.search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, this.queryParams.search, formdata)
            .takeWhile(() => this.isAlive).subscribe(
                (data) => {
                    this.isRequesting = false;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.riGrid.RefreshRequired();
                        this.populateGrid();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private addWasteConsignmentNoteDetail(): void {
        this.search = this.getURLSearchParamObject();
        let formdata: Object = {};
        formdata[this.serviceConstants.Function] = 'AddWasteConsignmentNoteDetail';
        formdata['serviceCoverRowID'] = this.serviceCoverRowID;
        formdata['SystemUniqueNumber'] = this.getControlValue('SystemUniqueNumber');
        this.search.set(this.serviceConstants.Action, '6');
        this.queryParams.search = this.search;
        this.isRequesting = true;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, this.queryParams.search, formdata)
            .takeWhile(() => this.isAlive).subscribe(
                (data) => {
                    this.isRequesting = false;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.riGrid.RefreshRequired();
                        this.populateGrid();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    // Refresh the grid data on user click
    public gridRefresh(): void {
        if (this.gridConfig.currentPage <= 0) {
            this.gridConfig.currentPage = 1;
        }
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    // Grid row double click event
    public riGridBodyOnDblClick(event: any): void {
        let gridObj: any = this.riGrid.Details;
        switch (this.riGrid.CurrentColumnName) {
            case 'WasteConsignmentNoteDetailExists':
                let wasteConsignmentNoteDetailExists: string = gridObj.GetValue('WasteConsignmentNoteDetailExists');
                if (wasteConsignmentNoteDetailExists === 'yes') {
                    this.wasteConsignmentNoteDetailRowID = gridObj.GetAttribute('WasteConsignmentNoteDetailExists', 'AdditionalProperty');
                    this.deleteWasteConsignmentNoteDetail();
                } else if (wasteConsignmentNoteDetailExists === 'no') {
                    this.serviceCoverRowID = gridObj.GetAttribute('productCode', 'AdditionalProperty');
                    this.addWasteConsignmentNoteDetail();
                }
                break;
        }
    }

    public getCurrentPage(currentPage: any): void {
        this.gridConfig.currentPage = currentPage.value;
        this.riGrid.RefreshRequired();
        this.gridRefresh();
    }
}
