import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Subscription } from 'rxjs';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';

@Component({
    templateUrl: 'iCABSAContractProductSummary.html'
})

export class ContractProductSummaryComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    private queryParams: Object = {
        operation: 'Application/iCABSAContractProductSummary',
        module: 'product',
        method: 'service-delivery/grid'
    };
    public pageId: string = '';
    public isHidePagination: boolean = true;
    public controls: Array<any> = [
        { name: 'ContractNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'InvoiceAnnivDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'AccountNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'AccountName', disabled: true, type: MntConst.eTypeText },
        { name: 'NegBranchNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'NegBranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'InvoiceFrequencyCode', disabled: true, type: MntConst.eTypeInteger },
        { name: 'ContractAnnualValue', disabled: true, type: MntConst.eTypeCurrency }
    ];
    public gridParams: any = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0
    };
    private lookUpSubscription: Subscription;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSACONTRACTPRODUCTSUMMARY;
        this.pageTitle = 'Contract Service Summary';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnload();
    }

    ngAfterViewInit(): void {
        this.browserTitle = this.riExchange.getCurrentContractTypeLabel() + ' Product Summary';
        this.utils.setTitle(this.browserTitle);
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
        this.setControlValue('ContractAnnualValue', this.riExchange.getParentHTMLValue('ContractAnnualValue'));
        this.doLookupformDataContract();
    }

    //Getting fields from LookUp
    private doLookupformDataContract(): void {
        let lookupIP = [
            {
                'table': 'Contract',
                'query': {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['InvoiceFrequencyCode', 'InvoiceAnnivDate', 'NegBranchNumber', 'AccountNumber']
            }
        ];
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data) {
                let contractData = data[0][0];
                this.setControlValue('AccountNumber', contractData.AccountNumber);
                let invoiceAnnivDate = this.globalize.parseDateToFixedFormat(new Date(contractData.InvoiceAnnivDate)).toString();
                this.setControlValue('InvoiceAnnivDate', this.globalize.parseDateStringToDate(invoiceAnnivDate));
                this.setControlValue('InvoiceFrequencyCode', contractData.InvoiceFrequencyCode);
                this.setControlValue('NegBranchNumber', contractData.NegBranchNumber);
                this.doLookupformDataBranch();
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            }
        });
    }

    //Getting fields from LookUp
    private doLookupformDataBranch(): void {
        let lookupIP = [
            {
                'table': 'Account',
                'query': {
                    'AccountNumber': this.getControlValue('AccountNumber'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['AccountName']
            },
            {
                'table': 'Branch',
                'query': {
                    'BranchNumber': this.getControlValue('NegBranchNumber'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['BranchName']
            }
        ];
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data) {
                let accountData = data[0][0];
                let branchData = data[1][0];
                this.setControlValue('AccountName', accountData.AccountName);
                this.setControlValue('NegBranchName', branchData.BranchName);
                this.buildGrid();
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            }
        });
    }

    //Grid Call
    private buildGrid(): void {
        this.riGrid.PageSize = 10;
        this.riGrid.AddColumn('ProductCode', 'ContractServiceSummary', 'ProductCode', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ProductDesc', 'ContractServiceSummary', 'ProductDesc', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('ProductDesc', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('Frequency', 'ContractServiceSummary', 'Frequency', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('Frequency', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('ServiceQuantity', 'ContractServiceSummary', 'ServiceQuantity', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('ServiceAnnualValue', 'ContractServiceSummary', 'ServiceAnnualValue', MntConst.eTypeCurrency, 10, true);
        this.riGrid.AddColumnAlign('ServiceAnnualValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('AverageValuePerUnit', 'ContractServiceSummary', 'AverageValuePerUnit', MntConst.eTypeCurrency, 10, true);
        this.riGrid.AddColumnAlign('AverageValuePerUnit', MntConst.eAlignmentRight);
        this.riGrid.Complete();
        this.loadData();
    }

    public loadData(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set('ContractNumber', this.getControlValue('ContractNumber'));
        search.set(this.serviceConstants.PageSize, this.gridParams['itemsPerPage']);
        search.set(this.serviceConstants.PageCurrent, this.gridParams['pageCurrent']);
        search.set(this.serviceConstants.GridMode, this.gridParams['riGridMode']);
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        let sortOrder = 'Descending';
        if (!this.riGrid.DescendingSort)
            sortOrder = 'Ascending';
        search.set('riSortOrder', sortOrder);
        search.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    }
                    else {
                        this.gridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.Execute(data);
                        if (data.pageData && (data.pageData.lastPageNumber * 10) > 0) {
                            this.isHidePagination = false;
                        } else {
                            this.isHidePagination = true;
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                }
            );
    }

    public getCurrentPage(currentPage: any): void {
        this.gridParams['pageCurrent'] = currentPage.value;
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateRow = true;
        this.riGrid.UpdateFooter = true;
        this.loadData();
    }

    public refresh(): void {
        this.loadData();
    }
}
