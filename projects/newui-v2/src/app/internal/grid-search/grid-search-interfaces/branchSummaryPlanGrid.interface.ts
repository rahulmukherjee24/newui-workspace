
export interface BranchSummaryPlanGridDataInterface {
    Action: string;
    BranchNumber: string;
    BranchServiceAreaCode: string;
    EndDate: Date | any;
    PlanDate?: string | boolean;
    PlanEndDate?: string | boolean;
    PlanSendDate?: string | boolean;
    ServicePlanNumber: number | string;
    StartDate: Date | any;
    TotalNettValue: number | boolean;
    TotalNoOfCalls: number | string;
    TotalNoOfExchanges: number | string;
    TotalTime: string | boolean;
}
