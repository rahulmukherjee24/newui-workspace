import { Component, OnInit, Injector, ViewChild, OnDestroy, ChangeDetectorRef, ElementRef, Renderer } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PageIdentifier } from './../../base/PageIdentifier';
import { EmployeeSearchComponent } from './../search/iCABSBEmployeeSearch';
import { ContractSearchComponent } from './../search/iCABSAContractSearch';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../shared/constants/message.constant';


@Component({
    templateUrl: 'iCABSSeHCASpecialInstructionsGrid.html'
})

export class SpecialInstructionsGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('EmployeeCode') EmployeeCode: ElementRef;

    private queryParams: any = {
        operation: 'Service/iCABSSeHCASpecialInstructionsGrid',
        module: 'pda',
        method: 'service-delivery/maintenance'
    };
    public pageId: string = '';
    public controls = [
        { name: 'EmployeeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', readonly: false, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ContractNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'ContractName', readonly: false, disabled: true, required: false, type: MntConst.eTypeText }
    ];
    public gridConfig = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 1,
        action: '2'
    };

    public ellipsisConfig = {
        employee: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: EmployeeSearchComponent
        },
        contract: {
            childConfigParams: {
                'currentContractType': 'C',
                'parentMode': 'LookUp',
                'currentContractTypeURLParameter': '<contract>',
                'showAddNew': false,
                'showCountry': false,
                'showBusiness': false
            },
            component: ContractSearchComponent
        }
    };

    constructor(injector: Injector, private ref: ChangeDetectorRef, private element: ElementRef, private renderer: Renderer) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEHCASPECIALINSTRUCTIONSGRID;
        this.browserTitle = 'Service Special Instructions Update';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageTitle = 'Service Special Instructions Update';
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private onWindowLoad(): void {
        this.renderer.invokeElementMethod(this.EmployeeCode.nativeElement, 'focus', []);
        this.buildGrid();
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;

        this.riGrid.AddColumn('EmployeeCode', 'Update', 'EmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EmployeeSurname', 'Update', 'EmployeeSurname', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContractNumber', 'Update', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNumber', 'Update', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'Update', 'PremiseName', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ProductCode', 'Update', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceNotes', 'Update', 'ServiceNotes', MntConst.eTypeText, 100);

        this.riGrid.AddColumn('Updated', 'Update', 'Updated', MntConst.eTypeImage, 1, false);
        this.riGrid.AddColumnAlign('Updated', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('EmployeeCode', true);
        this.riGrid.AddColumnOrderable('ContractNumber', true);

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());
        let form: any = {};
        form['BranchNumber'] = this.utils.getBranchCode();
        form[this.serviceConstants.EmployeeCode] = this.getControlValue('EmployeeCode');
        form[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        form[this.serviceConstants.GridCacheRefresh] = true;
        form[this.serviceConstants.PageSize] = this.gridConfig.pageSize.toString();
        form[this.serviceConstants.PageCurrent] = this.gridConfig.currentPage.toString();
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridSearch, form)
            .subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    } else {
                        this.riGrid.RefreshRequired();
                        this.riGridPagination.currentPage = this.gridConfig.currentPage = e.pageData ? e.pageData.pageNumber : 1;
                        this.riGridPagination.totalItems = this.gridConfig.totalRecords = e.pageData ? e.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                        this.riGrid.Execute(e);
                        this.ref.detectChanges();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public onRiGridRefresh(): void {
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh();
    }

    public populateDescriptions(): void {
        let descSearch: QueryParams = this.getURLSearchParamObject();
        descSearch.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData[this.serviceConstants.Function] = 'GetDescriptions';
        formData['BranchNumber'] = this.utils.getBranchCode();
        if (this.getControlValue('EmployeeCode')) {
            formData[this.serviceConstants.EmployeeCode] = this.getControlValue('EmployeeCode');
        }
        if (this.getControlValue('ContractNumber')) {
            formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, descSearch, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    if (this.getControlValue('EmployeeCode')) {
                        this.setControlValue('EmployeeSurname', e.EmployeeSurname);
                    }
                    if (this.getControlValue('ContractNumber')) {
                        this.setControlValue('ContractName', e.ContractName);
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onEmployeeDataReceived(data: any): void {
        if (data) {
            this.setControlValue('EmployeeCode', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
    }

    public onContractDataReceived(data: any): void {
        if (data) {
            this.setControlValue('ContractNumber', data.ContractNumber);
            this.setControlValue('ContractName', data.ContractName);
        }
    }

    public onEmployeeCodeChange(): void {
        if (this.getControlValue('EmployeeCode')) {
            this.populateDescriptions();
        } else {
            this.setControlValue('EmployeeSurname', '');
        }
    }

    public onContractNumberChange(): void {
        if (this.getControlValue('ContractNumber')) {
            this.populateDescriptions();
        } else {
            this.setControlValue('ContractName', '');
        }
    }

    public riGridBodyOnDblClick(event: any): void {
        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.ScreenNotReady));
    }
}
