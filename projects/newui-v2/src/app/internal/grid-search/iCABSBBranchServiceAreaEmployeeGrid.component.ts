import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';

import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { InternalMaintenanceServiceModuleRoutes } from '@app/base/PageRoutes';
import { IXHRParams } from '@app/base/XhrParams';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '../../base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBBranchServiceAreaEmployeeGrid.html'
})

export class BranchServiceAreaEmployeeGridComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit {

    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public pageId: string = '';
    public controls: IControls[] = [
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'BranchServiceAreaDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeCode, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'menu', value: '', type: MntConst.eTypeCode }
    ];
    public commonGridFunction: CommonGridFunction;
    private xhrParams: IXHRParams = {
        method: 'service-delivery/maintenance',
        module: 'service',
        operation: 'Business/iCABSBBranchServiceAreaEmployeeGrid'
    };

    constructor(injector: Injector) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSBBRANCHSERVICEAREAEMPLOYEEGRID;
        this.pageTitle = 'Branch Service Area Employees';
        this.browserTitle = 'Service Area Branch Details';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (!this.isReturning()) {
            this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
            this.riExchange.getParentHTMLValue('BranchServiceAreaDesc');
            this.riExchange.getParentHTMLValue('BranchName');
            this.riExchange.getParentHTMLValue('BranchNumber');
            this.pageParams.gridConfig = {
                itemsPerPage: 10,
                totalItem: 1
            };
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
        }
        this.onRiGridRefresh();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('EmployeeCode', 'BranchServiceAreaBranch', 'EmployeeCode', MntConst.eTypeCode, 8, true);
        this.riGrid.AddColumn('EmployeeSurname', 'BranchServiceAreaBranch', 'EmployeeSurname', MntConst.eTypeText, 40);

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        this.isRequesting = true;
        const search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');

        const formData: Object = {
            BusinessCode: this.businessCode(),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            BranchNumber: this.getControlValue('BranchNumber'),
            [this.serviceConstants.GridMode]: '0',
            [this.serviceConstants.GridHandle]: this.pageParams.gridHandle,
            [this.serviceConstants.PageSize]: this.pageParams.gridConfig.itemsPerPage,
            [this.serviceConstants.PageCurrent]: this.pageParams.gridCurrentPage || 1,
            [this.serviceConstants.GridHeaderClickedColumn]: '',
            [this.serviceConstants.GridSortOrder]: 'Descending'
        };

        this.commonGridFunction.fetchGridData(this.xhrParams, search, formData);
    }

    public onGridBodyDoubleClick(): void {
        if (this.riGrid.CurrentColumnName === 'EmployeeCode') {
            this.setAttribute('RowID', this.riGrid.Details.GetAttribute('EmployeeCode', 'rowid'));
            this.setAttribute('EmployeeCode', this.riGrid.Details.GetValue('EmployeeCode'));
            this.setAttribute('EmployeeSurname', this.riGrid.Details.GetValue('EmployeeSurname'));
            this.navigate('GridBranchServiceAreaEmployeeUpdate', InternalMaintenanceServiceModuleRoutes.ICABSBBRANCHSERVICEAREAEMPLOYEEMAINTENANCE);
        }
    }

    public onMenuChange(value: string): void {
        if (value === 'Add') {
            this.setControlValue('menu', '');
            this.navigate('GridBranchServiceAreaEmployeeAdd', InternalMaintenanceServiceModuleRoutes.ICABSBBRANCHSERVICEAREAEMPLOYEEMAINTENANCE);
        }
    }
}
