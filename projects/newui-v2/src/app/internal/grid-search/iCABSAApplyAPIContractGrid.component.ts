import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from './../../base/BaseComponent';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { GridComponent } from './../../../shared/components/grid/grid';
import { Component, OnInit, ViewChild, Injector, OnDestroy } from '@angular/core';

@Component({
    selector: 'icabs-apply-contract-grid',
    templateUrl: 'iCABSAApplyAPIContractGrid.html'
})
export class ApplyAPIContractGridComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('apiContractGrid') apiContractGrid: GridComponent;
    @ViewChild('apiContractPagination') apiContractPagination: PaginationComponent;

    public selectedrowdata: any;
    public search: QueryParams = new QueryParams();
    public pageId: string = '';
    public queryParams: any = {
        action: '2',
        operation: 'Application/iCABSAApplyAPIContractGrid',
        module: 'api',
        method: 'contract-management/maintenance'
    };

    public pageTitle: string = 'Preview API Contract';

    public itemsPerPage: number = 10;
    public maxColumn: number = 14;
    public validateProperties: Array<any> = [
        { 'type': MntConst.eTypeCode, 'index': 0, 'align': 'center' },
        { 'type': MntConst.eTypeCode, 'index': 1, 'align': 'center' },
        { 'type': MntConst.eTypeText, 'index': 2, 'align': 'center' },
        { 'type': MntConst.eTypeText, 'index': 3, 'align': 'center' },
        { 'type': MntConst.eTypeDate, 'index': 4, 'align': 'center' },
        { 'type': MntConst.eTypeDecimal2, 'index': 5, 'align': 'center' },
        { 'type': MntConst.eTypeDecimal2, 'index': 6, 'align': 'center' },
        { 'type': MntConst.eTypeDecimal2, 'index': 7, 'align': 'center' },
        { 'type': MntConst.eTypeDecimal2, 'index': 8, 'align': 'center' },
        { 'type': MntConst.eTypeDecimal2, 'index': 9, 'align': 'center' },
        { 'type': MntConst.eTypeDecimal2, 'index': 10, 'align': 'center' },
        { 'type': MntConst.eTypeDecimal2, 'index': 11, 'align': 'center' },
        { 'type': MntConst.eTypeDecimal2, 'index': 12, 'align': 'center' },
        { 'type': MntConst.eTypeDecimal2, 'index': 13, 'align': 'center' }
    ];

    public controls = [
        { name: 'BranchName', readonly: true, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'BranchNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'BusinessCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'BusinessDesc', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'LessThan', readonly: true, disabled: false, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'MonthName', readonly: true, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'MonthNo' },
        { name: 'RegionCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'RegionDescription', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'ViewBy' },
        { name: 'YearNo' }

    ];


    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSAAPPLYAPICONTRACTGRID;
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (!this.isReturning()) {
            this.pageParams.trBranch = true;
            this.pageParams.trRegion = true;
            this.pageParams.totalItems = 1;
            this.pageParams.currentPage = 1;
            this.setUpPageTitle();
            this.initPage();
        }
        this.loadGridData();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public setUpPageTitle(): void {
        let t1 = 'Preview API';
        let t2 = 'Contract';
        let translation = this.getTranslatedValue('Preview API').toPromise();
        translation.then((resp) => {
            t1 = resp;
            let translation1 = this.getTranslatedValue('Contract').toPromise();
            translation1.then((resp) => {
                t2 = resp;
                this.utils.setTitle(t1 + '-' + t2);
            });
        });
    }

    getCurrentPage(currentPage: any): void {
        this.pageParams.currentPage = currentPage.value;
        this.loadGridData();
    }

    getGridInfo(info: any): void {
        if (info.curPage) {
            this.pageParams.currentPage = info.curPage;
        }
        this.apiContractPagination.currentPage = info.curPage;
        this.pageParams.totalItems = info.totalRows;
    }

    getRefreshData(): void {
        this.loadGridData();
    }

    loadGridData(): void {
        let inputParams: any = {};
        inputParams.module = this.queryParams.module;
        inputParams.method = this.queryParams.method;
        inputParams.operation = this.queryParams.operation;
        this.search.set(this.serviceConstants.Action, this.queryParams.action);
        this.search.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.search.set(this.serviceConstants.CountryCode, this.countryCode());
        this.search.set('PageCurrent', this.pageParams.currentPage.toString());
        this.search.set('PageSize', this.itemsPerPage.toString());
        this.search.set('Level', this.getControlValue('ViewBy'));
        this.search.set('ViewBy', 'Contract');
        this.search.set('LessThan', this.globalize.parseDecimalToFixedFormat(this.getControlValue('LessThan'), 2).toString());
        this.search.set('MonthNo', this.getControlValue('MonthNo'));
        this.search.set('YearNo', this.getControlValue('YearNo'));
        if (this.pageParams.trRegion) {
            this.search.set('RegionCode', this.getControlValue('RegionCode'));
        } else {
            this.search.set('BranchNumber', this.getControlValue('BranchNumber'));
        }
        this.search.set('riGridHandle', '1180828');
        this.search.set('riGridMode', '0');
        inputParams.search = this.search;
        this.apiContractGrid.loadGridData(inputParams);
    }

    public getSelectedRowInfo(event: any): void {
        if (event.cellIndex === 0 && event.cellData && event.cellData['rowID'] !== 'TOTAL') {
            this.navigate('ContractExpiryGrid', this.ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                parentMode: 'ContractExpiryGrid',
                ContractNumber: event.cellData.text
            });
        }
    }

    initPage(): void {
        //Retrieve data from parent screen
        this.formData.BusinessDesc = this.riExchange.getParentHTMLValue('BusinessDesc');
        this.formData.BranchNumber = this.riExchange.getParentAttributeValue('BranchNumber');
        this.formData.BranchName = this.riExchange.getParentAttributeValue('BranchName');
        this.formData.RegionCode = this.riExchange.getParentAttributeValue('RegionCode');
        this.formData.RegionDescription = this.riExchange.getParentAttributeValue('RegionDescription');
        this.formData.ViewBy = this.riExchange.getParentAttributeValue('ViewBy');
        this.formData.MonthName = this.riExchange.getParentAttributeValue('MonthName');
        this.formData.YearNo = this.riExchange.getParentAttributeValue('YearNo');
        this.formData.LessThan = this.riExchange.getParentHTMLValue('LessThan');
        this.formData.MonthNo = this.riExchange.getParentAttributeValue('MonthNo');
        this.populateUIFromFormData();
        this.riExchange.riInputElement.Disable(this.uiForm, 'MonthName');
        this.riExchange.riInputElement.Disable(this.uiForm, 'BusinessCode');
        this.riExchange.riInputElement.Disable(this.uiForm, 'BusinessDesc');
        this.riExchange.riInputElement.Disable(this.uiForm, 'BranchNumber');
        this.riExchange.riInputElement.Disable(this.uiForm, 'BranchName');
        this.riExchange.riInputElement.Disable(this.uiForm, 'RegionCode');
        this.riExchange.riInputElement.Disable(this.uiForm, 'RegionDescription');
        this.riExchange.riInputElement.Disable(this.uiForm, 'LessThan');
        this.riExchange.riInputElement.Disable(this.uiForm, 'PreviewYear');
        if (this.formData.ViewBy === 'Region') {
            this.pageParams.trBranch = false;
            this.pageParams.trRegion = true;
        } else {
            this.pageParams.trBranch = true;
            this.pageParams.trRegion = false;
        }
    }
}
