import { Component, Injector, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';

import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../base/BaseComponent';
import { EllipsisComponent } from './../../../shared/components/ellipsis/ellipsis';
import { BranchServiceAreaSearchComponent } from '../../internal/search/iCABSBBranchServiceAreaSearch';
import { GridAdvancedComponent } from '../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceServiceModuleRoutes, InternalGridSearchServiceModuleRoutes } from './../../base/PageRoutes';
import { DropdownStaticComponent } from '../../../shared/components/dropdown-static/dropdownstatic';
import { MessageConstant } from '../../../shared/constants/message.constant';

@Component({
    templateUrl: 'iCABSSeServicePlanGrid.html'
})

export class ServicePlanGridComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('status') status: DropdownStaticComponent;
    @ViewChild('branchserviceareacodeEllipsis') branchserviceareacodeEllipsis: EllipsisComponent;

    private method: string = 'service-planning/maintenance';
    private module: string = 'planning';
    private operation: string = 'Service/iCABSSeServicePlanGrid';

    public pageId: string = '';
    public totalRecords: number = 0;
    public pageCurrent: number = 1;
    public curPage: number = 1;
    public isEnableInstallsRemovals: boolean = false;
    public isSingleServicePlanPerBranch: boolean = false;
    public isSingleServicePlanPerLog: boolean = false;
    public isVisiblePagination: boolean = false;
    public statusOptions: Array<any> = [];
    public ellipsis: any = {};
    public controls: any = [
        { name: 'BranchServiceAreaCode' },
        { name: 'EmployeeSurname', disabled: true },
        { name: 'WeekNumber' },
        { name: 'Status' }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANGRID;
        this.browserTitle = 'Confirmed Service Plans';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.createStatusOptions();
        this.getSysCharDtetails();
        this.ellipsis = {
            branchServiceAreaCode: {
                isAutoOpen: false,
                isShowCloseButton: true,
                childConfigParams: {
                    parentMode: 'LookUp-Emp-UpdateParent',
                    currentContractType: this.riExchange.getCurrentContractType(),
                    currentContractTypeURLParameter: this.riExchange.getCurrentContractTypeLabel()
                },
                contentComponent: BranchServiceAreaSearchComponent,
                isShowHeader: true,
                searchModalRoute: '',
                isDisabled: false
            }
        };
    }

    public ngAfterViewInit(): void {
        if (this.isReturning()) {
            this.refresh();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    //to create the options for the status dropdown
    private createStatusOptions(): void {
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams.methodtype = 'maintenance';
        postParams.Function = 'GetServicePlanStatusOptions';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
            (data) => {
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.statusOptions.push({ text: 'All', value: 'All' });
                    let servicePlanStatusArray: Array<string> = data.ServicePlanStatusOptions.toString().split(',');
                    let i: number = 0, newServicePlanStatusArray: any = {}, lngth: number = 0, txt: string, desc: string;
                    lngth = servicePlanStatusArray.length;
                    for (let i = 0; i < lngth; i++) {
                        desc = servicePlanStatusArray[i].substr(2, (servicePlanStatusArray[i].length - 2));
                        txt = servicePlanStatusArray[i].substr(0, 1);
                        newServicePlanStatusArray['value'] = txt;
                        newServicePlanStatusArray['text'] = desc;
                        switch (txt) {
                            case 'P':
                                this.statusOptions[1] = JSON.parse(JSON.stringify(newServicePlanStatusArray));
                                break;
                            case 'D':
                                this.statusOptions[2] = JSON.parse(JSON.stringify(newServicePlanStatusArray));
                                break;
                            case 'V':
                                this.statusOptions[3] = JSON.parse(JSON.stringify(newServicePlanStatusArray));
                                break;
                            case 'C':
                                this.statusOptions[4] = JSON.parse(JSON.stringify(newServicePlanStatusArray));
                                break;
                            case 'S':
                                this.statusOptions[5] = JSON.parse(JSON.stringify(newServicePlanStatusArray));
                                break;
                        }
                    }
                    if (!this.isReturning()) {
                        this.status.selectedItem = 'P';
                        this.setControlValue('Status', 'P');
                        this.pageParams.Status = 'P';
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    } else {
                        this.setControlValue('Status', this.pageParams.Status);
                        this.status.selectedItem = this.pageParams.Status;
                    }
                }
            },
            (error) => {
                this.errorService.emitError(error);
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    //to get the syschar records
    private getSysCharDtetails(): any {
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharEnableInstallsRemovals,
            this.sysCharConstants.SystemCharSingleServicePlanPerBranch
        ];
        let sysCharIP: any = {
            module: this.module,
            operation: this.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };
        this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            if (data && data.records) {
                let record = data.records;
                this.isEnableInstallsRemovals = record[0]['Required'];
                this.isSingleServicePlanPerBranch = record[1]['Required'];
                this.isSingleServicePlanPerLog = record[1]['Logical'];
                this.pageParams.isEnableInstallsRemovals = this.isEnableInstallsRemovals;
                this.pageParams.isSingleServicePlanPerBranch = this.isSingleServicePlanPerBranch;
                this.pageParams.isSingleServicePlanPerLog = this.isSingleServicePlanPerLog;
            } else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
            }
        });
    }

    //add the columns of the grid
    private buildGrid(): void {
        this.riGrid.Clear();
        if (!this.getControlValue('BranchServiceAreaCode')) {
            if (!this.pageParams.isSingleServicePlanPerBranch && !this.pageParams.isSingleServicePlanPerLog) {
                this.riGrid.AddColumn('ServiceAreaCode', 'ServicePlan', 'ServiceAreaCode', MntConst.eTypeCode, 5);
                this.riGrid.AddColumnAlign('ServiceAreaCode', MntConst.eAlignmentCenter);
                this.riGrid.AddColumnOrderable('ServiceAreaCode', true);
            }
        }

        this.riGrid.AddColumn('ServicePlanNumber', 'ServicePlan', 'ServicePlanNumber', MntConst.eTypeInteger, 5, true);
        this.riGrid.AddColumnAlign('ServicePlanNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ServicePlanNumber', true);

        this.riGrid.AddColumn('ServicePlanDescription', 'ServicePlan', 'ServicePlanDescription', MntConst.eTypeInteger, 18, true);
        this.riGrid.AddColumnAlign('ServicePlanDescription', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServicePlanStartDate', 'ServicePlan', 'ServicePlanStartDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServicePlanStartDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ServicePlanStartDate', true);

        this.riGrid.AddColumn('ServicePlanEndDate', 'ServicePlan', 'ServicePlanEndDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServicePlanEndDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceWeekNumber', 'ServicePlan', 'ServiceWeekNumber', MntConst.eTypeInteger, 2);
        this.riGrid.AddColumnAlign('ServiceWeekNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServicePlanNoOfCalls', 'ServicePlan', 'ServicePlanNoOfCalls', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('ServicePlanNoOfCalls', MntConst.eAlignmentCenter);

        if (this.pageParams.isEnableInstallsRemovals) {
            this.riGrid.AddColumn('ServicePlanNoOfExchanges', 'ServicePlan', 'ServicePlanNoOfExchanges', MntConst.eTypeInteger, 4);
            this.riGrid.AddColumnAlign('ServicePlanNoOfExchanges', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('ServicePlanTime', 'ServicePlan', 'ServicePlanTime', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('ServicePlanTime', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServicePlanNettValue', 'ServicePlan', 'ServicePlanNettValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('ServicePlanNettValue', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServicePlanStatusDesc', 'ServicePlan', 'ServicePlanStatusDesc', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('ServicePlanStatusDesc', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ProcessesDate/Time', 'ServicePlan', 'ProcessesDate/Time', MntConst.eTypeText, 16);
        this.riGrid.AddColumnAlign('ProcessesDate/Time', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('User', 'ServicePlan', 'User', MntConst.eTypeText, 30);
        this.riGrid.AddColumnAlign('User', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServicePlanCancel', 'ServicePlan', 'ServicePlanCancel', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('ServicePlanCancel', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
        this.riGridBeforeExecute();
    }

    //fetch the grid records
    private riGridBeforeExecute(): void {
        let search = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('ServicePlanStatusCode', this.getControlValue('Status'));
        search.set('WeekNumber', this.getControlValue('WeekNumber'));
        search.set('GridMode', 'ConfirmedServicePlan');
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.GridCacheRefresh, 'True');
        search.set(this.serviceConstants.PageSize, '10');
        search.set(this.serviceConstants.GridPageCurrent, this.curPage.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.method, this.module, this.operation, search)
            .subscribe(
            (data) => {
                if (data) {
                    this.curPage = data.pageData.pageNumber ? data.pageData.pageNumber : 1;
                    this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    this.isVisiblePagination = true;
                    if (this.totalRecords === 0)
                        this.isVisiblePagination = false;
                    this.riGrid.UpdateBody = true;
                    this.riGrid.UpdateFooter = true;
                    this.riGrid.UpdateHeader = true;
                    if (data && data.errorMessage) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.riGrid.Execute(data);
                    }
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            error => {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                this.totalRecords = 0;
                this.isVisiblePagination = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private serviceCoverFocus(rsrcElement: any): void {
        let iAddElementNumber: number = 0;
        let iBranchServiceArea: number = 0;
        let oTR: any = rsrcElement.parentElement.parentElement;
        if (this.isEnableInstallsRemovals)
            iAddElementNumber = 1;
        if (!this.getControlValue('BranchServiceAreaCode')) {
            if (!this.isSingleServicePlanPerBranch && !this.isSingleServicePlanPerLog) {
                iBranchServiceArea = 1;
                this.setAttribute('BranchServiceAreaCode', oTR.children[0].children[0].innerText);
                this.setAttribute('EmployeeSurname', this.riGrid.Details.GetAttribute('ServiceAreaCode', 'AdditionalProperty'));
            }
        }
        rsrcElement.focus();
        this.setAttribute('ServicePlanRowID', this.riGrid.Details.GetAttribute('ServicePlanNumber', 'RowID'));
        this.setAttribute('ServicePlanNumber', this.riGrid.Details.GetValue('ServicePlanNumber'));
        this.setAttribute('ServicePlanStartDate', oTR.children[iBranchServiceArea + 2].children[0].innerText);
        this.setAttribute('ServicePlanEndDate', oTR.children[iBranchServiceArea + 3].children[0].innerText);
        this.setAttribute('ServicePlanNoOfCalls', oTR.children[iBranchServiceArea + 5].children[0].innerText);
        this.setAttribute('ServicePlanNoOfExchanges', oTR.children[iBranchServiceArea + 5 + iAddElementNumber].children[0].innerText);
        this.setAttribute('ServicePlanTime', oTR.children[iBranchServiceArea + 6 + iAddElementNumber].children[0].innerText);
        this.setAttribute('ServicePlanNettValue', oTR.children[iBranchServiceArea + 7 + iAddElementNumber].children[0].innerText);
        this.setAttribute('ServicePlanCancel', this.riGrid.Details.GetAttribute('ServicePlanCancel', 'AdditionalProperty'));
        this.setAttribute('PlanVisitDetailInd', oTR.children[iBranchServiceArea].children[0].children[0].getAttribute('AdditionalProperty'));
        this.setAttribute('PlanVisitInd', oTR.children[iBranchServiceArea + 2].children[0].getAttribute('AdditionalProperty'));
        this.setAttribute('Row', this.riGrid.CurrentRow);
    }

    //to get the EmployeeSurname
    public onChangeBranchServiceAreaCode(): void {
        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.setControlValue('EmployeeSurname', '');
            return;
        }
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams.Function = 'GetEmployeeSurname';
        postParams.BranchNumber = this.utils.getBranchCode();
        postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                }
            },
            (error) => {
                this.errorService.emitError(error);
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    // On double click of the grid row
    public riGridBodyOnDblClick(event: any): void {
        let columnName: string = this.riGrid.CurrentColumnName;
        this.serviceCoverFocus(event.srcElement.parentElement);
        switch (columnName) {
            case 'ServicePlanNumber':
                if (this.getAttribute('PlanVisitInd') === 'Yes') {
                    if (this.getAttribute('PlanVisitDetailInd') === 'Yes') {
                        this.navigate('ServicePlan', 'serviceplanning/serviceplanninggridhg');
                    } else {
                        this.navigate('ServicePlan', InternalGridSearchServiceModuleRoutes.ICABSSESERVICEPLANNINGGRID);
                    }
                } else {
                    let objICabsModalVO: ICabsModalVO = new ICabsModalVO('Service Plan Contains No Visits');
                    objICabsModalVO.title = 'No Visits On Service Plan';
                    this.modalAdvService.emitMessage(objICabsModalVO);
                }
                break;
            case 'ServicePlanDescription':
                this.setAttribute('PlanVisitInd', event.srcElement.parentElement.getAttribute('AdditionalProperty'));
                this.navigate('ServicePlanDescription', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEPLANDESCMAINTENANCE);
                break;
            case 'ServicePlanCancel':
                if (this.getAttribute('ServicePlanCancel') === 'No')
                    this.navigate('ServicePlan', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEPLANCANCEL, { 'rowId': this.riGrid.Details.GetAttribute('ServicePlanNumber', 'rowId')});
                break;

        }
    }

    public riGridBodyOnKeyDown(event: any): void {
        switch (event.keyCode) {
            case 38:
                event.returnValue = 0;
                if (event.srcElement.parentElement.parentElement.parentElement.previousSibling) {
                    if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children) {
                        if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex]) {
                            if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]) {
                                this.serviceCoverFocus(event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]);
                            }
                        }
                    }

                }
                break;
            case 40:
            case 9:
                event.returnValue = 0;
                if (event.srcElement.parentElement.parentElement.parentElement.nextSibling) {
                    if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children) {
                        if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex]) {
                            if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]) {
                                this.serviceCoverFocus(event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]);
                            }
                        }
                    }

                }
                break;
        }
    }

    //getting the BranchServiceAreaCode and EmployeeSurname from ellipsis
    public onRecieveBranchServiceAreaCode(data: any): void {
        this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
    }

    //grid refresh
    public refresh(): void {
        this.buildGrid();
    }

    public riGridSort(event: any): void {
        this.riGridBeforeExecute();
    }

    public getCurrentPage(currentPage: any): void {
        this.curPage = currentPage.value;
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateRow = true;
        this.riGrid.UpdateFooter = true;
        this.riGridBeforeExecute();
    }

    //grid sort
    public onHeaderSortOrderable(): void {
        this.riGridBeforeExecute();
    }

    //after clicking on the status dropdown
    public onStatusChange(data: any): void {
        this.setControlValue('Status', data);
        this.pageParams.Status = data;
    }
}
