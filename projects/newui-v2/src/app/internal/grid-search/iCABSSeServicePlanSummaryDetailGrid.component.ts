import { Component, Injector, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../base/BaseComponent';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { IExportOptions } from '@app/base/ExportConfig';

@Component({
    templateUrl: 'iCABSSeServicePlanSummaryDetailGrid.html'
})

export class ServicePlanSummaryDetailGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') public riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') public riGridPagination: PaginationComponent;

    private queryParams: Object = {
        operation: 'Service/iCABSSeServicePlanSummaryDetailGrid',
        module: 'planning',
        method: 'service-planning/maintenance'
    };
    public gridParams: any = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0
    };
    public isHidePagination: boolean = true;
    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'ServicePlanNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'PlannedVisitDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'VisitTypeDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'VisitTypeDescTranslated', type: MntConst.eTypeText, disabled: true },
        { name: 'StartDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'EndDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'BranchServiceAreaCode' }
    ];
    public expCfg: IExportOptions = {};

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANSUMMARYDETAILGRID;
        this.browserTitle = this.pageTitle = 'Service Planning Summary Detail';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    //Onload Functionality
    private windowOnload(): void {
        this.riExchange.getParentHTMLValue('ServicePlanNumber');
        this.riExchange.getParentHTMLValue('StartDate');
        this.riExchange.getParentHTMLValue('EndDate');
        this.riExchange.getParentHTMLValue('PlannedVisitDate');
        this.riExchange.getParentHTMLValue('VisitTypeDesc');
        this.setControlValue('VisitTypeDescTranslated', this.getTranslatedValue(this.getControlValue('VisitTypeDesc')).value);
        this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
        this.buildGrid();
    }

    //Function to build Grid Columns
    private buildGrid(): void {
        if (this.riExchange.riInputElement.isError(this.uiForm, 'PlannedVisitDate')) {
            this.riGrid.AddColumn('VisitDate', 'PlanVisit', 'VisitDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('VisitDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnOrderable('VisitDate', true);
        }
        this.riGrid.AddColumn('ProductCode', 'PlanVisit', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ProductCode', true);
        this.riGrid.AddColumn('ProductDesc', 'PlanVisit', 'ProductDesc', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('ProductDesc', MntConst.eAlignmentLeft);
        if (this.getControlValue('VisitTypeDesc') === 'All') {
            this.riGrid.AddColumn('VisitTypeDescription', 'PlanVisit', 'VisitTypeDescription', MntConst.eTypeText, 10);
            this.riGrid.AddColumnOrderable('VisitTypeDescription', true);
        }
        this.riGrid.AddColumn('Quantity', 'PlanVisit', 'Quantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('Quantity', MntConst.eAlignmentCenter);
        this.riGrid.Complete();
        this.expCfg.dateColumns = this.riGrid.getColumnIndexListFromFull(['VisitDate']);
        this.loadGridData();
    }

    //Function to Load Grid Data
    private loadGridData(): void {
        this.riGrid.UpdateBody = true;
        this.riGrid.UpdateFooter = true;
        this.riGrid.UpdateHeader = true;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.PageSize, this.gridParams['itemsPerPage'].toString());
        search.set(this.serviceConstants.PageCurrent, this.gridParams['pageCurrent'].toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.GridMode, this.gridParams['riGridMode']);
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set('StartDate', this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate')).toString());
        search.set('EndDate', this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate')).toString());
        search.set('VisitTypeDesc', this.getControlValue('VisitTypeDesc'));
        search.set('PlannedVisitDate', this.globalize.parseDateToFixedFormat(this.getControlValue('PlannedVisitDate')).toString());
        search.set('ServicePlanNumber', this.getControlValue('ServicePlanNumber'));
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('BranchNumber', this.utils.getBranchCode());
        this.queryParams['search'] = search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'],
            this.queryParams['operation'], this.queryParams['search'])
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        this.gridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.Execute(data);
                        if (data.pageData && (data.pageData.lastPageNumber * 10) > 0)
                            this.isHidePagination = false;
                        else
                            this.isHidePagination = true;
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.gridParams['totalRecords'] = 1;
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    //Function to get data on pagination
    public getCurrentPage(currentPage: any): void {
        this.gridParams['pageCurrent'] = currentPage.value;
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    //Function to Refresh Grid
    public refreshGrid(): void {
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    //Function to sort the Grid data
    public riGridSort(): void {
        this.riGrid.DescendingSort = false;
        this.loadGridData();
    }
}

