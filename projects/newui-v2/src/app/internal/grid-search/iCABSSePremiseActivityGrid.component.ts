import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Component, OnInit, OnDestroy, Injector, ViewChild } from '@angular/core';

import { BaseComponent } from '../../base/BaseComponent';
import { PageIdentifier } from '../../base/PageIdentifier';
import { GridAdvancedComponent } from '../../../shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { EllipsisComponent } from './../../../shared/components/ellipsis/ellipsis';
import { EmployeeSearchComponent } from './../../internal/search/iCABSBEmployeeSearch';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { ContractManagementModuleRoutes, InternalGridSearchSalesModuleRoutes, InternalMaintenanceApplicationModuleRoutes } from './../../base/PageRoutes';


@Component({
    templateUrl: 'iCABSSePremiseActivityGrid.html'
})

export class PremiseActivityGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('employeeSearchEllipsis') employeeSearchEllipsis: EllipsisComponent;

    private method: string = 'service-delivery/maintenance';
    private module: string = 'pda';
    private operation: string = 'Service/iCABSSePremiseActivityGrid';
    private currentContractType: string;

    public curPage: number = 1;
    public totalRecords: number = 0;
    public pageId: string = '';
    public isDisabledConfirmAll: boolean = false;
    public isDisabledConfirmDriveTimes: boolean = false;
    public isVisibleTravelTimeCheckbox: boolean = false;
    public isVisibleDrivingTimes: boolean = false;
    public controls: any = [
        { name: 'BranchServiceAreaCode', disabled: true },
        { name: 'BranchServiceAreaDesc', disabled: true },
        { name: 'ServicePlanNumber', disabled: true },
        { name: 'ServiceWeekNumber', disabled: true },
        { name: 'ContractNumber', disabled: true },
        { name: 'ContractName', disabled: true },
        { name: 'ServicePlanStartDate', disabled: true },
        { name: 'ServicePlanEndDate', disabled: true },
        { name: 'PremiseNumber', disabled: true },
        { name: 'PremiseName', disabled: true },
        { name: 'PremiseName', type: MntConst.eTypeDate },
        { name: 'EmployeeCode' },
        { name: 'EmployeeSurname', disabled: true },
        { name: 'DrivingStartTime' },
        { name: 'DrivingEndTime' },
        { name: 'EndMileage' },
        { name: 'KeyedStartTime', required: true },
        { name: 'KeyedEndTime', required: true },
        { name: 'DateFrom', required: true },
        { name: 'DateTo', required: true },
        { name: 'ServiceDateStart', required: true },
        { name: 'EnterTravelTimes' }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPREMISEACTIVITYGRID;
        this.browserTitle = 'Premises Activity';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageTitle = 'Premises Activity';
        this.windowOnload();
        this.riGrid.FunctionUpdateSupport = true;
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public ellipsis = {
        employeeCodeEllipsis: {
            showCloseButton: true,
            showHeader: true,
            childparams: {
                parentMode: 'LookUp-Service-All'

            },
            component: EmployeeSearchComponent
        }
    };

    private windowOnload(): void {
        this.riGrid.FunctionUpdateSupport = true;
        this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
        this.riExchange.getParentHTMLValue('ServicePlanNumber');
        this.riExchange.getParentHTMLValue('ServiceWeekNumber');
        this.riExchange.getParentHTMLValue('ServicePlanStartDate');
        this.riExchange.getParentHTMLValue('ServicePlanEndDate');
        if (!this.getControlValue('BranchServiceAreaCode'))
            this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentAttributeValue('BranchServiceAreaCode'));
        if (!this.getControlValue('ServicePlanNumber'))
            this.setControlValue('ServicePlanNumber', this.riExchange.getParentAttributeValue('ServicePlanNumber'));
        this.setControlValue('ServiceDateStart', this.riExchange.getParentHTMLValue('ServiceDate'));
        if (!this.getControlValue('ServiceDateStart'))
            this.setControlValue('ServiceDateStart', this.riExchange.getParentAttributeValue('PlannedVisitDate'));
        this.setControlValue('ContractNumber', this.riExchange.getParentAttributeValue('ContractNumber'));
        this.setControlValue('ContractName', this.riExchange.getParentAttributeValue('ContractName'));
        this.setControlValue('PremiseNumber', this.riExchange.getParentAttributeValue('PremiseNumber'));
        this.setControlValue('PremiseName', this.riExchange.getParentAttributeValue('PremiseName'));
        if (this.riExchange.getParentAttributeValue('PremiseVisitRowID') === 'No') {
            this.isDisabledConfirmAll = false;
            this.isVisibleTravelTimeCheckbox = true;
        } else {
            this.isDisabledConfirmAll = true;
        }
        this.getServiceDetails();
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('PlannedVisitDate', 'Activity', 'PlannedVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('PlannedVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('PlannedVisitDate', true);

        this.riGrid.AddColumn('ProdCode', 'Activity', 'ProdCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('ProdCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ProdDesc', 'Activity', 'ProdDesc', MntConst.eTypeText, 20);

        this.riGrid.AddColumn('VisitFrequency', 'Activity', 'VisitFrequency', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('VisitFrequency', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PlanQuantity', 'Activity', 'PlanQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PlanQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('PlanQuantity', true);

        this.riGrid.AddColumn('VisitTypeCode', 'Activity', 'VisitTypeCode', MntConst.eTypeCode, 3);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('VisitTypeCode', true);

        this.riGrid.AddColumn('ActualVisitDate', 'Activity', 'ActualVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ActualVisitDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('DeliveryNoteNumber', 'Activity', 'DeliveryNoteNumber', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnUpdateSupport('DeliveryNoteNumber', true);

        this.riGrid.AddColumn('ConfirmVisit', 'Activity', 'ConfirmVisit', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('ConfirmVisit', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
        this.riGridBeforeExecute();
    }

    public riGridBeforeExecute(): void {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set(this.serviceConstants.Action, '2');
        search.set('riCacheRefresh', 'True');
        search.set('riGridMode', '0');
        search.set('riGridHandle', this.utils.gridHandle);
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('ServicePlanNumber', this.getControlValue('ServicePlanNumber'));
        search.set('ContractNumber', this.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        search.set('EmployeeCode', this.getControlValue('EmployeeCode'));
        search.set('DateFrom', this.globalize.parseDateToFixedFormat(this.getControlValue('DateFrom')).toString());
        search.set('DateTo', this.globalize.parseDateToFixedFormat(this.getControlValue('DateTo')).toString());
        search.set('ServiceDateStart', this.globalize.parseDateToFixedFormat(this.getControlValue('ServiceDateStart')).toString());
        search.set('KeyedStartTime', this.utils.hmsToSeconds(this.getControlValue('KeyedStartTime')));
        search.set('KeyedEndTime', this.utils.hmsToSeconds(this.getControlValue('KeyedEndTime')));
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.GridCacheRefresh, 'True');
        search.set(this.serviceConstants.PageSize, '10');
        search.set(this.serviceConstants.GridPageCurrent, this.curPage.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.method, this.module, this.operation, search).subscribe(
            (data) => {
                this.isRequesting = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data) {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.riGrid.Mode = MntConst.eModeNormal;
                        this.curPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.Execute(data);
                    }
                }
            },
            (error) => {
                this.logger.log('Error', error);
                this.isRequesting = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });

    }

    //to populate data in all fields
    private getServiceDetails(): void {
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams.Function = 'GetServiceDetails';
        postParams.BranchNumber = this.utils.getBranchCode();
        postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        postParams.ServicePlanNumber = this.getControlValue('ServicePlanNumber');
        postParams.PremiseNumber = this.getControlValue('PremiseNumber');
        postParams.ContractNumber = this.getControlValue('ContractNumber');
        if (this.getControlValue('ServiceDateStart'))
            postParams.ServiceDateStart = this.globalize.parseDateToFixedFormat(this.getControlValue('ServiceDateStart'));
        postParams.PremiseVisitRowID = this.riExchange.getParentAttributeValue('PremiseVisitRowID');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
                        this.setControlValue('EmployeeCode', data.EmployeeCode);
                        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                        if (data.KeyedEndTime)
                            this.setControlValue('KeyedEndTime', this.utils.secondsToHms(data.KeyedEndTime));
                        if (data.KeyedStartTime)
                            this.setControlValue('KeyedStartTime', this.utils.secondsToHms(data.KeyedStartTime));
                        if (data.ServiceDateStart)
                            this.setControlValue('ServiceDateStart', data.ServiceDateStart);
                        this.setControlValue('ServicePlanStartDate', data.ServicePlanStartDate);
                        this.setControlValue('ServicePlanEndDate', data.ServicePlanEndDate);
                        this.setControlValue('ServiceWeekNumber', data.ServiceWeekNumber);
                        this.setControlValue('DateFrom', this.getControlValue('ServicePlanStartDate'));
                        this.setControlValue('DateTo', this.getControlValue('ServicePlanEndDate'));
                        this.buildGrid();
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    // On double click of the grid row
    public riGridBodyOnDblClick(event: any): void {
        let columnName: string = this.riGrid.CurrentColumnName;
        switch (columnName) {
            case 'ProdCode':
                this.attributes['ServiceCoverRowID'] = this.riGrid.Details.GetAttribute('ProdCode', 'additionalproperty');
                switch (this.riGrid.Details.GetAttribute('PlannedVisitDate', 'additionalproperty')) {
                    case 'C':
                        this.currentContractType = '';
                        break;
                    case 'J':
                        this.currentContractType = '<job>';
                        break;
                    case 'P':
                        this.currentContractType = '<product>';
                        break;
                }
                if (this.currentContractType === '<product>') {
                    this.navigate('ServicePlanning', InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE, {
                        currentContractTypeURLParameter: this.currentContractType
                    });
                } else {
                    this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                        currentContractTypeURLParameter: this.currentContractType
                    });
                }
                break;
            case 'ConfirmVisit':
                let objTR = event.srcElement.parentElement.parentElement;
                if (event.srcElement.tagName === 'IMG')
                    objTR = event.srcElement.parentElement.parentElement.parentElement;
                this.attributes['OutputRowIDString'] = objTR.children[4].children[0].children[0].getAttribute('rowid');
                this.confirmSingleVisit();
        }
    }

    //after clicking on the visited column of any row
    public confirmSingleVisit(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            let searchParams: any = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            let postParams: any = {};
            postParams.Function = 'ConfirmSingleVisit';
            postParams.BranchNumber = this.utils.getBranchCode();
            postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
            postParams.ServicePlanNumber = this.getControlValue('ServicePlanNumber');
            postParams.ContractNumber = this.getControlValue('ContractNumber');
            postParams.PremiseNumber = this.getControlValue('PremiseNumber');
            postParams.DateFrom = this.globalize.parseDateToFixedFormat(this.getControlValue('DateFrom'));
            postParams.DateTo = this.globalize.parseDateToFixedFormat(this.getControlValue('DateTo'));
            postParams.ServiceDateStart = this.globalize.parseDateToFixedFormat(this.getControlValue('ServiceDateStart'));
            postParams.EmployeeCode = this.getControlValue('EmployeeCode');
            postParams.OutputRowIDString = this.attributes['OutputRowIDString'];
            postParams.KeyedStartTime = this.utils.hmsToSeconds(this.getControlValue('KeyedStartTime'));
            postParams.KeyedEndTime = this.utils.hmsToSeconds(this.getControlValue('KeyedEndTime'));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.buildGrid();
                            if (data.ErrorMessageDesc) {
                                let arrError: Array<string> = data.ErrorMessageDesc.split('|');
                                this.modalAdvService.emitMessage(new ICabsModalVO(arrError));
                            }
                        }
                    },
                    (error) => {
                        this.errorService.emitError(error);
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        } else {
            let objICabsModalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.visitnotcreated);
            this.modalAdvService.emitMessage(objICabsModalVO);
        }
    }

    //after clicking on the confirm alll button
    public confirmAllClick(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            let searchParams: any = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            let postParams: any = {};
            postParams.Function = 'ConfirmAll';
            postParams.BranchNumber = this.utils.getBranchCode();
            postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
            postParams.ServicePlanNumber = this.getControlValue('ServicePlanNumber');
            postParams.ContractNumber = this.getControlValue('ContractNumber');
            postParams.PremiseNumber = this.getControlValue('PremiseNumber');
            postParams.DateFrom = this.globalize.parseDateToFixedFormat(this.getControlValue('DateFrom'));
            postParams.DateTo = this.globalize.parseDateToFixedFormat(this.getControlValue('DateTo'));
            postParams.ServiceDateStart = this.globalize.parseDateToFixedFormat(this.getControlValue('ServiceDateStart'));
            postParams.EmployeeCode = this.getControlValue('EmployeeCode');
            postParams.KeyedStartTime = this.utils.hmsToSeconds(this.getControlValue('KeyedStartTime'));
            postParams.KeyedEndTime = this.utils.hmsToSeconds(this.getControlValue('KeyedEndTime'));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.ErrorMessageDesc) {
                            let arrError: Array<string> = data.ErrorMessageDesc.split('|');
                            this.modalAdvService.emitMessage(new ICabsModalVO(arrError));
                            this.refresh();
                            this.isDisabledConfirmAll = true;
                        }
                        else if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));

                        } else {
                            this.refresh();
                        }
                    },
                    (error) => {
                        this.errorService.emitError(error);
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        } else {
            let objICabsModalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.visitnotcreated);
            this.modalAdvService.emitMessage(objICabsModalVO);
        }
    }

    //after changing the column values
    public onCellBlur(): void {
        let oldValueQuantity: string = this.riGrid.previousValues[4].value;
        let newValueQuantity: string = this.riGrid.Details.GetValue('PlanQuantity');
        let oldValueVisitType: string = this.riGrid.previousValues[5].value;
        let newValueVisitType: string = this.riGrid.Details.GetValue('VisitTypeCode');
        let oldValueDeliveryNote: string = this.riGrid.previousValues[7].value;
        let newValueDeliveryNote: string = this.riGrid.Details.GetValue('DeliveryNoteNumber');
        if (newValueQuantity && newValueVisitType) {
            if (oldValueQuantity !== newValueQuantity || oldValueVisitType !== newValueVisitType || oldValueDeliveryNote !== newValueDeliveryNote) {
                this.UpdatePremisesActivityGrid(newValueQuantity, newValueVisitType, newValueDeliveryNote);
            }
        }
    }

    //after updating the grid
    private UpdatePremisesActivityGrid(newValueQuantity: string, newValueVisitType: string, newValueDeliveryNote: string): void {
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '2');
        let postParams: any = {};
        postParams.PlannedVisitDate = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('PlannedVisitDate'));
        postParams.ProdCode = this.riGrid.Details.GetValue('ProdCode');
        postParams.ProdDesc = this.riGrid.Details.GetValue('ProdDesc');
        postParams.VisitFrequency = this.riGrid.Details.GetValue('VisitFrequency');
        postParams.PlanQuantityRowID = this.riGrid.Details.GetAttribute('PlanQuantity', 'rowid');
        postParams.PlanQuantity = newValueQuantity;
        postParams.VisitTypeCodeRowID = this.riGrid.Details.GetAttribute('VisitTypeCode', 'rowid');
        postParams.VisitTypeCode = newValueVisitType;
        postParams.ActualVisitDate = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('ActualVisitDate'));
        postParams.DeliveryNoteNumber = newValueDeliveryNote;
        postParams.BranchNumber = this.utils.getBranchCode();
        postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        postParams.ServicePlanNumber = this.getControlValue('ServicePlanNumber');
        postParams.PremiseNumber = this.getControlValue('PremiseNumber');
        postParams.ContractNumber = this.getControlValue('ContractNumber');
        postParams.EmployeeCode = this.getControlValue('EmployeeCode');
        postParams.DateFrom = this.globalize.parseDateToFixedFormat(this.getControlValue('DateFrom'));
        postParams.DateTo = this.globalize.parseDateToFixedFormat(this.getControlValue('DateTo'));
        postParams.KeyedStartTime = this.utils.hmsToSeconds(this.getControlValue('KeyedStartTime'));
        postParams.KeyedEndTime = this.utils.hmsToSeconds(this.getControlValue('KeyedEndTime'));
        postParams.ServiceDateStart = this.globalize.parseDateToFixedFormat(this.getControlValue('ServiceDateStart'));
        postParams.riGridMode = '3';
        postParams.riGridHandle = this.utils.randomSixDigitString();
        postParams.HeaderClickedColumn = this.riGrid.HeaderClickedColumn;
        postParams.riSortOrder = this.riGrid.SortOrder;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.buildGrid();
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private ValidateVisitParameters(): void {
        this.setRequiredStatus('ServiceDateStart', true);
        this.setRequiredStatus('DrivingStartTime', true);
        this.setRequiredStatus('DrivingEndTime', true);
        this.setRequiredStatus('EndMileage', true);
    }

    //after clicking on the Confirm drive times button
    public onConfirmDriveTimesClick(): void {
        this.ValidateVisitParameters();
        if (this.riExchange.validateForm(this.uiForm)) {
            let searchParams: any = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            let postParams: any = {};
            postParams.Function = 'ConfirmDriveTimes';
            postParams.BranchNumber = this.utils.getBranchCode();
            postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
            postParams.ServiceDateStart = this.globalize.parseDateToFixedFormat(this.getControlValue('ServiceDateStart'));
            postParams.DrivingStartTime = this.utils.hmsToSeconds(this.getControlValue('DrivingStartTime'));
            postParams.DrivingEndTime = this.utils.hmsToSeconds(this.getControlValue('DrivingEndTime'));
            postParams.EmployeeCode = this.getControlValue('EmployeeCode');
            postParams.EndMileage = this.getControlValue('EndMileage');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.buildGrid();
                            this.disableControl('DrivingStartTime', true);
                            this.disableControl('DrivingEndTime', true);
                            this.disableControl('EndMileage', true);
                            this.isDisabledConfirmDriveTimes = true;
                        }
                    },
                    (error) => {
                        this.errorService.emitError(error);
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        }
    }

    // on change of the EnterTravelTimes CheckBox
    public onChangeEnterTravelTimesCheckBox(): void {
        this.isVisibleDrivingTimes = false;
        if (this.getControlValue('EnterTravelTimes'))
            this.isVisibleDrivingTimes = true;
    }

    //After clicking on the Alert Lead button
    public onClickAlertLead(): void {
        this.navigate('New', InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_1, {
            currentContractTypeURLParameter: this.currentContractType
        });
    }

    public getEmployeeSurname(): void {
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams.Function = 'GetEmployeeSurname';
        postParams.EmployeeCode = this.getControlValue('EmployeeCode');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }


    public onDataReceivedEmployeeCode(data: any): void {
        if (data) {
            this.setControlValue('EmployeeCode', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
    }

    public getCurrentPage(currentPage: any): void {
        this.curPage = currentPage.value;
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateRow = true;
        this.riGrid.UpdateFooter = true;
        this.riGridBeforeExecute();
    }

    //grid refresh
    public refresh(): void {
        this.buildGrid();
    }

    public riGridSort(): void {
        this.riGridBeforeExecute();
    }
}

