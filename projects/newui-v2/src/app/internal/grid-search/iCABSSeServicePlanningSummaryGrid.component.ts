import { Component, Injector, OnInit, OnDestroy, AfterContentInit, ViewChild, QueryList, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';

import { BaseComponent } from '@base/BaseComponent';
import { BranchSummaryPlanGridDataInterface } from './grid-search-interfaces/branchSummaryPlanGrid.interface';
import { GcpService } from '../../../GCP/gcp-service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { GridSearchModuleConstants } from './grid-search.constant';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceServiceModuleRoutes, ServiceDeliveryModuleRoutes, AppModuleRoutes, InternalGridSearchServiceModuleRoutes, InternalGridSearchSalesModuleRoutes } from './../../base/PageRoutes';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ServicePlanningGridHelper } from './iCABSSeServicePlanningGridHelper.service';
import { TableComponent } from '@shared/components/table/table';

@Component({
    templateUrl: 'iCABSSeServicePlanningSummaryGrid.html',
    styles: [`
        .modal-open .modal.fade.in{
            top:10%;
            z-index:99999
        }
    `],
    encapsulation: ViewEncapsulation.None
})

export class ServicePlanningSummaryGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit, AfterViewInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('icabs-grid-cell') icabsGridCell: QueryList<any>;
    @ViewChild('promptModal') public promptModal;
    @ViewChild('navErrorDataTable') public navErrorDataTable: TableComponent;

    public branchServicePlanningSummaryMode: boolean = false;
    private brServicePlanGridSelectedData: BranchSummaryPlanGridDataInterface = {
        Action: 'ConfirmServicePlanBranch',
        BranchNumber: '',
        BranchServiceAreaCode: '',
        EndDate: '0',
        PlanDate: '',
        PlanEndDate: '',
        PlanSendDate: '',
        ServicePlanNumber: 0,
        StartDate: '',
        TotalNettValue: 0,
        TotalNoOfCalls: '0',
        TotalNoOfExchanges: '0',
        TotalTime: '0'
    };
    private gridBody: any;
    private queryParams: Object = {
        operation: 'Service/iCABSSeServicePlanningSummaryGrid',
        module: 'planning',
        method: 'service-planning/grid'
    };
    private servicePlanNumberList: any;
    private timeout = Observable.timer(500).take(1);
    private customAlertMsgTimeStamp: number = (new Date()).getMilliseconds();

    public arrInfo: Array<any>;
    public brServicePlanGridData: any;
    public controls: Array<Object> = [
        { name: 'BranchServiceAreaCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'WeekNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'StartDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'EndDate', required: true, type: MntConst.eTypeDate },
        { name: 'ServicePlanNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'TotalNoOfCalls', disabled: true, type: MntConst.eTypeInteger },
        { name: 'TotalNoOfExchanges', disabled: true, type: MntConst.eTypeInteger },
        { name: 'TotalWeight', disabled: true, type: MntConst.eTypeDecimal2 },
        { name: 'TotalWED', disabled: true, type: MntConst.eTypeDecimal1 },
        { name: 'TotalTime', disabled: true, type: MntConst.eTypeTime },
        { name: 'TotalNettValue', disabled: true, type: MntConst.eTypeCurrency },
        { name: 'DisplayAverageWeight', type: MntConst.eTypeCheckBox },
        { name: 'BranchNumber' },
        { name: 'PlanDate' },
        { name: 'PlanSendDate' },
        { name: 'PlanEndDate' },
        { name: 'PlanExtractDate' },
        { name: 'ToOptomise', type: MntConst.eTypeCheckBox },
        { name: 'ToOptomiseDates' },
        { name: 'ServicePlanRID' },
        { name: 'OptimiseDates' },
        { name: 'OptimisePlan' },
        { name: 'PlannedVisitDate' },
        { name: 'RowID' },
        { name: 'VisitTypeDesc' },
        { name: 'Sel' },
        { name: 'EndDateBrServicePlan', disabled: true, type: MntConst.eTypeDate }
    ];
    public customAlertMsg: any;
    public isHidePagination: boolean = true;
    public gridParams: any = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0
    };
    public messageType: string = 'error';
    public naverrorlist: any;
    public pageId: string = '';
    public promptCallback: any;
    public promptContent: any = '';
    public promptNoCallback: any;
    public promptTitle: any = 'Update Navision Fails';
    public showMessageHeader: any = true;
    public reserveStockRptConstant: any = GridSearchModuleConstants.reserveStockReport;
    public isStockAvailable: boolean = true;

    public columns: any[] = [];
    public rows: any[] = ['Please wait, loading data.....'];

    constructor(injector: Injector, private gcpService: GcpService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNINGSUMMARYGRID;
        this.browserTitle = this.pageTitle = 'Service Planning Summary';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.brServicePlanConstants = GridSearchModuleConstants.branchServicePlanningSummary;
        this.pageParams.brServicePlanGridTotalCols = GridSearchModuleConstants.branchServicePlanningSummary.gridColCountPerRow;
        this.pageParams.buttonTitleWhenDisabled = GridSearchModuleConstants.branchServicePlanningSummary.btnDisabledTitle;
        this.pageParams.enableBrServicePlanActButtons = false;
        this.pageParams.enableBrServicePlanSelectAll = false;
        this.getSysCharDtetails();
        this.checkParentMode();
    }

    ngAfterContentInit(): void {
        if (this.isReturning()) {
            this.riGrid.FunctionPaging = true;
            this.riGrid.FunctionUpdateSupport = true;
            this.populateUIFromFormData();
            this.planningDiaryOptions();
        }
        else {
            this.windowOnload();
        }
        this.columns = [
            { 'title': 'Nav Company Code', 'name': 'CompanyCode', 'colCode': 'CompanyCode', 'isSorted': false, 'size': 8 },
            { 'title': 'Depot Code', 'sort': 'asc', 'name': 'DepotCode', 'colCode': 'DepotCode', 'isSorted': false, 'size': 5 },
            { 'title': 'iCABS Product', 'sort': 'asc', 'name': 'iCABSProduct', 'colCode': 'iCABSProduct', 'isSorted': false, 'size': 8 },
            { 'title': 'NAV Product Code', 'sort': 'asc', 'name': 'NavProduct', 'colCode': 'NavProduct', 'isSorted': false, 'size': 8 },
            { 'title': 'iCABS Descrption', 'sort': 'asc', 'name': 'iCABSProductDesc', 'colCode': 'iCABSProductDesc', 'isSorted': false, 'size': 20 },
            { 'title': 'Qty Available', 'sort': 'asc', 'name': 'QtyAvailable', 'colCode': 'QtyAvailable', 'isSorted': false, 'size': 8 },
            { 'title': 'Total Quantity Requested', 'sort': 'asc', 'name': 'QtyRequested', 'colCode': 'QtyRequested', 'isSorted': false, 'size': 8 },
            { 'title': 'Total Quantity Reserved', 'sort': 'asc', 'name': 'QtyReserved', 'colCode': 'QtyReserved', 'isSorted': false, 'size': 8 },
            { 'title': 'Difference', 'sort': 'asc', 'name': 'QtrDifference', 'colCode': 'QtrDifference', 'isSorted': true, 'size': 8 },
            { 'title': 'ATP Date', 'sort': 'asc', 'name': 'AvailableDate', 'colCode': 'AvailableDate', 'isSorted': false, 'size': 8 },
            { 'title': 'Service Area Affected', 'sort': 'NA', 'name': 'AffectedAreas', 'colCode': 'AffectedAreas', 'isSorted': false }
        ];
    }

    ngAfterViewInit(): void {

    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private checkParentMode(): void {
        this.branchServicePlanningSummaryMode = this.parentMode.toLowerCase() === ServicePlanningGridHelper.BRANCH_SERVICE_PLANNING_SUMMARY.toLowerCase();
        this.browserTitle = this.pageTitle = (this.parentMode.toLowerCase() === ServicePlanningGridHelper.BRANCH_SERVICE_PLANNING_SUMMARY.toLowerCase()) ? this.pageParams.brServicePlanConstants.pageTitle : 'Service Planning Summary';
    }

    private getSysCharDtetails(): any {
        let sysCharNumbers: number[] = [
            this.sysCharConstants.SystemCharSingleServicePlanPerBranch,
            this.sysCharConstants.SystemCharEnableWED,
            this.sysCharConstants.SystemCharEnableRouteOptimisationSoftwareIntegration,
            this.sysCharConstants.SystemCharUseServiceListingAsWorklist
        ];
        let sysCharIp: Object = {
            module: this.queryParams['module'],
            operation: this.queryParams['operation'],
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharNumbers.toString()
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.speedScript.sysChar(sysCharIp).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    let record: any = data.records;
                    if (data && data.records.length > 0) {
                        this.pageParams.vSCEnableSinglePlanPerBranch = record[0]['Logical'];
                        this.pageParams.vEnableWED = record[1]['Required'];
                        this.pageParams.vRouteOptimisation = record[2]['Required'];
                        this.pageParams.vUseServiceListing = record[3]['Required'];
                        if (this.pageParams.vEnableWED) {
                            this.disableControl('TotalWED', true);
                            this.setControlValue('TotalWED', this.riExchange.getParentHTMLValue('TotalWED'));
                            this.pageParams.isTotalWed = true;
                        }
                        if (this.pageParams.vRouteOptimisation) {
                            this.pageParams.isOptimse = true;
                            this.pageParams.isUndo = true;
                        }
                        this.planningDiaryOptions();
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private windowOnload(): void {
        this.riGrid.FunctionPaging = true;
        this.riGrid.FunctionUpdateSupport = true;
        this.setControlValue('StartDate', this.riExchange.getParentHTMLValue('StartDate'));
        this.setControlValue('EndDate', this.riExchange.getParentHTMLValue('EndDate'));
        this.setControlValue('WeekNumber', this.riExchange.getParentHTMLValue('WeekNumber'));
        this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
        this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname'));
        this.setControlValue('ServicePlanNumber', this.riExchange.getParentHTMLValue('ServicePlanNumber'));
        this.setControlValue('TotalNoOfCalls', this.riExchange.getParentHTMLValue('TotalNoOfCalls'));
        this.setControlValue('TotalNoOfExchanges', this.riExchange.getParentHTMLValue('TotalNoOfExchanges'));
        this.setControlValue('TotalWeight', this.riExchange.getParentHTMLValue('TotalWeight'));
        this.setControlValue('TotalTime', this.riExchange.getParentHTMLValue('TotalTime'));
        this.setControlValue('TotalTimeInteger', this.riExchange.getParentHTMLValue('TotalTimeInteger'));
        this.setControlValue('TotalNettValue', this.riExchange.getParentHTMLValue('TotalNettValue'));
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('EndDateBrServicePlan', this.riExchange.getParentHTMLValue('EndDate'));
    }

    private disableButton(): void {
        if (this.getControlValue('ServicePlanNumber') !== '' && this.getControlValue('ServicePlanNumber') !== 0 || this.riGrid.bodyArray.length === 0)
            this.pageParams.isUndoConfirmEnable = true;
        else
            this.pageParams.isUndoConfirmEnable = false;
    }

    private planningDiaryOptions(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};

        searchParams.set(this.serviceConstants.Action, '6');
        postParams['ActionType'] = 'GetPlanningDiaryOptions';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    if (data['DisplayAverageWeight'] === 'yes') {
                        this.pageParams.isTotalWeight = true;
                        this.setControlValue('DisplayAverageWeight', true);
                    } else {
                        this.setControlValue('DisplayAverageWeight', false);
                        this.pageParams.isTotalWeight = false;
                    }
                    this.branchServicePlanningSummaryMode ? this.buildGridForBranchServicePlanSummary() : this.buildGrid();
                    this.disableButton();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('Optomised', 'PlanVisit', 'Optomised', MntConst.eTypeImage, 1);
        this.riGrid.AddColumn('ToOptomise', 'PlanVisit', 'ToOptomise', MntConst.eTypeCheckBox, 10);
        this.riGrid.AddColumn('GrdServicePlanNumber', 'PlanVisit', 'GrdServicePlanNumber', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('ServicePlanDescription', 'PlanVisit', 'ServicePlanDescription', MntConst.eTypeText, 18);
        this.riGrid.AddColumn('PlannedVisitDate', 'PlanVisit', 'PlannedVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('Calls', 'PlanVisit', 'Calls', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Exchanges', 'PlanVisit', 'Exchanges', MntConst.eTypeInteger, 5);
        if (this.getControlValue('DisplayAverageWeight')) {
            this.riGrid.AddColumn('Weight', 'PlanVisit', 'Weight', MntConst.eTypeDecimal2, 5);
            this.riGrid.AddColumnAlign('Weight', MntConst.eAlignmentCenter);
        }
        if (this.pageParams.vEnableWED) {
            this.riGrid.AddColumn('WEDValue', 'PlanVisit', 'WEDValue', MntConst.eTypeDecimal2, 5);
            this.riGrid.AddColumnAlign('WEDValue', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('Time', 'PlanVisit', 'Time', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('NettValue', 'PlanVisit', 'NettValue', MntConst.eTypeCurrency, 5);
        this.riGrid.AddColumn('Routine', 'PlanVisit', 'Routine', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Installations', 'PlanVisit', 'Installations', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Removals', 'PlanVisit', 'Removals', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Repairs', 'PlanVisit', 'Repairs', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Urgents', 'PlanVisit', 'Urgents', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Deliveries', 'PlanVisit', 'Deliveries', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('WorkUnits', 'PlanVisit', 'WorkUnits', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('SendDate', 'PlanVisit', 'SendDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('ExpectedEndDate', 'PlanVisit', 'ExpectedEndDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('Optomised', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ToOptomise', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('GrdServicePlanNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServicePlanDescription', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PlannedVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Calls', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Exchanges', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Time', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('NettValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('Routine', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Installations', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Removals', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Repairs', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Urgents', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Deliveries', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('WorkUnits', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('SendDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ExpectedEndDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('SendDate', true);
        this.riGrid.AddColumnUpdateSupport('ExpectedEndDate', true);
        this.riGrid.Complete();
        this.riGrid.RefreshRequired();
        this.gridBeforeExecute();
        this.loadGridData();
    }

    private buildGridForBranchServicePlanSummary(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('Area', 'BranchServiceArea', 'Area', MntConst.eTypeText, 1);
        this.riGrid.AddColumn('Description', 'BranchServiceArea', 'Description', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('Employee', 'BranchServiceArea', 'Employee', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('EmployeeName', 'BranchServiceArea', 'EmployeeName', MntConst.eTypeText, 18);
        this.riGrid.AddColumn('StartDate', 'BranchServiceArea', 'StartDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('EndDate', 'BranchServiceArea', 'EndDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('Calls', 'BranchServiceArea', 'Calls', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Exchanges', 'BranchServiceArea', 'Exchanges', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Time', 'BranchServiceArea', 'Time', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('NettValue', 'BranchServiceArea', 'NettValue', MntConst.eTypeCurrency, 5);
        this.riGrid.AddColumn('Routine', 'BranchServiceArea', 'Routine', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Installations', 'BranchServiceArea', 'Installations', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Removals', 'BranchServiceArea', 'Removals', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Repairs', 'BranchServiceArea', 'Repairs', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Urgents', 'BranchServiceArea', 'Urgents', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Deliveries', 'BranchServiceArea', 'Deliveries', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('WorkUnits', 'BranchServiceArea', 'WorkUnits', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('SendDate', 'BranchServiceArea', 'SendDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('Sel', 'BranchServiceArea', 'Sel', MntConst.eTypeCheckBox, 1);

        this.riGrid.AddColumnAlign('Area', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Description', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Employee', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('EmployeeName', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('StartDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('EndDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Calls', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Exchanges', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Time', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('NettValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('Routine', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Installations', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Removals', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Repairs', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Urgents', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Deliveries', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('WorkUnits', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('SendDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Sel', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('SendDate', true);
        this.riGrid.Complete();
        this.riGrid.RefreshRequired();
        this.gridBeforeExecute();
        this.loadGridData();
    }

    private updateGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};

        search.set(this.serviceConstants.Action, '0');
        postParams[this.serviceConstants.GridMode] = '3';
        postParams[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        postParams[this.serviceConstants.PageSize] = this.gridParams['itemsPerPage'];
        postParams[this.serviceConstants.PageCurrent] = this.gridParams['pageCurrent'];
        postParams[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        postParams[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        postParams['BranchNumber'] = this.getControlValue('BranchNumber');
        postParams['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        postParams['ServicePlanNumber'] = this.getControlValue('ServicePlanNumber');
        postParams['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
        postParams['EndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate'));
        postParams['PlanDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('PlanDate'));
        postParams['SendDate'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('SendDate'));
        postParams['PlanSendDate'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('SendDate'));
        this.setControlValue('PlanSendDate', this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('SendDate')));
        postParams['PlanEndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('PlanExtractDate'));
        postParams['ToOptomise'] = this.globalize.parseDateToFixedFormat(this.getControlValue('ToOptomiseDates'));
        postParams['ServicePlanRID'] = this.riGrid.Details.GetAttribute('SendDate', 'additionalproperty').split('|')[1] || '';
        postParams['ToOptomiseRowID'] = this.riGrid.Details.GetAttribute('ToOptomise', 'rowid');
        postParams['ToOptomise'] = this.riGrid.Details.GetValue('ToOptomise');
        postParams['GrdServicePlanNumber'] = this.riGrid.Details.GetValue('GrdServicePlanNumber');
        postParams['ServicePlanDescription'] = this.riGrid.Details.GetValue('ServicePlanDescription');
        postParams['PlannedVisitDateRowID'] = this.riGrid.Details.GetAttribute('PlannedVisitDate', 'rowid');
        postParams['PlannedVisitDate'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('PlannedVisitDate'));
        postParams['Calls'] = this.riGrid.Details.GetValue('Calls');
        postParams['Exchanges'] = this.riGrid.Details.GetValue('Exchanges');
        postParams['Time'] = this.riGrid.Details.GetValue('Time');
        postParams['NettValue'] = this.globalize.parseCurrencyToFixedFormat(this.riGrid.Details.GetValue('NettValue'));
        postParams['Routine'] = this.riGrid.Details.GetValue('Routine');
        postParams['InstallationsRowID'] = this.riGrid.Details.GetAttribute('Installations', 'rowid');
        postParams['Installations'] = this.riGrid.Details.GetValue('Installations');
        postParams['Removals'] = this.riGrid.Details.GetValue('Removals');
        postParams['Repairs'] = this.riGrid.Details.GetValue('Repairs');
        postParams['Urgents'] = this.riGrid.Details.GetValue('Urgents');
        postParams['DeliveriesRowID'] = this.riGrid.Details.GetAttribute('Deliveries', 'rowid');
        postParams['Deliveries'] = this.riGrid.Details.GetValue('Deliveries');
        postParams['WorkUnits'] = this.riGrid.Details.GetValue('WorkUnits');
        postParams['SendDateRowID'] = this.riGrid.Details.GetAttribute('SendDate', 'rowid');
        postParams['ExpectedEndDate'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('ExpectedEndDate'));
        if (this.getControlValue('DisplayAverageWeight') === 'yes') {
            postParams['Weight'] = this.riGrid.Details.GetValue('Weight');
        }
        if (this.pageParams.vEnableWED) {
            postParams['WEDValue'] = this.globalize.parseDecimal2ToFixedFormat(this.riGrid.Details.GetValue('WEDValue'));
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data) {
                    this.riGrid.Mode = MntConst.eModeNormal;
                }
            },
            error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.riGrid.Mode = MntConst.eModeNormal;
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private loadGridData(): void {
        let search: QueryParams = this.generateQueryParamsForGrid();
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.gridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * this.gridParams['itemsPerPage'] : 1;
                    if (this.riGrid.Update) {
                        if (this.getAttribute('Row') !== '') {
                            this.riGrid.StartRow = this.getAttribute('Row');
                            this.riGrid.StartColumn = 0;
                            this.riGrid.UpdateHeader = false;
                            this.riGrid.UpdateBody = true;
                            this.riGrid.UpdateFooter = true;
                            this.riGrid.RowID = this.getAttribute('RowID');
                        }
                    }

                    this.setFormMode(this.c_s_MODE_UPDATE);
                    this.riGrid.RefreshRequired();
                    this.riGrid.Execute(data);
                    this.brServicePlanGridData = data;

                    if (this.branchServicePlanningSummaryMode && (this.brServicePlanGridData.body && this.brServicePlanGridData.body.cells && this.brServicePlanGridData.body.cells.length)) {

                        this.timeout.subscribe(() => {
                            this.gridBody = this.riGrid.HTMLGridBody;

                            this.handleGridActionButtonsState();  //handling buttons state based upon checkboxes in grid
                        });
                    } else {
                        this.brServicePlanGridData = undefined; // will hide grid action buttons
                    }

                    if (data.pageData && (data.pageData.lastPageNumber * 10) > 0)
                        this.isHidePagination = false;
                    else
                        this.isHidePagination = true;
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.messageType = 'error';
                this.customAlertMsg = {
                    msg: error.fullError,
                    timestamp: (new Date()).getMilliseconds()
                };
            });
    }

    private generateQueryParamsForGrid(): QueryParams {
        let search: QueryParams = this.getURLSearchParamObject();
        const endDate: any = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate')) || '';
        const startDate: any = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate')) || '';
        if (this.branchServicePlanningSummaryMode) {
            search.set(this.serviceConstants.Action, '0');
            search.set(this.serviceConstants.RunMode, 'Branch');
            search.set(this.serviceConstants.PageSize, (10 || this.gridParams['itemsPerPage']));

        } else {
            search.set(this.serviceConstants.Action, '2');
        }

        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridMode, this.gridParams['riGridMode']);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.PageCurrent, this.gridParams['pageCurrent']);
        search.set(this.serviceConstants.PageSize, this.gridParams['itemsPerPage']);

        search.set('BranchNumber', this.getControlValue('BranchNumber'));
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('EndDate', endDate);
        search.set('PlanDate', this.getControlValue('PlanDate') || '');
        search.set('PlanEndDate', this.getControlValue('PlanExtractDate') || '');
        search.set('PlanSendDate', this.getControlValue('PlanSendDate') || '');
        search.set('ServicePlanNumber', this.getControlValue('ServicePlanNumber'));
        search.set('ServicePlanRID', this.getControlValue('ServicePlanRID'));
        search.set('StartDate', startDate);
        search.set('ToOptomise', this.getControlValue('ToOptomiseDates') || '');
        return search;
    }

    private optimisePlan(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};

        searchParams.set(this.serviceConstants.Action, '6');
        postParams['Function'] = 'OptimisePlan';
        postParams['BranchNumber'] = this.getControlValue('BranchNumber');
        postParams['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        postParams['OptimiseDates'] = this.globalize.parseDateToFixedFormat(this.getControlValue('ToOptomiseDates'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.setControlValue('OptimisePlan', data['OptimisedPlan']);
                    this.gridRefresh();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    private undoPlan(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};

        searchParams.set(this.serviceConstants.Action, '6');
        postParams['Function'] = 'UndoPlan';
        postParams['BranchNumber'] = this.getControlValue('BranchNumber');
        postParams['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        postParams['OptimiseDates'] = this.globalize.parseDateToFixedFormat(this.getControlValue('ToOptomiseDates'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.setControlValue('OptimisePlan', data['OptimisedPlan']);
                    this.buildGrid();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    private promptUndoProposedPlan(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};

        searchParams.set(this.serviceConstants.Action, '6');
        postParams['Action'] = 'UndoServicePlan';
        postParams['BranchNumber'] = this.getControlValue('BranchNumber');
        postParams['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        postParams['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate') || '');
        postParams['EndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate') || '');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    if (data.ErrorMessage) {
                        this.gridRefresh();
                        this.modalAdvService.emitMessage(new ICabsModalVO(data.ErrorMessage));
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    private confirmPlan(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};
        const endDate: any = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate')) || '';
        const startDate: any = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate')) || '';

        searchParams.set(this.serviceConstants.Action, '6');
        postParams['Action'] = 'ConfirmServicePlan';
        postParams['BranchNumber'] = this.getControlValue('BranchNumber');
        postParams['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        postParams['ServicePlanNumber'] = this.getControlValue('ServicePlanNumber');
        postParams['StartDate'] = startDate;
        postParams['EndDate'] = endDate;
        /*This line is now being kept as commented as this might be helpful in future if any bugs are raised
        postParams['PlanDate'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetAttribute('SendDate', 'additionalproperty').split('|')[0]);
        */
        postParams['PlanDate'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('PlannedVisitDate'));
        postParams['PlanSendDate'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('SendDate'));
        postParams['PlanEndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('PlanExtractDate') || this.riGrid.Details.GetValue('ExpectedEndDate') || '');
        postParams['TotalNoOfCalls'] = this.getControlValue('TotalNoOfCalls');
        postParams['TotalNoOfExchanges'] = this.getControlValue('TotalNoOfExchanges');
        postParams['TotalTime'] = this.convertToSeconds(this.getControlValue('TotalTime')); //IUI-23969
        postParams['TotalNettValue'] = this.globalize.parseCurrencyToFixedFormat(this.getControlValue('TotalNettValue'));

        if (this.pageParams.vEnableWED) {
            postParams['TotalWED'] = this.globalize.parseDecimal1ToFixedFormat(this.getControlValue('TotalWED'));
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    if (data.ErrorMessage) {
                        this.setControlValue('ServicePlanNumber', data['ServicePlanNumber']);
                        this.modalAdvService.emitMessage(new ICabsModalVO(data['ErrorMessage']));
                        this.gridRefresh();
                    }
                    this.setControlValue('PlanDate', '');
                    this.setControlValue('PlanSendDate', '');
                    this.setControlValue('PlanExtractDate', '');

                    this.checkNAVStockReservationEnabled(); //only for Italy/4390 syschar
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    private convertToSeconds(value: any): string { //IUI-23969
        if (!value) { return '0'; }
        let parsedSeconds: any;
        let format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
        const hour: any = value.split(':')[0], min: any = value.split(':')[1];
        parsedSeconds = ((hour && !format.test(hour)) ? hour * 3600 : 0) + ((min && !format.test(min)) ? min * 60 : 0);  // INC-1874547-fix prod issue when value is without minute
        return parsedSeconds.toString();
    }

    private checkNAVStockReservationEnabled(): void {  //ITA-838 [navision would update only for Italy based on 4390 syschar]
        let querySysChar: QueryParams = this.getURLSearchParamObject();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.SystemCharNumber, this.sysCharConstants.SystemCharEnableNAVStockReservation.toString());

        this.httpService.sysCharRequest(querySysChar)
            .subscribe((response) => {
                if (response && response.records && response.records[0]['Required']) {
                    this.updateNavisionOnConfirm(this.getControlValue('BranchServiceAreaCode')); //ITA-614 > ita-664
                }

            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private ServiceCoverFocus(rsrcElement: any): void {
        let oTR = rsrcElement.parentElement.parentElement;
        this.setControlValue('PlannedVisitDate', this.riGrid.Details.GetValue('PlannedVisitDate'));
        this.setControlValue('VisitTypeDesc', rsrcElement.getAttribute('RowID'));
        oTR.focus();
    }

    private instr(str: any, substr: any, position: number = 0): any {
        return str.indexOf(substr, position);
    }

    private gridBeforeExecute(): void {

        let vServicePlanRID: string, vbAdditionalInfo: Array<any>, vPlanDate: string, vSendDate: string, vExtractEndDate: string;
        let planDateValue: string, sendDateValue: string, planSendDate: string, planExtractDate: string;

        sendDateValue = this.riGrid.Details.GetAttribute('SendDate', 'additionalproperty') ? this.riGrid.Details.GetAttribute('SendDate', 'additionalproperty') : '';
        vSendDate = this.riGrid.Details.GetValue('SendDate') ? this.riGrid.Details.GetValue('SendDate') : '';
        vExtractEndDate = this.riGrid.Details.GetValue('ExpectedEndDate') ? this.riGrid.Details.GetValue('ExpectedEndDate') : '';
        planDateValue = this.getControlValue('PlanDate') ? this.getControlValue('PlanDate') : '';
        planSendDate = this.getControlValue('PlanSendDate') ? this.getControlValue('PlanSendDate') : '';
        planExtractDate = this.getControlValue('PlanExtractDate') ? this.getControlValue('PlanExtractDate') : '';

        if (sendDateValue) {
            vbAdditionalInfo = this.riGrid.Details.GetAttribute('SendDate', 'additionalproperty').split('|');
            vPlanDate = vbAdditionalInfo[0];
            vServicePlanRID = vbAdditionalInfo[1];
            this.setControlValue('ServicePlanRID', vServicePlanRID);
        } else {
            vPlanDate = sendDateValue;
        }

        if (this.instr(planDateValue, vPlanDate)) {
            if (planDateValue === '') {
                this.setControlValue('PlanDate', this.globalize.parseDateToFixedFormat(vPlanDate));
                this.setControlValue('PlanSendDate', this.getControlValue('PlanSendDate'));
                this.setControlValue('PlanExtractDate', this.globalize.parseDateToFixedFormat(vExtractEndDate));
            } else {
                this.setControlValue('PlanDate', planDateValue + ';' + this.globalize.parseDateToFixedFormat(vPlanDate));
                this.setControlValue('PlanSendDate', planSendDate + ';' + this.globalize.parseDateToFixedFormat(vSendDate));
                this.setControlValue('PlanExtractDate', planExtractDate + ';' + this.globalize.parseDateToFixedFormat(vExtractEndDate));
            }
        } else {
            let tempPlanDate: Array<any>, tempSendDate: Array<any>, tempExtractEndDate: Array<any>, i: number = 0;

            tempPlanDate = this.getControlValue('PlanDate').split(';');
            tempSendDate = this.getControlValue('PlanSendDate').split(';');
            tempExtractEndDate = this.getControlValue('PlanExtractDate').split(';');
            planSendDate = '';
            planExtractDate = '';

            do {
                let vTempVarB: any, vTempVarC: any;
                vTempVarB = tempPlanDate[i];
                vTempVarC = tempExtractEndDate[i];
                if (tempPlanDate[i] === vPlanDate) {
                    vTempVarB = vSendDate;
                    vTempVarC = vExtractEndDate;
                }
                if (planSendDate === '')
                    planSendDate = vTempVarB;
                else
                    planSendDate = planSendDate + ';' + vTempVarB;
                if (planExtractDate === '')
                    planExtractDate = vTempVarC;
                else
                    planExtractDate = planExtractDate + ';' + vTempVarC;
                this.setControlValue('PlanSendDate', this.getControlValue('PlanSendDate') || this.globalize.parseDateToFixedFormat(planSendDate));
                this.setControlValue('PlanExtractDate', this.globalize.parseDateToFixedFormat(planExtractDate));
                i = i + 1;
            } while (i < tempPlanDate[i] + 1);
        }
    }

    private handleGridActionButtonsState(): void {

        // loop through all rows in grid and find checkbox state, if all are unchecked then disable buttons
        const totalRows: number = this.gridBody.children.length - 1 || 0;
        const totalColPerRow: number = GridSearchModuleConstants.branchServicePlanningSummary.gridColCountPerRow - 1;
        let checkBoxState: boolean = false;
        let selectAllStatesArray: boolean[] = [];

        if (totalRows) {
            for (let i = 0; i < totalRows; i++) {
                if (this.gridBody.children[i].children[totalColPerRow].children[0].children[0].checked) { // checked
                    checkBoxState = true;
                    selectAllStatesArray.push(false);

                } else { // unchecked
                    this.pageParams.enableBrServicePlanSelectAll = true;
                    selectAllStatesArray.push(true);
                }
            }
            this.pageParams.enableBrServicePlanActButtons = checkBoxState;
            this.pageParams.enableBrServicePlanSelectAll = selectAllStatesArray.indexOf(true) >= 0 ? true : false;
        }
    }

    private generateBrSummaryPlanGridAllRowsData(totalRows: number): void {
        if (totalRows) {
            this.resetBrServicePlanGridSelectedData();
            for (let i = 0; i < totalRows; i++) {
                this.generateBrSummaryPlanGridSelectedRowData(i, true, totalRows);
            }
        }
    }

    private resetBrServicePlanGridSelectedData(): void {
        this.brServicePlanGridSelectedData = {
            Action: 'ConfirmServicePlanBranch',
            BranchNumber: this.getControlValue('BranchNumber') || this.riExchange.getParentHTMLValue('BranchNumber'),
            BranchServiceAreaCode: '',
            EndDate: '',
            PlanDate: '',
            PlanEndDate: '',
            PlanSendDate: '',
            ServicePlanNumber: 0,
            StartDate: '',
            TotalNettValue: 0,
            TotalNoOfCalls: 0,
            TotalNoOfExchanges: 0,
            TotalTime: ''
        };
    }

    private addToPayloadArray(selectedDataArray: any, currentRowOrigin: any): any {
        selectedDataArray.push(this.riGrid.Details.GetValue('Area', currentRowOrigin));
        selectedDataArray = selectedDataArray.join(';');
        return selectedDataArray;
    }

    private removeToPayLoadArray(selectedDataArray: any, currentRowOrigin: any): any {
        if (selectedDataArray.length <= 1) { // if only one item in array
            return '';

        }
        else {
            selectedDataArray.splice(selectedDataArray.indexOf(this.riGrid.Details.GetValue('Area', currentRowOrigin)), 1);
            selectedDataArray = (selectedDataArray.length > 1) ? selectedDataArray.join(';') : selectedDataArray.toString() + ';';

            return selectedDataArray;
        }
    }

    private generateBrSummaryPlanGridSelectedRowData(currentRow: number, addToPayload: boolean, totalRows?: number): void {
        const currentRowOrigin = currentRow;
        currentRow = currentRow * this.pageParams.brServicePlanGridTotalCols;
        this.brServicePlanGridSelectedData['StartDate'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('StartDate', currentRowOrigin) || '');
        this.brServicePlanGridSelectedData['EndDate'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('EndDate', currentRowOrigin) || '');
        this.brServicePlanGridSelectedData['BranchNumber'] = this.getControlValue('BranchNumber') || this.riExchange.getParentHTMLValue('BranchNumber');

        let BranchServiceAreaCodeArray: any = (this.brServicePlanGridSelectedData['BranchServiceAreaCode'].search(';') > (-1)) ? this.brServicePlanGridSelectedData['BranchServiceAreaCode'].split(';') : this.brServicePlanGridSelectedData['BranchServiceAreaCode'] || '';

        if (Array.isArray(BranchServiceAreaCodeArray)) {
            if (BranchServiceAreaCodeArray[BranchServiceAreaCodeArray.length - 1] === '' || !BranchServiceAreaCodeArray[BranchServiceAreaCodeArray.length - 1]) {
                BranchServiceAreaCodeArray.pop();// remove last blank  from above split item
            }
            if (addToPayload) { // addition
                BranchServiceAreaCodeArray = this.addToPayloadArray(BranchServiceAreaCodeArray, currentRowOrigin);

            } else { //removal
                BranchServiceAreaCodeArray = this.removeToPayLoadArray(BranchServiceAreaCodeArray, currentRowOrigin);
            }

        } else { // string
            if (addToPayload) {//addition
                BranchServiceAreaCodeArray += this.riGrid.Details.GetValue('Area', currentRowOrigin) + ';';

            } else { //removal
                BranchServiceAreaCodeArray = '';
            }
        }
        this.brServicePlanGridSelectedData['BranchServiceAreaCode'] = BranchServiceAreaCodeArray;
    }

    private handleErrorOnAPI(data: any): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.messageType = 'error';
        this.customAlertMsg = {
            msg: (data['ErrorMessage'] && data['fullError']) ? data['ErrorMessage'] + data['fullError'] : (data['error']) ? data['error']['text'] : '  Unknown Error',
            timestamp: this.customAlertMsgTimeStamp
        };
    }

    private updateNavisionOnConfirm(value: any, servicePlanNumberList?: any): void { // ita-614/615 > 664  [call-3]
        let searchParams: any = this.getURLSearchParamObject();
        const endDate: any = this.getControlValue('EndDate') || this.riExchange.getParentHTMLValue('enddate') || this.activatedRoute.snapshot.queryParams['enddate'] || '';
        const startDate: any = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate') || this.riExchange.getParentHTMLValue('startdate') || this.activatedRoute.snapshot.queryParams['startdate']) || '';

        searchParams.set('cancelReservation', 'false');
        searchParams.set('confirmReservation', 'true');
        searchParams.set('planFromDate', startDate);
        searchParams.set('planToDate', endDate);
        searchParams.set('branchNumber', this.utils.getBranchCode());
        searchParams.set('email', this.utils.getEmail());

        if (value === 'branchsummary') {
            let BranchServiceAreaCode = this.brServicePlanGridSelectedData['BranchServiceAreaCode'];

            if (BranchServiceAreaCode.split(';')[1] === '' || BranchServiceAreaCode.split(';')[1].length === 0) {
                searchParams.set('branchServiceAreaList', BranchServiceAreaCode.split(';')[0]);
            } else {
                searchParams.set('branchServiceAreaList', this.brServicePlanGridSelectedData['BranchServiceAreaCode']);
            }
            searchParams.set('servicePlanNumberList', (servicePlanNumberList || ''));

        } else {
            searchParams.set('branchServiceAreaList', value);
        }

        const url = this.gcpService.getReserveStockUrl() + 'confirm/stock?'; //production issue fix

        this.httpService.getData(url, searchParams)
            .subscribe((response: any) => {

                if (response) {
                    let errorInResponse: any, errorNoInResponse: any;  //response[data] would always be blank
                    if (response.hasOwnProperty('naverrorlist')) {
                        if (response['naverrorlist']['records'] && response['naverrorlist']['records'].length) {
                            this.naverrorlist = response['naverrorlist'];
                            this.promptCallback = null;
                            this.brServicePlanGridSelectedData['BranchServiceAreaCode'] = '';
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        }
                    }

                    if (response.hasOwnProperty('error')) {
                        errorInResponse = response['error'];
                        errorNoInResponse = errorInResponse['ErrorNo'];
                    }

                    if (response.hasOwnProperty('Error')) {
                        errorInResponse = response['Error'];
                        errorNoInResponse = errorInResponse['ErrorNo'];
                    }

                    if (errorInResponse || errorNoInResponse) { // any error inspite of 20006/20007/0006/0007, show magic button
                        this.promptTitle = 'Update Navision Fails';
                        this.messageType = 'error';
                        this.customAlertMsg = {
                            msg: ('  Error :- ' + errorNoInResponse + ' -- ' + errorInResponse['Error']),
                            timestamp: this.customAlertMsgTimeStamp
                        };
                        this.showDialog(errorInResponse['Error'], this.updateNavisionOnConfirm); //ITA-844

                    } else {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.promptTitle = 'Update Navision Success !!';
                        this.showDialog('Stock Confirmation Success.....');
                    }
                } else { // if response is 200 but blank
                    this.promptTitle = 'Update Navision Fails';
                    this.messageType = 'error';
                    this.customAlertMsg = {
                        msg: ('  Error :- API without details, please check logs....... '),
                        timestamp: this.customAlertMsgTimeStamp
                    };
                    this.logger.warn(response);
                    this.showDialog(MessageConstant.PageSpecificMessage.navFailMessage, this.updateNavisionOnConfirm); //ITA-844
                }

            },
                (error: any) => { // status-500
                    let errorInResponse: any, errorNoInResponse: any;
                    if (error.hasOwnProperty('error')) {
                        errorInResponse = error['error'];
                        errorNoInResponse = errorInResponse['ErrorNo'];
                    } else if (error.hasOwnProperty('Error')) {
                        errorInResponse = error['Error'];
                        errorNoInResponse = errorInResponse['ErrorNo'];
                    }
                    this.messageType = 'error';
                    this.customAlertMsg = {
                        msg: (errorNoInResponse || errorInResponse['Error']) ? '  Error :- ' + (errorNoInResponse) ? errorNoInResponse : ' ' + errorInResponse['Error'] : MessageConstant.Message.TomcatErrorMessage,  //TomcatErrorMessage
                        timestamp: this.customAlertMsgTimeStamp
                    };
                    this.promptTitle = 'Update Navision Fails';
                    this.showDialog(MessageConstant.PageSpecificMessage.navFailMessage, this.updateNavisionOnConfirm); //ITA-844
                });
    }

    private parseServiceCodeForReserveStock(): any {
        const serviceAreaList: any = this.brServicePlanGridSelectedData['BranchServiceAreaCode'].split(';');
        return (serviceAreaList[serviceAreaList.length - 1] === '' || !serviceAreaList[serviceAreaList.length - 1]) ? serviceAreaList[0] : serviceAreaList.join(',');
    }

    public onGridBodyClick(data: any): void {
        let vbAdditionalInfo: Array<any>, vOptomiseDates: string, sendDateValue: string;

        switch (this.riGrid.CurrentColumnName) {
            case 'ToOptomise':
                sendDateValue = this.riGrid.Details.GetAttribute('SendDate', 'additionalproperty') ? this.riGrid.Details.GetAttribute('SendDate', 'additionalproperty') : '';
                if (sendDateValue !== '') {
                    vbAdditionalInfo = sendDateValue.split('|');
                    vOptomiseDates = vbAdditionalInfo[0];
                } else
                    vOptomiseDates = sendDateValue;
                if (this.riGrid.Details.GetValue('ToOptomise') === true) {
                    if (this.instr(this.getControlValue('ToOptomiseDates'), vOptomiseDates) !== 0) {
                        if (this.getControlValue('ToOptomiseDates') === '')
                            this.setControlValue('ToOptomiseDates', vOptomiseDates);
                        else
                            this.setControlValue('ToOptomiseDates', this.getControlValue('ToOptomiseDates') + ';' + vOptomiseDates);
                    }
                } else
                    this.setControlValue('ToOptomiseDates', this.getControlValue('ToOptomiseDates').replace(vOptomiseDates, ''));
                break;

            case 'Sel':
                /*
                    1. on select/check : add obj containing current row data in  brServicePlanGridSelectedData array for API payload
                    2. enable buttons  if checked
                    3. if unchecked then loop through all rows and all not checked then disable buttons
                */
                this.handleGridActionButtonsState();
                this.generateBrSummaryPlanGridSelectedRowData(this.riGrid.CurrentRow, data.target.checked);
                break;
        }

    }

    public onGridDblClick(data: any): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'PlannedVisitDate':
            case 'Installations':
            case 'Deliveries':
                if (this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'drilldown')) {
                    this.ServiceCoverFocus(data.srcElement);
                    this.navigate('ServicePlanningSummary', InternalGridSearchSalesModuleRoutes.ICABSSESERVICEPLANSUMMARYDETAILGRID);
                }
                break;
            case 'ServicePlanDescription':
                this.navigate('ServicePlanDescription', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEPLANDESCMAINTENANCE, {
                    'ServicePlanRowID': this.riGrid.Details.GetAttribute('ServicePlanDescription', 'additionalproperty')
                });
                break;
        }
    }

    public riGridAfterExecute(): void {
        let bodyArrayLen: number, colArrayLen: number;

        if (this.riGrid.bodyArray.length) {
            if (this.riGrid.Details.GetValue('Optomised')) {
                this.arrInfo = this.riGrid.bodyArray[0][2].additionalData.split('|');
                this.setControlValue('TotalNoOfCalls', this.arrInfo[0]);
                this.setControlValue('TotalNoOfExchanges', this.arrInfo[1]);
                this.setControlValue('TotalTime', this.arrInfo[2]);
                this.setControlValue('TotalWED', (this.arrInfo[4]));
                this.setControlValue('TotalWeight', this.arrInfo[5]);
                bodyArrayLen = this.riGrid.bodyArray.length - 1;
                colArrayLen = this.riGrid.colArray.length;
                //Based on syschar, NettValue column will be realigned in grid ans set to TotalNettValue
                setTimeout(() => {
                    switch (colArrayLen) {
                        case 20:
                            this.setControlValue('TotalNettValue', this.riGrid.bodyArray[bodyArrayLen][10].text);
                            break;
                        case 19:
                            this.setControlValue('TotalNettValue', this.riGrid.bodyArray[bodyArrayLen][9].text);
                            break;
                        default:
                            this.setControlValue('TotalNettValue', this.riGrid.bodyArray[bodyArrayLen][8].text);
                            break;
                    }
                }, 0);
            }
            this.disableButton();
        }
    }

    public getCurrentPage(currentPage: any): void {
        this.gridParams['pageCurrent'] = currentPage.value;
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }

    public gridRefresh(): void {
        this.riGrid.RefreshRequired();
        if (this.branchServicePlanningSummaryMode) {
            this.buildGridForBranchServicePlanSummary();
        } else {
            this.buildGrid();
        }
    }

    public endDateSelectedValue(value: any): void {
        if (value && value.value)
            this.setControlValue('EndDate', value.value);
    }

    public actionButtonClicked(action: string): void {
        switch (action) {
            case 'workList':
                if (this.pageParams.vUseServiceListing) {
                    this.navigate('ServicePlanning', AppModuleRoutes.SERVICEDELIVERY + ServiceDeliveryModuleRoutes.ICABSWORKLISTCONFIRM, {
                        'BranchNumber': this.getControlValue('BranchNumber'),
                        'DateFrom': this.getControlValue('StartDate'),
                        'DateTo': this.getControlValue('EndDate')
                    });
                } else {
                    this.navigate('ServicePlanningSummary', InternalGridSearchServiceModuleRoutes.ICABSSESERVICEWORKLISTGRID, {
                        'CurrentContractTypeURLParameter': this.riExchange.getCurrentContractType(),
                        'StartDate': this.getControlValue('StartDate'),
                        'EndDate': this.getControlValue('EndDate'),
                        'WeekNumber': this.getControlValue('WeekNumber'),
                        'BranchServiceAreaCode': this.getControlValue('BranchServiceAreaCode'),
                        'EmployeeSurname': this.getControlValue('EmployeeSurname'),
                        'ServicePlanNumber': this.getControlValue('ServicePlanNumber'),
                        'TotalNoOfCalls': this.getControlValue('TotalNoOfCalls'),
                        'TotalNoOfExchanges': this.getControlValue('TotalNoOfExchanges'),
                        'TotalTime': this.getControlValue('TotalTime'),
                        'TotalNettValue': this.getControlValue('TotalNettValue'),
                        'TotalWED': this.getControlValue('TotalWED'),
                        'SummaryDetail': 'Summary'
                    });
                }
                break;
            case 'automatic':
                this.optimisePlan();
                break;
            case 'manual':
                //to do
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotDeveloped));
                break;
            case 'undo':
                this.undoPlan();
                break;
            case 'undoProposed':
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.PageSpecificMessage.undoProposedPlan, '', this.promptUndoProposedPlan.bind(this)));
                break;
            case 'confirm':
                this.confirmPlan();
                break;
        }
    }

    public onCellBlur(data: any): void {
        this.updateGrid();
    }

    public onBrServicePlanGridSelectAll(val: boolean): void {
        const totalRows: number = this.riGrid.getAllRowsInGrid().length - 1 || 0;
        this.riGrid.selectCheckBoxInAllRows('Sel', val);

        if (val && totalRows > 0) { // select all
            this.generateBrSummaryPlanGridAllRowsData(totalRows); // generate data from all rows and make ready for API

        } else {  // select none
            this.resetBrServicePlanGridSelectedData();  // remove all data related to API
        }
        this.handleGridActionButtonsState();
    }

    public onBrServicePlanGridStockCheckSelected(): void { return; }

    public formatData(data: any, type: any): any {
        switch (type) {
            case MntConst.eTypeCode:
                data = data.toString().toUpperCase();
                break;
            case MntConst.eTypeTextFree:
                data = this.utils.toTitleCase(data.toString());
                break;
            case MntConst.eTypeText:
                data = data;
                break;
            case MntConst.eTypeInteger:
                data = this.globalize.formatIntegerToLocaleFormat(data);
                break;
            case MntConst.eTypeDecimal1:
                data = this.globalize.formatDecimalToLocaleFormat(this.utils.decimalFaultTolerance(data), 1);
                break;
            case MntConst.eTypeDecimal2:
                data = this.globalize.formatDecimalToLocaleFormat(this.utils.decimalFaultTolerance(data), 2);
                break;
            case MntConst.eTypeDecimal3:
                data = this.globalize.formatDecimalToLocaleFormat(this.utils.decimalFaultTolerance(data), 3);
                break;
            case MntConst.eTypeDecimal4:
                data = this.globalize.formatDecimalToLocaleFormat(this.utils.decimalFaultTolerance(data), 4);
                break;
            case MntConst.eTypeDecimal5:
                data = this.globalize.formatDecimalToLocaleFormat(this.utils.decimalFaultTolerance(data), 5);
                break;
            case MntConst.eTypeDecimal6:
                data = this.globalize.formatDecimalToLocaleFormat(this.utils.decimalFaultTolerance(data), 6);
                break;
            case MntConst.eTypeCurrency:
                data = this.globalize.formatCurrencyToLocaleFormat(this.utils.decimalFaultTolerance(data));
                break;
            case MntConst.eTypeTime:
                data = this.globalize.formatTimeToLocaleFormat(data);
                break;
            case MntConst.eTypeDate:
                data = this.globalize.formatDateToLocaleFormat(data);
                break;
            case MntConst.eTypeDateText:
                data = this.globalize.formatDateToLocaleFormat(data);
                break;
        }
        return data;
    }

    public checkStockBeforeConfirmPlan(): void {  //ITA-960 -[call 1]
        let searchParams: any = this.getURLSearchParamObject();

        searchParams.set('branchServiceAreaList', this.parseServiceCodeForReserveStock());
        searchParams.set('cancelReservation', 'false');
        searchParams.set('confirmReservation', 'false');
        searchParams.set('planFromDate', this.brServicePlanGridSelectedData.StartDate);
        searchParams.set('planToDate', this.brServicePlanGridSelectedData.EndDate);
        searchParams.set('branchNumber', this.brServicePlanGridSelectedData.BranchNumber);
        searchParams.set('email', this.utils.getEmail());
        searchParams.set('businessCode', this.businessCode());
        searchParams.set('countryCode', this.countryCode());

        const url = this.gcpService.getReserveStockUrl() + 'reserve/stock';
        this.rows = undefined;
        this.naverrorlist = undefined;

        this.ajaxSource.next(this.ajaxconstant.START);

        this.httpService.getData(url, searchParams)
            .subscribe((response: any) => {
                // show naverrorlist.records in table with red border
                // if response has "error" and "error" has "Error" then:-
                // show error in top bar
                // fill table with data.records, if any
                // else normal api calls for confirmation

                if (response) {
                    this.isStockAvailable = true;
                    let errorInResponse: any, responseData: any;

                    this.naverrorlist = response.hasOwnProperty('naverrorlist') ? response['naverrorlist'] : undefined; //naverrorlist.records might not available every time
                    if (response.hasOwnProperty('error')) {
                        errorInResponse = response['error'];
                    } else if (response.hasOwnProperty('Error')) {
                        errorInResponse = response['Error'];
                    }

                    if (response.hasOwnProperty('data')) { // show error table
                        if ((response['data']['records'] && response['data']['records'].length) || response['records'] && response['records'].length) {
                            responseData = response['data']['records'] || response['records'];//table data
                            this.rows = responseData;
                        }
                    }

                    if (errorInResponse) { //insufficent stock, stop here for confirm , response[data] and response[error] are mutually connected
                        this.messageType = 'error';
                        this.customAlertMsg = {
                            msg: errorInResponse['Error'],
                            timestamp: this.customAlertMsgTimeStamp
                        };
                        this.isStockAvailable = false;
                    }

                    if (responseData) {
                        this.isStockAvailable = false;
                        this.navErrorDataTable.loadTableData('', false, this.rows);
                    }

                    if (this.naverrorlist && this.naverrorlist['records'] && this.naverrorlist['records'].length) {
                        this.isStockAvailable = false;
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);

                    if (this.isStockAvailable) {
                        this.onBrServicePlanGridConfirmSelected(); // if no error proceed for call-2
                    }

                } else {
                    this.isStockAvailable = true;
                    this.rows = undefined;
                    this.naverrorlist = undefined;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.messageType = 'error';
                    this.customAlertMsg = {
                        msg: '  API Success but without reponse......', // very rare case
                        timestamp: this.customAlertMsgTimeStamp
                    };
                    this.onBrServicePlanGridConfirmSelected(); // pass if response is blank
                }

            },
                (error: any) => {
                    let errorInResponse: any, errorNoInResponse: any;
                    if (error.hasOwnProperty('error')) {
                        errorInResponse = error['error'];
                        errorNoInResponse = errorInResponse['ErrorNo'];

                    } else if (error.hasOwnProperty('Error')) {
                        errorInResponse = error['Error'];
                        errorNoInResponse = errorInResponse['ErrorNo'];
                    }

                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.rows = undefined;
                    this.naverrorlist = undefined;
                    this.isStockAvailable = false;
                    this.messageType = 'error';
                    this.customAlertMsg = {
                        msg: (errorNoInResponse || errorInResponse['Error']) ? '  Error :- ' + (errorNoInResponse) ? errorNoInResponse : ' ' + errorInResponse['Error'] : MessageConstant.Message.TomcatErrorMessage,  //TomcatErrorMessage
                        timestamp: (new Date()).getMilliseconds()
                    };
                });
    }

    public onBrServicePlanGridConfirmSelected(): void {  // [call-2]
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');

        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, this.brServicePlanGridSelectedData)
            .subscribe((data) => {

                if (data['hasError']) {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.messageType = 'error';
                    this.customAlertMsg = {
                        msg: data['ErrorMessage'] + data['fullError'],
                        timestamp: (new Date()).getMilliseconds()
                    };

                } else { // todo check if grid refresh then no need
                    const servicePlanNumberList = data.ServicePlanNumber || undefined;  //ITA-739
                    // If Service Planning happened with Over Weight, show the Warning message
                    if (data['ErrorMessage']) {
                        this.messageType = 'warning';
                        this.customAlertMsg = {
                            msg: data['ErrorMessage'],
                            timestamp: this.customAlertMsgTimeStamp
                        };
                    }
                    this.servicePlanNumberList = servicePlanNumberList;
                    this.updateNavisionOnConfirm('branchsummary', this.servicePlanNumberList); //ITA-614 > ita-664
                }

            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.messageType = 'error';
                this.customAlertMsg = {
                    msg: error['ErrorMessage'] + error.fullError,
                    timestamp: (new Date()).getMilliseconds()
                };
            }, () => { this.gridRefresh(); });
    }

    public showDialog(message: any, fncallback?: any, fnnocallback?: any): void {
        this.promptCallback = fncallback;
        this.promptNoCallback = fnnocallback;
        if (message instanceof Array) {
            this.promptContent = message;
            setTimeout(() => { this.promptModal.show(); }, 500);
        } else {
            this.getTranslatedValue(message, null).subscribe((res: string) => {
                this.promptContent = res || message;
            });
            setTimeout(() => { this.promptModal.show(); }, 500);
        }
    }

    public promptYes(event?: Event): void {
        if (this.promptCallback && typeof this.promptCallback === 'function') {
            this.promptModal.hide();
            this.promptCallback.call(this, 'branchsummary', this.servicePlanNumberList);
            this.promptNoCallback = null;
        }
    }

    public promptNo(event: Event): void {
        this.promptModal.hide();
        this.promptNoCallback = null;
        this.brServicePlanGridSelectedData['BranchServiceAreaCode'] = '';
    }
}
