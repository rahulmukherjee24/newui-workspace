import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSSNotificationGroupGrid.html'
})

export class NotificationGroupGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    private queryParams: any = {
        operation: 'System/iCABSSNotificationGroupGrid',
        module: 'rules',
        method: 'ccm/maintenance'
    };
    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'NotifyGroupCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'NotifyGroupSystemDesc', type: MntConst.eTypeTextFree, disabled: true }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSNOTIFICATIONGROUPGRID;
        this.browserTitle = this.pageTitle = 'Notification Group Details - Maintenance Grid';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // Initializes data
    private onWindowLoad(): void {
        this.setControlValue('NotifyGroupCode', this.riExchange.getParentHTMLValue('NotifyGroupCode'));
        this.setControlValue('NotifyGroupSystemDesc', this.riExchange.getParentHTMLValue('NotifyGroupSystemDesc'));
        this.buildGrid();
        if (!this.isReturning()) {
            this.pageParams.gridConfig = {
                pageSize: 10,
                currentPage: 1,
                totalRecords: 1
            };
        }
        this.populateGrid();
    }

    // Builds the structure of the grid
    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('NotifyEmployeeType', 'NotificationGroupEmployee', 'NotifyEmployeeType', MntConst.eTypeTextFree, 60);
        this.riGrid.AddColumn('NotifyType', 'NotificationGroupEmployee', 'NotifyType', MntConst.eTypeTextFree, 10);
        this.riGrid.AddColumn('NotifyContactList', 'NotificationGroupEmployee', 'NotifyContactList', MntConst.eTypeTextFree, 30);
        this.riGrid.Complete();
    }

    // Populate data into the grid
    private populateGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set('NotifyGroupCode', this.getControlValue('NotifyGroupCode'));
        // set grid building parameters
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.PageSize, this.pageParams.gridConfig.pageSize.toString());
        search.set(this.serviceConstants.PageCurrent, this.pageParams.gridConfig.currentPage.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.Action, '2');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGrid.RefreshRequired();
                    this.pageParams.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                    this.riGrid.Execute(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // Updates the pagelevel attributes on grid row activity
    private onGridFocus(): void {
        this.setAttribute('PESRezoneRowID', this.riGrid.Details.GetAttribute('NewBranchServiceAreaSeqNo', 'RowID'));
        this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'RowID'));
        this.setAttribute('Row', this.riGrid.CurrentRow);
    }

    // Refresh the grid data on user click
    public onRiGridRefresh(event: any): void {
        if (this.pageParams.gridConfig.currentPage <= 0) {
            this.pageParams.gridConfig.currentPage = 1;
        }
        if (event) {
            this.riGrid.HeaderClickedColumn = '';
        }
        this.populateGrid();
    }

    // Callback to retrieve the current page on user clicks
    public onGetCurrentPage(currentPage: any): void {
        this.pageParams.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh(null);
    }

    public onGridBodyDbClick(event: any): void {
        this.onGridFocus();
        if (this.riGrid.CurrentColumnName === 'NotifyEmployeeType') {
            this.setAttribute('NotifyEmployeeType', this.riGrid.Details.GetAttribute('NotifyEmployeeType', 'rowID'));
            this.navigate('Update', InternalMaintenanceModuleRoutes.ICABSSNOTIFICATIONGROUPEMPLOYEEMAINTENANCE);
        }
    }

    public onAddBtnClicked(event: any): void {
        this.navigate('Add', InternalMaintenanceModuleRoutes.ICABSSNOTIFICATIONGROUPEMPLOYEEMAINTENANCE);
    }
}

