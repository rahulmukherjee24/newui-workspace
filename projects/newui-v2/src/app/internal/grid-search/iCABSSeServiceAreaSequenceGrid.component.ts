import { Component, Injector, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';

import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../base/BaseComponent';
import { BranchServiceAreaSearchComponent } from '../../internal/search/iCABSBBranchServiceAreaSearch';
import { ContractSearchComponent } from '../../internal/search/iCABSAContractSearch';
import { GridAdvancedComponent } from '../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { ContractManagementModuleRoutes, InternalMaintenanceServiceModuleRoutes } from './../../base/PageRoutes';
import { DropdownStaticComponent } from '../../../shared/components/dropdown-static/dropdownstatic';
import { MessageConstant } from '../../../shared/constants/message.constant';

@Component({
    templateUrl: 'iCABSSeServiceAreaSequenceGrid.html'
})

export class ServiceAreaSequenceGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('portfolioStatus') portfolioStatus: DropdownStaticComponent;
    @ViewChild('entitlementProd') entitlementProd: DropdownStaticComponent;
    @ViewChild('groupBy') groupBy: DropdownStaticComponent;
    @ViewChild('contractType') contractType: DropdownStaticComponent;
    @ViewChild('showType') showType: DropdownStaticComponent;

    private method: string = 'service-planning/maintenance';
    private module: string = 'structure';
    private operation: string = 'Service/iCABSSeServiceAreaSequenceGrid';
    private isReturningFirstTime: boolean = false;
    private isReturningFirstTimeShowType: boolean = false;
    private isReturningFirstTimeEntitlementProd: boolean = false;
    private isReturningFirstTimePortfolioStatus: boolean = false;
    private currentContractType: string;
    private currentContractTypeActual: string;

    public ellipsis: any = {};
    public pageId: string = '';
    public totalRecords: number = 0;
    public curPage: number = 1;
    public isVisiblePagination: boolean = false;
    public isVisibleDesequenceExpired: boolean = false;
    public isVisibleReSequence: boolean = true;
    public isVisibleContractCommenceDate: boolean = false;
    public labelPostCode: string = MessageConstant.PageSpecificMessage.iCABSSeServiceAreaSequenceGrid.pageTitlePositionOnPostCode;
    public portfolioStatusOptions: Array<any> = [];
    public contractTypeOptions: Array<any> = [];
    public entitlementProdOptions: Array<any> = [];
    public groupByOptions: Array<any> = [];
    public showTypeOptions: Array<any> = [];
    public controls: any = [
        { name: 'BranchServiceAreaCode', required: true },
        { name: 'EmployeeSurname', disabled: true },
        { name: 'ContractNumber' },
        { name: 'ContractName', disabled: true },
        { name: 'SeqNumberFrom' },
        { name: 'PremisePostcode' },
        { name: 'SequenceGap' },
        { name: 'PortfolioStatusType' },
        { name: 'EntitlementProd' },
        { name: 'GroupBy' },
        { name: 'ContractTypeCode' },
        { name: 'ShowType' },
        { name: 'ContractCommenceDate', type: MntConst.eTypeDate }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEAREASEQUENCEGRID;
        this.browserTitle = 'Service Area Sequence';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageTitle = 'Service Area Sequence';
        this.getSysCharDtetails();
        this.createOptions();
        this.createContractTypeOptions();
        this.setControlValue('SequenceGap', 10);
        this.riGrid.FunctionUpdateSupport = true;
        this.ellipsis = {
            contractNumberEllipsis: {
                childparams: {
                    'parentMode': 'LookUp-All'
                },
                component: ContractSearchComponent
            },
            branchServiceAreaCode: {
                childConfigParams: {
                    parentMode: 'LookUp-Emp'
                },
                contentComponent: BranchServiceAreaSearchComponent
            }
        };
        if (this.isReturning()) {
            this.isReturningFirstTime = true;
            this.isReturningFirstTimeShowType = true;
            this.isReturningFirstTimeEntitlementProd = true;
            this.isReturningFirstTimePortfolioStatus = true;
        } else {
            this.pageParams.gridCurrentPage = 1;
        }
    }

    ngAfterViewInit(): void {
        this.isVisibleReSequence = true;
        if (this.isReturning()) {
            this.portfolioStatus.selectedItem = this.pageParams.portfolioStatusType;
            this.entitlementProd.selectedItem = this.pageParams.entitlementProd;
            this.groupBy.selectedItem = this.pageParams.groupBy;
            if (this.isVisibleDesequenceExpired) {
                this.showType.selectedItem = this.pageParams.showType;
            }
            this.groupBy.selectedItem = this.pageParams.groupBy;
            this.portfolioStatus.selectedItem = this.pageParams.portfolioStatusType;
            this.entitlementProd.selectedItem = this.pageParams.entitlementProd;
            this.showType.selectedItem = this.pageParams.showType;
        } else {
            this.portfolioStatus.selectedItem = 'Current';
            this.pageParams.portfolioStatusType = 'Current';
            this.entitlementProd.selectedItem = 'Exclude';
            this.pageParams.entitlementProd = 'Exclude';
        }
        this.setControlValue('EntitlementProd', this.pageParams.entitlementProd);
        this.setControlValue('PortfolioStatusType', this.pageParams.portfolioStatusType);
        this.setControlValue('GroupBy', this.pageParams.groupBy);
        this.setControlValue('ShowType', this.pageParams.showType);
        this.isVisibleReSequence = !(this.getControlValue('PortfolioStatusType') === 'All' || this.getControlValue('PortfolioStatusType') === 'NonCurrent');
        if (this.isReturning) {
            this.refresh();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * to create the options for the dropdowns
     * @param void
     * @return void
     */
    private createOptions(): void {
        this.portfolioStatusOptions = [
            { text: 'All', value: 'All' },
            { text: 'Current Portfolio', value: 'Current' },
            { text: 'Non Current Portfolio', value: 'NonCurrent' }
        ];
        this.entitlementProdOptions = [
            { text: 'Include', value: 'Include' },
            { text: 'Exclude', value: 'Exclude' }
        ];
        this.groupByOptions = [
            { text: 'Postcode', value: 'Postcode' },
            { text: 'Town', value: 'Town' }
        ];
        this.showTypeOptions = [
            { text: 'All', value: 'All' },
            { text: 'Expired', value: 'Expired' },
            { text: 'Unexpired', value: 'Unexpired' }
        ];
    }

    /**
     * to get the syschar records
     * @param void
     * @return void
     */
    private getSysCharDtetails(): void {
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharEnableJobSequencing
        ];
        let sysCharIP: any = {
            module: this.module,
            operation: this.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };
        this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            if (data && data.records) {
                let record = data.records;
                this.isVisibleDesequenceExpired = record[0]['Required'];
            } else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
            }
        });
    }

    /**
     * to create the options for the ContractType dropdown
     * @param void
     * @return void
     */
    private createContractTypeOptions(): void {
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams[this.serviceConstants.Function] = 'GetBusinessContractTypes';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
                (data) => {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.contractTypeOptions.push({ text: 'All', value: 'All' });
                        let contractTypeOptionsArray: Array<string> = data.BusinessContractTypes.toString().split(',');
                        let i: number = 0, newServicePlanStatusArray: any = {}, lngth: number = 0, txt: string, desc: string;
                        lngth = contractTypeOptionsArray.length;
                        for (let i = 0; i < lngth; i++) {
                            switch (contractTypeOptionsArray[i]) {
                                case 'C':
                                    this.contractTypeOptions.push({ text: 'Contracts', value: 'C' });
                                    break;
                                case 'J':
                                    this.contractTypeOptions.push({ text: 'Jobs', value: 'J' });
                                    break;
                                case 'P':
                                    this.contractTypeOptions.push({ text: 'Product Sales', value: 'P' });
                                    break;
                            }
                        }
                        if (!this.isReturning()) {
                            this.contractType.selectedItem = 'C';
                            this.setControlValue('ContractTypeCode', 'C');
                            this.pageParams.ContractTypeCode = 'C';
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        } else {
                            this.setControlValue('ContractTypeCode', this.pageParams.ContractTypeCode);
                            this.contractType.selectedItem = this.pageParams.ContractTypeCode;
                        }
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    /**
     * add the columns of the grid
     * @param void
     * @return void
     */
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'ServiceCover', 'BranchServiceAreaSeqNo', MntConst.eTypeInteger, 6, true);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentRight);
        this.riGrid.AddColumnUpdateSupport('BranchServiceAreaSeqNo', true);
        this.riGrid.AddColumn('Postcode', 'ServiceCover', 'Postcode', MntConst.eTypeCode, 30, true);
        this.riGrid.AddColumnAlign('Postcode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ContractPremise', 'ServiceCover', 'ContractPremise', MntConst.eTypeText, 16);
        this.riGrid.AddColumnAlign('ContractPremise', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseName', 'ServiceCover', 'PremiseName', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('PremiseAddressLine1', 'ServiceCover', 'PremiseAddressLine1', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseAddressLine1', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ProductCode', 'ServiceCover', 'ProductCode', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('PortfolioStatusDesc', 'ServiceVisit', 'PortfolioStatusDesc', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('PortfolioStatusDesc', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ServiceVisitFrequency', 'ServiceCover', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceQuantity', 'ServiceCover', 'ServiceQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);
        this.riGrid.AddColumnOrderable('Postcode', true);
        this.riGrid.AddColumnOrderable('ServiceVisitFrequency', true);
        this.riGrid.Complete();
        this.riGridBeforeExecute();
    }

    /**
     * This method is to fetch the grid records
     * @param void
     * @return void
     */
    private riGridBeforeExecute(): void {
        let search = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('ContractNumber', this.getControlValue('ContractNumber'));
        search.set('SeqNumberFrom', this.getControlValue('SeqNumberFrom'));
        search.set('PremisePostcode', this.getControlValue('PremisePostcode'));
        search.set('TownPostcode', this.getControlValue('GroupBy'));
        search.set('ShowType', this.getControlValue('ShowType'));
        search.set('ContractTypeCode', this.getControlValue('ContractTypeCode'));
        search.set('EntitlementProd', this.getControlValue('EntitlementProd'));
        search.set('PortfolioStatusType', this.getControlValue('PortfolioStatusType'));
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.GridCacheRefresh, 'true');
        search.set(this.serviceConstants.PageSize, '10');
        search.set(this.serviceConstants.GridPageCurrent, this.pageParams.gridCurrentPage.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.method, this.module, this.operation, search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data && data.pageData) {
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        this.isVisiblePagination = true;
                        if (!this.totalRecords)
                            this.isVisiblePagination = false;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.UpdateHeader = true;
                        if (data && data.errorMessage) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.riGrid.Execute(data);
                        }
                    }
                },
                error => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                    this.totalRecords = 0;
                    this.isVisiblePagination = false;
                });
    }

    private serviceCoverFocus(rsrcElement: any): void {
        rsrcElement.focus();
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('Postcode', 'rowid'));
        this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'rowid'));
        this.setAttribute('Row', this.riGrid.CurrentRow);
        this.currentContractTypeActual = this.riGrid.Details.GetAttribute('Postcode', 'additionalproperty');
        switch (this.currentContractTypeActual) {
            case 'C':
                this.currentContractType = '';
                break;
            case 'J':
                this.currentContractType = '<job>';
                break;
            case 'P':
                this.currentContractType = '<product>';
                break;
        }
    }

    /**
     * This method is used to update the grid with a new sequence number
     * @param newValueSeqNo to add the new sequence number in grid
     * @return void
     */
    private UpdatePremisesActivityGrid(newValueSeqNo: string): void {
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '2');
        let postParams: any = {};
        //grid atttributes
        postParams.BranchServiceAreaSeqNoRowID = this.riGrid.Details.GetAttribute('BranchServiceAreaSeqNo', 'rowid');
        postParams.BranchServiceAreaSeqNo = newValueSeqNo;
        postParams.PostcodeRowID = this.riGrid.Details.GetAttribute('Postcode', 'rowid');
        postParams.Postcode = this.riGrid.Details.GetValue('Postcode');
        postParams.ContractPremise = this.riGrid.Details.GetValue('ContractPremise');
        postParams.PremiseName = this.riGrid.Details.GetValue('PremiseName');
        postParams.PremiseAddressLine1 = this.riGrid.Details.GetValue('PremiseAddressLine1');
        postParams.ProductCodeRowID = this.riGrid.Details.GetAttribute('ProductCode', 'rowid');
        postParams.ProductCode = this.riGrid.Details.GetValue('ProductCode');
        postParams.PortfolioStatusDesc = this.riGrid.Details.GetValue('PortfolioStatusDesc');
        postParams.ServiceVisitFrequency = this.riGrid.Details.GetValue('ServiceVisitFrequency');
        postParams.ServiceQuantity = this.riGrid.Details.GetValue('ServiceQuantity');

        // page control values
        postParams.BranchNumber = this.utils.getBranchCode();
        postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        postParams.ContractNumber = this.getControlValue('ContractNumber');
        postParams.SeqNumberFrom = this.getControlValue('SeqNumberFrom');
        postParams.PremisePostcode = this.getControlValue('PremisePostcode');
        postParams.TownPostcode = this.getControlValue('GroupBy');
        postParams.ShowType = this.getControlValue('ShowType');
        postParams.ContractTypeCode = this.getControlValue('ContractTypeCode');
        postParams.EntitlementProd = this.getControlValue('EntitlementProd');
        postParams.PortfolioStatusType = this.getControlValue('PortfolioStatusType');

        postParams.riGridMode = '3';
        postParams.riGridHandle = this.utils.randomSixDigitString();
        postParams.HeaderClickedColumn = this.riGrid.HeaderClickedColumn;
        postParams.riSortOrder = this.riGrid.SortOrder;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.riGrid.Mode = MntConst.eModeNormal;
                        this.riGridBeforeExecute();
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    /**
     * This method will run after double clicking on a grid row
     * @param doubleclickevent
     * @return void
     */
    public riGridBodyOnDblClick(event: any): void {
        let columnName: string = this.riGrid.CurrentColumnName;
        this.serviceCoverFocus(event.srcElement.parentElement);
        switch (columnName) {
            case 'ProductCode':
                this.navigate('ServiceAreaSequence', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                    currentContractTypeURLParameter: this.currentContractType
                });
                break;
            case 'Postcode':
                this.navigate('ServiceAreaSequence', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    contractTypeCode: this.currentContractTypeActual
                });
                break;
            case 'BranchServiceAreaSeqNo':
                this.navigate('ServiceAreaSequence', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEAREASEQUENCEMAINTENANCE, {
                    currentContractTypeURLParameter: this.currentContractType
                });
                break;
        }
    }

    /**
     * to get the EmployeeSurname
     * @param void
     * @return void
     */
    public onChangeBranchServiceAreaCode(): void {
        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.setControlValue('EmployeeSurname', '');
            return;
        }
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams[this.serviceConstants.Function] = 'GetBranchServiceArea';
        postParams.BranchNumber = this.utils.getBranchCode();
        postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    /**
     * to get the Contract name and ContractCommenceDate
     * @param void
     * @return void
     */
    public onChangeContractNumber(): void {
        if (!this.getControlValue('ContractNumber')) {
            this.setControlValue('ContractName', '');
            return;
        }
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams[this.serviceConstants.Function] = 'GetContractName';
        postParams.ContractNumber = this.getControlValue('ContractNumber');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('ContractName', data.ContractName);
                        this.setControlValue('ContractCommenceDate', data.ContractCommenceDate);
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    /**
     * On clicking the button Desequence Expired Jobs button
     * @param void
     * @return void
     */
    public onClickDesequenceExpiredJobs(): void {
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams[this.serviceConstants.Function] = 'DesequenceExpired';
        postParams.BranchNumber = this.utils.getBranchCode();
        postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        postParams.DesequenceExpired = 'Desequence Expired Jobs';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.refresh();
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    /**
     * On clicking the button Resequence button
     * @param doubleclickevent
     * @return void
     */
    public onClickResequenceNumbers(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            let searchParams: any = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            let postParams: any = {};
            postParams[this.serviceConstants.Function] = 'ResequenceNumbers';
            postParams.BranchNumber = this.utils.getBranchCode();
            postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
            postParams.ResequenceNumbers = 'ResequenceNumbers';
            postParams.SequenceGap = this.getControlValue('SequenceGap');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.method, this.module, this.operation, searchParams, postParams)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.buildGrid();
                        }
                    },
                    (error) => {
                        this.errorService.emitError(error);
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        }
    }

    /**
     * getting the BranchServiceAreaCode and EmployeeSurname from ellipsis
     * @param void
     * @return void
     */
    public onRecieveBranchServiceAreaCode(data: any): void {
        this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
    }

    /**
     * getting the ContractNumber and ContractName from ellipsis
     * @param void
     * @return void
     */
    public onContractSearchDataReceived(data: any): void {
        this.setControlValue('ContractNumber', data.ContractNumber);
        this.setControlValue('ContractName', data.ContractName);
    }

    /**
     * after clicking on the portfolioStatus dropdown
     * @param void
     * @return void
     */
    public onPortfolioStatusChange(data: any): void {
        this.isVisibleReSequence = true;
        if (!this.isReturningFirstTimePortfolioStatus) {
            this.setControlValue('PortfolioStatusType', data);
            this.pageParams.portfolioStatusType = data;
        }
        this.isVisibleReSequence = !(this.getControlValue('PortfolioStatusType') === 'All' || this.getControlValue('PortfolioStatusType') === 'NonCurrent');
    }

    /**
     * after clicking on the entitlementProd dropdown
     * @param void
     * @return void
     */
    public entitlementProdChange(data: any): void {
        if (!this.isReturningFirstTimeEntitlementProd) {
            this.setControlValue('EntitlementProd', data);
            this.pageParams.entitlementProd = data;
        }
        this.isReturningFirstTimeEntitlementProd = false;
    }

    /**
     * after clicking on the GroupBy dropdown
     * @param void
     * @return void
     */
    public onGroupByChange(data: any): void {
        if (!this.isReturningFirstTime) {
            this.labelPostCode = MessageConstant.PageSpecificMessage.iCABSSeServiceAreaSequenceGrid.pageTitlePositionOnPostCode;
            if (data === 'Town')
                this.labelPostCode = MessageConstant.PageSpecificMessage.iCABSSeServiceAreaSequenceGrid.pageTitleTownsBeginning;
            this.pageParams.groupBy = data;
            this.setControlValue('GroupBy', this.pageParams.groupBy);
        }
        this.isReturningFirstTime = false;
    }

    /**
     * On change of the ContractType
     * @param void
     * @return void
     */
    public onContractTypeChange(data: any): void {
        this.setControlValue('ContractTypeCode', data);
        this.pageParams.ContractTypeCode = data;
    }

    /**
     * after clicking on the ShowType dropdown
     * @param void
     * @return void
     */
    public onShowTypeChange(data: any): void {
        if (!this.isReturningFirstTimeShowType) {
            this.setControlValue('ShowType', data);
            this.pageParams.showType = data;
        }
        this.isReturningFirstTimeShowType = false;
    }

    /**
     * grid refresh
     * @param void
     * @return void
     */
    public refresh(): void {
        if (this.riExchange.validateForm(this.uiForm))
            this.buildGrid();
    }

    public riGridSort(event: any): void {
        this.riGridBeforeExecute();
    }

    public getCurrentPage(currentPage: any): void {
        this.pageParams.gridCurrentPage = currentPage.value;
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateRow = true;
        this.riGrid.UpdateFooter = true;
        this.riGridBeforeExecute();
    }

    /**
     * after changing the column values
     * @param grid cell data
     * @return void
     */
    public onCellBlur(data: any): void {
        let oldValueSeqNo: string = this.riGrid.previousValues[0].value;
        let newValueSeqNo: string = this.riGrid.Details.GetValue('BranchServiceAreaSeqNo');
        if (newValueSeqNo && oldValueSeqNo !== newValueSeqNo) {
            this.UpdatePremisesActivityGrid(newValueSeqNo);
        }
    }

    public get isDisabled(): any {
        return MntConst.eModeUpdate;
    }
}
