import { Component, OnInit, OnDestroy, Injector, ViewChild, EventEmitter } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PageIdentifier } from './../../base/PageIdentifier';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceServiceModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSSeServiceVisitReleaseDetailGrid.html'
})

export class ServiceVisitReleaseDetailGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private queryParams: any = {
        module: 'release',
        method: 'bill-to-cash/grid',
        operation: 'Service/iCABSSeServiceVisitReleaseDetailGrid'
    };
    public setFocusOnBranchServiceAreaCode = new EventEmitter<boolean>();
    public pageId: string = '';
    public controls = [
        { name: 'ContractNumber', disabled: true, type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText, commonValidator: true },
        { name: 'DateFrom', required: true, disabled: true, type: MntConst.eTypeDate, commonValidator: true },
        { name: 'DateTo', required: true, disabled: true, type: MntConst.eTypeDate, commonValidator: true },
        { name: 'PremiseNumber', disabled: true, type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'PremiseName', disabled: true, type: MntConst.eTypeText, commonValidator: true },
        { name: 'ServiceAnnualValue', disabled: true, type: MntConst.eTypeCurrency, commonValidator: true },
        { name: 'ProductCode', disabled: true, type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText, commonValidator: true },
        { name: 'ServiceVisitFrequency', disabled: true, type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'VisitValue', disabled: true, type: MntConst.eTypeCurrency, commonValidator: true },
        { name: 'ServiceQuantity', disabled: true, type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'ProductivityCalculated', type: MntConst.eTypeCheckBox, commonValidator: true }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEVISITRELEASEDETAILGRID;
        this.browserTitle = this.pageTitle = 'Service Visit Detail Release';
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.buildGrid();
        } else {
            this.windowOnload();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.setCurrentContractType();

        this.riGrid.FunctionTabSupport = true;
        this.riGrid.FunctionUpdateSupport = true;

        this.riExchange.getParentHTMLValue('DateFrom');
        this.riExchange.getParentHTMLValue('DateTo');

        this.pageParams.vSCEnableDurations = true;
        this.setAttribute('ServiceCoverRowID', this.riExchange.getParentAttributeValue('ServiceCoverRowID'));

        this.populateDescriptions();
        this.buildGrid();
    }

    // set current page data
    private setCurrentContractType(): void {
        this.pageParams.currentContractType = this.riExchange.getCurrentContractType();
        this.pageParams.currentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
    }

    private populateDescriptions(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let postData: Object = {
            Function: 'SetDetailDisplayFields',
            ServiceCoverRowID: this.getAttribute('ServiceCoverRowID'),
            ContractNumber: this.getControlValue('ContractNumber'),
            ContractName: this.getControlValue('ContractName'),
            PremiseNumber: this.getControlValue('PremiseNumber'),
            PremiseName: this.getControlValue('PremiseName'),
            ProductCode: this.getControlValue('ProductCode'),
            ProductDesc: this.getControlValue('ProductDesc'),
            ServiceVisitFrequency: this.getControlValue('ServiceVisitFrequency'),
            ServiceAnnualValue: this.getControlValue('ServiceAnnualValue'),
            VisitValue: this.getControlValue('VisitValue'),
            ServiceQuantity: this.getControlValue('ServiceQuantity')
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('ContractName', data.ContractName);
                this.setControlValue('PremiseNumber', data.PremiseNumber);
                this.setControlValue('PremiseName', data.PremiseName);
                this.setControlValue('ProductCode', data.ProductCode);
                this.setControlValue('ProductDesc', data.ProductDesc);
                this.setControlValue('ServiceVisitFrequency', data.ServiceVisitFrequency);
                this.setControlValue('ServiceAnnualValue', data.ServiceAnnualValue);
                this.setControlValue('VisitValue', data.VisitValue);
                this.setControlValue('ServiceQuantity', data.ServiceQuantity);

                if (this.pageParams.vSCEnableDurations) {
                    this.pageParams.tdVisitValue = true;
                    this.pageParams.tdServiceQuantity = true;
                    this.pageParams.tdCalculate = true;
                    this.setControlValue('ProductivityCalculated', false);
                } else {
                    this.pageParams.tdVisitValue = false;
                    this.pageParams.tdServiceQuantity = true;
                    this.pageParams.tdCalculate = false;
                    this.setControlValue('ProductivityCalculated', true);
                }
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    /* ----------- Grid Routines ----------*/
    // Create grid staructure
    private buildGrid(): void {
        this.pageParams.totalRecords = 0;
        this.pageParams.pageSize = 10;
        this.pageParams.curPage = 1;
        this.pageParams.requestCacheRefresh = true;
        this.pageParams.riGridHandle = this.utils.randomSixDigitString();
        this.riGrid.Clear();

        this.riGrid.AddColumn('ServiceDateStart', 'ServiceVisit', 'ServiceDateStart', MntConst.eTypeDate, 10, true);
        this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitTypeCode', 'ServiceVisit', 'VisitTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EmployeeCode', 'ServiceVisit', 'EmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EmployeeSurname', 'ServiceVisit', 'EmployeeSurname', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);

        if (this.pageParams.vSCEnableDurations) {
            this.riGrid.AddColumn('StandardDuration', 'ServiceVisit', 'StandardDuration', MntConst.eTypeTime, 5);
            this.riGrid.AddColumnAlign('StandardDuration', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('OvertimeDuration', 'ServiceVisit', 'OvertimeDuration', MntConst.eTypeTime, 5);
            this.riGrid.AddColumnAlign('OvertimeDuration', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ProductivityValue', 'ServiceVisit', 'ProductivityValue', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumnAlign('ProductivityValue', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('ServicedQuantity', 'ServiceVisit', 'ServicedQuantity', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('ServicedQuantity', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ToBeReleased', 'ServiceVisit', 'ToBeReleased', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('ToBeReleased', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('ServiceDateStart', true);

        this.riGrid.Complete();
        this.riGridBeforeExecute();
    }

    private riGridBeforeExecute(): void {
        let gridQueryParams: QueryParams = this.getURLSearchParamObject();
        gridQueryParams.set(this.serviceConstants.Action, '2');
        gridQueryParams.set('Level', 'Detail');
        gridQueryParams.set('BranchNumber', this.utils.getBranchCode());
        gridQueryParams.set('DateFrom', this.getControlValue('DateFrom'));
        gridQueryParams.set('DateTo', this.getControlValue('DateTo'));
        gridQueryParams.set('ContractNumber', this.getControlValue('ContractNumber'));
        gridQueryParams.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        gridQueryParams.set('ProductCode', this.getControlValue('ProductCode'));
        gridQueryParams.set('ServiceCoverRowID', this.getAttribute('ServiceCoverRowID'));
        if (this.riGrid.Update) gridQueryParams.set('ROWID', this.getAttribute('ServiceVisitRowID'));
        gridQueryParams.set(this.serviceConstants.PageSize, this.pageParams.pageSize.toString());
        gridQueryParams.set(this.serviceConstants.PageCurrent, this.pageParams.curPage.toString());
        gridQueryParams.set(this.serviceConstants.GridCacheRefresh, this.pageParams.requestCacheRefresh);
        gridQueryParams.set(this.serviceConstants.GridMode, '0');
        gridQueryParams.set(this.serviceConstants.GridHandle, this.pageParams.riGridHandle);
        gridQueryParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridQueryParams.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridQueryParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    this.riGrid.ResetGrid();
                } else {
                    this.pageParams.curPage = data.pageData.pageNumber || 1;
                    this.pageParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.pageSize : 1;
                    this.riGrid.RefreshRequired();
                    if (this.riGrid.Update) {
                        this.riGrid.StartRow = this.getAttribute('Row');
                        this.riGrid.StartColumn = 0;
                        this.riGrid.RowID = this.getAttribute('PlanVisitRowID');
                        this.riGrid.UpdateHeader = false;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = false;
                    }
                    this.riGrid.Execute(data);
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public serviceVisitFocus(rsrcElement: any): void {
        rsrcElement.focus();
        this.setAttribute('Row', this.riGrid.CurrentRow);
        this.setAttribute('ServiceVisitRowID', this.riGrid.Details.GetAttribute('ServiceDateStart', 'rowid'));
        switch (this.riGrid.Details.GetAttribute('ServiceDateStart', 'additionalProperty')) {
            case 'C':
                this.pageParams.CurrentContractTypeURLParameter = '';
                break;
            case 'J':
                this.pageParams.CurrentContractTypeURLParameter = '<job>';
                break;
            case 'P':
                this.pageParams.CurrentContractTypeURLParameter = '<product>';
                break;
        }
    }

    public riGridBodyOnClick(event: Object): void {
        this.serviceVisitFocus(event['srcElement']);
        if (this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName)) {
            switch (this.riGrid.CurrentColumnName) {
                case 'ToBeReleased':
                    let search: QueryParams = this.getURLSearchParamObject();
                    search.set(this.serviceConstants.Action, '6');
                    let postData: Object = {
                        Function: 'ToBeReleased',
                        ServiceVisitRowID: this.getAttribute('ServiceVisitRowID')
                    };
                    this.ajaxSource.next(this.ajaxconstant.START);
                    this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.riGrid.Update = true;
                            this.riGridBeforeExecute();
                            if (this.pageParams.vSCEnableDurations) this.setControlValue('ProductivityCalculated', false);
                        }
                    }, (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
            }
        }
    }

    public riGridBodyOnDblClick(): void {
        this.serviceVisitFocus(event['srcElement']);
        if (this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName)) {
            switch (this.riGrid.CurrentColumnName) {
                case 'ServiceDateStart':
                    this.navigate('Release', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE, {
                        CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                        ServiceVisitRowID: this.getAttribute('ServiceVisitRowID')
                    });
                    break;
            }
        }
    }

    public getCurrentPage(event: Object): void {
        this.pageParams.curPage = event['value'];
        this.riGridBeforeExecute();
    }

    public refresh(): void {
        this.riExchange.validateForm(this.uiForm);
        if (this.pageParams.curPage <= 0) this.pageParams.curPage = 1;
        this.pageParams.requestCacheRefresh = true;
        this.riGridBeforeExecute();
    }

    public riGridSort(event: Object): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    /* --------------- Page routines -------------- */
    public cmdCalculateOnclick(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let postData: Object = {
            Function: 'CalculateProductivity',
            ServiceCoverRowID: this.getAttribute('ServiceCoverRowID'),
            BranchNumber: this.utils.getBranchCode(),
            DateFrom: this.getControlValue('DateFrom'),
            DateTo: this.getControlValue('DateTo')
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.pageParams.requestcacheRefresh = true;
                this.riGrid.Update = false;
                this.riGridBeforeExecute();
                this.setControlValue('ProductivityCalculated', true);
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    public cmdReleaseOnclick(): void {
        if (this.getControlValue('ProductivityCalculated')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let postData: Object = {
                Function: 'ReleaseServiceVisits',
                ServiceCoverRowID: this.getAttribute('ServiceCoverRowID'),
                BranchNumber: this.utils.getBranchCode(),
                DateFrom: this.getControlValue('DateFrom'),
                DateTo: this.getControlValue('DateTo')
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.pageParams.requestcacheRefresh = true;
                    this.riGrid.Update = false;
                    this.riGridBeforeExecute();
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        } else {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let postData: Object = {
                Function: 'GetErrorMessage'
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        }
    }

}
