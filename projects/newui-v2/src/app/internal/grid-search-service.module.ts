import { HttpClientModule } from '@angular/common/http';
import { PremiseSearchGridComponent } from './grid-search/iCABSAPremiseSearchGrid';
import { CustomerInformationSummaryComponent } from './grid-search/iCABSACustomerInformationSummary.component';
import { ProductSalesSCEntryGridComponent } from './grid-search/iCABSAProductSalesSCEntryGrid.component';
import { RecommendationGridComponent } from './grid-search/iCABSARecommendationGrid.component';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { VisitToleranceGridComponent } from './grid-search/iCABSSVisitToleranceGrid.component';
import { InternalGridSearchServiceModuleRoutesConstant } from '../base/PageRoutes';
import { InternalSearchModule } from './search.module';
import { InternalSearchEllipsisModule } from './search-ellipsis.module';
import { SearchEllipsisDropdownModule } from './search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from './search-ellipsis-business.module';
import { SharedModule } from './../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { Component, ViewChild, ViewContainerRef, NgModule } from '@angular/core';
import { DebriefOutstandingGridComponent } from './grid-search/iCABSSeDebriefOutstandingGrid.component';
import { CustomerSignatureDetailComponent } from './grid-search/iCABSSeCustomerSignatureDetail.component';
import { AnniversaryGenerateComponent } from './grid-search/iCABSAnniversaryGenerate';
import { BranchHolidayGridComponent } from './grid-search/iCABSBBranchHolidayGrid.component';
import { CalendarTemplateDateGridComponent } from './grid-search/iCABSACalendarTemplateDateGrid.component';
import { SeDebriefSummaryGridComponent } from './grid-search/iCABSSeDebriefSummaryGrid.component';
import { ServiceValueGridComponent } from './grid-search/iCABSAServiceValueGrid';
import { AccountAddressChangeHistoryComponent } from './grid-search/iCABSAAccountAddressChangeHistoryGrid';
import { CMGeneralSearchInfoGridComponent } from './grid-search/iCABSCMGeneralSearchInfoGrid.component';
import { FollowUpGridComponent } from './grid-search/iCABSSeFollowUpGrid.component';
import { PlanVisitGridDayBranchComponent } from './grid-search/iCABSSEPlanVisitGridDayBranch.component';
import { StaticVisitGridDayBranchComponent } from './grid-search/iCABSSeStaticVisitGridDayBranch.component';
import { ContractProductSummaryComponent } from './grid-search/iCABSAContractProductSummary.component';
import { ServicePlanningListEntryComponent } from './maintenance/iCABSSeServicePlanningListEntry.component';
import { TeamUserGridComponent } from './maintenance/TeamMaintenance/iCABSSTeamUserGrid.component';

@Component({
    template: `<router-outlet></router-outlet>
    <icabs-modal #errorModal="child" [(showHeader)]="showErrorHeader" [config]="{backdrop: 'static'}"></icabs-modal>`
})

export class InternalGridSearchServiceComponent {
    @ViewChild('errorModal') public errorModal;
    public showErrorHeader: boolean = true;
    constructor(viewContainerRef: ViewContainerRef) {
    }
}


@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        RouterModule.forChild([
            {
                path: '', component: InternalGridSearchServiceComponent, children: [
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEPLANNINGLISTENTRY, component: ServicePlanningListEntryComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSVISITTOLERANCEGRID, component: VisitToleranceGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSARECOMMENDATIONGRID.URL_1, component: RecommendationGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSARECOMMENDATIONGRID.URL_2, component: RecommendationGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSAPRODUCTSALESSCENTRYGRID.URL_1, component: ProductSalesSCEntryGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSAPRODUCTSALESSCENTRYGRID.URL_2, component: ProductSalesSCEntryGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSACUSTOMERINFORMATIONSUMMARY.URL_1, component: CustomerInformationSummaryComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSACUSTOMERINFORMATIONSUMMARY.URL_2, component: CustomerInformationSummaryComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSAPREMISESEARCHGRID.URL_1, component: PremiseSearchGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSAPREMISESEARCHGRID.URL_2, component: PremiseSearchGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSECUSTOMERSIGNATUREDETAIL, component: CustomerSignatureDetailComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSANNIVERSARYGENERATE, component: AnniversaryGenerateComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSBBRANCHHOLIDAYGRID, component: BranchHolidayGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSACALENDARTEMPLATEDATEGRID, component: CalendarTemplateDateGridComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSEDEBRIEFSUMMARYGRID, component: SeDebriefSummaryGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSASERVICEVALUEGRID, component: ServiceValueGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSEDEBRIEFOUTSTANDINGGRID, component: DebriefOutstandingGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSEFOLLOWUPGRID, component: FollowUpGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSAACCOUNTADDRESSCHANGEHISTORYGRID, component: AccountAddressChangeHistoryComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSCMGENERALSEARCHINFOGRID, component: CMGeneralSearchInfoGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSACONTRACTPRODUCTSUMMARY, component: ContractProductSummaryComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSEPLANVISITGRIDDAYBRANCH, component: PlanVisitGridDayBranchComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESTATICVISITGRIDDAYBRANCH, component: StaticVisitGridDayBranchComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSTEAMUSERGRID, component: TeamUserGridComponent }
                ]
            }
        ])
    ],

    declarations: [
        InternalGridSearchServiceComponent,
        VisitToleranceGridComponent,
        RecommendationGridComponent,
        ProductSalesSCEntryGridComponent,
        CustomerInformationSummaryComponent,
        PremiseSearchGridComponent,
        CustomerSignatureDetailComponent,
        PlanVisitGridDayBranchComponent,
        StaticVisitGridDayBranchComponent,
        AnniversaryGenerateComponent,
        FollowUpGridComponent,
        DebriefOutstandingGridComponent,
        BranchHolidayGridComponent,
        CalendarTemplateDateGridComponent,
        SeDebriefSummaryGridComponent,
        ServiceValueGridComponent,
        AccountAddressChangeHistoryComponent,
        CMGeneralSearchInfoGridComponent,
        ContractProductSummaryComponent,
        ServicePlanningListEntryComponent,
        TeamUserGridComponent
    ]
})
/*export class InternalGridSearchModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: InternalGridSearchModule,
            providers: [
                RouteAwayGuardService, RouteAwayGlobals
            ]
        };
    }
}*/

export class InternalGridSearchServiceModule { }
