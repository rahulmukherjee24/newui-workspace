import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { QueryParams } from '@shared/services/http-params-wrapper';

import { BaseComponent } from '@base/BaseComponent';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { IGenericEllipsisControl,GenericEllipsisEvent } from '@shared/components/ellipsis-generic/ellipsis-generic';
import { MessageConstant } from '@shared/constants/message.constant';
import { PageIdentifier } from '@base/PageIdentifier';
import { MntConst } from '@shared/services/riMaintenancehelper';
@Component({
    templateUrl: 'iCABSBWasteConsignmentNoteRangeTypeMaintenance.html'
})

export class WasteConsignmentNoteRangeTypelMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('messageModal') public messageModal;
    @ViewChild('promptConfirmModal') public promptConfirmModal;
    @ViewChild('promptDeleteModal') public promptDeleteModal;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private xhrParams = {
        module: 'waste',
        method: 'service-delivery/admin',
        operation: 'Business/iCABSBWasteConsignmentNoteRangeTypeMaintenance'
    };

    public controls = [
        { name: 'ConsignmentNoteRange' },
        { name: 'ConsignmentNoteRangeTypeDesc', required: true },
        { name: 'PrenotificationRange' },
        { name: 'RegulatoryAuthorityName', disabled: true },
        { name: 'RegulatoryAuthorityNumber', required: true },
        { name: 'WasteConsignmentNoteRangeType', required: true }
    ];
    public autoOpenRegulatoryAuthority: boolean = false;
    public autoOpenSearch: boolean = false;
    public disableCancel: boolean;
    public disableDelete: boolean;
    public disableSave: boolean;

    //Waste consignment Note Range Search
    public inputParamsWasteConsignmentNoteRangeSearchConfig: IGenericEllipsisControl = {
        autoOpen: false,
        ellipsisTitle: 'Waste Consignment Note Range Type Search',
        configParams: {
            table: '',
            shouldShowAdd: true,
            parentMode: 'LookUp-WCNoteRangeType',
            extraParams: {
                'search.sortby': 'WasteConsignmentNoteRangeType DESC',
                'jsonSortField': 'WasteConsignmentNoteRangeType',
                'RegulatoryAuthorityNumber' : 0
            }
        },
        httpConfig: {
            operation: 'Business/iCABSBWasteConsignmentNoteRangeTypeSearch',
            module: 'waste',
            method: 'service-delivery/search'
        },
        tableColumn: [
            { title: 'Regulatory Authority Number', name: 'RegulatoryAuthorityNumber', type: MntConst.eTypeInteger },
            { title: 'Waste Transfer Type Code', name: 'WasteConsignmentNoteRangeType', type: MntConst.eTypeCode },
            { title: 'Description', name: 'ConsignmentNoteRangeTypeDesc', type: MntConst.eTypeText },
            { title: 'Description', name: 'RegulatoryAuthorityName', type: MntConst.eTypeText }
        ],
        disable: false
    };
    public regulatoryAuthorityNumber: IGenericEllipsisControl = {
        autoOpen: true,
        ellipsisTitle: 'Regulatory Authority Search',
        configParams: {
            table: '',
            shouldShowAdd: false,
            parentMode: 'LookUp',
            extraParams: {
                'search.sortby': 'SeasonalTemplateNumber',
                'BranchNumber': this.utils.getBranchCode()
            }
        },
        httpConfig: {
            operation: 'Business/iCABSBRegulatoryAuthoritySearch',
            module: 'waste',
            method: 'service-delivery/search'
        },
        tableColumn: [
            { title: 'Regulatory Authority Number', name: 'RegulatoryAuthorityNumber' },
            { title: 'Name', name: 'RegulatoryAuthorityName' },
            { title: 'Max Weight Per Day (KG)', name: 'MaximumWeightPerDay' },
            { title: 'Requires Premises Registration', name: 'RequiresPremiseRegistrationInd' },
            { title: 'Requires Service Cover Waste', name: 'RequiresServiceCoverWasteInd' },
            { title: 'Requires Waste Transfer Notes', name: 'RequiresWasteTransferNotesInd' },
            { title: 'Waste Regulatory Carrier Name', name: 'WasteRegulatoryCarrierName' },
            { title: 'Waste Regulatory Registration Number', name: 'WasteRegulatoryRegistrationNum' }
        ],
        disable: false
    };
    public isRequesting: boolean = false;
    public pageId: string = '';
    public promptConfirmContent: string;
    public showHeader: boolean = true;
    public showMessageHeader: boolean = true;
    public storeDataTemp: any = {};
    public wasteConsignmentEllipsisFlag: boolean = false;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBWASTECONSIGNMENTNOTERANGETYPEMAINTENANCE;
        this.pageTitle = this.browserTitle = 'Waste Consignment Note Range Type Maintenance';
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.autoOpenRegulatoryAuthority = true;
        this.setInitialState();
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private setInitialState(): void {
        this.pageParams.pageMode = 'UPDATE/DELETE';
        this.disableDelete = true;
        this.disableSave = true;
        this.disableCancel = true;
        this.setControlValue('ConsignmentNoteRange', false);
        this.disableControl('ConsignmentNoteRange', true);
        this.setControlValue('PrenotificationRange', false);
        this.disableControl('PrenotificationRange', true);
        this.disableControl('ConsignmentNoteRangeTypeDesc', true);
        this.disableControl('WasteConsignmentNoteRangeType', true);
    }

    public onWasteConsignmentNoteRange(data: any): void {
        if (data[GenericEllipsisEvent.add]) {
            this.btnAddOnClick();
        }

        this.setControlValue('WasteConsignmentNoteRangeType', data.WasteConsignmentNoteRangeType);
        this.setControlValue('ConsignmentNoteRangeTypeDesc', data.ConsignmentNoteRangeTypeDesc);
        if (this.getControlValue('WasteConsignmentNoteRangeType') && this.getControlValue('RegulatoryAuthorityNumber')) {
            this.fetchDetails();
        }
    }

    private populateDescriptions(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '0');
        let postParams: any = {};
        postParams.RegulatoryAuthorityNumber = this.getControlValue('RegulatoryAuthorityNumber');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, postSearchParams, postParams).subscribe((data) => {
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.setControlValue('RegulatoryAuthorityName', data.RegulatoryAuthorityName);
                if (this.getControlValue('WasteConsignmentNoteRangeType') && this.getControlValue('RegulatoryAuthorityNumber')) {
                    this.fetchDetails();
                }
            }
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        }, (error) => {
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError || ''));
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    //Fetch details
    private fetchDetails(): void {
        let searchParams: QueryParams;
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '0');
        searchParams.set('RegulatoryAuthorityNumber', this.getControlValue('RegulatoryAuthorityNumber'));
        searchParams.set('WasteConsignmentNoteRangeType', this.getControlValue('WasteConsignmentNoteRangeType'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, searchParams).subscribe((data) => {
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('ConsignmentNoteRange', data.ConsignmentNoteRange);
                    this.setControlValue('PrenotificationRange', data.PrenotificationRange);
                    this.pageParams.ROWID = data.ttWasteConsignmentNoteRangeType;
                    this.afterFetch();
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            }, error => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private afterFetch(): void {
        this.pageParams.pageMode = 'UPDATE/DELETE';
        this.disableDelete = false;
        this.disableSave = false;
        this.disableCancel = false;
        this.disableControl('WasteConsignmentNoteRangeType', true);
        this.disableControl('ConsignmentNoteRangeTypeDesc', false);
        this.disableControl('ConsignmentNoteRange', false);
        this.disableControl('PrenotificationRange', false);
        this.setValuesInstoreDataTemp();
    }

    //Store values after fetch/update/add
    private setValuesInstoreDataTemp(): void {
        this.storeDataTemp = this.uiForm.getRawValue();
    }

    //Restore values on Cancel in update mode
    private restoreFieldsOnCancel(): void {
        for (let key in this.uiForm.controls) {
            if (key && this.uiForm.controls.hasOwnProperty(key)) {
                this.uiForm.controls[key].markAsPristine();
                this.setControlValue(key, this.storeDataTemp[key]);
            }
        }
    }

    //update record
    public updateRecord(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '2');
        let postParams: any = {};
        postParams.ROWID = this.pageParams.ROWID;
        postParams.ConsignmentNoteRangeTypeDesc = this.uiForm.controls['ConsignmentNoteRangeTypeDesc'].value;
        postParams.ConsignmentNoteRange = this.uiForm.controls['ConsignmentNoteRange'].value;
        postParams.PrenotificationRange = this.uiForm.controls['PrenotificationRange'].value;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, postSearchParams, postParams).subscribe((data) => {
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                this.setValuesInstoreDataTemp();
                this.formPristine();
            }
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        }, (error) => {
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });

    }

    //add new record
    public addRecord(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '1');
        let postParams: any = {};
        postParams.RegulatoryAuthorityNumber = this.uiForm.controls['RegulatoryAuthorityNumber'].value;
        postParams.WasteConsignmentNoteRangeType = this.uiForm.controls['WasteConsignmentNoteRangeType'].value;
        postParams.ConsignmentNoteRangeTypeDesc = this.uiForm.controls['ConsignmentNoteRangeTypeDesc'].value;
        postParams.ConsignmentNoteRange = this.uiForm.controls['WasteConsignmentNoteRangeType'].value;
        postParams.PrenotificationRange = this.uiForm.controls['PrenotificationRange'].value;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, postSearchParams, postParams).subscribe((data) => {
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.pageParams.pageMode = data.ttWasteConsignmentNoteRangeType;
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                this.afterAdd();
            }
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        }, (error) => {
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });

    }

    private afterAdd(): void {
        this.setValuesInstoreDataTemp();
        this.pageParams.pageMode = 'UPDATE/DELETE';
        this.disableControl('WasteConsignmentNoteRangeType', true);
        this.disableControl('ConsignmentNoteRangeTypeDesc', false);
        this.disableDelete = false;
        this.wasteConsignmentEllipsisFlag = false;
        this.formPristine();
    }

    //Delete record
    public deleteRecord(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '3');
        let postParams: any = {};
        postParams.ROWID = this.pageParams.ROWID;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, postSearchParams, postParams).subscribe((data) => {
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully));
                this.setInitialState();
                this.uiForm.reset();
            }
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        }, (error) => {
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });

    }

    /*Callback when add new button of ellipsis is clicked*/
    public btnAddOnClick(): void {
        this.formPristine();
        this.uiForm.reset();
        this.pageParams.pageMode = 'ADD NEW';
        this.disableDelete = true;
        this.disableSave = false;
        this.disableCancel = false;
        this.setControlValue('ConsignmentNoteRange', false);
        this.disableControl('ConsignmentNoteRange', false);
        this.setControlValue('PrenotificationRange', false);
        this.disableControl('PrenotificationRange', false);
        this.disableControl('WasteConsignmentNoteRangeType', false);
        this.disableControl('ConsignmentNoteRangeTypeDesc', false);
        this.wasteConsignmentEllipsisFlag = true;
    }

    /*Save button clicked*/
    public saveClicked(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, () => {
            this.promptConfirm('update');
        }));
    }

    /*Delete button clicked*/
    public deleteClicked(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, () => {
            this.promptConfirm('delete');
        }));
    }

    /*Cancel button clicked*/
    public cancelClicked(): void {
        if (this.pageParams.pageMode === 'UPDATE/DELETE') {
            this.restoreFieldsOnCancel();
        } else {
            this.formPristine();
            this.setInitialState();
            this.uiForm.reset();
            this.wasteConsignmentEllipsisFlag = false;
            //this.autoOpenSearch = true;
        }
    }

    /***After confirmation service call to addnew/update/delete  */
    public promptConfirm(type: any): void {
        switch (this.pageParams.pageMode) {
            case 'UPDATE/DELETE':
                if (type === 'update') {
                    this.updateRecord();
                } else {
                    this.deleteRecord();
                }
                break;
            case 'ADD NEW':
                this.addRecord();
                break;
        }
    }

    public onRegulatoryAuthorityDataReceived(data: any): void {
        this.setControlValue('RegulatoryAuthorityNumber', data['RegulatoryAuthorityNumber']);
        this.setControlValue('RegulatoryAuthorityName', data['RegulatoryAuthorityName']);

        this.inputParamsWasteConsignmentNoteRangeSearchConfig.configParams.extraParams.RegulatoryAuthorityNumber = data['RegulatoryAuthorityNumber'];
        this.onRegulatoryNumberChange();
    }

    public onRegulatoryNumberChange(): void {
        let regulatoryAuthorityNumber: string = this.getControlValue('RegulatoryAuthorityNumber');
        let lookupQuery: Array<any> = [];

        if (this.pageParams.pageMode !== 'ADD NEW') {
            this.uiForm.reset();
            this.disableDelete = true;
            this.disableSave = true;
            this.disableCancel = true;
            this.setControlValue('ConsignmentNoteRange', false);
            this.disableControl('ConsignmentNoteRange', true);
            this.setControlValue('PrenotificationRange', false);
            this.disableControl('PrenotificationRange', true);
            this.disableControl('WasteConsignmentNoteRangeType', true);
            this.disableControl('ConsignmentNoteRangeTypeDesc', true);
        }

        this.inputParamsWasteConsignmentNoteRangeSearchConfig.configParams.extraParams.RegulatoryAuthorityNumber = regulatoryAuthorityNumber;

        if (!regulatoryAuthorityNumber) {
            return;
        }

        this.setControlValue('RegulatoryAuthorityNumber', regulatoryAuthorityNumber);

        let postSearchParams: QueryParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '0');
        postSearchParams.set('RegulatoryAuthorityNumber', regulatoryAuthorityNumber);

        lookupQuery = [{
            'table': 'RegulatoryAuthority',
            'query': { 'RegulatoryAuthorityNumber': regulatoryAuthorityNumber, 'BusinessCode': this.businessCode() },
            'fields': ['RegulatoryAuthorityName']
        }];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupQuery).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data[0] && data[0].length && data[0][0]) {
                this.setControlValue('RegulatoryAuthorityName', data[0][0]['RegulatoryAuthorityName']);
                this.uiForm.controls['RegulatoryAuthorityNumber'].setErrors(null);
            } else {
                this.setControlValue('RegulatoryAuthorityName', '');
                this.uiForm.controls['RegulatoryAuthorityNumber'].setErrors({ incorrect: true });
            }
        }).catch(error => {
            this.logger.log(error);
        });
    }
}
