import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Injector, AfterContentInit } from '@angular/core';
import { AccountSearchComponent } from '../search/iCABSASAccountSearch';
import { InvoiceGroupGridComponent } from '../grid-search/iCABSAInvoiceGroupGrid';
import { FormGroup, Validators } from '@angular/forms';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { BaseComponent } from './../../base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { Subscription } from 'rxjs';
import { EllipsisComponent } from '../../../shared/components/ellipsis/ellipsis';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';

@Component({
    templateUrl: 'iCABSAInvoiceGroupPaymentMaintenance.html',
    providers: [CommonLookUpUtilsService]
})

export class InvoiceGroupPaymentMaintenanceComponent extends BaseComponent implements OnInit, AfterViewInit, AfterContentInit, OnDestroy {

    @ViewChild('messageModal') public messageModal;
    @ViewChild('errorModal') public errorModal;
    @ViewChild('promptModalForSave') public promptModalForSave;
    @ViewChild('invoiceGroupEllipsis') public invoiceGroupEllipsis: EllipsisComponent;
    @ViewChild('paymentTermEllipsis') public paymentTermEllipsis: EllipsisComponent;
    public pageId: string;
    public messageContentError: string;
    public showErrorHeader: boolean = true;
    public messageContentSave: string;
    public queryPost: QueryParams = new QueryParams();
    public errorMessage: string;
    public uiForm: FormGroup;
    public promptConfirmContent: string;
    public showMessageHeader = true;
    public showMessageHeaderSave: boolean = true;
    public showPromptCloseButtonSave: boolean = true;
    public promptTitleSave: string = '';
    public promptContentSave: string = MessageConstant.Message.ConfirmRecord;
    public showHeader = true;
    public showCloseButton = true;
    public modalConfig: Object;
    public controls = [];
    public autoOpenSearch: boolean = false;
    public lookUpSubscription: Subscription;
    public promptModalConfigSave = {
        ignoreBackdropClick: true
    };
    public cloneObj: Object = {};
    public formdata: Object = {};

    // inputParams
    public inputParams: any = {
        Operation: 'Application/iCABSAInvoiceGroupPaymentMaintenance',
        method: 'bill-to-cash/maintenance',
        module: 'payment',
        action: '',
        parentMode: '',
        businessCode: '',
        countryCode: ''
    };

    // Ellipsis Component
    public invoiceGroupGridComponent = InvoiceGroupGridComponent;
    public accountSearchComponent = AccountSearchComponent;
    public ellipsisQueryParams: any = {
        inputParamsAccountNumber: {
            parentMode: 'AccountSearch',
            showAddNewDisplay: false
        },
        inputParamsInvoiceGroupNumber: {
            parentMode: 'Lookup'
        },
        paymentTermSearch: {
            ellipsisTitle: 'Payment Term Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp-Active',
                rowmetadata: [
                    { name: 'DefaultPaymentTermInd', type: 'img' }
                ],
                extraParams: {
                    InactiveForNew: 'false'
                }
            },
            httpConfig: {
                operation: 'Business/iCABSBPaymentTermSearch',
                module: 'payment',
                method: 'bill-to-cash/search'
            },
            tableColumn: [
                { title: 'Code', name: 'PaymentTermCode' },
                { title: 'Description', name: 'PaymentTermDesc' },
                { title: 'Default For Business', name: 'DefaultPaymentTermInd' }
            ],
            disable: false
        }
    };
    public ellipsis: any = {
        account: {
            disable: false
        },
        invoice: {
            disable: true
        },
        payment: {
            disable: true
        }
    };

    public formClone: any;

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSAINVOICEGROUPPAYMENTMAINTENANCE;
        this.promptConfirmContent = MessageConstant.Message.ConfirmRecord;
        this.messageContentSave = MessageConstant.Message.SavedSuccessfully;
        this.messageContentError = MessageConstant.Message.SaveError;
    }

    public ngAfterViewInit(): void {
        // Set message call back
        this.setMessageCallback(this);
        // Set error message call back
        this.setErrorCallback(this);
    }

    public ngAfterContentInit(): void {
        this.autoOpenSearch = true;
    }

    public ngOnInit(): void {
        this.inputParams.businessCode = this.utils.getBusinessCode();
        this.inputParams.countryCode = this.utils.getCountryCode();

        // Initialize Form
        this.buildForm();
        this.disableControl('Save', true);
        this.disableControl('Cancel', true);
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public showErrorModal(data: any): void {
        this.errorModal.show({ msg: data.msg, title: 'Error' }, false);
    }

    public showMessageModal(data: any): void {
        this.messageModal.show({ msg: data.msg, title: 'Message' }, false);
    }

    private buildForm(): void {
        this.uiForm = this.formBuilder.group({
            AccountNumber: [{ value: '', disabled: false }, Validators.required],
            AccountName: [{ value: '', disabled: true }],
            InvoiceGroupNumber: [{ value: '', disabled: true }, Validators.required],
            InvoiceGroupName: [{ value: '', disabled: true }],
            PaymentTermNumber: [{ value: '', disabled: true }, Validators.required],
            PaymentTermDesc: [{ value: '', disabled: true }],
            Save: [{ disabled: true }],
            Cancel: [{ disabled: true }]
        });
    }

    //AccountNumver Received
    public onAccountNumberReceived(data: any): void {
        let accountNumber = data[this.serviceConstants.AccountNumber];
        let accountName = data['AccountName'];
        // Set Account Number
        this.setControlValue(this.serviceConstants.AccountNumber, accountNumber);
        this.setControlValue('AccountName', accountName);
        this.setControlValue('InvoiceGroupNumber', '');
        this.setControlValue('PaymentTermNumber', '');
        this.setControlValue('PaymentTermDesc', '');
        this.updateEllipsis();
    }

    private updateEllipsis(): void {
        this.disableControl('InvoiceGroupNumber', false);
        this.disableControl('PaymentTermNumber', true);
        this.ellipsis.invoice.disable = false;
        this.ellipsisQueryParams.paymentTermSearch.disable = true;
        this.formClone = this.uiForm.getRawValue();
        // Update ellipsis
        this.ellipsisQueryParams.inputParamsInvoiceGroupNumber[this.serviceConstants.AccountNumber] = this.getControlValue('AccountNumber');
        this.ellipsisQueryParams.inputParamsInvoiceGroupNumber['AccountName'] = this.getControlValue('AccountName');
        this.ellipsisQueryParams.inputParamsInvoiceGroupNumber['AccountNumberChanged'] = true;
        this.ellipsisQueryParams.inputParamsInvoiceGroupNumber['parentMode'] = 'InvoiceGroupPaymentMaintenance';
        this.invoiceGroupEllipsis.childConfigParams = this.ellipsisQueryParams.inputParamsInvoiceGroupNumber;
        this.setFormMode(this.c_s_MODE_UPDATE);
    }

    //InvoiceGroupNumber Received
    public onInvoiceGroupNumberReceived(data: any): void {
        let invoiceGroupNumber = data['Number'];
        let invoiceGroupName = data['Description'];

        // Set Invoice Group Number
        this.setControlValue('InvoiceGroupNumber', invoiceGroupNumber);
        this.setControlValue('InvoiceGroupName', invoiceGroupName);
        this.onInvoiceGroupUpdate();
    }

    private onInvoiceGroupUpdate(): void {
        this.disableControl('PaymentTermNumber', false);
        this.disableControl('Save', false);
        this.disableControl('Cancel', false);
        this.ellipsisQueryParams.paymentTermSearch.disable = false;
        this.formClone = this.uiForm.getRawValue();
        this.onPaymentTermNumberReceived();
        this.setFormMode(this.c_s_MODE_UPDATE);
    }

    //PaymentTerm Received
    public onPaymentTermReceived(data: any): void {
        let PaymentTermNumber = data['PaymentTermCode'];
        let PaymentTermDesc = data['PaymentTermDesc'];
        // Set Invoice Group Number
        this.setControlValue('PaymentTermNumber', PaymentTermNumber);
        this.setControlValue('PaymentTermDesc', PaymentTermDesc);
        this.setFormMode(this.c_s_MODE_UPDATE);

    }

    public onAccountNumberChange(): void {
        this.setControlValue('AccountName', '');
        this.setControlValue('InvoiceGroupNumber', '');
        this.setControlValue('PaymentTermNumber', '');
        this.setControlValue('PaymentTermDesc', '');
        this.disableControl('PaymentTermNumber', true);
        if (this.getControlValue('AccountNumber') !== '') {
            this.commonLookup.getAccountName(this.getControlValue('AccountNumber')).then(data => {
                if (data && data[0] && data[0].length) {
                    this.setControlValue('AccountName', data[0][0]['AccountName']);
                    this.updateEllipsis();
                } else {
                    this.setControlValue('AccountNumber', '');
                }
            }).catch(error => {
                this.showErrorModal({
                    msg: error
                });
            });
        }

        if (this.getControlValue('AccountNumber') === '') {
            this.disableControl('Save', true);
            this.disableControl('Cancel', true);
            this.setFormMode(this.c_s_MODE_SELECT);
            this.formClone = this.uiForm.getRawValue();
            this.ellipsisQueryParams.paymentTermSearch.disable = true;
            this.disableControl('InvoiceGroupNumber', true);
            this.disableControl('PaymentTermNumber', true);
            this.ellipsis.invoice.disable = true;
        }

        this.ellipsisQueryParams.inputParamsInvoiceGroupNumber[this.serviceConstants.AccountNumber] = this.getControlValue('AccountNumber');
        this.ellipsisQueryParams.inputParamsInvoiceGroupNumber['AccountName'] = this.getControlValue('AccountName');
    }

    public onInvoiceNumberChange(): void {
        this.setControlValue('InvoiceGroupName', '');
        this.setControlValue('PaymentTermNumber', '');
        this.setControlValue('PaymentTermDesc', '');
        if (this.getControlValue('InvoiceGroupNumber') !== '') {
            this.commonLookup.getInvoiceGroupName(this.getControlValue('InvoiceGroupNumber'), this.getControlValue('AccountNumber')).then(data => {
                if (data && data[0] && data[0].length) {
                    this.setControlValue('InvoiceGroupName', data[0][0]['InvoiceGroupName']);
                    this.onInvoiceGroupUpdate();
                } else {
                    this.setControlValue('InvoiceGroupNumber', '');
                }
            }).catch(error => {
                this.showErrorModal({
                    msg: error
                });
            });
        }

        if (this.getControlValue('InvoiceGroupNumber') === '') {
            this.disableControl('PaymentTermNumber', true);
            this.disableControl('Save', true);
            this.disableControl('Cancel', true);
            this.ellipsisQueryParams.paymentTermSearch.disable = true;
            this.formClone = this.uiForm.getRawValue();
        }
    }

    public onPaymentTermNumberChange(): void {
        if (this.getControlValue('PaymentTermNumber') === '') {
            this.setControlValue('PaymentTermDesc', '');
        }
    }

    // SavePrompt
    public saveInvoiceGroupData(): void {
        let lookupIP = [
            {
                'table': 'InvoiceGroup',
                'query': {
                    'AccountNumber': this.getControlValue('AccountNumber'),
                    'BusinessCode': this.utils.getBusinessCode(),
                    'InvoiceGroupNumber': this.getControlValue('InvoiceGroupNumber'),
                    'PaymentTermCode': this.getControlValue('PaymentTermNumber')
                },
                'fields': ['PaymentTermCode']
            },
            {
                'table': 'Account',
                'query': { 'AccountNumber': this.getControlValue('AccountNumber'), 'BusinessCode': this.utils.getBusinessCode() },
                'fields': ['AccountName']
            },
            {
                'table': 'PaymentTerm',
                'query': {
                    'PaymentTermCode': this.getControlValue('PaymentTermNumber'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['PaymentTermDesc']
            }
        ];
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data[2].length > 0) {
                this.promptModalForSave.show();
                this.setControlValue('PaymentTermDesc', data[2][0].PaymentTermDesc);
            } else {
                this.setControlValue('PaymentTermNumber', '');
                this.setControlValue('PaymentTermDesc', '');
                this.uiForm.controls['PaymentTermNumber'].setErrors({});
            }
        });
    }

    //Cancel
    public resetForm(): void {
        this.setControlValue('AccountNumber', this.cloneObj['AccountNumber']);
        this.setControlValue('AccountName', this.cloneObj['AccountName']);
        this.setControlValue('InvoiceGroupNumber', this.cloneObj['InvoiceGroupNumber']);
        this.setControlValue('PaymentTermNumber', this.cloneObj['PaymentTermNumber']);
        this.setControlValue('PaymentTermDesc', this.cloneObj['PaymentTermDesc']);
        this.fetchTranslationContent();
    }

    // Implementation of save logic
    public promptContentSaveData(): void {
        //let formdata: Object = {};
        this.queryPost.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        this.queryPost.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        this.queryPost.set(this.serviceConstants.Action, '2');
        this.formdata[this.serviceConstants.AccountNumber] = this.getControlValue(this.serviceConstants.AccountNumber);
        this.formdata['InvoiceGroupNumber'] = this.getControlValue('InvoiceGroupNumber');
        this.formdata['PaymentTermNumber'] = this.getControlValue('PaymentTermNumber');
        this.formdata['PaymentTermCode'] = this.getControlValue('PaymentTermNumber');
        this.cloneObj = this.formdata;
        this.cloneObj['AccountName'] = this.getControlValue('AccountName');
        this.cloneObj['PaymentTermDesc'] = this.getControlValue('PaymentTermDesc');
        this.inputParams.search = this.queryPost;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module, this.inputParams.Operation, this.queryPost, this.formdata)
            .subscribe(
                (data) => {
                    if (data.status === 'failure') {
                        this.showErrorModal(data.oResponse);
                    } else {
                        if (data.errorMessage && data.errorMessage !== '') {
                            this.showErrorModal({
                                msg: data.errorMessage
                            });
                        } else {
                            this.showMessageModal({
                                msg: MessageConstant.Message.RecordSavedSuccessfully
                            });
                            this.setControlValue('AccountNumber', this.formdata['AccountNumber']);
                            this.setControlValue('AccountName', this.getControlValue('AccountName'));
                            this.setControlValue('InvoiceGroupNumber', this.formdata['InvoiceGroupNumber']);
                            this.setControlValue('PaymentTermNumber', this.formdata['PaymentTermNumber']);
                            this.setControlValue('PaymentTermDesc', this.getControlValue('PaymentTermDesc'));
                        }
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.errorMessage = error as any;
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });

        this.disableControl('InvoiceGroupNumber', false);
        this.disableControl('AccountNumber', false);
    }

    public onPaymentTermNumberReceived(): void {
        if (this.getControlValue('AccountNumber') && this.getControlValue('InvoiceGroupNumber')) {
            this.doLookupforPaymentTermNumber();
        }

    }

    public doLookupforPaymentTermNumber(): void {
        let lookupIP = [
            {
                'table': 'InvoiceGroup',
                'query': {
                    'AccountNumber': this.getControlValue('AccountNumber'),
                    'BusinessCode': this.utils.getBusinessCode(),
                    'InvoiceGroupNumber': this.getControlValue('InvoiceGroupNumber')
                },
                'fields': ['PaymentTermCode']
            }
        ];
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            let PaymentTermData = data[0][0];
            this.setControlValue('PaymentTermNumber', PaymentTermData.PaymentTermCode);
            this.onPaymentTermDescReceived();
            this.disableControl('Save', false);
            this.disableControl('Cancel', false);
            this.formClone = this.uiForm.getRawValue();

        });
    }

    public onPaymentTermDescReceived(): void {
        if (this.getControlValue('PaymentTermNumber')) {
            this.doLookupforPaymentTermDesc();
        }
    }

    public doLookupforPaymentTermDesc(): void {
        let lookupIP = [
            {
                'table': 'PaymentTerm',
                'query': {
                    'PaymentTermCode': this.getControlValue('PaymentTermNumber'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['PaymentTermDesc']
            }
        ];
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            let PaymentTermData = data[0][0];
            this.setControlValue('PaymentTermDesc', PaymentTermData.PaymentTermDesc);
            this.formClone = this.uiForm.getRawValue();
        });
    }

    private fetchTranslationContent(): void {
        this.getTranslatedValue('Save', null).subscribe((res: string) => {
            if (res) {
                this.setControlValue('Save', res);
            }
        });
        this.getTranslatedValue('Cancel', null).subscribe((res: string) => {
            if (res) {
                this.setControlValue('Cancel', res);
            }
        });
    }

}
