import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QueryParams } from '../../../shared/services/http-params-wrapper';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/takeWhile';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { AjaxConstant } from '../../../shared/constants/AjaxConstants';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { ProductSearchGridComponent } from '../../../app/internal/search/iCABSBProductSearch';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { ServiceTypeSearchComponent } from '../../../app/internal/search/iCABSBServiceTypeSearch.component';
import { BusinessOriginLangSearchComponent } from '../../../app/internal/search/iCABSBBusinessOriginLanguageSearch.component';
import { IGenericEllipsisControl } from '@shared/components/ellipsis-generic/ellipsis-generic';
import { InternalMaintenanceServiceModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSCMNatAxJobServiceCoverMaintenance.html'
})

export class CMNatAxJobServiceCoverMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('serviceTypeCodeDropDown') public serviceTypeCodeDropDown: ServiceTypeSearchComponent;
    @ViewChild('businessOriginDropDown') public businessOriginDropDown: BusinessOriginLangSearchComponent;

    private rowId: string = '';
    private routeParams: any;
    private sub: Subscription;
    private isAlive: boolean = true;
    private isDirty: boolean = false;

    public editMode: string = 'UPDATE';
    public serviceTypeCodeSelected: Object = {
        id: '',
        text: ''
    };
    public pageId: string = '';
    public controls: any = [
        { name: 'ProspectNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'ProductCode', readonly: true, disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'ProductDesc', readonly: true, disabled: true, required: false, type: MntConst.eTypeTextFree },
        { name: 'Quantity', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'AnnualValue', readonly: true, disabled: true, required: true, type: MntConst.eTypeDecimal2 },
        { name: 'VisitFrequency', readonly: true, disabled: true, required: true, type: MntConst.eTypeInteger },
        { name: 'ServiceTypeCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'TaxCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BusinessOriginCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'TaxCodeDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BusinessOriginCodeDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode }
    ];
    public branchItemsToDisplay: Array<any> = ['item', 'desc'];
    public serviceTypeList: Array<any> = [];
    public vatCodeList: Array<any> = [];
    public businessOriginList: Array<any> = [];
    public search: QueryParams = new QueryParams();
    public headerParams: any = {
        method: 'prospect-to-contract/maintenance',
        operation: 'ContactManagement/iCABSCMNatAxJobServiceCoverMaintenance',
        module: 'natax'
    };
    public isAddUpdateDeleteVisible: boolean = true;
    public isSaveCancelVisible: boolean = false;
    public uiElement: any;
    public postSearchParams: QueryParams = new QueryParams();
    public promptConfirmTitle: string;
    public promptConfirmContent: string;
    public isDeleteButtonDisabled: boolean = true;
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public taxCodeSelected: Object = {
        id: '',
        text: ''
    };
    public businessOriginCodeSelected: Object = {
        id: '',
        text: ''
    };
    public isServiceTypeCodeDisabled: boolean = false;
    public isTaxCodeDisabled: boolean = false;
    public isBusinessOriginCodeDisabled: boolean = false;
    public productSearchGridComponent = ProductSearchGridComponent;
    public inputParams: any = {
        'parentMode': 'LookUp'
    };
    public TaxCodeSearchConfig: IGenericEllipsisControl = {
        autoOpen: false,
        ellipsisTitle: 'Tax Code Search',
        configParams: {
            table: '',
            shouldShowAdd: false
        },
        httpConfig: {
            operation: 'System/iCABSSTaxCodeSearch',
            module: 'tax',
            method: 'bill-to-cash/search'
        },
        tableColumn: [
            { title: 'Tax Code', name: 'TaxCode', type: MntConst.eTypeCode },
            { title: 'Description', name: 'TaxCodeDesc', type: MntConst.eTypeText },
            { title: 'System Default', name: 'TaxCodeDefaultInd', type: MntConst.eTypeCheckBox }
        ],
        disable: false
    };
    public dropdown: any = {
        serviceTypeSearch: {
            isRequired: true,
            isDisabled: false,
            params: {
                parentMode: 'LookUp'
            }
        },
        businessOriginLang: {
            triggerValidate: false
        }
    };
    public isProductCodeDisabled: boolean = false;
    public isOptionDisabled: boolean = true;
    public isSaveDisabled: boolean = false;
    public iscancelDisabled: boolean = false;
    public isDeleteDisabled: boolean = false;

    constructor(injector: Injector, public router: Router, private route: ActivatedRoute, private elem: ElementRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSCMNATAXJOBSERVICECOVERMAINTENANCE;
        this.browserTitle = this.pageTitle = 'National Account Job Service Cover Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.sub = this.route.queryParams.subscribe(
            (params: any) => {
                this.routeParams = params;
            }
        );
        this.windowOnLoad();
        this.setControlValue('ProspectNumber', this.riExchange.getParentHTMLValue('ProspectNumber'));
        this.parentMode = this.riExchange.getParentMode();
        if (this.parentMode === 'Search' || this.pageParams['serviceGroupDetail'] === 'saved') {
            this.updateClicked();
        } else {
            this.addClicked();
        }
    }

    ngAfterViewInit(): void {
        this.uiElement = this.riExchange.riInputElement;
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        //Release memory
        this.serviceConstants = null;
        this.httpService = null;
        this.errorService = null;
        this.utils = null;
        this.ajaxSource = null;
        //Unsubscribe all subscription
        this.isAlive = false;
    }

    private windowOnLoad(): void {
        if (this.pageParams['serviceGroupDetail'] === 'saved') {
            this.rowId = this.pageParams['rowId'];
        } else {
            this.rowId = this.riExchange.getParentHTMLValue('ProspectROWID');
        }
        if (this.parentMode === 'Search') {
            this.setAttribute('NatAxJobServiceCoverRowID', this.riExchange.getParentAttributeValue('NatAxJobServiceCoverRowID') || this.riExchange.getParentHTMLValue('NatAxJobServiceCoverRowID'));
        }
    }

    private setUIFieldsEditablity(editability: string): void {
        this.riExchange.riInputElement[editability](this.uiForm, 'VisitFrequency');
        this.riExchange.riInputElement[editability](this.uiForm, 'Quantity');
        this.riExchange.riInputElement[editability](this.uiForm, 'AnnualValue');
        this.riExchange.riInputElement[editability](this.uiForm, 'VisitFrequency');
        this.riExchange.riInputElement[editability](this.uiForm, 'ServiceTypeCode');
        this.riExchange.riInputElement[editability](this.uiForm, 'TaxCode');
        this.riExchange.riInputElement[editability](this.uiForm, 'BusinessOriginCode');
        this.isServiceTypeCodeDisabled = (editability === 'Disable') ? true : false;
        this.isTaxCodeDisabled = (editability === 'Disable') ? true : false;
        this.isBusinessOriginCodeDisabled = (editability === 'Disable') ? true : false;
        this.riExchange.riInputElement[editability](this.uiForm, 'ProductCode');
    }

    private setFieldsValue(data: any, isAddEditMode?: boolean): void {
        if (!isAddEditMode) {
            this.setControlValue('ProductCode', data['ProductCode']);
            this.setControlValue('ProductDesc', '');
        }
        this.setControlValue('VisitFrequency', data['VisitFrequency']);
        this.setControlValue('Quantity', data['Quantity']);
        this.setControlValue('AnnualValue', data['AnnualValue']);
        this.setControlValue('ServiceTypeCode', data['ServiceTypeCode']);
        this.setControlValue('TaxCode', data['TaxCode']);
        this.setControlValue('TaxCodeDesc', '');
        this.setControlValue('BusinessOriginCode', data['BusinessOriginDesc']);
    }

    private fetchOtherDetails(detailObj: any): void {
        let data: any = [{
            'table': 'Product',
            'query': { 'ProductCode': detailObj.ProductCode, 'BusinessCode': detailObj.BusinessCode },
            'fields': ['ProductDesc']
        },
        {
            'table': 'ServiceType',
            'query': { 'ServiceTypeCode': detailObj.ServiceTypeCode, 'BusinessCode': detailObj.BusinessCode },
            'fields': ['ServiceTypeDesc']
        },
        {
            'table': 'TaxCode',
            'query': { 'TaxCode': detailObj.TaxCode },
            'fields': ['TaxCodeDesc']
        },
        {
            'table': 'BusinessOriginLang',
            'query':

            { 'BusinessOriginCode': detailObj.BusinessOriginCode, 'LanguageCode': 'ENG', 'BusinessCode': detailObj.BusinessCode },
            'fields': ['BusinessOriginDesc']
        }
        ];
        this.lookUpRecord(data, 100).then(
            (e) => {
                let arr: any = [];
                let vatCodeArr: any = [];
                let businessOriginArr: any = [];
                let none: any = {
                    item: 'All',
                    desc: 'All'
                };
                let product: any = e['results'][0][0];
                let serviceType: any = e['results'][1][0];
                let taxCode: any = e['results'][2][0];
                let BusinessOrigin: any = e['results'][3][0];
                let serviceTypeDesc: string = '', taxCodeDesc: string = '', businessOriginDesc: string = '', productDesc: string = '';
                arr.push(none);
                if (serviceType !== null) {
                    serviceTypeDesc = serviceType.ServiceTypeDesc;
                }
                if (taxCode !== null) {
                    taxCodeDesc = taxCode.TaxCodeDesc;
                }
                if (BusinessOrigin !== null) {
                    businessOriginDesc = BusinessOrigin.BusinessOriginDesc;
                }
                if (product !== null) {
                    productDesc = product.ProductDesc;
                }
                let obj: any = {
                    item: detailObj.ServiceTypeCode,
                    desc: serviceTypeDesc
                };
                let resObj: any = JSON.parse(JSON.stringify(obj));
                if (resObj !== null) {
                    arr.push(resObj);
                }
                this.serviceTypeList = arr;
                this.serviceTypeCodeSelected = {
                    id: detailObj.ServiceTypeCode,
                    text: detailObj.ServiceTypeCode + ' - ' + serviceTypeDesc
                };

                vatCodeArr.push(none);
                let vatCodeobj: any = {
                    item: detailObj.TaxCode,
                    desc: taxCodeDesc
                };
                let resObj1: any = JSON.parse(JSON.stringify(vatCodeobj));
                if (resObj1 !== null) {
                    arr.push(resObj1);
                }
                vatCodeArr.push(resObj1);
                this.vatCodeList = vatCodeArr;
                this.taxCodeSelected = {
                    id: '',
                    text: detailObj.TaxCode + ' - ' + taxCodeDesc
                };

                businessOriginArr.push(none);
                let businessOriginObj: any = {
                    item: detailObj.BusinessOriginCode,
                    desc: businessOriginDesc
                };
                let resObj2: any = JSON.parse(JSON.stringify(businessOriginObj));
                if (resObj2 !== null) {
                    businessOriginArr.push(resObj2);
                }
                this.businessOriginList = businessOriginArr;
                this.businessOriginCodeSelected = {
                    id: detailObj.BusinessOriginCode,
                    text: detailObj.BusinessOriginCode + ' - ' + businessOriginDesc
                };
                this.setControlValue('ProductDesc', productDesc);
                this.setControlValue('ServiceTypeCode', detailObj.ServiceTypeCode);
                this.setControlValue('TaxCode', detailObj.TaxCode);
                this.setControlValue('BusinessOriginCode', detailObj.BusinessOriginCode);
                this.setControlValue('TaxCodeDesc', this.taxCodeSelected['text']);
                this.setControlValue('BusinessOriginCodeDesc', this.businessOriginCodeSelected['text']);
                this.businessOriginDropDown.active = this.businessOriginCodeSelected;
                this.serviceTypeCodeDropDown.active = this.serviceTypeCodeSelected;
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    private lookUpRecord(data: any, maxresults: number): any {
        let queryLookUp: QueryParams = this.getURLSearchParamObject();
        queryLookUp.set(this.serviceConstants.Action, '5');
        queryLookUp.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        queryLookUp.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (maxresults) {
            queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        }
        return this.httpService.lookUpPromise(queryLookUp, data);
    }

    public fetchJobServiceCoverMaintenanceData(): void {
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '0');
        this.search.set('ROWID', this.rowId);
        this.ajaxSource.next(AjaxConstant.START);

        this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module,
            this.headerParams.operation, this.search).takeWhile(() => this.isAlive)
            .subscribe(
            (e) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.setFieldsValue(e);
                    let otherDtailsObj: any = {
                        ServiceTypeCode: e['ServiceTypeCode'],
                        TaxCode: e['TaxCode'],
                        BusinessOriginCode: e['BusinessOriginCode'],
                        BusinessCode: e['BusinessCode'],
                        ProductCode: e['ProductCode'],
                        ProductDesc: ''
                    };
                    this.fetchOtherDetails(otherDtailsObj);
                }

            },
            (error) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onUpdateClicked(): void {
        this.editMode = 'UPDATE';
        this.isAddUpdateDeleteVisible = false;
        this.isSaveCancelVisible = true;
        this.isProductCodeDisabled = true;
        this.setUIFieldsEditablity('Enable');
        this.riExchange.riInputElement.Disable(this.uiForm, 'ProductCode');
        this.dropdown.serviceTypeSearch.isDisabled = false;
        this.isOptionDisabled = false;
        this.fetchJobServiceCoverMaintenanceData();
        this.isSaveDisabled = false;
        this.iscancelDisabled = false;
        this.isDeleteDisabled = false;
    }

    public addClicked(): void {
        this.beforeAdd();
        this.editMode = 'ADD';
        this.isAddUpdateDeleteVisible = false;
        this.isSaveCancelVisible = true;
        this.isProductCodeDisabled = false;
        this.setUIFieldsEditablity('Enable');
        let clearFieldObj: any = {
            ProspectNumber: '',
            ProductCode: '',
            VisitFrequency: '',
            Quantity: '',
            AnnualValue: '',
            ProductDesc: ''
        };
        this.riExchange.riInputElement.Enable(this.uiForm, 'ProductCode');
        this.setFieldsValue(clearFieldObj, true);
        this.dropdown.serviceTypeSearch.isDisabled = false;
        this.isOptionDisabled = true;
        let clearData: any = {
            id: '',
            text: ''
        };
        this.businessOriginDropDown.active = clearData;
        this.serviceTypeCodeDropDown.active = clearData;
        this.isSaveDisabled = false;
        this.iscancelDisabled = false;
        this.isDeleteDisabled = false;
    }

    public updateClicked(): void {
        if (this.parentMode === 'Prospect' && this.editMode === 'view') {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.NoRecordSelected));
        } else {
            this.editMode = 'UPDATE';
            this.onUpdateClicked();
        }
    }

    public saveRecord(): void {
        if (!this.uiForm.invalid) {
            switch (this.editMode) {
                case 'UPDATE':
                    this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.updateRecord.bind(this)));
                    break;
                case 'ADD':
                    this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.addNewRecord.bind(this)));
                    break;

            }
        }
    }

    public addNewRecord(): void {
        let formData: Object = {};
        formData['ProspectNumber'] = this.getControlValue('ProspectNumber');
        formData['ProductCode'] = this.getControlValue('ProductCode');
        formData['VisitFrequency'] = this.getControlValue('VisitFrequency');
        formData['Quantity'] = this.getControlValue('Quantity');
        formData['AnnualValue'] = this.getControlValue('AnnualValue');
        formData['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
        formData['TaxCode'] = this.getControlValue('TaxCode');
        formData['BusinessOriginCode'] = this.getControlValue('BusinessOriginCode');
        this.postSearchParams.set(this.serviceConstants.Action, '1');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());


        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                }
                else {
                    if (e.NatAxJobServiceCover) {
                        this.setAttribute('NatAxJobServiceCoverRowID', e.NatAxJobServiceCover);
                    }
                    this.pageParams['rowId'] = e['NatAxJobServiceCover'];
                    this.pageParams['serviceGroupDetail'] = 'saved';
                    this.isDirty = false;
                    this.editMode = 'UPDATE';
                    this.riExchange.riInputElement.Disable(this.uiForm, 'ProductCode');
                    this.isProductCodeDisabled = true;
                    this.dropdown.serviceTypeSearch.isDisabled = false;
                    if (e.DetailRequiredInd && e.DetailRequiredInd === 'yes') {
                        this.navigate('NatAxJobServiceCover', InternalMaintenanceServiceModuleRoutes.ICABSCMNATAXJOBSERVICEDETAILGROUPMAINTENANCE, {
                            NatAxJobServiceCoverRowID: this.getAttribute('NatAxJobServiceCoverRowID')
                        });
                    } else {
                        this.ngOnInit();
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public updateRecord(): void {
        let formData: Object = {};
        formData['TABLE'] = 'NatAxJobServiceCover';
        formData['ROWID'] = this.rowId;
        formData['VisitFrequency'] = this.getControlValue('VisitFrequency');
        formData['Quantity'] = this.getControlValue('Quantity');
        formData['AnnualValue'] = this.getControlValue('AnnualValue');
        formData['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
        formData['TaxCode'] = this.getControlValue('TaxCode');
        formData['BusinessOriginCode'] = this.getControlValue('BusinessOriginCode');

        this.postSearchParams.set(this.serviceConstants.Action, '2');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());


        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                    this.isSaveCancelVisible = false;
                    this.isAddUpdateDeleteVisible = true;
                    this.dropdown.serviceTypeSearch.isDisabled = false;
                    this.isDirty = false;
                    this.fetchJobServiceCoverMaintenanceData();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public deleteRecord(): void {
        let formData: Object = {};
        formData['TABLE'] = 'NatAxJobServiceCover';
        formData['ROWID'] = this.rowId;
        this.postSearchParams.set(this.serviceConstants.Action, '3');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());


        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully));
                    this.addClicked();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public beforeAdd(): void {
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '6');
        this.search.set('Function', 'GetTaxCode');
        this.ajaxSource.next(AjaxConstant.START);
        this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module,
            this.headerParams.operation, this.search)
            .takeWhile(() => this.isAlive).subscribe(
            (e) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    if (e['TaxCode'] && e['TaxCodeDesc']) {
                        this.setControlValue('TaxCode', e['TaxCode']);
                        this.setControlValue('TaxCodeDesc', e['TaxCodeDesc']);
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public cancelClicked(): void {
        this.isAddUpdateDeleteVisible = true;
        this.isSaveCancelVisible = false;
        this.isProductCodeDisabled = true;
        this.elem.nativeElement.querySelector('form').classList.remove('ng-dirty');
        if (this.isDirty) {
            if (this.editMode !== 'ADD') {
                this.updateClicked();
            } else {
                this.addClicked();
            }
            this.isDirty = false;
        }
    }

    public deleteClicked(): void {
        if (this.editMode === 'view') {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.NoRecordSelected));
        } else {
            this.editMode = 'DELETE';
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteRecord.bind(this)));
        }
    }

    public onServiceTypeSelected(event: any): void {
        this.setControlValue('ServiceTypeCode', event.value.item);
    }

    public onTaxCoceSelected(event: any): void {
        this.setControlValue('TaxCode', event.value.item);
    }

    public onBusinessOriginSelected(event: any): void {
        this.setControlValue('BusinessOriginCode', event.value.item);
    }

    public optionsChange(value: any): void {
        if (value === 'NatAxJobServiceDetail') {
            this.navigate('NatAxJobServiceCover', InternalMaintenanceServiceModuleRoutes.ICABSCMNATAXJOBSERVICEDETAILGROUPMAINTENANCE, {
                NatAxJobServiceCoverRowID: this.getAttribute('NatAxJobServiceCoverRowID')
            });
        }
    }

    public onProductSelectFromEllipsis(event: any, route: any): void {
        this.setControlValue('ProductCode', event.ProductCode);
        this.setControlValue('ProductDesc', event.ProductDesc);
        this.isDirty = true;
        this.markFormAsDirty();
    }

    public onBusinessOriginFromEllipsis(event: any): void {
        this.setControlValue('BusinessOriginCode', event['BusinessOriginLang.BusinessOriginCode']);
        this.setControlValue('BusinessOriginCodeDesc', event['BusinessOriginLang.BusinessOriginDesc']);
        this.isDirty = true;
        this.markFormAsDirty();
    }

    public onTaxCodeFromEllipsis(event: any): void {
        this.setControlValue('TaxCode', event.TaxCode);
        this.setControlValue('TaxCodeDesc', event.TaxCodeDesc);
        this.isDirty = true;
        this.markFormAsDirty();

    }

    public canDeactivate(): Observable<boolean> {
        if (this.pageParams['serviceGroupDetail'] === 'saved') {
            this.routeAwayGlobals.setDirtyFlag(false);
        } else {
            this.routeAwayGlobals.setDirtyFlag(this.isDirty);
        }
        return this.routeAwayComponent.canDeactivate();
    }

    public onServiceTypeDataReceived(data: any): void {
        if (data && data['ServiceTypeCode']) {
            this.setControlValue('ServiceTypeCode', data['ServiceTypeCode']);
            this.isDirty = true;
        }
        else {
            this.setControlValue('ServiceTypeCode', '');
        }
    }

    public dropDownMarkError(fieldName: string): boolean {
        return (this.uiForm.controls[fieldName].invalid && this.uiForm.controls[fieldName].touched);
    }

    public markFormAsDirty(): void {
        this.elem.nativeElement.querySelector('form').classList.add('ng-dirty');
        this.isDirty = true;
    }

}
