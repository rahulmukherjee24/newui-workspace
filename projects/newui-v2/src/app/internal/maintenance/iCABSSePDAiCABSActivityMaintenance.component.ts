import { Component, Injector, OnInit, OnDestroy, AfterViewInit, ViewChild, EventEmitter, Renderer } from '@angular/core';
import { QueryParams } from '../../../shared/services/http-params-wrapper';

import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../base/BaseComponent';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { RouteAwayComponent } from './../../../shared/components/route-away/route-away';
import { EllipsisComponent } from './../../../shared/components/ellipsis/ellipsis';
import { ProductSearchGridComponent } from './../search/iCABSBProductSearch';
import { ServiceCoverSearchComponent } from './../search/iCABSAServiceCoverSearch';
import { PremiseSearchComponent } from './../search/iCABSAPremiseSearch';
import { ContractSearchComponent } from './../search/iCABSAContractSearch';
import { PlanVisitSearchComponent } from './../search/iCABSSePlanVisitSearch.component';
import { EmployeeSearchComponent } from './../search/iCABSBEmployeeSearch';
import { SystemPDAActivityTypeLanguageSearchComponent } from './../search/iCABSSSystemPDAActivityTypeLanguageSearch.component';
import { InternalGridSearchServiceModuleRoutes, InternalMaintenanceSalesModuleRoutes, InternalSearchModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSSePDAiCABSActivityMaintenance.html'
})

export class SePDAActivityMaintenanceComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('VisitTypeSearch') public visitTypeSearch;
    @ViewChild('SecondTabPane') public secondTabPane;
    @ViewChild('TabContent') public tabContent;
    @ViewChild('NavTabs') public navTabs;
    @ViewChild('Save') public save;
    @ViewChild('serviceCoverSearch') public serviceCoverSearch: EllipsisComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;

    private isFetchSCNumber: boolean = false;
    private outCardTypeCode: number = 5;
    private currentTab: number = 0;
    private tabLength: number = 2;
    private isRecordFound: boolean = false;
    private isBtnCreateReplacement: boolean = false;
    private dummyProductCodes: any;
    private fieldValues: Object = {};
    private headerParams: any = {
        method: 'service-delivery/maintenance',
        module: 'pda',
        operation: 'Service/iCABSSePDAiCABSActivityMaintenance'
    };

    public currentMode: string = MntConst.eModeNormal;
    public lblKeyedStartTime: string = 'Keyed Start Time';
    public selectedMenu: string = '';

    public printedNameSrc: string;
    public signatureSrc: string;

    public menuArray: Array<any> = [{ value: '', text: 'Options' }];
    public statusArray: Array<any> = [];
    public pageId: string = '';

    public setEmployeeFocus = new EventEmitter<boolean>();
    public setCompDescFocus = new EventEmitter<boolean>();

    public uiDisplay: any = {
        tab: {
            tab1: { visible: true, active: true },
            tab2: { visible: true, active: false },
            tab3: { visible: this.pageParams.isOutcardTabVisible, active: false }
        }
    };

    public ellipsisConfig: any = {
        employee: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'showAddNew': false
            },
            component: EmployeeSearchComponent
        },
        activityType: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'showAddNew': false
            },
            component: SystemPDAActivityTypeLanguageSearchComponent
        },
        planVisit: {
            childConfigParams: {
                'parentMode': 'LookUp-VisitRejections',
                'showAddNew': false
            },
            component: PlanVisitSearchComponent
        },
        contract: {
            childConfigParams: {
                'parentMode': 'LookUp-All',
                'showAddNew': false,
                'currentContractType': this.riExchange.getCurrentContractType()
            },
            component: ContractSearchComponent
        },
        premise: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'ContractNumber': '',
                'ContractName': '',
                'showAddNew': false,
                'currentContractType': this.riExchange.getCurrentContractType()
            },
            component: PremiseSearchComponent
        },
        product: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'currentContractType': this.riExchange.getCurrentContractType()
            },
            component: ServiceCoverSearchComponent
        },
        productComponent: {
            childConfigParams: {
                'parentMode': 'ComponentReplacement',
                'SelComponentTypeCode': ''
            },
            component: ProductSearchGridComponent
        },
        alternateProduct: {
            childConfigParams: {
                'parentMode': 'AlternateComponentReplacement'
            },
            component: ProductSearchGridComponent
        }
    };

    public dropdownConfig: any = {
        visitType: {
            isRequired: false,
            triggerValidate: false,
            active: { id: '', text: '' },
            arrData: [],
            inputParams: {
                parentMode: 'LookUp'
            }
        },
        manualVisitReason: {
            isRequired: false,
            isDisabled: false,
            active: { id: '', text: '' },
            params: {
                method: 'service-delivery/search',
                module: 'manual-service',
                operation: 'Business/iCABSBManualVisitReasonSearch'
            },
            displayFields: ['ManualVisitReasonCode', 'ManualVisitReasonDesc']
        },
        noSignatureManual: {
            isRequired: false,
            isDisabled: false,
            active: { id: '', text: '' },
            params: {
                module: 'service',
                method: 'service-delivery/search',
                operation: 'Business/iCABSBNoSignatureReasonSearch'
            },
            displayFields: ['ReasonNumber', 'ReasonDesc']
        },
        noServiceManual: {
            isRequired: false,
            isDisabled: false,
            active: { id: '', text: '' },
            params: {
                method: 'service-delivery/search',
                module: 'service',
                operation: 'Business/iCABSBNoServiceReasonSearch'
            },
            displayFields: ['NoServiceReasonCode', 'NoServiceReasonDesc']
        },
        noPremiseManual: {
            isRequired: false,
            isDisabled: false,
            active: { id: '', text: '' },
            params: {
                method: 'service-delivery/search',
                module: 'service',
                operation: 'Business/iCABSBNoServiceReasonSearch'
            },
            displayFields: ['NoServiceReasonCode', 'NoServiceReasonDesc']
        }
    };

    public controls: Array<Object> = [
        //Primary Field
        { name: 'PDAVisitRef', type: MntConst.eTypeText },
        { name: 'EmployeeCode', required: true, type: MntConst.eTypeCode, commonValidator: true },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceDateStart', required: true, type: MntConst.eTypeDate },
        { name: 'ActivityTypeCode', required: true, disabled: true, type: MntConst.eTypeInteger },
        { name: 'ActivityTypeDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ActivityStatusErrorMessage', disabled: true },

        //1st Tab
        { name: 'ActivityStatusSelect' },
        { name: 'ActivityStatusCode', type: MntConst.eTypeInteger },
        { name: 'ActivityStatusDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'OutcardNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'WasteConsignmentNoteNumber' },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: true, type: MntConst.eTypeText },
        { name: 'ProductCode', type: MntConst.eTypeCode },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceCoverNumber', type: MntConst.eTypeInteger },
        { name: 'ItemDescription', disabled: true, type: MntConst.eTypeText },
        { name: 'VisitTypeCode', required: false, type: MntConst.eTypeCode },
        { name: 'VisitTypeDesc' },
        { name: 'ProductComponentRemoved', type: MntConst.eTypeText },
        { name: 'ProductComponentRemDesc', type: MntConst.eTypeText },
        { name: 'ProductComponentCode', type: MntConst.eTypeText },
        { name: 'AlternateProductCode', type: MntConst.eTypeCode },
        { name: 'ProductComponentDesc', type: MntConst.eTypeText },
        { name: 'ServicedQuantity', type: MntConst.eTypeInteger },
        { name: 'PlanVisitNumber', type: MntConst.eTypeInteger },
        { name: 'DeliveryNoteNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'ActualStartTime', type: MntConst.eTypeTime },
        { name: 'ActualEndTime', type: MntConst.eTypeTime },
        { name: 'KeyedStartTime', required: true, type: MntConst.eTypeTime },
        { name: 'KeyedEndTime', required: true, type: MntConst.eTypeTime },
        { name: 'StandardDuration', required: true, type: MntConst.eTypeTime },
        { name: 'OvertimeDuration', required: true, type: MntConst.eTypeTime },
        { name: 'StartMileage', type: MntConst.eTypeInteger },

        //2nd TAb
        { name: 'ManualVisitReasonCode', type: MntConst.eTypeCode },
        { name: 'ManualVisitReasonDesc', type: MntConst.eTypeText },
        { name: 'NoPremiseVisitReasonCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'NoPremiseVisitReasonDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'NoPremiseVisitManualCode', type: MntConst.eTypeCode },
        { name: 'NoPremiseVisitManualDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'NoServiceReasonCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'NoServiceReasonDesc', disabled: true, type: MntConst.eTypeCode },
        { name: 'NoServiceManualCode', type: MntConst.eTypeCode },
        { name: 'NoServiceManualDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'NoBarcodeReasonCode', type: MntConst.eTypeCode },
        { name: 'NoBarcodeReasonDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'NoBarcodeManualCode', type: MntConst.eTypeCode },
        { name: 'NoBarcodeManualDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ReasonNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'ReasonDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'NoSignatureManualNumber', type: MntConst.eTypeInteger },
        { name: 'NoSignatureManualDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseContactName', disabled: true, type: MntConst.eTypeText },
        { name: 'VisitNote', type: MntConst.eTypeText, commonValidator: true },
        { name: 'OutcardPremiseContactName', disabled: true, type: MntConst.eTypeText },
        { name: 'OutcardAddressLine1', disabled: true, type: MntConst.eTypeText },
        { name: 'OutcardAddressLine2', disabled: true, type: MntConst.eTypeText },
        { name: 'OutcardAddressLine3', disabled: true, type: MntConst.eTypeText },
        { name: 'OutcardAddressLine4', disabled: true, type: MntConst.eTypeText },
        { name: 'OutcardAddressLine5', disabled: true, type: MntConst.eTypeText },
        { name: 'OutcardPremisePostcode', disabled: true, type: MntConst.eTypeText },
        { name: 'OutcardPhone', disabled: true, type: MntConst.eTypeText },
        { name: 'OutcardServiceTypeCode', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceTypeDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'OutcardPrice', disabled: true, type: MntConst.eTypeCurrency },
        { name: 'OutcardVisitFrequency', disabled: true, type: MntConst.eTypeInteger },

        //hidden
        { name: 'NotVisitActivity' },
        { name: 'ServiceCoverRowID' },
        { name: 'GotSignature' },
        { name: 'PremiseContactSignatureURL' },
        { name: 'NameCapturedInd' },
        { name: 'PremiseContactPrintedNameURL' },
        { name: 'BranchServiceAreaCode' },
        { name: 'ComponentTypeCode' },
        { name: 'SelProductCode' },
        { name: 'SelProductDesc' },
        { name: 'SelComponentTypeCode' },
        { name: 'SelProductAlternateCode' },
        { name: 'ServiceCoverItemNumber' },
        { name: 'VisitNarrativeCode' },
        { name: 'ServiceCoverComponentNumber' },
        { name: 'PremiseLocationNumber' },
        { name: 'PremiseLocationDesc' },
        { name: 'ComponentReplacementNumber' },
        { name: 'ComponentReplaceRequest' },
        { name: 'VisitReviewRequired' },
        { name: 'Recordinfestations' },
        { name: 'PrepsEnabled' }
    ];

    constructor(injector: Injector, public renderer: Renderer) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPDAICABSACTIVITYMAINTENANCE;
        this.browserTitle = 'Service Visit Rejections Maintenance';
        this.pageTitle = 'Service Visit Rejections Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterViewInit(): void {
        if (this.isReturning()) {
            this.buildMenu();

            this.isRecordFound = true;
            if (this.parentMode === 'ServiceActivityAdd') {
                this.renderer.setElementClass(this.secondTabPane.nativeElement, 'tab-pane', false);
            }

            if (this.getControlValue('VisitTypeCode')) {
                this.dropdownConfig.visitType.active = {
                    id: this.getControlValue('VisitTypeCode'),
                    text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
                };
            }

            if (this.getControlValue('ManualVisitReasonCode')) {
                this.dropdownConfig.manualVisitReason.active = {
                    id: this.getControlValue('ManualVisitReasonCode'),
                    text: this.getControlValue('ManualVisitReasonCode') + ' - ' + this.getControlValue('ManualVisitReasonDesc')
                };
            }

            if (this.getControlValue('NoPremiseVisitManualCode')) {
                this.dropdownConfig.noPremiseManual.active = {
                    id: this.getControlValue('NoPremiseVisitManualCode'),
                    text: this.getControlValue('NoPremiseVisitManualCode') + ' - ' + this.getControlValue('NoPremiseVisitManualDesc')
                };
            }

            if (this.getControlValue('NoServiceReasonCode')) {
                this.dropdownConfig.noServiceManual.active = {
                    id: this.getControlValue('NoServiceReasonCode'),
                    text: this.getControlValue('NoServiceReasonCode') + ' - ' + this.getControlValue('NoServiceReasonDesc')
                };
            }

            if (this.getControlValue('NoSignatureManualNumber')) {
                this.dropdownConfig.noSignatureManual.active = {
                    id: this.getControlValue('NoSignatureManualNumber'),
                    text: this.getControlValue('NoSignatureManualNumber') + ' - ' + this.getControlValue('NoSignatureManualDesc')
                };
            }
            this.currentMode = this.pageParams.pageMode;

        } else {
            this.loadSpeedScript();
        }

        let isParentModeServiceActivityAdd: boolean = this.parentMode === 'ServiceActivityAdd';
        this.pageParams.isDeleteBtnVisible = isParentModeServiceActivityAdd;

        this.disableControl('EmployeeCode', !isParentModeServiceActivityAdd);
        this.disableControl('ActivityTypeCode', !isParentModeServiceActivityAdd);
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private loadSpeedScript(): void {
        let sysCharNumbers = [
            this.sysCharConstants.SystemCharEnablePreps,
            this.sysCharConstants.SystemCharEnableInfestations,
            this.sysCharConstants.SystemCharShowWasteConsNumInVisitEntry,
            this.sysCharConstants.SystemCharEnableManualVisitReasonCode,
            this.sysCharConstants.SystemCharEnableBarcodes,
            this.sysCharConstants.SystemCharEnableNoServiceReasons,
            this.sysCharConstants.SystemCharEnableServiceCoverDisplayLevel
        ];

        let sysCharIP: Object = {
            operation: 'iCABSSePDAiCABSActivityMaintenance',
            action: 0,
            businessCode: this.utils.getBusinessCode(),
            countryCode: this.utils.getCountryCode(),
            SysCharList: sysCharNumbers.toString()
        };

        this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            this.pageParams.isSCEnablePreps = data['records'][0].Required;
            this.pageParams.isSCEnableInfestations = data['records'][1].Required;
            this.pageParams.isSCShowConsNote = data['records'][2].Required;
            this.pageParams.isSCEnableManualVisitReasonCode = data['records'][3].Required;
            this.pageParams.isSCEnableBarcodes = data['records'][4].Required;
            this.pageParams.isSCEnableNoServiceReasons = data['records'][5].Required;
            this.pageParams.isSCServiceCoverDispLevelEnabled = data['records'][6].Required;

            this.windowOnLoad();
        });
    }

    private windowOnLoad(): void {
        //Required Variables
        this.pageParams.isMenuDisabled = false;
        this.pageParams.isContractNumberRequired = false;
        this.pageParams.isPremiseNumberRequired = false;
        this.pageParams.isProductCodeRequired = false;
        this.pageParams.isVisitTypeCodeRequired = false;
        this.pageParams.isPlanVisitNumberRequired = false;
        this.pageParams.isActualTimeRequired = false;

        //Show Hide Controls
        this.pageParams.isDeleteBtnVisible = false;
        this.pageParams.isActivityTypeCodeVisible = false;
        this.pageParams.isActivityStatusSelectVisible = false;
        this.pageParams.isActivityStatusDescVisible = false;
        this.pageParams.isManualVisitReasonVisible = false;
        this.pageParams.isNoBarcodeVisible = false;
        this.pageParams.isNoPremiseVisitVisible = false;
        this.pageParams.isReasonVisible = false;
        this.pageParams.isSignatureVisible = false;
        this.pageParams.isPremiseContactVisible = false;
        this.pageParams.isNoServiceVisible = false;
        this.pageParams.isStartMileageVisible = false;
        this.pageParams.isActualTimesVisible = true;
        this.pageParams.isActivityStatusErrorMessageVisible = true;
        this.pageParams.isComponentInstalledVisible = false;
        this.pageParams.isComponentRemovedVisible = false;
        this.pageParams.isItemDescriptionVisible = false;
        this.pageParams.isConsNoteVisible = false;
        this.pageParams.isCmdCompReplacementVisible = false;
        this.pageParams.isPremiseContactNameVisible = false;
        this.pageParams.isPremiseContactSignatureVisible = true;
        this.pageParams.isPremiseContactPrintedNameVisible = false;
        this.pageParams.isOutcardVisible = true;
        this.pageParams.isDeliveryVisible = true;
        this.pageParams.isOutcardTabVisible = false;

        this.isBtnCreateReplacement = false;

        switch (this.parentMode) {
            case 'ServiceVisitRejections':
            case 'ServiceActivity':
            case 'ServiceActivityAdd':
                this.pageParams.isContractNumberRequired = true;
                this.pageParams.isPremiseNumberRequired = true;
                this.pageParams.isProductCodeRequired = true;
                break;
            case 'TechWorkSummary':
            case 'ServiceVisit':
                this.pageParams.isContractNumberRequired = false;
                this.pageParams.isPremiseNumberRequired = false;
                this.pageParams.isProductCodeRequired = false;
                this.disableControl('ContractNumber', true);
                this.disableControl('PremiseNumber', true);
                this.disableControl('ProductCode', true);
                break;
        }

        this.setRequiredStatus('ContractNumber', this.pageParams.isContractNumberRequired);
        this.setRequiredStatus('PremiseNumber', this.pageParams.isPremiseNumberRequired);
        this.setRequiredStatus('ProductCode', this.pageParams.isProductCodeRequired);

        if (this.parentMode === 'ServiceVisitRejections' || this.parentMode === 'TechWorkSummary' || this.parentMode === 'ServiceVisit') {
            this.pageParams.isActualTimeRequired = true;
            this.setRequiredStatus('ActualStartTime', true);
            this.setRequiredStatus('ActualEndTime', true);
        }

        if (this.pageParams.isSCShowConsNote) {
            this.pageParams.isConsNoteVisible = true;
        }
        this.buildMenu();
        this.setRowIDAndMode();
    }

    private buildMenu(): void {
        if (this.pageParams.isSCEnablePreps || this.pageParams.isSCEnableInfestations) {
            if (this.pageParams.isSCEnablePreps) this.addMenuOption('Prep', 'Prep Used', 'optionMenu');
            if (this.pageParams.isSCEnableInfestations) this.addMenuOption('Infest', 'Infestations', 'optionMenu');
        }

        this.addMenuOption('Recommendation', 'Service Recommendations', 'optionMenu');

        switch (this.parentMode) {
            case 'ServiceActivityAdd':
                this.addMenuOption('NotVisit', 'Not Visit', 'statusMenu');
                this.addMenuOption('New', 'To Be Processed', 'statusMenu');
                break;
            case 'ServiceActivity':
            case 'Nothing':
                break;
            default:
                this.addMenuOption('NotProcessed', 'To Be Processed', 'statusMenu');
                this.addMenuOption('Deleted', 'Deleted', 'statusMenu');
                break;
        }
    }

    private addMenuOption(value: string, text: string, menu: string): void {
        let option: Object = {
            value: value,
            text: text
        };
        if (menu === 'statusMenu') {
            this.statusArray.push(option);
        } else {
            this.menuArray.push(option);
        }
    }

    private setRowIDAndMode(): void {
        switch (this.parentMode) {
            case 'ServiceActivityAdd':
                this.pageParams.isMenuDisabled = true;
                this.tabLength = 1;
                this.riExchange.getParentHTMLValue('EmployeeCode');
                this.riExchange.getParentHTMLValue('EmployeeSurname');
                this.riExchange.getParentHTMLValue('ActivityTypeCode');
                this.riExchange.getParentHTMLValue('ActivityTypeDesc');
                this.riExchange.getParentHTMLValue('ContractNumber');
                this.riExchange.getParentHTMLValue('ContractName');
                this.riExchange.getParentHTMLValue('ServiceCoverNumber');
                this.riExchange.getParentHTMLValue('PremiseNumber');
                this.riExchange.getParentHTMLValue('PremiseName');
                this.riExchange.getParentHTMLValue('ProductCode');
                this.riExchange.getParentHTMLValue('ProductDesc');
                let visitTypeCode: string = this.riExchange.getParentHTMLValue('VisitTypeCode');
                let visitTypeDesc: string = this.riExchange.getParentHTMLValue('VisitTypeDesc');

                if (visitTypeCode || visitTypeDesc) {
                    this.dropdownConfig.visitType.active = {
                        id: visitTypeCode,
                        text: visitTypeCode + ' - ' + visitTypeDesc
                    };
                }
                this.riExchange.getParentHTMLValue('ServiceDateStart');
                this.riExchange.getParentHTMLValue('ServiceTimeStart');
                this.riExchange.getParentHTMLValue('ServiceTimeEnd');
                this.riExchange.getParentHTMLValue('StandardDuration');
                this.riExchange.getParentHTMLValue('OvertimeDuration');
                this.riExchange.getParentHTMLValue('Mileage');
                this.currentMode = MntConst.eModeAdd;
                this.beforeAdd();
                break;
            case 'ServiceVisit':
                this.pageParams.PDAICABSActivityRowID = this.riExchange.getParentHTMLValue('PDAICABSActivityRowID');
                this.currentMode = MntConst.eModeUpdate;
                this.fetchRecord();
                break;
            default:
                this.pageParams.PDAICABSActivityRowID = this.riExchange.getParentAttributeValue('PDAICABSActivityRowID');
                this.currentMode = MntConst.eModeUpdate;
                this.fetchRecord();
        }

        switch (this.parentMode) {
            case 'ServiceVisitRejections':
                this.pageParams.isActivityStatusSelectVisible = true;
                this.pageParams.isActivityStatusDescVisible = false;
                this.setControlValue('ActivityStatusSelect', 'NotProcessed');
                this.utils.setTitle('Service Visit Rejections Maintenance');
                this.pageTitle = 'Service Visit Rejections Maintenance';
                this.pageParams.isManualVisitReasonVisible = this.pageParams.isSCEnableManualVisitReasonCode;
                this.pageParams.isNoBarcodeVisible = this.pageParams.isSCEnableBarcodes;
                this.pageParams.isNoPremiseVisitVisible = this.pageParams.isSCEnableNoServiceReasons;
                this.pageParams.isNoServiceVisible = this.pageParams.isSCEnableNoServiceReasons;
                break;

            case 'TechWorkSummary':
            case 'ServiceVisit':
                this.pageParams.isActivityStatusSelectVisible = false;
                this.pageParams.isActivityStatusDescVisible = true;
                this.setControlValue('ActivityStatusSelect', '');
                this.pageParams.isStartMileageVisible = true;
                this.utils.setTitle('Servicing Work Summary Maintenance');
                this.pageTitle = 'Servicing Work Summary Maintenance';
                break;

            case 'ServiceActivity':
                this.pageParams.isActivityStatusSelectVisible = false;
                this.pageParams.isActivityStatusDescVisible = true;
                this.setControlValue('ActivityStatusSelect', '');
                this.pageParams.isStartMileageVisible = true;
                this.pageParams.isActualTimesVisible = false;
                this.pageParams.isActivityStatusErrorMessageVisible = false;
                this.lblKeyedStartTime = 'Start Time';
                this.utils.setTitle('Service Activity Maintenance');
                this.pageTitle = 'Service Activity Maintenance';
                break;

            case 'ServiceActivityAdd':
                this.pageParams.isActivityStatusSelectVisible = true;
                this.pageParams.isActivityStatusDescVisible = false;
                this.pageParams.isActivityTypeCodeVisible = true;
                this.pageParams.isStartMileageVisible = true;
                this.pageParams.isActualTimesVisible = false;
                this.pageParams.isActivityStatusErrorMessageVisible = false;
                this.lblKeyedStartTime = 'Start Time';
                this.utils.setTitle('Service Activity Maintenance');
                this.pageTitle = 'Service Activity Maintenance';
                break;
        }
        this.activityStatusSelectOnChange();
    }

    private beforeUpdate(): void {
        this.renderer.setElementClass(this.secondTabPane.nativeElement, 'tab-pane', true);
        this.pageParams.isOutcardVisible = !(this.getControlValue('OutcardNumber') === '');
        this.pageParams.isDeliveryVisible = !(this.getControlValue('DeliveryNoteNumber') === '');

        if (this.getControlValue('NotVisitActivity') === 'NotVisit') {
            this.pageParams.isContractNumberRequired = false;
            this.pageParams.isPremiseNumberRequired = false;
            this.pageParams.isProductCodeRequired = false;
            this.pageParams.isVisitTypeCodeRequired = false;
            this.pageParams.isPlanVisitNumberRequired = false;
            this.setRequiredStatus('ContractNumber', false);
            this.setRequiredStatus('PremiseNumber', false);
            this.setRequiredStatus('ProductCode', false);
            this.setRequiredStatus('PlanVisitNumber', false);
            this.setRequiredStatus('VisitTypeCode', false);
            this.dropdownConfig.visitType.isRequired = false;
        }

        if (this.currentMode === MntConst.eModeUpdate) {
            if (this.pageParams.isSCServiceCoverDispLevelEnabled && this.getControlValue('VisitTypeCode')) {
                if (this.pageParams.isComponentRemovedVisible) {
                    if ((!this.getControlValue('ComponentReplacementNumber') || this.getControlValue('ComponentReplacementNumber') === '0') &&
                        this.getControlValue('ComponentReplaceRequest') && this.getControlValue('VisitReviewRequired')) {
                        this.pageParams.isCmdCompReplacementVisible = true;
                    }
                    else {
                        this.disableControl('ProductComponentCode', true);
                        this.disableControl('ProductComponentDesc', true);
                        this.disableControl('AlternateProductCode', true);
                    }

                    this.disableControl('ContractNumber', true);
                    this.disableControl('PremiseNumber', true);
                    this.disableControl('ProductCode', true);
                    this.disableControl('ProductComponentRemoved', true);
                    this.disableControl('ProductComponentRemDesc', true);
                }
            } else {
                this.pageParams.isCmdCompReplacementVisible = false;
                this.pageParams.isComponentRemovedVisible = false;
                this.pageParams.isComponentInstalledVisible = false;
            }

            if (this.pageParams.isSCServiceCoverDispLevelEnabled && this.pageParams.isCmdCompReplacementVisible) {
                if (this.getControlValue('ProductComponentCode')) {
                    this.disableControl('ProductComponentDesc', true);

                    for (let productCode in this.dummyProductCodes) {
                        if (this.getControlValue('ProductComponentCode').toUpperCase() === productCode.toUpperCase()) {
                            this.disableControl('ProductComponentDesc', false);
                            break;
                        }
                    }
                } else {
                    this.disableControl('ProductComponentDesc', true);
                    this.setControlValue('ProductComponentCode', '');
                }
            }
            this.formPristine();
        }
    }

    private beforeAdd(): void {
        this.pageParams.isOutcardVisible = false;
        this.pageParams.isDeliveryVisible = false;
        this.setControlValue('ActivityStatusSelect', 'NotVisit');
        this.renderer.setElementClass(this.secondTabPane.nativeElement, 'tab-pane', false);
        this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
        this.setControlValue('ServiceDateStart', this.riExchange.getParentHTMLValue('ServiceDate'));
        if (this.getControlValue('EmployeeCode')) {
            this.getEmployeeSurname();
        }
        this.activityStatusSelectOnChange();
        this.setEmployeeFocus.emit(true);
        this.storeFieldValues();
    }

    private populateDescriptions(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');

        let bodyParams: any = {};
        if (this.getControlValue('ContractNumber')) {
            bodyParams[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        }
        if (this.getControlValue('PremiseNumber')) {
            bodyParams[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        }
        if (this.getControlValue('ProductCode')) {
            bodyParams[this.serviceConstants.ProductCode] = this.getControlValue('ProductCode');
        }

        bodyParams['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }

                if (data.ContractName) {
                    this.setControlValue('ContractNumber', data.ContractNumber);
                    this.setControlValue('ContractName', data.ContractName);
                    this.fetchEllipsisData(data, 'contract');
                } else {
                    this.setControlValue('ContractNumber', '');
                    this.setControlValue('ContractName', '');
                }

                if (data.PremiseName) {
                    this.setControlValue('PremiseNumber', data.PremiseNumber);
                    this.setControlValue('PremiseName', data.PremiseName);
                    this.fetchEllipsisData(data, 'premise');
                } else {
                    this.setControlValue('PremiseNumber', '');
                    this.setControlValue('PremiseName', '');
                }

                if (data.ProductDesc) {
                    this.setControlValue('ProductCode', data.ProductCode);
                    this.setControlValue('ProductDesc', data.ProductDesc);
                    this.fetchEllipsisData(data, 'product');
                } else {
                    this.setControlValue('ProductCode', '');
                    this.setControlValue('ProductDesc', '');
                }

                if (data.ServiceCoverNumber === '0') {
                    this.setControlValue('ServiceCoverNumber', '');
                } else {
                    this.setControlValue('ServiceCoverNumber', data.ServiceCoverNumber);
                    this.setControlValue('ServiceCoverRowID', '');
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private lookupDescriptions(data: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let lookup_details = [{
            'table': 'Employee',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'EmployeeCode': data.EmployeeCode
            },
            'fields': ['EmployeeSurname']
        }, {
            'table': 'SystemPDAActivityTypeLang',
            'query': {
                'LanguageCode': this.riExchange.LanguageCode(),
                'SystemPDAActivityTypeCode': data.ActivityTypeCode
            },
            'fields': ['SystemPDAActivityTypeDesc']
        }, {
            'table': 'SystemPDAActivityStatusLang',
            'query': {
                'LanguageCode': this.riExchange.LanguageCode(),
                'SystemPDAActivityStatusCode': data.ActivityStatusCode
            },
            'fields': ['SystemPDAActivityStatusDesc']
        }, {
            'table': 'Product',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'ProductCode': data.ProductCode
            },
            'fields': ['ProductDesc', 'Recordinfestations', 'PrepsEnabled', 'ComponentTypeCode']
        }, {
            'table': 'VisitType',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'VisitTypeCode': data.VisitTypeCode
            },
            'fields': ['VisitTypeCode', 'VisitTypeDesc']
        }, {
            'table': 'ManualVisitReason',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'ManualVisitReasonCode': data.PlanVisitStatusCode //TODO
            },
            'fields': ['ManualVisitReasonDesc']
        }, {
            'table': 'NoServiceReason',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'NoServiceReasonCode': data.NoServiceReasonCode
            },
            'fields': ['NoServiceReasonDesc']
        }, {
            'table': 'NoBarcodeReason',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'NoBarcodeReasonCode': data.NoBarcodeReasonCode
            },
            'fields': ['NoBarcodeReasonDesc']
        }, {
            'table': 'NoSignatureReason',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'ReasonNumber': data.ReasonNumber
            },
            'fields': ['ReasonDesc']
        }, {
            'table': 'ServiceCover',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'ContractNumber': data.ContractNumber,
                'PremiseNumber': data.PremiseNumber,
                'ProductCode': data.ProductCode,
                'ServiceCoverNumber': data.ServiceCoverNumber
            },
            'fields': ['BranchServiceAreaCode']
        }];

        this.LookUp.lookUpRecord(lookup_details).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.length) {
                    let employeeData = data[0];
                    let activityTypeData = data[1];
                    let activityStatusData = data[2];
                    let productData = data[3];
                    let visitTypeData = data[4];
                    let manualVisitData = data[5];
                    let noServiceData = data[6];
                    let noBarcodeData = data[7];
                    let noSignatureData = data[8];
                    let serviceCoverData = data[9];

                    if (employeeData.length) {
                        this.setControlValue('EmployeeSurname', employeeData[0].EmployeeSurname);
                    }
                    if (activityTypeData.length) {
                        this.setControlValue('ActivityTypeDesc', activityTypeData[0].SystemPDAActivityTypeDesc);
                    }
                    if (activityStatusData.length) {
                        this.setControlValue('ActivityStatusDesc', activityStatusData[0].SystemPDAActivityStatusDesc);
                    }
                    if (productData.length) {
                        this.setControlValue('ProductDesc', productData[0].ProductDesc);
                        this.setControlValue('Recordinfestations', productData[0].Recordinfestations);
                        this.setControlValue('PrepsEnabled', productData[0].PrepsEnabled);
                        this.setControlValue('ComponentTypeCode', productData[0].ComponentTypeCode);
                    }
                    if (visitTypeData.length) {
                        if (visitTypeData[0].VisitTypeCode || visitTypeData[0].VisitTypeDesc) {
                            this.dropdownConfig.visitType.active = {
                                id: visitTypeData[0].VisitTypeCode,
                                text: visitTypeData[0].VisitTypeCode + ' - ' + visitTypeData[0].VisitTypeDesc
                            };
                        }
                        this.setControlValue('VisitTypeCode', visitTypeData[0].VisitTypeCode);
                        this.setControlValue('VisitTypeDesc', visitTypeData[0].VisitTypeDesc);
                    }
                    if (manualVisitData.length) {
                        this.setControlValue('ManualVisitReasonDesc', manualVisitData[0].ManualVisitReasonDesc);
                    }
                    if (noServiceData.length) {
                        this.setControlValue('NoServiceReasonDesc', noServiceData[0].NoServiceReasonDesc);
                    }
                    if (noBarcodeData.length) {
                        this.setControlValue('NoBarcodeReasonDesc', noBarcodeData[0].NoBarcodeReasonDesc);
                    }
                    if (noSignatureData.length) {
                        this.setControlValue('ReasonDesc', noSignatureData[0].ReasonDesc);
                    }
                    if (serviceCoverData.length) {
                        this.setControlValue('BranchServiceAreaCode', serviceCoverData[0].BranchServiceAreaCode);
                    }

                    this.storeFieldValues();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private getEmployeeSurname(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');

        let bodyParams: any = {};
        bodyParams[this.serviceConstants.Function] = 'GetEmployeeSurname';
        bodyParams[this.serviceConstants.EmployeeCode] = this.getControlValue('EmployeeCode');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                if (data.EmployeeSurname) {
                    this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                } else {
                    this.setControlValue('EmployeeCode', '');
                }

                this.activityStatusSelectOnChange();
                this.storeFieldValues();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private fetchRecord(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '0');
        searchParams.set('PDAICABSActivityRowID', this.pageParams.PDAICABSActivityRowID);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.currentMode = MntConst.eModeUpdate;
                this.isRecordFound = true;
                for (let key in data) {
                    if (key === 'VisitReviewRequired' || key === 'GotSignature') {
                        this.setControlValue(key, this.utils.convertResponseValueToCheckboxInput(data[key]));
                    } else {
                        this.setControlValue(key, data[key]);
                    }
                }
                if (Number(data.ActivityTypeCode) === this.outCardTypeCode) {
                    this.pageParams.isOutcardTabVisible = true;
                    this.tabLength = 3;
                }

                if (data.ManualVisitReasonCode) {
                    if (data.ManualVisitReasonCode || data.ManualVisitReasonDesc) {
                        this.dropdownConfig.manualVisitReason.active = {
                            id: data.ManualVisitReasonCode,
                            text: data.ManualVisitReasonCode + ' - ' + data.ManualVisitReasonDesc
                        };
                    }
                }

                if (data.NoPremiseVisitManualCode) {
                    if (data.ManualVisitReasonCode || data.ManualVisitReasonDesc) {
                        this.dropdownConfig.noPremiseManual.active = {
                            id: data.NoPremiseVisitManualCode,
                            text: data.NoPremiseVisitManualCode + ' - ' + data.NoPremiseVisitManualDesc
                        };
                    }
                }

                if (data.NoServiceManualCode) {
                    if (data.NoServiceManualCode || data.NoServiceManualDesc) {
                        this.dropdownConfig.noServiceManual.active = {
                            id: data.NoServiceManualCode,
                            text: data.NoServiceManualCode + ' - ' + data.NoServiceManualDesc
                        };
                    }
                }

                if (data.NoSignatureManualNumber) {
                    if (data.NoSignatureManualNumber || data.NoSignatureManualDesc) {
                        this.dropdownConfig.noSignatureManual.active = {
                            id: data.NoSignatureManualNumber,
                            text: data.NoSignatureManualNumber + ' - ' + data.NoSignatureManualDesc
                        };
                    }
                }

                this.lookupDescriptions(data);
                this.afterFetch();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private afterFetch(): void {
        if (this.getControlValue('GotSignature')) {
            this.pageParams.isSignatureVisible = true;
            this.pageParams.isReasonVisible = false;
            this.pageParams.isPremiseContactVisible = true;

            if (this.getControlValue('NameCapturedInd')) {
                this.pageParams.isPremiseContactPrintedNameVisible = true;
            } else {
                this.pageParams.isPremiseContactNameVisible = true;
            }
        } else {
            this.pageParams.isSignatureVisible = false;
            this.pageParams.isReasonVisible = this.getControlValue('ReasonNumber');
        }

        if (this.pageParams.isSCServiceCoverDispLevelEnabled) {
            this.getDummyProductList();

            if (this.getControlValue('VisitTypeCode')) {
                this.displayComponentReplacement();
            }
        }

        this.beforeUpdate();
    }

    private getDummyProductList(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');

        let bodyParams: any = {};
        bodyParams[this.serviceConstants.Function] = 'DummyProductCodeList';
        bodyParams[this.serviceConstants.ProductCode] = this.getControlValue('ProductCode');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }

                this.dummyProductCodes = data.DummyProductCodes;
                if (this.dummyProductCodes) {
                    this.dummyProductCodes = this.dummyProductCodes.split('|');
                    if (this.currentMode === MntConst.eModeAdd || this.currentMode === MntConst.eModeUpdate) {
                        if (this.getControlValue('ProductComponentCode')) {
                            this.disableControl('ProductComponentDesc', true);
                            for (let code in this.dummyProductCodes) {
                                if (this.getControlValue('ProductComponentCode').toUpperCase() === code.toUpperCase()) {
                                    this.disableControl('ProductComponentDesc', false);
                                }
                            }
                        }
                    } else {
                        this.disableControl('ProductComponentDesc', true);
                        this.setControlValue('ProductComponentDesc', '');
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private defaultStandardDuration(): void {
        if (this.getControlValue('KeyedStartTime') && this.getControlValue('KeyedEndTime') &&
            !this.riExchange.riInputElement.isError(this.uiForm, 'KeyedStartTime') &&
            !this.riExchange.riInputElement.isError(this.uiForm, 'KeyedEndTime')) {
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');

            let bodyParams: any = {
                KeyedStartTime: this.getControlValue('KeyedStartTime'),
                KeyedEndTime: this.getControlValue('KeyedEndTime'),
                StandardDuration: this.getControlValue('StandardDuration'),
                OvertimeDuration: this.getControlValue('OvertimeDuration')
            };
            bodyParams[this.serviceConstants.Function] = 'DefaultStandardDuration';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }

                    this.setControlValue('StandardDuration', this.globalize.formatTimeToLocaleFormat(data.StandardDuration));
                    if (this.currentMode === MntConst.eModeAdd) {
                        this.setControlValue('OvertimeDuration', '00:00');
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    private findExistingPremiseVisit(): void {
        if (this.getControlValue('EmployeeCode') && this.getControlValue('ServiceDateStart') &&
            this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber')) {
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');

            let bodyParams: any = {
                ServiceDateStart: this.getControlValue('ServiceDateStart')
            };
            bodyParams[this.serviceConstants.Function] = 'FindExistingPremiseVisit';
            bodyParams[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
            bodyParams[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
            bodyParams[this.serviceConstants.EmployeeCode] = this.getControlValue('EmployeeCode');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }
                    this.setControlValue('StartMileage', data.StartMileage);
                    this.setControlValue('KeyedStartTime', data.KeyedStartTime);
                    this.setControlValue('KeyedEndTime', data.KeyedEndTime);
                    this.setControlValue('StandardDuration', data.StandardDuration);
                    this.setControlValue('OvertimeDuration', data.OvertimeDuration);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    private displayComponentReplacement(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');

        let bodyParams: any = {
            VisitTypeCode: this.getControlValue('VisitTypeCode')
        };
        bodyParams[this.serviceConstants.Function] = 'EnableComponentReplacement';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }

                this.setControlValue('ComponentReplaceRequest', this.utils.convertResponseValueToCheckboxInput(data.ComponentReplaceRequest));

                if (data.EnableCompReplace === 'yes') {
                    if (this.currentMode === MntConst.eModeUpdate &&
                        (!this.getControlValue('ComponentReplacementNumber') || this.getControlValue('ComponentReplacementNumber') === '0') &&
                        this.getControlValue('ComponentReplaceRequest') && this.getControlValue('VisitReviewRequired')) {
                        this.pageParams.isCmdCompReplacementVisible = true;
                    }
                    this.pageParams.isComponentRemovedVisible = true;
                    this.pageParams.isComponentInstalledVisible = true;
                    this.disableControl('ContractNumber', true);
                    this.disableControl('PremiseNumber', true);
                    this.disableControl('ProductCode', true);
                    this.disableControl('ProductComponentRemoved', true);
                    this.disableControl('ProductComponentRemDesc', true);
                }
                else {
                    this.pageParams.isCmdCompReplacementVisible = false;
                    this.pageParams.isComponentRemovedVisible = false;
                    this.pageParams.isComponentInstalledVisible = false;
                    this.setControlValue('ProductComponentRemoved', '');
                    this.setControlValue('ProductComponentRemDesc', '');
                    this.setControlValue('ProductComponentCode', '');
                    this.setControlValue('AlternateProductCode', '');
                    this.setControlValue('ProductComponentDesc', '');
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private storeFieldValues(): void {
        for (let control in this.uiForm.controls) {
            if (control) {
                this.fieldValues[control] = this.getControlValue(control);
            }
        }
        this.setEllipsisValue();
    }

    private restoreFieldValues(): void {
        this.uiForm.reset();
        for (let control in this.uiForm.controls) {
            if (control) {
                this.setControlValue(control, this.fieldValues[control]);
            }
        }

        if (this.getControlValue('VisitTypeCode')) {
            this.dropdownConfig.visitType.active = {
                id: this.getControlValue('VisitTypeCode'),
                text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
            };
        } else {
            this.dropdownConfig.visitType.active = {
                id: '',
                text: ''
            };
        }

        if (this.getControlValue('ManualVisitReasonCode')) {
            this.dropdownConfig.manualVisitReason.active = {
                id: this.getControlValue('ManualVisitReasonCode'),
                text: this.getControlValue('ManualVisitReasonCode') + ' - ' + this.getControlValue('ManualVisitReasonDesc')
            };
        } else {
            this.dropdownConfig.manualVisitReason.active = {
                id: '',
                text: ''
            };
        }

        if (this.getControlValue('NoPremiseVisitManualCode')) {
            this.dropdownConfig.noPremiseManual.active = {
                id: this.getControlValue('NoPremiseVisitManualCode'),
                text: this.getControlValue('NoPremiseVisitManualCode') + ' - ' + this.getControlValue('NoPremiseVisitManualDesc')
            };
        } else {
            this.dropdownConfig.noPremiseManual.active = {
                id: '',
                text: ''
            };
        }

        if (this.getControlValue('NoServiceReasonCode')) {
            this.dropdownConfig.noServiceManual.active = {
                id: this.getControlValue('NoServiceReasonCode'),
                text: this.getControlValue('NoServiceReasonCode') + ' - ' + this.getControlValue('NoServiceReasonDesc')
            };
        } else {
            this.dropdownConfig.noServiceManual.active = {
                id: '',
                text: ''
            };
        }

        if (this.getControlValue('NoSignatureManualNumber')) {
            this.dropdownConfig.noSignatureManual.active = {
                id: this.getControlValue('NoSignatureManualNumber'),
                text: this.getControlValue('NoSignatureManualNumber') + ' - ' + this.getControlValue('NoSignatureManualDesc')
            };
        } else {
            this.dropdownConfig.noSignatureManual.active = {
                id: '',
                text: ''
            };
        }
        this.setEllipsisValue();
        this.activityStatusSelectOnChange();
        if (this.pageParams.isSCServiceCoverDispLevelEnabled && this.getControlValue('VisitTypeCode')) {
            this.displayComponentReplacement();
        } else {
            this.setControlValue('ComponentReplaceRequest', false);
            this.pageParams.isCmdCompReplacementVisible = false;
            this.pageParams.isComponentRemovedVisible = false;
            this.pageParams.isComponentInstalledVisible = false;
        }

        this.disableControl('ContractNumber', false);
        this.disableControl('PremiseNumber', false);
        this.disableControl('ProductCode', false);
    }

    private setEllipsisValue(): void {
        this.ellipsisConfig.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
        this.ellipsisConfig.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');

        this.ellipsisConfig.product.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
        this.ellipsisConfig.product.childConfigParams['ContractName'] = this.getControlValue('ContractName');
        this.ellipsisConfig.product.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
        this.ellipsisConfig.product.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
        this.ellipsisConfig.product.childConfigParams['ProductCode'] = this.getControlValue('ProductCode');
        this.ellipsisConfig.product.childConfigParams['ProductDesc'] = this.getControlValue('ProductDesc');

        this.ellipsisConfig.planVisit.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
        this.ellipsisConfig.planVisit.childConfigParams['ContractName'] = this.getControlValue('ContractName');
        this.ellipsisConfig.planVisit.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
        this.ellipsisConfig.planVisit.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
        this.ellipsisConfig.planVisit.childConfigParams['ProductCode'] = this.getControlValue('ProductCode');
        this.ellipsisConfig.planVisit.childConfigParams['ProductDesc'] = this.getControlValue('ProductDesc');
        this.ellipsisConfig.planVisit.childConfigParams['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
    }

    private beforeSave(): void {
        if (!this.isBtnCreateReplacement && this.pageParams.isCmdCompReplacementVisible &&
            (this.getControlValue('ComponentReplacementNumber') || this.getControlValue('ComponentReplacementNumber') === '0') &&
            this.getControlValue('ActivityStatusSelect') === 'NotProcessed') {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.activityNotProcessed));
        } else {
            if (this.getControlValue('ActivityStatusSelect') === 'NotProcessed' || this.getControlValue('ActivityStatusSelect') === 'New') {
                if (!this.getControlValue('ServiceCoverNumber') && !this.getControlValue('ServiceCoverRowID')) {
                    if (this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber') && this.getControlValue('ProductCode')) {
                        this.isFetchSCNumber = true;
                        this.serviceCoverSearch.openModal();
                    }
                }
                if (!this.isFetchSCNumber) {
                    this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveRecord.bind(this)));
                }
            } else {
                this.disableControl('ContractNumber', true);
                this.disableControl('PremiseNumber', true);
                this.disableControl('ProductCode', true);
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveRecord.bind(this)));
            }
        }
    }

    private saveRecord(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, (this.currentMode === MntConst.eModeAdd) ? '1' : '2');

        let checkBoxList = ['GotSignature', 'ComponentReplaceRequest'];

        let saveControls: Array<any> = ['EmployeeCode', 'ActivityTypeCode', 'ActivityStatusCode', 'ActivityStatusErrorMessage', 'ActivityStatusSelect', 'OutcardNumber',
            'DeliveryNoteNumber', 'ContractNumber', 'PremiseNumber', 'ProductCode', 'ServiceCoverNumber', 'WasteConsignmentNoteNumber', 'VisitTypeCode', 'PlanVisitNumber',
            'ServicedQuantity', 'StartMileage', 'ServiceDateStart', 'KeyedStartTime', 'KeyedEndTime', 'StandardDuration', 'OvertimeDuration', 'ManualVisitReasonCode',
            'NoPremiseVisitReasonCode', 'NoPremiseVisitManualCode', 'NoServiceReasonCode', 'NoServiceManualCode', 'NoBarcodeReasonCode', 'NoBarcodeManualCode', 'ReasonNumber',
            'NoSignatureManualNumber', 'PremiseContactName', 'VisitNote', 'PDAVisitRef', 'OutcardPremiseContactName', 'OutcardAddressLine1', 'OutcardAddressLine2', 'OutcardAddressLine3',
            'OutcardAddressLine4', 'OutcardAddressLine5', 'OutcardPremisePostcode', 'OutcardPhone', 'OutcardServiceTypeCode', 'OutcardPrice', 'OutcardVisitFrequency', 'ContractName',
            'ServiceTypeDesc', 'PremiseName', 'NotVisitActivity', 'NoPremiseVisitReasonDesc', 'NoPremiseVisitManualDesc', 'NoBarcodeManualDesc', 'NoServiceManualDesc', 'NoSignatureManualDesc',
            'GotSignature', 'PremiseContactSignatureURL', 'PremiseContactPrintedNameURL', 'ItemDescription', 'PremiseLocationDesc', 'ComponentReplaceRequest', 'ServiceCoverRowID', 'ActualStartTime', 'ActualEndTime'];

        let bodyParams: Object = {};
        for (let i = 0; i < saveControls.length; i++) {
            if (checkBoxList.indexOf(saveControls[i]) > -1) {
                bodyParams[saveControls[i]] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue(saveControls[i]));
            } else {
                if (saveControls[i] === 'OutcardPrice') {
                    bodyParams[saveControls[i]] = this.getControlValue(saveControls[i]) ? this.getControlValue(saveControls[i]) : 0;
                } else {
                    bodyParams[saveControls[i]] = this.getControlValue(saveControls[i]);
                }
            }
        }
        if (this.pageParams.isActualTimeRequired === false) {
            bodyParams['ActualStartTime'] = 0;
            bodyParams['ActualEndTime'] = 0;
        }
        if (this.currentMode === MntConst.eModeUpdate) {
            bodyParams['PDAICABSActivityROWID'] = this.pageParams.PDAICABSActivityRowID;
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.isRecordFound = true;
                this.pageParams.isMenuDisabled = false;
                this.currentMode = MntConst.eModeUpdate;
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                this.pageParams.isCmdCompReplacementVisible = false;
                this.formPristine();
                this.storeFieldValues();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private deleteRecord(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '3');

        let bodyParams: any = {
            PDAICABSActivityROWID: this.pageParams.PDAICABSActivityRowID
        };
        bodyParams[this.serviceConstants.EmployeeCode] = this.getControlValue('EmployeeCode');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                let employeeCode = this.getControlValue('EmployeeCode');
                this.uiForm.reset();
                this.setControlValue('EmployeeCode', employeeCode);
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully));
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onSaveClick(): void {
        let formValid = true;
        this.dropdownConfig.visitType.triggerValidate = true;

        if (!this.riExchange.validateForm(this.uiForm)) return;

        let lookup_details = [{
            'table': 'Employee',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'EmployeeCode': this.getControlValue('EmployeeCode')
            },
            'fields': ['EmployeeSurname']
        }, {
            'table': 'SystemPDAActivityTypeLang',
            'query': {
                'LanguageCode': this.riExchange.LanguageCode(),
                'SystemPDAActivityTypeCode': this.getControlValue('ActivityTypeCode').toString()
            },
            'fields': ['SystemPDAActivityTypeDesc']
        }];

        this.LookUp.lookUpRecord(lookup_details).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.length) {
                    let employeeData = data[0];
                    let activityTypeData = data[1];

                    if (employeeData.length) {
                        this.setControlValue('EmployeeSurname', employeeData[0].EmployeeSurname);
                    }
                    if (activityTypeData.length) {
                        this.setControlValue('ActivityTypeDesc', activityTypeData[0].SystemPDAActivityTypeDesc);
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        this.beforeSave();
    }

    public onCancelClick(): void {
        this.restoreFieldValues();
    }

    public onDeleteClick(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteRecord.bind(this)));
    }

    public fetchEllipsisData(data: any, type: string, isEllipsisReturn?: boolean): void {
        switch (type) {
            case 'employee':
                this.setControlValue('EmployeeCode', data.EmployeeCode);
                this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                if (isEllipsisReturn) {
                    this.uiForm.controls['EmployeeCode'].markAsDirty();
                }
                break;
            case 'activityType':
                this.setControlValue('ActivityTypeCode', data.SystemPDAActivityTypeCode);
                this.setControlValue('ActivityTypeDesc', data.SystemPDAActivityTypeDesc);
                if (isEllipsisReturn) {
                    this.uiForm.controls['ActivityTypeCode'].markAsDirty();
                }
                break;
            case 'planVisit':
                this.setControlValue('PlanVisitNumber', data.PlanVisitNumber);
                if (isEllipsisReturn) {
                    this.uiForm.controls['PlanVisitNumber'].markAsDirty();
                }
                break;
            case 'contract':
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('ContractName', data.ContractName);
                this.ellipsisConfig.premise.childConfigParams['ContractNumber'] = data.ContractNumber;
                this.ellipsisConfig.premise.childConfigParams['ContractName'] = data.ContractName;
                this.ellipsisConfig.product.childConfigParams['ContractNumber'] = data.ContractNumber;
                this.ellipsisConfig.product.childConfigParams['ContractName'] = data.ContractName;
                this.ellipsisConfig.planVisit.childConfigParams['ContractNumber'] = data.ContractNumber;
                this.ellipsisConfig.planVisit.childConfigParams['ContractName'] = data.ContractName;
                if (isEllipsisReturn) {
                    this.uiForm.controls['ContractNumber'].markAsDirty();
                }
                break;
            case 'premise':
                this.setControlValue('PremiseNumber', data.PremiseNumber);
                this.setControlValue('PremiseName', data.PremiseName);
                this.ellipsisConfig.product.childConfigParams['PremiseNumber'] = data.PremiseNumber;
                this.ellipsisConfig.product.childConfigParams['PremiseName'] = data.PremiseName;
                this.ellipsisConfig.planVisit.childConfigParams['PremiseNumber'] = data.PremiseNumber;
                this.ellipsisConfig.planVisit.childConfigParams['PremiseName'] = data.PremiseName;
                if (isEllipsisReturn) {
                    this.uiForm.controls['PremiseNumber'].markAsDirty();
                }
                if (this.currentMode === MntConst.eModeAdd) {
                    this.findExistingPremiseVisit();
                }
                break;
            case 'product':
                this.setControlValue('ProductCode', data.ProductCode);
                this.setControlValue('ProductDesc', data.ProductDesc);
                this.ellipsisConfig.product.childConfigParams['ProductCode'] = data.ProductCode;
                this.ellipsisConfig.product.childConfigParams['ProductDesc'] = data.ProductDesc;
                this.ellipsisConfig.planVisit.childConfigParams['ProductCode'] = data.ProductCode;
                this.ellipsisConfig.planVisit.childConfigParams['ProductDesc'] = data.ProductDesc;
                if (isEllipsisReturn) {
                    this.uiForm.controls['ProductCode'].markAsDirty();
                }

                if (data.row) {
                    this.setControlValue('ServiceCoverRowID', data.row.ttServiceCover);
                    this.ellipsisConfig.planVisit.childConfigParams['ServiceCoverRowID'] = data.row.ttServiceCover;

                    /*If Service Cover RowID is set then remove the ServiceCoverNumber field value to force the update routine
                    to use the ServiceCover RowID value to find the correct Service Cover that the PDAiCABSActivity record is
                    to be linked with. */
                    if (this.getControlValue('ServiceCoverRowID')) {
                        this.setControlValue('ServiceCoverNumber', '');
                    }
                }
                break;
            case 'productComponent':
                if (data.SelProductCode !== this.getControlValue('ProductComponentCode')) {
                    this.disableControl('ProductComponentDesc', true);
                    this.setControlValue('AlternateProductCode', data.SelProductAlternateCode);
                    this.setControlValue('ProductComponentDesc', data.SelProductDesc);
                    this.setControlValue('ProductComponentCode', data.SelProductCode);
                    if (this.getControlValue('ProductComponentCode')) {
                        for (let productCode in this.dummyProductCodes) {
                            if (this.getControlValue('ProductComponentCode').toUpperCase() === productCode.toUpperCase()) {
                                this.disableControl('ProductComponentDesc', false);
                                this.setCompDescFocus.emit(true);
                            }
                        }
                    } else {
                        this.disableControl('ProductComponentDesc', true);
                        this.setControlValue('ProductComponentDesc', '');
                    }
                }
                if (isEllipsisReturn) {
                    this.uiForm.controls['ProductComponentCode'].markAsDirty();
                }
                break;
            case 'alternateProduct':
                if ((data.SelProductAlternateCode !== this.getControlValue('AlternateProductCode')) || (data.SelProductCode !== this.getControlValue('ProductComponentCode'))) {
                    this.disableControl('ProductComponentDesc', true);
                    this.setControlValue('AlternateProductCode', data.SelProductAlternateCode);
                    this.setControlValue('ProductComponentDesc', data.SelProductDesc);
                    this.setControlValue('ProductComponentCode', data.SelProductCode);

                    for (let productCode in this.dummyProductCodes) {
                        if (this.getControlValue('ProductComponentCode').toUpperCase() === productCode.toUpperCase()) {
                            this.disableControl('ProductComponentDesc', false);

                            this.setCompDescFocus.emit(true);
                        }
                    }
                }
                if (isEllipsisReturn) {
                    this.uiForm.controls['AlternateProductCode'].markAsDirty();
                }
                break;
        }
    }

    public keyedTimeOnChange(): void {
        this.defaultStandardDuration();
    }

    public activityStatusSelectOnChange(): void {
        if (this.currentMode === MntConst.eModeAdd || this.currentMode === MntConst.eModeUpdate) {
            let activityStatusNewOrNotProcessed: boolean = ((this.getControlValue('ActivityStatusSelect') === 'NotProcessed') || (this.getControlValue('ActivityStatusSelect') === 'New'));

            this.pageParams.isContractNumberRequired = activityStatusNewOrNotProcessed;
            this.pageParams.isPremiseNumberRequired = activityStatusNewOrNotProcessed;
            this.pageParams.isProductCodeRequired = activityStatusNewOrNotProcessed;
            this.pageParams.isVisitTypeCodeRequired = activityStatusNewOrNotProcessed;
            this.pageParams.isPlanVisitNumberRequired = activityStatusNewOrNotProcessed;

            this.setRequiredStatus('ContractNumber', this.pageParams.isContractNumberRequired);
            this.setRequiredStatus('PremiseNumber', this.pageParams.isPremiseNumberRequired);
            this.setRequiredStatus('ProductCode', this.pageParams.isProductCodeRequired);
            this.setRequiredStatus('PlanVisitNumber', this.pageParams.isPlanVisitNumberRequired);
            this.setRequiredStatus('VisitTypeCode', this.pageParams.isVisitTypeCodeRequired);
            this.dropdownConfig.visitType.isRequired = this.pageParams.isVisitTypeCodeRequired;

            this.dropdownConfig.visitType.triggerValidate = false;
        }
    }

    public contractPremiseOnBlur(type: any): void {
        if (this.currentMode === MntConst.eModeAdd) {
            switch (type) {
                case 'contract':
                    if (!this.getControlValue('ContractNumber')) {
                        this.setControlValue('ContractName', '');
                    }
                    break;
                case 'premise':
                    if (!this.getControlValue('PremiseNumber')) {
                        this.setControlValue('PremiseName', '');
                    }
                    break;
            }

            this.findExistingPremiseVisit();
        }
    }

    public productOnBlur(): void {
        if (this.currentMode === MntConst.eModeAdd) {
            if (!this.getControlValue('ProductCode')) {
                this.setControlValue('ProductDesc', '');
                this.setControlValue('ServiceCoverNumber', '');
                this.setControlValue('ServiceCoverRowID', '');
            }
        }
        if (this.currentMode === MntConst.eModeUpdate) {
            this.setControlValue('ServiceCoverRowID', '');

            if (this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber')) {
                if (this.getControlValue('ServiceCoverNumber') === '') {
                    this.serviceCoverSearch.openModal();
                }
            }
        }
    }

    public contractPremiseProductOnChange(event: any, type?: any): void {
        if (this.currentMode === MntConst.eModeAdd || this.currentMode === MntConst.eModeUpdate) {
            this.setControlValue('ServiceCoverRowID', '');
            if (event.target.value === '') {
                this.setEllipsisValue();
                return;
            }

            this.populateDescriptions();

            if (type && type === 'ProductCode') {
                this.getDummyProductList();
            }
        }
    }

    public productComponentCodeOnChange(): void {
        if (this.getControlValue('ProductComponentCode')) {
            this.disableControl('ProductComponentDesc', true);
            for (let productCode in this.dummyProductCodes) {
                if (this.getControlValue('ProductComponentCode').toUpperCase() === productCode.toUpperCase()) {
                    this.disableControl('ProductComponentDesc', false);

                    this.setCompDescFocus.emit(true);
                }
            }
        } else {
            this.disableControl('ProductComponentDesc', true);
            this.setControlValue('ProductComponentDesc', '');
            this.setControlValue('AlternateProductCode', '');
        }
    }

    public alternateProductCodeOnChange(): void {
        if (this.getControlValue('AlternateProductCode')) {
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');

            let bodyParams: any = {
                AlternateProductCode: this.getControlValue('AlternateProductCode')
            };
            bodyParams[this.serviceConstants.Function] = 'GetAlternateProductValues';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }

                    this.setControlValue('ProductComponentCode', data.ProductComponentCode);
                    this.setControlValue('ProductComponentDesc', data.ProductComponentDesc);

                    this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'ProductComponentCode', false);
                    this.disableControl('ProductComponentDesc', true);

                    for (let productCode in this.dummyProductCodes) {
                        if (this.getControlValue('ProductComponentCode').toUpperCase() === productCode.toUpperCase()) {
                            this.disableControl('ProductComponentDesc', false);

                            this.setCompDescFocus.emit(true);
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    public renderTab(tabindex: number): void {
        switch (tabindex) {
            case 1:
                this.uiDisplay.tab.tab1.active = true;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = false;
                break;
            case 2:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = true;
                this.uiDisplay.tab.tab3.active = false;
                break;
            case 3:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = true;
                break;
        }
    }

    public onOptionChange(data: string): void {
        this.pageParams.pageMode = this.currentMode;
        if (this.isRecordFound) {
            switch (data) {
                case 'Prep':
                    this.navigate('PDAICABSActivity', InternalSearchModuleRoutes.ICABSSEPDAICABSPREPUSEDSEARCH,
                        {
                            PDAICABSActivity: this.pageParams.PDAICABSActivityRowID
                        });
                    break;
                case 'Infest':
                    this.navigate('PDAICABSActivity', InternalSearchModuleRoutes.ICABSSEPDAICABSINFESTATIONSEARCH,
                        {
                            PDAICABSActivityRowID: this.pageParams.PDAICABSActivityRowID
                        });
                    break;
                case 'Recommendation':
                    this.navigate('PDAICABSActivity', InternalGridSearchServiceModuleRoutes.ICABSARECOMMENDATIONGRID.URL_1,
                        {
                            parentMode: 'PDAICABSActivity',
                            ContractNumber: this.getControlValue('ContractNumber'),
                            ContractName: this.getControlValue('ContractName'),
                            PremiseNumber: this.getControlValue('PremiseNumber'),
                            PremiseName: this.getControlValue('PremiseName'),
                            ProductCode: this.getControlValue('ProductCode'),
                            ProductDesc: this.getControlValue('ProductDesc'),
                            PDAVisitRef: this.getControlValue('PDAVisitRef'),
                            EmployeeCode: this.getControlValue('EmployeeCode'),
                            ServiceDateFrom: this.getControlValue('ServiceDateStart'),
                            ServiceDateTo: this.getControlValue('ServiceDateStart')
                        });
                    break;
            }
        } else {
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.noRecordSelected));
        }
    }

    public cmdCompReplacementOnClick(): void {
        this.isBtnCreateReplacement = true;

        let query: any = {};
        query['ProductCode'] = this.getControlValue('ProductComponentCode');
        query[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        let queryData = [
            {
                'table': 'Product',
                'query': query,
                'fields': ['ComponentTypeCode']
            }
        ];
        this.LookUp.lookUpPromise(queryData)
            .then(data => {
                if (data && data[0][0]) {
                    this.setControlValue('ComponentTypeCode', data[0][0].ComponentTypeCode);
                    this.navigate('PDAICABSActivity', InternalMaintenanceSalesModuleRoutes.ICABSASERVICECOVERCOMPONENTREPLACEMENT, {
                        'ProductComponentCode': this.getControlValue('ProductComponentCode'),
                        'ProductComponentRemoved': this.getControlValue('ProductComponentRemoved'),
                        'ProductComponentRemDesc': this.getControlValue('ProductComponentRemDesc'),
                        'PDAEmployeeCode': this.getControlValue('EmployeeCode'),
                        'PDAVisitRef': this.getControlValue('PDAVisitRef'),
                        'ComponentTypeCode': this.getControlValue('ComponentTypeCode')
                    });
                }
            });

        if (this.getControlValue('ComponentReplacementNumber')) {
            if (Number(this.getControlValue('ComponentReplacementNumber')) !== 0) {
                //this.saveRecord();

                this.pageParams.isCmdCompReplacementVisible = false;
            }
        }
        this.isBtnCreateReplacement = false;
    }

    public onServiceCoverModalClose(): void {
        if (this.isFetchSCNumber) {
            this.isFetchSCNumber = false;
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveRecord.bind(this)));
        }
    }

    public visitTypeSelectedValue(data: any): void {
        this.dropdownConfig.visitType.triggerValidate = false;
        this.uiForm.controls['VisitTypeCode'].markAsDirty();
        this.setControlValue('VisitTypeCode', data.VisitTypeCode);
        this.setControlValue('VisitTypeDesc', data.VisitTypeDesc);
        if (this.pageParams.isSCServiceCoverDispLevelEnabled && data.VisitTypeCode) {
            this.displayComponentReplacement();
        } else {
            this.setControlValue('ComponentReplaceRequest', false);
            this.pageParams.isCmdCompReplacementVisible = false;
            this.pageParams.isComponentRemovedVisible = false;
            this.pageParams.isComponentInstalledVisible = false;
        }
    }

    public onManualVisitDataReceived(data: any): void {
        this.setControlValue('ManualVisitReasonCode', data.ManualVisitReasonCode);
        this.setControlValue('ManualVisitReasonDesc', data.ManualVisitReasonDesc);
    }

    public onNoSignatureManualDataRecieved(data: any): void {
        this.setControlValue('NoSignatureManualNumber', data.ReasonNumber);
        this.setControlValue('NoSignatureManualDesc', data.ReasonDesc);
    }

    public onServiceManualDataReceived(data: any): void {
        this.setControlValue('NoServiceReasonCode', data.NoServiceReasonCode);
        this.setControlValue('NoServiceReasonDesc', data.NoServiceReasonDesc);
    }

    public onPremiseManualDataReceived(data: any): void {
        this.setControlValue('NoPremiseVisitManualCode', data.NoServiceReasonCode);
        this.setControlValue('NoPremiseVisitManualDesc', data.NoServiceReasonDesc);
    }

    /***Tab Focus */
    //On Focus on last element
    public focusSave(obj: any): void {
        if (obj.relatedTarget || obj.keyCode === 9) {
            let currtab = this.getCurrentActiveTab();
            let focustab = this.getNextActiveTab(currtab);
            if (currtab !== focustab) {
                this.tabFocus(focustab);
                this.focusFirstField();
            } else {
                this.save.nativeElement.focus();
            }
        }
    }

    public getCurrentActiveTab(): any {
        let i = 0;
        for (let tab in this.uiDisplay.tab) {
            if (tab !== '') {
                i++;
                if (this.uiDisplay.tab[tab].active) {
                    this.currentTab = i;
                    return i;
                }
            }
        }
    }

    public getNextActiveTab(tabindex: number): any {
        let i = 0;
        for (let tab in this.uiDisplay.tab) {
            if (tab !== '') {
                i++;
                if (this.uiDisplay.tab[tab].visible && i > tabindex && i <= this.tabLength) return i;
            }
        }
        return tabindex;
    }

    public tabFocus(tabIndex: number): void {
        this.currentTab = tabIndex;
        //Bug - unable to explicitly remove 'active' class as those are binded. hence below lines added
        let elem = this.navTabs.nativeElement.children;
        for (let i = 0; i < elem.length; i++) {
            if (this.utils.hasClass(elem[i], 'error')) {
                this.utils.removeClass(elem[i], 'active');
                this.renderer.setElementClass(this.tabContent.nativeElement, 'active', false);
            }
        }

        let i = 0;
        for (let tab in this.uiDisplay.tab) {
            if (tab !== '') {
                i++;
                this.uiDisplay.tab[tab].active = (i === tabIndex) ? true : false;
            }
        }

        //Failsafe
        this.utils.addClass(elem[tabIndex - 1], 'active');
        this.renderer.setElementClass(this.tabContent.nativeElement.children[tabIndex - 1], 'active', true);
        setTimeout(() => this.utils.makeTabsRed(), 200);
    }

    public focusFirstField(): any {
        let elem = this.tabContent.nativeElement.children[this.currentTab - 1];
        if (elem.querySelector('input')) elem.querySelector('input').focus();
        else if (elem.querySelector('textarea')) elem.querySelector('textarea').focus();
    }

}

