import { Component, OnInit, ViewChild, Injector, AfterContentInit } from '@angular/core';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { GenericSearch } from '@app/base/GenericSearch';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { IGenericSearchQuery } from '@app/base/GenericSearch';
import { InternalGridSearchServiceModuleRoutes } from '@app/base/PageRoutes';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { ModalAdvService } from '@shared/components/modal-adv/modal-adv.service';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSTeamMaintenance.html'
})

export class TeamMaintenanceComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('promptModalForSave') public promptModalForSave;
    @ViewChild('errorModal') public errorModal;
    @ViewChild('messageModal') public messageModal;
    @ViewChild('promptModal') public promptModal;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'AllowOtherTeamAssigneeCloseInd', type: MntConst.eTypeCheckBox },
        { name: 'AllowOtherTeamOwnerCloseInd', type: MntConst.eTypeCheckBox },
        { name: 'AssignOwnerToTeamAdministrator', type: MntConst.eTypeInteger, value: '0' },
        { name: 'ComplaintsTeamInd', type: MntConst.eTypeCheckBox },
        { name: 'FromEmailAddress', type: MntConst.eTypeTextFree },
        { name: 'KeyAccountInd', type: MntConst.eTypeCheckBox },
        { name: 'LedgerTeamInd', type: MntConst.eTypeCheckBox },
        { name: 'ResidentialTeamInd', type: MntConst.eTypeCheckBox },
        { name: 'SystemDefaultTeamInd', type: MntConst.eTypeCheckBox },
        { name: 'TeamClosureSecurityLevel', type: MntConst.eTypeInteger, value: '4' },
        { name: 'TeamID', type: MntConst.eTypeText },
        { name: 'TeamSystemName', type: MntConst.eTypeText, required: true },
        { name: 'TeamTelephone', type: MntConst.eTypeTextFree }
    ];

    public isDisabled: boolean = true;
    public pageMode = MntConst.eModeUpdate;
    public promptConfirmContent: string;
    public teamCodeValues: Array<any>;
    public promptContentDel: string = MessageConstant.Message.DeleteRecord;
    public genericSearch: GenericSearch;

    public gen: IGenericSearchQuery = {
        table: 'Team'
    };

    public TeamClosureValues: Array<Object> = [
        { text: 'Cannot Close Any Tickets', value: '1' },
        { text: 'Close Tickets From Home Branch Only', value: '2' },
        { text: 'Close Tickets Based Upon Branch Write Access Setup', value: '3' },
        { text: 'Close All Tickets', value: '4' }
    ];

    public OwnershipChangeValues: Array<Object> = [
        { text: 'Allow Employee To Keep Ownership', value: '0' },
        { text: 'Auto Assign Ownership To This Teams Administrator', value: '1' }
    ];

    public headerParams: any = {
        operation: 'System/iCABSSTeamMaintenance',
        module: 'team',
        method: 'ccm/admin'
    };

    constructor(injector: Injector,
        private modalAdvService: ModalAdvService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSTEAMMAINTENANCE;
        this.genericSearch = new GenericSearch(injector);
        this.pageTitle = 'Team Maintenance';
        this.browserTitle = 'Team Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.genericSearch.init(this.gen);
        this.updateDropdownValues();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (!this.isReturning()) {
            this['uiForm'].disable();
            this.disableControl('TeamID', false);
        } else {
            this.isDisabled = false;
            this['uiForm'].enable();
        }
    }

    private updateDropdownValues(): void {
        this.genericSearch.fetch().then((data) => {
            this.teamCodeValues = data.records;
        });
    }

    private saveRecord(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.pageMode === MntConst.eModeAdd ? 1 : 2);
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        let postObj: Object = {};

        postObj['TeamID'] = this.getControlValue('TeamID');
        postObj['TeamSystemName'] = this.getControlValue('TeamSystemName');
        postObj['TeamClosureSecurityLevel'] = this.getControlValue('TeamClosureSecurityLevel');
        postObj['AssignOwnerToTeamAdministrator'] = this.getControlValue('AssignOwnerToTeamAdministrator');
        postObj['SystemDefaultTeamInd'] = this.getControlValue('SystemDefaultTeamInd');
        postObj['KeyAccountInd'] = this.getControlValue('KeyAccountInd');
        postObj['ResidentialTeamInd'] = this.getControlValue('ResidentialTeamInd');
        postObj['ComplaintsTeamInd'] = this.getControlValue('ComplaintsTeamInd');
        postObj['LedgerTeamInd'] = this.getControlValue('LedgerTeamInd');
        postObj['AllowOtherTeamOwnerCloseInd'] = this.getControlValue('AllowOtherTeamOwnerCloseInd');
        postObj['AllowOtherTeamAssigneeCloseInd'] = this.getControlValue('AllowOtherTeamAssigneeCloseInd');
        postObj['FromEmailAddress'] = this.getControlValue('FromEmailAddress');
        postObj['TeamTelephone'] = this.getControlValue('TeamTelephone');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, postObj).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.fullError) {
                    this.displayMessage(data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                } else {
                    this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                    this.updateDropdownValues();
                }
            });
        this.pageMode = MntConst.eModeUpdate;
    }

    private deleteRecord(): void {
        this.teamCodeValues = this.teamCodeValues.filter((item) => item.TeamID !== this.getControlValue('TeamID'));

        if (this.pageMode === MntConst.eModeDelete) {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '3');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: Object = {};
            formData['TeamID'] = this.getControlValue('TeamID');

            this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.formPristine();
                    if (data.hasError) {
                        this.displayMessage(data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                        this.updateDropdownValues();
                    } else {
                        this.displayMessage(MessageConstant.Message.RecordDeletedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                        this.uiForm.reset();
                        this.setControlValue('AssignOwnerToTeamAdministrator', '0');
                        this.setControlValue('TeamClosureSecurityLevel', '4');
                        this['uiForm'].disable();
                        this.disableControl('TeamID', false);
                        this.isDisabled = true;
                    }
                });
        }
    }

    public onTeamSelect(): void {
        this.isDisabled = false;
        this['uiForm'].enable();
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: Object = {};
        formData['TeamID'] = this.getControlValue('TeamID');
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.displayMessage(data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                } else {
                    for (let i in data) {
                        if (data.hasOwnProperty(i)) {
                            this.setControlValue(i, data[i]);
                        }
                    }
                    this.uiForm.markAsPristine();
                }
            });
    }

    public onAddClick(): void {
        this.isDisabled = false;
        this['uiForm'].enable();
        this.setControlValue('AssignOwnerToTeamAdministrator', '0');
        this.setControlValue('TeamClosureSecurityLevel', '4');
        this.pageMode = MntConst.eModeAdd;
        this.setControlValue('TeamID', '');
        this.setControlValue('TeamSystemName', '');
        this.setControlValue('SystemDefaultTeamInd', '');
        this.setControlValue('ResidentialTeamInd', '');
        this.setControlValue('ComplaintsTeamInd', '');
        this.setControlValue('LedgerTeamInd', '');
        this.setControlValue('KeyAccountInd', '');
        this.setControlValue('AllowOtherTeamOwnerCloseInd', '');
        this.setControlValue('AllowOtherTeamAssigneeCloseInd', '');
        this.setControlValue('FromEmailAddress', '');
        this.setControlValue('TeamTelephone', '');
    }

    public onCancelClick(): void {
        if (this.pageMode === MntConst.eModeUpdate) {
            const checkTeamID = this.getControlValue('TeamID');
            for (let item of this.teamCodeValues) {
                if (checkTeamID === item.TeamID) {
                    this.setControlValue('TeamSystemName', item.TeamSystemName);
                    this.setControlValue('TeamClosureSecurityLevel', item.TeamClosureSecurityLevel);
                    this.setControlValue('AssignOwnerToTeamAdministrator', item.AssignOwnerToTeamAdministrator);
                    this.setControlValue('SystemDefaultTeamInd', item.SystemDefaultTeamInd);
                    this.setControlValue('KeyAccountInd', item.KeyAccountInd);
                    this.setControlValue('ResidentialTeamInd', item.ResidentialTeamInd);
                    this.setControlValue('ComplaintsTeamInd', item.ComplaintsTeamInd);
                    this.setControlValue('LedgerTeamInd', item.LedgerTeamInd);
                    this.setControlValue('AllowOtherTeamOwnerCloseInd', item.AllowOtherTeamOwnerCloseInd);
                    this.setControlValue('AllowOtherTeamAssigneeCloseInd', item.AllowOtherTeamAssigneeCloseInd);
                    this.setControlValue('FromEmailAddress', item.FromEmailAddress);
                    this.setControlValue('TeamTelephone', item.TeamTelephone);
                }
            }
        }
        else {
            this.pageMode = MntConst.eModeUpdate;
            this.uiForm.reset();
            this.setControlValue('AssignOwnerToTeamAdministrator', '0');
            this.setControlValue('TeamClosureSecurityLevel', '4');
            this['uiForm'].disable();
            this.disableControl('TeamID', false);
            this.isDisabled = true;
        }
    }

    public confirmed(obj: any): void {
        if (this.pageMode === MntConst.eModeDelete) {
            this.deleteRecord();
        }
        else {
            this.saveRecord();
        }
    }

    public onDelete(): void {
        this.pageMode = MntConst.eModeDelete;
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteRecord.bind(this)));
        this.promptConfirmContent = MessageConstant.Message.DeleteRecord;
    }

    public onSaveClick(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.promptModal.show();
            this.promptConfirmContent = MessageConstant.Message.ConfirmRecord;
        }
    }

    public selectTeamMembers(): void {
        if (this.getControlValue('TeamID') !== '' && this.pageMode === MntConst.eModeUpdate) {
            this.uiForm.controls.TeamID.markAsPristine();
            this.navigate('AddTeam', InternalGridSearchServiceModuleRoutes.ICABSSTEAMUSERGRID);
        }
    }
}
