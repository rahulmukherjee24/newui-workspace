
/**
 * The SeServicePlanningDetailHgComponent component to manage service planing details add/update
 * @author icabs ui team
 * @version 1.0
 * @since   12/18/2017
 */
import { QueryParams } from '../../../shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy, ElementRef } from '@angular/core';

import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { AjaxConstant } from './../../../shared/constants/AjaxConstants';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';


@Component({
    templateUrl: 'iCABSCMTelesalesEntryOrderLineMaintenance.html'
})

//Class definition starts
export class TelesalesEntryOrderLineMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('routeAwayComponent') public routeAwayComponent;

//Private properties of component starts
    private queryParams: any = {
        operation: 'ContactManagement/iCABSCMTelesalesEntryOrderLineMaintenance',
        module: 'telesales',
        method: 'ccm/maintenance'
    };

//Public properties of component starts
    public pageId: string = '';
    public pageHeading: string = 'Telesales Order Line Details';
    public isRequesting: boolean = false;
    public fieldHidden = {
        'AgreedValueInclTax': false,
        'ListPriceInclTax': false

    };
    public btnDisable = {
        cmdSave: false,
        cmdCancel: false,
        cmdCancelOrder: false
    };
    public controls = [
        { name: 'ProductDescription', disabled: false, required: false, type: MntConst.eTypeTextFree, commonValidator: true },
        { name: 'StandardUnitPrice', disabled: false, required: false, type: MntConst.eTypeDecimal2, commonValidator: true },
        { name: 'OrderQuantity', disabled: false, required: false, type: MntConst.eTypeDecimal2, commonValidator: true },
        { name: 'ProductUnitPrice', disabled: false, required: false, type: MntConst.eTypeDecimal2, commonValidator: true },
        { name: 'DiscountPercentage', disabled: false, required: false, type: MntConst.eTypeDecimal2, commonValidator: true },
        { name: 'DiscountAmount', disabled: false, required: false, type: MntConst.eTypeDecimal2, commonValidator: true },
        { name: 'AgreedValue', disabled: false, required: false, type: MntConst.eTypeDecimal2, commonValidator: true },
        { name: 'ListPriceExclTax', disabled: false, required: false, type: MntConst.eTypeDecimal2, commonValidator: true },
        { name: 'AgreedValueInclTax', disabled: false, required: false, type: MntConst.eTypeDecimal2, commonValidator: true },
        { name: 'ListPriceInclTax', disabled: false, required: false, type: MntConst.eTypeDecimal2, commonValidator: true },
        { name: 'OLCreatedDate', disabled: false, required: false, type: MntConst.eTypeDate, commonValidator: true },
        { name: 'OLCreatedTime', disabled: false, required: false, type: MntConst.eTypeTime, commonValidator: true },
        { name: 'OLCreatedUserName', disabled: false, required: false, type: MntConst.eTypeTextFree, commonValidator: true },
        { name: 'OLCancelledDate', disabled: false, required: false, type: MntConst.eTypeDate, commonValidator: true },
        { name: 'OLCancelledTime', disabled: false, required: false, type: MntConst.eTypeTime, commonValidator: true },
        { name: 'OLCancelledUserName', disabled: false, required: false, type: MntConst.eTypeTextFree, commonValidator: true },
        { name: 'OLStockDate', disabled: false, required: false, type: MntConst.eTypeDate, commonValidator: true },
        { name: 'OLStockTime', disabled: false, required: false, type: MntConst.eTypeTime, commonValidator: true },
        { name: 'OLDeliveredDate', disabled: false, required: false, type: MntConst.eTypeDate, commonValidator: true },
        { name: 'OLDeliveredTime', disabled: false, required: false, type: MntConst.eTypeTime, commonValidator: true },
        { name: 'OLSupplierID', disabled: false, required: false, type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'OLSupplierName', disabled: false, required: false, type: MntConst.eTypeTextFree, commonValidator: true },
        { name: 'OLStockRequestNumber', disabled: false, required: false, type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'TelesalesOrderNumber', disabled: false, required: false, type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'TelesalesOrderLineNumber', disabled: false, required: false, type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'CurrentCallLogID', disabled: false, required: false, value: '', type: MntConst.eTypeInteger, commonValidator: true }

    ];

//Class constructor definition
    constructor(injector: Injector, private el: ElementRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSCMTELESALESENTRYORDERLINEMAINTENANCE;
    }

//Lifecycle hooks methods starts
    ngOnInit(): void {
        super.ngOnInit();
        this.initData();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    /**
     * This method is used to initialize component data
     * @param void
     * @return void
     */

    private initData(): void {
        this.pageParams.lShowTaxDetails = false;
        this.pageParams.cCanDiscount = '';
        this.pageTitle = 'Telesales Entry - Order Line Maintenance';
        this.utils.setTitle(this.pageTitle);
        this.getRegistrySetting('Show_Tax_In_Telesales_Entry');
        this.setControlValue('TelesalesOrderNumber', this.riExchange.getParentHTMLValue('TelesalesOrderNumber'));
        this.setControlValue('TelesalesOrderLineNumber', this.riExchange.getParentHTMLValue('TelesalesOrderLineNumber'));
        this.setControlValue('CurrentCallLogID', this.riExchange.getParentHTMLValue('CurrentCallLogID'));
        this.fetchOrderLine();
    }

    /**
     * This method is used to fetch order line
     * @param void
     * @return void
     */
    private fetchOrderLine(): void {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject();
        postData['TelesalesOrderNumber'] = this.getControlValue('TelesalesOrderNumber');
        postData['TelesalesOrderLineNumber'] = this.getControlValue('TelesalesOrderLineNumber');
        postData['OrderQuantity'] = this.getControlValue('OrderQuantity');
        postData['DiscountPercentage'] = this.getControlValue('DiscountPercentage');
        postData['DiscountAmount'] = this.getControlValue('DiscountAmount');
        postData['AgreedValue'] = this.getControlValue('AgreedValue');
        postData[this.serviceConstants.Function] = 'fetchorderLine';
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(AjaxConstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                try {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                    } else {
                        this.setControlValue('OLCreatedDate', data.OLCreatedDate);
                        this.setControlValue('OLCreatedTime', data.OLCreatedTime);
                        this.setControlValue('OLCreatedUserName', data.OLCreatedUserName);
                        this.setControlValue('OLStockDate', data.OLStockDate);
                        this.setControlValue('OLStockTime', data.OLStockTime);
                        this.setControlValue('OLDeliveredDate', data.OLDeliveredDate);
                        this.setControlValue('OLDeliveredTime', data.OLDeliveredTime);
                        this.setControlValue('OLCancelledDate', data.OLCancelledDate);
                        this.setControlValue('OLCancelledTime', data.OLCancelledTime);
                        this.setControlValue('OLCancelledUserName', data.OLCancelledUserName);
                        this.setControlValue('OLSupplierID', data.OLSupplierID);
                        this.setControlValue('OLSupplierName', data.OLSupplierName);
                        this.setControlValue('OLStockRequestNumber', data.OLStockRequestNumber);
                        this.pageParams.cCanDiscount = data.CanDiscount;
                        this.btnDisable.cmdSave = true;
                        this.btnDisable.cmdCancelOrder = true;
                        this.btnDisable.cmdCancel = false;
                        this.disableUpdateFields(true);
                        if (data.CanUpdate === 'Y') {
                            this.btnDisable.cmdSave = false;
                            this.btnDisable.cmdCancel = false;
                            this.disableUpdateFields(false);
                            this.el.nativeElement.querySelector('#OrderQuantity').focus();
                        }
                        if (data.CanCancel === 'Y') {
                            this.btnDisable.cmdCancelOrder = false;
                        }
                        this.setControlValue('ProductDescription', data.ProductDescription);
                        this.setControlValue('StandardUnitPrice', data.StandardUnitPrice);
                        this.setControlValue('ProductUnitPrice', data.ProductUnitPrice);
                        this.setControlValue('OrderQuantity', data.OrderQuantity);
                        this.setControlValue('DiscountAmount', data.DiscountAmount);
                        this.setControlValue('DiscountPercentage', data.DiscountPercentage);
                        this.setControlValue('AgreedValue', data.AgreedValue);
                        this.setControlValue('AgreedValueInclTax', data.AgreedValueInclTax);
                        this.setControlValue('ListPriceExclTax', data.ListPriceExclTax);
                        this.setControlValue('ListPriceInclTax', data.ListPriceInclTax);
                    }

                } catch (error) {
                    this.logger.warn(error);
                }
                this.ajaxSource.next(AjaxConstant.COMPLETE);
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
                this.ajaxSource.next(AjaxConstant.COMPLETE);
            }
        );
    }

    /**
     * This method is used to disable update fields
     * @param lDisable checks disable is required or not
     * @return void
     */
    private disableUpdateFields(lDisable: boolean): void {
        if (lDisable) {
            this.disableControl('OrderQuantity', true);
            this.disableControl('DiscountPercentage', true);
            this.disableControl('DiscountAmount', true);
            this.disableControl('AgreedValue', true);
        } else {
            this.disableControl('OrderQuantity', false);
            if (this.pageParams.cCanDiscount === 'Y') {
                this.disableControl('DiscountPercentage', false);
                this.disableControl('DiscountAmount', false);
            }
            this.disableControl('AgreedValue', false);
        }
    }

    /**
     * This method is used to calculate all parameters based on inputs
     * @param cChangedField fieldname which is changed
     * @return void
     */
    private recalcValues(cChangedField: string): void {
        if (this.uiForm.controls['OrderQuantity'].valid && this.uiForm.controls['ProductUnitPrice'].valid
        && this.uiForm.controls['DiscountPercentage'].valid && this.uiForm.controls['DiscountAmount'].valid && this.uiForm.controls['AgreedValue'].valid) {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject();
        postData['TelesalesOrderNumber'] = this.getControlValue('TelesalesOrderNumber');
        postData['TelesalesOrderLineNumber'] = this.getControlValue('TelesalesOrderLineNumber');
        postData['FieldChanged'] = cChangedField;
        postData['OrderQuantity'] = this.getControlValue('OrderQuantity');
        postData['ProductUnitPrice'] = this.getControlValue('ProductUnitPrice');
        postData['DiscountPercentage'] = this.getControlValue('DiscountPercentage');
        postData['DiscountAmount'] = this.getControlValue('DiscountAmount');
        postData['AgreedValue'] = this.getControlValue('AgreedValue');
        postData[this.serviceConstants.Function] = 'RecalcValues';
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(AjaxConstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                try {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                    } else {
                        this.setControlValue('ProductUnitPrice', data.ProductUnitPrice);
                        this.setControlValue('DiscountPercentage', data.DiscountPercentage);
                        this.setControlValue('DiscountAmount', data.DiscountAmount);
                        this.setControlValue('AgreedValue', data.AgreedValue);
                        this.setControlValue('AgreedValueInclTax', data.AgreedValueInclTax);
                        this.setControlValue('ListPriceExclTax', data.ListPriceExclTax);
                        this.setControlValue('ListPriceInclTax', data.ListPriceInclTax);
                    }

                } catch (error) {
                    this.logger.warn(error);
                }
                this.ajaxSource.next(AjaxConstant.COMPLETE);
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
                this.ajaxSource.next(AjaxConstant.COMPLETE);
            }
        );
        }
    }

    /**
     * This method is used to initialize lShowTaxDetails
     * @param ipcRegKey is RegKey criteria
     * @return void
     */
    public getRegistrySetting(ipcRegKey: string): void {
        let setting: Array<any> = [
            {
                'table': 'BusinessRegistry',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode(),
                    'RegSection': 'Telesales',
                    'RegKey': ipcRegKey
                },
                'fields': ['EffectiveDate', 'RegValue']
            }
        ];
        this.LookUp.lookUpRecord(setting).subscribe((data) => {
            let cdate: Date = new Date(),edateArr: Array<any> = [], edate: Date;
            if (data[0]) {
                for (let j = data[0].length - 1;j >= 0;j--) {
                edateArr = data[0][j].EffectiveDate.split('-');
                edate = new Date(edateArr[0], edateArr[1] - 1, edateArr[2]);
                if (edate.getTime() < cdate.getTime()) {
                    this.pageParams.lShowTaxDetails = data[0][j].RegValue;
                    break;
                }
                }
            } else {
               this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.recordNotFound));
            }
             this.setDisplayFields();
        });
    }

    /**
     * This method is used to set display fields
     * @param void
     * @return void
     */
    public setDisplayFields(): void {
        if (this.pageParams.lShowTaxDetails) {
            this.fieldHidden.AgreedValueInclTax = false;
            this.fieldHidden.ListPriceInclTax = false;
        } else {
            this.fieldHidden.AgreedValueInclTax = true;
            this.fieldHidden.ListPriceInclTax = true;
        }
        this.disableControl('ProductDescription', true);
        this.disableControl('StandardUnitPrice', true);
        this.disableControl('ListPriceInclTax', true);
        this.disableControl('OLCreatedDate', true);
        this.disableControl('OLCreatedTime', true);
        this.disableControl('OLCreatedUserName', true);
        this.disableControl('OLStockDate', true);
        this.disableControl('OLStockTime', true);
        this.disableControl('OLDeliveredTime', true);
        this.disableControl('OLDeliveredDate', true);
        this.disableControl('OLCancelledDate', true);
        this.disableControl('OLCancelledTime', true);
        this.disableControl('OLCancelledUserName', true);
        this.disableControl('OLSupplierID', true);
        this.disableControl('OLSupplierName', true);
        this.disableControl('OLStockRequestNumber', true);
        this.disableControl('AgreedValueInclTax', true);
        this.disableControl('ListPriceExclTax', true);
    }

    /**
     * This method is used to look up service call
     * @param data parameters array,maxresults how many records required
     * @return void
     */
    public lookUpRecord(data: any, maxresults: number): any {
        let queryLookUp: QueryParams = this.getURLSearchParamObject();
        queryLookUp.set(this.serviceConstants.Action, '0');
        if (maxresults) {
            queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        }
        return this.httpService.lookUpRequest(queryLookUp, data);
    }

    /**
     * This method is used to recalculate after changing control value
     * @param void
     * @return void
     */
    public orderQuantityOnChange(): void {
        this.recalcValues('OrderQuantity');
    }

    /**
     * This method is used to recalculate after changing control value
     * @param void
     * @return void
     */
    public productUnitPriceOnChange(): void {
        this.recalcValues('ProductUnitPrice');

    }

    /**
     * This method is used to recalculate after changing control value
     * @param void
     * @return void
     */
    public discountPercentageOnChange(): void {
        this.recalcValues('DiscountPercentage');

    }

    /**
     * This method is used to recalculate after changing control value
     * @param void
     * @return void
     */
    public discountAmountOnChange(): void {
        this.recalcValues('DiscountAmount');

    }

    /**
     * This method is used to recalculate after changing control value
     * @param void
     * @return void
     */
    public agreedValueOnChange(): void {
        this.recalcValues('AgreedValue');

    }

    /**
     * This method is used to cancel order line
     * @param void
     * @return void
     */
    public cancelOrderLine(): void {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject();
        postData['TelesalesOrderNumber'] = this.getControlValue('TelesalesOrderNumber');
        postData['TelesalesOrderLineNumber'] = this.getControlValue('TelesalesOrderLineNumber');
        postData['CurrentCallLogID'] = this.getControlValue('CurrentCallLogID');
        postData[this.serviceConstants.Function] = 'CancelOrderLine';
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(AjaxConstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                try {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                    } else {
                         this.cancelClick();
                    }

                } catch (error) {
                    this.logger.warn(error);
                }
                this.ajaxSource.next(AjaxConstant.COMPLETE);
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
                this.ajaxSource.next(AjaxConstant.COMPLETE);
            }
        );
    }

    /**
     * This method is used to save order line
     * @param void
     * @return void
     */
    public saveOrderLine(): void {
        if (this.uiForm.valid) {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject();
        postData['TelesalesOrderNumber'] = this.getControlValue('TelesalesOrderNumber');
        postData['TelesalesOrderLineNumber'] = this.getControlValue('TelesalesOrderLineNumber');
        postData['CurrentCallLogID'] = this.getControlValue('CurrentCallLogID');
        postData['OrderQuantity'] = this.getControlValue('OrderQuantity');
        postData['DiscountPercentage'] = this.getControlValue('DiscountPercentage');
        postData['DiscountAmount'] = this.getControlValue('DiscountAmount');
        postData['AgreedValue'] = this.getControlValue('AgreedValue');
        postData[this.serviceConstants.Function] = 'SaveOrderLine';
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(AjaxConstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                try {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                    } else {
                       this.cancelClick();
                    }

                } catch (error) {
                    this.logger.warn(error);
                }
                this.ajaxSource.next(AjaxConstant.COMPLETE);
            },
            (error) => {
               this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
               this.ajaxSource.next(AjaxConstant.COMPLETE);
            }
        );
        }
    }

    /**
     * This method is used to call after click on cancel button
     * @param void
     * @return void
     */
    public cancelClick(): void {
        this.formPristine();
        this.location.back();
    }
}
