import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { DropdownStaticComponent } from './../../../shared/components/dropdown-static/dropdownstatic';
import { VariableService } from '../../../shared/services/variable.service';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSSNotificationGroupEmployeeMaintenance.html'
})

export class NotificationGroupEmployeeMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('employeeSelectFilter') employeeSelectFilter: DropdownStaticComponent;
    @ViewChild('notifyTypeFilter') notifyTypeFilter: DropdownStaticComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    private subscription: Subscription = new Subscription();
    private groupCodeValue: any = { NotifyGroupCode: '', NotifyGroupSystemDesc: '' };
    public selectedOccupation: number;
    public pageId: string = '';
    public isVisisbleOccupation: boolean = false;
    public isVisisbleNotification: boolean;
    public isVisisbleDelete: boolean;
    public mode = 'U';
    public isVisibleAdd: boolean = false;
    public isDisableButton: boolean = false;
    public notificationGroupEmployeeRowID: string;
    public isDeleteClicked: boolean = false;
    public displayFields: Array<string> = ['OccupationDesc'];
    public controls: Array<Object> = [
        { name: 'NotifyGroupCode', type: MntConst.eTypeCode },
        { name: 'NotifyGroupSystemDesc', type: MntConst.eTypeText },
        { name: 'NotifyContactList', type: MntConst.eTypeTextFree },
        { name: 'NotifyEmployeeTypeSelect', type: MntConst.eTypeTextFree },
        { name: 'NotifyTypeSelect', type: MntConst.eTypeCode },
        { name: 'NotifyType', type: MntConst.eTypeCode },
        { name: 'NotifyEmployeeType', type: MntConst.eTypeCode },
        { name: 'OccupationCode', type: MntConst.eTypeCode }
    ];
    public groupCodeColumns: Array<string> = ['NotifyGroupCode', 'NotifyGroupSystemDesc'];
    public groupCodeSearchDropdown: any = {
        operation: 'System/iCABSSNotificationGroupSearch',
        module: 'rules',
        method: 'ccm/search'
    };
    public groupCodeActive: any = { id: '', text: '' };
    public isGroupCodeDisabled: boolean = true;
    public staticDropdownParams: any = {
        employeeSelectFilter: {
            inputData: [
                { value: 1, text: 'Contact Assignee' }, { value: 2, text: 'Contact Assignee - Supervisor' },
                { value: 3, text: 'Contact Assignee - Supervisor Tree' }, { value: 4, text: 'Contact Creator' },
                { value: 5, text: 'Contact Creator - Supervisor' }, { value: 6, text: 'Contact Creator - Supervisor Tree' },
                { value: 7, text: 'Contact Owner' }, { value: 8, text: 'Contact Owner - Supervisor' },
                { value: 9, text: 'Contact Owner - Supervisor Tree' }, { value: 10, text: 'Contact Regarding Employee' },
                { value: 11, text: 'Contact Regarding Employee - Supervisor' }, { value: 12, text: 'Contact Regarding Employee - Supervisor Tree' },
                { value: 13, text: 'Contact Team Members' }, { value: 14, text: 'Contact Team Administrator' },
                { value: 15, text: 'Contact Team Administrator - Supervisor' }, { value: 16, text: 'Contact Team Administrator - Supervisor Tree' },
                { value: 17, text: 'Contact Usual Employee' }, { value: 18, text: 'Contact Usual Employee - Supervisor' },
                { value: 19, text: 'Contact Usual Employee - Supervisor Tree' }, { value: 20, text: 'Negotiating Branch - Branch Manager' },
                { value: 21, text: 'Negotiating Branch - Branch Manager - Supervisor' }, { value: 22, text: 'Negotiating Branch - Branch Manager - Supervisor Tree' },
                { value: 23, text: 'Negotiating Branch - Branch Secretary' }, { value: 24, text: 'Negotiating Branch - Branch Secretary - Supervisor' }, { value: 25, text: 'Negotiating Branch - Branch Secretary - Supervisor Tree' },
                { value: 26, text: 'Negotiating Branch - Regional Manager' }, { value: 27, text: 'Negotiating Branch - Regional Manager - Supervisor' },
                { value: 28, text: 'Negotiating Branch - Regional Manager - Supervisor Tree' }, { value: 29, text: 'Negotiating Employee' },
                { value: 30, text: 'Negotiating Employee - Supervisor' }, { value: 31, text: 'Negotiating Employee - Supervisor Tree' },
                { value: 32, text: 'Sales Employee' }, { value: 33, text: 'Sales Employee - Supervisor' },
                { value: 34, text: 'Sales Employee - Supervisor Tree' }, { value: 35, text: 'Servicing Branch - Branch Manager' },
                { value: 36, text: 'Servicing Branch - Branch Manager - Supervisor' }, { value: 37, text: 'Servicing Branch - Branch Manager - Supervisor Tree' },
                { value: 38, text: 'Servicing Branch - Branch Secretary' }, { value: 39, text: 'Servicing Branch - Branch Secretary - Supervisor' },
                { value: 40, text: 'Servicing Branch - Branch Secretary - Supervisor Tree' }, { value: 41, text: 'Servicing Branch - Regional Manager' },
                { value: 42, text: 'Servicing Branch - Regional Manager - Supervisor' }, { value: 43, text: 'Servicing Branch - Regional Manager - Supervisor Tree' },
                { value: 44, text: 'Technician Employee' }, { value: 45, text: 'Technician Employee - Supervisor' },
                { value: 46, text: 'Technician Employee - Supervisor Tree' }, { value: 47, text: 'Specific Employee Code List' },
                { value: 48, text: 'Specific Email Address List' }, { value: 49, text: 'Specific SMS Address List' },
                { value: 50, text: 'Account Owner Employee' }, { value: 51, text: 'Account Owner Employee - Supervisor' },
                { value: 52, text: 'Account Owner Employee - Supervisor Tree' }, { value: 53, text: 'Occupation - Negotiating Branch' },
                { value: 54, text: 'Occupation - Negotiating Branch - Supervisor' }, { value: 55, text: 'Occupation - Negotiating Branch - Supervisor Tree' },
                { value: 56, text: 'Occupation - Servicing Branch' }, { value: 57, text: 'Occupation - Servicing Branch - Supervisor' },
                { value: 58, text: 'Occupation - Servicing Branch - Supervisor Tree' }, { value: 59, text: 'Occupation - Negotiating Region' },
                { value: 60, text: 'Occupation - Negotiating Region - Supervisor' }, { value: 61, text: 'Occupation - Negotiating Region - Supervisor Tree' },
                { value: 62, text: 'Occupation - Servicing Region' }, { value: 63, text: 'Occupation - Servicing Region - Supervisor' },
                { value: 64, text: 'Occupation - Servicing Region - Supervisor Tree' }, { value: 65, text: 'Occupation - Business' }, { value: 66, text: 'Occupation - Business - Supervisor' },
                { value: 67, text: 'Occupation - Business - Supervisor Tree' }, { value: 68, text: 'Premise - Telesales Employee' },
                { value: 69, text: 'Premise - Telesales Employee - Supervisor' }, { value: 70, text: 'Premise - Telesales Employee - Supervisor Tree' }
            ],
            isDisbaled: false
        },
        notifyTypeFilter: {
            inputData: [
                { value: 0, text: 'None' },
                { value: 1, text: 'Email' },
                { value: 2, text: 'SMS' },
                { value: 3, text: 'Email & SMS' }
            ],
            isDisbaled: false
        },
        occupationFilter: {
            inputData: [{ value: 0, text: 'None' }],
            isDisbaled: false
        }
    };
    public queryParams: any = {
        save: {
            operation: 'System/iCABSSNotificationGroupEmployeeMaintenance',
            module: 'rules',
            method: 'ccm/admin'
        },
        fetchOne: {
            operation: 'System/iCABSSNotificationGroupEmployeeMaintenance',
            module: 'rules',
            action: 0,
            method: 'ccm/admin'
        },
        fetchTwo: {
            operation: 'System/iCABSSNotificationGroupEmployeeMaintenance',
            module: 'rules',
            action: 0,
            method: 'ccm/admin'
        },
        delete: {
            operation: 'System/iCABSSNotificationGroupEmployeeMaintenance',
            module: 'rules',
            action: 3,
            method: 'ccm/admin'
        }
    };
    constructor(injector: Injector, private variableService: VariableService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSNOTIFICATIONGROUPEMPLOYEEMAINTENANCE;
        this.pageTitle = this.browserTitle = 'Notification Group Employee Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    //This method will execute when page loads.It's also check for parent modes, depending on the modes it's perform specific tasks.
    private windowOnload(): void {
        this.dolookUpOccupation();
        this.riExchange.riInputElement.Disable(this.uiForm, 'NotifyEmployeeType');
        this.setControlValue('NotifyType', this.riExchange.getParentHTMLValue('NotifyType'));
        switch (this.parentMode) {
            case 'Update':
                this.mode = 'U';
                this.isVisisbleDelete = true;
                this.isVisibleAdd = true;
                this.notificationGroupEmployeeRowID = this.riExchange.getParentAttributeValue('NotifyEmployeeType');
                this.staticDropdownParams.employeeSelectFilter.isDisbaled = true;
                this.getDetailsByRowID();
                break;
            case 'Add':
                this.mode = 'A';
                this.setControlValue('NotifyGroupCode', (this.riExchange.getParentHTMLValue('NotifyGroupCode')) ? this.riExchange.getParentHTMLValue('NotifyGroupCode') : '');
                this.setControlValue('NotifyGroupSystemDesc', (this.riExchange.getParentHTMLValue('NotifyGroupSystemDesc')) ? this.riExchange.getParentHTMLValue('NotifyGroupCode') : '');
                this.groupCodeValue.NotifyGroupCode = this.getControlValue('NotifyGroupCode');
                this.groupCodeValue.NotifyGroupSystemDesc = this.getControlValue('NotifyGroupSystemDesc');
                this.groupCodeActive = { 'id': this.getControlValue('NotifyGroupCode'), 'text': this.getControlValue('NotifyGroupCode') + ' - ' + this.getControlValue('NotifyGroupSystemDesc') };
                break;
        }
    }
    //It's used to get details depending on the ROWID
    private getDetailsByRowID(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let searchForFetchOne = new QueryParams();
        searchForFetchOne.set(this.serviceConstants.Action, this.queryParams.fetchOne.action);
        searchForFetchOne.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        searchForFetchOne.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        searchForFetchOne.set('ROWID', this.notificationGroupEmployeeRowID);
        this.subscription.add(this.httpService.makeGetRequest(this.queryParams.fetchOne.method, this.queryParams.fetchOne.module, this.queryParams.fetchOne.operation, searchForFetchOne)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.notificationGroupEmployeeRowID = data.ttNotificationGroupEmployee;
                    if (data.NotifyGroupCode) {
                        this.getNotifyGroupCodeAndDescription(data.NotifyGroupCode);
                    }
                    this.setSelectedItemToDropdownStatic(this.employeeSelectFilter, this.staticDropdownParams.employeeSelectFilter.inputData, data.NotifyEmployeeType);
                    this.setSelectedItemToDropdownStatic(this.notifyTypeFilter, this.staticDropdownParams.notifyTypeFilter.inputData, data.NotifyType);
                    this.onDropdownStaticChange('employeeSelectFilter', data.NotifyEmployeeType);
                    if (data.NotifyContactList) {
                        this.setControlValue('NotifyContactList', data.NotifyContactList);
                    }
                    if (data.OccupationCode) {
                        this.selectedOccupation = data.OccupationCode;
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
            ));
    }

    //This method used to get groupcode value and description
    private getNotifyGroupCodeAndDescription(groupCode: string): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let searchForFetchTwo = new QueryParams();
        searchForFetchTwo.set(this.serviceConstants.Action, this.queryParams.fetchTwo.action);
        searchForFetchTwo.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        searchForFetchTwo.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        searchForFetchTwo.set('NotifyGroupCode', groupCode);
        this.subscription.add(this.httpService.makeGetRequest(this.queryParams.fetchTwo.method, this.queryParams.fetchTwo.module, this.queryParams.fetchTwo.operation, searchForFetchTwo)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.groupCodeValue.NotifyGroupCode = data.NotifyGroupCode;
                    this.groupCodeValue.NotifyGroupSystemDesc = data.NotifyGroupSystemDesc;
                    this.groupCodeActive = { 'id': data.NotifyGroupCode, 'text': data.NotifyGroupCode + ' - ' + data.NotifyGroupSystemDesc };
                    this.formPristine();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
            ));
    }

    //This methode used to sort dropdown value.
    private sortDropDownData(filterTypeData: Array<any>): void {
        filterTypeData.sort(function (elementOne: any, elementTwo: any): number {
            if (elementOne.text < elementTwo.text) {
                return -1;
            } else if (elementOne.text > elementTwo.text) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    //This method used to set selection for dropdown value
    private setSelectedItemToDropdownStatic(dropdownObject: DropdownStaticComponent, dataArray: any, value: any): void {
        if (dropdownObject && value >= 0) {
            let index: number = this.functiontofindIndexByKeyValue(dataArray, value);
            dropdownObject.updateSelectedItem(index);
        }
    }

    //This lookup method used to get occupation details
    private dolookUpOccupation(): void {
        let lookupIP: any = [
            {
                'table': 'Occupation',
                'query': {
                },
                'fields': ['OccupationCode', 'OccupationDesc']
            }
        ];
        this.lookupDetails(lookupIP);
    }

    //This lookup method used to get occupation details
    private lookupDetails(query: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(query).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data[0] && data[0].length > 0) {
                this.staticDropdownParams.occupationFilter.inputData = this.changeKey({ first: 'OccupationCode', second: 'OccupationDesc' }, { first: 'value', second: 'text' }, data[0]);
                this.sortDropDownData(this.staticDropdownParams.employeeSelectFilter.inputData);
                this.sortDropDownData(this.staticDropdownParams.notifyTypeFilter.inputData);
                this.setSelectedItemToDropdownStatic(this.employeeSelectFilter, this.staticDropdownParams.employeeSelectFilter.inputData, 1);
                this.setSelectedItemToDropdownStatic(this.notifyTypeFilter, this.staticDropdownParams.notifyTypeFilter.inputData, 3);
                this.setControlValue('NotifyEmployeeType', 1);
                this.setControlValue('NotifyType', 3);
                this.setControlValue('OccupationCode', 11);
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error));
        });
    }

    //This method is used to show the conformation model before save or update.
    private proceedWithSaveConfirmation(): void {
        if (this['uiForm'].valid) {
            if (this.getControlValue('NotifyContactList').length > 0) {
                this.validateContractList();
            }
            else {
                if (this.mode === 'A') {
                    let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveAddedData.bind(this));
                    this.modalAdvService.emitPrompt(modalVO);
                }
                else if (this.mode === 'U') {
                    let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveUpdatedData.bind(this));
                    this.modalAdvService.emitPrompt(modalVO);
                }
            }
        }
    }

    //This methode use to save newly added valiue
    private saveAddedData(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '1');
        let postParams: any = {};
        postParams['NotifyGroupCode'] = this.getControlValue('NotifyGroupCode');
        postParams['NotifyEmployeeType'] = this.getControlValue('NotifyEmployeeType');
        if (this.getControlValue('NotifyEmployeeType') && this.getControlValue('NotifyEmployeeType') >= 53 && this.getControlValue('NotifyEmployeeType') <= 67) {
            postParams['OccupationCode'] = this.getControlValue('OccupationCode');
        }
        if (this.getControlValue('NotifyContactList')) {
            postParams['NotifyContactList'] = this.getControlValue('NotifyContactList');
        }
        postParams['NotifyType'] = this.getControlValue('NotifyType');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.subscription.add(this.httpService.makePostRequest(this.queryParams.save.method, this.queryParams.save.module, this.queryParams.save.operation, postSearchParams, postParams)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                else {
                    this.formPristine();
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                    this.notificationGroupEmployeeRowID = data.ttNotificationGroupEmployee;
                    this.mode = 'U';
                    this.staticDropdownParams.employeeSelectFilter.isDisbaled = true;
                    this.isVisisbleDelete = true;
                    this.isVisibleAdd = true;
                    this.isDisableButton = false;
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            }));
    }

    //This methode use to save updated valiue
    private saveUpdatedData(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '2');
        let postParams: any = {};
        postParams['ROWID'] = this.notificationGroupEmployeeRowID;
        postParams['NotifyType'] = this.getControlValue('NotifyType');
        if (this.getControlValue('NotifyEmployeeType') && this.getControlValue('NotifyEmployeeType') >= 53 && this.getControlValue('NotifyEmployeeType') <= 67) {
            postParams['OccupationCode'] = this.getControlValue('OccupationCode');
        }
        if (this.getControlValue('NotifyContactList')) {
            postParams['NotifyContactList'] = this.getControlValue('NotifyContactList');
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.subscription.add(this.httpService.makePostRequest(this.queryParams.save.method, this.queryParams.save.module, this.queryParams.save.operation, postSearchParams, postParams)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                    this.formPristine();
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            }));
    }

    //This methode use to validate contract list valiue
    private validateContractList(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams['Function'] = 'ValidateNotifyContactList';
        postParams['NotifyEmployeeType'] = this.getControlValue('NotifyEmployeeType');
        postParams['NotifyContactList'] = this.getControlValue('NotifyContactList');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.subscription.add(this.httpService.makePostRequest(this.queryParams.save.method, this.queryParams.save.module, this.queryParams.save.operation, postSearchParams, postParams)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                else {
                    if (data.ErrorMessage) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.ErrorMessage));
                    }
                    else {
                        if (this.mode === 'A') {
                            let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveAddedData.bind(this));
                            this.modalAdvService.emitPrompt(modalVO);
                        }
                        else {
                            let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveUpdatedData.bind(this));
                            this.modalAdvService.emitPrompt(modalVO);
                        }

                    }

                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            }));
    }

    //This methode use to delete selected value
    private deleteSavedData(): void {
        let delSearchParams = this.getURLSearchParamObject();
        delSearchParams.set(this.serviceConstants.Action, this.queryParams.delete.action);
        let postParams: any = {};
        postParams['ROWID'] = this.notificationGroupEmployeeRowID;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.subscription.add(this.httpService.makePostRequest(this.queryParams.delete.method, this.queryParams.delete.module, this.queryParams.delete.operation, delSearchParams, postParams)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data['hasError']) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullerror']));
                    return;
                }
                let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordDeleted);
                this.modalAdvService.emitMessage(modalVO);
                this.groupCodeActive = { 'id': '', 'text': '' };
                this.isVisibleAdd = true;
                this.isDisableButton = true;
                this.isDeleteClicked = true;
                this.staticDropdownParams.notifyTypeFilter.isDisbaled = true;

            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.errorService.emitError(error);
            }));
    }

    //This method used to restore the window to default behaviour
    private restoreWindow(): void {
        this.dolookUpOccupation();
        this.mode = 'U';
        this.staticDropdownParams.employeeSelectFilter.isDisbaled = true;
        this.getDetailsByRowID();
    }

    //This method use to perform specific task when Add button pressed
    private proceedAddOperation(): void {
        this.mode = 'A';
        this.changeVisibilityForElements(false);
        this.isVisisbleNotification = false;
        this.isVisisbleOccupation = false;
        this.groupCodeActive = { 'id': this.groupCodeValue.NotifyGroupCode, 'text': this.groupCodeValue.NotifyGroupCode + ' - ' + this.groupCodeValue.NotifyGroupSystemDesc };
        this.setSelectedItemToDropdownStatic(this.employeeSelectFilter, this.staticDropdownParams.employeeSelectFilter.inputData, 1);
        this.setSelectedItemToDropdownStatic(this.notifyTypeFilter, this.staticDropdownParams.notifyTypeFilter.inputData, 3);

    }
    //This method use to change status for certain element and buttons
    private changeVisibilityForElements(status: boolean): void {
        this.isVisisbleDelete = status;
        this.isVisibleAdd = status;
        this.isDisableButton = status;
        this.staticDropdownParams.notifyTypeFilter.isDisbaled = status;
        this.staticDropdownParams.employeeSelectFilter.isDisbaled = status;
    }

    //This methode use set selected dropdown value
    public onDropdownStaticChange(type: string, value: number): void {
        this.uiForm.markAsDirty();
        switch (type) {
            case 'employeeSelectFilter':
                this.setControlValue('NotifyEmployeeType', value);
                this.isVisisbleOccupation = false;
                this.isVisisbleNotification = false;
                if (value >= 53 && value <= 67) {
                    this.isVisisbleOccupation = true;
                }
                if (value >= 47 && value <= 49) {
                    this.isVisisbleNotification = true;
                }
                break;
            case 'notifyTypeFilter':
                this.setControlValue('NotifyType', value);
                break;
            case 'occupationDropDown':
                this.setControlValue('OccupationCode', value);
                break;
        }
    }

    //This methode use get selected index value
    public functiontofindIndexByKeyValue(arraytosearch: Array<any>, valuetosearch: number): number {
        for (let i = 0; i < arraytosearch.length; i++) {
            if (arraytosearch[i].value === valuetosearch) {
                return i;
            }
        }
        return null;
    }

    //This method used to show deleteion popup conformation
    public proceedWithDeleteConfirmation(): void {
        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteSavedData.bind(this));
        this.modalAdvService.emitPrompt(modalVO);
    }

    //This methode get executed when any button click on UI
    public buttonClicked(type: string): void {
        switch (type) {
            case 'cancel':
                if (this.parentMode === 'Add') {
                    event.preventDefault();
                    this.variableService.setBackClick(true);
                    this.location.back();
                }
                else if (this.parentMode === 'Update') {
                    if (this.isDeleteClicked) {
                        this.formPristine();
                        this.groupCodeActive = { 'id': '', 'text': '' };
                        this.changeVisibilityForElements(true);
                    }
                    else {
                        this.formPristine();
                        this.restoreWindow();
                        this.mode = 'U';
                        this.isVisisbleDelete = true;
                        this.isVisibleAdd = true;
                    }
                }
                break;
            case 'save':
                this.proceedWithSaveConfirmation();
                break;
            case 'delete':
                this.proceedWithDeleteConfirmation();
                break;
            case 'add':
                this.proceedAddOperation();
                break;
        }
    }

    //This method used to set chnaged groupcode value
    public onGroupCodeDataReceived(event: any): void {
        this.setControlValue('NotifyGroupCode', event.NotifyGroupCode);
    }


    //This methode used to manupulate key value pairing
    public changeKey(originalKey: Object, newKey: Object, arr: any): Array<any> {
        let newArr = [];
        for (let i = 0; i < arr.length; i++) {
            let obj = arr[i];
            obj[newKey['first']] = obj[originalKey['first']];
            obj[newKey['second']] = obj[originalKey['second']];
            delete (obj[originalKey['first']]);
            delete (obj[originalKey['second']]);
            newArr.push(obj);
        }
        return newArr;
    }


}
