import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { PreparationSearchComponent } from '../search/iCABSBPreparationSearch.component';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { InternalGridSearchModuleRoutes } from './../../base/PageRoutes';
import { CommonDropdownComponent } from '../../../shared/components/common-dropdown/common-dropdown.component';

@Component({
    templateUrl: 'iCABSBPreparationMaintenance.html'
})

export class PreparationMaintenanceComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('losSearchDropDown') losSearchDropDown: CommonDropdownComponent;
    @ViewChild('promptModal') public promptConfirmModal;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'PrepCode', type: MntConst.eTypeCode },
        { name: 'PrepDesc', required: true },
        { name: 'UsedFor' },
        { name: 'MeasureBy', required: true },
        { name: 'MaximumValue', type: MntConst.eTypeInteger },
        { name: 'PassToPDAInd' },
        { name: 'RejectPrepInd' },
        { name: 'PestNetInd' },
        { name: 'PestControlInd' },
        { name: 'EnvironmentalMetricInd' },
        { name: 'WeightingFactor', type: MntConst.eTypeDecimal2 },
        { name: 'SelEnvironmentUsage', value: '' },
        { name: 'EBCRInd' },
        { name: 'EPANumber' },
        { name: 'ItemCode' },
        { name: 'WHOClass' },
        { name: 'PreMixPercentage', type: MntConst.eTypeDecimal6 },
        { name: 'Percentage', type: MntConst.eTypeDecimal6 },
        { name: 'LowPercentage', type: MntConst.eTypeDecimal6 },
        { name: 'HighPercentage', type: MntConst.eTypeDecimal6 },
        { name: 'PreMixRatio', type: MntConst.eTypeDecimal6 },
        { name: 'HighMixRatio', type: MntConst.eTypeDecimal6 },
        { name: 'LowMixRatio', type: MntConst.eTypeDecimal6 },
        { name: 'ActiveIngredient' },
        { name: 'PrecautionaryStatements' },
        { name: 'CounterAgents' },
        { name: 'EnvironmentCode' },
        { name: 'LOSName', required: true },
        { name: 'PrepROWID' },
        { name: 'BtnAdd' },
        { name: 'BtnSubmit' },
        { name: 'BtnCancel' },
        { name: 'BtnDelete', disabled: true },
        { name: 'menu', disabled: true, value: '' }
    ];
    public inputParams: any = {
        method: 'service-delivery/admin',
        module: 'preps',
        operation: 'Business/iCABSBPreparationMaintenance'
    };
    public businessOption: Array<any> = [];
    public isSelEnvironmentUsageHide: boolean = false;
    public preparationSearchComponent = PreparationSearchComponent;
    public isDeleteShow: boolean = true;
    public isLOS: boolean = false;
    public isMeasureBy: boolean = false;
    public activeLos: any = {
        id: '',
        text: ''
    };
    public selEnvironmentUsageOption: Array<any> = [];
    public ellipsis = {
        PrepCode: {
            autoOpen: false,
            childConfigParams: {
                parentMode: 'LookUpDesc',
                isAddNewHidden: false
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: this.preparationSearchComponent,
            searchModalRoute: '',
            disabled: false
        }
    };
    public dropDown = {
        losSearch: {
            inputParams: {
                parentMode: 'LookUp',
                method: 'it-functions/search',
                module: 'structure',
                operation: 'System/iCABSSLOSSearch'

            }
        }
    };
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBPREPARATIONMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Preparation Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            this.formMode = this.c_s_MODE_UPDATE;
            this.enableFieldInUpdate();
            this.activeLos = {
                        id: this.pageParams.LOSName,
                        text: this.pageParams.LOSName + ' - ' + this.pageParams.LOSDesc
                    };
            this.hideShow();
            this.setControlValue('menu','');
            this.isLOS = true;
            this.isMeasureBy = true;
        } else {
            this.formMode = this.c_s_MODE_SELECT;
            this.disabledFieldInUpdate();
        }
        this.getEnvironmentUsage();
    }

    ngAfterViewInit(): void {
        this.ellipsis.PrepCode.autoOpen = !this.isReturning();
        this.losSearchDropDown.isDisabled = !this.isReturning();
        this.setControlValue('menu','');
    }

     ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private disabledFieldInUpdate(): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PrepCode', false);
        this.losSearchDropDown.isDisabled = true;
        this.disableControl('BtnAdd', false);
        if (this.formMode === this.c_s_MODE_SELECT) {
        this.disableControl('PrepCode',false);
        this.disableControls(['PrepCode', 'BtnAdd']);
        } else {
         this.disableControls(['PrepDesc', 'BtnAdd']);
        }
    }

    private enableFieldInUpdate(): void {
        this.disableControl('PrepCode', true);
        this.enableControls(['PrepCode']);
        this.losSearchDropDown.isDisabled = false;
    }

    private doAfterfetch(): void {
        this.hideShow();
    }

    private hideShow(): void {
        if (this.getControlValue('EnvironmentalMetricInd')) {
            this.isSelEnvironmentUsageHide = true;
        } else {
            this.isSelEnvironmentUsageHide = false;
        }
    }

    private setUiControl(data: any): void {
        if (data) {
            this.enableFieldInUpdate();
        }
        this.setControlValue('PrepDesc', data.PrepDesc);
        if (this.formMode !== this.c_s_MODE_ADD) {
            this.setControlValue('LOSName', data.LOSCode);
            this.setControlValue('MeasureBy', data.MeasureBy);
            this.setControlValue('UsedFor', data.UsedFor);
            this.setControlValue('PassToPDAInd', data.PassToPDAInd);
            this.setControlValue('RejectPrepInd', data.RejectPrepInd);
            this.setControlValue('MaximumValue', data.MaximumValue);
            this.setControlValue('PestNetInd', data.PestNetInd);
            this.setControlValue('PestControlInd', data.PestControlInd);
            this.setControlValue('EnvironmentalMetricInd', data.EnvironmentalMetricInd);
            this.setControlValue('EnvironmentCode', data.EnvironmentCode);
            this.setControlValue('SelEnvironmentUsage', data.EnvironmentCode);
            this.setControlValue('WeightingFactor', data.WeightingFactor);
            this.setControlValue('EBCRInd', data.EBCRInd);
            this.setControlValue('EPANumber', data.EPANumber);
            this.setControlValue('Percentage', data.Percentage);
            this.setControlValue('HighMixRatio', data.HighMixRatio);
            this.setControlValue('HighPercentage', data.HighPercentage);
            this.setControlValue('LowPercentage', data.LowPercentage);
            this.setControlValue('LowMixRatio', data.LowMixRatio);
            this.setControlValue('PreMixPercentage', data.PreMixPercentage);
            this.setControlValue('PreMixRatio', data.PreMixRatio);
            this.setControlValue('ItemCode', data.ItemCode);
            this.setControlValue('ActiveIngredient', data.ActiveIngredient);
            this.setControlValue('WHOClass', data.WHOclass);
            this.setControlValue('PrecautionaryStatements', data.PrecautionaryStatements);
            this.setControlValue('CounterAgents', data.CounterAgents);
            this.setControlValue('PrepROWID', data.ttPrep);
            for (let i = 0; i < Object.keys(this.uiForm.controls).length; i++) {
                this.pageParams[Object.keys(this.uiForm.controls)[i]] = this.getControlValue(Object.keys(this.uiForm.controls)[i]);
            }
            this.getLOSName();
        }
    }

    private getLOSName(): void {
        if (this.getControlValue('LOSName')) {
            let lookupIP = [
                {
                    'table': 'LineOfService',
                    'query': {
                        'BusinessCode': this.businessCode(),
                        'LOSCode': this.getControlValue('LOSName')
                    },
                    'fields': ['LOSName']
                }
            ];
            this.LookUp.lookUpPromise(lookupIP).then((data) => {
                let record = data[0];
                if (record.length > 0) {
                    this.activeLos = {
                        id: this.getControlValue('LOSName'),
                        text: this.getControlValue('LOSName') + ' - ' + record[0].LOSName
                    };
                    this.pageParams.LOSDesc = record[0].LOSName;
                } else {
                    this.activeLos = {
                        id: '',
                        text: ''
                    };
                }
            });
        }
    }

    private getEnvironmentUsage(): void {
         let lookupIP = [
                {
                    'table': 'EnvironmentalUsage',
                    'query': {
                        'BusinessCode': this.businessCode()
                    },
                    'fields': ['EnvironmentCode','EnvironmentDesc']
                }
            ];
            this.LookUp.lookUpPromise(lookupIP).then((data) => {
                let record = data[0];
                if (record.length > 0) {
                 for ( let i = 0;i < record.length ; i++) {
                     this.selEnvironmentUsageOption.push({
                         value: record[i].EnvironmentCode,
                         text: record[i].EnvironmentDesc
                     });
                 }
                }
            });
    }

    private callDeleteService(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '3');
        let formdata: Object = {};
        formdata['PrepCode'] = this.getControlValue('PrepCode');
        formdata['Table'] = 'Prep';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module, this.inputParams.operation, search, formdata).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeleted));
                    this.uiForm.reset();
                    this.setControlValue('menu','');
                    this.activeLos = {
                        id: '',
                        text: ''
                    };
                    this.formMode = this.c_s_MODE_SELECT;
                    this.isLOS = false;
                    this.isMeasureBy = false;
                    this.disabledFieldInUpdate();
                    this.disableControl('BtnAdd', true);
                }

            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            });
    }

    private callService(): void {
        let prepCode: string = this.getControlValue('PrepCode');
        let search: QueryParams = this.getURLSearchParamObject();
        if (this.formMode === this.c_s_MODE_ADD)
            search.set(this.serviceConstants.Action, '1');
        else
            search.set(this.serviceConstants.Action, '2');
        let formdata: Object = {};
        formdata['PrepROWID'] = this.getControlValue('PrepROWID');
        formdata['LOSCode'] = this.getControlValue('LOSName');
        formdata['PrepDesc'] = this.getControlValue('PrepDesc');
        formdata['MeasureBy'] = this.getControlValue('MeasureBy');
        formdata['PrepCode'] = this.getControlValue('PrepCode');
        formdata['PrepROWID'] = this.getControlValue('PrepROWID');
        formdata['UsedFor'] = this.getControlValue('UsedFor');
        formdata['PassToPDAInd'] = this.getControlValue('PassToPDAInd') ? 'yes' : 'no';
        formdata['RejectPrepInd'] = this.getControlValue('RejectPrepInd') ? 'yes' : 'no';
        formdata['MaximumValue'] = this.getControlValue('MaximumValue');
        formdata['PestNetInd'] = this.getControlValue('PestNetInd') ? 'yes' : 'no';
        formdata['PestControlInd'] = this.getControlValue('PestControlInd') ? 'yes' : 'no';
        formdata['EnvironmentalMetricInd'] = this.getControlValue('EnvironmentalMetricInd') ? 'yes' : 'no';
        formdata['EnvironmentCode'] = this.getControlValue('SelEnvironmentUsage');
        formdata['WeightingFactor'] = !this.getControlValue('WeightingFactor') ? '0' : this.getControlValue('WeightingFactor');
        formdata['EBCRInd'] = this.getControlValue('EBCRInd') ? 'yes' : 'no';
        formdata['EPANumber'] = this.getControlValue('EPANumber');
        formdata['Percentage'] = this.getControlValue('Percentage');
        formdata['HighPercentage'] = this.getControlValue('HighPercentage');
        formdata['HighMixRatio'] = this.getControlValue('HighMixRatio');
        formdata['LowPercentage'] = this.getControlValue('LowPercentage');
        formdata['LowMixRatio'] = this.getControlValue('LowMixRatio');
        formdata['PreMixPercentage'] = this.getControlValue('PreMixPercentage');
        formdata['PreMixRatio'] = this.getControlValue('PreMixRatio');
        formdata['ItemCode'] = this.getControlValue('ItemCode');
        formdata['ActiveIngredient'] = this.getControlValue('ActiveIngredient');
        formdata['WHOClass'] = this.getControlValue('WHOClass');
        formdata['PrecautionaryStatements'] = this.getControlValue('PrecautionaryStatements');
        formdata['CounterAgents'] = this.getControlValue('CounterAgents');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module, this.inputParams.operation, search, formdata).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));

                    this.uiForm.reset();
                    this.setControlValue('menu','');
                    this.activeLos = {
                        id: '',
                        text: ''
                    };
                    this.formMode = this.c_s_MODE_UPDATE;
                    this.isLOS = true;
                    this.isMeasureBy = true;
                    this.enableFieldInUpdate();
                    this.isDeleteShow = true;
                    this.setControlValue('PrepCode', prepCode);
                    this.doLookUpCall();
                }

            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            });
    }

    public doLookUpCall(): void {
        this.formPristine();
        this.isLOS = true;
        this.isMeasureBy = true;
        if (this.formMode !== this.c_s_MODE_ADD) {
            let lookupIP = [
                {
                    'table': 'Prep',
                    'query': {
                        'BusinessCode': this.businessCode(),
                        'PrepCode': this.getControlValue('PrepCode')
                    },
                    'fields': ['LOSCode', 'PrepDesc', 'MeasureBy', 'UsedFor', 'PassToPDAInd', 'RejectPrepInd', 'MaximumValue', 'PestNetInd', 'PestControlInd', 'EnvironmentalMetricInd', 'EnvironmentCode', 'WeightingFactor', 'EBCRInd', 'EPANumber', 'Percentage', 'HighMixRatio', 'LowPercentage', 'HighPercentage', 'LowMixRatio', 'PreMixPercentage', 'PreMixRatio', 'ItemCode', 'ActiveIngredient', 'WHOClass', 'PrecautionaryStatements', 'CounterAgents']
                }
            ];
            this.LookUp.lookUpPromise(lookupIP).then((data) => {
                let record = data[0];
                if (record.length > 0) {
                    record = record[0];
                    this.setUiControl(record);
                    this.doAfterfetch();
                    this.riExchange.riInputElement.Enable(this.uiForm,'menu');
                } else {
                    if (this.getControlValue('PrepCode')) {
                        this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.recordNotFound));
                    }
                    this.disabledFieldInUpdate();
                    this.setUiControl('');
                    this.activeLos = {
                        id: '',
                        text: ''
                    };
                }
            }).catch(e => {
                this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                this.disabledFieldInUpdate();
                this.setUiControl('');
                this.activeLos = {
                    id: '',
                    text: ''
                };
            });
        }

    }

    public onPrepSearchSelect(data: any): void {
        this.isLOS = true;
        this.isMeasureBy = true;
        this.formPristine();
        this.losSearchDropDown.isTriggerValidate = false;
        this.losSearchDropDown.isRequired = true;
        if (data && data.addMode) {
        this.formMode = this.c_s_MODE_ADD;
        this.isSelEnvironmentUsageHide = false;
        this.addPrep();
        } else {
            this.formMode = this.c_s_MODE_UPDATE;
            this.setControlValue('PrepCode', data.PrepCode);
            this.setControlValue('PrepDesc', data.PrepDesc);
            this.doLookUpCall();
        }
    }

    public onValueChange(event: any): void {
        this.setControlValue('LOSName', event.target.value);
    }

    public onEnvironmentalMetricIndChange(event: any): void {
        this.doAfterfetch();
    }


    public deletePrep(): void {
        this.formMode = this.c_s_MODE_DELETE;
        let promptVO: ICabsModalVO = new ICabsModalVO();
        promptVO.msg = MessageConstant.Message.DeleteRecord;
        promptVO.confirmCallback = this.callDeleteService.bind(this);
        this.modalAdvService.emitPrompt(promptVO);
    }

    public addPrep(): void {
        this.uiForm.reset();
        this.setControlValue('menu','');
        this.activeLos = {
            id: '',
            text: ''
        };
        this.isDeleteShow = false;
        this.disableControl('BtnAdd', true);
        this.enableControls(['BtnDelete', 'BtnAdd','menu']);
        this.losSearchDropDown.isDisabled = false;
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PrepCode', true);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PrepDesc', true);
        this.formMode = this.c_s_MODE_ADD;
    }



    public updatePrep(): void {
        if (this.formMode === this.c_s_MODE_DELETE) {
            this.formMode = this.c_s_MODE_UPDATE;
        }
        if (this.uiForm.valid) {
         let promptVO: ICabsModalVO = new ICabsModalVO();
         promptVO.msg = MessageConstant.Message.ConfirmRecord;
         promptVO.confirmCallback = this.callService.bind(this);
         this.modalAdvService.emitPrompt(promptVO);
         return;
        }
        this.riExchange.riInputElement.isError(this.uiForm, 'PrepCode');
        this.riExchange.riInputElement.isError(this.uiForm, 'PrepDesc');
        this.riExchange.riInputElement.isError(this.uiForm, 'MeasureBy');
        this.losSearchDropDown.isTriggerValidate = true;
        this.losSearchDropDown.isRequired = true;
    }
    public onMenuChange(event: any): void {
        this.uiForm.controls['menu'].markAsPristine();
        if (event.target.value === 'PrepMix' && this.getControlValue('PrepCode')) {
            this.navigate('PrepMixMode', InternalGridSearchModuleRoutes.ICABSBPREPARATIONMIXGRID,{});
        } else {
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.recordNotFound));
        }
    }

    public onLOSSearcheDataReceived(data: any): void {
        this.setControlValue('LOSName', data.LOSCode);
        this.uiForm.controls['LOSName'].markAsDirty();
    }

    public onCancelClick(): void {
        if (this.formMode !== this.c_s_MODE_ADD) {
        this.activeLos = {
            id: this.pageParams.LOSName,
            text: this.pageParams.LOSName + ' - ' + this.pageParams.LOSDesc
        };
         for (let i = 0;i < Object.keys(this.pageParams).length; i++) {
             this.setControlValue(Object.keys(this.pageParams)[i], this.pageParams[Object.keys(this.pageParams)[i]]);
        }
        this.isSelEnvironmentUsageHide = this.getControlValue('EnvironmentalMetricInd');
    } else {
        this.formMode = this.c_s_MODE_SELECT;
       this.ellipsis.PrepCode.autoOpen = true;
       this.isDeleteShow = true;
       this.uiForm.reset();
       this.setControlValue('menu','');
       this.disabledFieldInUpdate();
       this.activeLos = {
            id: '',
            text: ''
        };
    }
    this.formPristine();
    }



}
