import { ExportConfig } from './../../base/ExportConfig';
import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';

import { Observable } from 'rxjs/Rx';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { InternalGridSearchSalesModuleRoutes } from './../../base/PageRoutes';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { EllipsisComponent } from './../../../shared/components/ellipsis/ellipsis';
import { EmployeeSearchComponent } from './../search/iCABSBEmployeeSearch';
import { PostCodeSearchComponent } from './../search/iCABSBPostcodeSearch.component';
import { ScreenNotReadyComponent } from './../../../shared/components/screenNotReady';
import { ActionTypes } from './../../actions/prospect';
import { QueryParams } from '@shared/services/http-params-wrapper';


@Component({
    templateUrl: 'iCABSCMProspectMaintenance.html'
})

export class CMProspectMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('postcodeSearchEllipsis') public postcodeSearchEllipsis: EllipsisComponent;
    @ViewChild('riMPAFSearchEllipsis') public riMPAFSearchEllipsis: EllipsisComponent;

    private muleConfig = {
        module: 'prospect',
        method: 'prospect-to-contract/maintenance',
        operation: 'ContactManagement/iCABSCMProspectMaintenance'
    };
    private prospectRowID: string;
    private syscharQuery: QueryParams;
    private queryPost: QueryParams = this.getURLSearchParamObject();
    private sysCharParams: Object = {

        vSCEnableAddressLine3: '', // 420
        vSCAddressLine3Logical: '', // 420
        vSCEnableMaxAddress: '', // 470
        vSCEnableMaxAddressValue: '', // 470
        vSCEnableHopewiserPAF: '', // 740
        vSCEnableDatabasePAF: '', // 750
        vSCAddressLine4Required: '', // 760
        vSCAddressLine5Required: '', // 770
        vSCAddressLine5Logical: '', // 770
        vSCPostCodeRequired: '', // 780
        vSCPostCodeMustExistInPAF: '', // 790
        vSCRunPAFSearchOn1stAddressLine: '' // 800
    };

    public pageId: string = '';
    public controls = [
        { name: 'ProspectNumber', required: true, disabled: true, type: MntConst.eTypeAutoNumber },
        { name: 'AccountNumber', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseNumber', required: false, disabled: true, type: MntConst.eTypeInteger },
        { name: 'PremiseName', required: true, disabled: false, type: MntConst.eTypeText },
        { name: 'PremiseAddressLine1', required: true, disabled: false, type: MntConst.eTypeText },
        { name: 'PremiseAddressLine2', required: false, disabled: false, type: MntConst.eTypeText },
        { name: 'PremiseAddressLine3', required: false, disabled: false, type: MntConst.eTypeText },
        { name: 'PremiseAddressLine4', required: true, disabled: false, type: MntConst.eTypeText },
        { name: 'PremiseAddressLine5', required: false, disabled: false, type: MntConst.eTypeText },
        { name: 'PremisePostCode', required: true, disabled: false, type: MntConst.eTypeText },
        { name: 'PremiseContactName', required: true, disabled: false, type: MntConst.eTypeText },
        { name: 'PremiseContactPosition', required: true, disabled: false, type: MntConst.eTypeText },
        { name: 'PremiseContactTelephone', required: true, disabled: false, type: MntConst.eTypeText },
        { name: 'PremiseContactMobile', required: false, disabled: false, type: MntConst.eTypeText },
        { name: 'PremiseContactFax', required: false, disabled: false, type: MntConst.eTypeText },
        { name: 'PremiseContactEmail', required: false, disabled: false, type: MntConst.eTypeEMail },
        { name: 'BusinessOriginCode', required: false, disabled: false, type: MntConst.eTypeCode },
        { name: 'BusinessOriginDesc', required: false, disabled: false, type: MntConst.eTypeText },
        { name: 'ProspectStatusCode', required: true, disabled: false, type: MntConst.eTypeCode },
        { name: 'ProspectStatusDesc', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ProspectConvertedInd' },
        { name: 'PDALeadInd', required: false, disabled: true },
        { name: 'PDALeadEmployeeCode', required: false, disabled: true, type: MntConst.eTypeCode },
        { name: 'PDALeadEmployeeSurname', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'TotalConvertedValue', required: false, disabled: true, type: MntConst.eTypeCurrency },
        { name: 'ConvertedSalesEmployeeCode', required: false, disabled: true, type: MntConst.eTypeCode },
        { name: 'ConvertedSalesEmployeeSurname', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'Narrative', required: true, disabled: false, type: MntConst.eTypeTextFree },
        { name: 'ProspectOriginCode', required: false, disabled: false, type: MntConst.eTypeCode },
        { name: 'Name', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'AddressLine1', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'AddressLine2', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'AddressLine3', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'AddressLine4', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'AddressLine5', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'PostCode', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ContactName', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ContactPosition', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ContactTelephone', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ContactMobile', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ContactFax', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ContactEmail', required: false, disabled: true, type: MntConst.eTypeEMail },
        { name: 'OriginalOriginCode' }
    ];

    public searchConfigs: any = {
        businessOriginLangSearch: {
            params: {
                parentMode: 'LookUp'
            },
            isDisabled: false,
            active: {
                id: '',
                text: ''
            }
        },
        prospectStatusSearchConfig: {
            autoOpen: false,
            ellipsisTitle: 'Prospect Status Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'Lookup'
            },
            httpConfig: {
                operation: 'ContactManagement/iCABSCMProspectStatusSearch',
                module: 'prospect',
                method: 'prospect-to-contract/search'
            },
            tableColumn: [
                { title: 'Code', name: 'ProspectStatusCode', type: MntConst.eTypeCode },
                { title: 'Description', name: 'ProspectStatusDesc', type: MntConst.eTypeText },
                { title: 'Converted', name: 'ProspectConvertedInd', type: MntConst.eTypeCheckBox },
                { title: 'User Selectable', name: 'UserSelectableInd', type: MntConst.eTypeCheckBox }
            ],
            disable: false
        },
        pdaEmployeeLeadSearch: {
            isDisabled: true,
            params: {
                parentMode: 'LookUp-PDALead'
            },
            component: EmployeeSearchComponent
        },
        salesEmployeeSearch: {
            isDisabled: true,
            params: {
                parentMode: 'LookUp-ConvertedSales'
            },
            component: EmployeeSearchComponent
        },
        postCodeSearch: {
            params: {
                parentMode: 'PremiseProspect',
                PremisePostCode: '',
                PremiseAddressLine4: '',
                PremiseAddressLine5: ''
            },
            component: PostCodeSearchComponent
        },
        riMPAFSearch: {
            params: {
                parentMode: 'PremiseProspect'
            },
            component: ScreenNotReadyComponent
        },
        modalConfig: {
            backdrop: 'static',
            keyboard: true
        }
    };

    public tabActive: any;
    public displayState: any = {
        cmdGetAddressVisibility: false,
        cmdGetAddressState: true,
        trPremiseAddressLine3: false,
        trAddressLine3: false
    };
    public mandatoryState: any = {
        premiseAddressLine3: false,
        premiseAddressLine5: false,
        convertedSalesEmployeeCode: false
    };

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.setTabActive(1);
        this.fetchSysChar();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSCMPROSPECTMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Prospect Maintenance';
    }

    /**
     * Method - prepareQueryDefaults
     * Prepares common query defaults
     * Takes action as argument
     * If action not passed it will be defaulted to 0
     */
    private prepareQueryDefaults(action?: string): QueryParams {
        let query: QueryParams = this.getURLSearchParamObject();

        if (!action)
            action = '0';

        query.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        query.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        query.set(this.serviceConstants.Action, action);

        return query;
    }

    /**
     * Method - createSysCharListForQuery
     * Creates A Comma Seperated List Of SysChars
     * This List Needs To Be Passed With The SysChar Query
     */
    private sysCharListForQuery(): string {
        let sysCharList = [
            this.sysCharConstants.SystemCharEnableAddressLine3, // 420
            this.sysCharConstants.SystemCharMaximumAddressLength, // 470
            this.sysCharConstants.SystemCharEnableHopewiserPAF, // 740
            this.sysCharConstants.SystemCharEnableDatabasePAF, // 750
            this.sysCharConstants.SystemCharAddressLine4Required, // 760
            this.sysCharConstants.SystemCharAddressLine5Required, // 770
            this.sysCharConstants.SystemCharPostCodeRequired, // 780
            this.sysCharConstants.SystemCharPostCodeMustExistinPAF, // 790
            this.sysCharConstants.SystemCharRunPAFSearchOnFirstAddressLine // 800
        ];
        return sysCharList.join(',');
    }

    /**
     * Method - getSysChars
     * Method To Get SysChar Values From Service
     */
    private fetchSysChar(): any {
        this.syscharQuery = new QueryParams();

        // Add Parameters To Query
        this.syscharQuery = this.prepareQueryDefaults();
        this.syscharQuery.set(this.serviceConstants.SystemCharNumber, this.sysCharListForQuery());

        // Make HTTP Call
        Observable.forkJoin(this.httpService.sysCharRequest(this.syscharQuery)).subscribe(
            (data) => {
                // Call Method To Update SysChars
                this.updateSysChar(data[0]);
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    /**
     * Method - updateFromSysChars
     * Update View And Store From The SysChar Values
     */
    private updateSysChar(sysChars: any): void {
        let records: Object;

        // If No Record Returned; Throw Error And Break From Method
        if (!sysChars || !sysChars.records.length)
            return;

        records = sysChars.records;

        this.sysCharParams['vSCEnableAddressLine3'] = records[0].Required;
        this.sysCharParams['vSCAddressLine3Logical'] = records[0].Logical;

        this.sysCharParams['vSCEnableMaxAddress'] = records[1].Required;
        this.sysCharParams['vSCEnableMaxAddressValue'] = records[1].Integer;

        this.sysCharParams['vSCEnableHopewiserPAF'] = records[2].Required;

        this.sysCharParams['vSCEnableDatabasePAF'] = records[3].Required;

        this.sysCharParams['vSCAddressLine4Required'] = records[4].Required;

        this.sysCharParams['vSCAddressLine5Required'] = records[5].Required;
        this.sysCharParams['vSCAddressLine5Logical'] = records[5].Logical;

        this.sysCharParams['vSCPostCodeRequired'] = records[6].Required;

        this.sysCharParams['vSCPostCodeMustExistInPAF'] = records[7].Required;

        this.sysCharParams['vSCRunPAFSearchOn1stAddressLine'] = records[8].Required;

        this.displayState.cmdGetAddressVisibility = true;

        this.windowOnLoad();
    }

    private windowOnLoad(): void {
        if (this.riExchange.routerParams['closed']) {
            this.disableControls([]);
            this.uiForm.controls['PDALeadInd'].disable();
            this.searchConfigs.businessOriginLangSearch.isDisabled = true;
            this.searchConfigs.prospectStatusSearchConfig.disable = true;

        }
        if (!this.sysCharParams['vSCEnableHopewiserPAF'] && !this.sysCharParams['vSCEnableDatabasePAF'])
            this.displayState.cmdGetAddressVisibility = false;
        if (this.sysCharParams['vSCEnableAddressLine3']) {
            this.displayState.trPremiseAddressLine3 = true;
            this.displayState.trAddressLine3 = true;
        }
        if (this.sysCharParams['vSCAddressLine3Logical']) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PremiseAddressLine3', true);
            this.mandatoryState.premiseAddressLine3 = true;
        }
        if (this.sysCharParams['vSCAddressLine5Logical']) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PremiseAddressLine5', true);
            this.mandatoryState.premiseAddressLine5 = true;
        }
        switch (this.parentMode) {
            case 'ContactManagement':
                this.setControlValue('ProspectNumber', this.riExchange.getParentHTMLValue('ProspectNumber'));
                break;
            case 'GeneralSearch':
                this.setControlValue('ProspectNumber', this.riExchange.getParentAttributeValue('ProspectNumber'));
                break;
        }

        this.fetchProspectRelatedData();
    }

    private fetchProspectRelatedData(): void {
        let queryGet: QueryParams = this.getURLSearchParamObject();
        queryGet.set(this.serviceConstants.Action, '0');
        queryGet.set(this.serviceConstants.ProspectNumber, this.getControlValue('ProspectNumber'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryGet)
            .subscribe(
                (data) => {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.pageParams = data;
                        this.prospectRowID = data.ttProspect;
                        this.setControlValue('ProspectNumber', data.ProspectNumber);
                        this.setControlValue('ProspectOriginCode', data.ProspectOriginCode);
                        this.setControlValue('Name', data.Name);
                        this.setControlValue('AddressLine1', data.AddressLine1);
                        this.setControlValue('AddressLine2', data.AddressLine2);
                        this.setControlValue('AddressLine3', data.AddressLIne3);
                        this.setControlValue('AddressLine4', data.AddressLine4);
                        this.setControlValue('AddressLine5', data.AddressLine5);
                        this.setControlValue('PostCode', data.Postcode);
                        this.setControlValue('ContactName', data.ContactName);
                        this.setControlValue('ContactPosition', data.ContactPosition);
                        this.setControlValue('ContactTelephone', data.ContactTelephone);
                        this.setControlValue('ContactFax', data.ContactFax);
                        this.setControlValue('ContactEmail', data.ContactEmail);
                        this.setControlValue('Narrative', data.Narrative);
                        this.setControlValue('ProspectStatusCode', data.ProspectStatusCode);
                        this.setControlValue('AccountNumber', data.AccountNumber);
                        this.setControlValue('ContractNumber', data.ContractNumber);
                        this.setControlValue('PremiseNumber', data.PremiseNumber);
                        this.setControlValue('PremiseName', data.PremiseName);
                        this.setControlValue('PremiseAddressLine1', data.PremiseAddressLine1);
                        this.setControlValue('PremiseAddressLine2', data.PremiseAddressLine2);
                        this.setControlValue('PremiseAddressLine3', data.PremiseAddressLine3);
                        this.setControlValue('PremiseAddressLine4', data.PremiseAddressLine4);
                        this.setControlValue('PremiseAddressLine5', data.PremiseAddressLine5);
                        this.setControlValue('PremisePostCode', data.PremisePostcode);
                        this.setControlValue('PremiseContactName', data.PremiseContactName);
                        this.setControlValue('PremiseContactPosition', data.PremiseContactPosition);
                        this.setControlValue('PremiseContactTelephone', data.PremiseContactTelephone);
                        this.setControlValue('PremiseContactMobile', data.PremiseContactMobile);
                        this.setControlValue('PremiseContactEmail', data.PremiseContactEmail);
                        this.setControlValue('PremiseContactFax', data.PremiseContactFax);
                        this.setControlValue('PDALeadEmployeeCode', data.PDALeadEmployeeCode);
                        this.setControlValue('ConvertedSalesEmployeeCode', data.ConvertedSalesEmployeeCode);
                        this.setControlValue('ContactMobile', data.ContactMobile);
                        this.setControlValue('BusinessOriginCode', data.BusinessOriginCode);
                        this.afterFetch();
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private afterFetch(): void {
        this.loadLookupData();
        this.searchConfigs.postCodeSearch.params.PremisePostCode = this.getControlValue('PremisePostCode');
        this.searchConfigs.postCodeSearch.params.PremiseAddressLine4 = this.getControlValue('PremiseAddressLine4');
        this.searchConfigs.postCodeSearch.params.PremiseAddressLine5 = this.getControlValue('PremiseAddressLine5');

        if (this.getControlValue('PDALeadEmployeeCode')) {
            this.setControlValue('PDALeadInd', true);
            this.pageParams.PDALeadInd = true;
        } else {
            this.pageParams.PDALeadInd = false;
        }
        this.setControlValue('OriginalOriginCode', this.getControlValue('ProspectOriginCode'));

        this.displayState.cmdGetAddressState = false;
        if (!this.riExchange.routerParams['closed']) {
            this.uiForm.controls['PDALeadInd'].enable();
        }

        if (this.getControlValue('ProspectConvertedInd')) {
            this.uiForm.controls['ConvertedSalesEmployeeCode'].enable();
            this.searchConfigs.salesEmployeeSearch.isDisabled = false;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ConvertedSalesEmployeeCode', true);
        }
        if (this.getControlValue('PDALeadInd')) {
            this.uiForm.controls['PDALeadInd'].disable();
            this.uiForm.controls['PDALeadEmployeeCode'].enable();
            this.searchConfigs.pdaEmployeeLeadSearch.isDisabled = false;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PDALeadEmployeeCode', true);
        }

        this.getTotalConvertedValue();
    }

    // Tabout for ProspectStatusCode
    private loadLookupData(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let lookupDetails: Array<any> = [{
            'table': 'ProspectStatus',
            'query': {
                'ProspectStatusCode': this.getControlValue('ProspectStatusCode')
            },
            'fields': ['ProspectConvertedInd']
        }, {
            'table': 'ProspectStatusLang',
            'query': {
                'ProspectStatusCode': this.getControlValue('ProspectStatusCode')
            },
            'fields': ['ProspectStatusDesc']
        }, {
            'table': 'BusinessOriginLang',
            'query': {
                'BusinessOriginCode': this.getControlValue('BusinessOriginCode')
            },
            'fields': ['BusinessOriginDesc']
        }, {
            'table': 'Employee',
            'query': {
                'EmployeeCode': this.getControlValue('ConvertedSalesEmployeeCode')
            },
            'fields': ['EmployeeSurname']
        }, {
            'table': 'Employee',
            'query': {
                'EmployeeCode': this.getControlValue('PDALeadEmployeeCode')
            },
            'fields': ['EmployeeSurname']
        }];

        this.LookUp.lookUpRecord(lookupDetails).subscribe((data) => {
            if (data.length > 0) {
                let prospectConvertedIndData: Array<any> = data[0],
                    prospectStatusDescData: Array<any> = data[1],
                    businessOriginDescData: Array<any> = data[2],
                    convertedSalesEmployeeSurnameData: Array<any> = data[3],
                    pdaLeadEmployeeSurnameData: Array<any> = data[4];

                if (prospectConvertedIndData.length > 0)
                    this.setControlValue('ProspectConvertedInd', prospectConvertedIndData[0].ProspectConvertedInd);
                else
                    this.setControlValue('ProspectConvertedInd', '');

                if (prospectStatusDescData.length > 0)
                    this.setControlValue('ProspectStatusDesc', prospectStatusDescData[0].ProspectStatusDesc);
                else
                    this.setControlValue('ProspectStatusDesc', '');

                if (businessOriginDescData.length > 0) {
                    this.setControlValue('BusinessOriginDesc', businessOriginDescData[0].BusinessOriginDesc);
                    this.searchConfigs.businessOriginLangSearch.active = {
                        id: this.getControlValue('BusinessOriginCode'),
                        text: this.getControlValue('BusinessOriginCode') + ' - ' + this.getControlValue('BusinessOriginDesc')
                    };
                }
                else
                    this.setControlValue('BusinessOriginDesc', '');

                if (this.getControlValue('ConvertedSalesEmployeeCode') && convertedSalesEmployeeSurnameData.length > 0)
                    this.setControlValue('ConvertedSalesEmployeeSurname', convertedSalesEmployeeSurnameData[0].EmployeeSurname);
                else
                    this.setControlValue('ConvertedSalesEmployeeSurname', '');

                if (this.getControlValue('PDALeadEmployeeCode') && pdaLeadEmployeeSurnameData.length > 0) {
                    this.setControlValue('PDALeadEmployeeSurname', pdaLeadEmployeeSurnameData[0].EmployeeSurname);
                }

                else
                    this.setControlValue('PDALeadEmployeeSurname', '');
            }
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });


    }

    private getTotalConvertedValue(): void {
        this.queryPost.set(this.serviceConstants.Action, '6');
        let formData: any = {
            BusinessCode: this.businessCode(),
            ProspectNumber: this.getControlValue('ProspectNumber'),
            Function: 'GetTotalConvertedValue'
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        if (+data.TotalConvertedValue !== 0) {
                            this.setControlValue('TotalConvertedValue', data.TotalConvertedValue);
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private saveProspect(): void {
        this.queryPost.set(this.serviceConstants.Action, '2');
        let formData: any = {
            ROWID: this.prospectRowID,
            PremiseName: this.getControlValue('PremiseName'),
            PremiseAddressLine1: this.getControlValue('PremiseAddressLine1'),
            PremiseAddressLine2: this.getControlValue('PremiseAddressLine2'),
            PremiseAddressLine3: this.getControlValue('PremiseAddressLine3'),
            PremiseAddressLine4: this.getControlValue('PremiseAddressLine4'),
            PremiseAddressLine5: this.getControlValue('PremiseAddressLine5'),
            PremisePostCode: this.getControlValue('PremisePostCode'),
            PremiseContactName: this.getControlValue('PremiseContactName'),
            PremiseContactPosition: this.getControlValue('PremiseContactPosition'),
            PremiseContactTelephone: this.getControlValue('PremiseContactTelephone'),
            PremiseContactMobile: this.getControlValue('PremiseContactMobile'),
            PremiseContactFax: this.getControlValue('PremiseContactFax'),
            PremiseContactEmail: this.getControlValue('PremiseContactEmail'),
            ContactName: this.getControlValue('ContactName'),
            ContactPosition: this.getControlValue('ContactPosition'),
            ContactTelephone: this.getControlValue('ContactTelephone'),
            PostCode: this.getControlValue('PostCode'),
            AddressLine1: this.getControlValue('AddressLine1'),
            AddressLine4: this.getControlValue('AddressLine4'),
            ProspectOriginCode: this.getControlValue('ProspectOriginCode'),
            Name: this.getControlValue('Name'),
            Narrative: this.getControlValue('Narrative'),
            ProspectStatusCode: this.getControlValue('ProspectStatusCode'),
            BusinessOriginCode: this.getControlValue('BusinessOriginCode'),
            PDALeadEmployeeCode: this.getControlValue('PDALeadEmployeeCode'),
            ConvertedSalesEmployeeCode: this.getControlValue('ConvertedSalesEmployeeCode') || '~~IGNORE~~'
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                    }
                    this.formPristine();
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    // Marking Controls as Touched
    private uiFormMarkAsTouched(): void {
        for (let control of this.controls) {
            if (!this.uiForm.controls[control.name].valid)
                this.uiForm.controls[control.name].markAsTouched();
        }
    }

    // Tabout for ProspectStatusCode
    public prospectStatusCodeOnChange(): void {
        this.queryPost.set(this.serviceConstants.Action, '6');
        let formData: any = {
            LanguageCode: this.riExchange.LanguageCode(),
            ProspectStatusCode: this.getControlValue('ProspectStatusCode'),
            Function: 'GetProspectConvertedInd'
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        this.setControlValue('ProspectConvertedInd', data.ProspectConvertedInd === 'yes' || data.ProspectConvertedInd === true ? true : false);
                        this.setControlValue('ProspectStatusDesc', data.ProspectStatusDesc);
                    }
                    this.enableField();
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public enableField(): void {
        if (this.getControlValue('ProspectConvertedInd')) {
            this.mandatoryState.convertedSalesEmployeeCode = true;
            this.uiForm.controls['ConvertedSalesEmployeeCode'].enable();
            this.searchConfigs.salesEmployeeSearch.isDisabled = false;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ConvertedSalesEmployeeCode', true);
        } else {
            this.setControlValue('ConvertedSalesEmployeeCode', '');
            this.setControlValue('ConvertedSalesEmployeeSurname', '');
            this.mandatoryState.convertedSalesEmployeeCode = false;
            this.uiForm.controls['ConvertedSalesEmployeeCode'].disable();
            this.searchConfigs.salesEmployeeSearch.isDisabled = true;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ConvertedSalesEmployeeCode', false);
        }
    }

    public prospectStatusCodeOnBlur(): void {
        this.prospectStatusCodeOnChange();
    }

    // Tab switching logic
    public setTabActive(index: any): void {
        this.tabActive = index;
    }

    public setProspectStatus(data: any): void {
        if (data) {
            this.setControlValue('ProspectStatusCode', data.ProspectStatusCode);
            this.setControlValue('ProspectStatusDesc', data.ProspectStatusDesc);
            this.prospectStatusCodeOnChange();
        }
    }

    // Callback for Business Origin Lang Ellipsis - Not yet developed
    public setBusinessOrigin(data: any): void {
        if (data)
            this.setControlValue('BusinessOriginCode', data['BusinessOriginLang.BusinessOriginCode']);
    }

    public setPDALeadEmployee(data: any): void {
        if (data) {
            this.setControlValue('PDALeadEmployeeCode', data.PDALeadEmployeeCode);
            this.setControlValue('PDALeadEmployeeSurname', data.PDALeadEmployeeSurname);
        }
    }

    public setSalesEmployee(data: any): void {
        if (data) {
            this.setControlValue('ConvertedSalesEmployeeCode', data.ConvertedSalesEmployeeCode);
            this.setControlValue('ConvertedSalesEmployeeSurname', data.ConvertedSalesEmployeeSurname);
        }
    }

    public pdaLeadIndOnChecked(): void {
        if (this.getControlValue('PDALeadInd')) {
            this.uiForm.controls['PDALeadEmployeeCode'].enable();
            this.searchConfigs.pdaEmployeeLeadSearch.isDisabled = false;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PDALeadEmployeeCode', true);
            this.setControlValue('ProspectOriginCode', '01');
        } else {
            this.uiForm.controls['PDALeadEmployeeCode'].disable();
            this.searchConfigs.pdaEmployeeLeadSearch.isDisabled = true;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PDALeadEmployeeCode', false);
            this.setControlValue('PDALeadEmployeeCode', '');
            this.setControlValue('PDALeadEmployeeSurname', '');
            this.setControlValue('ProspectOriginCode', this.getControlValue('OriginalOriginCode'));
        }

    }

    public setAddreddData(data: any): void {
        if (data) {
            this.setControlValue('PremiseAddressLine4', data.PremiseAddressLine4);
            this.setControlValue('PremiseAddressLine5', data.PremiseAddressLine5);
            this.setControlValue('PremisePostCode', data.PremisePostcode);
        }
    }

    public cmdGetAddressOnClick(): void {
        this.searchConfigs.postCodeSearch.params.PremisePostCode = this.getControlValue('PremisePostCode');
        this.searchConfigs.postCodeSearch.params.PremiseAddressLine4 = this.getControlValue('PremiseAddressLine4');
        this.searchConfigs.postCodeSearch.params.PremiseAddressLine5 = this.getControlValue('PremiseAddressLine5');

        if (this.sysCharParams['vSCEnableHopewiserPAF'])
            this.riMPAFSearchEllipsis.openModal();
        else if (this.sysCharParams['vSCEnableDatabasePAF'])
            this.postcodeSearchEllipsis.openModal();
    }

    public selectedOption(optionValue: string): void {
        switch (optionValue) {
            case 'ProspectConversion':
                this.store.dispatch({
                    type: ActionTypes.SAVE_DATA,
                    payload: {
                        ProspectNumber: this.getControlValue('ProspectNumber')
                    }
                });
                this.store.dispatch({
                    type: ActionTypes.SAVE_CODE,
                    payload: {
                        business: this.businessCode(),
                        country: this.countryCode()
                    }
                });
                this.navigate('Prospect', InternalGridSearchSalesModuleRoutes.ICABSCMPROSPECTCONVGRID);
                break;
        }
    }

    public promptModalForSave(): void {
        if (this.uiForm.valid) {
            this.loadLookupData();
            let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveProspect.bind(this));
            this.modalAdvService.emitPrompt(modalVO);
        } else
            this.uiFormMarkAsTouched();
    }

    // Cancel Operation
    public cancelProspect(): void {
        this.setControlValue('ProspectNumber', this.pageParams.ProspectNumber);
        this.setControlValue('ProspectOriginCode', this.pageParams.ProspectOriginCode);
        this.setControlValue('Name', this.pageParams.Name);
        this.setControlValue('AddressLine1', this.pageParams.AddressLine1);
        this.setControlValue('AddressLine2', this.pageParams.AddressLine2);
        this.setControlValue('AddressLine3', this.pageParams.AddressLIne3);
        this.setControlValue('AddressLine4', this.pageParams.AddressLine4);
        this.setControlValue('AddressLine5', this.pageParams.AddressLine5);
        this.setControlValue('PostCode', this.pageParams.Postcode);
        this.setControlValue('ContactName', this.pageParams.ContactName);
        this.setControlValue('ContactPosition', this.pageParams.ContactPosition);
        this.setControlValue('ContactTelephone', this.pageParams.ContactTelephone);
        this.setControlValue('ContactFax', this.pageParams.ContactFax);
        this.setControlValue('ContactEmail', this.pageParams.ContactEmail);
        this.setControlValue('Narrative', this.pageParams.Narrative);
        this.setControlValue('ProspectStatusCode', this.pageParams.ProspectStatusCode);
        this.setControlValue('AccountNumber', this.pageParams.AccountNumber);
        this.setControlValue('ContractNumber', this.pageParams.ContractNumber);
        this.setControlValue('PremiseNumber', this.pageParams.PremiseNumber);
        this.setControlValue('PremiseName', this.pageParams.PremiseName);
        this.setControlValue('PremiseAddressLine1', this.pageParams.PremiseAddressLine1);
        this.setControlValue('PremiseAddressLine2', this.pageParams.PremiseAddressLine2);
        this.setControlValue('PremiseAddressLine3', this.pageParams.PremiseAddressLine3);
        this.setControlValue('PremiseAddressLine4', this.pageParams.PremiseAddressLine4);
        this.setControlValue('PremiseAddressLine5', this.pageParams.PremiseAddressLine5);
        this.setControlValue('PremisePostCode', this.pageParams.PremisePostcode);
        this.setControlValue('PremiseContactName', this.pageParams.PremiseContactName);
        this.setControlValue('PremiseContactPosition', this.pageParams.PremiseContactPosition);
        this.setControlValue('PremiseContactTelephone', this.pageParams.PremiseContactTelephone);
        this.setControlValue('PremiseContactMobile', this.pageParams.PremiseContactMobile);
        this.setControlValue('PremiseContactEmail', this.pageParams.PremiseContactEmail);
        this.setControlValue('PremiseContactFax', this.pageParams.PremiseContactFax);
        this.setControlValue('PDALeadEmployeeCode', this.pageParams.PDALeadEmployeeCode);
        this.setControlValue('ConvertedSalesEmployeeCode', this.pageParams.ConvertedSalesEmployeeCode);
        this.setControlValue('ContactMobile', this.pageParams.ContactMobile);
        this.setControlValue('BusinessOriginCode', this.pageParams.BusinessOriginCode);
        this.setControlValue('PDALeadInd', this.pageParams.PDALeadInd);
        this.afterFetch();
        this.formPristine();
    }

    public validatePremisesTab(): boolean {
        if (this.uiForm.dirty) {
            if (!(this.uiForm.controls['PremiseName'].valid && this.uiForm.controls['PremiseAddressLine1'].valid && this.uiForm.controls['PremiseAddressLine2'].valid && this.uiForm.controls['PremiseAddressLine3'].valid && this.uiForm.controls['PremiseAddressLine4'].valid && this.uiForm.controls['PremiseAddressLine5'].valid && this.uiForm.controls['PremisePostCode'].valid && this.uiForm.controls['PremiseContactName'].valid && this.uiForm.controls['PremiseContactPosition'].valid && this.uiForm.controls['PremiseContactTelephone'].valid && this.uiForm.controls['ProspectStatusCode'].valid))
                return true;
            else
                return false;
        } else
            return false;
    }

    public onClickExportContact(): void {
        let gridRequest: any = {};
        let search: QueryParams = new QueryParams();

        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set(this.serviceConstants.CountryCode, this.countryCode());
        search.set(this.serviceConstants.Action, '2');
        search.set('ProspectNumber', this.getControlValue('ProspectNumber'));
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.PageSize, '2000');
        search.set(this.serviceConstants.PageCurrent, '1');

        gridRequest['method'] = 'prospect-to-contract/maintenance';
        gridRequest['module'] = 'prospect';
        gridRequest['operation'] = 'ContactManagement/iCABSCMProspectMaintenance';
        gridRequest['http_method'] = 'GET';
        gridRequest['search'] = search;

        gridRequest[ExportConfig.c_s_OPTIONS_KEY] = {
            dateCell: '31:1',
            datePattern: ExportConfig.c_s_DATE_FORMAT_1
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeExportRequest(gridRequest).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data && data.hasOwnProperty('SpreadsheetURL')) {
                window.open(data['SpreadsheetURL']);
            } else {
                this.globalNotifications.message = {
                    msg: MessageConstant.Message.GeneralError,
                    timestamp: (new Date()).getMilliseconds()
                };
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.logger.log(error);
            this.globalNotifications.message = {
                msg: MessageConstant.Message.GeneralError,
                timestamp: (new Date()).getMilliseconds()
            };
        });
    }
}
