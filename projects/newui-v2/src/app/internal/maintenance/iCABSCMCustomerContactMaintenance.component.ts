import { ExportConfig } from './../../base/ExportConfig';
import { Component, Injector, OnInit, OnDestroy, ViewChild, Renderer, AfterViewInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { LocalStorageService } from 'ngx-webstorage';
import { Observable, Subscription, ReplaySubject } from 'rxjs';

import { AccountSearchComponent } from '@internal/search/iCABSASAccountSearch';
import { BaseComponent } from '@base/BaseComponent';
import { CBBConstants } from '@shared/constants/cbb.constants';
import { ContractSearchComponent } from './../search/iCABSAContractSearch';
import { EllipsisComponent } from '@shared/components/ellipsis/ellipsis';
import { EmployeeSearchComponent } from './../search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { GridToSlideComponent } from '@shared/components/gridToSlide/gridToSlide.component';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceServiceModuleRoutes, ContractManagementModuleRoutes, CCMModuleRoutes, ProspectToContractModuleRoutes, InternalGridSearchApplicationModuleRoutes, InternalGridSearchServiceModuleRoutes, InternalMaintenanceApplicationModuleRoutes, AppModuleRoutes, BillToCashModuleRoutes, InternalMaintenanceModuleRoutes } from './../../base/PageRoutes';
import { MessageConstant } from '@shared/constants/message.constant';
import { OrderBy } from '@shared/pipes/orderBy';
import { PageIdentifier } from '@base/PageIdentifier';
import { PremiseSearchComponent } from './../search/iCABSAPremiseSearch';
import { RiTab, MntConst } from '@shared/services/riMaintenancehelper';
import { ScreenNotReadyComponent } from '@shared/components/screenNotReady';
import { ServiceCoverSearchComponent } from './../search/iCABSAServiceCoverSearch';
import { SmallDeviceConstants } from '../../mobile-version/constants/small-device.constants';
import { AuthService } from '@shared/services/auth.service';

@Component({
    templateUrl: 'iCABSCMCustomerContactMaintenance.html'
})

export class CustomerContactMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('riGridTickets') riGridTickets: GridAdvancedComponent;
    @ViewChild('riGridLogs') riGridLogs: GridAdvancedComponent;
    @ViewChild('riGridNotify') riGridNotify: GridAdvancedComponent;
    @ViewChild('riGridWorkOrder') riGridWorkOrder: GridAdvancedComponent;
    @ViewChild('riGridRootCause') riGridRootCause: GridAdvancedComponent;
    @ViewChild('riGridDisputedInvoices') riGridDisputedInvoices: GridAdvancedComponent;
    @ViewChild('contractNumberEllipsis') contractNumberEllipsis: EllipsisComponent;
    @ViewChild('premisesNumberEllipsis') premisesNumberEllipsis: EllipsisComponent;
    @ViewChild('productCodeEllipsis') productCodeEllipsis: EllipsisComponent;
    @ViewChild('accountNumberEllipsis') accountNumberEllipsis: EllipsisComponent;
    @ViewChild('nextActionEmployeeCodeEllipsis') nextActionEmployeeCodeEllipsis: EllipsisComponent;
    @ViewChild('originatingEmployeeCodeEllipsis') originatingEmployeeCodeEllipsis: EllipsisComponent;
    @ViewChild('setTabForSmallDevice') setTabForSmallDevice: any;
    @ViewChild('gridToSlide_dispute') gridToSlide_dispute: GridToSlideComponent;
    @ViewChild('gridToSlide_notify') gridToSlide_notify: GridToSlideComponent;
    @ViewChild('gridToSlide_logs') gridToSlide_logs: GridToSlideComponent;
    @ViewChild('gridToSlide_tickets') gridToSlide_tickets: GridToSlideComponent;
    @ViewChild('gridToSlide_rootcause') gridToSlide_rootcause: GridToSlideComponent;
    @ViewChild('gridToSlide_workorder') gridToSlide_workorder: GridToSlideComponent;

    private muleConfig: any = {
        method: 'ccm/maintenance',
        module: 'tickets',
        operation: 'ContactManagement/iCABSCMCustomerContactMaintenance'
    };
    private orderBy: OrderBy;
    private storeData: Object = {};
    private cboPostObj: Object = {};
    private customBusinessObjectAdditionalPostData = {};
    private subscriptionManager: Subscription;
    private cancelEvent: boolean = false;
    private lastNavRouteUrl: string = '';
    private previousRouteUrl: string = '';
    private copyPreviousMode: string = '';
    private pageData: Object = {};
    private currentTabID: string = '';
    private oldFormData: Object = {};
    private isNavReturnStatus: boolean = false;
    private isSaveReloadStatus: boolean = false;
    private isSavedEnableFlag: boolean = true;
    public pageId: string = '';
    public riTab: RiTab;
    public tabList: Object = {};
    public tab: any;
    public contactStatusCodeSelectList: Array<any> = [];
    public contactTypeDetailCodeSelectList: Array<any> = [];
    public contactTypeCodeSelectList: any = [];
    public menuOptionList: Array<any> = [];
    public addressCodeSelectList: Array<any> = [];
    public originalContactMediumCodeSelectList: Array<any> = [];
    public showControlBtn: boolean = true;
    public saveCancelDisable: boolean = false;
    public lReturningFromChild: boolean = false;
    public currentMode: string = '';
    public isSaved: boolean;
    public isInitialising: boolean;
    public customerContactROWID: string = '';
    public currentScreenMode: string = '';
    public ttContactType: Array<any> = [];
    public ttContactMedium: Array<any> = [];
    public ttContactStatus: Array<any> = [];
    public contactNotifyWhenSelectList: Array<any> = [
        { value: '0', text: 'Never' },
        { value: '1', text: 'On Create' },
        { value: '2', text: 'On Close' },
        { value: '3', text: 'On Create & Close' }
    ];
    public contactNotifyTypeSelectList: Array<any> = [
        { value: '1', text: 'Using Email' },
        { value: '2', text: 'Using SMS' }
    ];
    public cloneFormValues: Object = {};
    public tdDefaultAssigneeEmployeeLabelBgColor: boolean = false;
    public tdContactTypeDetailCodeLabel: boolean = false;
    public ellipsisConfig: any = {
        nextActionEmployeeCodeSearch: {
            isAutoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'ContactManagement',
                serviceBranchNumber: this.utils.getBranchCode(),
                showAddNew: false
            },
            contentComponent: EmployeeSearchComponent,
            showHeader: true,
            disabled: true
        },
        originatingEmployeeCodeSearch: {
            isAutoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'ContactManagement-Originating',
                serviceBranchNumber: this.utils.getBranchCode(),
                showAddNew: false
            },
            contentComponent: EmployeeSearchComponent,
            showHeader: true,
            disabled: true
        },
        contract: {
            isAutoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'LookUp',
                showAddNew: false,
                currentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                currentContractType: this.riExchange.getCurrentContractType(),
                showCountry: false,
                showBusiness: false
            },
            modalConfig: '',
            contentComponent: ContractSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        },
        premises: {
            isAutoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'LookUp',
                ContractNumber: '',
                ContractName: '',
                showAddNew: false,
                currentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                currentContractType: this.riExchange.getCurrentContractType()
            },
            modalConfig: '',
            contentComponent: PremiseSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        },
        scmPremises: {
            isAutoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: '',
                ContractNumber: '',
                ContractName: '',
                showAddNew: false,
                currentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                currentContractType: this.riExchange.getCurrentContractType()
            },
            modalConfig: '',
            contentComponent: ScreenNotReadyComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        },
        productCode: {
            isAutoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'LookUp',
                ContractNumber: '',
                ContractName: '',
                PremiseNumber: '',
                PremiseName: '',
                ProductCode: '',
                ProductDesc: '',
                showAddNew: false
            },
            modalConfig: '',
            contentComponent: ServiceCoverSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        },
        account: {
            isAutoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'ContractManagement',
                ContractNumber: '',
                ContractName: '',
                showAddNew: false,
                showAddNewDisplay: false,
                showCountryCode: false,
                showBusinessCode: false
            },
            modalConfig: '',
            contentComponent: AccountSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        }
    };
    public gridConfig: any = {
        riGridTickets: {
            pageSize: 10,
            gridCurPage: 1,
            totalRecords: 0,
            gridSort: true
        },
        riGridNotify: {
            pageSize: 10,
            gridCurPage: 1,
            totalRecords: 0
        },
        riGridLogs: {
            pageSize: 10,
            gridCurPage: 1,
            totalRecords: 0,
            gridSort: true
        },
        riGridRootCause: {
            pageSize: 10,
            gridCurPage: 1,
            totalRecords: 0,
            gridSort: true
        },
        riGridWorkOrder: {
            pageSize: 10,
            gridCurPage: 1,
            totalRecords: 0
        },
        riGridDisputedInvoices: {
            pageSize: 10,
            gridCurPage: 1,
            totalRecords: 0
        }
    };

    public buttonControls: any =
        {
            btnProspect: { disabled: false },
            cmdDisputedInvoices: { disabled: false },
            cmdAddRootCause: { disabled: false },
            BtnAmendContact: { disabled: false },
            cmdIntNotifications: { disabled: false },
            cmdScheduleVisit: { disabled: false },
            cmdCopyTicket: { disabled: false }
        };
    public fieldVisibility: any = {
        spanTeamDetails: false,
        tdcmdDisputedInvoices: true,
        tdCopyTicket: false,
        tdCopyTicketReturn: false,
        tdSelectDisputedInvoices: false,
        tdContactExport: false,
        tdContactEmployee: true,
        tdScheduleVisit: false,
        cmdAddRootCause: true,
        trLatestCreated: true,
        spnKeepOwnership: true,
        spnChkContactPassedToActioner: true,
        spanSMSSendOn: false,
        tdCallOutLabel: false,
        tdCallOut: false,
        trNotifications: false,
        spanContactNotifyTypeSelect: true,
        spanIntNotifications: true,
        spnCreateProspect: false,
        spnCRNoticePeriod: false,
        spanServiceArea: false,
        ServiceAreaCode: false,
        spanSalesArea: false,
        SalesAreaCode: false,
        trClientRetention: false,
        tdServiceTypeCode: false,
        tdServiceTypeCodeLabel: false,
        trNotifyMessage: true,
        trNotifyRecipients: false,
        trOriginatingEmployee: false,
        ContactPostcode: false,
        spanLatestDetails: false,
        spanNewEntry: false,
        tdScheduledCloseDate: false,
        spanBtnAmendContact: false,
        tdPostCode: true,
        tdProspect2: false,
        btnProspect: false
    };

    public fieldRequired: any = {};

    public translatedMessageList: any = {
        This_Contact_has_been_closed_Do_you_want_to_open_it: '',
        Their_work_is_being_redirected_to: '',
        Do_you_want_to_reassign_this_ticket_now: '',
        You_are_about_to_relate_this_ticket_to: '',
        Are_you_sure: '',
        Please_select: '',
        Never: '',
        On_Create: '',
        On_Close: '',
        On_Create_Close: '',
        Using_Email: '',
        Using_SMS: ''
    };

    public controls: Array<any> = [
        //Header grdHeader
        { name: 'CustomerContactNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeAutoNumber },
        { name: 'CurrentTeamID', readonly: false, disabled: false, required: false, type: MntConst.eTypeTextFree },
        { name: 'ActionCount', readonly: false, disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'ClosedDateTime', readonly: false, disabled: false, required: false, type: MntConst.eTypeTextFree },
        { name: 'AccountNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'AccountName', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'GroupAccountNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'GroupName', readonly: false, disabled: false, required: false, type: MntConst.eTypeTextFree },
        { name: 'ContractNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'ContractName', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'CurrentOwnerEmployeeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'CurrentOwnerEmployeeName', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'PremiseNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'PremiseName', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'ServiceTypeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'ServiceTypeDesc', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'ProductCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'ProductDesc', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'ContactPostcode', readonly: false, disabled: false, required: false, type: MntConst.eTypeTextFree },
        { name: 'DispProspectNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'ContactTypeDetailCodeSelect', readonly: false, disabled: false, required: false },
        { name: 'ContactTypeCodeSelect', readonly: false, disabled: false, required: false },
        //Tab grdAddress
        { name: 'AddressName', readonly: false, disabled: false, required: false },
        { name: 'AddressContactName', readonly: false, disabled: false, required: false },
        { name: 'AddressLine1', readonly: false, disabled: false, required: false },
        { name: 'AddressContactPosn', readonly: false, disabled: false, required: false },
        { name: 'AddressLine2', readonly: false, disabled: false, required: false },
        { name: 'AddressLine3', readonly: false, disabled: false, required: false },
        { name: 'AddressLine4', readonly: false, disabled: false, required: false },
        { name: 'AddressContactPhone', readonly: false, disabled: false, required: false },
        { name: 'AddressLine5', readonly: false, disabled: false, required: false },
        { name: 'AddressContactMobile', readonly: false, disabled: false, required: false },
        { name: 'AddressPostcode', readonly: false, disabled: false, required: false },
        { name: 'AddressContactEmail', readonly: false, disabled: false, required: false },
        { name: 'AddressCodeSelect', readonly: false, disabled: false, required: false },
        //Tab grdNotify
        { name: 'NotifyStyle', readonly: false, disabled: false, required: false },
        { name: 'NotifyDestination', readonly: false, disabled: false, required: false },
        { name: 'NotifyRecipients', readonly: false, disabled: false, required: false },
        { name: 'NotifyMessage', readonly: false, disabled: false, required: false },
        //Tab grdLatest
        { name: 'ContactName', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'ContactPosition', readonly: false, disabled: false, required: false, type: MntConst.eTypeText, commonValidate: true },
        { name: 'ContactTelephone', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'ContactMobileNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeText, commonValidate: true },
        { name: 'ContactEmailAddress', readonly: false, disabled: false, required: false, type: MntConst.eTypeTextFree },
        { name: 'ContactFax', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'TicketReference', readonly: false, disabled: false, required: false, type: MntConst.eTypeTextFree },
        { name: 'LatestCreatedDate', readonly: false, disabled: false, required: false, type: MntConst.eTypeDate },
        { name: 'LatestCreatedTime', readonly: false, disabled: false, required: false, type: MntConst.eTypeTime },
        { name: 'LatestCreatedByEmployeeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'LatestCreatedByEmployeeName', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'ShortDescription', readonly: false, disabled: false, required: false, type: MntConst.eTypeTextFree },
        { name: 'ServiceAreaCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'SalesAreaCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'EarliestEffectDate', readonly: false, disabled: false, required: false, type: MntConst.eTypeDate },
        { name: 'chkCreateProspect', readonly: false, disabled: false, required: false, type: MntConst.eTypeCheckBox },
        { name: 'CRCommenceDate', readonly: false, disabled: false, required: false, type: MntConst.eTypeDate },
        { name: 'CRNoticePeriod', readonly: false, disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'NextActionByDate', readonly: false, disabled: false, required: true, type: MntConst.eTypeDate },
        { name: 'NextActionByTime', readonly: false, disabled: false, required: false, type: MntConst.eTypeTime },
        { name: 'DefaultAssigneeEmployeeDetails', readonly: false, disabled: false, required: false, type: MntConst.eTypeTextFree },
        { name: 'OriginatingEmployeeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'OriginatingEmployeeName', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'NextActionEmployeeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'NextActionEmployeeName', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'chkKeepOwnership', readonly: false, disabled: false, required: false, type: MntConst.eTypeCheckBox },
        { name: 'chkContactPassedToActioner', readonly: false, disabled: false, required: false, type: MntConst.eTypeCheckBox },
        { name: 'SMSSendOnDate', readonly: false, disabled: false, required: false, type: MntConst.eTypeDate },
        { name: 'SMSSendOnTime', readonly: false, disabled: false, required: false, type: MntConst.eTypeTime },
        { name: 'chkCreateCallOut', readonly: false, disabled: false, required: false, type: MntConst.eTypeCheckBox },
        { name: 'ScheduledCloseDate', readonly: false, disabled: false, required: false, type: MntConst.eTypeDate },
        { name: 'ContactMediumCodeSelect', readonly: false, disabled: false, required: false },
        { name: 'ContactNotifyWhenSelect', readonly: false, disabled: false, required: false, value: '' },
        { name: 'ContactNotifyTypeSelect', readonly: false, disabled: false, required: false },
        { name: 'ContactStatusCodeSelect', readonly: false, disabled: false, required: false },
        { name: 'Comments', readonly: false, disabled: false, required: false, type: MntConst.eTypeTextFree },
        // Tab grdOriginal
        { name: 'CustomerContactName', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'CustomerContactPosition', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'CustomerContactTelephone', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'CustomerContactMobileNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'CustomerContactEmailAddress', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'CustomerContactFax', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'CustomerContactTicketReference', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'CreatedDate', readonly: false, disabled: false, required: false, type: MntConst.eTypeDate },
        { name: 'CreatedTime', readonly: false, disabled: false, required: false, type: MntConst.eTypeTime },
        { name: 'CreatedByEmployeeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'CreatedByEmployeeName', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'ShortNarrative', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'ContactNarrative', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'OriginalContactMediumCodeSelect', readonly: false, disabled: false, required: false },
        //Tab grdLogs
        { name: 'CallLogSummary', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'CallLogDetails', readonly: false, disabled: false, required: false },
        //hidden fields
        { name: 'CanAmendTicketReference' },
        { name: 'LanguageCode' },
        { name: 'ThisEmployeeCode' },
        { name: 'SearchCompanyName' },
        { name: 'SearchContactName' },
        { name: 'ContactTypeCallout', type: MntConst.eTypeCheckBox },
        { name: 'ContactTypeClientRetention', type: MntConst.eTypeCheckBox },
        { name: 'AllowCallout', type: MntConst.eTypeCheckBox },
        { name: 'AlwaysShowAssignOptions', type: MntConst.eTypeCheckBox },
        { name: 'ShowExportButton', type: MntConst.eTypeCheckBox },
        { name: 'ServiceBranchNumber' },
        { name: 'CallOutRowID' },
        { name: 'ServiceCoverNumber' },
        { name: 'ServiceCoverRowID' },
        { name: 'ErrorMessageDesc' },
        { name: 'ContractTypeCode' },
        { name: 'ContractTypeDesc' },
        { name: 'ContractTypeCodeList' },
        { name: 'CurrentCallLogID' },
        { name: 'ClosedDate', type: MntConst.eTypeDate },
        { name: 'ClosedTime', type: MntConst.eTypeTime },
        { name: 'chkCloseTopic', type: MntConst.eTypeCheckBox },
        { name: 'ContactStatusCode' },
        { name: 'DefaultKeepOwnership', type: MntConst.eTypeCheckBox },
        { name: 'CanSendIntNotify', type: MntConst.eTypeCheckBox },
        { name: 'ContactTypeCode' },
        { name: 'ContactTypeDesc' },
        { name: 'ContactTypeDetailCode' },
        { name: 'ContactTypeDetailDesc' },
        { name: 'ContactTypeDetailPassedList' },
        { name: 'ContactTypeDetailOrigList' },
        { name: 'ContactTypeDetailSuperList' },
        { name: 'ContactTypeDetailMandAccList' },
        { name: 'ContactTypeDetailMandConList' },
        { name: 'ContactTypeDetailMandPreList' },
        { name: 'ContactTypeDetailMandProList' },
        { name: 'ContactTypeDetailMandPostList' },
        { name: 'ContactTypeDetailAcctReviewList' },
        { name: 'ContactTypeDetailRootCauseList' },
        { name: 'ContactTypeDetailCRProspectList' },
        { name: 'DefaultAssigneeEmployeeCode' },
        { name: 'DefaultAssigneeEmployeeName' },
        { name: 'ContactRedirectionUniqueID' },
        { name: 'ContactMediumCode' },
        { name: 'OriginalContactMediumCode' },
        { name: 'ExternalCREmailInd', type: MntConst.eTypeCheckBox },
        { name: 'ExternalCLEmailInd', type: MntConst.eTypeCheckBox },
        { name: 'ExternalCRSMSInd', type: MntConst.eTypeCheckBox },
        { name: 'ExternalCLSMSInd', type: MntConst.eTypeCheckBox },
        { name: 'InternalSMSInd', type: MntConst.eTypeCheckBox },
        { name: 'ContactNotifyWhen' },
        { name: 'ContactNotifyType' },
        { name: 'RunFromParentMode' },
        { name: 'ContactTypeDetailCodeList' },
        { name: 'ContactTypeDetailDescList' },
        { name: 'ContactStatusCodeList' },
        { name: 'ContactStatusDescList' },
        { name: 'AvailabilityWarningMessage' },
        { name: 'TerminationWarningMessage' },
        { name: 'EmployeeLeftWarningMessage' },
        { name: 'IntNotifyType' },
        { name: 'IntNotifySubjectText' },
        { name: 'IntNotifyBodyText' },
        { name: 'SaveIntNotifySubjectText' },
        { name: 'SaveIntNotifyBodyText' },
        { name: 'UserAuthContractUpdate', type: MntConst.eTypeCheckBox },
        { name: 'WONumber' },
        { name: 'AddressCodeList' },
        { name: 'AddressDescList' },
        { name: 'AddressCodeDefault' },
        { name: 'AccountAddressName' },
        { name: 'AccountAddressLine1' },
        { name: 'AccountAddressLine2' },
        { name: 'AccountAddressLine3' },
        { name: 'AccountAddressLine4' },
        { name: 'AccountAddressLine5' },
        { name: 'AccountAddressPostcode' },
        { name: 'AccountAddressContactName' },
        { name: 'AccountAddressContactPosn' },
        { name: 'AccountAddressContactPhone' },
        { name: 'AccountAddressContactMobile' },
        { name: 'AccountAddressContactFax' },
        { name: 'AccountAddressContactEmail' },
        { name: 'PremiseAddressName' },
        { name: 'PremiseAddressLine1' },
        { name: 'PremiseAddressLine2' },
        { name: 'PremiseAddressLine3' },
        { name: 'PremiseAddressLine4' },
        { name: 'PremiseAddressLine5' },
        { name: 'PremiseAddressPostcode' },
        { name: 'PremiseAddressContactName' },
        { name: 'PremiseAddressContactPosn' },
        { name: 'PremiseAddressContactPhone' },
        { name: 'PremiseAddressContactMobile' },
        { name: 'PremiseAddressContactFax' },
        { name: 'PremiseAddressContactEmail' },
        { name: 'ProAcctAddressName' },
        { name: 'ProAcctAddressLine1' },
        { name: 'ProAcctAddressLine2' },
        { name: 'ProAcctAddressLine3' },
        { name: 'ProAcctAddressLine4' },
        { name: 'ProAcctAddressLine5' },
        { name: 'ProAcctAddressPostcode' },
        { name: 'ProAcctAddressContactName' },
        { name: 'ProAcctAddressContactPosn' },
        { name: 'ProAcctAddressContactPhone' },
        { name: 'ProAcctAddressContactMobile' },
        { name: 'ProAcctAddressContactFax' },
        { name: 'ProAcctAddressContactEmail' },
        { name: 'ProPremAddressName' },
        { name: 'ProPremAddressLine1' },
        { name: 'ProPremAddressLine2' },
        { name: 'ProPremAddressLine3' },
        { name: 'ProPremAddressLine4' },
        { name: 'ProPremAddressLine5' },
        { name: 'ProPremAddressPostcode' },
        { name: 'ProPremAddressContactName' },
        { name: 'ProPremAddressContactPosn' },
        { name: 'ProPremAddressContactPhone' },
        { name: 'ProPremAddressContactMobile' },
        { name: 'ProPremAddressContactFax' },
        { name: 'ProPremAddressContactEmail' },
        { name: 'PublicAddressName' },
        { name: 'PublicAddressLine1' },
        { name: 'PublicAddressLine2' },
        { name: 'PublicAddressLine3' },
        { name: 'PublicAddressLine4' },
        { name: 'PublicAddressLine5' },
        { name: 'PublicAddressPostcode' },
        { name: 'PublicAddressContactName' },
        { name: 'PublicAddressContactPosn' },
        { name: 'PublicAddressContactPhone' },
        { name: 'PublicAddressContactMobile' },
        { name: 'PublicAddressContactFax' },
        { name: 'PublicAddressContactEmail' },
        { name: 'FromCustomerContactNumber' },
        { name: 'ProspectNumber' },
        { name: 'LinkedProspectNumber' },
        { name: 'CRContractTerm', type: MntConst.eTypeCheckBox },
        { name: 'RelatedTickets', type: MntConst.eTypeCheckBox },
        { name: 'RelatedNotify', type: MntConst.eTypeCheckBox },
        { name: 'RelatedLogs', type: MntConst.eTypeCheckBox },
        { name: 'RelatedWorkOrder', type: MntConst.eTypeCheckBox },
        { name: 'RelatedCustomerContactRowID' },
        { name: 'RelatedCustomerContactNumber' },
        { name: 'RelatedProspectNumber' },
        { name: 'RelatedProspectRowID' },
        { name: 'WarnRedirectEmployeeCode' },
        { name: 'WarnRedirectEmployeePeriod' },
        { name: 'WarnRedirectEmployeeName' },
        { name: 'WindowClosingName' },
        { name: 'ClosedWithChanges' },
        { name: 'RootCauseCode' },
        { name: 'ActionFromDate' },
        { name: 'CanAmendInvoiceDetails' },
        { name: 'ActionFromTime', type: MntConst.eTypeTime },
        { name: 'DisputedInvoiceCacheName' },
        { name: 'SelectedInvoiceNumber' },
        { name: 'SelectedCompanyInvoiceNumber' },
        { name: 'SelectedCompanyCode' },
        { name: 'SelectedCompanyDesc' },
        { name: 'SelectedContractNumber' },
        { name: 'SelectedContractName' },
        { name: 'SelectedInvoiceName' },
        { name: 'DisputedInvoiceCacheTime' },
        { name: 'CopyFromTicketNumber' },
        { name: 'CopyFromTicketNumberSave' },
        { name: 'CurrentActionEmployeeCode' },
        { name: 'CurrentActionEmployeeName' },
        { name: 'CopyMode' },
        { name: 'CopyContactTypeCode' },
        { name: 'CopyContactTypeDetailCode' },
        { name: 'CopyDisputed' },
        { name: 'CopyRootCause' },
        { name: 'SaveCopyContactTypeCode' },
        { name: 'SaveCopyContactTypeDetailCode' },
        { name: 'OldContactTypeCode', value: '' },
        { name: 'menu' }
    ];

    public totalWidth: number = 0;
    public sdTabWrapperAnimateBy: number = 0;
    public processTabsForMobile: boolean = false;
    public columnsToHide: string;
    public readonly columnsToHideForActiveSlide: any = {
        'dispute': ['Extract Date', 'Period Start', 'Tax Point', 'Value Excl Tax'],
        'notification': ['Employee', 'Send On', 'Created'],
        'logs': ['call ended', 'Fax', 'Telephone', 'Position', 'Call Started'],
        'tickets': ['number', 'employee', 'send on'],
        'rootcause': [],
        'workorder': ['Result', 'WO Date/Time', 'Employee Name']
    };
    public pageMsgForTabs: any = {
        'grdDisputedInvoices': '',
        'grdLogs': '',
        'grdNotify': '',
        'grdRootCause': '',
        'grdTickets': '',
        'grdWorkOrder': ''
    };
    private pageMsgObj: any;
    public activeTabGridToSlide: GridToSlideComponent;
    public currentTabIdx: number = -1;
    public currentPageForSlides: number = 1;
    public userName: string;

    constructor(injector: Injector,
        private renderer: Renderer,
        private authService: AuthService,
        public localStorage: LocalStorageService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSCMCUSTOMERCONTACTMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Ticket Maintenance';
        this.orderBy = injector.get(OrderBy);
        this.subscriptionManager = new Subscription();
        this.lastNavRouteUrl = this.riExchange.getBackRoute();
        this.setCurrentPageUrl();
        this.pageMsgObj = SmallDeviceConstants.PageMsgList;
        this.userName = this.authService.displayName || this.localStorage.retrieve('DISPLAYNAME');

    }

    ngOnInit(): void {
        super.ngOnInit();

        this.fetchTranslationContent();
        this.pageParams.isPageLoading = true;
        this.isNavReturnStatus = (this.isReturning() || this.getNavReturnStatus() || this.getReturnFlagStatus()) ? true : false;
        if (this.isNavReturnStatus && this.pageParams.selfLoad) {
            this.isNavReturnStatus = !this.selfReloadStatus();
            this.parentMode = this.riExchange.getParentHTMLValue('parentMode');
        }
        if (this.isReturning() && this.parentMode === 'New') {
            this.location.back();
        }
        this.isSaveReloadStatus = false;
        this.pageParams.selfLoad = '';
        this.pageParams.isReturn = false;
        this.tabDraw();
        if (this.isNavReturnStatus) {
            this.copyPreviousMode = this.pageParams.cInitialMode.toString();
            this.previousRouteUrl = this.pageParams.previousRouteUrl;
            this.setControlValue('OldContactTypeCode', '');
            this.setControlValue('menu', '');
            this.captureOldFormData();
            this.windowOnload();
        }
        if (!this.isNavReturnStatus) {
            this.loadSpeedScriptData();
        }

    }

    ngAfterViewInit(): void {
        let slide_container = document.querySelectorAll('.slide-container');
        if (this.isMobile && slide_container.length) {
            slide_container.forEach((container) => {
                if (container) { container.addEventListener('scroll', this.scroll, true); }
            });
        }
    }

    ngOnDestroy(): void {
        if (this.isNavReturnStatus) {
            this.riExchangeUnloadHTMLDocument();
        }
        super.ngOnDestroy();
        if (this.subscriptionManager) {
            this.subscriptionManager.unsubscribe();
        }
    }

    private scroll = (e: any): void => {
        if (this.isRequesting) { return; }
        const maxScroll: any = e.srcElement.scrollHeight - e.srcElement.clientHeight;
        const currentScroll: any = e.srcElement.scrollTop;
        let pCent: any = (currentScroll / maxScroll) * 100;
        let data = { value: this.currentPageForSlides }; //set/increased on every prev api call
        if ((Math.ceil(pCent) > 85)) {
            switch (this.currentTabIdx) {
                case 2:
                    this.getCurrentPage(data, 'riGridDisputedInvoices');
                    break;

                case 3:
                    this.getCurrentPage(data, 'riGridNotify');
                    break;

                case 4:
                    this.getCurrentPage(data, 'riGridLogs');
                    break;
            }
        }
    }

    /**
     * Draw tab list and configure the individual tab section
     * @params void
     * @returns void
     */
    private tabDraw(): void {
        this.tabList = {
            tab1: { id: 'grdAddress', name: '', visible: true, active: true },
            tab2: { id: 'grdDisputedInvoices', name: '', visible: true, active: false },
            tab3: { id: 'grdNotify', name: '', visible: true, active: false },
            tab4: { id: 'grdLogs', name: '', visible: true, active: false },
            tab5: { id: 'grdTickets', name: '', visible: true, active: false },
            tab6: { id: 'grdRootCause', name: '', visible: true, active: false },
            tab7: { id: 'grdWorkOrder', name: '', visible: true, active: false },
            tab8: { id: 'grdOriginal', name: '', visible: true, active: false },
            tab9: { id: 'grdLatest', name: '', visible: true, active: false }
        };
        this.riTab = new RiTab(this.tabList, this.utils);
        this.tab = this.riTab['tabObject'];
    }

    //Get translated content and store the translated values into list
    private fetchTranslationContent(): void {
        this.getTranslatedValuesBatch((data: any) => {
            if (data) {
                this.translatedMessageList.This_Contact_has_been_closed_Do_you_want_to_open_it = data[0].toString();
                this.translatedMessageList.Their_work_is_being_redirected_to = data[1].toString();
                this.translatedMessageList.Do_you_want_to_reassign_this_ticket_now = data[2].toString();
                this.translatedMessageList.You_are_about_to_relate_this_ticket_to = data[3].toString();
                this.translatedMessageList.Are_you_sure = data[4].toString();
                this.translatedMessageList.Please_select = data[5].toString();
                this.translatedMessageList.Never = data[6].toString();
                this.translatedMessageList.On_Create = data[7].toString();
                this.translatedMessageList.On_Close = data[8].toString();
                this.translatedMessageList.On_Create_Close = data[9].toString();
                this.translatedMessageList.Using_Email = data[10].toString();
                this.translatedMessageList.Using_SMS = data[11].toString();
            }
        },
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.This_Contact_has_been_closed_Do_you_want_to_open_it],
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.Their_work_is_being_redirected_to],
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.Do_you_want_to_reassign_this_ticket_now],
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.You_are_about_to_relate_this_ticket_to],
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.Are_you_sure],
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.Please_select],
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.Never],
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.On_Create],
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.On_Close],
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.On_Create_Close],
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.Using_Email],
            [MessageConstant.PageSpecificMessage.customerContactMaintenance.Using_SMS]
        );
    }

    // Implement windows load method
    private windowOnload(): any {
        this.isSaved = false;
        this.isInitialising = true;
        this.setControlValue('ThisEmployeeCode', '');
        if (this.parentMode === 'CallCentreSearchNew' || this.parentMode === 'CallCentreSearch') {
            this.tdContactTypeDetailCodeLabel = true;
        }
        this.setupNotifyGrid();
        this.setupLogGrid();
        this.setupTicketGrid();
        this.setupWorkOrderGrid();
        this.setupRootCauseGrid();
        this.pageParams.lOrigDisuptedInvoicesInUse = this.pageParams.lDisputedInvoicesInUse;
        if (this.pageParams.lDisputedInvoicesInUse) {
            this.setupDisputedInvoicesGrid();
        }

        if (this.pageParams.SCEnableAddressLine3 === true) {
            this.fieldVisibility.trAddressLine3 = true;
        }

        this.buildTable();
        this.buildContactTypeList(true);

        this.disableControl('ContactTypeCode', true);
        this.disableControl('ContactTypeDetailCode', true);
        this.disableControl('ContactStatusCodeSelect', true);
        this.disableControl('ContactMediumCodeSelect', true);
        this.disableControl('OriginalContactMediumCodeSelect', true);
        this.disableControl('ContactNotifyWhenSelect', true);
        this.disableControl('ContactNotifyTypeSelect', true);
        this.buttonControls['cmdIntNotifications'].disabled = true;

        this.setControlValue('SearchCompanyName', this.riExchange.getParentHTMLValue('SearchCompanyName'));
        this.setControlValue('SearchContactName', this.riExchange.getParentHTMLValue('SearchContactName'));
        this.setControlValue('DisputedInvoiceCacheTime', this.riExchange.getParentHTMLValue('DisputedInvoiceCacheTime'));
        this.getStaticServerInfo();
        let params: Object = {};
        switch (this.parentMode) {
            case 'CallCentreSearchNew':
                if (!this.isReloadAddMode) {
                    this.riMaintenanceBeforeAdd();
                    this.riMaintenanceBeforeAddMode();
                    this.pageParams.cInitialMode = 'Add';
                    this.currentMode = MntConst.eModeAdd;
                    this.setControlValue('AccountNumber', this.riExchange.getParentHTMLValue('AccountNumber'));
                    this.setControlValue('AccountName', this.riExchange.getParentHTMLValue('AccountName'));
                    this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
                    this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
                    this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
                    this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
                    this.setControlValue('ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));
                    this.setControlValue('ServiceCoverNumber', this.riExchange.getParentHTMLValue('ServiceCoverNumber'));
                    this.setControlValue('ServiceCoverRowID', this.riExchange.getParentHTMLValue('ServiceCoverRowID'));
                    this.setAllSCRowID(this.riExchange.getParentHTMLValue('ServiceCoverRowID'));
                    this.setControlValue('ContactTypeCode', this.riExchange.getParentHTMLValue('ContactTypeCode'));
                    this.setControlValue('ContactTypeDetailCode', this.riExchange.getParentHTMLValue('ContactTypeDetailCode'));
                    this.setControlValue('CurrentCallLogID', this.riExchange.getParentHTMLValue('CurrentCallLogID'));
                    this.setControlValue('ContactMediumCode', this.riExchange.getParentHTMLValue('ContactMediumCode'));
                    this.setControlValue('DisputedInvoiceCacheName', this.riExchange.getParentHTMLValue('DisputedInvoiceCacheName'));
                    this.setControlValue('ContactPostcode', this.riExchange.getParentHTMLValue('CallContactPostCode'));
                    this.setControlValue('ContactNarrative', this.riExchange.getParentAttributeValue('CallNotepadvalue'));
                    let obs_1, obs_2: Observable<any>;
                    obs_1 = new Observable(observer => {
                        if (this.trim(this.getControlValue('ProductCode')) !== '') {
                            let pCode: Subscription = this.productCodeOnChange().subscribe((p) => {
                                observer.next(true);
                            });
                            this.subscriptionManager.add(pCode);
                        }
                        else {
                            observer.next(true);
                        }
                    });

                    obs_2 = new Observable(observer => {
                        if (this.getControlValue('PremiseNumber') === '0') {
                            this.setControlValue('PremiseNUmber', '');
                        }
                        this.setControlValue('ContactName', this.riExchange.getParentHTMLValue('CallContactName'));
                        this.setControlValue('ContactPosition', this.riExchange.getParentHTMLValue('CallContactPosition'));
                        this.setControlValue('ContactTelephone', this.riExchange.getParentHTMLValue('CallContactTelephone'));
                        this.setControlValue('ContactFax', this.riExchange.getParentHTMLValue('CallContactFax'));
                        this.setControlValue('TicketReference', this.riExchange.getParentHTMLValue('CallTicketReference'));
                        this.setControlValue('ContactMobileNumber', this.riExchange.getParentHTMLValue('CallContactMobile'));
                        this.setControlValue('ContactEmailAddress', this.riExchange.getParentHTMLValue('CallContactEmail'));
                        this.setControlValue('ShortDescription', this.riExchange.getParentHTMLValue('CallNotepadSummary'));
                        this.setControlValue('Comments', this.riExchange.getParentHTMLValue('CallNotepad'));
                        this.setControlValue('ContactMediumCode', this.riExchange.getParentHTMLValue('ContactMediumCode'));
                        this.setControlValue('ContactMediumCodeSelect', this.getControlValue('ContactMediumCode'));
                        this.setControlValue('PublicAddressName', this.riExchange.getParentHTMLValue('CallAddressName'));
                        this.setControlValue('PublicAddressLine1', this.riExchange.getParentHTMLValue('CallAddressLine1'));
                        this.setControlValue('PublicAddressLine2', this.riExchange.getParentHTMLValue('CallAddressLine2'));
                        this.setControlValue('PublicAddressLine3', this.riExchange.getParentHTMLValue('CallAddressLine3'));
                        //' Please Note: To Enable AUS Postcode defaulting Line4 & 5 Will Always Be Populated!
                        this.setControlValue('PublicAddressLine4', this.riExchange.getParentHTMLValue('CallAddressLine4'));
                        this.setControlValue('PublicAddressLine5', this.riExchange.getParentHTMLValue('CallAddressLine5'));
                        this.setControlValue('PublicAddressPostcode', this.riExchange.getParentHTMLValue('CallContactPostcode'));
                        this.setControlValue('ContactTypeCodeSelect', this.riExchange.getParentHTMLValue('ContactTypeCode'));
                        this.setControlValue('ContactTypeDetailCode', this.riExchange.getParentHTMLValue('ContactTypeDetailCode'));
                        let cTypeCode: Subscription = this.contactTypeCodeSelectOnChange().subscribe((e) => {
                            this.setControlValue('ContactTypeDetailCodeSelect', this.riExchange.getParentHTMLValue('ContactTypeDetailCode'));
                            let cTypeDesc: Subscription = this.contactTypeDetailCodeSelectOnChange().subscribe((e1) => {
                                observer.next(true);
                            });
                            this.subscriptionManager.add(cTypeDesc);
                        });
                        this.subscriptionManager.add(cTypeCode);
                        this.focusToTab();
                    });

                    if ((this.trim(this.getControlValue('ContractNumber')) !== '' && this.trim(this.getControlValue('AccountNumber')) === '') || (this.trim(this.getControlValue('ContractNumber')) !== '' || this.trim(this.getControlValue('AccountNumber')) !== '')) {
                        let dataServiceSubs: Subscription = this.getServerData('ChangeOfContractOnEntryNew').subscribe((s) => {
                            let sub1: Subscription = obs_1.subscribe((e1) => {
                                let sub2: Subscription = obs_2.subscribe((e2) => {
                                    this.loadPostScreenData();
                                });
                                this.subscriptionManager.add(sub2);
                            });
                            this.subscriptionManager.add(sub1);
                        });
                        this.subscriptionManager.add(dataServiceSubs);
                    }
                    else {
                        let sub3: Subscription = obs_1.subscribe((e1) => {
                            let sub4: Subscription = obs_2.subscribe((e2) => {
                                this.loadPostScreenData();
                            });
                            this.subscriptionManager.add(sub4);
                        });
                        this.subscriptionManager.add(sub3);
                    }
                }
                else {
                    this.reloadPageInUpdateMode();
                }
                break;
            case 'CallCentreSearch':
                this.setControlValue('CustomerContactNumber', this.riExchange.getParentHTMLValue('CustomerContactNumber'));
                this.pageParams.cInitialMode = 'Update';
                this.currentMode = MntConst.eModeUpdate;
                params = { CustomerContactNumber: this.getControlValue('CustomerContactNumber') };
                this.riMaintenanceFetchRecord(params);
                this.setControlValue('CurrentCallLogID', this.riExchange.getParentHTMLValue('CurrentCallLogID'));
                this.setScreenTo('update');
                this.setControlValue('ServiceCoverRowID', this.riExchange.getParentHTMLValue('TicketServiceCoverRowID'));
                this.setAllSCRowID(this.riExchange.getParentHTMLValue('TicketServiceCoverRowID'));
                this.loadPostScreenData();
                break;
            case 'EmployeeDiary':
                this.setControlValue('PassCustomerContactNumber', this.riExchange.getParentHTMLValue('PassCustomerContactNumber'));
                this.pageParams.cInitialMode = 'Update';
                this.currentMode = MntConst.eModeUpdate;
                params = { CustomerContactNumber: this.getControlValue('CustomerContactNumber') };
                this.riMaintenanceFetchRecord(params);
                this.setControlValue('CurrentCallLogID', this.riExchange.getParentHTMLValue('CurrentCallLogID'));
                this.setScreenTo('update');
                this.loadPostScreenData();
                break;
            case 'ClientRetentionRequest':
                this.pageParams.cInitialMode = 'Update';
                this.currentMode = MntConst.eModeUpdate;
                this.setControlValue('CustomerContactNumber', this.riExchange.getParentHTMLValue('CustomerContactNumber'));
                params = { CustomerContactNumber: this.getControlValue('CustomerContactNumber') };
                this.riMaintenanceFetchRecord(params);
                this.setScreenTo('update');
                this.loadPostScreenData();
                break;
            case 'PDAICABSLead':
                this.pageParams.cInitialMode = 'Update';
                this.currentMode = MntConst.eModeUpdate;
                this.setControlValue('CustomerContactNumber', this.riExchange.getParentHTMLValue('CustomerContactNumber'));
                params = { CustomerContactNumber: this.getControlValue('CustomerContactNumber') };
                this.riMaintenanceFetchRecord(params);
                this.setScreenTo('view');
                this.loadPostScreenData();
                break;
            case 'SalesOrderCMSearch':
                this.pageParams.cInitialMode = 'Update';
                this.currentMode = MntConst.eModeUpdate;
                this.pageParams.CustomerContact = this.riExchange.getParentHTMLValue('PassContactROWID');
                params = { CustomerContactROWID: this.pageParams.CustomerContact };
                this.riMaintenanceFetchRecord(params);
                this.setScreenTo('view');
                this.loadPostScreenData();
                break;
            case 'CMSearch':
            case 'CallCentreReview':
                this.pageParams.cInitialMode = 'Update';
                this.currentMode = MntConst.eModeUpdate;
                let rowId: string = this.riExchange.getParentAttributeValue('CustomerContactNumberRowID');
                this.pageParams.CustomerContact = this.riExchange.getParentHTMLValue('CustomerContactROWID') || rowId;
                params = { 'CustomerContactROWID': this.pageParams.CustomerContact, 'CustomerContactNumber': '' };
                this.riMaintenanceFetchRecord(params);
                this.setScreenTo('view');
                this.loadPostScreenData();
                break;
            //' ReRun From RelatedTickets Tab From A Previous Instance Of This Maintenance Routine
            case 'ContactRelatedTicket':
                this.pageParams.cInitialMode = 'Update';
                this.currentMode = MntConst.eModeUpdate;
                this.pageParams.CustomerContact = this.riExchange.getParentHTMLValue('RelatedCustomerContactRowID');
                this.setControlValue('CustomerContactNumber', this.riExchange.getParentHTMLValue('RelatedCustomerContactNumber'));
                params = { 'CustomerContactROWID': this.pageParams.CustomerContact, 'CustomerContactNumber': this.getControlValue('CustomerContactNumber') || '' };
                this.riMaintenanceFetchRecord(params);
                this.browserTitle = this.pageTitle = 'Related Ticket Maintenance';
                this.utils.setTitle(this.pageTitle);
                this.setScreenTo('view');
                this.loadPostScreenData();
                break;
            case 'New':
            case 'ClientRetention':
                if (!this.isReloadAddMode) {
                    this.riMaintenanceBeforeAdd();
                    this.riMaintenanceBeforeAddMode();
                    this.pageParams.cInitialMode = 'Add';
                    this.currentMode = MntConst.eModeAdd;
                    this.setControlValue('AccountNumber', this.riExchange.getParentHTMLValue('AccountNumber'));
                    this.setControlValue('AccountName', this.riExchange.getParentHTMLValue('AccountName'));
                    this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
                    this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
                    this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
                    this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
                    this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
                    this.setControlValue('ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));
                    this.setControlValue('ServiceCoverNumber', this.riExchange.getParentHTMLValue('ServiceCoverNumber'));
                    this.setControlValue('ServiceCoverRowID', this.riExchange.getParentHTMLValue('ServiceCoverRowID'));
                    this.setAllSCRowID(this.riExchange.getParentHTMLValue('ServiceCoverRowID'));
                    if ((this.trim(this.getControlValue('ContractNumber')) !== '' && this.trim(this.getControlValue('AccountNumber')) === '') || (this.trim(this.getControlValue('ContractNumber')) !== '' || this.trim(this.getControlValue('AccountNumber')) !== '')) {
                        this.getServerData('ChangeOfContractOnEntryNew');
                    }
                    if (this.parentMode === 'ClientRetention') {
                        //'Get details from LostBusinessRequest
                        this.setControlValue('ContactName', this.riExchange.getParentHTMLValue('ClientContact'));
                        this.setControlValue('ContactPosition', this.riExchange.getParentHTMLValue('ClientContactPosition'));
                        this.setControlValue('ShortDescription', this.riExchange.getParentAttributeValue('LostBusinessRequestNarrativeValue'));
                        this.setControlValue('Comments', this.riExchange.getParentAttributeValue('LostBusinessRequestNarrativeValue'));
                        this.setControlValue('NextActionEmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
                        this.setControlValue('NextActionEmployeeName', this.riExchange.getParentHTMLValue('EmployeeSurname'));
                        let tempDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 2);
                        this.setControlValue('NextActionByDate', tempDate);
                        this.setControlValue('NextActionByTime', this.getShortTime());
                    }
                    //set default value into ContactMediumCodeSelect dropdown list
                    if ((this.ttContactMedium.length > 0) && !this.getControlValue('ContactMediumCode')) {
                        this.setControlValue('ContactMediumCode', this.ttContactMedium[0].ContactMediumCode);
                        this.setControlValue('ContactMediumCodeSelect', this.getControlValue('ContactMediumCode'));
                    }

                    this.loadPostScreenData();
                }
                else {
                    this.reloadPageInUpdateMode();
                }
                break;
            default:
                if (this.storeData)
                    this.onUpdateMode(this.storeData);
                this.setScreenTo('view');
                this.loadPostScreenData();
        }
    }

    private loadPostScreenData(): void {
        if (this.parentMode === 'New' || this.parentMode === 'CallCentreSearchNew' || this.parentMode === 'ClientRetention') {
            this.disableControl('ContactStatusCodeSelect', false);
            this.disableControl('ContactMediumCodeSelect', false);
            this.disableControl('ContactNotifyWhenSelect', false);
            this.disableControl('ContactNotifyTypeSelect', false);
            this.buttonControls['cmdIntNotifications'].disabled = false;
            //' Used To Link Prospect && None-Account Tickets Together
            this.setControlValue('FromCustomerContactNumber', this.riExchange.getParentHTMLValue('SelectedTicketNumber'));
        }
        else {
            this.disableControl('ContactStatusCodeSelect', true);
            this.disableControl('ContactNotifyWhenSelect', true);
            this.disableControl('ContactNotifyTypeSelect', true);
            this.buttonControls['cmdIntNotifications'].disabled = true;
        }

        if (this.parentMode === 'CallCentreReview') {
            this.contactStatusCodeSelectOnChange();
        }

        if (this.pageParams.cInitialMode === 'Update') {
            this.pageParams.cInitialContactTypeDetail = this.getControlValue('ContactTypeDetailCode');
        }
        else {
            if (!this.getControlValue('ContactTypeCodeSelect')) {
                this.setControlValue('ContactTypeCodeSelect', this.pageParams.cDefaultContactType);
                this.contactTypeCodeSelectOnChange();
            }
        }

        let obs: Observable<any> = new Observable(observer => {
            if (this.pageParams.lCanAmendInvoice === true) {
                this.fieldVisibility.tdcmdDisputedInvoices = true;
                this.setControlValue('CanAmendInvoiceDetails', 'Y');
            }
            if (this.pageParams.cSchedulingURLAddress) {
                this.fieldVisibility.tdScheduleVisit = true;
                this.enableScheduleButton();
            }
            this.processForm();
            observer.next(true);
        });


        //' Set FOCUS Based upon the values found, where we have come from and the MODE in which we are running this window
        if (this.pageParams.cInitialMode === 'Update') {
            let subs: Subscription = obs.subscribe();
            this.subscriptionManager.add(subs);
            this.utils.makeTabsNormal();
            this.cloneFormData();
        }
        else {
            //' Get Data From Server
            let serviceData: Subscription = this.getServerData('ChangeOfContactType').subscribe((e1) => {
                switch (this.parentMode) {
                    case 'New':
                    case 'ClientRetention':
                        this.setFocusToControl('ContactTypeCodeSelect');
                        break;
                    default:
                        if (this.trim(this.getControlValue('AccountNumber')) === '') {
                            this.setFocusToControl('AccountNumber');
                        } else if (this.trim(this.getControlValue('ContractNumber')) === '') {
                            this.setFocusToControl('ContractNumber');
                        } else if (this.trim(this.getControlValue('PremiseNumber')) === '') {
                            this.setFocusToControl('PremiseNumber');
                        } else if (this.trim(this.getControlValue('ProductCode')) === '') {
                            this.setFocusToControl('ProductCode');
                        }
                        else {
                            this.setFocusToControl('ContactName');
                        }
                }

                obs.subscribe();
                setTimeout(() => {
                    this.setControlValue('ContactTypeDetailCodeSelect', this.getControlValue('ContactTypeDetailCode'));
                    this.eventCategoryAddMode();
                    this.utils.makeTabsNormal();
                }, 0);
            });
            this.subscriptionManager.add(serviceData);
        }
        if (this.pageParams.backRouteUrl === InternalMaintenanceServiceModuleRoutes.ICABSCMCUSTOMERCONTACTEMPLOYEEVIEW) {
            this.setControlValue('NextActionEmployeeCode', this.pageParams.NextActionEmployeeCode);
            this.setControlValue('NextActionEmployeeName', this.pageParams.NextActionEmployeeName);
            this.pageParams.backRouteUrl = '';
        }
    }

    //This method is used to load Speed Script Data
    private loadSpeedScriptData(): any {
        this.setDefaultPageParams();
        this.getSysCharDetails();
    }

    //This method is used to load Sys char Data
    private getSysCharDetails(): any {
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharAutoGenerateProspectFromClientRetention,
            this.sysCharConstants.SystemCharEnableAddressLine3
        ];
        let sysCharIP = {
            module: this.muleConfig.module,
            operation: this.muleConfig.operation,
            action: 0,
            businessCode: this.utils.getBusinessCode(),
            countryCode: this.pageParams.vCountryCode,
            SysCharList: sysCharList.toString()
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.speedScript.sysCharPromise(sysCharIP).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            let records = data.records;
            this.pageParams.glSCAutoCreateProspect = records[0].Required ? records[0].Required : false;
            this.pageParams.glSCEnableAddressLine3 = records[1].Required ? records[1].Required : false;
            this.processSpeedScriptData();
        }).catch((error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.errorService.emitError(error);
        });
    }

    //This method is used to Initialize page params
    private setDefaultPageParams(): void {
        this.pageParams.gcScheduledStatusList = '';
        this.pageParams.gcDefaultContactType = '';
        this.pageParams.gcContactTypeCodeAddList = '';
        this.pageParams.gcContactTypeDescAddList = '';
        this.pageParams.gcContactTypeCodeFullList = '';
        this.pageParams.gcContactTypeDescFullList = '';
        this.pageParams.glUserCanAdd = false;
        this.pageParams.glSCAutoCreateProspect = false;
        this.pageParams.gcThisEmployeeCode = '';
        this.pageParams.gcContractTypeCodeList = '';
        this.pageParams.glDefaultKeepOwnership = false;
        this.pageParams.glValidForAdd = false;
        this.pageParams.glSCEnableAddressLine3 = false;
        this.pageParams.glContactTypeDetailAmendInd = false;
        this.pageParams.gcCanAmendContactTypeList = '';
        this.pageParams.gcCanAmendContactTypeReg = '';
        this.pageParams.gcCannotAmendCTDError = '';
        this.pageParams.giCount = 0;
        this.pageParams.gcDefaultNotificationType = '';
        this.pageParams.glDisputedInvoicesInUse = false;
        this.pageParams.glCanAmendInvoice = false;
        this.pageParams.gcSchedulingURLAddress = '';
        this.pageParams.grdLatestTabLabel = false;
        this.cloneFormData();
        this.pageParams.previousRouteUrl = '';
    }

    //This method is used to process the speed script response after receiving the speed script data
    private processSpeedScriptData(): void {
        let gLanguageCode = this.riExchange.LanguageCode();
        let data = [
            {
                'table': 'ContactType',
                'query': { 'BusinessCode': this.utils.getBusinessCode() },
                'fields': ['BusinessCode', 'ContactTypeCode', 'ContactTypeSystemDesc', 'ValidForNewInd']
            },
            {
                'table': 'ContactTypeLang',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'LanguageCode': gLanguageCode },
                'fields': ['BusinessCode', 'ContactTypeCode', 'ContactTypeDesc']
            },
            {
                'table': 'ContactTypeDetail',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'CallCentreInd': true },
                'fields': ['ContactTypeCode', 'ContactCreateSecurityLevel', 'ValidForNewInd', 'URL']
            },
            {
                'table': 'ContactStatus',
                'query': { 'BusinessCode': this.utils.getBusinessCode() },
                'fields': ['BusinessCode', 'ContactStatusCode', 'ContactStatusSystemDesc', 'ScheduledCloseStatusInd']
            },
            {
                'table': 'ContactStatusLang',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'LanguageCode': gLanguageCode },
                'fields': ['BusinessCode', 'ContactStatusCode', 'ContactStatusDesc']
            },
            {
                'table': 'ContactMedium',
                'query': { 'BusinessCode': this.utils.getBusinessCode() },
                'fields': ['BusinessCode', 'ContactMediumCode', 'ContactMediumSystemDesc']
            },
            {
                'table': 'ContactMediumLang',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'LanguageCode': gLanguageCode },
                'fields': ['BusinessCode', 'ContactMediumCode', 'ContactMediumDesc']
            },
            {
                'table': 'Employee',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'UserCode': this.utils.getUserCode() },
                'fields': ['BusinessCode', 'EmployeeCode', 'BranchNumber']
            },
            {
                'table': 'Branch',
                'query': { 'BusinessCode': this.utils.getBusinessCode() },
                'fields': ['BusinessCode', 'BranchNumber', 'NationalAccountBranch']
            },
            {
                'table': 'ContractType',
                'query': { 'BusinessCode': this.utils.getBusinessCode() },
                'fields': ['BusinessCode', 'ContractType', 'ContractTypeCode']
            },
            {
                'table': 'Team',
                'query': { 'BusinessCode': this.utils.getBusinessCode() },
                'fields': ['BusinessCode', 'TeamID', 'LedgerTeamInd']
            },
            {
                'table': 'TeamUser',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'UserCode': this.utils.getUserCode() },
                'fields': ['BusinessCode', 'LedgerTeamInd']
            },
            {
                'table': 'riRegistry', // 12 row
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'RegSection': 'Contact Centre Search', 'RegKey': this.utils.getBusinessCode() + '_' + 'Allow Change Of ContactTypeList' },
                'fields': ['RegValue', 'RegSection', 'RegKey']
            },
            {
                'table': 'riRegistry',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'RegSection': 'CCM Disputed Invoices', 'RegKey': this.utils.getBusinessCode() + '_' + 'Enable CCM Dispute Processing' },
                'fields': ['RegValue', 'RegSection', 'RegKey']
            },
            {
                'table': 'riRegistry',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'RegSection': 'Contact Centre Search', 'RegKey': this.utils.getBusinessCode() + '_' + 'Ticket Maintenance: Default Notification Type' },
                'fields': ['RegValue', 'RegSection', 'RegKey']
            },
            {
                'table': 'riRegistry',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'RegSection': 'Scheduling', 'RegKey': this.utils.getBusinessCode() + '_' + 'URLAddress' },
                'fields': ['RegValue', 'RegSection', 'RegKey']
            },
            {
                'table': 'UserAuthority',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'UserCode': this.utils.getUserCode() },
                'fields': ['UserCode', 'ContactTypeDetailAmendInd', 'ContactCreateSecurityLevel']
            },
            {
                'table': 'riRegistry',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'RegSection': this.speedScriptConstants.CNFContactPersonRegSection },
                'fields': ['RegValue', 'RegSection', 'RegKey']
            }
        ];

        this.ajaxSource.next(this.ajaxconstant.START);
        let lookupScp: Subscription = this.LookUp.lookUpRecord(data, 500).subscribe((e) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (e) {
                let riRegistryTemp: any[] = (e.length >= 18) && e[17] ? e[17] : [];
                this.pageParams.glMultiContactInd = (riRegistryTemp && riRegistryTemp.length > 0) ? true : false;
                let userAuthority: any[] = (e.length > 16) && e[16] ? e[16] : [];
                if (!userAuthority || userAuthority.length === 0) {
                    this.modalAdvService.emitError(new ICabsModalVO('2 - UserAuthority'));
                    return;
                }

                if (e[0].length > 0) {
                    let contactType: any[] = e[0];
                    let contactTypeLang: any[] = [];
                    let contactTypeDetail: any[] = [];
                    let contactStatus: any[] = e[3] ? e[3] : [];
                    let contactStatusLang: any[] = e[4] ? e[4] : [];
                    let contactMedium: any[] = e[5] ? e[5] : [];
                    let contactMediumLang: any[] = e[6] ? e[6] : [];
                    let employee: any[] = e[7] ? e[7] : [];
                    let branch: any[] = e[8] ? e[8] : [];
                    let contractType: any[] = e[9] ? e[9] : [];
                    let team: any[] = e[10] ? e[10] : [];
                    let teamUser: any[] = e[11] ? e[11] : [];
                    let riRegistry_0: any[] = e[12] ? e[12] : [];
                    let riRegistry_1: any[] = e[13] ? e[13] : [];
                    let riRegistry_2: any[] = e[14] ? e[14] : [];
                    let riRegistry_3: any[] = e[15] ? e[15] : [];
                    contactTypeLang = (e.length > 1 && e[1].length > 0) ? e[1] : [];
                    contactTypeDetail = (e.length > 2 && e[2].length > 0) ? e[2] : [];
                    contactType.forEach(item => {
                        let filterData = contactTypeLang.find(langObj => (langObj.ContactTypeCode === item.ContactTypeCode));
                        let contactTypeDetailData = contactTypeDetail.find(obj => (obj.ContactTypeCode === item.ContactTypeCode));
                        this.pageParams.glValidForAdd = false;
                        if (contactTypeDetailData) {
                            for (let ctype of contactTypeDetail) {
                                if (ctype && (ctype.ContactTypeCode === item.ContactTypeCode) && (ctype.ContactCreateSecurityLevel <= userAuthority[0].ContactCreateSecurityLevel)) {
                                    if ((ctype['ValidForNewInd'] === true) && (ctype['URL'] === '?' || ctype['URL'] === '')) {
                                        this.pageParams.glValidForAdd = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (this.pageParams.glValidForAdd === true) {
                            this.ttContactType.push({
                                'ContactTypeCode': item.ContactTypeCode,
                                'ContactTypeDesc': (filterData) ? filterData.ContactTypeDesc : item.ContactTypeSystemDesc,
                                'ValidForNewInd': true
                            });
                            this.pageParams.glUserCanAdd = true;
                        }
                        else {
                            this.ttContactType.push({
                                'ContactTypeCode': item.ContactTypeCode,
                                'ContactTypeDesc': (filterData) ? filterData.ContactTypeDesc : item.ContactTypeSystemDesc,
                                'ValidForNewInd': false
                            });
                        }
                    });

                    if (this.ttContactType.length > 0) {
                        if (this.ttContactType.length > 0) {
                            this.ttContactType = this.orderBy.transform(this.ttContactType, 'ContactTypeDesc');
                        }
                        for (let obj of this.ttContactType) {
                            if (obj) {
                                if (obj.ValidForNewInd) {
                                    this.pageParams.gcContactTypeCodeAddList = this.pageParams.gcContactTypeCodeAddList
                                        + ((this.pageParams.gcContactTypeCodeAddList === '') ? '' : '^') + obj.ContactTypeCode;

                                    this.pageParams.gcContactTypeDescAddList = this.pageParams.gcContactTypeDescAddList
                                        + ((this.pageParams.gcContactTypeDescAddList === '') ? '' : '^') + obj.ContactTypeDesc;

                                    this.pageParams.gcDefaultContactType = (!this.pageParams.gcDefaultContactType) ? obj.ContactTypeCode : this.pageParams.gcDefaultContactType;
                                }
                                this.pageParams.gcContactTypeCodeFullList = this.pageParams.gcContactTypeCodeFullList
                                    + ((this.pageParams.gcContactTypeCodeFullList === '') ? '' : '^') + obj.ContactTypeCode;
                                this.pageParams.gcContactTypeDescFullList = this.pageParams.gcContactTypeDescFullList
                                    + ((this.pageParams.gcContactTypeDescFullList === '') ? '' : '^') + obj.ContactTypeDesc;

                            }
                        }
                    }

                    if (contactStatus && (contactStatus.length > 0)) {
                        this.contactStatusCodeSelectList = [];
                        for (let cStatus of contactStatus) {
                            if (cStatus) {
                                let tempObj = {
                                    ContactStatusCode: cStatus.ContactStatusCode,
                                    ContactStatusSystemDesc: cStatus.ContactStatusSystemDesc,
                                    ScheduledCloseStatusInd: cStatus.ScheduledCloseStatusInd
                                };
                                let contactStatusTemp = {
                                    text: cStatus.ContactStatusSystemDesc,
                                    value: cStatus.ContactStatusCode
                                };
                                let filterData = contactStatusLang.find(langObj => (langObj.ContactStatusCode === cStatus.ContactStatusCode));
                                tempObj.ContactStatusSystemDesc = (filterData) ? filterData.ContactStatusDesc : cStatus.ContactStatusSystemDesc;
                                contactStatusTemp.text = (filterData) ? filterData.ContactStatusDesc : cStatus.ContactStatusSystemDesc;
                                this.ttContactStatus.push(tempObj);
                                this.contactStatusCodeSelectList.push(contactStatusTemp);

                                if (cStatus.ScheduledCloseStatusInd) {
                                    this.pageParams.gcScheduledStatusList = this.pageParams.gcScheduledStatusList
                                        + ((this.pageParams.gcScheduledStatusList === '') ? '' : ',') + cStatus.ContactStatusCode;
                                }
                            }
                        }
                        if (this.contactStatusCodeSelectList.length > 0) {
                            this.contactStatusCodeSelectList = this.orderBy.transform(this.contactStatusCodeSelectList, 'ContactStatusSystemDesc');
                        }
                    }

                    if (contactMedium && (contactMedium.length > 0)) {
                        for (let cMedium of contactMedium) {
                            if (cMedium) {
                                let tempObj = { ContactMediumCode: cMedium.ContactMediumCode, ContactMediumSystemDesc: cMedium.ContactMediumSystemDesc };
                                let filterData = contactMediumLang.find(obj => (obj.ContactMediumCode === cMedium.ContactMediumCode));
                                if (filterData && filterData.ContactMediumDesc) {
                                    tempObj.ContactMediumSystemDesc = filterData.ContactMediumDesc;
                                }

                                this.ttContactMedium.push(tempObj);
                            }
                        }

                        if (this.ttContactMedium.length > 0) {
                            this.ttContactMedium = this.orderBy.transform(this.ttContactMedium, 'ContactMediumSystemDesc');
                        }
                    }

                    if (employee && (employee.length > 0)) {
                        this.pageParams.gcThisEmployeeCode = employee[0].EmployeeCode;
                        let filterData = branch.find(langObj => (langObj.BranchNumber === employee[0].BranchNumber));
                        if (filterData) {
                            this.pageParams.glDefaultKeepOwnership = !filterData.NationalAccountBranch;
                        }
                    }

                    if (contractType && (contractType.length > 0)) {
                        this.pageParams.gcContractTypeCodeList = '';
                        for (let cType of contractType) {
                            if (cType && cType.ContractTypeCode) {
                                this.pageParams.gcContractTypeCodeList = this.pageParams.gcContractTypeCodeList
                                    + ((!this.pageParams.gcContractTypeCodeList) ? '' : ',') + cType.ContractTypeCode;
                            }
                        }
                    }

                    if (riRegistry_0 && (riRegistry_0.length > 0)) {
                        this.pageParams.gcCanAmendContactTypeREG = riRegistry_0[0].RegValue;
                        this.pageParams.gcCanAmendContactTypeList = '';
                        if (this.pageParams.gcCanAmendContactTypeREG) {
                            for (let item in this.pageParams.gcCanAmendContactTypeREG.split(',')) {
                                if (item) {
                                    this.pageParams.gcCanAmendContactTypeList = this.pageParams.gcCanAmendContactTypeList + '^' + item + '^';
                                }
                            }
                        }
                    }

                    this.httpService.riGetErrorMessage(2886, this.utils.getCountryCode(), this.utils.getBusinessCode()).subscribe(
                        (e) => {
                            if (e.status === 'failure') {
                                this.errorService.emitError({ msg: e.oResponse });
                            } else {
                                this.pageParams.gcCannotAmendCTDError = e;
                            }
                        });

                    if (riRegistry_1 && (riRegistry_1.length > 0)) {
                        if (riRegistry_1[0].RegValue === 'Y') {
                            this.pageParams.glDisputedInvoicesInUse = true;
                        }
                    }

                    if (riRegistry_2 && (riRegistry_2.length > 0)) {
                        this.pageParams.gcDefaultNotificationType = riRegistry_2[0].RegValue;
                        this.pageParams.gcDefaultNotificationType = (this.pageParams.gcDefaultNotificationType === '') ? 'S' : this.pageParams.gcDefaultNotificationType;
                    }

                    if (this.pageParams.glDisputedInvoicesInUse) {
                        this.pageParams.glCanAmendInvoice = true;

                        let filterData = team.find(obj => (obj.LedgerTeamInd === true));
                        if (filterData) {
                            this.pageParams.glCanAmendInvoice = false;
                            for (let cType of teamUser) {
                                if (cType) {
                                    let data = team.find(obj => (obj.TeamID === filterData.TeamID));
                                    if (data && data.LedgerTeamInd) {
                                        this.pageParams.glCanAmendInvoice = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    this.pageParams.gcSchedulingURLAddress = (riRegistry_3 && (riRegistry_3.length > 0)) ? riRegistry_3[0].RegValue : '';
                }
                this.setPageParams();
                this.windowOnload();
            }

        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.errorService.emitError(error);
            });

        this.subscriptionManager.add(lookupScp);
    }

    //This method is used to process page params data
    private setPageParams(): void {
        this.pageParams.cScheduledStatusList = this.pageParams.gcScheduledStatusList;
        this.pageParams.cDefaultContactType = this.pageParams.gcDefaultContactType;
        this.pageParams.cContactTypeCodeFullList = this.pageParams.gcContactTypeCodeFullList;
        this.pageParams.cContactTypeDescFullList = this.pageParams.gcContactTypeDescFullList;
        this.pageParams.cContactTypeCodeAddList = this.pageParams.gcContactTypeCodeAddList;
        this.pageParams.cContactTypeDescAddList = this.pageParams.gcContactTypeDescAddList;
        this.pageParams.cLastContactTypeDetailCodeList = '';
        this.pageParams.cLastContactTypeDetailDescList = '';
        this.pageParams.cInitialMode = '';
        this.pageParams.cInitialContactTypeDetail = '';
        this.pageParams.lUserCanAdd = this.pageParams.glUserCanAdd;
        this.pageParams.cThisEmployeeCode = this.pageParams.gcThisEmployeeCode;
        this.pageParams.cContractTypeCodeList = this.pageParams.gcContractTypeCodeList;
        this.pageParams.lDefaultKeepOwnership = this.pageParams.glDefaultKeepOwnership;
        this.pageParams.CRAutoCreateProspect = this.pageParams.glSCAutoCreateProspect;
        this.pageParams.SCEnableAddressLine3 = this.pageParams.glSCEnableAddressLine3;
        this.pageParams.lContactTypeDetailAmendInd = this.pageParams.glContactTypeDetailAmendInd;
        this.pageParams.lRefreshTicketsGrid = true;
        this.pageParams.lRefreshLogsGrid = true;
        this.pageParams.lRefreshNotifyGrid = true;
        this.pageParams.lRefreshWorkOrderGrid = true;
        this.pageParams.lRefreshRootCauseGrid = true;
        this.pageParams.lRefreshDisputedInvoicesGrid = true;
        this.pageParams.iTabNum = '';
        this.pageParams.cCanAmendContactTypeList = this.pageParams.gcCanAmendContactTypeList;
        this.pageParams.cCannotAmendCTDError = this.pageParams.gcCannotAmendCTDError;
        this.pageParams.lAccountReviewInd = false;
        this.pageParams.lRootCauseInd = false;
        this.pageParams.cDefaultNotificationType = this.pageParams.gcDefaultNotificationType;
        this.pageParams.lDisputedInvoicesInUse = this.pageParams.glDisputedInvoicesInUse;
        this.pageParams.lOrigDisputedInvoicesInUse = this.pageParams.lDisputedInvoicesInUse;
        this.pageParams.lCanAmendInvoice = this.pageParams.glCanAmendInvoice;
        this.pageParams.cSchedulingURLAddress = this.pageParams.gcSchedulingURLAddress;
    }

    private loadFieldDescription(): void {
        let params: Object = {
            action: 5
        };
        let postData: Object = {
            ContactActionROWID: this.storeData['ContactAction'] || '',
            CustomerContactROWID: this.storeData['CustomerContact'] || '',
            AccountNumber: this.getControlValue('AccountNumber'),
            BusinessCode: this.utils.getBusinessCode(),
            ContractNumber: this.getControlValue('ContractNumber'),
            PremiseNumber: this.getControlValue('PremiseNumber'),
            ServiceCoverNumber: this.getControlValue('ServiceCoverNumber'),
            ProductCode: this.getControlValue('ProductCode'),
            NextActionEmployeeCode: this.getControlValue('NextActionEmployeeCode'),
            CreatedByEmployeeCode: this.getControlValue('CreatedByEmployeeCode'),
            CurrentOwnerEmployeeCode: this.getControlValue('CurrentOwnerEmployeeCode'),
            CurrentActionEmployeeCode: this.getControlValue('CurrentActionEmployeeCode'),
            LatestCreatedByEmployeeCode: this.getControlValue('LatestCreatedByEmployeeCode'),
            OriginatingEmployeeCode: this.getControlValue('OriginatingEmployeeCode'),
            DefaultAssigneeEmployeeCode: this.getControlValue('DefaultAssigneeEmployeeCode'),
            ServiceTypeCode: this.getControlValue('ServiceTypeCode')
        };
        this.executeRequest(this, '', params, postData, (data) => {
            if (data) {
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setValueIntoFormControl(data);
                }
            }
        });
    }

    private selectLatestTab(): any {
        //' this.procedure applies focus to the Latest tab
        let iTabNum: number = 0;
        //'Focus to 'Latest' tab
        if (this.currentMode !== MntConst.eModeAdd && this.getControlValue('CopyMode') !== 'Y') {
            if (this.pageParams.lDisputedInvoicesInUse) {
                iTabNum = 4;
            } else {
                iTabNum = 3;
            }
        } else {
            iTabNum = 1;
        }
        if ((this.getControlValue('RelatedNotify') === true) && this.getControlValue('CopyMode') !== 'Y') {
            iTabNum = iTabNum + 1;
        }
        if ((this.getControlValue('RelatedLogs') === true) && this.getControlValue('CopyMode') !== 'Y') {
            iTabNum = iTabNum + 1;
        }
        if ((this.getControlValue('RelatedTickets') === true) && this.getControlValue('CopyMode') !== 'Y') {
            iTabNum = iTabNum + 1;
        }
        if ((this.getControlValue('RelatedWorkOrder') === true) && this.getControlValue('CopyMode') !== 'Y') {
            iTabNum = iTabNum + 1;
        }
        if (this.pageParams.lRootCauseInd && this.currentMode !== MntConst.eModeAdd && this.getControlValue('CopyMode') !== 'Y') {
            iTabNum = iTabNum + 1;
        }

        let counter: number = 0;
        for (let key in this.tabList) {
            if (this.tabList.hasOwnProperty(key)) {
                let tab = this.tabList[key];
                if (tab && tab.visible) {
                    counter++;
                    if (iTabNum === counter) {
                        this.focusToTab(tab['id']);
                        break;
                    }
                }
            }
        }
    }

    private buildContactStatusList(): any {
        if (this.getControlValue('ContactTypeCode') !== this.getControlValue('OldContactTypeCode')) {
            this.contactStatusCodeSelectList = [];
            let valArray = this.getControlValue('ContactStatusCodeList').split('^');
            let descArray = this.getControlValue('ContactStatusDescList').split('^');
            for (let i = 0; i < valArray.length; i++) {
                this.contactStatusCodeSelectList.push({ value: valArray[i], text: descArray[i] });
            }
            if (this.trim(this.getControlValue('ContactStatusCode')) !== '') {
                this.setControlValue('ContactStatusCodeSelect', this.getControlValue('ContactStatusCode'));
            }
        }
    }

    private buildContactTypeDetailList(): any {
        if (this.getControlValue('ContactTypeCode') !== this.getControlValue('OldContactTypeCode')) {
            this.contactTypeDetailCodeSelectList = [];
            let valArray = this.pageParams.cLastContactTypeDetailCodeList.split('^');
            let descArray = this.pageParams.cLastContactTypeDetailDescList.split('^');
            for (let i = 0; i < valArray.length; i++) {
                if (this.utils.customTruthyCheck(valArray[i])) {
                    this.contactTypeDetailCodeSelectList.push({ value: valArray[i], text: descArray[i] });
                }
            }
        }
    }

    private buildContactTypeList(lFullListInd: any): any {
        this.contactTypeCodeSelectList = [];
        this.contactTypeDetailCodeSelectList = [];
        let cCodeList: string = '';
        let cDescList: string = '';
        if (lFullListInd) {
            cCodeList = this.pageParams.cContactTypeCodeFullList;
            cDescList = this.pageParams.cContactTypeDescFullList;
        } else {
            cCodeList = this.pageParams.cContactTypeCodeAddList;
            cDescList = this.pageParams.cContactTypeDescAddList;
        }

        let valArray = cCodeList.split('^');
        let descArray = cDescList.split('^');
        for (let i = 0; i < valArray.length; i++) {
            if (this.utils.customTruthyCheck(valArray[i])) {
                this.contactTypeCodeSelectList.push({ value: valArray[i], text: descArray[i] });
            }
        }
        this.setControlValue('ContactTypeCodeSelect', this.getControlValue('ContactTypeCode'));
    }

    private clearPremiseDetails(): any {
        this.setControlValue('PremiseNumber', '');
        this.setControlValue('PremiseName', '');
        this.clearProductDetails();
    }

    private clearProductDetails(): any {
        this.setControlValue('ProductCode', '');
        this.setControlValue('ProductDesc', '');
        this.setControlValue('ServiceCoverNumber', '');
        this.setAllSCRowID('');
        this.showHideServiceType(false);
    }

    private setScreenTo(rstrMode: any): any {
        this.currentScreenMode = rstrMode;
        this.pageParams.grdLatestTabLabel = false;
        for (let key in this.tabList) {
            if (this.tabList.hasOwnProperty(key)) {
                let tab = this.tabList[key];
                if (tab)
                    tab.visible = false;
            }
        }

        switch (rstrMode.toLowerCase()) {
            case 'view':
                this.saveCancelDisable = true;
                this.tabList['tab1'].visible = true;
                if (this.pageParams.lDisputedInvoicesInUse && this.currentMode !== MntConst.eModeAdd && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab2'].visible = true;
                }

                if (this.getControlValue('RelatedNotify') && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab3'].visible = true;
                }

                if (this.getControlValue('RelatedLogs') && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab4'].visible = true;
                }

                if (this.getControlValue('RelatedTickets') && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab5'].visible = true;
                }

                if (this.getControlValue('RelatedWorkOrder') && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab7'].visible = true;
                }

                if (this.pageParams.lRootCauseInd && this.currentMode !== MntConst.eModeAdd && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab6'].visible = false;
                    if (this.getControlValue('ClosedDate') && this.getControlValue('ContactTypeCallout') === false) {
                        this.fieldVisibility.cmdAddRootCause = false;
                    } else {
                        this.fieldVisibility.cmdAddRootCause = true;
                    }
                }

                this.tabList['tab8'].visible = true;
                this.tabList['tab9'].visible = true;

                //'Make Header cell of tab display correct mode
                this.setLatestTabHeader('latest');
                //'Make sure the 'Original' table is visible
                this.fieldVisibility.grdOriginal = true;
                //'Display/Hide fields
                this.fieldVisibility.trLatestCreated = true;     //' Show ContactAction Creation Details
                this.fieldVisibility.spnKeepOwnership = false; //' 'Keep Ownership' Checkbox
                this.fieldVisibility.spnChkContactPassedToActioner = false;
                this.fieldVisibility.tdCallOutLabel = false;
                this.fieldVisibility.tdCallOut = false;
                this.setControlValue('chkCreateCallOut', false);  //' Hide Checkbox
                this.buttonControls['cmdIntNotifications'].disabled = true;
                this.disableControl('ContactStatusCodeSelect', true);
                this.disableControl('ContactMediumCodeSelect', true);
                this.disableControl('ContactNotifyWhenSelect', true);
                this.disableControl('ContactNotifyTypeSelect', true);
                this.showHideServiceAreaCode(false);
                this.showHideSalesAreaCode(false);
                this.selectLatestTab();
                break;
            case 'add':
                this.saveCancelDisable = false;
                this.tabList['tab9'].visible = true;
                this.pageParams.grdLatestTabLabel = true;
                this.disableControl('CustomerContactNumber', true);
                this.setLatestTabHeader('new');
                this.fieldVisibility.grdOriginal = false;
                this.fieldVisibility.trLatestCreated = false; //' Hide ContactAction Creation Details
                this.fieldVisibility.spnKeepOwnership = true;     //' 'Keep Ownership' Checkbox
                this.fieldVisibility.spnChkContactPassedToActioner = true;
                this.fieldVisibility.ContactStatusCodeSelect = true;
                this.disableControl('ContactStatusCodeSelect', false);
                this.disableControl('ContactMediumCodeSelect', false);
                this.disableControl('ContactNotifyWhenSelect', false);
                this.disableControl('ContactNotifyTypeSelect', false);
                this.buttonControls['cmdIntNotifications'].disabled = false;

                this.showHideServiceAreaCode(false);
                this.showHideSalesAreaCode(false);
                this.showHideNextActionerOptions();
                this.setErrorStatus('ContactName', false);
                break;
            case 'update':
                if (this.currentMode !== MntConst.eModeAdd && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab1'].visible = true;
                }
                if (this.pageParams.lDisputedInvoicesInUse && this.currentMode !== MntConst.eModeAdd && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab2'].visible = true;
                }
                if (this.getControlValue('RelatedNotify') && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab3'].visible = true;
                }
                if (this.getControlValue('RelatedLogs') && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab4'].visible = true;
                }
                if (this.getControlValue('RelatedTickets') && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab5'].visible = true;
                }
                if (this.getControlValue('RelatedWorkOrder') && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab7'].visible = true;
                }
                if (this.pageParams.lRootCauseInd && this.currentMode !== MntConst.eModeAdd && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab6'].visible = true;
                    if (this.getControlValue('ClosedDate') && this.getControlValue('ContactTypeCallout') === false) {
                        this.fieldVisibility.cmdAddRootCause = false;
                    } else {
                        this.fieldVisibility.cmdAddRootCause = true;
                    }
                }

                this.tabList['tab8'].visible = false;
                this.tabList['tab9'].visible = false;

                if (this.currentMode !== MntConst.eModeAdd && this.getControlValue('CopyMode') !== 'Y') {
                    this.tabList['tab8'].visible = true;
                    this.tabList['tab9'].visible = true;
                    this.pageParams.grdLatestTabLabel = false;
                } else {
                    this.tabList['tab9'].visible = true;
                    this.pageParams.grdLatestTabLabel = true;
                }

                this.riTab.TabDraw();
                //'Disable Key Contract/Premise/Product Code Fields
                //'NEVER Allow the Account Number To Be Changed
                this.disableControl('AccountNumber', true);
                if (this.getControlValue('CopyMode') !== 'Y') {
                    this.disableControl('ContractNumber', true);
                    this.disableControl('PremiseNumber', true);
                    this.disableControl('ProductCode', true);
                }

                //'Disable Contact Type fields
                this.disableControl('ContactTypeCode', true);
                this.disableControl('ContactTypeDetailCode', true);

                //'Disable Fields In Original Tab
                this.disableControl('CustomerContactName', true);
                this.disableControl('CustomerContactPosition', true);
                this.disableControl('CustomerContactTelephone', true);
                this.disableControl('CustomerContactFax', true);
                this.disableControl('CustomerContactTicketReference', true);
                this.disableControl('CustomerContactMobileNumber', true);
                this.disableControl('CustomerContactEmailAddress', true);
                this.disableControl('OriginalContactMediumCode', true);
                this.disableControl('ShortNarrative', true);
                this.disableControl('ContactNarrative', true);

                this.fieldVisibility.trLatestCreated = false; //' Hide ContactAction Creation Details
                this.fieldVisibility.spnKeepOwnership = true;     //' 'Keep Ownership' Checkbox
                this.fieldVisibility.spnChkContactPassedToActioner = true;
                this.fieldVisibility.ContactStatusCodeSelect = true;
                this.disableControl('ContactStatusCodeSelect', false);
                if (this.getControlValue('CanAmendTicketReference') === 'N') {
                    this.disableControl('ContactMediumCodeSelect', true);
                } else {
                    this.disableControl('ContactMediumCodeSelect', false);
                }
                this.disableControl('ContactNotifyWhenSelect', false);
                this.disableControl('ContactNotifyTypeSelect', false);
                this.buttonControls['cmdIntNotifications'].disabled = false;

                this.showHideServiceAreaCode(false);
                this.showHideSalesAreaCode(false);
                this.showHideNextActionerOptions();
                this.selectLatestTab();
        }

        //'Make Header cell of tab display correct mode
        if (this.currentMode === MntConst.eModeAdd || this.getControlValue('CopyMode') === 'Y') {
            this.setLatestTabHeader('new');
        } else {
            this.setLatestTabHeader('latest');
        }

        if (this.currentMode === MntConst.eModeAdd && this.getControlValue('CopyMode') !== 'Y') {
            this.disableControl('ContactTypeCodeSelect', false);
            this.disableControl('ContactTypeDetailCodeSelect', false);
        } else {
            this.disableControl('ContactTypeCodeSelect', true);
            this.disableControl('ContactTypeDetailCodeSelect', true);
        }

        if (this.currentMode !== MntConst.eModeAdd) {
            this.fieldVisibility.tdCallOutLabel = false;
            this.fieldVisibility.tdCallOut = false;
        }

        this.utils.makeTabsNormal();

        if (this.isMobile) {
            this.processTabsForMobile = true;
        }
    }

    public setTabsForSmallDevice(): void {
        setTimeout(() => {
            const elemList: any = document.querySelectorAll('.screen-body .nav-tabs li:not(.hidden)'); // document.querySelectorAll('.screen-body .nav-tabs li');
            const activeTab: any = document.querySelector('.screen-body .nav-tabs li.nav-item.active');
            const sdTabWrapper: any = document.getElementsByClassName('sd-tab-wrapper')[0];
            let activeTabIdx: number = Array.prototype.indexOf.call(elemList, activeTab) || 0;
            this.totalWidth = 0;

            elemList.forEach((item: any) => { // fetching width to all LIs
                if (item['offsetWidth'] > 0) {
                    this.totalWidth += item['offsetWidth'];
                }
                this.totalWidth += 5;
            });

            if (activeTabIdx >= 1 && activeTabIdx < elemList.length) {  // if not first one and last one
                let distanceToScroll: number = 0;

                elemList.forEach((ele: any, i: number) => { // i=0, activeTabIdx=1
                    if (i === (activeTabIdx - 1)) {
                        distanceToScroll += ele['offsetWidth'] / 2;  //half width of just before one
                    } else if (i < (activeTabIdx - 1)) { // only for elem which are prev
                        if ((i + 1) < (activeTabIdx - 1)) {
                            distanceToScroll += ele['offsetWidth']; //full width
                        }
                        if ((i + 1) === (activeTabIdx - 1)) { // just prev sibiling, then take half
                            distanceToScroll += ele['offsetWidth'] / 2;  //half width of just before one
                        }
                    }
                });
                sdTabWrapper['scrollLeft'] = 0; // reset

                setTimeout(() => {
                    sdTabWrapper['scrollLeft'] += distanceToScroll;
                }, 500);
            }
        }, 400);

    }

    private showHideNextActionerOptions(): any {
        if (this.trim(this.getControlValue('NextActionEmployeeCode')) === this.trim(this.getControlValue('ThisEmployeeCode'))) {
            this.showHideAssignToOptions(false);
        } else {
            this.showHideAssignToOptions(true);
        }
    }

    private showHideAssignToOptions(blnShow: any): any {
        if (blnShow) {
            this.fieldVisibility.spnKeepOwnership = true;
            if ((this.currentMode === MntConst.eModeAdd) && (this.getControlValue('ContactTypeCallout') === true)) {
                this.fieldVisibility.spnChkContactPassedToActioner = false;
            } else {
                this.fieldVisibility.spnChkContactPassedToActioner = true;
            }
            if (this.currentMode === MntConst.eModeAdd) {
                let typeCode = this.getControlValue('ContactTypeDetailCodeSelect');
                //start region : This section has been added to avoid the fallback scenario when user change the Ticket type drop down list
                let typeDetailList: Array<any> = this.getControlValue('ContactTypeDetailCodeList').split('^');
                let clist: any = typeDetailList.find(item => (item === typeCode));
                if ((!clist) && typeDetailList.length > 0) {
                    typeCode = typeDetailList[0];
                }
                // end region
                if ((!this.getControlValue('ContactTypeCallout')) && this.instr(this.getControlValue('ContactTypeDetailPassedList'), '^' + typeCode + '^') >= 0) {
                    this.setControlValue('chkContactPassedToActioner', true);
                } else {
                    this.setControlValue('chkContactPassedToActioner', false);
                }
            }
        }
        else {
            this.fieldVisibility.spnKeepOwnership = true;
            this.fieldVisibility.spnChkContactPassedToActioner = true;
            this.setAttribute('chkContactPassedToActionerriOldValueChecked', false);
            this.setAttribute('chkKeepOwnershipriOldValueChecked', false);
            this.fieldVisibility.spnKeepOwnership = false;
            this.fieldVisibility.spnChkContactPassedToActioner = false;
        }
    }

    private getStaticServerInfo(): any {
        //' These defaults are now taken from variables set up within the top part of this.procedure
        this.setControlValue('ThisEmployeeCode', this.pageParams.cThisEmployeeCode);
        this.setControlValue('ContractTypeCodeList', this.pageParams.cContractTypeCodeList);
        this.setControlValue('DefaultKeepOwnership', this.pageParams.lDefaultKeepOwnership);
    }

    private riMaintenanceAfterEvent(): any {
        this.buildMenus();
        //'Determine if (export button should be shown
        this.showHideExportButton();
    }

    private riMaintenanceBeforeAddMode(): any {
        this.fieldVisibility.tdCopyTicketReturn = false;
        this.setControlValue('CopyMode', 'N');
        this.fieldVisibility.tdCopyTicket = false;
        if (this.pageParams.lDisputedInvoicesInUse) {
            this.fieldVisibility.tdSelectDisputedInvoices = true;
        }
        this.hideExportButton();
        this.contactStatusCodeSelectOnChange();

        if (this.pageParams.glMultiContactInd) {
            this.fieldVisibility.spanBtnAmendContact = true;
        }
    }

    private riMaintenanceBeforeSelectMode(): any {
        this.hideExportButton();
    }

    private riMaintenanceBeforeUpdateMode(): any {
        this.fieldVisibility.tdCopyTicketReturn = false;
        if ((this.getControlValue('ContactTypeClientRetention') === true)) {
            this.showHideClientRetention(false);
            this.setRequiredStatus('EarliestEffectDate', false);
            this.fieldRequired['EarliestEffectDate'] = false;
            this.disableControl('EarliestEffectDate', true);
        } else {
            this.showHideClientRetention(true);
        }
        this.setControlValue('CopyMode', 'N');
        this.contactStatusCodeSelectOnChange();

        if (this.pageParams.glMultiContactInd) {
            this.fieldVisibility.spanBtnAmendContact = true;
        }
    }

    private riMaintenanceBeforeAdd(): any {
        this.fieldVisibility.tdCopyTicketReturn = false;
        this.setControlValue('CopyMode', 'N');
        this.setControlValue('CustomerContactNumber', '0');
        this.setControlValue('SMSSendOnDate', new Date());
        this.setControlValue('SMSSendOnTime', this.getShortTime());
        this.buildContactTypeList(false);
        this.contactStatusCodeSelectOnChange();
        if (!this.getControlValue('ThisEmployeeCode')) {
            this.getStaticServerInfo();
        }

        //'Default next actioner to current employee.
        this.setControlValue('NextActionEmployeeCode', this.getControlValue('ThisEmployeeCode'));
        //'Default KeepOwnership tick box
        this.setControlValue('chkKeepOwnership', this.getControlValue('DefaultKeepOwnership'));
        this.setScreenTo('add');
    }

    private riMaintenanceBeforeUpdate(): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        let status: boolean = true;
        this.fieldVisibility.tdCopyTicketReturn = false;
        this.setControlValue('CopyMode', 'N');
        //' Dont Allow Copying From Client Retention, Callouts or Prospect Related Tickets

        if ((!this.getControlValue('ContactTypeClientRetention')) && !this.getControlValue('CallOutRowID')) { //IUI-23421
            this.fieldVisibility.tdCopyTicket = true;
        }

        if (!this.getControlValue('ThisEmployeeCode')) {
            this.getStaticServerInfo();
        }

        this.contactStatusCodeSelectOnChange();
        this.disableControl('ContactNotifyWhenSelect', true);
        this.disableControl('ContactNotifyTypeSelect', true);
        this.buttonControls['cmdIntNotifications'].disabled = true;

        if (this.getControlValue('ClosedDate')) {
            if (this.getControlValue('ProspectNumber') && this.trim(this.getControlValue('ProspectNumber')) !== '' && this.getControlValue('ContractNumber') && this.trim(this.getControlValue('ContractNumber')) !== '') {
                status = false;
                this.cancelEvent = true;
                this.saveCancelDisable = true;
                if (!this.isNavReturnStatus) {
                    let modalVo: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.customerContactMaintenance.This_prospect_has_been_converted_You_cannot_reopen_it);
                    modalVo.title = MessageConstant.Message.Information;
                    this.modalAdvService.emitMessage(modalVo);
                }
                retObj.next(false);
                return retObj;
            }
            else {
                status = false;
                let yesSuspension: any = function yesSuspension(): any {
                    this.pageParams['BeforeUpdate1'] = 'Y';
                    this.setScreenTo('update');
                    let riBeforeUpdate: Subscription = this.riMaintenanceBeforeUpdate_section().subscribe((e) => {
                        if (e === false) {
                            this.cancelEvent = true;
                            this.saveCancelDisable = true;
                            retObj.next(false);
                        }
                        else {
                            retObj.next(true);
                        }
                    });
                    this.subscriptionManager.add(riBeforeUpdate);
                }.bind(this);

                let cancelSuspension: any = function cancelSuspension(): any {
                    this.pageParams['BeforeUpdate1'] = 'N';
                    this.cancelEvent = true;
                    this.saveCancelDisable = true;
                    // CR:: When user click on 'NO' button , application will navigate to the previous screen
                    this.location.back();
                    //retObj.next(false);
                }.bind(this);

                if ((this.isNavReturnStatus || this.isSaveReloadStatus) && this.pageParams['BeforeUpdate1']) {
                    if (this.pageParams['BeforeUpdate1'] === 'Y') {
                        yesSuspension();
                    }
                    else {
                        cancelSuspension();
                    }
                } else if (this.pageParams.isPageLoading) {
                    let promptVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.customerContactMaintenance.This_Contact_has_been_closed_Do_you_want_to_open_it, null, yesSuspension, cancelSuspension);
                    promptVO.title = MessageConstant.PageSpecificMessage.customerContactMaintenance.Contact_Management;
                    promptVO.confirmLabel = 'Yes';
                    promptVO.cancelLabel = 'No';
                    this.modalAdvService.emitPrompt(promptVO);
                    this.pageParams.isPageLoading = false;
                }
            }
        }
        else {
            this.setScreenTo('update');
        }
        if (status === true) {
            let riSubs: Subscription = this.riMaintenanceBeforeUpdate_section().subscribe((e) => {
                if (e === false) {
                    this.cancelEvent = true;
                    this.saveCancelDisable = true;
                    retObj.next(false);
                }
                else {
                    retObj.next(true);
                }
            });
            this.subscriptionManager.add(riSubs);
        }

        return retObj;
    }

    private riMaintenanceBeforeUpdate_section(): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        //' Cannot allow amendment of all contacttypedetails - CR creates too many other records
        if (this.pageParams.lContactTypeDetailAmendInd && (this.getControlValue('ContactTypeClientRetention') === false) && this.instr(this.pageParams.cCanAmendContactTypeList, '^' + this.getControlValue('ContactTypeCodeSelect') + '^') >= 0) {
            this.disableControl('ContactTypeDetailCodeSelect', false);
        } else {
            this.disableControl('ContactTypeDetailCodeSelect', true);
        }

        if (this.getControlValue('WarnRedirectEmployeeCode')) {
            let msg = this.translatedMessageList.This_Contact_has_been_closed_Do_you_want_to_open_it + String.fromCharCode(10)
                + this.getControlValue('WarnRedirectEmployeePeriod') + String.fromCharCode(10) + String.fromCharCode(10) +
                this.translatedMessageList.Their_work_is_being_redirected_to + String.fromCharCode(10) +
                this.getControlValue('WarnRedirectEmployeeCode') + ' ' + this.getControlValue('WarnRedirectEmployeeName') + String.fromCharCode(10) + String.fromCharCode(10)
                + this.translatedMessageList.Do_you_want_to_reassign_this_ticket_now;

            let yesPrompt = function yesPrompt(): any {
                this.pageParams['BeforeUpdate2'] = 'Y';
                this.setControlValue('NextActionEmployeeCode', this.getControlValue('WarnRedirectEmployeeCode'));
                this.setControlValue('NextActionEmployeeName', this.getControlValue('WarnRedirectEmployeeName'));
                retObj.next(true);
            }.bind(this);

            let cancelPrompt = function cancelPrompt(): any {
                this.pageParams['BeforeUpdate2'] = 'N';
                retObj.next(false);
            }.bind(this);

            if ((this.isNavReturnStatus || this.isSaveReloadStatus) && this.pageParams['BeforeUpdate2']) {
                if (this.pageParams['BeforeUpdate2'] === 'Y') {
                    yesPrompt();
                }
                else {
                    cancelPrompt();
                }
            }
            else {
                let promptVO: ICabsModalVO = new ICabsModalVO(msg, null, yesPrompt, cancelPrompt);
                promptVO.title = MessageConstant.PageSpecificMessage.customerContactMaintenance.Assignee_Not_Available;
                promptVO.cancelLabel = 'No';
                promptVO.confirmLabel = 'Yes';
                this.modalAdvService.emitPrompt(promptVO);
            }
        }
        else {
            retObj.next(true);
        }
        return retObj;
    }

    private riMaintenanceAfterAbandon(): any {
        this.disableControl('cmdCopyTicket', false);
        this.fieldVisibility.tdCopyTicket = false;
        this.setControlValue('CopyFromTicketNumber', '0');
        this.fieldVisibility.tdCopyTicketReturn = false;
        if (this.pageParams.lDisputedInvoicesInUse) {
            this.fieldVisibility.tdSelectDisputedInvoices = false;
        }
        if (this.pageParams.glMultiContactInd) {
            this.fieldVisibility.spanBtnAmendContact = false;
        }

        if (this.getControlValue('CopyMode') === 'Y') {
            this.setControlValue('ContactTypeCode', this.getControlValue('SaveCopyContactTypeCode'));
            this.setControlValue('ContactTypeCodeSelect', this.getControlValue('SaveCopyContactTypeCode'));
        }

        if (this.getControlValue('CopyMode') === 'Y') {
            this.setControlValue('ContactTypeDetailCode', this.getControlValue('SaveCopyContactTypeDetailCode'));
            this.setControlValue('ContactTypeDetailCodeSelect', this.getControlValue('SaveCopyContactTypeDetailCode'));
            this.pageParams.cInitialContactTypeDetail = this.getControlValue('SaveCopyContactTypeDetailCode');
        }

        this.setControlValue('CopyMode', 'N');
        this.setScreenTo('view');
        this.showHideExportButton();
        if ((this.parentMode === 'CallCentreSearch') || (this.parentMode === 'CallCentreSearchNew') || (this.parentMode === 'ContactRelatedTicket')) {
            setTimeout(() => {
                this.location.back();
            }, 100);
        }
    }

    private riMaintenanceAfterSaveForUpdate(): boolean {
        let isCreateCallout: boolean = this.getControlValue('chkCreateCallOut');
        let isReloadStatus: boolean = true;
        if (this.getControlValue('CopyMode') === 'Y') {
            this.fieldVisibility.tdCopyTicketReturn = true;
            this.setControlValue('CopyFromTicketNumberSave', this.getControlValue('CopyFromTicketNumber'));
        }

        this.setControlValue('CopyFromTicketNumber', '0');
        this.setControlValue('CopyMode', 'N');

        if (this.getControlValue('ServiceBranchNumber') === '0' || this.getControlValue('ServiceBranchNumber')) {
            this.setControlValue('ServiceBranchNumber', this.utils.getBranchCode());
        }
        if (isCreateCallout || this.parentMode === 'CallCentreSearchNew' || this.parentMode === 'ContactRelatedTicket' || this.parentMode === 'EmployeeDiary') {
            isReloadStatus = false;
            setTimeout(() => {
                if (isCreateCallout) {
                    this.pageParams.previousRouteUrl = InternalMaintenanceServiceModuleRoutes.ICABSCMCALLOUTMAINTENANCE;
                    this.navigate('CreateCallOut', InternalMaintenanceServiceModuleRoutes.ICABSCMCALLOUTMAINTENANCE);
                } else {
                    if (this.parentMode === 'CallCentreSearchNew' || this.parentMode === 'ContactRelatedTicket' || this.parentMode === 'EmployeeDiary') {
                        this.location.back();
                    }
                }
            }, 0);
        }
        this.isSaved = true;
        return isReloadStatus;
    }

    private riMaintenanceAfterSave(): boolean {
        let isCreateCallout: boolean = this.getControlValue('chkCreateCallOut');
        let isReloadStatus: boolean = true;
        if (this.getControlValue('CopyMode') === 'Y') {
            this.fieldVisibility.tdCopyTicketReturn = true;
            this.setControlValue('CopyFromTicketNumberSave', this.getControlValue('CopyFromTicketNumber'));
        }

        this.setControlValue('CopyFromTicketNumber', '0');
        this.setControlValue('CopyMode', 'N');
        this.disableControl('cmdCopyTicket', false);
        this.fieldVisibility.tdCopyTicket = false;

        if (this.pageParams.lDisputedInvoicesInUse) {
            this.fieldVisibility.tdSelectDisputedInvoices = false;
        }

        if (this.pageParams.glMultiContactInd) {
            this.fieldVisibility.spanBtnAmendContact = false;
        }

        this.pageParams.lRefreshNotifyGrid = true;

        this.setScreenTo('view');
        this.buildAddressTabInfo();

        if (this.getControlValue('ServiceBranchNumber') === '0' || this.getControlValue('ServiceBranchNumber')) {
            this.setControlValue('ServiceBranchNumber', this.utils.getBranchCode());
        }

        if (isCreateCallout || this.parentMode === 'CallCentreSearchNew' || this.parentMode === 'ContactRelatedTicket' || this.parentMode === 'EmployeeDiary') {
            isReloadStatus = false;
            setTimeout(() => {
                if (isCreateCallout) {
                    this.pageParams.previousRouteUrl = InternalMaintenanceServiceModuleRoutes.ICABSCMCALLOUTMAINTENANCE;
                    this.navigate('CreateCallOut', InternalMaintenanceServiceModuleRoutes.ICABSCMCALLOUTMAINTENANCE);
                } else {
                    if (this.parentMode === 'CallCentreSearchNew' || this.parentMode === 'ContactRelatedTicket' || this.parentMode === 'EmployeeDiary') {
                        this.location.back();
                    }
                }
            }, 0);
        }
        this.isSaved = true;
        return isReloadStatus;
    }

    private riMaintenanceBeforeSave(): any {
        let lCannotAmendCTD;
        this.setControlValue('ShortNarrative', this.replace(this.getControlValue('ShortNarrative'), String.fromCharCode(34), '\''));
        this.setControlValue('ContactNarrative', this.replace(this.getControlValue('ContactNarrative'), String.fromCharCode(34), '\''));
        this.setControlValue('ShortDescription', this.replace(this.getControlValue('ShortDescription'), String.fromCharCode(34), '\''));
        this.setControlValue('Comments', this.replace(this.getControlValue('Comments'), String.fromCharCode(34), '\''));
        //' Used To Link Prospect And None-Account Tickets Together
        if (this.currentMode === MntConst.eModeAdd) {
            this.setControlValue('FromCustomerContactNumber', this.riExchange.getParentHTMLValue('SelectedTicketNumber'));
        }
        this.setControlValue('ContactTypeCode', this.getControlValue('ContactTypeCodeSelect'));
        this.setControlValue('ContactTypeDetailCode', this.getControlValue('ContactTypeDetailCodeSelect'));
        this.setControlValue('ContactStatusCode', this.getControlValue('ContactStatusCodeSelect'));
        this.setControlValue('ContactMediumCode', this.getControlValue('ContactMediumCodeSelect'));
        this.setControlValue('ContactNotifyWhen', this.getControlValue('ContactNotifyWhenSelect'));
        this.setControlValue('ContactNotifyType', this.getControlValue('ContactNotifyTypeSelect'));
        this.setControlValue('IntNotifySubjectText', this.getControlValue('SaveIntNotifySubjectText'));
        this.setControlValue('IntNotifyBodyText', this.getControlValue('SaveIntNotifyBodyText'));

        if (this.getControlValue('ContactNotifyWhen') === '-1') {
            this.cancelEvent = true;
            this.setFocusToControl('ContactNotifyWhenSelect');
        }
        this.customBusinessObjectAdditionalPostData = '';
    }

    private riMaintenanceAfterFetch(): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        let status: boolean = false;
        let cSaveContactTypeDetailCode;
        this.disableControl('ContactStatusCodeSelect', true);

        if (this.getControlValue('CanAmendTicketReference') === 'N') {
            this.disableControl('TicketReference', true);
            this.disableControl('ContactMediumCodeSelect', true);
        } else {
            this.disableControl('TicketReference', false);
        }

        this.setControlValue('SaveIntNotifySubjectText', this.getControlValue('IntNotifySubjectText'));
        this.setControlValue('SaveIntNotifyBodyText', this.getControlValue('IntNotifyBodyText'));

        if (this.getControlValue('AccountNumber') === '') {
            this.pageParams.lDisputedInvoicesInUse = false;
        } else {
            this.pageParams.lDisputedInvoicesInUse = this.pageParams.lOrigDisputedInvoicesInUse;
        }
        this.buildAddressTabInfo();
        if (!this.pageParams.lReturningFromChild) {
            if (this.currentMode !== MntConst.eModeAdd) {
                this.buildContactTypeList(true);
            } else {
                this.buildContactTypeList(false);
            }
        }

        if (this.getControlValue('CurrentTeamID')) {
            this.fieldVisibility.spanTeamDetails = true;
        } else {
            this.fieldVisibility.spanTeamDetails = false;
        }

        this.setControlValue('OriginalContactMediumCodeSelect', this.getControlValue('OriginalContactMediumCode'));
        this.setControlValue('ContactTypeCodeSelect', this.getControlValue('ContactTypeCode'));
        cSaveContactTypeDetailCode = this.getControlValue('ContactTypeDetailCode');
        this.setControlValue('ContactMediumCodeSelect', this.getControlValue('ContactMediumCode'));

        let obs_1: Observable<any>;
        obs_1 = new Observable(observer => {
            let closedTime: string = this.getControlValue('ClosedTime');
            let closedDate: string = this.getRawControlValue('ClosedDate');
            if (closedDate || closedTime) {
                this.setControlValue('ClosedDateTime', this.globalize.formatDateToLocaleFormat(closedDate) + ' ' + this.globalize.formatTimeToLocaleFormat(closedTime));
            }
            if ((!this.getControlValue('ProspectNumber') && this.trim(this.getControlValue('ProspectNumber')) === '') && (!this.getControlValue('LinkedProspectNumber') && this.trim(this.getControlValue('LinkedProspectNumber')) === '')) {
                this.fieldVisibility.tdPostcodelabel = true;
                this.fieldVisibility.tdPostCode = true;
                this.fieldVisibility.tdProspect1 = false;
                this.fieldVisibility.tdProspect2 = false;
            } else {
                this.fieldVisibility.tdPostcodelabel = false;
                this.fieldVisibility.tdPostCode = false;
                this.fieldVisibility.tdProspect1 = true;
                this.fieldVisibility.tdProspect2 = true;
            }

            if (this.utils.customTruthyCheck(this.getControlValue('ProspectNumber')) && (this.trim(this.getControlValue('ProspectNumber')) !== '')) {
                this.setControlValue('DispProspectNumber', this.getControlValue('ProspectNumber'));
            }

            if (this.utils.customTruthyCheck(this.getControlValue('LinkedProspectNumber')) && (this.trim(this.getControlValue('LinkedProspectNumber')) !== '')) {
                this.setControlValue('DispProspectNumber', this.getControlValue('LinkedProspectNumber'));
                this.fieldVisibility.btnProspect = false;
            }

            if (!this.isInitialising) {
                this.contactStatusCodeSelectOnChange();
            }

            if ((this.getControlValue('ContactTypeClientRetention') === true)) {
                this.showHideClientRetention(false);
            } else {
                this.showHideClientRetention(true);
            }

            this.showHideServiceType((this.getControlValue('ProductCode')));
            this.nextActionEmployeeCodeOnChange();
            observer.next(true);
        });


        if (this.currentMode !== MntConst.eModeAdd && this.currentMode !== MntConst.eModeNormal) {
            status = true;
            let serverDataSubs: Subscription = this.getServerData('GetDetailsOnEntryUpdate').subscribe((e1) => {
                this.setControlValue('ContactTypeDetailCodeSelect', cSaveContactTypeDetailCode);
                this.setControlValue('ContactTypeDetailCode', cSaveContactTypeDetailCode);
                let sub1: Subscription = obs_1.subscribe((e1) => {
                    retObj.next(true);
                });
                this.subscriptionManager.add(sub1);
            });
            this.subscriptionManager.add(serverDataSubs);
        }

        if (status === false) {
            let sub2: Subscription = obs_1.subscribe((e1) => {
                retObj.next(true);
            });
            this.subscriptionManager.add(sub2);
        }
        return retObj;
    }

    private buildAddressTabInfo(): any {
        let valArray: any;
        let descArray: any;
        this.setControlValue('AddressCodeSelect', '');
        valArray = this.getControlValue('AddressCodeList').split('^');
        descArray = this.getControlValue('AddressDescList').split('^');
        this.addressCodeSelectList = [];
        for (let i = 0; i < valArray.length; i++) {
            this.addressCodeSelectList.push({ value: valArray[i], text: descArray[i] });
        }
        if (this.getControlValue('AddressCodeDefault')) {
            this.setControlValue('AddressCodeSelect', this.getControlValue('AddressCodeDefault'));
            this.addressCodeSelectOnChange();
        }
    }

    private buildMenus(): any {
        this.menuOptionList = [];
        let cContractTypeDescr: string;
        let cContractTypeValue: string;
        if (this.getControlValue('ContractTypeDesc')) {
            //' When language based - the 'Contract' text was not being translated
            //' When language other than english - the 'Value' was the languicised version so contract/job option didn't work
            if (this.getControlValue('ContractTypeCode') === 'P') {
                cContractTypeDescr = 'Product Sale';
                cContractTypeValue = 'product sale';
            } else {
                if (this.getControlValue('ContractTypeCode') === 'J') {
                    cContractTypeDescr = 'Job';
                    cContractTypeValue = 'job';
                } else {
                    cContractTypeDescr = 'Contract';
                    cContractTypeValue = 'contract';
                }
            }
        }

        if ((this.trim(this.getControlValue('ProspectNumber')) !== '') && (this.currentMode !== MntConst.eModeAdd)) {
            if (this.getControlValue('ContactTypeCallout')) {
                this.menuOptionList.push({ value: 'callout', text: 'Callouts' });
            }

            this.menuOptionList.push({ value: 'detail', text: 'Ticket History' });
            this.menuOptionList.push({ value: 'history', text: 'Event History' });

            if (this.getControlValue('AccountNumber')) {
                this.menuOptionList.push({ value: 'contacts', text: 'Contact Details' });
                this.menuOptionList.push({ value: 'account', text: 'Account' });
            }

            if (!this.getControlValue('ContactTypeClientRetention') && this.getControlValue('UserAuthContractUpdate')) {
                if (this.instr(this.getControlValue('ContractTypeCodeList'), 'C') >= 0) {
                    this.menuOptionList.push({ value: 'tocontract', text: 'Convert to Contract' });
                }

                if (this.instr(this.getControlValue('ContractTypeCodeList'), 'J') >= 0) {
                    this.menuOptionList.push({ value: 'tojob', text: 'Convert to Job' });
                }

                if (this.instr(this.getControlValue('ContractTypeCodeList'), 'P') >= 0) {
                    this.menuOptionList.push({ value: 'toproductsale', text: 'Convert to Product Sale' });
                }
            }

            if (this.getControlValue('ContractTypeDesc')) {
                this.menuOptionList.push({ value: cContractTypeValue, text: cContractTypeDescr });
            }

            if (this.trim(this.getControlValue('PremiseNumber')) !== '') {
                this.menuOptionList.push({ value: 'premise', text: 'Premises' });
            }

            if (this.getControlValue('ContactTypeClientRetention')) {
                this.menuOptionList.push({ value: 'lostbusiness', text: 'Client Retention' });
            }

            if (this.getControlValue('AccountNumber') || this.getControlValue('ContractNumber') || this.getControlValue('PremiseNumber')) {
                this.menuOptionList.push({ value: 'contactcentresearch', text: 'Contact Centre Search' });
            }

        } else {
            if (this.getControlValue('ContactTypeCallout')) {
                this.menuOptionList.push({ value: 'callout', text: 'Callouts' });
            }
            this.menuOptionList.push({ value: 'detail', text: 'Ticket History' });
            this.menuOptionList.push({ value: 'history', text: 'Event History' });

            if (this.getControlValue('AccountNumber')) {
                this.menuOptionList.push({ value: 'contacts', text: 'Contact Details' });
                this.menuOptionList.push({ value: 'account', text: 'Account' });
            }

            if (this.getControlValue('ContractTypeDesc')) {
                this.menuOptionList.push({ value: cContractTypeValue, text: cContractTypeDescr });
            }

            if (this.trim(this.getControlValue('PremiseNumber')) !== '') {
                this.menuOptionList.push({ value: 'premise', text: 'Premises' });
            }

            if (this.getControlValue('ContactTypeClientRetention')) {
                this.menuOptionList.push({ value: 'lostbusiness', text: 'Client Retention' });
            }

            if (this.pageParams.lAccountReviewInd === true) {
                this.menuOptionList.push({ value: 'campaign', text: 'Campaign/Survey Entry' });
            }

            if (this.getControlValue('AccountNumber') || this.getControlValue('ContractNumber') || this.getControlValue('PremiseNumber')) {
                this.menuOptionList.push({ value: 'contactcentresearch', text: 'Contact Centre Search' });
            }
        }
    }

    private riExchange_CBORequest(): any {
        if (this.uiForm.controls['ShortDescription'].dirty) {
            if (this.getControlValue('Comments') && this.trim(this.getControlValue('Comments')) === '') {
                this.setControlValue('Comments', this.getControlValue('ShortDescription'));
            }
        }
    }

    //' Gets details && sets fields for the NextActionEmployeeCode
    private getEmployeeDetails(): any {
        let strAddMode: string;
        if (this.currentMode === MntConst.eModeAdd) {
            strAddMode = 'Yes';
        } else {
            strAddMode = 'No';
        }

        this.CBORequestClear();
        this.customBusinessObjectAdditionalPostData = 'Function=GetAllowCallOut,GetEmployeeDetails' +
            '&AddMode=' + strAddMode;

        this.CBORequestAddCS('BusinessCode');
        this.CBORequestAddCS('BranchNumber');

        if (this.trim(this.getControlValue('ContractNumber')) !== '') {
            this.CBORequestAdd('ContractNumber');
        }
        if (this.trim(this.getControlValue('PremiseNumber')) !== '') {
            this.CBORequestAdd('PremiseNumber');
        }

        this.CBORequestAdd('NextActionEmployeeCode');
        this.CBORequestExecute((data) => {
            if (!data.hasError) {
                this.setValueIntoFormControl(data);
                if (this.getControlValue('ContactTypeCallout') && this.getControlValue('AllowCallout')) {
                    this.disableControl('chkCreateCallOut', false);
                    this.setControlValue('chkCreateCallOut', true);
                } else {
                    this.disableControl('chkCreateCallOut', true);
                    this.setControlValue('chkCreateCallOut', false);
                }
            }
        });

        this.CBORequestClear();
        this.customBusinessObjectAdditionalPostData = '';
    }

    //' Gets ServiceCoverNumber
    private getServiceCoverNumber(): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        if (this.getAttribute('ProductCodeSCRowID')) {
            let sub1: Subscription = this.getServiceCoverNumberFromRowID().subscribe((e1) => {
                retObj.next(true);
            });
            this.subscriptionManager.add(sub1);
        } else {
            let sub2: Subscription = this.getServiceCoverNumberFromRecord().subscribe((e2) => {
                retObj.next(true);
            });
            this.subscriptionManager.add(sub2);
        }
        return retObj;
    }

    // ' Gets ServiceCoverNumber using RowID
    private getServiceCoverNumberFromRowID(): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        this.CBORequestClear();
        this.customBusinessObjectAdditionalPostData = { Function: 'GetServiceCoverFromRowID', SCRowID: this.getAttribute('ProductCodeSCRowID') };
        this.CBORequestExecute((data) => {
            if (!data.hasError) {
                this.setValueIntoFormControl(data);
                this.setAllSCRowID(this.getAttribute('ProductCodeSCRowID'));
                this.CBORequestClear();
                this.customBusinessObjectAdditionalPostData = {};
                this.showHideServiceType(true);
            }
            retObj.next(true);
        });

        return retObj;
    }

    private riMaintenanceBeforeConfirm(): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        //' When Adding a new record we need to ensure that we can relate this type of ticket to another
        if (this.currentMode === MntConst.eModeAdd) {
            this.setControlValue('FromCustomerContactNumber', this.riExchange.getParentHTMLValue('SelectedTicketNumber'));
        }
        if (this.currentMode === MntConst.eModeAdd && this.getControlValue('FromCustomerContactNumber')) {
            this.setControlValue('FromCustomerContactNumber', this.riExchange.getParentHTMLValue('SelectedTicketNumber'));

            let postDataAdd = {
                Action: '6',
                Function: 'ValidateCanRelateTicket',
                BusinessCode: this.utils.getBusinessCode(),
                ContactTypeCode: this.getControlValue('ContactTypeCode'),
                ContactTypeDetailCode: this.getControlValue('ContactTypeDetailCode'),
                FromCustomerContactNumber: this.getControlValue('FromCustomerContactNumber'),
                AccountNumber: this.getControlValue('AccountNumber'),
                ContractNumber: this.getControlValue('ContractNumber'),
                PremiseNumber: this.getControlValue('PremiseNumber'),
                ContactAddressName: this.getControlValue('PublicAddressName'),
                ContactName: this.getControlValue('ContactName'),
                ContactPostcode: this.getControlValue('ContactPostcode')
            };

            this.executeRequest(this, 'ValidateCanRelateTicket', '', postDataAdd, (data) => {
                if (data) {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        retObj.next(false);
                    }
                    else {
                        this.setControlValue('FromCustomerContactNumber', this.returnDataFetch(data, 'FromCustomerContactNumber'));
                        if (this.getControlValue('FromCustomerContactNumber')) {
                            let msg: string = this.translatedMessageList.You_are_about_to_relate_this_ticket_to + ':'
                                + String.fromCharCode(10) + String.fromCharCode(10) + this.getControlValue('FromCustomerContactNumber') + ' ' + data['FromCustomerContactDesc']
                                + String.fromCharCode(10) + String.fromCharCode(10)
                                + this.translatedMessageList.Are_you_sure;

                            let promptVO: ICabsModalVO = new ICabsModalVO(msg, null, function yesPrompt(): any {
                                retObj.next(true);
                            }.bind(this),
                                function cancelPrompt(): any {
                                    this.setControlValue('FromCustomerContactNumber', '');
                                    retObj.next(true);
                                }.bind(this));
                            promptVO.cancelLabel = 'No';
                            promptVO.confirmLabel = 'Yes';
                            promptVO.title = MessageConstant.PageSpecificMessage.customerContactMaintenance.Linking_To_Other_Ticket;
                            this.modalAdvService.emitPrompt(promptVO);
                        }
                    }
                }
            });
        }
        else {
            retObj.next(true);
        }
        return retObj;
    }

    private showHideExportButton(): any {
        this.fieldVisibility.tdContactExport = this.getControlValue('ShowExportButton');
        if (this.fieldVisibility.tdContactExport) {

        }
    }

    private hideExportButton(): any {
        this.fieldVisibility.tdContactExport = false;
    }

    private showHideServiceType(blnShow: any): any {
        if (blnShow) {
            this.fieldVisibility.tdServiceTypeCode = true;
            this.fieldVisibility.tdServiceTypeCodeLabel = true;
        } else {
            this.fieldVisibility.tdServiceTypeCode = false;
            this.fieldVisibility.tdServiceTypeCodeLabel = false;
        }
    }

    private setupTicketGrid(): any {
        this.riGridTickets.DefaultBorderColor = 'ADD8E6';
        this.riGridTickets.DefaultTextColor = '0000FF';
        this.riGridTickets.HighlightBar = true;
        this.riGridTickets.Clear();
        this.riGridTickets.AddColumn('RelatedCustomerContactNumber', 'Tickets', 'RelatedCustomerContactNumber', MntConst.eTypeInteger, 8, false);
        this.riGridTickets.AddColumnAlign('RelatedCustomerContactNumber', MntConst.eAlignmentCenter);
        this.riGridTickets.AddColumn('FromCustomerContactNumber', 'Tickets', 'FromCustomerContactNumber', MntConst.eTypeInteger, 8, false);
        this.riGridTickets.AddColumnAlign('FromCustomerContactNumber', MntConst.eAlignmentCenter);
        this.riGridTickets.AddColumn('RelatedProspectNumber', 'Tickets', 'RelatedProspectNumber', MntConst.eTypeInteger, 8, false);
        this.riGridTickets.AddColumnAlign('RelatedProspectNumber', 'eAlignCenter');
        this.riGridTickets.AddColumn('TicketOpen', 'Tickets', 'TicketOpen', MntConst.eTypeImage, 1);
        this.riGridTickets.AddColumn('ContactType', 'Tickets', 'ContactType', MntConst.eTypeTextFree, 30, false);
        this.riGridTickets.AddColumn('NumActions', 'Tickets', 'NumActions', MntConst.eTypeInteger, 3, false);
        this.riGridTickets.AddColumnAlign('NumActions', MntConst.eAlignmentCenter);
        this.riGridTickets.AddColumn('Created', 'Tickets', 'Created', MntConst.eTypeTextFree, 12, false);
        this.riGridTickets.AddColumnAlign('Created', MntConst.eAlignmentCenter);
        this.riGridTickets.AddColumn('ActionBy', 'Tickets', 'ActionBy', MntConst.eTypeTextFree, 12, false);
        this.riGridTickets.AddColumnAlign('ActionBy', MntConst.eAlignmentCenter);
        this.riGridTickets.AddColumn('Assignee', 'Tickets', 'Assignee', MntConst.eTypeTextFree, 30, false);
        this.riGridTickets.AddColumn('ContactStatus', 'Tickets', 'ContactStatus', MntConst.eTypeTextFree, 30, false);
        this.riGridTickets.AddColumn('Notified', 'Tickets', 'Notified', MntConst.eTypeImage, 1);
        this.riGridTickets.AddColumnOrderable('RelatedCustomerContactNumber', true);
        this.riGridTickets.AddColumnOrderable('FromCustomerContactNumber', true);
        this.riGridTickets.AddColumnOrderable('RelatedProspectNumber', true);
        this.riGridTickets.AddColumnOrderable('TicketOpen', true);
        this.riGridTickets.AddColumnOrderable('ContactType', true);
        this.riGridTickets.AddColumnOrderable('ContactStatus', true);
        this.riGridTickets.AddColumnOrderable('Created', true);
        this.riGridTickets.AddColumnOrderable('ActionBy', true);
        this.riGridTickets.Complete();
    }

    private setupLogGrid(): any {
        this.riGridLogs.DefaultBorderColor = 'ADD8E6';
        this.riGridLogs.DefaultTextColor = '0000FF';
        this.riGridLogs.HighlightBar = true;
        this.riGridLogs.Clear();
        this.riGridLogs.AddColumn('CallLogID', 'Logs', 'CallLogID', MntConst.eTypeInteger, 8, false);
        this.riGridLogs.AddColumnAlign('CallLogID', MntConst.eAlignmentCenter);
        this.riGridLogs.AddColumn('CallStartTime', 'Logs', 'CallStartTime', MntConst.eTypeTextFree, 10, false);
        this.riGridLogs.AddColumnAlign('CallStartTime', MntConst.eAlignmentCenter);
        this.riGridLogs.AddColumn('CallEndTime', 'Logs', 'CallEndTime', MntConst.eTypeTextFree, 10, false);
        this.riGridLogs.AddColumnAlign('CallEndTime', MntConst.eAlignmentCenter);
        this.riGridLogs.AddColumn('CallTaken', 'Logs', 'CallTaken', MntConst.eTypeTextFree, 20, false);
        this.riGridLogs.AddColumn('NumberTickets', 'Logs', 'NumberTickets', MntConst.eTypeInteger, 4, false);
        this.riGridLogs.AddColumnAlign('NumberTickets', MntConst.eAlignmentCenter);
        this.riGridLogs.AddColumn('ContactName', 'Logs', 'ContactName', MntConst.eTypeTextFree, 40, false);
        this.riGridLogs.AddColumn('ContactPosn', 'Logs', 'ContactPosn', MntConst.eTypeTextFree, 20, false);
        this.riGridLogs.AddColumn('ContactPhone', 'Logs', 'ContactPhone', MntConst.eTypeTextFree, 20, false);
        this.riGridLogs.AddColumn('ContactFax', 'Logs', 'ContactFax', MntConst.eTypeTextFree, 20, false);
        this.riGridLogs.AddColumnOrderable('CallLogID', true);
        this.riGridLogs.AddColumnOrderable('CallStartTime', true);
        this.riGridLogs.AddColumnOrderable('CallTaken', true);
        this.riGridLogs.AddColumnOrderable('NumberTickets', true);
        this.riGridLogs.AddColumnOrderable('ContactName', true);
        this.riGridLogs.AddColumnOrderable('ContactPosn', true);
        this.riGridLogs.AddColumnOrderable('ContactPhone', true);
        this.riGridLogs.AddColumnOrderable('ContactFax', true);
        this.riGridLogs.Complete();
    }

    private setupNotifyGrid(): any {
        this.riGridNotify.DefaultBorderColor = 'ADD8E6';
        this.riGridNotify.DefaultTextColor = '0000FF';
        this.riGridNotify.HighlightBar = true;

        if (this.pageParams.cDefaultNotificationType === 'E') {
            this.setControlValue('NotifyStyle', 'email');
        } else {
            this.setControlValue('NotifyStyle', 'sms');
        }
        this.buildNotifyGrid();
    }

    private buildNotifyGrid(): any {
        this.riGridNotify.Clear();
        if (this.getControlValue('NotifyStyle') === 'email') {
            this.riGridNotify.AddColumn('Created', 'Notify', 'Created', MntConst.eTypeTextFree, 16, false);
            this.riGridNotify.AddColumnAlign('Created', MntConst.eAlignmentCenter);
            this.riGridNotify.AddColumn('Sent', 'Notify', 'Sent', MntConst.eTypeTextFree, 12, false);
            this.riGridNotify.AddColumnAlign('Sent', MntConst.eAlignmentCenter);
            this.riGridNotify.AddColumn('SubjectText', 'Notify', 'SubjectText', MntConst.eTypeTextFree, 40, false);
            this.riGridNotify.AddColumn('NumRecipients', 'Notify', 'NumRecipients', MntConst.eTypeInteger, 3, false);
            this.riGridNotify.AddColumnAlign('NumRecipients', MntConst.eAlignmentCenter);
        }

        if (this.getControlValue('NotifyStyle') === 'sms') {
            this.riGridNotify.AddColumn('Created', 'Notify', 'Created', MntConst.eTypeTextFree, 16, false);
            this.riGridNotify.AddColumnAlign('Created', MntConst.eAlignmentCenter);
            this.riGridNotify.AddColumn('Send', 'Notify', 'Send', MntConst.eTypeTextFree, 12, false);
            this.riGridNotify.AddColumnAlign('Send', MntConst.eAlignmentCenter);
            this.riGridNotify.AddColumn('Sent', 'Notify', 'Sent', MntConst.eTypeTextFree, 12, false);
            this.riGridNotify.AddColumnAlign('Sent', MntConst.eAlignmentCenter);
            this.riGridNotify.AddColumn('StatusImg', 'Notify', 'StatusImg', MntConst.eTypeImage, 2, false);
            this.riGridNotify.AddColumn('SubjectText', 'Notify', 'SubjectText', MntConst.eTypeTextFree, 40, false);
            this.riGridNotify.AddColumn('Employee', 'Notify', 'Employee', MntConst.eTypeTextFree, 20, false);
            this.riGridNotify.AddColumn('Number', 'Notify', 'Number', MntConst.eTypeTextFree, 15, false);
        }
        this.riGridNotify.Complete();
    }

    private setupWorkOrderGrid(): any {
        this.riGridWorkOrder.DefaultBorderColor = 'ADD8E6';
        this.riGridWorkOrder.DefaultTextColor = '0000FF';
        this.riGridWorkOrder.HighlightBar = true;
        this.riGridWorkOrder.Clear();
        this.riGridWorkOrder.AddColumn('WONumber', 'WorkOrder', 'WONumber', MntConst.eTypeInteger, 8, false);
        this.riGridWorkOrder.AddColumnAlign('WONumber', MntConst.eAlignmentCenter);
        this.riGridWorkOrder.AddColumn('WOAppointmentMade', 'WorkOrder', 'WOAppointmentMade', MntConst.eTypeImage, 1);
        this.riGridWorkOrder.AddColumn('WOTypeDesc', 'WorkOrder', 'WOTypeDesc', MntConst.eTypeTextFree, 20, false);
        this.riGridWorkOrder.AddColumn('WODateTime', 'WorkOrder', 'WODateTime', MntConst.eTypeTextFree, 12, false);
        this.riGridWorkOrder.AddColumnAlign('WODateTime', MntConst.eAlignmentCenter);
        this.riGridWorkOrder.AddColumn('WOEmployeeName', 'WorkOrder', 'WOEmployeeName', MntConst.eTypeTextFree, 20, false);
        this.riGridWorkOrder.AddColumn('WOStatusDesc', 'WorkOrder', 'WOStatusDesc', MntConst.eTypeTextFree, 20, false);
        this.riGridWorkOrder.AddColumn('WOCloseDateTime', 'WorkOrder', 'WOCloseDateTime', MntConst.eTypeTextFree, 12, false);
        this.riGridWorkOrder.AddColumnAlign('WOCloseDateTime', MntConst.eAlignmentCenter);
        this.riGridWorkOrder.AddColumnOrderable('WONumber', true);
        this.riGridWorkOrder.Complete();
    }

    private setupRootCauseGrid(): any {
        this.riGridRootCause.DefaultBorderColor = 'ADD8E6';
        this.riGridRootCause.DefaultTextColor = '0000FF';
        this.riGridRootCause.HighlightBar = true;
        this.riGridRootCause.Clear();
        this.riGridRootCause.AddColumn('RootCauseCode', 'RootCause', 'RootCauseCode', MntConst.eTypeTextFree, 30, false);
        this.riGridRootCause.AddColumn('OccupationCode', 'RootCause', 'OccupationCode', MntConst.eTypeTextFree, 30, false);
        this.riGridRootCause.AddColumn('EmployeeDetails', 'RootCause', 'EmployeeDetails', MntConst.eTypeTextFree, 30, false);
        this.riGridRootCause.AddColumn('RootCauseNotes', 'RootCause', 'RootCauseNotes', MntConst.eTypeTextFree, 30, false);
        this.riGridRootCause.Complete();
    }

    private setupDisputedInvoicesGrid(): any {
        this.riGridDisputedInvoices.DefaultBorderColor = 'ADD8E6';
        this.riGridDisputedInvoices.DefaultTextColor = '0000FF';
        this.riGridDisputedInvoices.HighlightBar = true;
        this.riGridDisputedInvoices.Clear();
        this.riGridDisputedInvoices.AddColumn('Company', 'DisputedInvoices', 'Company', MntConst.eTypeTextFree, 5, false);
        this.riGridDisputedInvoices.AddColumnAlign('Company', MntConst.eAlignmentCenter);
        this.riGridDisputedInvoices.AddColumn('InvoiceNumber', 'DisputedInvoices', 'InvoiceNumber', MntConst.eTypeInteger, 9, false);
        this.riGridDisputedInvoices.AddColumnAlign('InvoiceNumber', MntConst.eAlignmentCenter);
        this.riGridDisputedInvoices.AddColumn('InDisputeReference', 'DisputedInvoices', 'InDisputeReference', MntConst.eTypeTextFree, 20, false);
        this.riGridDisputedInvoices.AddColumn('ExtractDate', 'DisputedInvoices', 'ExtractDate', MntConst.eTypeDate, 10, false);
        this.riGridDisputedInvoices.AddColumnAlign('ExtractDate', MntConst.eAlignmentCenter);
        this.riGridDisputedInvoices.AddColumn('PeriodStart', 'DisputedInvoices', 'PeriodStart', MntConst.eTypeDate, 10, false);
        this.riGridDisputedInvoices.AddColumnAlign('PeriodStart', MntConst.eAlignmentCenter);
        this.riGridDisputedInvoices.AddColumn('TaxPoint', 'DisputedInvoices', 'TaxPoint', MntConst.eTypeDate, 10, false);
        this.riGridDisputedInvoices.AddColumnAlign('TaxPoint', MntConst.eAlignmentCenter);
        this.riGridDisputedInvoices.AddColumn('ValueEXCLTax', 'DisputedInvoices', 'ValueEXCLTax', MntConst.eTypeText, 15, true);
        this.riGridDisputedInvoices.AddColumnAlign('ValueEXCLTax', MntConst.eAlignmentRight);
        this.riGridDisputedInvoices.AddColumn('TaxValue', 'DisputedInvoices', 'TaxValue', MntConst.eTypeText, 15, true);
        this.riGridDisputedInvoices.AddColumnAlign('TaxValue', MntConst.eAlignmentRight);
        this.riGridDisputedInvoices.AddColumn('InvoiceName', 'DisputedInvoices', 'InvoiceName', MntConst.eTypeTextFree, 30, false);
        this.riGridDisputedInvoices.Complete();
    }

    public cmdCopyTicketOnClick(loadPage: boolean = false): any {
        this.buttonControls['cmdCopyTicket'].disabled = true;
        this.setControlValue('CopyFromTicketNumber', this.getControlValue('CustomerContactNumber'));
        if (!loadPage) {
            this.pageParams.previousRouteUrl = InternalMaintenanceServiceModuleRoutes.ICABSCMCUSTOMERCONTACTMAINTENANCECOPY;
            this.navigate('TicketMaintenance', InternalMaintenanceServiceModuleRoutes.ICABSCMCUSTOMERCONTACTMAINTENANCECOPY);
        }
        if (loadPage && (this.getControlValue('CopyMode') === 'Y')) {
            this.cloneFormValues = {};
            this.pageParams.lRefreshTicketsGrid = true;
            this.pageParams.lRefreshLogsGrid = true;
            this.pageParams.lRefreshNotifyGrid = true;
            this.pageParams.lRefreshWorkOrderGrid = true;
            this.pageParams.lRefreshRootCauseGrid = true;
            this.pageParams.lRefreshDisputedInvoicesGrid = true;
            this.setControlValue('SaveCopyContactTypeCode', this.getControlValue('ContactTypeCode'));
            this.setControlValue('SaveCopyContactTypeDetailCode', this.getControlValue('ContactTypeDetailCode'));
            this.setControlValue('CustomerContactNumber', '0');
            this.setControlValue('ContactTypeCodeSelect', this.getControlValue('CopyContactTypeCode'));
            let cTypeCode: Subscription = this.contactTypeCodeSelectOnChange().subscribe((e) => {
                this.setControlValue('ContactTypeDetailCodeSelect', this.getControlValue('CopyContactTypeDetailCode'));
                let cTypeDesc: Subscription = this.contactTypeDetailCodeSelectOnChange().subscribe((e1) => {
                    this.disableControl('AccountNumber', false);
                    this.disableControl('ContractNumber', false);
                    this.disableControl('PremiseNumber', false);
                    this.disableControl('ProductCode', false);
                    this.setScreenTo('add');
                    this.setFocusToControl('ContractNumber');
                    setTimeout(() => {
                        this.disableControl('ContactTypeCodeSelect', true);
                        this.disableControl('ContactTypeDetailCodeSelect', true);
                        this.disableControl('menu', true);
                        this.cloneFormData();
                    }, 0);
                });
                this.subscriptionManager.add(cTypeDesc);
            });
            this.subscriptionManager.add(cTypeCode);
        }
    }

    public cmdCopyTicketReturnOnClick(): any {
        //' Return To The Copied From Ticket Number To Enable The User To Close It Etc...
        if (this.getControlValue('CopyFromTicketNumberSave')) {
            this.saveCancelDisable = false;
            this.setControlValue('CustomerContactNumber', this.getControlValue('CopyFromTicketNumberSave'));
            this.setControlValue('CopyFromTicketNumberSave', '');
            let params: Object = { 'CustomerContactNumber': this.getControlValue('CustomerContactNumber') };
            //TODO:
            this.riMaintenanceFetchRecord(params);
            this.fieldVisibility.tdCopyTicketReturn = false;
            this.setControlValue('CopyMode', 'N');
            this.disableControl('ContactMediumCodeSelect', true);
            this.disableControl('ContactStatusCodeSelect', true);
        }
    }

    public cmdSelectDisputedInvoicesOnClick(): any {
        this.setControlValue('DisputedInvoiceCacheName', this.utils.getBusinessCode() +
            'A' + this.utils.getBranchCode() +
            'A' + this.riExchange.getParentHTMLValue('DisputedInvoiceCacheTime') +
            'A' + this.getControlValue('AccountNumber') +
            'A' + this.getControlValue('ContractNumber') +
            'A' + this.getControlValue('PremiseNumber'));


        /* let pageParams: Object = {
            ContractNumber: this.getControlValue('ContractNumber'),
            ContractName: this.getControlValue('ContractName'),
            PremiseNumber: this.getControlValue('PremiseNumber'),
            PremiseName: this.getControlValue('PremiseName'),
            AccountNumber: this.getControlValue('AccountNumber'),
            AccountName: this.getControlValue('AccountContactName'),
            customerContactNumber: this.getControlValue('CustomerContactNumber'),
            disputedInvoiceCacheName: this.getControlValue('DisputedInvoiceCacheName'),
            parentMode: 'ContactCentreSearch',
            isReturnFlag: true
        }; */

        //navigate to screen Application/iCABSAContractInvoiceGrid.htm'  Mode = 'ContactCentreSearch'
        this.setNavReturnValues();
        // this.router.navigate([BillToCashModuleRoutes.ICABSACONTRACTINVOICEGRID], { queryParams: pageParams });


        let url: string = '#' + BillToCashModuleRoutes.ICABSACONTRACTINVOICEGRID;

        url += '?parentMode=ContactCentreSearch';
        url += '&ContractNumber=' + this.getControlValue('ContractNumber');
        url += '&ContractName=' + this.getControlValue('ContractName');
        url += '&PremiseNumber=' + this.getControlValue('PremiseNumber');
        url += '&PremiseName=' + this.getControlValue('PremiseName');
        url += '&AccountNumber=' + this.getControlValue('AccountNumber');
        url += '&AccountName=' + this.getControlValue('AccountContactName');
        url += '&customerContactNumber=' + this.getControlValue('CustomerContactNumber');
        url += '&disputedInvoiceCacheName=' + this.getControlValue('DisputedInvoiceCacheName');
        url += '&isReturnFlag=' + true;
        url += '&' + CBBConstants.c_s_URL_PARAM_COUNTRYCODE + '=' + this.utils.getCountryCode();
        url += '&' + CBBConstants.c_s_URL_PARAM_BUSINESSCODE + '=' + this.utils.getBusinessCode();
        url += '&' + CBBConstants.c_s_URL_PARAM_BRANCHCODE + '=' + this.utils.getBranchCode();

        window.open(url,
            '_blank',
            'fullscreen=yes,menubar=no,resizable=no,status=no,titlebar=no,toolbar=no');
        return;
    }

    public btnProspectOnClick(): any {
        let closedProspect: any = {};
        if (this.getControlValue('ClosedDateTime')) {
            closedProspect = {
                closed: true
            };
        }
        this.navigate('ContactManagement', InternalMaintenanceModuleRoutes.ICABSCMPROSPECTMAINTENANCE, closedProspect);
    }

    private productCode_search(): any {
        if (this.productCodeEllipsis)
            this.productCodeEllipsis.openModal();
    }

    private showHideClientRetention(lHide: boolean): any {
        if (lHide) {
            this.fieldVisibility.trClientRetention = false;
            this.setRequiredStatus('EarliestEffectDate', false);
            this.fieldRequired['EarliestEffectDate'] = false;
        } else {
            this.fieldVisibility.trClientRetention = true;
            this.setRequiredStatus('EarliestEffectDate', true);
            this.fieldRequired['EarliestEffectDate'] = true;
        }
    }

    private setMandatoryFields(): any {
        if (this.instr(this.getControlValue('ContactTypeDetailAcctReviewList'), '^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
            if (this.pageParams.lAccountReviewInd === false) {
                this.pageParams.lAccountReviewInd = true;
                this.buildMenus();
            }
        } else {
            if (this.pageParams.lAccountReviewInd === true) {
                this.pageParams.lAccountReviewInd = false;
                this.buildMenus();
            }
        }

        if (this.instr(this.getControlValue('ContactTypeDetailRootCauseList'), '^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
            if (this.pageParams.lRootCauseInd === false) {
                this.pageParams.lRootCauseInd = true;
                this.buildMenus();
            }
        } else {
            if (this.pageParams.lRootCauseInd === true) {
                this.pageParams.lRootCauseInd = false;
                this.buildMenus();
            }
        }

        if (this.instr(this.getControlValue('ContactTypeDetailOrigList'), '^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
            this.fieldVisibility.trOriginatingEmployee = true;
        } else {
            this.fieldVisibility.trOriginatingEmployee = false;
        }

        if (this.instr(this.getControlValue('ContactTypeDetailMandAccList'), '^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
            this.setRequiredStatus('AccountNumber', true);
            this.fieldRequired['AccountNumber'] = true;
        } else {
            this.setRequiredStatus('AccountNumber', false);
            this.fieldRequired['AccountNumber'] = false;
        }

        if (this.instr(this.getControlValue('ContactTypeDetailMandConList'), '^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
            this.setRequiredStatus('ContractNumber', true);
            this.fieldRequired['ContractNumber'] = true;
        } else {
            this.setRequiredStatus('ContractNumber', false);
            this.fieldRequired['ContractNumber'] = false;
        }

        if (this.instr(this.getControlValue('ContactTypeDetailMandPreList'), '^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
            this.setRequiredStatus('PremiseNumber', true);
            this.fieldRequired['PremiseNumber'] = true;
        } else {
            this.setRequiredStatus('PremiseNumber', false);
            this.fieldRequired['PremiseNumber'] = false;
        }

        if (this.instr(this.getControlValue('ContactTypeDetailMandProList'), '^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
            this.setRequiredStatus('ProductCode', true);
            this.fieldRequired['ProductCode'] = true;
        } else {
            this.setRequiredStatus('ProductCode', false);
            this.fieldRequired['ProductCode'] = false;
        }

        if ((this.instr(this.getControlValue('ContactTypeDetailMandPostList'), '^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) && this.fieldVisibility.tdPostcode === true) {
            this.setRequiredStatus('ContactPostcode', true);
            this.fieldRequired['ContactPostcode'] = true;
        } else {
            this.setRequiredStatus('ContactPostcode', false);
            this.fieldRequired['ContactPostcode'] = false;
        }
    }

    private cmdContactDetails(cFrom: any): any {
        let mode = '';
        if (cFrom === 'Button') {
            mode = 'CCMEntry';
        } else {
            mode = 'CCMEntryMenu';
        }
        //navigate to screen ContactManagement/iCABSCMContactPersonMaintenance.htm'
        this.navigate(mode, InternalMaintenanceServiceModuleRoutes.ICABSCMCONTACTPERSONMAINTENANCE, {
            parentMode: mode,
            accountNumber: this.getControlValue('AccountNumber'),
            contractNumber: this.getControlValue('ContractNumber'),
            premiseNumber: this.getControlValue('PremiseNumber')
        });
    }

    // ' Sets all used attributes for certain input boxes to be a ServiceCover rowid
    private setAllSCRowID(ipRowID: any): any {
        this.setAttribute('ContractNumberServiceCoverRowID', ipRowID);
        this.setAttribute('ContractNumberSCRowID', ipRowID);
        this.setAttribute('ProductCodeServiceCoverRowID', ipRowID);
        this.setAttribute('ProductCodeSCRowID', ipRowID);
        this.setAttribute('ServiceCoverNumberServiceCoverRowID', ipRowID);
        this.setAttribute('ServiceCoverNumberSCRowID', ipRowID);
        this.setControlValue('ServiceCoverRowID', ipRowID);
    }

    private setLatestTabHeader(strSpan: string): any {
        switch (strSpan.toLowerCase()) {
            case 'latest':
                this.fieldVisibility.spanLatestDetails = true;
                this.fieldVisibility.spanNewEntry = false;
                break;
            case 'new':
                this.fieldVisibility.spanLatestDetails = false;
                this.fieldVisibility.spanNewEntry = true;
                break;
        }
    }

    private recordSelected(): Boolean {
        return this.storeData ? true : false;
    }

    private setErrorStatus(controlName: any, flag: boolean = true): void {
        if (controlName && this.uiForm.controls.hasOwnProperty(controlName)) {
            if (flag) {
                this.disableControl(controlName, true);
            }
            else {
                this.disableControl(controlName, false);
            }
        }
    }

    private contractNumberFormatOnChange(): any {
        let elementValue = this.getControlValue('ContractNumber');
        if (elementValue)
            this.setControlValue('ContractNumber', this.utils.fillLeadingZeros(elementValue, 8));
    }

    private accountNumberFormatOnChange(): any {
        let elementValue = this.getControlValue('AccountNumber');
        if (elementValue)
            this.setControlValue('AccountNumber', this.utils.fillLeadingZeros(elementValue, 9));
        else
            this.setControlValue('AccountName', '');
    }

    private riMaintenanceFetchRecord(params: any): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        this.executeRequest(this, '', params, '', (data) => {
            if (data) {
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.cloneFormValues = {};
                    this.processForm();
                    this.storeData = data;
                    this.pageParams.ContactAction = data.ContactAction || '';
                    this.pageParams.CustomerContact = data.CustomerContact || '';
                    this.onUpdateMode(data);
                    this.showHideExportButton();
                }
            }
            retObj.next(true);
        });
        return retObj;
    }

    private CBORequestClear(): void {
        this.cboPostObj = {};
        this.customBusinessObjectAdditionalPostData = {};
    }

    private CBORequestAddCS(controlName: any): void {
        switch (controlName) {
            case 'BusinessCode':
                this.cboPostObj['BusinessCode'] = this.utils.getBusinessCode();
                break;
        }
    }

    private CBORequestAdd(controlName: any): void {
        this.cboPostObj[controlName] = this.getControlValue(controlName);
    }

    private CBORequestExecute(callback: any): any {
        let functionName: string = '';

        let tempCBO = {};
        for (let key in this.customBusinessObjectAdditionalPostData) {
            if (this.customBusinessObjectAdditionalPostData.hasOwnProperty(key)) {
                tempCBO[key] = this.customBusinessObjectAdditionalPostData[key];
                if (key.toUpperCase() === 'Function'.toUpperCase()) {
                    functionName = this.customBusinessObjectAdditionalPostData[key];
                }
            }
        }

        for (let key in this.cboPostObj) {
            if (this.cboPostObj.hasOwnProperty(key)) {
                tempCBO[key] = this.cboPostObj[key];
            }
        }
        this.executeRequest(this, functionName, '', tempCBO, (data) => {
            if (data.hasError) {
                let modalVO: ICabsModalVO = new ICabsModalVO(data.errorMessage, data.fullError);
                modalVO.closeCallback = function closeModal(): any {
                    // In case there is any exception/ error in service response then lock screen controls
                    return callback(data);
                }.bind(this);
                this.modalAdvService.emitError(modalVO);
            } else {
                return callback(data);
            }
        });
    }

    private setFormData(data: any): void {
        //Tab grdHeader
        this.setControlValue('CustomerContactNumber', this.setFieldValue(data.CustomerContactNumber));
        this.setControlValue('CurrentTeamID', this.setFieldValue(data.CurrentTeamID));
        this.setControlValue('ActionCount', this.setFieldValue(data.ActionCount));
        this.setControlValue('ClosedDateTime', this.setFieldValue(data.ClosedDateTime));
        this.setControlValue('AccountNumber', this.setFieldValue(data.AccountNumber));
        this.setControlValue('AccountName', this.setFieldValue(data.AccountName));
        this.setControlValue('GroupAccountNumber', this.setFieldValue(data.GroupAccountNumber));
        this.setControlValue('GroupName', this.setFieldValue(data.GroupName));
        this.setControlValue('ContractNumber', this.setFieldValue(data.ContractNumber));
        this.setControlValue('ContractName', this.setFieldValue(data.ContractName));
        this.setControlValue('CurrentOwnerEmployeeCode', this.setFieldValue(data.CurrentOwnerEmployeeCode));
        this.setControlValue('CurrentOwnerEmployeeName', this.setFieldValue(data.CurrentOwnerEmployeeName));
        this.setControlValue('PremiseNumber', this.setFieldValue(data.PremiseNumber));
        this.setControlValue('PremiseName', this.setFieldValue(data.PremiseName));
        this.setControlValue('ServiceTypeCode', this.setFieldValue(data.ServiceTypeCode));
        this.setControlValue('ServiceTypeDesc', this.setFieldValue(data.ServiceTypeDesc));
        this.setControlValue('ProductCode', this.setFieldValue(data.ProductCode));
        this.setControlValue('ProductDesc', this.setFieldValue(data.ProductDesc));
        this.setControlValue('ContactPostcode', this.setFieldValue(data.ContactPostcode));
        this.setControlValue('DispProspectNumber', this.setFieldValue(data.DispProspectNumber));
        this.setControlValue('ContactTypeDetailCodeSelect', this.setFieldValue(data.ContactTypeDetailCode));
        this.setControlValue('ContactTypeCodeSelect', this.setFieldValue(data.ContactTypeCodeSelect));

        //Tab grdAddress
        this.setControlValue('AddressName', this.setFieldValue(data.AddressName));
        this.setControlValue('AddressContactName', this.setFieldValue(data.CustomerContactName));
        this.setControlValue('AddressLine1', this.setFieldValue(data.AddressLine1));
        this.setControlValue('AddressContactPosn', this.setFieldValue(data.AddressContactPosn));
        this.setControlValue('AddressLine2', this.setFieldValue(data.AddressLine2));
        this.setControlValue('AddressLine3', this.setFieldValue(data.AddressLine3));
        this.setControlValue('AddressLine4', this.setFieldValue(data.AddressLine4));
        this.setControlValue('AddressContactPhone', this.setFieldValue(data.AddressContactPhone));
        this.setControlValue('AddressLine5', this.setFieldValue(data.AddressLine5));
        this.setControlValue('AddressContactMobile', this.setFieldValue(data.AddressContactMobile));
        this.setControlValue('AddressPostcode', this.setFieldValue(data.AddressPostcode));
        this.setControlValue('AddressContactEmail', this.setFieldValue(data.AddressContactEmail));
        this.setControlValue('AddressCodeSelect', this.setFieldValue(data.AddressCodeSelect));
        //Tab grdNotify
        let notifyStyle: string = this.setFieldValue(data.NotifyStyle);
        let notifyDestination: string = this.setFieldValue(data.NotifyDestination);
        this.setControlValue('NotifyStyle', notifyStyle ? notifyStyle : 'sms');
        this.setControlValue('NotifyDestination', notifyDestination ? notifyDestination : 'i');
        //Tab grdLatest
        this.setControlValue('ContactName', this.setFieldValue(data.ContactName));
        this.setControlValue('ContactPosition', this.setFieldValue(data.ContactPosition));
        this.setControlValue('ContactTelephone', this.setFieldValue(data.ContactTelephone));
        this.setControlValue('ContactMobileNumber', this.setFieldValue(data.ContactMobileNumber));
        this.setControlValue('ContactEmailAddress', this.setFieldValue(data.ContactEmailAddress));
        this.setControlValue('ContactFax', this.riExchange.getParentHTMLValue('CallContactFax'));
        this.setControlValue('TicketReference', this.setFieldValue(data.TicketReference));
        this.setControlValue('LatestCreatedDate', this.setFieldValue(data.LatestCreatedDate));
        this.setControlValue('LatestCreatedTime', this.setFieldValue(data.LatestCreatedTime));
        this.setControlValue('LatestCreatedByEmployeeCode', this.setFieldValue(data.LatestCreatedByEmployeeCode));
        this.setControlValue('LatestCreatedByEmployeeName', this.setFieldValue(data.LatestCreatedByEmployeeName));
        this.setControlValue('ShortDescription', this.setFieldValue(data.ShortDescription));
        this.setControlValue('ServiceAreaCode', this.setFieldValue(data.ServiceAreaCode));
        this.setControlValue('SalesAreaCode', this.setFieldValue(data.SalesAreaCode));
        this.setControlValue('EarliestEffectDate', this.setFieldValue(data.EarliestEffectDate));
        this.setControlValue('chkCreateProspect', this.setFieldValue(data.chkCreateProspect, true));
        this.setControlValue('CRCommenceDate', this.setFieldValue(data.CRCommenceDate));
        this.setControlValue('CRNoticePeriod', this.setFieldValue(data.CRNoticePeriod));
        this.setControlValue('NextActionByDate', this.setFieldValue(data.NextActionByDate));
        this.setControlValue('NextActionByTime', this.setFieldValue(data.NextActionByTime));
        this.setControlValue('DefaultAssigneeEmployeeDetails', this.setFieldValue(data.DefaultAssigneeEmployeeDetails));
        this.setControlValue('OriginatingEmployeeCode', this.setFieldValue(data.OriginatingEmployeeCode));
        this.setControlValue('OriginatingEmployeeName', this.setFieldValue(data.OriginatingEmployeeName));
        this.setControlValue('NextActionEmployeeCode', this.setFieldValue(data.NextActionEmployeeCode));
        this.setControlValue('NextActionEmployeeName', this.setFieldValue(data.NextActionEmployeeName));
        this.setControlValue('chkKeepOwnership', this.setFieldValue(data.chkKeepOwnership, true));
        this.setControlValue('chkContactPassedToActioner', this.setFieldValue(data.chkContactPassedToActioner, true));
        this.setControlValue('SMSSendOnDate', this.setFieldValue(data.SMSSendOnDate));
        this.setControlValue('SMSSendOnTime', this.setFieldValue(data.SMSSendOnTime));
        this.setControlValue('chkCreateCallOut', this.setFieldValue(data.chkCreateCallOut, true));
        this.setControlValue('ScheduledCloseDate', this.setFieldValue(data.ScheduledCloseDate));
        this.setControlValue('ContactMediumCodeSelect', this.setFieldValue(data.ContactMediumCode));
        this.setControlValue('ContactNotifyWhenSelect', this.setFieldValue(data.ContactNotifyWhenSelect));
        this.setControlValue('ContactNotifyTypeSelect', this.setFieldValue(data.ContactNotifyTypeSelect));
        this.setControlValue('ContactStatusCodeSelect', this.setFieldValue(data.ContactStatusCode));
        this.setControlValue('Comments', this.setFieldValue(data.Comments));
        //Tab grdOriginal
        this.setControlValue('CustomerContactName', this.setFieldValue(data.CustomerContactName));
        this.setControlValue('CustomerContactPosition', this.setFieldValue(data.CustomerContactPosition));
        this.setControlValue('CustomerContactTelephone', this.setFieldValue(data.CustomerContactTelephone));
        this.setControlValue('CustomerContactMobileNumber', this.setFieldValue(data.CustomerContactMobileNumber));
        this.setControlValue('CustomerContactEmailAddress', this.setFieldValue(data.CustomerContactEmailAddress));
        this.setControlValue('CustomerContactFax', this.riExchange.getParentHTMLValue('CallContactFax'));
        this.setControlValue('CustomerContactTicketReference', this.setFieldValue(data.TicketReference));
        this.setControlValue('CreatedDate', this.setFieldValue(data.CreatedDate));
        this.setControlValue('CreatedTime', this.setFieldValue(data.CreatedTime));
        this.setControlValue('CreatedByEmployeeCode', this.setFieldValue(data.CreatedByEmployeeCode));
        this.setControlValue('CreatedByEmployeeName', this.setFieldValue(data.CreatedByEmployeeName));
        this.setControlValue('ShortNarrative', this.setFieldValue(data.ShortNarrative));
        this.setControlValue('ContactNarrative', this.setFieldValue(data.ContactNarrative));
        this.setControlValue('OriginalContactMediumCodeSelect', this.setFieldValue(data.OriginalContactMediumCode));
        //hidden values
        this.setControlValue('CanAmendTicketReference', this.setFieldValue(data.CanAmendTicketReference));
        this.setControlValue('LanguageCode', this.setFieldValue(data.LanguageCode));
        this.setControlValue('ThisEmployeeCode', this.setFieldValue(data.ThisEmployeeCode));
        this.setControlValue('SearchCompanyName', this.setFieldValue(data.SearchCompanyName));
        this.setControlValue('SearchContactName', this.setFieldValue(data.SearchContactName));
        this.setControlValue('ContactTypeCallout', this.setFieldValue(data.ContactTypeCallout, true));
        this.setControlValue('ContactTypeClientRetention', this.setFieldValue(data.ContactTypeClientRetention, true));
        this.setControlValue('AllowCallout', this.setFieldValue(data.AllowCallOut, true));
        this.setControlValue('AlwaysShowAssignOptions', this.setFieldValue(data.AlwaysShowAssignOptions, true));
        this.setControlValue('ShowExportButton', this.setFieldValue(data.ShowExportButton, true));
        this.setControlValue('ServiceBranchNumber', this.setFieldValue(data.ServiceBranchNumber));
        this.setControlValue('CallOutRowID', this.setFieldValue(data.CallOutRowID));
        this.setControlValue('ServiceCoverNumber', this.setFieldValue(data.ServiceCoverNumber));
        this.setControlValue('ErrorMessageDesc', this.setFieldValue(data.ErrorMessageDesc));
        this.setControlValue('ContractTypeCode', this.setFieldValue(data.ContractTypeCode));
        this.setControlValue('ContractTypeDesc', this.setFieldValue(data.ContractTypeDesc));
        this.setControlValue('ContractTypeCodeList', this.setFieldValue(data.ContractTypeCodeList));
        this.setControlValue('CurrentCallLogID', this.setFieldValue(data.CurrentCallLogID));
        this.setControlValue('ClosedDate', this.setFieldValue(data.ClosedDate));
        this.setControlValue('ClosedTime', this.setFieldValue(data.ClosedTime));
        this.setControlValue('chkCloseTopic', this.setFieldValue(data.chkCloseTopic, true));
        this.setControlValue('ContactStatusCode', this.setFieldValue(data.ContactStatusCode));
        this.setControlValue('DefaultKeepOwnership', this.setFieldValue(data.DefaultKeepOwnership, true));
        this.setControlValue('CanSendIntNotify', this.setFieldValue(data.CanSendIntNotify, true));
        this.setControlValue('ContactTypeCode', this.setFieldValue(data.ContactTypeCode));
        this.setControlValue('ContactTypeDesc', this.setFieldValue(data.ContactTypeDesc));
        this.setControlValue('ContactTypeDetailCode', this.setFieldValue(data.ContactTypeDetailCode));
        this.setControlValue('ContactTypeDetailDesc', this.setFieldValue(data.ContactTypeDetailDesc));
        this.setControlValue('ContactTypeDetailPassedList', this.setFieldValue(data.ContactTypeDetailPassedList));
        this.setControlValue('ContactTypeDetailOrigList', this.setFieldValue(data.ContactTypeDetailOrigList));
        this.setControlValue('ContactTypeDetailSuperList', this.setFieldValue(data.ContactTypeDetailSuperList));
        this.setControlValue('ContactTypeDetailMandAccList', this.setFieldValue(data.ContactTypeDetailMandAccList));
        this.setControlValue('ContactTypeDetailMandConList', this.setFieldValue(data.ContactTypeDetailMandConList));
        this.setControlValue('ContactTypeDetailMandPreList', this.setFieldValue(data.ContactTypeDetailMandPreList));
        this.setControlValue('ContactTypeDetailMandProList', this.setFieldValue(data.ContactTypeDetailMandProList));
        this.setControlValue('ContactTypeDetailMandPostList', this.setFieldValue(data.ContactTypeDetailMandPostList));
        this.setControlValue('ContactTypeDetailAcctReviewList', this.setFieldValue(data.ContactTypeDetailAcctReviewList));
        this.setControlValue('ContactTypeDetailRootCauseList', this.setFieldValue(data.ContactTypeDetailRootCauseList));
        this.setControlValue('ContactTypeDetailCRProspectList', this.setFieldValue(data.ContactTypeDetailCRProspectList));
        this.setControlValue('DefaultAssigneeEmployeeCode', this.setFieldValue(data.DefaultAssigneeEmployeeCode));
        this.setControlValue('DefaultAssigneeEmployeeName', this.setFieldValue(data.DefaultAssigneeEmployeeName));
        this.setControlValue('ContactRedirectionUniqueID', this.setFieldValue(data.ContactRedirectionUniqueID));
        this.setControlValue('ContactMediumCode', this.setFieldValue(data.ContactMediumCode));
        this.setControlValue('OriginalContactMediumCode', this.setFieldValue(data.OriginalContactMediumCode));
        this.setControlValue('ExternalCREmailInd', this.setFieldValue(data.ExternalCREmailInd, true));
        this.setControlValue('ExternalCLEmailInd', this.setFieldValue(data.ExternalCLEmailInd, true));
        this.setControlValue('ExternalCRSMSInd', this.setFieldValue(data.ExternalCRSMSInd, true));
        this.setControlValue('ExternalCLSMSInd', this.setFieldValue(data.ExternalCLSMSInd, true));
        this.setControlValue('InternalSMSInd', this.setFieldValue(data.InternalSMSInd, true));
        this.setControlValue('ContactNotifyWhen', this.setFieldValue(data.ContactNotifyWhen));
        this.setControlValue('ContactNotifyType', this.setFieldValue(data.ContactNotifyType));
        this.setControlValue('RunFromParentMode', this.setFieldValue(data.RunFromParentMode));
        this.setControlValue('ContactTypeDetailCodeList', this.setFieldValue(data.ContactTypeDetailCodeList));
        this.setControlValue('ContactTypeDetailDescList', this.setFieldValue(data.ContactTypeDetailDescList));
        this.setControlValue('ContactStatusCodeList', this.setFieldValue(data.ContactStatusCodeList));
        this.setControlValue('ContactStatusDescList', this.setFieldValue(data.ContactStatusDescList));
        this.setControlValue('AvailabilityWarningMessage', this.setFieldValue(data.AvailabilityWarningMessage));
        this.setControlValue('TerminationWarningMessage', this.setFieldValue(data.TerminationWarningMessage));
        this.setControlValue('EmployeeLeftWarningMessage', this.setFieldValue(data.EmployeeLeftWarningMessage));
        this.setControlValue('IntNotifyType', this.setFieldValue(data.IntNotifyType));
        this.setControlValue('IntNotifySubjectText', this.setFieldValue(data.IntNotifySubjectText));
        this.setControlValue('IntNotifyBodyText', this.setFieldValue(data.IntNotifyBodyText));
        this.setControlValue('SaveIntNotifySubjectText', this.setFieldValue(data.SaveIntNotifySubjectText));
        this.setControlValue('SaveIntNotifyBodyText', this.setFieldValue(data.SaveIntNotifyBodyText));
        this.setControlValue('UserAuthContractUpdate', this.setFieldValue(data.UserAuthContractUpdate, true));
        this.setControlValue('WONumber', this.setFieldValue(data.WONumber));
        this.setControlValue('AddressCodeList', this.setFieldValue(data.AddressCodeList));
        this.setControlValue('AddressDescList', this.setFieldValue(data.AddressDescList));
        this.setControlValue('AddressCodeDefault', this.setFieldValue(data.AddressCodeDefault));
        this.setControlValue('AccountAddressName', this.setFieldValue(data.AccountAddressName));
        this.setControlValue('AccountAddressLine1', this.setFieldValue(data.AccountAddressLine1));
        this.setControlValue('AccountAddressLine2', this.setFieldValue(data.AccountAddressLine2));
        this.setControlValue('AccountAddressLine3', this.setFieldValue(data.AccountAddressLine3));
        this.setControlValue('AccountAddressLine4', this.setFieldValue(data.AccountAddressLine4));
        this.setControlValue('AccountAddressLine5', this.setFieldValue(data.AccountAddressLine5));
        this.setControlValue('AccountAddressPostcode', this.setFieldValue(data.AccountAddressPostcode));
        this.setControlValue('AccountAddressContactName', this.setFieldValue(data.AccountAddressContactName));
        this.setControlValue('AccountAddressContactPosn', this.setFieldValue(data.AccountAddressContactPosn));
        this.setControlValue('AccountAddressContactPhone', this.setFieldValue(data.AccountAddressContactPhone));
        this.setControlValue('AccountAddressContactMobile', this.setFieldValue(data.AccountAddressContactMobile));
        this.setControlValue('AccountAddressContactFax', this.setFieldValue(data.AccountAddressContactFax));
        this.setControlValue('AccountAddressContactEmail', this.setFieldValue(data.AccountAddressContactEmail));
        this.setControlValue('PremiseAddressName', this.setFieldValue(data.PremiseAddressName));
        this.setControlValue('PremiseAddressLine1', this.setFieldValue(data.PremiseAddressLine1));
        this.setControlValue('PremiseAddressLine2', this.setFieldValue(data.PremiseAddressLine2));
        this.setControlValue('PremiseAddressLine3', this.setFieldValue(data.PremiseAddressLine3));
        this.setControlValue('PremiseAddressLine4', this.setFieldValue(data.PremiseAddressLine4));
        this.setControlValue('PremiseAddressLine5', this.setFieldValue(data.PremiseAddressLine5));
        this.setControlValue('PremiseAddressPostcode', this.setFieldValue(data.PremiseAddressPostcode));
        this.setControlValue('PremiseAddressContactName', this.setFieldValue(data.PremiseAddressContactName));
        this.setControlValue('PremiseAddressContactPosn', this.setFieldValue(data.PremiseAddressContactPosn));
        this.setControlValue('PremiseAddressContactPhone', this.setFieldValue(data.PremiseAddressContactPhone));
        this.setControlValue('PremiseAddressContactMobile', this.setFieldValue(data.PremiseAddressContactMobile));
        this.setControlValue('PremiseAddressContactFax', this.setFieldValue(data.PremiseAddressContactFax));
        this.setControlValue('PremiseAddressContactEmail', this.setFieldValue(data.PremiseAddressContactEmail));
        this.setControlValue('ProAcctAddressName', this.setFieldValue(data.ProAcctAddressName));
        this.setControlValue('ProAcctAddressLine1', this.setFieldValue(data.ProAcctAddressLine1));
        this.setControlValue('ProAcctAddressLine2', this.setFieldValue(data.ProAcctAddressLine2));
        this.setControlValue('ProAcctAddressLine3', this.setFieldValue(data.ProAcctAddressLine3));
        this.setControlValue('ProAcctAddressLine4', this.setFieldValue(data.ProAcctAddressLine4));
        this.setControlValue('ProAcctAddressLine5', this.setFieldValue(data.ProAcctAddressLine5));
        this.setControlValue('ProAcctAddressPostcode', this.setFieldValue(data.ProAcctAddressPostcode));
        this.setControlValue('ProAcctAddressContactName', this.setFieldValue(data.ProAcctAddressContactName));
        this.setControlValue('ProAcctAddressContactPosn', this.setFieldValue(data.ProAcctAddressContactPosn));
        this.setControlValue('ProAcctAddressContactPhone', this.setFieldValue(data.ProAcctAddressContactPhone));
        this.setControlValue('ProAcctAddressContactMobile', this.setFieldValue(data.ProAcctAddressContactMobile));
        this.setControlValue('ProAcctAddressContactFax', this.setFieldValue(data.ProAcctAddressContactFax));
        this.setControlValue('ProAcctAddressContactEmail', this.setFieldValue(data.ProAcctAddressContactEmail));
        this.setControlValue('ProPremAddressName', this.setFieldValue(data.ProPremAddressName));
        this.setControlValue('ProPremAddressLine1', this.setFieldValue(data.ProPremAddressLine1));
        this.setControlValue('ProPremAddressLine2', this.setFieldValue(data.ProPremAddressLine2));
        this.setControlValue('ProPremAddressLine3', this.setFieldValue(data.ProPremAddressLine3));
        this.setControlValue('ProPremAddressLine4', this.setFieldValue(data.ProPremAddressLine4));
        this.setControlValue('ProPremAddressLine5', this.setFieldValue(data.ProPremAddressLine5));
        this.setControlValue('ProPremAddressPostcode', this.setFieldValue(data.ProPremAddressPostcode));
        this.setControlValue('ProPremAddressContactName', this.setFieldValue(data.ProPremAddressContactName));
        this.setControlValue('ProPremAddressContactPosn', this.setFieldValue(data.ProPremAddressContactPosn));
        this.setControlValue('ProPremAddressContactPhone', this.setFieldValue(data.ProPremAddressContactPhone));
        this.setControlValue('ProPremAddressContactMobile', this.setFieldValue(data.ProPremAddressContactMobile));
        this.setControlValue('ProPremAddressContactFax', this.setFieldValue(data.ProPremAddressContactFax));
        this.setControlValue('ProPremAddressContactEmail', this.setFieldValue(data.ProPremAddressContactEmail));
        this.setControlValue('PublicAddressName', this.setFieldValue(data.PublicAddressName));
        this.setControlValue('PublicAddressLine1', this.setFieldValue(data.PublicAddressLine1));
        this.setControlValue('PublicAddressLine2', this.setFieldValue(data.PublicAddressLine2));
        this.setControlValue('PublicAddressLine3', this.setFieldValue(data.PublicAddressLine3));
        this.setControlValue('PublicAddressLine4', this.setFieldValue(data.PublicAddressLine4));
        this.setControlValue('PublicAddressLine5', this.setFieldValue(data.PublicAddressLine5));
        this.setControlValue('PublicAddressPostcode', this.setFieldValue(data.PublicAddressPostcode));
        this.setControlValue('PublicAddressContactName', this.setFieldValue(data.PublicAddressContactName));
        this.setControlValue('PublicAddressContactPosn', this.setFieldValue(data.PublicAddressContactPosn));
        this.setControlValue('PublicAddressContactPhone', this.setFieldValue(data.PublicAddressContactPhone));
        this.setControlValue('PublicAddressContactMobile', this.setFieldValue(data.PublicAddressContactMobile));
        this.setControlValue('PublicAddressContactFax', this.setFieldValue(data.PublicAddressContactFax));
        this.setControlValue('PublicAddressContactEmail', this.setFieldValue(data.PublicAddressContactEmail));
        this.setControlValue('FromCustomerContactNumber', this.setFieldValue(data.FromCustomerContactNumber));
        this.setControlValue('ProspectNumber', this.setFieldValue(data.ProspectNumber));
        this.setControlValue('LinkedProspectNumber', this.setFieldValue(data.LinkedProspectNumber));
        this.setControlValue('CRContractTerm', this.setFieldValue(data.CRContractTerm, true));
        this.setControlValue('RelatedTickets', this.setFieldValue(data.RelatedTickets, true));
        this.setControlValue('RelatedNotify', this.setFieldValue(data.RelatedNotify, true));
        this.setControlValue('RelatedLogs', this.setFieldValue(data.RelatedLogs, true));
        this.setControlValue('RelatedWorkOrder', this.setFieldValue(data.RelatedWorkOrder, true));
        this.setControlValue('RelatedCustomerContactRowID', this.setFieldValue(data.RelatedCustomerContactRowID));
        this.setControlValue('RelatedCustomerContactNumber', this.setFieldValue(data.RelatedCustomerContactNumber));
        this.setControlValue('RelatedProspectNumber', this.setFieldValue(data.RelatedProspectNumber));
        this.setControlValue('RelatedProspectRowID', this.setFieldValue(data.RelatedProspectRowID));
        this.setControlValue('WarnRedirectEmployeeCode', this.setFieldValue(data.WarnRedirectEmployeeCode));
        this.setControlValue('WarnRedirectEmployeePeriod', this.setFieldValue(data.WarnRedirectEmployeePeriod));
        this.setControlValue('WarnRedirectEmployeeName', this.setFieldValue(data.WarnRedirectEmployeeName));
        this.setControlValue('WindowClosingName', this.setFieldValue(data.WindowClosingName));
        this.setControlValue('ClosedWithChanges', this.setFieldValue(data.ClosedWithChanges));
        this.setControlValue('RootCauseCode', this.setFieldValue(data.RootCauseCode));
        this.setControlValue('ActionFromDate', this.setFieldValue(data.SMSSendOnDate));
        this.setControlValue('CanAmendInvoiceDetails', this.setFieldValue(data.CanAmendInvoiceDetails));
        this.setControlValue('ActionFromTime', this.setFieldValue(data.ActionFromTime));
        this.setControlValue('DisputedInvoiceCacheName', this.setFieldValue(data.DisputedInvoiceCacheName));
        this.setControlValue('SelectedInvoiceNumber', this.setFieldValue(data.SelectedInvoiceNumber));
        this.setControlValue('SelectedCompanyInvoiceNumber', this.setFieldValue(data.SelectedCompanyInvoiceNumber));
        this.setControlValue('SelectedCompanyCode', this.setFieldValue(data.SelectedCompanyCode));
        this.setControlValue('SelectedCompanyDesc', this.setFieldValue(data.SelectedCompanyDesc));
        this.setControlValue('SelectedContractNumber', this.setFieldValue(data.SelectedContractNumber));
        this.setControlValue('SelectedContractName', this.setFieldValue(data.SelectedContractName));
        this.setControlValue('SelectedInvoiceName', this.setFieldValue(data.SelectedInvoiceName));
        this.setControlValue('DisputedInvoiceCacheTime', this.setFieldValue(data.DisputedInvoiceCacheTime));
        this.setControlValue('CopyFromTicketNumber', this.setFieldValue(data.CopyFromTicketNumber));
        this.setControlValue('CopyFromTicketNumberSave', this.setFieldValue(data.CopyFromTicketNumberSave));
        this.setControlValue('CurrentActionEmployeeCode', this.setFieldValue(data.CurrentActionEmployeeCode));
        this.setControlValue('CurrentActionEmployeeName', this.setFieldValue(data.CurrentActionEmployeeName));
        this.setControlValue('CopyMode', this.setFieldValue(data.CopyMode));
        this.setControlValue('CopyContactTypeCode', this.setFieldValue(data.CopyContactTypeCode));
        this.setControlValue('CopyContactTypeDetailCode', this.setFieldValue(data.CopyContactTypeDetailCode));
        this.setControlValue('CopyDisputed', this.setFieldValue(data.CopyDisputed));
        this.setControlValue('CopyRootCause', this.setFieldValue(data.CopyRootCause));
        this.setControlValue('SaveCopyContactTypeCode', this.setFieldValue(data.SaveCopyContactTypeCode));
        this.setControlValue('SaveCopyContactTypeDetailCode', this.setFieldValue(data.SaveCopyContactTypeDetailCode));
        this.setControlValue('OldContactTypeCode', this.setFieldValue(data.OldContactTypeCode));
    }

    private buildTable(): any {
        this.disableControl('ClosedDateTime', true);
        this.riMaintenanceAddTableField('CopyFromTicketNumber', MntConst.eTypeInteger, 'ReadOnly');
        this.riMaintenanceAddTableField('CopyDisputed', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('CopyRootCause', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('ProspectNumber', MntConst.eTypeInteger, 'ReadOnly');
        this.riMaintenanceAddTableField('LinkedProspectNumber', MntConst.eTypeInteger, 'ReadOnly');
        this.riMaintenanceAddTableField('AccountNumber', MntConst.eTypeText, 'Optional');
        this.riMaintenanceAddTableField('ContractNumber', MntConst.eTypeCode, 'Optional');
        this.riMaintenanceAddTableField('PremiseNumber', MntConst.eTypeInteger, 'Optional');
        this.riMaintenanceAddTableField('ServiceCoverNumber', MntConst.eTypeInteger, 'ReadOnly');
        this.riMaintenanceAddTableField('ProductCode', MntConst.eTypeCode, 'Optional');
        this.riMaintenanceAddTableField('CustomerContactName', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('CustomerContactPosition', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('CustomerContactTelephone', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('CustomerContactEmailAddress', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('CustomerContactMobileNumber', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('CustomerContactFax', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('CustomerContactTicketReference', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('ShortNarrative', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactNarrative', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('CreatedByEmployeeCode', MntConst.eTypeCode, 'ReadOnly');
        this.riMaintenanceAddTableField('LatestCreatedByEmployeeCode', MntConst.eTypeCode, 'ReadOnly');
        this.riMaintenanceAddTableField('CurrentOwnerEmployeeCode', MntConst.eTypeCode, 'ReadOnly');
        this.riMaintenanceAddTableField('CurrentActionEmployeeCode', MntConst.eTypeCode, 'ReadOnly');
        this.riMaintenanceAddTableField('CreatedDate', MntConst.eTypeDate, 'ReadOnly');
        this.riMaintenanceAddTableField('CreatedTime', MntConst.eTypeTime, 'ReadOnly');
        this.riMaintenanceAddTableField('ClosedDate', MntConst.eTypeDate, 'ReadOnly');
        this.riMaintenanceAddTableField('ClosedTime', MntConst.eTypeTime, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeCodeSelect', MntConst.eTypeCode, 'Required');
        this.riMaintenanceAddTableField('ContactTypeDetailCodeSelect', MntConst.eTypeCode, 'Required');
        this.riMaintenanceAddTableField('OriginalContactMediumCode', MntConst.eTypeCode, 'Required');
        this.riMaintenanceAddTableField('OriginatingEmployeeCode', MntConst.eTypeCode, 'Optional');
        this.riMaintenanceAddTableField('DefaultAssigneeEmployeeCode', MntConst.eTypeCode, 'ReadOnly');
        this.riMaintenanceAddTableField('DefaultAssigneeEmployeeDetails', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('chkContactPassedToActioner', MntConst.eTypeCheckBox, 'Optional');
        this.riMaintenanceAddTableField('CurrentTeamID', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('CurrentCallLogID', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ContactStatusCode', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ScheduledCloseDate', MntConst.eTypeDate, 'Optional');
        this.riMaintenanceAddTableField('ContactNotifyWhen', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ContactNotifyType', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ContactPostcode', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('DisputedInvoiceCacheName', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('RelatedTickets', MntConst.eTypeCheckBox, 'Optional');
        this.riMaintenanceAddTableField('RelatedNotify', MntConst.eTypeCheckBox, 'Optional');
        this.riMaintenanceAddTableField('RelatedLogs', MntConst.eTypeCheckBox, 'Optional');
        this.riMaintenanceAddTableField('RelatedWorkOrder', MntConst.eTypeCheckBox, 'Optional');
        this.riMaintenanceAddTableField('WarnRedirectEmployeeCode', MntConst.eTypeCode, 'Optional');
        this.riMaintenanceAddTableField('WarnRedirectEmployeePeriod', MntConst.eTypeCode, 'Optional');
        this.riMaintenanceAddTableField('WarnRedirectEmployeeName', MntConst.eTypeCode, 'Optional');
        this.riMaintenanceAddTableField('AddressCodeList', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AddressDescList', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AddressCodeDefault', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('EarliestEffectDate', MntConst.eTypeDate, 'Optional');
        this.riMaintenanceAddTableField('CRCommenceDate', MntConst.eTypeDate, 'ReadOnly');
        this.riMaintenanceAddTableField('CRNoticePeriod', MntConst.eTypeInteger, 'ReadOnly');
        this.riMaintenanceAddTableField('CRContractTerm', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('AccountAddressName', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressLine1', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressLine2', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressLine3', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressLine4', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressLine5', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressPostcode', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressContactName', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressContactPosn', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressContactPhone', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressContactMobile', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressContactFax', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('AccountAddressContactEmail', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressName', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressLine1', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressLine2', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressLine3', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressLine4', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressLine5', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressPostcode', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressContactName', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressContactPosn', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressContactPhone', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressContactMobile', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressContactFax', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PremiseAddressContactEmail', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressName', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressLine1', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressLine2', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressLine3', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressLine4', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressLine5', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressPostcode', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressContactName', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressContactPosn', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressContactPhone', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressContactMobile', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressContactFax', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProAcctAddressContactEmail', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressName', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressLine1', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressLine2', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressLine3', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressLine4', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressLine5', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressPostcode', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressContactName', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressContactPosn', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressContactPhone', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressContactMobile', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressContactFax', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('ProPremAddressContactEmail', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressName', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressLine1', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressLine2', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressLine3', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressLine4', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressLine5', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressPostcode', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressContactName', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressContactPosn', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressContactPhone', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressContactMobile', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressContactFax', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('PublicAddressContactEmail', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('FromCustomerContactNumber', MntConst.eTypeInteger, 'Optional');
        this.riMaintenanceAddTableField('ActionFromDate', MntConst.eTypeDate, 'Optional');
        this.riMaintenanceAddTableField('ActionFromTime', MntConst.eTypeTime, 'Optional');
        this.riMaintenanceAddTableField('ContactName', MntConst.eTypeText, 'Required', true);
        this.riMaintenanceAddTableField('ContactPosition', MntConst.eTypeText, 'Normal', true);
        this.riMaintenanceAddTableField('ContactTelephone', MntConst.eTypeText, 'Required', true);
        this.riMaintenanceAddTableField('ContactFax', MntConst.eTypeText, 'Normal', true);
        this.riMaintenanceAddTableField('ShortDescription', MntConst.eTypeTextFree, 'Required', true);
        this.riMaintenanceAddTableField('Comments', MntConst.eTypeTextFree, 'Required', true);
        this.riMaintenanceAddTableField('ContactEmailAddress', MntConst.eTypeTextFree, 'Optional', true);
        this.riMaintenanceAddTableField('ContactMobileNumber', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('TicketReference', MntConst.eTypeTextFree, 'Optional', true);
        this.riMaintenanceAddTableField('SMSSendOnDate', MntConst.eTypeDate, 'Optional');
        this.riMaintenanceAddTableField('SMSSendOnTime', MntConst.eTypeTime, 'Optional');
        this.riMaintenanceAddTableField('NextActionByDate', MntConst.eTypeDate, 'Required');
        this.riMaintenanceAddTableField('NextActionByTime', MntConst.eTypeTime, 'Required');
        this.riMaintenanceAddTableField('NextActionEmployeeCode', MntConst.eTypeCode, 'Required');
        this.riMaintenanceAddTableField('LatestCreatedDate', MntConst.eTypeDate, 'ReadOnly', true);
        this.riMaintenanceAddTableField('LatestCreatedTime', MntConst.eTypeTime, 'ReadOnly');
        this.riMaintenanceAddTableField('chkCloseTopic', MntConst.eTypeCheckBox, 'Optional');
        this.riMaintenanceAddTableField('chkKeepOwnership', MntConst.eTypeCheckBox, 'Optional');
        this.riMaintenanceAddTableField('chkCreateCallOut', MntConst.eTypeCheckBox, 'Optional');
        this.riMaintenanceAddTableField('chkCreateProspect', MntConst.eTypeCheckBox, 'Optional');
        this.riMaintenanceAddTableField('ActionCount', MntConst.eTypeInteger, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactMediumCodeSelect', MntConst.eTypeCode, 'Required');
        this.riMaintenanceAddTableField('IntNotifyType', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('IntNotifySubjectText', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('IntNotifyBodyText', MntConst.eTypeTextFree, 'Optional');
        this.riMaintenanceAddTableField('CanAmendTicketReference', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('ThisEmployeeCode', MntConst.eTypeCode, 'ReadOnly');
        this.riMaintenanceAddTableField('UserAuthContractUpdate', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeCallout', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeClientRetention', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('AllowCallout', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('AlwaysShowAssignOptions', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('ShowExportButton', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('ServiceAreaCode', MntConst.eTypeCode, 'ReadOnly');
        this.riMaintenanceAddTableField('SalesAreaCode', MntConst.eTypeCode, 'ReadOnly');
        this.riMaintenanceAddTableField('CallOutRowID', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('ServiceCoverRowID', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('ErrorMessageDesc', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('ContractTypeCodeList', MntConst.eTypeText, 'ReadOnly');
        this.riMaintenanceAddTableField('DefaultKeepOwnership', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('CanSendIntNotify', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('ExternalCREmailInd', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('ExternalCRSMSInd', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('ExternalCLEmailInd', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('ExternalCLSMSInd', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('InternalSMSInd', MntConst.eTypeCheckBox, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactStatusCodeList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactStatusDescList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailCodeList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailDescList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailOrigList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailSuperList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailPassedList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailMandAccList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailAcctReviewList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailRootCauseList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailMandConList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailMandPreList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailMandProList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailMandPostList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('ContactTypeDetailCRProspectList', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('AvailabilityWarningMessage', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('TerminationWarningMessage', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('EmployeeLeftWarningMessage', MntConst.eTypeTextFree, 'ReadOnly');
        this.riMaintenanceAddTableField('GroupAccountNumber', MntConst.eTypeInteger, 'ReadOnly');
        this.riMaintenanceAddTableField('GroupName', MntConst.eTypeTextFree, 'ReadOnly');

        this.disableControl('DispProspectNumber', true);
        this.disableControl('AddressName', true);
        this.disableControl('AddressLine1', true);
        this.disableControl('AddressLine2', true);
        this.disableControl('AddressLine3', true);
        this.disableControl('AddressLine4', true);
        this.disableControl('AddressLine5', true);
        this.disableControl('AddressPostcode', true);
        this.disableControl('AddressContactName', true);
        this.disableControl('AddressContactPosn', true);
        this.disableControl('AddressContactPhone', true);
        this.disableControl('AddressContactMobile', true);
        this.disableControl('AddressContactEmail', true);
        this.disableControl('CallLogSummary', true);
        this.disableControl('CallLogDetails', true);
        this.disableControl('NotifyMessage', true);
        this.disableControl('NotifyRecipients', true);
    }

    private getFormDataForCustomerContactMaintenance(): any {
        let formdata: Object = {};
        //Tab grdHeader
        formdata['CustomerContactNumber'] = this.getFieldValue(this.getControlValue('CustomerContactNumber'));
        formdata['CurrentTeamID'] = this.getFieldValue(this.getControlValue('CurrentTeamID'));
        formdata['ActionCount'] = this.getFieldValue(this.getControlValue('ActionCount'));
        formdata['ClosedDateTime'] = this.getFieldValue(this.getControlValue('ClosedDateTime'));
        formdata['AccountNumber'] = this.getFieldValue(this.getControlValue('AccountNumber'));
        formdata['AccountName'] = this.getFieldValue(this.getControlValue('AccountName'));
        formdata['GroupAccountNumber'] = this.getFieldValue(this.getControlValue('GroupAccountNumber'));
        formdata['GroupName'] = this.getFieldValue(this.getControlValue('GroupName'));
        formdata['ContractNumber'] = this.getFieldValue(this.getControlValue('ContractNumber'));
        formdata['ContractName'] = this.getFieldValue(this.getControlValue('ContractName'));
        formdata['CurrentOwnerEmployeeCode'] = this.getFieldValue(this.getControlValue('CurrentOwnerEmployeeCode'));
        formdata['CurrentOwnerEmployeeName'] = this.getFieldValue(this.getControlValue('CurrentOwnerEmployeeName'));
        formdata['PremiseNumber'] = this.getFieldValue(this.getControlValue('PremiseNumber'));
        formdata['PremiseName'] = this.getFieldValue(this.getControlValue('PremiseName'));
        formdata['ServiceTypeCode'] = this.getFieldValue(this.getControlValue('ServiceTypeCode'));
        formdata['ServiceTypeDesc'] = this.getFieldValue(this.getControlValue('ServiceTypeDesc'));
        formdata['ProductCode'] = this.getFieldValue(this.getControlValue('ProductCode'));
        formdata['ProductDesc'] = this.getFieldValue(this.getControlValue('ProductDesc'));
        formdata['ContactPostcode'] = this.getFieldValue(this.getControlValue('ContactPostcode'));
        formdata['DispProspectNumber'] = this.getFieldValue(this.getControlValue('DispProspectNumber'));
        formdata['ContactTypeDetailCodeSelect'] = this.getFieldValue(this.getControlValue('ContactTypeDetailCode'));
        formdata['ContactTypeCodeSelect'] = this.getFieldValue(this.getControlValue('ContactTypeCodeSelect'));

        //Tab grdAddress
        formdata['AddressName'] = this.getFieldValue(this.getControlValue('AddressName'));
        formdata['AddressContactName'] = this.getFieldValue(this.getControlValue('CustomerContactName'));
        formdata['AddressLine1'] = this.getFieldValue(this.getControlValue('AddressLine1'));
        formdata['AddressContactPosn'] = this.getFieldValue(this.getControlValue('AddressContactPosn'));
        formdata['AddressLine2'] = this.getFieldValue(this.getControlValue('AddressLine2'));
        formdata['AddressLine3'] = this.getFieldValue(this.getControlValue('AddressLine3'));
        formdata['AddressLine4'] = this.getFieldValue(this.getControlValue('AddressLine4'));
        formdata['AddressContactPhone'] = this.getFieldValue(this.getControlValue('AddressContactPhone'));
        formdata['AddressLine5'] = this.getFieldValue(this.getControlValue('AddressLine5'));
        formdata['AddressContactMobile'] = this.getFieldValue(this.getControlValue('AddressContactMobile'));
        formdata['AddressPostcode'] = this.getFieldValue(this.getControlValue('AddressPostcode'));
        formdata['AddressContactEmail'] = this.getFieldValue(this.getControlValue('AddressContactEmail'));
        formdata['AddressCodeSelect'] = this.getFieldValue(this.getControlValue('AddressCodeSelect'));
        //Tab grdNotify
        formdata['NotifyStyle'] = this.getFieldValue(this.getControlValue('NotifyStyle'));
        formdata['NotifyDestination'] = this.getFieldValue(this.getControlValue('NotifyDestination'));
        //Tab grdLatest
        formdata['ContactName'] = this.getFieldValue(this.getControlValue('ContactName'));
        formdata['ContactPosition'] = this.getFieldValue(this.getControlValue('ContactPosition'));
        formdata['ContactTelephone'] = this.getFieldValue(this.getControlValue('ContactTelephone'));
        formdata['ContactMobileNumber'] = this.getFieldValue(this.getControlValue('ContactMobileNumber'));
        formdata['ContactEmailAddress'] = this.getFieldValue(this.getControlValue('ContactEmailAddress'));
        formdata['ContactFax'] = this.getFieldValue(this.getControlValue('ContactFax'));
        formdata['TicketReference'] = this.getFieldValue(this.getControlValue('TicketReference'));
        formdata['LatestCreatedDate'] = this.getFieldValue(this.getControlValue('LatestCreatedDate'));
        formdata['LatestCreatedTime'] = this.getFieldValue(this.getControlValue('LatestCreatedTime'));
        formdata['LatestCreatedByEmployeeCode'] = this.getFieldValue(this.getControlValue('LatestCreatedByEmployeeCode'));
        formdata['LatestCreatedByEmployeeName'] = this.getFieldValue(this.getControlValue('LatestCreatedByEmployeeName'));
        formdata['ShortDescription'] = this.getFieldValue(this.getControlValue('ShortDescription'));
        formdata['ServiceAreaCode'] = this.getFieldValue(this.getControlValue('ServiceAreaCode'));
        formdata['SalesAreaCode'] = this.getFieldValue(this.getControlValue('SalesAreaCode'));
        formdata['EarliestEffectDate'] = this.getFieldValue(this.getControlValue('EarliestEffectDate'));
        formdata['chkCreateProspect'] = this.getFieldValue(this.getControlValue('chkCreateProspect'), true);
        formdata['CRCommenceDate'] = this.getFieldValue(this.getControlValue('CRCommenceDate'));
        formdata['CRNoticePeriod'] = this.getFieldValue(this.getControlValue('CRNoticePeriod'));
        formdata['NextActionByDate'] = this.globalize.parseDateToFixedFormat(this.getFieldValue(this.getControlValue('NextActionByDate'))).toString();
        formdata['NextActionByTime'] = this.getFieldValue(this.getControlValue('NextActionByTime'));
        formdata['DefaultAssigneeEmployeeDetails'] = this.getFieldValue(this.getControlValue('DefaultAssigneeEmployeeDetails'));
        formdata['OriginatingEmployeeCode'] = this.getFieldValue(this.getControlValue('OriginatingEmployeeCode'));
        formdata['OriginatingEmployeeName'] = this.getFieldValue(this.getControlValue('OriginatingEmployeeName'));
        formdata['NextActionEmployeeCode'] = this.getFieldValue(this.getControlValue('NextActionEmployeeCode'));
        formdata['NextActionEmployeeName'] = this.getFieldValue(this.getControlValue('NextActionEmployeeName'));
        formdata['chkKeepOwnership'] = this.getFieldValue(this.getControlValue('chkKeepOwnership'), true);
        formdata['chkContactPassedToActioner'] = this.getFieldValue(this.getControlValue('chkContactPassedToActioner'), true);
        formdata['SMSSendOnDate'] = this.getFieldValue(this.getControlValue('SMSSendOnDate'));
        formdata['SMSSendOnTime'] = this.getFieldValue(this.getControlValue('SMSSendOnTime'));
        formdata['chkCreateCallOut'] = this.getFieldValue(this.getControlValue('chkCreateCallOut'), true);
        formdata['ScheduledCloseDate'] = this.getFieldValue(this.getControlValue('ScheduledCloseDate'));
        formdata['ContactMediumCodeSelect'] = this.getFieldValue(this.getControlValue('ContactMediumCode'));
        formdata['ContactNotifyWhenSelect'] = this.getFieldValue(this.getControlValue('ContactNotifyWhenSelect'));
        formdata['ContactNotifyTypeSelect'] = this.getFieldValue(this.getControlValue('ContactNotifyTypeSelect'));
        formdata['ContactStatusCodeSelect'] = this.getFieldValue(this.getControlValue('ContactStatusCode'));
        formdata['Comments'] = this.getFieldValue(this.getControlValue('Comments'));
        //Tab grdOriginal
        formdata['CustomerContactName'] = this.getFieldValue(this.getControlValue('CustomerContactName'));
        formdata['CustomerContactPosition'] = this.getFieldValue(this.getControlValue('CustomerContactPosition'));
        formdata['CustomerContactTelephone'] = this.getFieldValue(this.getControlValue('CustomerContactTelephone'));
        formdata['CustomerContactMobileNumber'] = this.getFieldValue(this.getControlValue('CustomerContactMobileNumber'));
        formdata['CustomerContactEmailAddress'] = this.getFieldValue(this.getControlValue('CustomerContactEmailAddress'));
        formdata['CustomerContactFax'] = this.getFieldValue(this.getControlValue('CustomerContactFax'));
        formdata['CustomerContactTicketReference'] = this.getFieldValue(this.getControlValue('CustomerContactTicketReference'));
        formdata['CreatedDate'] = this.getFieldValue(this.getControlValue('CreatedDate'));
        formdata['CreatedTime'] = this.getFieldValue(this.getControlValue('CreatedTime'));
        formdata['CreatedByEmployeeCode'] = this.getFieldValue(this.getControlValue('CreatedByEmployeeCode'));
        formdata['CreatedByEmployeeName'] = this.getFieldValue(this.getControlValue('CreatedByEmployeeName'));
        formdata['ShortNarrative'] = this.getFieldValue(this.getControlValue('ShortNarrative'));
        formdata['ContactNarrative'] = this.getFieldValue(this.getControlValue('ContactNarrative'));
        formdata['OriginalContactMediumCodeSelect'] = this.getFieldValue(this.getControlValue('OriginalContactMediumCode'));
        //hidden values
        formdata['CanAmendTicketReference'] = this.getFieldValue(this.getControlValue('CanAmendTicketReference'));
        formdata['LanguageCode'] = this.getFieldValue(this.getControlValue('LanguageCode'));
        formdata['ThisEmployeeCode'] = this.getFieldValue(this.getControlValue('ThisEmployeeCode'));
        formdata['SearchCompanyName'] = this.getFieldValue(this.getControlValue('SearchCompanyName'));
        formdata['SearchContactName'] = this.getFieldValue(this.getControlValue('SearchContactName'));
        formdata['ContactTypeCallout'] = this.getFieldValue(this.getControlValue('ContactTypeCallout'), true);
        formdata['ContactTypeClientRetention'] = this.getFieldValue(this.getControlValue('ContactTypeClientRetention'), true);
        formdata['AllowCallOut'] = this.getFieldValue(this.getControlValue('AllowCallout'), true);
        formdata['AlwaysShowAssignOptions'] = this.getFieldValue(this.getControlValue('AlwaysShowAssignOptions'), true);
        formdata['ShowExportButton'] = this.getFieldValue(this.getControlValue('ShowExportButton'), true);
        formdata['ServiceBranchNumber'] = this.getFieldValue(this.getControlValue('ServiceBranchNumber'));
        formdata['CallOutRowID'] = this.getFieldValue(this.getControlValue('CallOutRowID'));
        formdata['ServiceCoverNumber'] = this.getFieldValue(this.getControlValue('ServiceCoverNumber'));
        formdata['ServiceCoverRowID'] = this.getFieldValue(this.getControlValue('ServiceCoverRowID'));
        formdata['ErrorMessageDesc'] = this.getFieldValue(this.getControlValue('ErrorMessageDesc'));
        formdata['ContractTypeCode'] = this.getFieldValue(this.getControlValue('ContractTypeCode'));
        formdata['ContractTypeDesc'] = this.getFieldValue(this.getControlValue('ContractTypeDesc'));
        formdata['ContractTypeCodeList'] = this.getFieldValue(this.getControlValue('ContractTypeCodeList'));
        formdata['CurrentCallLogID'] = this.getFieldValue(this.getControlValue('CurrentCallLogID'));
        formdata['ClosedDate'] = this.getFieldValue(this.getControlValue('ClosedDate'));
        formdata['ClosedTime'] = this.getFieldValue(this.getControlValue('ClosedTime'));
        formdata['chkCloseTopic'] = this.getFieldValue(this.getControlValue('chkCloseTopic'), true);
        formdata['ContactStatusCode'] = this.getFieldValue(this.getControlValue('ContactStatusCode'));
        formdata['DefaultKeepOwnership'] = this.getFieldValue(this.getControlValue('DefaultKeepOwnership'), true);
        formdata['CanSendIntNotify'] = this.getFieldValue(this.getControlValue('CanSendIntNotify'), true);
        formdata['ContactTypeCode'] = this.getFieldValue(this.getControlValue('ContactTypeCode'));
        formdata['ContactTypeDesc'] = this.getFieldValue(this.getControlValue('ContactTypeDesc'));
        formdata['ContactTypeDetailCode'] = this.getFieldValue(this.getControlValue('ContactTypeDetailCode'));
        formdata['ContactTypeDetailDesc'] = this.getFieldValue(this.getControlValue('ContactTypeDetailDesc'));
        formdata['ContactTypeDetailPassedList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailPassedList'));
        formdata['ContactTypeDetailOrigList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailOrigList'));
        formdata['ContactTypeDetailSuperList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailSuperList'));
        formdata['ContactTypeDetailMandAccList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailMandAccList'));
        formdata['ContactTypeDetailMandConList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailMandConList'));
        formdata['ContactTypeDetailMandPreList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailMandPreList'));
        formdata['ContactTypeDetailMandProList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailMandProList'));
        formdata['ContactTypeDetailMandPostList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailMandPostList'));
        formdata['ContactTypeDetailAcctReviewList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailAcctReviewList'));
        formdata['ContactTypeDetailRootCauseList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailRootCauseList'));
        formdata['ContactTypeDetailCRProspectList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailCRProspectList'));
        formdata['DefaultAssigneeEmployeeCode'] = this.getFieldValue(this.getControlValue('DefaultAssigneeEmployeeCode'));
        formdata['DefaultAssigneeEmployeeName'] = this.getFieldValue(this.getControlValue('DefaultAssigneeEmployeeName'));
        formdata['ContactRedirectionUniqueID'] = this.getFieldValue(this.getControlValue('ContactRedirectionUniqueID'));
        formdata['ContactMediumCode'] = this.getFieldValue(this.getControlValue('ContactMediumCode'));
        formdata['OriginalContactMediumCode'] = this.getFieldValue(this.getControlValue('OriginalContactMediumCode'));
        formdata['ExternalCREmailInd'] = this.getFieldValue(this.getControlValue('ExternalCREmailInd'), true);
        formdata['ExternalCLEmailInd'] = this.getFieldValue(this.getControlValue('ExternalCLEmailInd'), true);
        formdata['ExternalCRSMSInd'] = this.getFieldValue(this.getControlValue('ExternalCRSMSInd'), true);
        formdata['ExternalCLSMSInd'] = this.getFieldValue(this.getControlValue('ExternalCLSMSInd'), true);
        formdata['InternalSMSInd'] = this.getFieldValue(this.getControlValue('InternalSMSInd'), true);
        formdata['ContactNotifyWhen'] = this.getFieldValue(this.getControlValue('ContactNotifyWhenSelect'));
        formdata['ContactNotifyType'] = this.getFieldValue(this.getControlValue('ContactNotifyTypeSelect'));
        formdata['RunFromParentMode'] = this.getFieldValue(this.getControlValue('RunFromParentMode'));
        formdata['ContactTypeDetailCodeList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailCodeList'));
        formdata['ContactTypeDetailDescList'] = this.getFieldValue(this.getControlValue('ContactTypeDetailDescList'));
        formdata['ContactStatusCodeList'] = this.getFieldValue(this.getControlValue('ContactStatusCodeList'));
        formdata['ContactStatusDescList'] = this.getFieldValue(this.getControlValue('ContactStatusDescList'));
        formdata['AvailabilityWarningMessage'] = this.getFieldValue(this.getControlValue('AvailabilityWarningMessage'));
        formdata['TerminationWarningMessage'] = this.getFieldValue(this.getControlValue('TerminationWarningMessage'));
        formdata['EmployeeLeftWarningMessage'] = this.getFieldValue(this.getControlValue('EmployeeLeftWarningMessage'));
        formdata['IntNotifyType'] = this.getFieldValue(this.getControlValue('IntNotifyType'));
        formdata['IntNotifySubjectText'] = this.getFieldValue(this.getControlValue('IntNotifySubjectText'));
        formdata['IntNotifyBodyText'] = this.getFieldValue(this.getControlValue('IntNotifyBodyText'));
        formdata['SaveIntNotifySubjectText'] = this.getFieldValue(this.getControlValue('SaveIntNotifySubjectText'));
        formdata['SaveIntNotifyBodyText'] = this.getFieldValue(this.getControlValue('SaveIntNotifyBodyText'));
        formdata['UserAuthContractUpdate'] = this.getFieldValue(this.getControlValue('UserAuthContractUpdate'), true);
        formdata['WONumber'] = this.getFieldValue(this.getControlValue('WONumber'));
        formdata['AddressCodeList'] = this.getFieldValue(this.getControlValue('AddressCodeList'));
        formdata['AddressDescList'] = this.getFieldValue(this.getControlValue('AddressDescList'));
        formdata['AddressCodeDefault'] = this.getFieldValue(this.getControlValue('AddressCodeDefault'));
        formdata['AccountAddressName'] = this.getFieldValue(this.getControlValue('AccountAddressName'));
        formdata['AccountAddressLine1'] = this.getFieldValue(this.getControlValue('AccountAddressLine1'));
        formdata['AccountAddressLine2'] = this.getFieldValue(this.getControlValue('AccountAddressLine2'));
        formdata['AccountAddressLine3'] = this.getFieldValue(this.getControlValue('AccountAddressLine3'));
        formdata['AccountAddressLine4'] = this.getFieldValue(this.getControlValue('AccountAddressLine4'));
        formdata['AccountAddressLine5'] = this.getFieldValue(this.getControlValue('AccountAddressLine5'));
        formdata['AccountAddressPostcode'] = this.getFieldValue(this.getControlValue('AccountAddressPostcode'));
        formdata['AccountAddressContactName'] = this.getFieldValue(this.getControlValue('AccountAddressContactName'));
        formdata['AccountAddressContactPosn'] = this.getFieldValue(this.getControlValue('AccountAddressContactPosn'));
        formdata['AccountAddressContactPhone'] = this.getFieldValue(this.getControlValue('AccountAddressContactPhone'));
        formdata['AccountAddressContactMobile'] = this.getFieldValue(this.getControlValue('AccountAddressContactMobile'));
        formdata['AccountAddressContactFax'] = this.getFieldValue(this.getControlValue('AccountAddressContactFax'));
        formdata['AccountAddressContactEmail'] = this.getFieldValue(this.getControlValue('AccountAddressContactEmail'));
        formdata['PremiseAddressName'] = this.getFieldValue(this.getControlValue('PremiseAddressName'));
        formdata['PremiseAddressLine1'] = this.getFieldValue(this.getControlValue('PremiseAddressLine1'));
        formdata['PremiseAddressLine2'] = this.getFieldValue(this.getControlValue('PremiseAddressLine2'));
        formdata['PremiseAddressLine3'] = this.getFieldValue(this.getControlValue('PremiseAddressLine3'));
        formdata['PremiseAddressLine4'] = this.getFieldValue(this.getControlValue('PremiseAddressLine4'));
        formdata['PremiseAddressLine5'] = this.getFieldValue(this.getControlValue('PremiseAddressLine5'));
        formdata['PremiseAddressPostcode'] = this.getFieldValue(this.getControlValue('PremiseAddressPostcode'));
        formdata['PremiseAddressContactName'] = this.getFieldValue(this.getControlValue('PremiseAddressContactName'));
        formdata['PremiseAddressContactPosn'] = this.getFieldValue(this.getControlValue('PremiseAddressContactPosn'));
        formdata['PremiseAddressContactPhone'] = this.getFieldValue(this.getControlValue('PremiseAddressContactPhone'));
        formdata['PremiseAddressContactMobile'] = this.getFieldValue(this.getControlValue('PremiseAddressContactMobile'));
        formdata['PremiseAddressContactFax'] = this.getFieldValue(this.getControlValue('PremiseAddressContactFax'));
        formdata['PremiseAddressContactEmail'] = this.getFieldValue(this.getControlValue('PremiseAddressContactEmail'));
        formdata['ProAcctAddressName'] = this.getFieldValue(this.getControlValue('ProAcctAddressName'));
        formdata['ProAcctAddressLine1'] = this.getFieldValue(this.getControlValue('ProAcctAddressLine1'));
        formdata['ProAcctAddressLine2'] = this.getFieldValue(this.getControlValue('ProAcctAddressLine2'));
        formdata['ProAcctAddressLine3'] = this.getFieldValue(this.getControlValue('ProAcctAddressLine3'));
        formdata['ProAcctAddressLine4'] = this.getFieldValue(this.getControlValue('ProAcctAddressLine4'));
        formdata['ProAcctAddressLine5'] = this.getFieldValue(this.getControlValue('ProAcctAddressLine5'));
        formdata['ProAcctAddressPostcode'] = this.getFieldValue(this.getControlValue('ProAcctAddressPostcode'));
        formdata['ProAcctAddressContactName'] = this.getFieldValue(this.getControlValue('ProAcctAddressContactName'));
        formdata['ProAcctAddressContactPosn'] = this.getFieldValue(this.getControlValue('ProAcctAddressContactPosn'));
        formdata['ProAcctAddressContactPhone'] = this.getFieldValue(this.getControlValue('ProAcctAddressContactPhone'));
        formdata['ProAcctAddressContactMobile'] = this.getFieldValue(this.getControlValue('ProAcctAddressContactMobile'));
        formdata['ProAcctAddressContactFax'] = this.getFieldValue(this.getControlValue('ProAcctAddressContactFax'));
        formdata['ProAcctAddressContactEmail'] = this.getFieldValue(this.getControlValue('ProAcctAddressContactEmail'));
        formdata['ProPremAddressName'] = this.getFieldValue(this.getControlValue('ProPremAddressName'));
        formdata['ProPremAddressLine1'] = this.getFieldValue(this.getControlValue('ProPremAddressLine1'));
        formdata['ProPremAddressLine2'] = this.getFieldValue(this.getControlValue('ProPremAddressLine2'));
        formdata['ProPremAddressLine3'] = this.getFieldValue(this.getControlValue('ProPremAddressLine3'));
        formdata['ProPremAddressLine4'] = this.getFieldValue(this.getControlValue('ProPremAddressLine4'));
        formdata['ProPremAddressLine5'] = this.getFieldValue(this.getControlValue('ProPremAddressLine5'));
        formdata['ProPremAddressPostcode'] = this.getFieldValue(this.getControlValue('ProPremAddressPostcode'));
        formdata['ProPremAddressContactName'] = this.getFieldValue(this.getControlValue('ProPremAddressContactName'));
        formdata['ProPremAddressContactPosn'] = this.getFieldValue(this.getControlValue('ProPremAddressContactPosn'));
        formdata['ProPremAddressContactPhone'] = this.getFieldValue(this.getControlValue('ProPremAddressContactPhone'));
        formdata['ProPremAddressContactMobile'] = this.getFieldValue(this.getControlValue('ProPremAddressContactMobile'));
        formdata['ProPremAddressContactFax'] = this.getFieldValue(this.getControlValue('ProPremAddressContactFax'));
        formdata['ProPremAddressContactEmail'] = this.getFieldValue(this.getControlValue('ProPremAddressContactEmail'));
        formdata['PublicAddressName'] = this.getFieldValue(this.getControlValue('PublicAddressName'));
        formdata['PublicAddressLine1'] = this.getFieldValue(this.getControlValue('PublicAddressLine1'));
        formdata['PublicAddressLine2'] = this.getFieldValue(this.getControlValue('PublicAddressLine2'));
        formdata['PublicAddressLine3'] = this.getFieldValue(this.getControlValue('PublicAddressLine3'));
        formdata['PublicAddressLine4'] = this.getFieldValue(this.getControlValue('PublicAddressLine4'));
        formdata['PublicAddressLine5'] = this.getFieldValue(this.getControlValue('PublicAddressLine5'));
        formdata['PublicAddressPostcode'] = this.getFieldValue(this.getControlValue('PublicAddressPostcode'));
        formdata['PublicAddressContactName'] = this.getFieldValue(this.getControlValue('PublicAddressContactName'));
        formdata['PublicAddressContactPosn'] = this.getFieldValue(this.getControlValue('PublicAddressContactPosn'));
        formdata['PublicAddressContactPhone'] = this.getFieldValue(this.getControlValue('PublicAddressContactPhone'));
        formdata['PublicAddressContactMobile'] = this.getFieldValue(this.getControlValue('PublicAddressContactMobile'));
        formdata['PublicAddressContactFax'] = this.getFieldValue(this.getControlValue('PublicAddressContactFax'));
        formdata['PublicAddressContactEmail'] = this.getFieldValue(this.getControlValue('PublicAddressContactEmail'));
        formdata['FromCustomerContactNumber'] = this.getFieldValue(this.getControlValue('FromCustomerContactNumber'));
        formdata['ProspectNumber'] = this.getFieldValue(this.getControlValue('ProspectNumber'));
        formdata['LinkedProspectNumber'] = this.getFieldValue(this.getControlValue('LinkedProspectNumber'));
        formdata['CRContractTerm'] = this.getFieldValue(this.getControlValue('CRContractTerm'), true);
        formdata['RelatedTickets'] = this.getFieldValue(this.getControlValue('RelatedTickets'), true);
        formdata['RelatedNotify'] = this.getFieldValue(this.getControlValue('RelatedNotify'), true);
        formdata['RelatedLogs'] = this.getFieldValue(this.getControlValue('RelatedLogs'), true);
        formdata['RelatedWorkOrder'] = this.getFieldValue(this.getControlValue('RelatedWorkOrder'), true);
        formdata['RelatedCustomerContactRowID'] = this.getFieldValue(this.getControlValue('RelatedCustomerContactRowID'));
        formdata['RelatedCustomerContactNumber'] = this.getFieldValue(this.getControlValue('RelatedCustomerContactNumber'));
        formdata['RelatedProspectNumber'] = this.getFieldValue(this.getControlValue('RelatedProspectNumber'));
        formdata['RelatedProspectRowID'] = this.getFieldValue(this.getControlValue('RelatedProspectRowID'));
        formdata['WarnRedirectEmployeeCode'] = this.getFieldValue(this.getControlValue('WarnRedirectEmployeeCode'));
        formdata['WarnRedirectEmployeePeriod'] = this.getFieldValue(this.getControlValue('WarnRedirectEmployeePeriod'));
        formdata['WarnRedirectEmployeeName'] = this.getFieldValue(this.getControlValue('WarnRedirectEmployeeName'));
        formdata['WindowClosingName'] = this.getFieldValue(this.getControlValue('WindowClosingName'));
        formdata['ClosedWithChanges'] = this.getFieldValue(this.getControlValue('ClosedWithChanges'));
        formdata['RootCauseCode'] = this.getFieldValue(this.getControlValue('RootCauseCode'));
        formdata['ActionFromDate'] = this.getFieldValue(this.getControlValue('ActionFromDate'));
        formdata['CanAmendInvoiceDetails'] = this.getFieldValue(this.getControlValue('CanAmendInvoiceDetails'));
        formdata['ActionFromTime'] = this.getFieldValue(this.getControlValue('ActionFromTime'));
        formdata['DisputedInvoiceCacheName'] = this.getFieldValue(this.getControlValue('DisputedInvoiceCacheName'));
        formdata['SelectedInvoiceNumber'] = this.getFieldValue(this.getControlValue('SelectedInvoiceNumber'));
        formdata['SelectedCompanyInvoiceNumber'] = this.getFieldValue(this.getControlValue('SelectedCompanyInvoiceNumber'));
        formdata['SelectedCompanyCode'] = this.getFieldValue(this.getControlValue('SelectedCompanyCode'));
        formdata['SelectedCompanyDesc'] = this.getFieldValue(this.getControlValue('SelectedCompanyDesc'));
        formdata['SelectedContractNumber'] = this.getFieldValue(this.getControlValue('SelectedContractNumber'));
        formdata['SelectedContractName'] = this.getFieldValue(this.getControlValue('SelectedContractName'));
        formdata['SelectedInvoiceName'] = this.getFieldValue(this.getControlValue('SelectedInvoiceName'));
        formdata['DisputedInvoiceCacheTime'] = this.getFieldValue(this.getControlValue('DisputedInvoiceCacheTime'));
        formdata['CopyFromTicketNumber'] = this.getFieldValue(this.getControlValue('CopyFromTicketNumber'));
        formdata['CopyFromTicketNumberSave'] = this.getFieldValue(this.getControlValue('CopyFromTicketNumberSave'));
        formdata['CurrentActionEmployeeCode'] = this.getFieldValue(this.getControlValue('CurrentActionEmployeeCode'));
        formdata['CurrentActionEmployeeName'] = this.getFieldValue(this.getControlValue('CurrentActionEmployeeName'));
        formdata['CopyMode'] = this.getFieldValue(this.getControlValue('CopyMode'));
        formdata['CopyContactTypeCode'] = this.getFieldValue(this.getControlValue('CopyContactTypeCode'));
        formdata['CopyContactTypeDetailCode'] = this.getFieldValue(this.getControlValue('CopyContactTypeDetailCode'));
        formdata['CopyDisputed'] = this.getFieldValue(this.getControlValue('CopyDisputed'));
        formdata['CopyRootCause'] = this.getFieldValue(this.getControlValue('CopyRootCause'));
        formdata['SaveCopyContactTypeCode'] = this.getFieldValue(this.getControlValue('SaveCopyContactTypeCode'));
        formdata['SaveCopyContactTypeDetailCode'] = this.getFieldValue(this.getControlValue('SaveCopyContactTypeDetailCode'));
        formdata['OldContactTypeCode'] = this.getFieldValue(this.getControlValue('OldContactTypeCode'));
        return formdata;
    }

    private updateCustomerContactMaintenanceData(): any {
        let formdata: Object = {};
        let queryParams: QueryParams = this.getURLSearchParamObject();
        queryParams.set(this.serviceConstants.Action, '2');
        formdata = this.getFormDataForCustomerContactMaintenance();
        formdata['CustomerContact'] = this.storeData['CustomerContact'] || '';
        formdata['ContactAction'] = this.storeData['ContactAction'] || '';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryParams, formdata).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully);
                    modalVO.closeCallback = function closeModal(): any {
                        this.formPristine();
                        // CR: For closed ticket, navigate to the previous screen
                        if (this.getControlValue('ContactStatusCodeSelect') === 'CLOSED') {
                            this.location.back();
                        }
                        else {
                            this.cancelEvent = false;
                            this.storeData = data;
                            this.cloneFormData();
                            this.isSaveReloadStatus = true;
                            let copyMode: string = this.getControlValue('CopyMode');
                            let reloadStatus: boolean = this.riMaintenanceAfterSaveForUpdate();
                            this.setFormData(data);
                            this.pageParams.CustomerContactNumber = data['CustomerContactNumber'] || '';
                            this.pageParams.CustomerContact = data['CustomerContact'] || '';
                            this.pageParams.ContactAction = data['ContactAction'] || '';
                            if (reloadStatus) {
                                if (copyMode === 'Y') {
                                    this.setControlValue('CopyMode', '');
                                    this.reloadFormWithUpdatedData();
                                }
                                else {
                                    this.reloadPageInUpdateMode();
                                }
                            }
                        }
                    }.bind(this);
                    this.modalAdvService.emitMessage(modalVO);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    private addCustomerContactMaintenanceData(): any {
        let formdata: Object = {};
        let queryParams: QueryParams = this.getURLSearchParamObject();
        queryParams.set(this.serviceConstants.Action, '1');
        formdata = this.getFormDataForCustomerContactMaintenance();
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryParams, formdata).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                }
                else {
                    this.setControlValue('CustomerContactNumber', e['CustomerContactNumber']);
                    this.pageParams.CustomerContactNumber = e['CustomerContactNumber'] || '';
                    this.pageParams.CustomerContact = e['CustomerContact'] || '';
                    this.pageParams.ContactAction = e['ContactAction'] || '';
                    let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully);
                    modalVO.closeCallback = function closeModal(): any {
                        this.storeData = e;
                        this.formPristine();
                        this.pageParams.cInitialMode = 'Update';
                        this.currentMode = MntConst.eModeUpdate;
                        let reloadStatus: boolean = this.riMaintenanceAfterSave();
                        this.setFormData(e);
                        if (reloadStatus) {
                            this.setControlValue('CopyMode', '');
                            this.reloadFormWithUpdatedData();
                        }
                    }.bind(this);
                    this.modalAdvService.emitMessage(modalVO);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    private reloadFormWithUpdatedData(): void {
        let urlParams: Object = {
            CustomerContactNumber: this.pageParams['CustomerContactNumber'],
            CustomerContactROWID: this.pageParams['CustomerContact'],
            ContactAction: this.pageParams['ContactAction'],
            isReturnFlag: true
        };
        //navigate to screen ContactManagement/iCABSCMCustomerContactMaintenance.htm' Mode = current mode
        let url = InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_2;
        if (this.pageParams.currentRouteUrl && this.instr(this.pageParams.currentRouteUrl.toString().toLowerCase(), InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_2.toLowerCase()) >= 0) {
            url = InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_1;
        }
        this.pageParams.cInitialMode = 'Update';
        this.pageParams.previousRouteUrl = url;
        this.pageParams.selfLoad = true;
        this.navigate(this.parentMode, url, urlParams);
    }

    private riTabFocusAfter(): any {
        //' Only Refresh The Grids Once On Entry To The Tab
        if (this.currentTabID === 'grdDisputedInvoices' && this.pageParams.lRefreshDisputedInvoicesGrid) {
            this.setupDisputedInvoicesGrid();
            this.riGridDisputedInvoicesBeforeExecute();
            this.pageParams.lRefreshDisputedInvoicesGrid = false;
            if (this.isMobile) {
                this.activeTabGridToSlide = this.gridToSlide_dispute;
                this.columnsToHide = this.columnsToHideForActiveSlide['dispute'];
            }
        }
        if (this.currentTabID === 'grdRootCause' && this.pageParams.lRefreshRootCauseGrid) {
            this.setupRootCauseGrid();
            this.riGridRootCauseBeforeExecute();
            this.pageParams.lRefreshRootCauseGrid = false;
            if (this.isMobile) {
                this.activeTabGridToSlide = this.gridToSlide_rootcause;
                this.columnsToHide = this.columnsToHideForActiveSlide['rootcause'];
            }
        }
        if (this.currentTabID === 'grdWorkOrder' && this.pageParams.lRefreshWorkOrderGrid) {
            this.setupWorkOrderGrid();
            this.riGridWorkOrderBeforeExecute();
            this.pageParams.lRefreshWorkOrderGrid = false;
            if (this.isMobile) {
                this.activeTabGridToSlide = this.gridToSlide_workorder;
                this.columnsToHide = this.columnsToHideForActiveSlide['workorder'];
            }
        }
        if (this.currentTabID === 'grdNotify' && this.pageParams.lRefreshNotifyGrid) {
            this.setupNotifyGrid();
            this.riGridNotifyBeforeExecute();
            this.pageParams.lRefreshNotifyGrid = false;
            if (this.isMobile) {
                this.activeTabGridToSlide = this.gridToSlide_notify;
                this.columnsToHide = this.columnsToHideForActiveSlide['notification'];
            }
        }
        if (this.currentTabID === 'grdLogs' && this.pageParams.lRefreshLogsGrid) {
            this.setupLogGrid();
            this.riGridLogsBeforeExecute();
            this.pageParams.lRefreshLogsGrid = false;
            if (this.isMobile) {
                this.activeTabGridToSlide = this.gridToSlide_logs;
                this.columnsToHide = this.columnsToHideForActiveSlide['logs'];
            }
        }
        if (this.currentTabID === 'grdTickets' && this.pageParams.lRefreshTicketsGrid) {
            this.setupTicketGrid();
            this.riGridTicketsBeforeExecute();
            this.pageParams.lRefreshTicketsGrid = false;
            if (this.isMobile) {
                this.activeTabGridToSlide = this.gridToSlide_tickets;
                this.columnsToHide = this.columnsToHideForActiveSlide['tickets'];
            }
        }
    }

    private riGridTicketsBeforeExecute(): any {
        let gridPostParams: Object = {
            CustomerContactNumber: this.getControlValue('CustomerContactNumber'),
            TabName: 'RelatedTickets'
        };
        this.riGridTickets.HighlightBar = true;
        this.executeGrid(this.riGridTickets, this.gridConfig.riGridTickets, '', gridPostParams);
    }

    private riGridLogsBeforeExecute(): any {
        let gridPostParams: Object = {
            CustomerContactNumber: this.getControlValue('CustomerContactNumber'),
            TabName: 'EntryLogs'
        };
        this.riGridLogs.HighlightBar = true;
        this.executeGrid(this.riGridLogs, this.gridConfig.riGridLogs, '', gridPostParams);
    }

    private riGridNotifyBeforeExecute(): any {
        this.setControlValue('NotifyRecipients', '');
        this.setControlValue('NotifyMessage', '');
        this.fieldVisibility.trNotifyMessage = false;
        this.fieldVisibility.trNotifyRecipients = false;
        let gridParams: Object = {
            CustomerContactNumber: this.getControlValue('CustomerContactNumber'),
            NotifyStyle: this.getControlValue('NotifyStyle'),
            NotifyDestination: this.getControlValue('NotifyDestination')
        };
        this.riGridNotify.HighlightBar = true;
        this.executeGrid(this.riGridNotify, this.gridConfig.riGridNotify, gridParams);
    }

    private riGridWorkOrderBeforeExecute(): any {
        let gridPostParams: Object = {
            CustomerContactNumber: this.getControlValue('CustomerContactNumber'),
            TabName: 'EntryWorkOrders'
        };
        this.riGridWorkOrder.HighlightBar = true;
        this.executeGrid(this.riGridWorkOrder, this.gridConfig.riGridWorkOrder, '', gridPostParams);
    }

    private riGridRootCauseBeforeExecute(): any {
        let gridPostParams: Object = {
            CustomerContactNumber: this.getControlValue('CustomerContactNumber'),
            TabName: 'EntryRootCause'
        };
        this.executeGrid(this.riGridRootCause, this.gridConfig.riGridRootCause, '', gridPostParams);
    }

    private riGridDisputedInvoicesBeforeExecute(): any {
        let gridPostParams: Object = {
            CustomerContactNumber: this.getControlValue('CustomerContactNumber'),
            CanAmendInvoice: this.getControlValue('CanAmendInvoiceDetails')
        };
        this.executeGrid(this.riGridDisputedInvoices, this.gridConfig.riGridDisputedInvoices, '', gridPostParams);
    }

    private executeGrid(grid: GridAdvancedComponent, gridConfigData: any, gridParams: any, postData?: any): any {
        let gridQueryParams: QueryParams = this.getURLSearchParamObject();
        gridQueryParams.set(this.serviceConstants.GridPageSize, gridConfigData.pageSize.toString());
        gridQueryParams.set(this.serviceConstants.Action, '2');
        gridQueryParams.set(this.serviceConstants.GridMode, '0');
        gridQueryParams.set(this.serviceConstants.GridCacheRefresh, 'True');
        gridQueryParams.set(this.serviceConstants.GridHandle, this.utils.gridHandle);
        gridQueryParams.set(this.serviceConstants.GridHeaderClickedColumn, grid.HeaderClickedColumn);
        gridQueryParams.set(this.serviceConstants.PageCurrent, gridConfigData.gridCurPage.toString());

        if (this.isMobile) {
            gridQueryParams.set(this.serviceConstants.PageCurrent, this.currentPageForSlides);
        }

        if (gridConfigData && gridConfigData['gridSort'] === true) {
            let sortOrder: any = 'Descending';
            if (!grid.DescendingSort) {
                sortOrder = 'Ascending';
            }
            gridQueryParams.set('riSortOrder', sortOrder);
        }
        for (let key in gridParams) {
            if (gridParams.hasOwnProperty(key)) {
                gridQueryParams.set(key, gridParams[key]);
            }
        }

        if (postData) {
            this.ajaxSource.next(this.ajaxconstant.START);
            let httpServiceSubs: Subscription = this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, gridQueryParams, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data) {
                        this.processGridRequest(grid, gridConfigData, data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
            this.subscriptionManager.add(httpServiceSubs);
        }
        else {
            this.ajaxSource.next(this.ajaxconstant.START);
            let httpSubs: Subscription = this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, gridQueryParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data) {
                        this.processGridRequest(grid, gridConfigData, data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
            this.subscriptionManager.add(httpSubs);
        }
    }

    private processGridRequest(grid: GridAdvancedComponent, gridConfigData: any, data: any): void {
        if (data) {
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                gridConfigData.totalRecords = 0;
                grid.ResetGrid();
            } else {
                if (this.isMobile) {
                    if (data.body && data.body.cells && !data.body.cells.length) { // body with not records to show
                        this.pageMsgForTabs[this.currentTabID] = this.pageMsgObj['noRecord'];
                        return;
                    }
                    this.activeTabGridToSlide.loadSlides(data);
                    this.pageMsgForTabs[this.currentTabID] = '';
                    this.currentPageForSlides = (this.currentPageForSlides < data.pageData.lastPageNumber) ? this.currentPageForSlides++ : data.pageData.lastPageNumber;
                    return;
                }

                gridConfigData.gridCurPage = data.pageData ? data.pageData.pageNumber : 1;
                gridConfigData.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                grid.UpdateBody = true;
                grid.UpdateFooter = true;
                grid.UpdateHeader = true;
                grid.Execute(data);
            }
        }
    }

    private returnDataFetch(data: any, field: any): any {
        return (data && this.utils.customTruthyCheck(data[field])) ? data[field] : '';
    }

    private enableScheduleButton(): any {
        if (this.getControlValue('NextActionEmployeeCode')) {
            let postDataAdd = {
                Function: 'GetExternalID',
                BusinessCode: this.utils.getBusinessCode(),
                EmployeeCode: this.getControlValue('NextActionEmployeeCode')
            };

            this.executeRequest(this, 'GetExternalID', '', postDataAdd, (data) => {
                if (data && !data.hasError) {
                    if (data['ExternalID']) {
                        this.buttonControls.cmdScheduleVisit.disabled = false;
                    } else {
                        this.buttonControls.cmdScheduleVisit.disabled = true;
                    }
                }
                else {
                    this.buttonControls.cmdScheduleVisit.disabled = true;
                }
            });
        }
    }

    private getServerData(strEvent: any): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        let strAddMode: string;
        let strFunctionList: string;
        let strCopying: string;
        let description: string;
        let modalVo: ICabsModalVO;
        //' When copying a ticket we need to ensure that the contact details from the origin ticket are not overwritten
        //' by the contact details from the account, contract and/or premise
        if (this.getControlValue('CopyFromTicketNumber')) {
            strCopying = 'Y';
        } else {
            strCopying = 'N';
        }
        if (this.currentMode === MntConst.eModeAdd || this.getControlValue('CopyMode') === 'Y') {
            strAddMode = 'Yes';
        } else {
            strAddMode = 'No';
        }

        // ' Lets decide what needs to be run based upon whats just happened!
        switch (strEvent) {
            case 'GetDetailsOnEntryUpdate':
                strFunctionList = 'GetContactTypeInfo,GetNotificationDetails';
                break;
            case 'ChangeOfContactType':
                strFunctionList = 'GetContactStatus,GetContactTypeInfo,CheckContractTerm';
                break;
            case 'ChangeOfContactTypeDetail':
                if (strAddMode === 'Yes' || (this.currentMode === MntConst.eModeUpdate && this.pageParams.lContactTypeDetailAmendInd)) {
                    strFunctionList = 'GetCRDisplayDefaults,GetOrigAndAssignToEmployeeCode,GetNotificationDetails';
                }
                break;
            case 'ChangeOfAccount':
                if (this.trim(this.getControlValue('AccountNumber')) !== '') {
                    if (strAddMode === 'Yes') {
                        if (strCopying === 'Y') {
                            strFunctionList = 'GetAccountDetails';
                        } else {
                            strFunctionList = 'GetAccountDetails,GetContactInfo';
                        }
                    }
                }
                break;
            case 'ChangeOfContractOnEntryNew':
                if (strAddMode === 'Yes') {
                    if (strCopying === 'Y') {
                        strFunctionList = 'GetContractAndAccountDetails';
                    } else {
                        strFunctionList = 'GetContractAndAccountDetails,GetContactInfo';
                    }
                    if (this.trim(this.getControlValue('PremiseNumber')) !== '') {
                        strFunctionList = strFunctionList + ',GetPremiseDetails';
                    }
                }
                break;
            case 'ChangeOfPostcode':
                strFunctionList = 'GetOrigAndAssignToEmployeeCode,GetNotificationDetails';
                break;
            case 'ChangeOfContract':
                if (strAddMode === 'Yes') {
                    if (strCopying === 'Y') {
                        strFunctionList = 'GetContractAndAccountDetails,GetOrigAndAssignToEmployeeCode,GetNotificationDetails';
                    } else {
                        strFunctionList = 'GetContractAndAccountDetails,GetOrigAndAssignToEmployeeCode,GetContactInfo,GetNotificationDetails';
                    }
                }
                break;
            case 'ChangeOfPremise':
                if (strAddMode === 'Yes') {
                    if (strCopying === 'Y') {
                        strFunctionList = 'GetPremiseDetails,GetOrigAndAssignToEmployeeCode,GetNotificationDetails';
                    } else {
                        strFunctionList = 'GetPremiseDetails,GetOrigAndAssignToEmployeeCode,GetContactInfo,GetNotificationDetails';
                    }
                }
                break;
            case 'ChangeOfProduct':
                if (strAddMode === 'Yes') {
                    if (strCopying === 'Y') {
                        strFunctionList = 'GetCRDisplayDefaults,GetOrigAndAssignToEmployeeCode,GetNotificationDetails';
                    } else {
                        strFunctionList = 'GetCRDisplayDefaults,GetOrigAndAssignToEmployeeCode,GetContactInfo,GetNotificationDetails';
                    }
                }
                break;
            case 'ChangeOfOriginatingEmployee':
                strFunctionList = 'GetAssignToEmployeeCode,GetNotificationDetails';
                break;
            case 'ChangeOfAssignee':
                if (!this.isInitialising) {
                    strFunctionList = 'GetAssigneeName,GetNotificationDetails,GetAvailability,CheckEmployeeLeft';
                } else {
                    strFunctionList = 'GetNotificationDetails,CheckEmployeeLeft';
                }
                break;
        }

        if (this.trim(strFunctionList)) {
            this.CBORequestClear();
            this.customBusinessObjectAdditionalPostData = {
                Function: strFunctionList, LanguageCode: this.riExchange.LanguageCode()
                , BranchNumber: this.utils.getBranchCode(), AddMode: strAddMode
            };

            //' Don't send any null values to the server, otherwise the this is not made && no err || is shown - really //friendly eh!
            this.setControlValue('RunFromParentMode', this.parentMode);

            // ' When Simply Getting Details (called from AfterFetch) ) { Don't ReAssign Anything!
            if (strEvent !== 'GetDetailsOnEntryUpdate') {
                this.setControlValue('ContactTypeCode', this.getControlValue('ContactTypeCodeSelect'));
            }
            if (strEvent !== 'GetDetailsOnEntryUpdate' && this.getControlValue('CopyMode') !== 'Y') {
                this.setControlValue('ContactTypeDetailCode', this.getControlValue('ContactTypeDetailCodeSelect'));
            }
            if (this.trim(this.getControlValue('RunFromParentMode'))) {
                this.CBORequestAdd('RunFromParentMode');
            }
                if (this.trim(this.getControlValue('CustomerContactNumber'))) {
                    this.CBORequestAdd('CustomerContactNumber');
                }
            if (this.trim(this.getControlValue('CurrentCallLogID')) && this.getControlValue('CurrentCallLogID') !== '?') {
                    this.CBORequestAdd('CurrentCallLogID');
                }
                if (this.trim(this.getControlValue('AccountNumber'))) {
                    this.CBORequestAdd('AccountNumber');
                }
                if (this.trim(this.getControlValue('ContractNumber'))) {
                    this.CBORequestAdd('ContractNumber');
                }
                if (this.trim(this.getControlValue('PremiseNumber'))) {
                    this.CBORequestAdd('PremiseNumber');
                }
                if (this.trim(this.getControlValue('ProductCode'))) {
                    this.CBORequestAdd('ProductCode');
                }
                if (this.trim(this.getControlValue('ServiceCoverNumber'))) {
                    this.CBORequestAdd('ServiceCoverNumber');
                }
                if (this.trim(this.getControlValue('ContactTypeCode'))) {
                    this.CBORequestAdd('ContactTypeCode');
                }
                if (this.trim(this.getControlValue('ContactTypeDetailCode'))) {
                    this.CBORequestAdd('ContactTypeDetailCode');
                }
                if (this.trim(this.getControlValue('OriginatingEmployeeCode'))) {
                    this.CBORequestAdd('OriginatingEmployeeCode');
                }
                if (this.trim(this.getControlValue('DefaultAssigneeEmployeeCode'))) {
                    this.CBORequestAdd('DefaultAssigneeEmployeeCode');
                }
                if (this.trim(this.getControlValue('NextActionEmployeeCode'))) {
                    this.CBORequestAdd('NextActionEmployeeCode');
                }
                if (this.trim(this.getControlValue('ContactPostcode'))) {
                    this.CBORequestAdd('ContactPostcode');
                }
                if (this.trim(this.getControlValue('PublicAddressLine4'))) {
                    this.CBORequestAdd('PublicAddressLine4');
                }
                if (this.trim(this.getControlValue('PublicAddressLine5'))) {
                    this.CBORequestAdd('PublicAddressLine5');
                }
                if (this.trim(this.getControlValue('NextActionByDate'))) {
                    this.cboPostObj['NextActionByDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('NextActionByDate')).toString();
                }
                if (this.trim(this.getControlValue('NextActionByTime'))) {
                    this.CBORequestAdd('NextActionByTime');
                }

                this.CBORequestExecute((data) => {
                    if (!data.hasError) {
                        this.setValueIntoFormControl(data);
                        this.uiForm.updateValueAndValidity();
                    }
                    // Relocate the below code block to execute it in sync mode
                    //' Based upon what has been run, perform any further setup/processing required
                    if (this.instr(strFunctionList, 'GetContactTypeInfo') >= 0) {
                        if (this.getControlValue('ContactTypeCallout')) {
                            this.fieldVisibility.tdCallOutLabel = true;
                            this.fieldVisibility.tdCallOut = true;
                            this.fieldVisibility.spanIntNotifications = false;
                            if (this.currentMode === MntConst.eModeAdd) {
                                this.setControlValue('chkContactPassedToActioner', false);
                            }

                            if (this.getControlValue('AllowCallout')) {
                                this.disableControl('chkCreateCallOut', false);
                                //this.disableControl('SMSMessage');
                            } else {
                                this.disableControl('chkCreateCallOut', true);
                            }

                        } else { //' Not this.Out
                            this.fieldVisibility.tdCallOutLabel = false;
                            this.fieldVisibility.tdCallOut = false;
                        }

                        if (this.getControlValue('ContactTypeClientRetention')) {
                            this.setRequiredStatus('ContactPosition', true);
                            this.fieldRequired['ContactPosition'] = true;
                            if (strAddMode === 'Yes') {
                                this.showHideClientRetention(false);
                            } else {
                                this.showHideClientRetention(true);
                            }

                            if (strAddMode === 'Yes' && this.pageParams.CRAutoCreateProspect) {
                                this.fieldVisibility.spnCreateProspect = true;
                                if (this.instr(this.getControlValue('ContactTypeDetailCRProspectList'), '^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
                                    this.setControlValue('chkCreateProspect', true);
                                } else {
                                    this.setControlValue('chkCreateProspect', false);
                                }
                            } else {
                                this.fieldVisibility.spnCreateProspect = false;
                                this.setControlValue('chkCreateProspect', false);
                            }
                        } else {
                            this.showHideClientRetention(true);
                            this.setRequiredStatus('ContactPosition', false);
                            this.fieldRequired['ContactPosition'] = false;
                        }

                        if (this.getControlValue('ServiceAreaCode')) {
                            this.showHideServiceAreaCode(true);
                        } else {
                            this.showHideServiceAreaCode(false);
                        }

                        if (this.getControlValue('SalesAreaCode')) {
                            this.showHideSalesAreaCode(true);
                        } else {
                            this.showHideSalesAreaCode(false);
                        }
                        //' Show/Hide Assign To options
                        this.showHideAssignToOptions(this.getControlValue('AlwaysShowAssignOptions'));

                        if ((this.getControlValue('ServiceBranchNumber') === '0') || (this.trim(this.getControlValue('ServiceBranchNumber')) === '')) {
                            this.setControlValue('ServiceBranchNumber', this.utils.getBranchCode());
                        }

                        this.setControlValue('OldContactTypeCode', '');
                        //'Build Up A List Of Valid Contact Type Details Codes
                        this.pageParams.cLastContactTypeDetailCodeList = this.getControlValue('ContactTypeDetailCodeList');
                        this.pageParams.cLastContactTypeDetailDescList = this.getControlValue('ContactTypeDetailDescList');
                        this.buildContactStatusList();
                        this.buildContactTypeDetailList();

                        if (this.parentMode === 'CallCentreSearchNew') {
                            this.setControlValue('ContactTypeDetailCode', this.riExchange.getParentHTMLValue('ContactTypeDetailCode'));
                            this.setControlValue('ContactTypeDetailCodeSelect', this.getControlValue('ContactTypeDetailCode'));
                        }

                        this.setControlValue('ContactTypeDetailCodeSelect', this.getControlValue('ContactTypeDetailCode'));
                        this.setMandatoryFields();
                        if (this.currentMode === MntConst.eModeUpdate) {
                            this.setControlValue('ContactStatusCodeSelect', this.getControlValue('ContactStatusCode'));
                        }
                    } //' GetContactTypeInfo

                    if (!this.getControlValue('CRContractTerm')) {
                        this.fieldVisibility.spnCRNoticePeriod = false;
                    } else {
                        this.fieldVisibility.spnCRNoticePeriod = true;
                    }

                    //' Based upon The AssignTo Employee ) { Show/Hide Notications Buttons i.e. can they be sms'd  || emailed? */
                    if (this.instr(strFunctionList, 'GetNotificationDetails') >= 0 || this.instr(strFunctionList, 'GetAssignToEmployeeCode') >= 0 || this.instr(strFunctionList, 'GetOrigAndAssignToEmployeeCode') >= 0) {
                        if (!this.getControlValue('CanSendIntNotify')) {
                            this.fieldVisibility.spanIntNotifications = false;
                        } else {
                            this.fieldVisibility.spanIntNotifications = true;
                        }
                    }
                    if (this.instr(strFunctionList, 'GetNotificationDetails') >= 0) {
                        this.showHideNotifyOptions();
                    }

                    if (this.instr(strFunctionList, 'GetAvailability') >= 0) {
                        if (this.getControlValue('AvailabilityWarningMessage')) {
                            description = this.getControlValue('AvailabilityWarningMessage') || '';
                            modalVo = new ICabsModalVO(description);
                            modalVo.title = 'Information';
                            this.modalAdvService.emitMessage(modalVo);
                        }
                    }
                    if (this.instr(strFunctionList, 'CheckContractTerm') >= 0) {
                        if (this.getControlValue('TerminationWarningMessage')) {
                            description = this.getControlValue('TerminationWarningMessage') || '';
                            modalVo = new ICabsModalVO(description);
                            modalVo.title = 'Information';
                            this.modalAdvService.emitMessage(modalVo);
                        }
                    }
                    if (this.instr(strFunctionList, 'CheckEmployeeLeft') >= 0) {
                        if (this.getControlValue('EmployeeLeftWarningMessage')) {
                            description = this.getControlValue('EmployeeLeftWarningMessage') || '';
                            modalVo = new ICabsModalVO(description);
                            modalVo.title = 'Information';
                            this.modalAdvService.emitMessage(modalVo);
                        }
                    }

                    this.CBORequestClear();
                    this.customBusinessObjectAdditionalPostData = '';
                    this.setScreenTo('update');
                    this.setControlValue('OldContactTypeCode', this.getControlValue('ContactTypeCode'));
                    retObj.next(true);
                });
        }
        else {
            retObj.next(true);
        }
        return retObj;
    }

    private showHideNotifyOptions(): any {
        let codeList: string = '';
        let descList: string = '';
        let whenDefault: string = '';
        let howDefault: string = '';

        if (this.getControlValue('InternalSMSInd') === true && this.getControlValue('ContactTypeCallout') === false) {
            this.fieldVisibility.spanSMSSendOn = true;
        } else {
            this.fieldVisibility.spanSMSSendOn = false;
        }

        if (!this.getControlValue('ExternalCREmailInd') && !this.getControlValue('ExternalCRSMSInd') && !this.getControlValue('ExternalCLEmailInd') && !this.getControlValue('ExternalCLSMSInd')) {
            this.fieldVisibility.trNotifications = false;
        } else {
            this.fieldVisibility.trNotifications = true;
            whenDefault = '';
            //' Force the user to select a value
            if (this.currentMode === MntConst.eModeAdd) {
                whenDefault = '-1';
                codeList = '-1^';
                descList = '< please select >' + '^';
            }

            codeList = codeList + '0';
            descList = descList + 'Never';

            if (this.getControlValue('ExternalCREmailInd') || this.getControlValue('ExternalCRSMSInd')) {
                codeList = codeList + '^1';
                descList = descList + '^' + 'On Create';
            }

            if (this.getControlValue('ExternalCLEmailInd') || this.getControlValue('ExternalCLSMSInd')) {
                codeList = codeList + '^2';
                descList = descList + '^' + 'On Close';
                if (!whenDefault) {
                    whenDefault = '2';
                }
            }

            if ((this.getControlValue('ExternalCREmailInd') || this.getControlValue('ExternalCRSMSInd')) && (this.getControlValue('ExternalCLEmailInd') || this.getControlValue('ExternalCLSMSInd'))) {
                codeList = codeList + '^3';
                descList = descList + '^' + 'On Create & Close';
                //' If default is t)o allow both then default to this option
                if (!whenDefault) {
                    whenDefault = '3';
                }
            }
            this.contactNotifyWhenSelectList = [];
            //this.setControlValue('ContactNotifyWhenSelect', '');
            let valArray = codeList.split('^');
            let descArray = descList.split('^');
            for (let i = 0; i < valArray.length; i++) {
                this.contactNotifyWhenSelectList.push({ value: valArray[i], text: descArray[i] });
            }

            if (this.currentMode === MntConst.eModeAdd) {
                this.setControlValue('ContactNotifyWhenSelect', whenDefault);
            } else {
                this.setControlValue('ContactNotifyWhenSelect', this.getControlValue('ContactNotifyWhen'));
            }

            codeList = '';
            descList = '';

            if (this.getControlValue('ExternalCREmailInd') || this.getControlValue('ExternalCLEmailInd')) {
                codeList = '1';
                descList = 'Using Email';
                howDefault = '1';
            }

            if (this.getControlValue('ExternalCRSMSInd') || this.getControlValue('ExternalCLSMSInd')) {
                if (!codeList) {
                    codeList = '2';
                    descList = 'Using SMS';
                } else {
                    codeList = codeList + '^2';
                    descList = descList + '^' + 'Using SMS';
                }
                if (!howDefault) {
                    howDefault = '2';
                }
            }
            this.contactNotifyTypeSelectList = [];
            //this.setControlValue('ContactNotifyTypeSelect', '');
            valArray = codeList.split('^');
            descArray = descList.split('^');
            for (let i = 0; i < valArray.length; i++) {
                this.contactNotifyTypeSelectList.push({ value: valArray[i], text: descArray[i] });
            }

            if (this.currentMode === MntConst.eModeAdd) {
                this.setControlValue('ContactNotifyTypeSelect', howDefault);
            } else {
                this.setControlValue('ContactNotifyTypeSelect', this.getControlValue('ContactNotifyType'));
            }

            this.contactNotifyWhenSelectOnChange();
            this.contactNotifyTypeSelectOnChange();
        }
    }

    private warningMessage(): Observable<boolean> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        let blnReturn = false;

        if (!this.getControlValue('AccountNumber') && !this.getControlValue('ContractNumber') && !this.getControlValue('PremiseNumber')) {
            let promptVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.customerContactMaintenance.Have_You_updated_the_Prospect_Details, null, function yesSuspension(): any {
                blnReturn = true;
                retObj.next(blnReturn);
            }.bind(this), function cancelSuspension(): any {
                blnReturn = false;
                retObj.next(blnReturn);
            }.bind(this));
            promptVO.title = MessageConstant.PageSpecificMessage.customerContactMaintenance.Prospect_Details;
            promptVO.cancelLabel = 'No';
            promptVO.confirmLabel = 'Yes';
            this.modalAdvService.emitPrompt(promptVO);
        } else {
            blnReturn = true;
            retObj.next(blnReturn);
        }
        return retObj;
    }

    private showHideServiceAreaCode(blnShow: any): any {
        if (blnShow) {
            this.fieldVisibility.spanServiceArea = true; //'Label
            this.fieldVisibility.ServiceAreaCode = true; //'Input box
        } else {
            this.fieldVisibility.spanServiceArea = false;
            this.fieldVisibility.ServiceAreaCode = false;
        }
    }

    private showHideSalesAreaCode(blnShow: any): any {
        if (blnShow) {
            this.fieldVisibility.spanSalesArea = true; //'Label
            this.fieldVisibility.SalesAreaCode = true; // 'Input box
        } else {
            this.fieldVisibility.spanSalesArea = false;
            this.fieldVisibility.SalesAreaCode = false;
        }
    }

    //' Get ServiceCoverNumber using product code
    private getServiceCoverNumberFromRecord(): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        this.CBORequestClear();
        this.customBusinessObjectAdditionalPostData = { Function: 'GetServiceCoverFromRecord' };
        this.CBORequestAddCS('BusinessCode');
        this.CBORequestAdd('ContractNumber');
        this.CBORequestAdd('PremiseNumber');
        this.CBORequestAdd('ProductCode');

        this.CBORequestExecute((data) => {
            if (!data.hasError) {
                this.setValueIntoFormControl(data);
                this.processServiceCoverData();
            }
            retObj.next(true);
        });

        this.CBORequestClear();
        this.customBusinessObjectAdditionalPostData = {};
        return retObj;
    }

    private processServiceCoverData(): void {
        switch (this.getControlValue('ServiceCoverNumber')) {
            case '-1':
                this.productCode_search();
                //' Need to call ProcessDoEvents to get search screen to appear
                this.getServiceCoverNumberFromRowID();
                break;
            case '0':
                this.setErrorStatus('ProductCode', true);
                this.setFocusToControl('ProductCode');
                break;
            default:
                this.setAllSCRowID(this.getControlValue('ServiceCoverRowID'));
                this.showHideServiceType(false);
        }
    }

    private getShortTime(): any {
        let today = new Date();
        let hour = today.getHours();
        let minute = today.getMinutes();
        return hour + ':' + minute;
    }

    private checkFormDirty(): boolean {
        if (this.storeData && this.uiForm.dirty) {
            return true;
        }
        return false;
    }

    private focusToTab(tabId: string = 'grdLatest'): any {
        let element = document.querySelector('.nav-tabs li#' + tabId + ' a');
        if (element) {
            let click = new CustomEvent('click', { bubbles: true });
            this.renderer.invokeElementMethod(element, 'dispatchEvent', [click]);
        }
    }

    private riMaintenanceAddTableField(controlName: string, fieldType: any, option: string, commonValidator: boolean = false): any {
        if (this.uiForm.controls.hasOwnProperty(controlName)) {
            let validatorArr = [];
            this.fieldRequired[controlName] = false;
            if (commonValidator) {
                validatorArr.push(this.utils.commonValidate);
            }

            switch (option) {
                case 'Required':
                    validatorArr.push(this.nullValidate);
                    this.fieldRequired[controlName] = true;
                    break;
                case 'Optional':
                case 'Lookup':
                case 'Normal':
                    this.setRequiredStatus(controlName, false);
                    this.fieldRequired[controlName] = false;
                    break;
                case 'ReadOnly':
                    this.disableControl(controlName, true);
                    break;
            }

            if (validatorArr.length > 0) {
                this.uiForm.controls[controlName].setValidators(Validators.compose(validatorArr));
            }
            else {
                this.uiForm.controls[controlName].clearValidators();
            }
            this.uiForm.controls[controlName].updateValueAndValidity();
        }
    }

    private processForm(): void {
        this.disableControl('CustomerContactNumber', true);
        this.disableControl('AccountName', true);
        this.disableControl('ContractName', true);
        this.disableControl('PremiseName', true);
        this.disableControl('ProductDesc', true);
        this.disableControl('CurrentOwnerEmployeeName', true);
        this.disableControl('CreatedByEmployeeName', true);
        this.disableControl('NextActionEmployeeName', true);
        this.disableControl('OriginatingEmployeeName', true);
        this.disableControl('ServiceTypeCode', true);
        this.disableControl('ServiceTypeDesc', true);
    }

    private fetchRecordData(functionName: any, params: any, postdata?: any): any {
        let queryParams: QueryParams = this.getURLSearchParamObject();
        queryParams.set(this.serviceConstants.Action, '0');

        if (functionName !== '') {
            queryParams.set(this.serviceConstants.Action, '6');
        }
        for (let key in params) {
            if (params.hasOwnProperty(key)) {
                queryParams.set(key, params[key]);
            }
        }

        if (postdata) {
            return this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryParams, postdata);
        }
        else {
            return this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryParams);
        }
    }

    private executeRequest(obj: any, fname: any = '', params: any = {}, postData: any = {}, callbackResponse: any): any {
        this.ajaxSource.next(this.ajaxconstant.START);
        let fetchRecordSubs: Subscription = this.fetchRecordData(fname, params, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                return callbackResponse(data);
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
        this.subscriptionManager.add(fetchRecordSubs);
    }

    private onUpdateMode(data: any): any {
        this.saveCancelDisable = false;
        if (!this.isNavReturnStatus)
            this.setFormData(data);
        this.loadFieldDescription();
        let riFetchSubs: Subscription = this.riMaintenanceAfterFetch().subscribe((e1) => {
            this.cloneFormData();
            // this is to detect page initialization status
            this.isInitialising = false;
            if (!this.callOutBackNavStatus())
                this.eventCategoryUpdateMode();
            else {
                this.disableFormControls();
            }
            this.buildMenus();
        });
        this.subscriptionManager.add(riFetchSubs);
    }

    private callOutBackNavStatus(): boolean {
        //if retuns from call out maintenance screen then disable all form controls.
        return (this.isNavReturnStatus && this.previousRouteUrl && this.instr(this.previousRouteUrl.toString().toLowerCase(), InternalMaintenanceServiceModuleRoutes.ICABSCMCALLOUTMAINTENANCE.toLowerCase()) >= 0) ? true : false;
    }

    private reloadPageWithBackData(): void {
        if (this.isNavReturnStatus && this.oldFormData) {
            if ((this.oldFormData['WindowClosingName'] === 'Campaign')) {
                let comment: string = this.oldFormData['Comments'] || '';
                comment = comment.split('[CHR10]').join('\n');
                this.setControlValue('Comments', comment);
            }

            if (this.previousRouteUrl && this.instr(this.previousRouteUrl.toString().toLowerCase(), InternalMaintenanceServiceModuleRoutes.ICABSCMCUSTOMERCONTACTMAINTENANCECOPY.toLowerCase()) >= 0) {
                this.setControlValue('CopyMode', this.oldFormData['CopyMode']);
                this.cloneFormData();
                if (this.getControlValue('CopyMode') === 'Y') {
                    this.cloneFormValues = {};
                    this.setControlValue('CopyContactTypeCode', this.oldFormData['CopyContactTypeCode']);
                    this.setControlValue('CopyContactTypeDetailCode', this.oldFormData['CopyContactTypeDetailCode']);
                    this.setControlValue('CopyDisputed', this.oldFormData['CopyDisputed']);
                    this.setControlValue('CopyRootCause', this.oldFormData['CopyRootCause']);
                    this.cloneFormValues = {};
                    this.cmdCopyTicketOnClick(true);
                    this.uiForm.markAsPristine();
                    this.cloneFormData();
                }
                else
                    this.cmdCopyTicketOnClick(true);
            }
            else if (this.previousRouteUrl && this.instr(this.previousRouteUrl.toString().toLowerCase(), InternalGridSearchApplicationModuleRoutes.ICABSCMCUSTOMERCONTACTNOTIFICATIONSGRID.toLowerCase()) >= 0) {
                this.setControlValue('IntNotifyType', this.oldFormData['IntNotifyType']);
                this.setControlValue('SaveIntNotifySubjectText', this.oldFormData['SaveIntNotifySubjectText']);
                this.setControlValue('SaveIntNotifyBodyText', this.oldFormData['SaveIntNotifyBodyText']);
            }
        }
        //capture data from contact person maintenance screen
        if (this.isNavReturnStatus && localStorage.getItem('contactInfo')) {
            let contactInfo: any = JSON.parse(localStorage.getItem('contactInfo'));
            if (contactInfo) {
                if (contactInfo.Mode === 'CCMEntry') {
                    this.setControlValue('ContactName', contactInfo.ContactName);
                    this.setControlValue('ContactPosition', contactInfo.ContactPosition);
                    this.setControlValue('ContactTelephone', contactInfo.ContactTelephone);
                    this.setControlValue('ContactMobileNumber', contactInfo.ContactMobileNumber);
                    this.setControlValue('ContactEmailAddress', contactInfo.ContactEmailAddress);
                }
                localStorage.removeItem('contactInfo');
            }
        }
    }

    private captureOldFormData(): void {
        this.oldFormData = {};
        if (this.formData['WindowClosingName']) {
            this.oldFormData['WindowClosingName'] = this.formData['WindowClosingName'];
            this.oldFormData['Comments'] = this.formData['Comments'];
        }

        if (this.previousRouteUrl && this.instr(this.previousRouteUrl.toString().toLowerCase(), InternalMaintenanceServiceModuleRoutes.ICABSCMCUSTOMERCONTACTMAINTENANCECOPY.toLowerCase()) >= 0) {
            this.oldFormData['CopyMode'] = this.formData['CopyMode'];
            this.oldFormData['CopyContactTypeCode'] = this.formData['CopyContactTypeCode'];
            this.oldFormData['CopyContactTypeDetailCode'] = this.formData['CopyContactTypeDetailCode'];
            this.oldFormData['CopyDisputed'] = this.formData['CopyDisputed'];
            this.oldFormData['CopyRootCause'] = this.formData['CopyRootCause'];
        }
        else if (this.previousRouteUrl && this.instr(this.previousRouteUrl.toString().toLowerCase(), InternalGridSearchApplicationModuleRoutes.ICABSCMCUSTOMERCONTACTNOTIFICATIONSGRID.toLowerCase()) >= 0) {
            this.oldFormData['IntNotifyType'] = this.formData['IntNotifyType'];
            this.oldFormData['SaveIntNotifySubjectText'] = this.formData['IntNotifySubjectText'];
            this.oldFormData['SaveIntNotifyBodyText'] = this.formData['IntNotifyBodyText'];
        }
    }

    private eventCategoryUpdateMode(): any {
        let riBeforeUpdate = this.riMaintenanceBeforeUpdate().subscribe(
            (e) => {
                if (!this.cancelEvent && e) {
                    setTimeout(() => {
                        this.riExchange_CBORequest();
                        this.riMaintenanceBeforeUpdateMode();
                        this.buildMenus();
                        this.reloadPageWithBackData();
                        this.updateEllipsisConfigValue();
                        this.formPristine();
                        this.clearErrorTabs();
                        this.uiForm.markAsPristine();
                        this.cloneFormData();
                        if (this.getControlValue('CallOutRowID')) {
                            this.disableFormControls();
                        }
                    }, 100);
                }
                else {
                    this.disableFormControls();
                }
            });
        this.subscriptionManager.add(riBeforeUpdate);
    }

    private eventCategoryAddMode(): any {
        this.disableControl('menu', true);
        this.cloneFormData();
        setTimeout(() => {
            this.updateEllipsisConfigValue();
            this.reloadPageWithBackData();
            this.formPristine();
            this.clearErrorTabs();
            // this is to detect page initialization status
            this.isInitialising = false;
            this.cloneFormData();
        }, 200);
    }

    private clearErrorTabs(): void {
        let tablist: any = document.querySelector('.nav-tabs');
        if (tablist) {
            let elem: any = tablist.children;
            for (let i = 0; i < elem.length; i++) {
                if (this.utils.hasClass(elem[i], 'error')) {
                    this.utils.removeClass(elem[i], 'error');
                }
            }
        }
    }

    private setFocusToControl(elementId: string): void {
        let elm = document.getElementById(elementId);
        if (elm) {
            let focus = new CustomEvent('focus', { bubbles: true });
            setTimeout(() => {
                this.renderer.invokeElementMethod(elm, 'focus', [focus]);
            }, 0);
        }
    }

    private setValueIntoFormControl(data: any): void {
        if (data) {
            for (let controlName in data) {
                if (data.hasOwnProperty(controlName) && this.uiForm.controls.hasOwnProperty(controlName)) {
                    let type: string = this.controlDataTypes[controlName] || '';
                    let val: string = (this.utils.customTruthyCheck(data[controlName]) === true) ? data[controlName].trim() : data[controlName];
                    this.setControlValue(controlName, this.setFieldValue(val, (type === MntConst.eTypeCheckBox) ? true : false));
                }
            }
        }
    }

    public onSaveFocus(e: any): void {
        let nextTab: number = 0;
        let code = (e.keyCode ? e.keyCode : e.which);
        let elemList = document.querySelectorAll('.screen-body .nav-tabs li a');
        let currentSelectedIndex = Array.prototype.indexOf.call(elemList, document.querySelector('.screen-body .nav-tabs li a.active'));
        if (code === 9 && currentSelectedIndex < (elemList.length - 1)) {
            let click = new CustomEvent('click', {});
            nextTab = currentSelectedIndex + 1;
            if (elemList[nextTab])
                elemList[nextTab]['click']();
        }
    }

    private setFocusToTabElement(idx: number): any {
        setTimeout(() => {
            const nextTab = idx;
            const tabItemList = document.querySelectorAll('.screen-body .tab-content .tab-pane:nth-child(' + nextTab + ') .ui-select-toggle, .screen-body .tab-content .tab-pane:nth-child(' + nextTab + ') input:not([disabled]), .screen-body .tab-content .tab-pane:nth-child(' + nextTab + ') select:not([disabled])');

            for (let l = 0; l < tabItemList.length; l++) {
                const el = tabItemList[l];
                if (el) {
                    if ((el.getAttribute('type') === undefined) || (el.getAttribute('type') === null) || (el.getAttribute('type') && el.getAttribute('type').toLowerCase() !== 'hidden' && el.getAttribute('type').toLowerCase() !== 'button')) {
                        setTimeout(() => {
                            el['focus']();
                        }, 90);
                        break;
                    }
                }
            }
        }, 100);
    }

    private setFieldValue(controlObj: any, isCheckBox?: boolean): any {
        if (isCheckBox === true) {
            return (controlObj) ? ((controlObj.toUpperCase() === 'YES' || controlObj.toUpperCase() === 'Y' || controlObj.toUpperCase() === 'TRUE') ? true : false) : false;
        }
        return (controlObj !== undefined && controlObj !== null) ? controlObj : '';
    }

    private getFieldValue(val: any, isCheckBox?: boolean): any {
        if (isCheckBox === true) {
            return (val) ? ((val === true) ? 'yes' : 'no') : 'no';
        }
        return (val !== undefined && val !== null) ? val : '';
    }

    private isCheckBoxChecked(arg: any): boolean {
        if (typeof arg === 'boolean' || (typeof arg === 'object' && typeof arg.valueOf() === 'boolean'))
            return arg;
        return (arg) ? (((arg.toLowerCase() === 'false') || (arg.toLowerCase() === 'f') || (arg.toLowerCase() === 'no')) ? false : true) : false;
    }

    private sortByKey(array: any, key: any, reverse: boolean = false): any {
        return array.sort(function (a: any, b: any): any {
            let x = a[key]; let y = b[key];
            return reverse ? ((y < x) ? -1 : ((y > x) ? 1 : 0)) : ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }

    private instr(str: any, substr: any, position: number = 0): any {
        return str.indexOf(substr, position);
    }

    private trim(val: any): any {
        return (val !== undefined && val !== null) ? val.toString().trim() : '';
    }

    private replace(val: any, from: string, to: string): any {
        if (this.utils.customTruthyCheck(val)) {
            return val.toString().replace(new RegExp(from, 'g'), to);
        }
        return val;
    }

    public onEmployeeSearchDataReceived(data: any, name: string): void {
        if (name === 'OriginatingEmployeeCode') {
            this.setControlValue('OriginatingEmployeeCode', data.OriginatingEmployeeCode);
            this.setControlValue('OriginatingEmployeeName', data.OriginatingEmployeeName);
        }
        else if (name === 'NextActionEmployeeCode') {
            this.setControlValue('NextActionEmployeeCode', data.NextActionEmployeeCode);
            this.setControlValue('NextActionEmployeeName', data.NextActionEmployeeName);
            this.nextActionEmployeeCodeOnChange();
        }
        this.uiForm.controls['NextActionEmployeeCode'].markAsDirty();
    }

    public cmdContactExportOnClick(): any {
        let gridRequest: any = {};
        let search: QueryParams = new QueryParams();

        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set(this.serviceConstants.CountryCode, this.countryCode());
        search.set(this.serviceConstants.Action, '2');
        search.set('CustomerContactNumber', this.getControlValue('CustomerContactNumber'));
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.PageSize, '2000');
        search.set(this.serviceConstants.PageCurrent, '1');

        gridRequest['method'] = 'ccm/maintenance';
        gridRequest['module'] = 'tickets';
        gridRequest['operation'] = 'ContactManagement/iCABSCMCustomerContactMaintenance';
        gridRequest['http_method'] = 'GET';
        gridRequest['search'] = search;

        gridRequest[ExportConfig.c_s_OPTIONS_KEY] = {
            dateCell: '31:1',
            datePattern: ExportConfig.c_s_DATE_FORMAT_1
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeExportRequest(gridRequest).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data && data.hasOwnProperty('SpreadsheetURL')) {
                window.open(data['SpreadsheetURL']);
            } else {
                this.globalNotifications.message = {
                    msg: MessageConstant.Message.GeneralError,
                    timestamp: (new Date()).getMilliseconds()
                };
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.logger.log(error);
            this.globalNotifications.message = {
                msg: MessageConstant.Message.GeneralError,
                timestamp: (new Date()).getMilliseconds()
            };
        });
    }

    public cmdDisputedInvoicesOnClick(): any {
        this.setControlValue('DisputedInvoiceCacheName', this.utils.getBusinessCode() +
            'A' + this.utils.getBranchCode() +
            'A' + this.getShortTime() +
            'A' + this.getControlValue('CustomerContactNumber') +
            'A' + this.getControlValue('AccountNumber') +
            'A' + this.getControlValue('ContractNumber') +
            'A' + this.getControlValue('PremiseNumber'));

        //navigate to screen  Application/iCABSAContractInvoiceGrid.htm' Mode = 'TicketMaintenance'
        /* let pageParams: Object = {
            ContractNumber: this.getControlValue('ContractNumber'),
            ContractName: this.getControlValue('ContractName'),
            PremiseNumber: this.getControlValue('PremiseNumber'),
            PremiseName: this.getControlValue('PremiseName'),
            AccountNumber: this.getControlValue('AccountNumber'),
            AccountName: this.getControlValue('AccountContactName'),
            customerContactNumber: this.getControlValue('CustomerContactNumber'),
            disputedInvoiceCacheName: this.getControlValue('DisputedInvoiceCacheName'),
            parentMode: 'TicketMaintenance',
            isReturnFlag: true
        }; */
        this.setNavReturnValues();

        let url: string = '#' + BillToCashModuleRoutes.ICABSACONTRACTINVOICEGRID;

        url += '?parentMode=TicketMaintenance';
        url += '&ContractNumber=' + this.getControlValue('ContractNumber');
        url += '&ContractName=' + this.getControlValue('ContractName');
        url += '&PremiseNumber=' + this.getControlValue('PremiseNumber');
        url += '&PremiseName=' + this.getControlValue('PremiseName');
        url += '&AccountNumber=' + this.getControlValue('AccountNumber');
        url += '&AccountName=' + this.getControlValue('AccountContactName');
        url += '&customerContactNumber=' + this.getControlValue('CustomerContactNumber');
        url += '&disputedInvoiceCacheName=' + this.getControlValue('DisputedInvoiceCacheName');
        url += '&isReturnFlag=' + true;
        url += '&' + CBBConstants.c_s_URL_PARAM_COUNTRYCODE + '=' + this.utils.getCountryCode();
        url += '&' + CBBConstants.c_s_URL_PARAM_BUSINESSCODE + '=' + this.utils.getBusinessCode();
        url += '&' + CBBConstants.c_s_URL_PARAM_BRANCHCODE + '=' + this.utils.getBranchCode();

        window.open(url,
            '_blank',
            'fullscreen=yes,menubar=no,resizable=no,status=no,titlebar=no,toolbar=no');
        return;
    }

    public btnAmendContactOnClick(): any {
        this.cmdContactDetails('Button');
    }

    public cmdDisputedInvoiceOnClick(): any {
        // navigate to screen Application/iCABSAContractInvoiceGrid.htm' Mode = 'TicketMaintenance'
        let pageParams: Object = {
            ContractNumber: this.getControlValue('ContractNumber'),
            ContractName: this.getControlValue('ContractName'),
            PremiseNumber: this.getControlValue('PremiseNumber'),
            PremiseName: this.getControlValue('PremiseName'),
            AccountNumber: this.getControlValue('AccountNumber'),
            AccountName: this.getControlValue('AccountContactName'),
            customerContactNumber: this.getControlValue('CustomerContactNumber'),
            disputedInvoiceCacheName: this.getControlValue('DisputedInvoiceCacheName'),
            parentMode: 'TicketMaintenance',
            isReturnFlag: true
        };
        this.setNavReturnValues();
        this.router.navigate([BillToCashModuleRoutes.ICABSACONTRACTINVOICEGRID], { queryParams: pageParams });
    }

    public promptConfirm(): any {
        if (!this.cancelEvent) {
            if (this.currentMode === MntConst.eModeUpdate) {
                this.updateCustomerContactMaintenanceData();
            }
            else if (this.currentMode === MntConst.eModeAdd) {
                this.addCustomerContactMaintenanceData();
            }
        }
    }

    public promptCancel(): any {
        //TODO:
    }

    public onSubmit(formdata: any, valid: any, event: any): void {
        this.routeAwayGlobals.setSaveEnabledFlag(false);
        event.preventDefault();
        //this.pageParams.addData = '';
        this.cancelEvent = false;
        if (this.riExchange.validateForm(this.uiForm)) {
            //' When Callout then don't update this record but take them to the CallOut window
            //As per Allec's comment this CallOut redirection section has been added here
            if (this.getControlValue('CallOutRowID')) {
                //Navigate to page ContactManagement/iCABSCMCallOutMaintenance.htm Mode = 'UpdateCallOut-ContactSearch'
                this.isSaved = true;
                this.cancelEvent = true;
                this.pageParams.previousRouteUrl = InternalMaintenanceServiceModuleRoutes.ICABSCMCALLOUTMAINTENANCE;
                if (this.storeData) {
                    this.pageParams.CustomerContact = this.storeData['CustomerContact'] || '';
                    this.pageParams.ContactAction = this.storeData['ContactAction'] || '';
                    this.pageParams.CustomerContactNumber = this.storeData['CustomerContactNumber'] || '';
                }
                this.formPristine();
                this.navigate('UpdateCallOut-ContactSearch', InternalMaintenanceServiceModuleRoutes.ICABSCMCALLOUTMAINTENANCE, {
                    CallOutROWID: this.getControlValue('CallOutRowID')
                });
            }
            else {
                this.riMaintenanceBeforeSave();
                let confirmSubs: Subscription = this.riMaintenanceBeforeConfirm().subscribe((c) => {
                    if (c !== false) {
                        setTimeout(() => {
                            let promptVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.promptConfirm.bind(this), this.promptCancel.bind(this));
                            promptVO.cancelLabel = 'Cancel';
                            promptVO.confirmLabel = 'Confirm';
                            this.modalAdvService.emitPrompt(promptVO);
                        }, 0);
                    }
                });
                this.subscriptionManager.add(confirmSubs);
            }
        }
        else {
            this.utils.makeTabsRed();
        }
    }

    public onCancel(): void {
        event.preventDefault();
        if (this.cloneFormValues) {
            this.zone.run(() => {
                if (this.cloneFormValues['contactTypeDetailCodeSelectList']) this.contactTypeDetailCodeSelectList = this.cloneFormValues['contactTypeDetailCodeSelectList'];
                if (this.cloneFormValues['buttonControls']) this.buttonControls = JSON.parse(this.cloneFormValues['buttonControls']);
                if (this.cloneFormValues['fieldVisibility']) this.fieldVisibility = JSON.parse(this.cloneFormValues['fieldVisibility']);
                if (this.cloneFormValues['fieldRequired']) this.fieldRequired = JSON.parse(this.cloneFormValues['fieldRequired']);
                if (this.cloneFormValues['FormValues']) this.uiForm.reset(this.cloneFormValues['FormValues']);
            });
        }
        if (this.currentMode === MntConst.eModeAdd) {
            //TODO: Added this code block for future purpose only
            //this.riMaintenanceAfterAbandon();
        }
        this.isNavReturnStatus = false;
        this.reloadPageInUpdateMode();
        this.uiForm.markAsPristine();
        this.pageParams.isFormDirty = this.uiForm.dirty;
        this.uiForm.updateValueAndValidity();
    }

    public getCurrentPage(data: any, gridName: string): void {
        switch (gridName) {
            case 'riGridTickets':
                this.gridConfig.riGridTickets.gridCurPage = data.value;
                this.riGridTickets.RefreshRequired();
                this.riGridTicketsBeforeExecute();
                break;
            case 'riGridRootCause':
                this.gridConfig.riGridRootCause.gridCurPage = data.value;
                this.riGridRootCause.RefreshRequired();
                this.riGridRootCauseBeforeExecute();
                break;
            case 'riGridWorkOrder':
                this.gridConfig.riGridWorkOrder.gridCurPage = data.value;
                this.riGridWorkOrder.RefreshRequired();
                this.riGridWorkOrderBeforeExecute();
                break;
            case 'riGridDisputedInvoices':
                this.gridConfig.riGridDisputedInvoices.gridCurPage = data.value;
                this.riGridDisputedInvoices.RefreshRequired();
                this.riGridDisputedInvoicesBeforeExecute();
                break;
            case 'riGridNotify':
                this.gridConfig.riGridNotify.gridCurPage = data.value;
                this.riGridNotify.RefreshRequired();
                this.riGridNotifyBeforeExecute();
                break;
            case 'riGridLogs':
                this.gridConfig.riGridLogs.gridCurPage = data.value;
                this.riGridLogs.RefreshRequired();
                this.riGridLogsBeforeExecute();
                break;
        }
    }

    public refresh(gridName: string): void {
        switch (gridName) {
            case 'riGridTickets':
                this.riGridTickets.UpdateHeader = false;
                this.riGridTickets.UpdateRow = true;
                this.riGridTickets.UpdateFooter = false;
                this.riGridTickets.RefreshRequired();
                this.riGridTicketsBeforeExecute();
                break;
            case 'riGridRootCause':
                this.riGridRootCause.UpdateHeader = false;
                this.riGridRootCause.UpdateRow = true;
                this.riGridRootCause.UpdateFooter = false;
                this.riGridRootCause.RefreshRequired();
                this.riGridRootCauseBeforeExecute();
                break;
            case 'riGridWorkOrder':
                this.riGridWorkOrder.UpdateHeader = false;
                this.riGridWorkOrder.UpdateRow = true;
                this.riGridWorkOrder.UpdateFooter = false;
                this.riGridWorkOrder.RefreshRequired();
                this.riGridWorkOrderBeforeExecute();
                break;
            case 'riGridDisputedInvoices':
                this.riGridDisputedInvoices.UpdateHeader = false;
                this.riGridDisputedInvoices.UpdateRow = true;
                this.riGridDisputedInvoices.UpdateFooter = false;
                this.riGridDisputedInvoices.RefreshRequired();
                this.riGridDisputedInvoicesBeforeExecute();
                break;
            case 'riGridNotify':
                this.buildNotifyGrid();
                this.riGridNotify.RefreshRequired();
                this.riGridNotifyBeforeExecute();
                break;
            case 'riGridLogs':
                this.setupLogGrid();
                this.riGridLogs.RefreshRequired();
                this.riGridLogsBeforeExecute();
                break;
        }
    }

    public onTabClick(tabName: string, idx: number): void {
        this.currentTabID = tabName;
        this.currentTabIdx = idx;
        this.riTabFocusAfter();
        this.setFocusToTabElement(idx);
        if (this.isMobile) {
            this.adjustActiveTabScroll();
        }
    }

    private adjustActiveTabScroll(): void {
        const sdTabWrapper: any = document.getElementById('sd-tab-wrapper');
        const stTabWrapperWidth: number = sdTabWrapper.clientWidth;
        const midPoint: number = stTabWrapperWidth / 2;
        const ul: any = sdTabWrapper.getElementsByClassName('nav-tabs')[0];
        const activeTab: any = ul.getElementsByTagName('li')[this.currentTabIdx - 1];
        const activeTabWidth: number = activeTab.clientWidth;
        const activeTabCoord: any = activeTab.getBoundingClientRect();

        setTimeout(() => {
            if (activeTabCoord > midPoint) {
                // on right side
                const distanceToScroll: number = (activeTabCoord.left + (activeTabWidth / 2)) - midPoint;
                sdTabWrapper.scrollLeft += distanceToScroll;

            } else {
                //on left side
                const distanceToScroll: number = midPoint - (activeTabCoord.left + (activeTabWidth / 2));
                sdTabWrapper.scrollLeft -= distanceToScroll;
            }
        }, 500);
    }

    public gridSort(event: any, gridName?: string): void {
        switch (gridName) {
            case 'riGridTickets':
                this.riGridTickets.RefreshRequired();
                this.riGridTicketsBeforeExecute();
                break;
            case 'riGridRootCause':
                this.riGridRootCause.RefreshRequired();
                this.riGridRootCauseBeforeExecute();
                break;
            case 'riGridWorkOrder':
                this.riGridWorkOrder.RefreshRequired();
                this.riGridWorkOrderBeforeExecute();
                break;
            case 'riGridDisputedInvoices':
                this.riGridDisputedInvoices.RefreshRequired();
                this.riGridDisputedInvoicesBeforeExecute();
                break;
            case 'riGridNotify':
                this.riGridNotify.RefreshRequired();
                this.riGridNotifyBeforeExecute();
                break;
            case 'riGridLogs':
                this.riGridLogs.RefreshRequired();
                this.riGridLogsBeforeExecute();
                break;
        }
    }

    public notifyStyleOnChange(): any {
        this.riGridNotify.RefreshRequired();
    }

    public notifyDestinationOnChange(): any {
        this.riGridNotify.RefreshRequired();
    }

    public riGridLogsAfterExecute(): any {
        //' Default the description of the first log on refresh - Please Note: There will ALWAYS be a record otherwise this //tab will not be shown!
        if (this.riGridLogs.HTMLGridBody && this.riGridLogs.HTMLGridBody.children[0]) {
            if (this.riGridLogs.HTMLGridBody.children[0].children[0]) {
                this.riGridLogs.SetDefaultFocus();
                this.selectedRowFocusLogs(this.riGridLogs.HTMLGridBody.children[0].children[0]);
            }
        }
    }

    public riGridTicketsAfterExecute(): any {
        //' Default the description of the first log on refresh - Please Note: There will ALWAYS be a record otherwise this //tab will not be shown!
        if (this.riGridTickets.HTMLGridBody && this.riGridTickets.HTMLGridBody.children[0]) {
            if (this.riGridTickets.HTMLGridBody.children[0].children[0]) {
                this.riGridTickets.SetDefaultFocus();
                this.selectedRowFocusTickets(this.riGridTickets.HTMLGridBody.children[0].children[0]);
            }
        }
    }

    public riGridDisputedInvoicesAfterExecute(): any {
        //' Default the description of the first log on refresh - Please Note: There will ALWAYS be a record otherwise this //tab will not be shown!
        if (this.riGridDisputedInvoices.HTMLGridBody && this.riGridDisputedInvoices.HTMLGridBody.children[0]) {
            if (this.riGridDisputedInvoices.HTMLGridBody.children[0].children[0]) {
                this.riGridDisputedInvoices.SetDefaultFocus();
                this.selectedRowFocusDisputedInvoices(this.riGridDisputedInvoices.HTMLGridBody.children[0].children[0]);
            }
        }
    }

    public riGridNotifyAfterExecute(): any {
        //' Default the description of the first log on refresh - Only show details when records found
        if (this.riGridNotify.HTMLGridBody && this.riGridNotify.HTMLGridBody.children[0]) {
            if (this.riGridNotify.HTMLGridBody.children[0].children[0]) {
                this.selectedRowFocusNotify(this.riGridNotify.HTMLGridBody.children[0].children[0]);
            }
        }
    }

    public riGridWorkOrderAfterExecute(): any {
        //' Default the description of the first log on refresh - Only show details when records found
        if (this.riGridWorkOrder.HTMLGridBody && this.riGridWorkOrder.HTMLGridBody.children[0]) {
            if (this.riGridWorkOrder.HTMLGridBody.children[0].children[0]) {
                this.selectedRowFocusWorkOrder(this.riGridWorkOrder.HTMLGridBody.children[0].children[0]);
            }
        }
    }

    public riGridRootCauseAfterExecute(): any {
        //' Default the description of the first log on refresh - Only show details when records found
        if (this.riGridRootCause.HTMLGridBody && this.riGridRootCause.HTMLGridBody.children[0]) {
            this.selectedRowFocusRootCause(this.riGridRootCause.HTMLGridBody.children[0].children[0]);
        }
    }

    public riGridTicketsBodyOnClick(event: any): any {
        let target: any = event.target || event.srcElement || event.currentTarget;
        this.selectedRowFocusTickets(target);
    }

    public riGridLogsBodyOnClick(event: any): any {
        let target: any = event.target || event.srcElement || event.currentTarget;
        this.selectedRowFocusLogs(target);
    }

    public riGridLogsBodyOnDblClick(event: any): any {
        let target: any = event.target || event.srcElement || event.currentTarget;
        this.selectedRowFocusLogs(target);
    }

    public riGridNotifyBodyOnClick(event: any): any {
        let target: any = event.target || event.srcElement || event.currentTarget;
        this.selectedRowFocusNotify(target);
    }

    public riGridRootCauseBodyOnClick(event: any): any {
        let target: any = event.target || event.srcElement || event.currentTarget;
        this.selectedRowFocusRootCause(target);
    }

    public riGridDisputedInvoicesBodyOnClick(event: any): any {
        let target: any = event.target || event.srcElement || event.currentTarget;
        this.selectedRowFocusDisputedInvoices(target);
    }

    public riGridWorkOrderBodyOnClick(event: any): any {
        let target: any = event.target || event.srcElement || event.currentTarget;
        this.selectedRowFocusWorkOrder(target);
    }

    public selectedRowFocusTickets(rsrcElement: any): any {
        this.setControlValue('RelatedCustomerContactNumber', this.riGridTickets.Details.GetValue('RelatedCustomerContactNumber'));
        this.setControlValue('RelatedCustomerContactRowID', this.riGridTickets.Details.GetAttribute('RelatedCustomerContactNumber', 'RowID'));
        this.setControlValue('RelatedProspectNumber', this.riGridTickets.Details.GetValue('RelatedProspectNumber'));
        this.setControlValue('RelatedProspectRowID', this.riGridTickets.Details.GetAttribute('RelatedProspectNumber', 'RowID'));
    }

    public selectedRowFocusDisputedInvoices(rsrcElement: any): any {
        this.setControlValue('SelectedCompanyCode', this.riGridDisputedInvoices.Details.GetValue('Company'));
        this.setControlValue('SelectedCompanyDesc', this.riGridDisputedInvoices.Details.GetAttribute('Company', 'AdditionalProperty'));
        this.setControlValue('SelectedCompanyInvoiceNumber', this.riGridDisputedInvoices.Details.GetValue('InvoiceNumber'));
        this.setControlValue('SelectedInvoiceNumber', this.riGridDisputedInvoices.Details.GetAttribute('InvoiceNumber', 'AdditionalProperty'));
        this.setControlValue('SelectedInvoiceName', this.riGridDisputedInvoices.Details.GetValue('InvoiceName'));
        this.setControlValue('SelectedContractNumber', this.riGridDisputedInvoices.Details.GetAttribute('ExtractDate', 'AdditionalProperty'));
        this.setControlValue('SelectedContractName', this.riGridDisputedInvoices.Details.GetAttribute('PeriodStart', 'AdditionalProperty'));
    }

    public selectedRowFocusLogs(rsrcElement: any): any {
        // ' Show Footer Info Below The Grid
        let cAdditionalInfo: string = this.riGridLogs.Details.GetAttribute('CallLogID', 'AdditionalProperty').split('^');
        if (cAdditionalInfo) {
            this.setControlValue('CallLogSummary', cAdditionalInfo[0]);
            this.setControlValue('CallLogDetails', cAdditionalInfo[1]);
        }
    }

    public selectedRowFocusNotify(rsrcElement: any): any {
        if (this.riGridNotify.HTMLGridBody) {
            //' Show Footer Info Below The Grid
            let cAdditionalInfo: any = this.riGridNotify.Details.GetAttribute('Created', 'AdditionalProperty').split('^');
            setTimeout(() => {
                this.setControlValue('NotifyRecipients', cAdditionalInfo[0]);
                this.setControlValue('NotifyMessage', cAdditionalInfo[1]);
                if (this.getControlValue('NotifyStyle') === 'email') {
                    this.fieldVisibility.trNotifyRecipients = true;
                }
                else {
                    this.fieldVisibility.trNotifyRecipients = false;
                }
                this.fieldVisibility.trNotifyMessage = true;
            }, 0);
        }
    }

    public selectedRowFocusWorkOrder(rsrcElement: any): any {
        this.setControlValue('WONumber', this.riGridWorkOrder.Details.GetValue('WONumber'));
    }

    public selectedRowFocusRootCause(rsrcElement: any): any {
        this.setControlValue('RootCauseCode', this.riGridRootCause.Details.GetAttribute('RootCauseCode', 'AdditionalProperty'));
    }

    public riGridTicketsBodyOnDblClick(event: any): any {
        let target: any = event.target || event.srcElement || event.currentTarget;
        this.selectedRowFocusTickets(target);
        setTimeout(() => {
            switch (this.riGridTickets.CurrentColumnName) {
                case 'RelatedCustomerContactNumber':
                    //' Only execute when the listed ticket is not the same as being updated
                    if (this.getControlValue('RelatedCustomerContactNumber') !== this.getControlValue('CustomerContactNumber')) {
                        //navigate to screen ContactManagement/iCABSCMCustomerContactMaintenance.htm' Mode = 'ContactRelatedTicket'
                        let url = InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_2;
                        if (this.instr(this.pageParams.currentRouteUrl.toString().toLowerCase(), InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_2.toLowerCase()) >= 0) {
                            url = InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_1;
                        }
                        this.pageParams.previousRouteUrl = url;
                        this.pageParams.selfLoad = true;
                        this.navigate('ContactRelatedTicket', url, { RelatedCustomerContactRowID: this.getControlValue('RelatedCustomerContactRowID'), RelatedCustomerContactNumber: this.getControlValue('RelatedCustomerContactNumber') });
                    }
                    break;
                case 'RelatedProspectNumber':
                    //navigate to screen ContactManagement/iCABSCMPipelineProspectMaintenance.htm<Prospect>' Mode = 'ContactRelatedTicket'
                    this.navigate('ContactRelatedTicket', AppModuleRoutes.PROSPECTTOCONTRACT + ProspectToContractModuleRoutes.ICABSCMPIPELINEPROSPECTMAINTENANCE, {
                        RelatedCustomerContactRowID: this.getControlValue('RelatedCustomerContactRowID'),
                        CurrentContractTypeURLParameter: '<Prospect>',
                        RelatedProspectNumber: this.getControlValue('RelatedProspectNumber'),
                        CurrentCallLogID: this.getControlValue('CurrentCallLogID'),
                        SelectedTicketNumber: this.getControlValue('CustomerContactNumber'),
                        AccountNumber: this.getControlValue('AccountNumber'),
                        ContractNumber: this.getControlValue('ContractNumber'),
                        PremiseNumber: this.getControlValue('PremiseNumber')
                    });
                    break;
                case 'NumActions':
                    // navigate screen ContactManagement/iCABSCMCustomerContactDetailGrid.htm Mode = 'ContactRelatedTicket'
                    this.navigate('ContactRelatedTicket', InternalGridSearchApplicationModuleRoutes.ICABSCMCUSTOMERCONTACTDETAILGRID, { RelatedCustomerContactRowID: this.getControlValue('RelatedCustomerContactRowID') });
                    break;
            }
        }, 0);
    }

    public riGridWorkOrderBodyOnDblClick(event?: any): any {
        switch (this.riGridWorkOrder.CurrentColumnName) {
            case 'WONumber':
                //navigate to screen  ContactManagement/iCABSCMWorkOrderMaintenance.htm' Mode = 'TicketMaintenance'
                this.navigate('TicketMaintenance', InternalMaintenanceServiceModuleRoutes.ICABSCMWORKORDERMAINTENANCE, { WONumber: this.getControlValue('WONumber') });
                break;
        }
    }

    public riGridRootCauseBodyOnDblClick(event?: any): any {
        switch (this.riGridRootCause.CurrentColumnName) {
            case 'RootCauseCode':
                //navigate to screen ContactManagement/iCABSCMCustomerContactRootCauseEntry.htm  Mode = 'TicketMaintenanceUpdate
                this.navigate('TicketMaintenanceUpdate', InternalMaintenanceServiceModuleRoutes.ICABSCMCUSTOMERCONTACTROOTCAUSEENTRY);
                break;
        }
    }

    public riGridDisputedInvoicesBodyOnDblClick(event: any): any {
        switch (this.riGridDisputedInvoices.CurrentColumnName) {
            case 'InvoiceNumber':
                //navigate to screen Application/iCABSAContractInvoiceDetailGrid.htm' + CurrentContractTypeURLParameter Mode = 'TicketMaintenance'
                this.navigate('TicketMaintenance', ContractManagementModuleRoutes.ICABSACONTRACTINVOICEDETAILGRID);
                break;
            case 'InDisputeReference':
                // iCABSCMCustomerContactInvoiceMaintenance
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotCovered));
                break;
        }
    }

    public cmdAddRootCauseOnClick(): any {
        //navigate to screen ContactManagement/iCABSCMCustomerContactRootCauseEntry.htm  Mode = 'TicketMaintenanceAdd'
        // this.navigate('TicketMaintenanceAdd', InternalMaintenanceServiceModuleRoutes.ICABSCMCUSTOMERCONTACTROOTCAUSEENTRY);

        let url: string = '#' + InternalMaintenanceServiceModuleRoutes.ICABSCMCUSTOMERCONTACTROOTCAUSEENTRY;

        url += '?parentMode=TicketMaintenanceAdd';
        url += '&' + CBBConstants.c_s_URL_PARAM_COUNTRYCODE + '=' + this.utils.getCountryCode();
        url += '&' + CBBConstants.c_s_URL_PARAM_BUSINESSCODE + '=' + this.utils.getBusinessCode();
        url += '&' + CBBConstants.c_s_URL_PARAM_BRANCHCODE + '=' + this.utils.getBranchCode();
        url += '&' + 'CustomerContactNumber' + '=' + this.getControlValue('CustomerContactNumber');

        window.open(url,
            '_blank',
            'fullscreen=yes,menubar=no,resizable=no,status=no,titlebar=no,toolbar=no');
        return;
    }

    public contactTypeCodeSelectOnChange(eventStatus: boolean = false): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        this.setControlValue('ContactTypeCode', this.getControlValue('ContactTypeCodeSelect'));
        let serviceDataSubs: Subscription = this.getServerData('ChangeOfContactType').subscribe((e) => {
            if (eventStatus) {
                let clist: any = this.contactTypeDetailCodeSelectList.find(item => (item.value === this.getControlValue('ContactTypeDetailCode')));
                if (!clist) {
                    this.setControlValue('ContactTypeDetailCodeSelect', '');
                    this.setControlValue('ContactTypeDetailCode', '');
                }
            }
            retObj.next(e);
        });
        this.subscriptionManager.add(serviceDataSubs);
        return retObj;
    }

    public contactTypeDetailCodeSelectOnChange(): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        this.setMandatoryFields();
        if (this.currentMode === MntConst.eModeAdd) {
            if (this.getControlValue('ContactTypeDetailPassedList').indexOf('^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
                this.setControlValue('chkContactPassedToActioner', true);
            } else {
                this.setControlValue('chkContactPassedToActioner', false);
            }
            if (this.pageParams.CRAutoCreateProspect && this.getControlValue('ContactTypeDetailCRProspectList').indexOf('^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
                this.setControlValue('chkCreateProspect', true);
            } else {
                this.setControlValue('chkCreateProspect', false);
            }
        }

        this.setControlValue('ContactTypeDetailCode', this.getControlValue('ContactTypeDetailCodeSelect'));
        let dataSubs: Subscription = this.getServerData('ChangeOfContactTypeDetail').subscribe((e) => {
            if (!this.getControlValue('CRContractTerm')) {
                this.fieldVisibility.spnCRNoticePeriod = false;
            } else {
                this.fieldVisibility.spnCRNoticePeriod = true;
            }
            this.showHideNextActionerOptions();
            retObj.next(e);
        });
        this.subscriptionManager.add(dataSubs);
        return retObj;
    }

    public contactStatusCodeSelectOnChange(): any {
        if (this.getControlValue('ContactStatusCodeSelect')) {
            this.setControlValue('ContactStatusCode', this.getControlValue('ContactStatusCodeSelect'));

            if (this.pageParams.cScheduledStatusList.indexOf(this.getControlValue('ContactStatusCode')) >= 0) {
                if (!this.getControlValue('ScheduledCloseDate')) {
                    this.setControlValue('ScheduledCloseDate', new Date());
                }
                this.fieldVisibility.tdScheduledCloseDate = true;
                this.setRequiredStatus('ScheduledCloseDate', true);
                this.fieldRequired['ScheduledCloseDate'] = true;
            } else {
                this.setRequiredStatus('ScheduledCloseDate', false);
                this.fieldVisibility.tdScheduledCloseDate = false;
                this.fieldRequired['ScheduledCloseDate'] = false;
            }
        }
    }

    public cmdScheduleVisitOnClick(): any {
        let strURL: string;
        let postDataAdd = {
            Function: 'GetURLLink',
            BusinessCode: this.utils.getBusinessCode(),
            EmployeeCode: this.getControlValue('NextActionEmployeeCode'),
            ServiceCoverRowID: this.getControlValue('ServiceCoverRowID')
        };

        this.executeRequest(this, 'GetURLLink', '', postDataAdd, (data) => {
            if (data) {
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    strURL = data['URLLink'];
                    if (strURL) {
                        window.open(strURL, '_blank');
                    }
                }
            }
        });
    }

    public cmdContactEmployeeOnClick(): any {
        // navigate to screen ContactManagement/iCABSCMCustomerContactEmployeeView.htm  Mode = 'CustomerContactEntry'
        this.isSavedEnableFlag = false;
        this.pageParams.isFormDirty = this.uiForm.dirty;
        this.pageParams.backRouteUrl = InternalMaintenanceServiceModuleRoutes.ICABSCMCUSTOMERCONTACTEMPLOYEEVIEW;
        this.pageParams.NextActionEmployeeCode = this.getControlValue('NextActionEmployeeCode');
        this.pageParams.NextActionEmployeeName = this.getControlValue('NextActionEmployeeName');
        this.navigate('CustomerContactEntry', InternalMaintenanceServiceModuleRoutes.ICABSCMCUSTOMERCONTACTEMPLOYEEVIEW);
    }

    public accountNumber_onkeydown(): any {
        if (this.accountNumberEllipsis)
            this.accountNumberEllipsis.openModal();
    }

    public contactPostcodeOnChange(): any {
        this.getServerData('ChangeOfPostcode');
    }

    public originatingEmployeeCodeOnChange(): any {
        let lPopulateAssignTo: boolean;
        if (this.instr(this.getControlValue('ContactTypeDetailOrigList'), '^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
            lPopulateAssignTo = false;
            if (this.currentMode === MntConst.eModeAdd) {
                lPopulateAssignTo = true;
            } else {
                if (this.instr(this.getControlValue('ContactTypeDetailSuperList'), '^' + this.getControlValue('ContactTypeDetailCodeSelect') + '^') >= 0) {
                    let promptVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.customerContactMaintenance.Do_you_want_to_update_the_AssignTo_Employee_with_their_Supervisor, null, function yesSuspension(): any {
                        this.getServerData('ChangeOfOriginatingEmployee');
                    }.bind(this));
                    promptVO.title = MessageConstant.PageSpecificMessage.customerContactMaintenance.Employee_Has_Changed;
                    promptVO.cancelLabel = 'No';
                    promptVO.confirmLabel = 'Yes';
                    this.modalAdvService.emitPrompt(promptVO);
                }
            }

            if (lPopulateAssignTo) {
                this.getServerData('ChangeOfOriginatingEmployee');
            }
        }
    }

    public nextActionEmployeeCodeOnChange(): any {
        if (this.getControlValue('ThisEmployeeCode') !== this.getControlValue('NextActionEmployeeCode')) {
            this.fieldVisibility.spnKeepOwnership = true;
            if ((this.currentMode === MntConst.eModeAdd) && this.getControlValue('ContactTypeCallout') === true) {
                this.fieldVisibility.spnChkContactPassedToActioner = false;
            } else {
                this.fieldVisibility.spnChkContactPassedToActioner = true;
            }
        } else {
            this.fieldVisibility.spnKeepOwnership = false;
            this.fieldVisibility.spnChkContactPassedToActioner = false;
        }

        if (this.getControlValue('DefaultAssigneeEmployeeCode') && this.getControlValue('DefaultAssigneeEmployeeCode') !== this.getControlValue('NextActionEmployeeCode')) {
            this.tdDefaultAssigneeEmployeeLabelBgColor = true;
        } else {
            this.tdDefaultAssigneeEmployeeLabelBgColor = false;
        }
        if (!this.isInitialising) {
            this.getServerData('ChangeOfAssignee');
        }
        this.enableScheduleButton();
    }

    public contactNotifyWhenSelectOnChange(): any {
        if (this.getControlValue('ContactNotifyWhenSelect') === '0' || this.getControlValue('ContactNotifyWhenSelect') === '-1') {
            this.fieldVisibility.spanContactNotifyTypeSelect = false;
        } else {
            this.fieldVisibility.spanContactNotifyTypeSelect = true;
        }
        this.contactNotifyTypeSelectOnChange();
    }

    public contactNotifyTypeSelectOnChange(): any {
        if (this.getControlValue('ContactNotifyWhenSelect') === '0' || this.getControlValue('ContactNotifyWhenSelect') === '-1') {
            this.setRequiredStatus('ContactEmailAddress', false);
            this.setRequiredStatus('ContactMobileNumber', false);
            this.fieldRequired['ContactEmailAddress'] = false;
            this.fieldRequired['ContactMobileNumber'] = false;
        } else {
            if (this.getControlValue('ContactNotifyTypeSelect') === '1') {
                this.setRequiredStatus('ContactEmailAddress', true);
                this.setRequiredStatus('ContactMobileNumber', false);
                this.fieldRequired['ContactEmailAddress'] = true;
                this.fieldRequired['ContactMobileNumber'] = false;
            } else {
                this.setRequiredStatus('ContactEmailAddress', false);
                this.setRequiredStatus('ContactMobileNumber', true);
                this.fieldRequired['ContactEmailAddress'] = false;
                this.fieldRequired['ContactMobileNumber'] = true;
            }
        }
    }

    public menuOnChange(val: any): any {
        this.uiForm.controls['menu'].markAsPristine();
        let strURLParam: string = '';
        if (this.recordSelected() && this.currentMode !== MntConst.eModeAdd) {
            switch (val) {
                case 'callout':
                    if (this.getControlValue('CallOutRowID') !== '' && this.getControlValue('CallOutRowID')) {
                        //navigate to screen ContactManagement/iCABSCMCallOutMaintenance.htm Mode = 'UpdateCallOut-ContactSearch'
                        this.navigate('UpdateCallOut-ContactSearch', InternalMaintenanceServiceModuleRoutes.ICABSCMCALLOUTMAINTENANCE, {
                            CallOutROWID: this.getControlValue('CallOutRowID')
                        });
                        this.isSaved = true;
                    }
                    break;
                case 'detail':
                    //navigate to screen ContactManagement/iCABSCMCustomerContactDetailGrid.htm Mode = 'Contact'
                    this.navigate('Contact', InternalGridSearchApplicationModuleRoutes.ICABSCMCUSTOMERCONTACTDETAILGRID, { CustomerContactRowID: this.pageParams.CustomerContact, CustomerContactNumber: this.getControlValue('CustomerContactNumber') });
                    break;
                case 'history':
                    //navigate to screen  ContactManagement/iCABSCMCustomerContactHistoryGrid.htm
                    this.navigate('', InternalGridSearchServiceModuleRoutes.ICABSCMCUSTOMERCONTACTHISTORYGRID);
                    break;
                case 'contacts':
                    if (this.recordSelected()) {
                        this.cmdContactDetails('Menu');
                    }
                    break;
                case 'account':
                    if (this.getControlValue('AccountNumber') && this.trim(this.getControlValue('AccountNumber')) !== '') {
                        //navigate to screen Application/iCABSAAccountMaintenance.htm  Mode = 'LoadByKeyFields'
                        this.setNavReturnValues();
                        const queryParams: object = {  //IUI-23933
                            parentMode: 'LoadByKeyFields',
                            AccountNumber: this.getControlValue('AccountNumber'),
                            AccountName: this.getControlValue('AccountName'),
                            isReturnFlag: true
                        };
                        this.navigate('LoadByKeyFields', ContractManagementModuleRoutes.ICABSAACCOUNTMAINTENANCE, queryParams);
                    }
                    break;
                case 'contract':
                    if (this.getControlValue('ContractNumber') && this.trim(this.getControlValue('ContractNumber')) !== '') {
                        //navigate to screen Application/iCABSAContractMaintenance.htm' Mode = 'LoadByKeyFields'
                        this.setNavReturnValues();
                        const queryParams: object = {  //IUI-23933
                            parentMode: 'LoadByKeyFields',
                            ContractNumber: this.getControlValue('ContractNumber'),
                            isReturnFlag: true
                        };
                        this.navigate('LoadByKeyFields', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, queryParams);
                    }
                    break;
                case 'job':
                    if (this.getControlValue('ContractNumber') && this.trim(this.getControlValue('ContractNumber')) !== '') {
                        //navigate to screen Application/iCABSAContractMaintenance.htm<job>  Mode = 'LoadByKeyFields'
                        this.setNavReturnValues();
                        const queryParams: object = {  //IUI-23933
                            parentMode: 'LoadByKeyFields',
                            ContractNumber: this.getControlValue('ContractNumber'),
                            CurrentContractTypeURLParameter: '<job>',
                            isReturnFlag: true
                        };
                        this.navigate('LoadByKeyFields', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, queryParams);
                    }
                    break;
                case 'product sales':
                    if (this.getControlValue('ContractNumber') && this.trim(this.getControlValue('ContractNumber')) !== '') {
                        //navigate to screen Application/iCABSAContractMaintenance.htm<product>  Mode = 'LoadByKeyFields'
                        this.setNavReturnValues();
                        const queryParams: object = {  //IUI-23933
                            parentMode: 'LoadByKeyFields',
                            ContractNumber: this.getControlValue('ContractNumber'),
                            CurrentContractTypeURLParameter: '<product>',
                            isReturnFlag: true
                        };
                        this.navigate('LoadByKeyFields', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, queryParams);
                    }
                    break;
                case 'premise':
                    if ((this.trim(this.getControlValue('ContractNumber')) !== '') && (this.trim(this.getControlValue('PremiseNumber')) !== '')) {
                        switch (this.getControlValue('ContractTypeCode').toUpperCase()) {
                            case 'C':
                                strURLParam = 'contract';
                                break;
                            case 'J':
                                strURLParam = 'job';
                                break;
                            case 'P':
                                strURLParam = 'product';
                                break;
                            default:
                                strURLParam = '';
                        }
                        //navigate to screen Application/iCABSAPremiseMaintenance.htm<' + //strURLParam + '>' Mode = 'LoadByKeyFields'
                        this.navigate('LoadByKeyFields', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE,
                            {
                                contractTypeCode: this.getControlValue('ContractTypeCode') ? this.getControlValue('ContractTypeCode').toUpperCase() : '',
                                ContractNumber: this.getControlValue('ContractNumber'),
                                PremiseNumber: this.getControlValue('PremiseNumber')
                            });
                    }
                    break;
                case 'lostbusiness':
                    if (this.getControlValue('ContactTypeClientRetention')) {
                        this.isSaved = true;
                        if (this.trim(this.getControlValue('ContractNumber') !== '') && this.trim(this.getControlValue('PremiseNumber') !== '') && this.trim(this.getControlValue('ProductCode') !== '')) {
                            //navigate to screen Application/iCABSALostBusinessRequestMaintenance.htm<ServiceCover>' Mode = 'ContactManagement'
                            this.navigate('ContactManagement', InternalMaintenanceApplicationModuleRoutes.ICABSALOSTBUSINESSREQUESTMAINTENANCE, { CurrentContractTypeURLParameter: '<ServiceCover>' });
                        } else if (this.trim(this.getControlValue('ContractNumber') !== '') && this.trim(this.getControlValue('PremiseNumber') !== '')) {
                            //navigate to screen Application/iCABSALostBusinessRequestMaintenance.htm<Premise>'  Mode = 'ContactManagement'
                            this.navigate('ContactManagement', InternalMaintenanceApplicationModuleRoutes.ICABSALOSTBUSINESSREQUESTMAINTENANCE, { CurrentContractTypeURLParameter: '<Premise>' });
                        } else if (this.trim(this.getControlValue('ContractNumber') !== '')) {
                            //navigate to screen Application/iCABSALostBusinessRequestMaintenance.htm<Contract>' Mode = 'ContactManagement'
                            this.navigate('ContactManagement', InternalMaintenanceApplicationModuleRoutes.ICABSALOSTBUSINESSREQUESTMAINTENANCE, { CurrentContractTypeURLParameter: '<Contract>' });
                        }
                    }
                    break;
                case 'campaign':
                    if (this.recordSelected) {
                        //navigate to screen ContactManagement/iCABSCMCampaignEntry.htm'   Mode = 'ContactManagement'
                        this.navigate('ContactManagement', InternalMaintenanceServiceModuleRoutes.ICABSCMCAMPAIGNENTRY);
                    }
                    break;
                case 'rootcause':
                    if (this.recordSelected) {
                        //navigate to screen ContactManagement/iCABSCMCustomerContactRootCauseEntry.htm<bottom>'   Mode = 'TicketMaintenanceAdd'
                        this.navigate('TicketMaintenanceAdd', InternalMaintenanceServiceModuleRoutes.ICABSCMCUSTOMERCONTACTROOTCAUSEENTRY);
                        //this.riGridRootCause.Execute
                    }
                    break;
                case 'tocontract':
                    //navigate to screen Application/iCABSAContractMaintenance.htm  Mode = 'AddFromProspect'
                    let msg: Subscription = this.warningMessage().subscribe((e) => {
                        if (this.recordSelected() && e) {
                            this.setNavReturnValues();
                            this.router.navigate([ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE], {
                                queryParams: {
                                    parentMode: 'AddFromProspect',
                                    ProspectNumber: this.getControlValue('DispProspectNumber'),
                                    isReturnFlag: true
                                }
                            });
                        }
                        else {
                            this.btnProspectOnClick();
                        }
                    });
                    this.subscriptionManager.add(msg);
                    break;
                case 'tojob':
                    //navigate to screen Application/iCABSAContractMaintenance.htm<job> Mode = 'AddFromProspect'
                    let msgJobSubs: Subscription = this.warningMessage().subscribe((e) => {
                        if (this.recordSelected() && e) {
                            this.setNavReturnValues();
                            this.router.navigate([ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE], {
                                queryParams: {
                                    parentMode: 'AddFromProspect',
                                    ProspectNumber: this.getControlValue('DispProspectNumber'),
                                    CurrentContractTypeURLParameter: 'job',
                                    isReturnFlag: true
                                }
                            });
                        }
                        else {
                            this.btnProspectOnClick();
                        }
                    });
                    this.subscriptionManager.add(msgJobSubs);
                    break;
                case 'toproductsale':
                    //navigate to screen iCABSAContractMaintenance.htm<product>'  Mode = 'AddFromProspect'
                    let msgSubs: Subscription = this.warningMessage().subscribe((e) => {
                        if (this.recordSelected() && e) {
                            this.setNavReturnValues();
                            this.router.navigate([ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE], {
                                queryParams: {
                                    parentMode: 'AddFromProspect',
                                    ProspectNumber: this.getControlValue('DispProspectNumber'),
                                    CurrentContractTypeURLParameter: 'product',
                                    isReturnFlag: true
                                }
                            });
                        }
                        else {
                            this.btnProspectOnClick();
                        }
                    });
                    this.subscriptionManager.add(msgSubs);
                    break;
                case 'contactcentresearch':
                    if (this.recordSelected) {
                        //navigate to screen ContactManagement/iCABSCMCallCentreGrid.htm' Mode = 'ContactManagement'
                        this.navigate('ContactManagement', AppModuleRoutes.CCM + CCMModuleRoutes.ICABSCMCALLCENTREGRID,
                            {
                                parentMode: 'ContactManagement',
                                ContractNumber: this.getControlValue('ContractNumber'),
                                PremiseNumber: this.getControlValue('PremiseNumber'),
                                AccountNumber: this.getControlValue('AccountNumber')
                            }
                        );
                    }
                    break;
            }
        }
    }

    public contractNumberOnChange(): any {
        this.contractNumberFormatOnChange(); // ' fill the contract number field with leading zeroes.
        this.clearPremiseDetails();
        let dataSubs: Subscription = this.getServerData('ChangeOfContract').subscribe((e) => {
            this.updateEllipsisConfigValue();
        });
        this.subscriptionManager.add(dataSubs);
    }

    public accountNumberOnChange(): any {
        this.setControlValue('ContractNumber', '');
        this.setControlValue('ContractName', '');
        this.clearPremiseDetails();
        this.accountNumberFormatOnChange();
        let dataSubs: Subscription = this.getServerData('ChangeOfAccount').subscribe((e) => {
            this.updateEllipsisConfigValue();
        });
        this.subscriptionManager.add(dataSubs);
    }

    public premiseNumberOnChange(): any {
        this.clearProductDetails();
        let dataSubs: Subscription = this.getServerData('ChangeOfPremise').subscribe((e) => {
            this.updateEllipsisConfigValue();
        });
        this.subscriptionManager.add(dataSubs);
    }

    public get premiseSearchModeStatus(): any {
        //return (this.getControlValue('ContractNumber') && this.trim(this.getControlValue('ContractNumber')) !== '') ? true : false;
        //Screen is out of scope : ContactManagement/iCABSCMPremiseSearch.htm
        return true;
    }

    public get isPremiseSearchDisable(): any {
        return this.uiForm.controls['PremiseNumber'].disabled;
    }

    public get isAccountSearchDisable(): any {
        return this.uiForm.controls['AccountNumber'].disabled;
    }

    public get isContractSearchDisable(): any {
        return this.uiForm.controls['ContractNumber'].disabled;
    }

    public get isProductCodeSearchDisable(): any {
        return this.uiForm.controls['ProductCode'].disabled;
    }

    public get isOriginatingEmployeeCodeDisable(): any {
        return this.uiForm.controls['OriginatingEmployeeCode'].disabled;
    }

    public get isNextActionEmployeeCodeDisable(): any {
        return this.uiForm.controls['NextActionEmployeeCode'].disabled;
    }

    public userAuthStatus(): boolean {
        //' If the user is not allowed to create any tickets (based upon the userauthority check above then...
        if (this.pageParams.cInitialMode === 'Add') {
            return !this.pageParams.lUserCanAdd ? false : true;
        }
        return true;
    }

    public productCodeOnChange(): Observable<any> {
        let retObj: ReplaySubject<any> = new ReplaySubject(1);
        if (!this.trim(this.getControlValue('ProductCode'))) {
            this.setControlValue('ProductCode', '');
            this.setControlValue('ProductDesc', '');
            this.setControlValue('ServiceCoverNumber', '');
            this.setAllSCRowID('');
            this.showHideServiceType(false);
            retObj.next(true);
        } else {
            let subs: Subscription = this.getServiceCoverNumber().subscribe((e1) => {
                let dataSubs: Subscription = this.getServerData('ChangeOfProduct').subscribe((e) => {
                    retObj.next(true);
                });
                this.subscriptionManager.add(dataSubs);
            });
            this.subscriptionManager.add(subs);
        }
        return retObj;
    }

    public cmdIntNotificationsOnClick(): any {
        //navigate to screen ContactManagement/iCABSCMCustomerContactNotificationsGrid.htm Mode = 'ContactManagement'
        this.isSavedEnableFlag = false;
        this.pageParams.isFormDirty = this.uiForm.dirty;
        this.pageParams.previousRouteUrl = InternalGridSearchApplicationModuleRoutes.ICABSCMCUSTOMERCONTACTNOTIFICATIONSGRID;
        this.navigate('ContactManagement', InternalGridSearchApplicationModuleRoutes.ICABSCMCUSTOMERCONTACTNOTIFICATIONSGRID);
    }

    public onCallOutTktDtlsClick(): any {
        if (this.getControlValue('CallOutRowID') !== '' && this.getControlValue('CallOutRowID')) {
            this.navigate('UpdateCallOut-ContactSearch', InternalMaintenanceServiceModuleRoutes.ICABSCMCALLOUTMAINTENANCE, {
                CallOutROWID: this.getControlValue('CallOutRowID')
            });
            this.isSaved = true;
        }
    }

    public addressCodeSelectOnChange(): any {
        //' Show the relevant address
        switch (this.getControlValue('AddressCodeSelect')) {
            case 'account':
                this.setControlValue('AddressName', this.getControlValue('AccountAddressName'));
                this.setControlValue('AddressLine1', this.getControlValue('AccountAddressLine1'));
                this.setControlValue('AddressLine2', this.getControlValue('AccountAddressLine2'));
                this.setControlValue('AddressLine3', this.getControlValue('AccountAddressLine3'));
                this.setControlValue('AddressLine4', this.getControlValue('AccountAddressLine4'));
                this.setControlValue('AddressLine5', this.getControlValue('AccountAddressLine5'));
                this.setControlValue('AddressPostcode', this.getControlValue('AccountAddressPostcode'));
                this.setControlValue('AddressContactName', this.getControlValue('AccountAddressContactName'));
                this.setControlValue('AddressContactPosn', this.getControlValue('AccountAddressContactPosn'));
                this.setControlValue('AddressContactPhone', this.getControlValue('AccountAddressContactPhone'));
                this.setControlValue('AddressContactMobile', this.getControlValue('AccountAddressContactMobile'));
                this.setControlValue('AddressContactEmail', this.getControlValue('AccountAddressContactEmail'));
                break;
            case 'premise':
                this.setControlValue('AddressName', this.getControlValue('PremiseAddressName'));
                this.setControlValue('AddressLine1', this.getControlValue('PremiseAddressLine1'));
                this.setControlValue('AddressLine2', this.getControlValue('PremiseAddressLine2'));
                this.setControlValue('AddressLine3', this.getControlValue('PremiseAddressLine3'));
                this.setControlValue('AddressLine4', this.getControlValue('PremiseAddressLine4'));
                this.setControlValue('AddressLine5', this.getControlValue('PremiseAddressLine5'));
                this.setControlValue('AddressPostcode', this.getControlValue('PremiseAddressPostcode'));
                this.setControlValue('AddressContactName', this.getControlValue('PremiseAddressContactName'));
                this.setControlValue('AddressContactPosn', this.getControlValue('PremiseAddressContactPosn'));
                this.setControlValue('AddressContactPhone', this.getControlValue('PremiseAddressContactPhone'));
                this.setControlValue('AddressContactMobile', this.getControlValue('PremiseAddressContactMobile'));
                this.setControlValue('AddressContactEmail', this.getControlValue('PremiseAddressContactEmail'));
                break;
            case 'prospectaccount':
                this.setControlValue('AddressName', this.getControlValue('ProAcctAddressName'));
                this.setControlValue('AddressLine1', this.getControlValue('ProAcctAddressLine1'));
                this.setControlValue('AddressLine2', this.getControlValue('ProAcctAddressLine2'));
                this.setControlValue('AddressLine3', this.getControlValue('ProAcctAddressLine3'));
                this.setControlValue('AddressLine4', this.getControlValue('ProAcctAddressLine4'));
                this.setControlValue('AddressLine5', this.getControlValue('ProAcctAddressLine5'));
                this.setControlValue('AddressPostcode', this.getControlValue('ProAcctAddressPostcode'));
                this.setControlValue('AddressContactName', this.getControlValue('ProAcctAddressContactName'));
                this.setControlValue('AddressContactPosn', this.getControlValue('ProAcctAddressContactPosn'));
                this.setControlValue('AddressContactPhone', this.getControlValue('ProAcctAddressContactPhone'));
                this.setControlValue('AddressContactMobile', this.getControlValue('ProAcctAddressContactMobile'));
                this.setControlValue('AddressContacteMail', this.getControlValue('ProAcctAddressContactEmail'));
                break;
            case 'prospectpremise':
                this.setControlValue('AddressName', this.getControlValue('ProPremAddressName'));
                this.setControlValue('AddressLine1', this.getControlValue('ProPremAddressLine1'));
                this.setControlValue('AddressLine2', this.getControlValue('ProPremAddressLine2'));
                this.setControlValue('AddressLine3', this.getControlValue('ProPremAddressLine3'));
                this.setControlValue('AddressLine4', this.getControlValue('ProPremAddressLine4'));
                this.setControlValue('AddressLine5', this.getControlValue('ProPremAddressLine5'));
                this.setControlValue('AddressPostcode', this.getControlValue('ProPremAddressPostcode'));
                this.setControlValue('AddressContactName', this.getControlValue('ProPremAddressContactName'));
                this.setControlValue('AddressContactPosn', this.getControlValue('ProPremAddressContactPosn'));
                this.setControlValue('AddressContactPhone', this.getControlValue('ProPremAddressContactPhone'));
                this.setControlValue('AddressContactMobile', this.getControlValue('ProPremAddressContactMobile'));
                this.setControlValue('AddressContactEmail', this.getControlValue('ProPremAddressContactEmail'));
                break;
            case 'public':
                this.setControlValue('AddressName', this.getControlValue('PublicAddressName'));
                this.setControlValue('AddressLine1', this.getControlValue('PublicAddressLine1'));
                this.setControlValue('AddressLine2', this.getControlValue('PublicAddressLine2'));
                this.setControlValue('AddressLine3', this.getControlValue('PublicAddressLine3'));
                this.setControlValue('AddressLine4', this.getControlValue('PublicAddressLine4'));
                this.setControlValue('AddressLine5', this.getControlValue('PublicAddressLine5'));
                this.setControlValue('AddressPostcode', this.getControlValue('PublicAddressPostcode'));
                this.setControlValue('AddressContactName', this.getControlValue('PublicAddressContactName'));
                this.setControlValue('AddressContactPosn', this.getControlValue('PublicAddressContactPosn'));
                this.setControlValue('AddressContactPhone', this.getControlValue('PublicAddressContactPhone'));
                this.setControlValue('AddressContactMobile', this.getControlValue('PublicAddressContactMobile'));
                this.setControlValue('AddressContactEmail', this.getControlValue('PublicAddressContactEmail'));
        }
        if (this.getControlValue('CallOutRowID')) {
            this.formPristine();
        }
    }

    public onContractSearchDataReceived(data: any, route: any): void {
        this.setControlValue('ContractNumber', data.ContractNumber);
        this.setControlValue('ContractName', data.ContractName);
        if (this.ellipsisConfig['premises']) {
            this.ellipsisConfig['premises'].childConfigParams['ContractNumber'] = data.ContractNumber;
            this.ellipsisConfig['premises'].childConfigParams['ContractName'] = data.ContractName;
        }
        if (this.ellipsisConfig['productCode']) {
            this.ellipsisConfig['productCode'].childConfigParams['ContractNumber'] = data.ContractNumber;
            this.ellipsisConfig['productCode'].childConfigParams['ContractName'] = data.ContractName;
        }
        this.uiForm.controls['ContractNumber'].markAsDirty();
    }

    public onPremisesDataReceived(data: any, route: any): void {
        this.setControlValue('PremiseNumber', data.PremiseNumber);
        this.setControlValue('PremiseName', data.PremiseName || '');
        if (this.ellipsisConfig['productCode']) {
            this.ellipsisConfig['productCode'].childConfigParams['PremiseNumber'] = data.PremiseNumber;
            this.ellipsisConfig['productCode'].childConfigParams['PremiseName'] = data.PremiseName || '';
        }
        this.uiForm.controls['PremiseNumber'].markAsDirty();
    }

    public onProductCodeDataReceived(data: any, route: any): void {
        this.setControlValue('ProductCode', data.ProductCode || '');
        this.setControlValue('ProductDesc', data.ProductDesc || '');
        this.uiForm.controls['ProductCode'].markAsDirty();
        if (data['row']) {
            this.setControlValue('ServiceCoverNumber', data['row']['ServiceCoverNumber']);
            this.setControlValue('ServiceCoverRowID', data['row']['ttServiceCover']);
            this.processServiceCoverData();
        }
    }

    public onAccountSearchDataReceived(data: any, route: any): void {
        //Set values in contract modal
        if (this.ellipsisConfig['contract']) {
            this.ellipsisConfig['contract'].childConfigParams['accountNumber'] = data.AccountNumber;
            this.ellipsisConfig['contract'].childConfigParams['accountName'] = data.AccountName;
        }
        this.setControlValue('AccountNumber', data.AccountNumber);
        this.setControlValue('AccountName', data.AccountName);
        this.uiForm.controls['AccountNumber'].markAsDirty();
    }

    public updateEllipsisConfigValue(): void {
        if (this.ellipsisConfig['contract']) {
            this.ellipsisConfig.contract.childConfigParams['accountNumber'] = this.getControlValue('AccountNumber') || '';
            this.ellipsisConfig.contract.childConfigParams['accountName'] = this.getControlValue('AccountName') || '';
        }
        if (this.ellipsisConfig['premises']) {
            this.ellipsisConfig.premises.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber') || '';
            this.ellipsisConfig.premises.childConfigParams['ContractName'] = this.getControlValue('ContractName') || '';
        }
        if (this.ellipsisConfig['productCode']) {
            this.ellipsisConfig.productCode.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber') || '';
            this.ellipsisConfig.productCode.childConfigParams['ContractName'] = this.getControlValue('ContractName') || '';
            this.ellipsisConfig.productCode.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber') || '';
            this.ellipsisConfig.productCode.childConfigParams['PremiseName'] = this.getControlValue('PremiseName') || '';
        }
    }

    public riExchangeUnloadHTMLDocument(): void {
        if (this.isSaved) {
            //' The following coding is used within CCentre To Fire Off The 'End Log' Window On Return
            this.setControlValue('ClosedWithChanges', 'Y');
            this.setControlValue('WindowClosingName', 'AmendmentsMade');
            // TODO: to be used in future
            //this.riExchange.SetParentHTMLInputValue('WindowClosingName', this.getControlValue('WindowClosingName'));
            //this.riExchange.SetParentHTMLInputValue('ClosedWithChanges', this.getControlValue('ClosedWithChanges'));
        }
    }

    public get isReloadAddMode(): boolean {
        return (this.isNavReturnStatus && this.copyPreviousMode === 'Update');
    }

    public reloadPageInUpdateMode(): void {
        this.pageParams.cInitialMode = 'Update';
        this.currentMode = MntConst.eModeUpdate;
        this.setControlValue('CustomerContactNumber', this.pageParams['CustomerContactNumber']);
        let params = {
            CustomerContactNumber: this.getControlValue('CustomerContactNumber'),
            CustomerContactROWID: this.pageParams['CustomerContact']
        };
        this.cloneFormData();
        let subs: Subscription = this.riMaintenanceFetchRecord(params).subscribe((e) => {
            this.setScreenTo('Update');
            this.loadPostScreenData();
            this.formPristine();
            this.clearErrorTabs();
            this.cloneFormData();
        });
        this.subscriptionManager.add(subs);
    }

    public getReturnFlagStatus(): boolean {
        let isReturnFlag: boolean;
        if (this.riExchange.URLParameterContains('isReturnFlag')) {
            isReturnFlag = (this.riExchange.getParentHTMLValue('isReturnFlag').toLowerCase() === 'true') ? true : false;
        }
        return isReturnFlag;
    }

    public setNavReturnValues(): void {
        let pageObj = this.riExchange.createPageObject(this.uiForm, this.controls, this.pageParams);
        this.pageParams.isReturn = true;
        this.riExchange.initBridge(pageObj);
    }

    public getNavReturnStatus(): boolean {
        if (typeof this.riExchange.bridgeBaseClass !== 'undefined') {
            if (this.riExchange.bridgeBaseClass['self']) {
                let params = this.riExchange.bridgeBaseClass['self'];
                if (params && params.isReturn === true) {
                    this.pageParams = params;
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public selfReloadStatus(): boolean {
        if (this.pageParams.selfLoad && this.pageParams.currentRouteUrl && (this.instr(this.pageParams.currentRouteUrl.toLowerCase(), InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_1.toLowerCase()) >= 0)
            && this.pageParams.lastNavRouteUrl && (this.instr(this.pageParams.lastNavRouteUrl.toLowerCase(), InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_1.toLowerCase()) >= 0)) {
            return true;
        }
        return false;
    }

    public cloneFormData(): void {
        this.cloneFormValues = {
            FormValues: this.uiForm.getRawValue(),
            contactTypeDetailCodeSelectList: this.contactTypeDetailCodeSelectList,
            contactStatusCodeSelectList: this.contactStatusCodeSelectList,
            contactTypeCodeSelectList: this.contactTypeCodeSelectList,
            addressCodeSelectList: this.addressCodeSelectList,
            originalContactMediumCodeSelectList: this.originalContactMediumCodeSelectList,
            buttonControls: JSON.stringify(this.buttonControls),
            fieldVisibility: JSON.stringify(this.fieldVisibility),
            fieldRequired: JSON.stringify(this.fieldRequired)
        };
    }

    public disableFormControls(): void {
        for (let key in this.uiForm.controls) {
            if (key !== 'menu' && key !== 'AddressCodeSelect' && this.uiForm.controls[key]) {
                this.uiForm.controls[key].disable();
                this.disableControl('NotifyStyle', false);
                this.disableControl('NotifyDestination', false);
            }
        }
    }

    public setCurrentPageUrl(): void {
        this.routerSubscription = this.router.events.subscribe(event => {
            if (event) {
                this.pageParams.currentRouteUrl = event['url'];
            }
        });
    }

    public fromDateSelectedValue(value: any, controlName: string): void {
        if (this.uiForm.controls.hasOwnProperty(controlName)) {
            this.uiForm.controls[controlName].markAsDirty();
        }
    }

    public onShortDescriptionChange(): void {
        if (!this.getControlValue('Comments')) {
            this.setControlValue('Comments', this.getControlValue('ShortDescription'));
        }
    }

    public canDeactivate(): Observable<boolean> {
        if (this.isSavedEnableFlag && (this.uiForm.dirty || this.pageParams.isFormDirty)) {
            this.routeAwayGlobals.setSaveEnabledFlag(true);
        } else {
            this.routeAwayGlobals.setSaveEnabledFlag(false);
        }
        if (this.routeAwayComponent) {
            return this.routeAwayComponent.canDeactivate();
        }
    }

    public onSlideClick(e: any, slideName: string): void {
        switch (slideName) {
            case 'workorder':
                this.setControlValue('WONumber', this.getControlValue('WONumber') || e.slide[0]['value']);
                this.riGridWorkOrder.CurrentColumnName = 'WONumber';
                this.riGridWorkOrderBodyOnDblClick();
                break;

            case 'notify':
                this.riGridNotifyBodyOnClick(e.event);
                break;

            case 'rootcause':
                this.riGridRootCauseBodyOnDblClick();
                break;

            case 'tickets':
                this.riGridTicketsBodyOnDblClick(e.event);
                break;

            case 'dispute':
                this.riGridDisputedInvoices.CurrentColumnName = 'InvoiceNumber';
                this.riGridDisputedInvoicesBodyOnDblClick(e.event);
                break;

            case 'logs':
                //no drill down
                break;
        }
    }

}
