import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Injector } from '@angular/core';
import { QueryParams } from '../../../shared/services/http-params-wrapper';

import { Observable } from 'rxjs/Rx';

import { PageIdentifier } from './../../base/PageIdentifier';
import { QueryParametersCallback } from './../../base/Callback';
import { BaseComponent } from './../../../app/base/BaseComponent';
import { HttpService } from './../../../shared/services/http-service';
import { MntConst, RiTab } from './../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { PreparationSearchComponent } from './../search/iCABSBPreparationSearch.component';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';

@Component({
    templateUrl: 'iCABSSePrepUsedMaintenance.html'
})

export class SePrepUsedMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit, QueryParametersCallback {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    //Ellipsis
    @ViewChild('PrepCodeEllipsis') PrepCodeEllipsis: any;
    public ellipsis: any = {
        PrepCode: {
            disabled: false,
            showCloseButton: true,
            isCloseButtonEnabled: true,
            isHeaderEnabled: true,
            childparams: {
                'parentMode': 'LookUp',
                'isAddNewHidden': true
            },
            component: PreparationSearchComponent
        }
    };

    public controls: Array<any> = [
        { name: 'ContractNumber', disabled: true, type: MntConst.eTypeCode, required: true, value: '', primary: true },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText, required: false, value: '', primary: true },
        { name: 'PremiseNumber', disabled: true, type: MntConst.eTypeInteger, required: true, value: '', primary: true },
        { name: 'PremiseName', disabled: true, type: MntConst.eTypeText, required: false, value: '', primary: true },
        { name: 'ProductCode', disabled: true, type: MntConst.eTypeCode, required: true, value: '', primary: true },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText, required: false, value: '', primary: true },
        { name: 'PrepCode', disabled: false, type: MntConst.eTypeCode, required: true, value: '' },
        { name: 'PrepDesc', disabled: true, type: MntConst.eTypeText, required: false, value: '' },

        { name: 'MeasureBy', disabled: true, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'PrepVolume', disabled: false, type: MntConst.eTypeDecimal2, required: true, value: '' },
        { name: 'PrepValue', disabled: false, type: MntConst.eTypeCurrency, required: true, value: '' },
        { name: 'MixRatio', disabled: false, type: MntConst.eTypeDecimal6, required: false, value: '' },
        { name: 'EPANumber', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'PrepUsedText', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'PrepBatchNumber', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'AreaCovered', disabled: false, type: MntConst.eTypeDecimal2, required: false, value: '0.00' },
        { name: 'Temperature', disabled: false, type: MntConst.eTypeInteger, required: false, value: '0' },
        { name: 'Humidity', disabled: false, type: MntConst.eTypeInteger, required: false, value: '0' },
        { name: 'WindSpeed', disabled: false, type: MntConst.eTypeInteger, required: false, value: '0' },
        { name: 'ApplicationNote', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' },
        { name: 'TargetPest', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' },

        { name: 'AreaMeasurementID', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'AreaMeasurementDesc', disabled: true, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'HardwareID', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'HardwareDesc', disabled: true, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'TreatmentMethodID', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'TreatmentMethodDesc', disabled: true, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'SkyConditionID', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'SkyConditionDesc', disabled: true, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'WindDirectionID', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'WindDirectionDesc', disabled: true, type: MntConst.eTypeText, required: false, value: '' },

        { name: 'ServiceVisitDate', disabled: false, type: MntConst.eTypeDate, required: false, value: '' },
        { name: 'PrepUsedRowID', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'ServiceVisitRowID', disabled: false, type: MntConst.eTypeText, required: false, value: '', primary: true }
    ];

    public refLookUp: any = {
        Contract: {
            table: 'Contract',
            query: { BusinessCode: '', ContractNumber: '' },
            fields: ['ContractName']
        },
        Premise: {
            table: 'Premise',
            query: { BusinessCode: '', ContractNumber: '', PremiseNumber: '' },
            fields: ['PremiseName']
        },
        Product: {
            table: 'Product',
            query: { BusinessCode: '', ProductCode: '' },
            fields: ['ProductDesc']
        },
        Prep: {
            table: 'Prep',
            query: { BusinessCode: '', PrepCode: '' },
            fields: ['PrepDesc', 'MeasureBy']
        },
        AreaMeasurement: {
            table: 'AreaMeasurement',
            query: { BusinessCode: '', AreaMeasurementID: '' },
            fields: ['AreaMeasurementDesc']
        },
        Hardware: {
            table: 'Hardware',
            query: { BusinessCode: '', HardwareID: '' },
            fields: ['HardwareDesc']
        },
        SkyCondition: {
            table: 'SkyCondition',
            query: { BusinessCode: '', SkyConditionID: '' },
            fields: ['SkyConditionDesc']
        },
        WindDirection: {
            table: 'WindDirection',
            query: { BusinessCode: '', WindDirectionID: '' },
            fields: ['WindDirectionDesc']
        },
        TreatmentMethod: {
            table: 'TreatmentMethod',
            query: { BusinessCode: '', TreatmentMethodID: '' },
            fields: ['TreatmentMethodDesc']
        }
    };

    public isInit: boolean = false;
    public isUpdateEnabled: boolean = true;
    public cachedData: any = {};
    public queryParams: any;
    public pageMode: string = '';
    public pageSize: number = 10;
    public curPage: number = 1;
    public totalRecords: number = 10;
    public promptContent: string = MessageConstant.Message.ConfirmRecord;
    public promptContentDel: string = MessageConstant.Message.DeleteRecord;
    public modalConfig: any = { backdrop: 'static', keyboard: true };

    public xhr: HttpService;
    public xhrParams: any = {
        module: 'preps',
        method: 'service-delivery/maintenance',
        operation: 'Service/iCABSSePrepUsedMaintenance'
    };

    public pageId: string = '';
    public riTab: RiTab;
    public tab: any = {
        tab1: { id: 'grdGeneral', name: 'General', visible: true, active: true },
        tab2: { id: 'grdTreatment', name: 'Treatment', visible: true, active: false },
        tab3: { id: 'grdConditions', name: 'Conditions', visible: true, active: false },
        tab4: { id: 'grdNotes', name: 'Notes', visible: true, active: false },
        tab5: { id: 'grdTargetedPests', name: 'Targeted Pests', visible: true, active: false }
    };

    public dropDown: any = {
        menu: []
    };
    private isFromAddeNew: boolean = false;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPREPUSEDMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Preparation Used Maintenance';
        this.setURLQueryParameters(this);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterViewInit(): void {
        this.isInit = true;
        this.init();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public getURLQueryParameters(param: any): void {
        this.queryParams = param;
        this.isRequesting = true;
        if (this.isInit) { this.init(); }
    }
    public canDeactivate(): Observable<boolean> {
        return super.canDeactivate();
    }

    public init(): void {
        this.parentMode = this.riExchange.getParentMode();
        this.riTab = new RiTab(this.tab, this.utils);
        this.tab = this.riTab.tabObject;
        this.uiForm.reset();
        this.onLoad();
    }
    public onLoad(isUpdated?: boolean): any {
        switch (this.parentMode) {
            case 'Search':
            case 'SearchAdd':
            case 'ServiceVisit':
            default:
                this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
                this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
                this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
                this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
                this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
                this.setControlValue('ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));

                if (!isUpdated && !this.isFromAddeNew) {
                    this.setControlValue('PrepCode', this.riExchange.getParentHTMLValue('PrepCode'));
                    this.setControlValue('PrepUsedRowID', this.riExchange.getParentHTMLValue('PrepUsedRowID'));
                }

                let tempRowID: string = this.riExchange.getParentHTMLValue('ServiceVisit');
                if (!tempRowID) {
                    tempRowID = this.riExchange.getParentHTMLValue('ServiceVisitRowID');
                }
                this.setControlValue('ServiceVisitRowID', tempRowID);
                this.setControlValue('ServiceVisitDate', this.riExchange.getParentHTMLValue('ServiceVisitDate'));
                break;
        }

        if (!this.isFromAddeNew) {
            switch (this.parentMode) {
                case 'ServiceVisit':
                case 'SearchAdd':
                    this.pageMode = MntConst.eModeAdd;
                    this.PrepCodeEllipsis.openModal();
                    break;
                default:
                    this.pageMode = MntConst.eModeUpdate;
            }
        } else {
            this.pageMode = MntConst.eModeAdd;
            this.ellipsis.PrepCode.childparams.isAddNewHidden = true;
            this.PrepCodeEllipsis.openModal();
        }

        if (this.pageMode === MntConst.eModeUpdate) {
            this.ellipsis.PrepCode.childparams.isAddNewHidden = false;
            this.riExchange.riInputElement.Disable(this.uiForm, 'PrepCode');
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '0');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['PrepUsedRowID'] = this.getControlValue('PrepUsedRowID');
            formData['ServiceVisitRowID'] = this.getControlValue('ServiceVisitRowID');
            formData['ContractNumber'] = this.getControlValue('ContractNumber');
            formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
            formData['ProductCode'] = this.getControlValue('ProductCode');
            formData['PrepCode'] = this.getControlValue('PrepCode');
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.cachedData = data;
                    this.populateForm(this.cachedData);
                    this.doLookup();
                }
            });
        } else {
            this.riExchange.riInputElement.Enable(this.uiForm, 'PrepCode');
            this.doLookup();
        }

        this.riGrid.HighlightBar = true;
        this.buildGrid();

        this.riTab.TabFocus(1);
        this.isRequesting = false;
        this.isFromAddeNew = false;
    }
    public populateForm(data: any): void {
        for (let i in data) {
            if (data.hasOwnProperty(i)) {
                this.setControlValue(i, data[i]);
            }
        }
    }
    public doLookup(): any {
        this.riTab.TabsToNormal();
        let lookupIP1: Array<any> = [];
        this.refLookUp.Contract.query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp.Contract.query.ContractNumber = this.getControlValue('ContractNumber');
        lookupIP1.push(this.refLookUp.Contract);

        this.refLookUp.Premise.query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp.Premise.query.ContractNumber = this.getControlValue('ContractNumber');
        this.refLookUp.Premise.query.PremiseNumber = this.getControlValue('PremiseNumber');
        lookupIP1.push(this.refLookUp.Premise);

        this.refLookUp.Product.query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp.Product.query.ProductCode = this.getControlValue('ProductCode');
        lookupIP1.push(this.refLookUp.Product);

        this.LookUp.lookUpPromise(lookupIP1).then((data) => {
            if (data) {
                if (this.refLookUp.Contract.query.ContractNumber) this.utils.setLookupValue(this, this.refLookUp.Contract.fields, data[0]);
                if (this.refLookUp.Premise.query.PremiseNumber) this.utils.setLookupValue(this, this.refLookUp.Premise.fields, data[1]);
                if (this.refLookUp.Product.query.ProductCode) this.utils.setLookupValue(this, this.refLookUp.Product.fields, data[2]);
            }
        });

        if (this.pageMode === MntConst.eModeUpdate) {
            let lookupIP2: Array<any> = [];
            this.refLookUp.Prep.query.BusinessCode = this.utils.getBusinessCode();
            this.refLookUp.Prep.query.PrepCode = this.getControlValue('PrepCode');
            lookupIP2.push(this.refLookUp.Prep);

            this.refLookUp.AreaMeasurement.query.BusinessCode = this.utils.getBusinessCode();
            this.refLookUp.AreaMeasurement.query.AreaMeasurementID = this.getControlValue('AreaMeasurementID');
            lookupIP2.push(this.refLookUp.AreaMeasurement);

            this.refLookUp.Hardware.query.BusinessCode = this.utils.getBusinessCode();
            this.refLookUp.Hardware.query.HardwareID = this.getControlValue('HardwareID');
            lookupIP2.push(this.refLookUp.Hardware);

            this.LookUp.lookUpPromise(lookupIP2).then((data) => {
                if (data) {
                    if (this.refLookUp.Prep.query.PrepCode) this.utils.setLookupValue(this, this.refLookUp.Prep.fields, data[0]);
                    if (this.refLookUp.AreaMeasurement.query.AreaMeasurementID) this.utils.setLookupValue(this, this.refLookUp.AreaMeasurement.fields, data[1]);
                    if (this.refLookUp.Hardware.query.HardwareID) this.utils.setLookupValue(this, this.refLookUp.Hardware.fields, data[2]);
                }
            });

            let lookupIP3: Array<any> = [];
            this.refLookUp.SkyCondition.query.BusinessCode = this.utils.getBusinessCode();
            this.refLookUp.SkyCondition.query.SkyConditionID = this.getControlValue('SkyConditionID');
            lookupIP3.push(this.refLookUp.SkyCondition);

            this.refLookUp.WindDirection.query.BusinessCode = this.utils.getBusinessCode();
            this.refLookUp.WindDirection.query.WindDirectionID = this.getControlValue('WindDirectionID');
            lookupIP3.push(this.refLookUp.WindDirection);

            this.refLookUp.TreatmentMethod.query.BusinessCode = this.utils.getBusinessCode();
            this.refLookUp.TreatmentMethod.query.TreatmentMethodID = this.getControlValue('TreatmentMethodID');
            lookupIP3.push(this.refLookUp.TreatmentMethod);

            this.LookUp.lookUpPromise(lookupIP3).then((data) => {
                if (data) {
                    if (this.refLookUp.SkyCondition.query.SkyConditionID) this.utils.setLookupValue(this, this.refLookUp.SkyCondition.fields, data[0]);
                    if (this.refLookUp.WindDirection.query.WindDirectionID) this.utils.setLookupValue(this, this.refLookUp.WindDirection.fields, data[1]);
                    if (this.refLookUp.TreatmentMethod.query.TreatmentMethodID) this.utils.setLookupValue(this, this.refLookUp.TreatmentMethod.fields, data[2]);
                }
            });
        }
    }

    //Grid Functionalities - Start
    public buildGrid(): any {
        this.riGrid.Clear();
        this.riGrid.AddColumn('ProductDetailCode', 'ProductDetailCodeInp', 'ProductDetailCodeInp', MntConst.eTypeCode, 6, true);
        this.riGrid.AddColumn('ProductDetailDesc', 'ProductDetailDescInp', 'ProductDetailDescInp', MntConst.eTypeText, 40, false);
        this.riGrid.Complete();
        this.riGridExecute();
    }
    public riGridExecute(): any {
        if (this.getControlValue('PrepUsedRowID')) {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '2');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            search.set(this.serviceConstants.GridMode, '0');
            search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
            search.set(this.serviceConstants.GridPageSize, this.pageSize.toString());
            search.set(this.serviceConstants.GridPageCurrent, this.curPage.toString());
            search.set('riSortOrder', this.riGrid.SortOrder);
            search.set('riCacheRefresh', 'true');
            search.set('PrepUsedRowID', this.getControlValue('PrepUsedRowID'));
            search.set('TargetPest', this.getControlValue('TargetPest'));

            this.httpService.xhrGet(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.curPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageSize : 1;
                    this.riGrid.Execute(data);
                }
            });
        }
    }
    public riGridBodyDblClick(e: Event): void {
        let addDisplayTime: number = 0, objSrc: any, objTR: any, objSrcName: string;
        objSrc = e.srcElement;
        objTR = objSrc.parentElement;
        objSrcName = this.riGrid.CurrentColumnName;
        switch (objSrcName) {
            case 'ProductDetailCode':
                this.logger.log('Debug:', objSrcName, this.riGrid.Details.GetValue('ProductDetailCode'), this.riGrid.Details.GetAttribute('ProductDetailCode', 'rowID'));
                //The navigation to the below page is out of scope
                // this.navigate('Select', 'Service/iCABSeTargetedPestsMaintenance.htm', { ProductDetailCode: this.riGrid.Details.GetValue('ProductDetailCode'), RowID: this.riGrid.Details.GetAttribute('ProductDetailCode', 'rowID')});
                break;
        }
    }
    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.riGridExecute();
    }
    public getCurrentPage(data: any): void {
        this.curPage = data.value;
        this.riGrid.RefreshRequired();
        this.riGridExecute();
    }
    //Grid Functionalities - End

    //Ellipsis - Start
    public ellipsisRespHandler(data: any, ellipsisID: string): any {
        switch (ellipsisID) {
            case 'PrepCode':
                if (data.addMode) {
                    this.isInit = true;
                    this.isFromAddeNew = true;
                    this.init();
                    return;
                }
                this.setControlValue('PrepCode', data.PrepCode);
                this.setControlValue('PrepDesc', data.PrepDesc);
                this.setControlValue('MeasureBy', data.MeasureBy);
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'PrepCode');
                break;
        }
    }
    //Ellipsis - End

    public getDropdownDesc(refName: string, id: string): void {
        let lookupIP: Array<any> = [];
        this.refLookUp[refName].query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp[refName].query[id] = this.getControlValue(id);
        lookupIP.push(this.refLookUp[refName]);
        this.LookUp.lookUpPromise(lookupIP).then((data) => {
            if (data) {
                let tempVal: string = this.utils.setLookupValue(this, this.refLookUp[refName].fields, data[0]);
                if (tempVal === '') {
                    this.riExchange.riInputElement.markAsError(this.uiForm, id);
                    this.setControlValue(this.refLookUp[refName].fields[0], '');
                }
            }
        });
    }
    public onChangeAreaMeasurementID(): any {
        if (this.getControlValue('AreaMeasurementID') !== '') {
            this.getDropdownDesc('AreaMeasurement', 'AreaMeasurementID');
        } else {
            this.riExchange.riInputElement.isCorrect(this.uiForm, 'AreaMeasurementID');
            this.setControlValue('AreaMeasurementDesc', '');
        }
    }
    public onChangeHardwareID(): any {
        if (this.getControlValue('HardwareID') !== '') {
            this.getDropdownDesc('Hardware', 'HardwareID');
        } else {
            this.riExchange.riInputElement.isCorrect(this.uiForm, 'HardwareID');
            this.setControlValue('HardwareDesc', '');
        }
    }
    public onChangeSkyConditionID(): any {
        if (this.getControlValue('SkyConditionID') !== '') {
            this.getDropdownDesc('SkyCondition', 'SkyConditionID');
        } else {
            this.riExchange.riInputElement.isCorrect(this.uiForm, 'SkyConditionID');
            this.setControlValue('SkyConditionDesc', '');
        }
    }
    public onChangeWindDirectionID(): any {
        if (this.getControlValue('WindDirectionID') !== '') {
            this.getDropdownDesc('WindDirection', 'WindDirectionID');
        } else {
            this.riExchange.riInputElement.isCorrect(this.uiForm, 'WindDirectionID');
            this.setControlValue('WindDirectionDesc', '');
        }
    }
    public onChangeTreatmentMethodID(): any {
        if (this.getControlValue('TreatmentMethodID') !== '') {
            this.getDropdownDesc('TreatmentMethod', 'TreatmentMethodID');
        } else {
            this.riExchange.riInputElement.isCorrect(this.uiForm, 'TreatmentMethodID');
            this.setControlValue('TreatmentMethodDesc', '');
        }
    }

    public riExchangeCBORequest(): any {
        if (this.riExchange.riInputElement.HasChanged(this.uiForm, 'PrepVolume')
            && this.getControlValue('PrepVolume') !== ''
            && !this.riExchange.riInputElement.isError(this.uiForm, 'PrepVolume')) {
            this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'PrepVolume');
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Function'] = 'GetPrepValue';
            formData['ServiceVisitRowID'] = this.getControlValue('ServiceVisitRowID');
            formData['PrepCode'] = this.getControlValue('PrepCode');
            formData['PrepVolume'] = this.getControlValue('PrepVolume');
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('PrepValue', this.globalize.formatCurrencyToLocaleFormat(data.PrepValue));
                }
            });
        }
        if (this.riExchange.riInputElement.HasChanged(this.uiForm, 'PrepCode') && this.getControlValue('PrepCode') !== '') {
            this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'PrepCode');
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Function'] = 'PrepCodeChanged';
            formData['PrepCode'] = this.getControlValue('PrepCode');
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    for (let i in data) {
                        if (data.hasOwnProperty(i)) {
                            this.setControlValue(i, data[i]);
                        }
                    }
                }
            });
        }
    }

    public onkeypressApplicationNote(event: any): any {
        let len: number = this.getControlValue('ApplicationNote') ? this.getControlValue('ApplicationNote').length : 0;
        if (len > 500) {
            this.trimApplicationNote();
        }
    }
    public onblurApplicationNote(): any {
        let len: number = this.getControlValue('ApplicationNote') ? this.getControlValue('ApplicationNote').length : 0;
        if (len > 500) {
            let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.noteGreaterthan500);
            modalVO.closeCallback = this.trimApplicationNote.bind(this);
            this.modalAdvService.emitMessage(modalVO);
        }
    }
    public trimApplicationNote(): void {
        let note: string = this.getControlValue('ApplicationNote');
        note = note.substring(0, 500);
        this.setControlValue('ApplicationNote', note);
    }

    //Buttons
    public save(): void {
        this.utils.highlightTabs();
        this.riExchange.validateForm(this.uiForm);

        for (let i in this.dropDown) {
            if (this.dropDown.hasOwnProperty(i)) {
                if (this.dropDown[i].hasOwnProperty('isRequired') && this.dropDown[i].isRequired) {
                    this.dropDown[i].triggerValidate = true;
                }
            }
        }

        if (this.uiForm.status === 'VALID') {
            if (this.pageMode === MntConst.eModeAdd) {
                this.add();
            } else {
                this.update();
            }
        } else {
            for (let control in this.uiForm.controls) {
                if (this.uiForm.controls[control].invalid) {
                    this.logger.log('   >> Invalid:', control, this.uiForm.controls[control].errors, this.uiForm.controls[control]);
                }
            }
        }
    }
    public cancel(): void {
        if (this.cachedData.hasOwnProperty('PrepCode') && this.cachedData['PrepCode']) {
            this.pageMode = MntConst.eModeUpdate;
        }
        if (this.pageMode === MntConst.eModeUpdate) {
            this.disableControl('PrepCode', true);
            this.populateForm(this.cachedData);
            this.doLookup();
            this.ellipsis.PrepCode.childparams.isAddNewHidden = false;
        } else {
            for (let i = 0; i < this.controls.length; i++) {
                let ctrl = this.controls[i];
                if (ctrl.hasOwnProperty('primary')) {
                    if (!ctrl['primary']) this.setControlValue(ctrl.name, '');
                } else {
                    this.setControlValue(ctrl.name, '');
                }
            }
            this.ellipsis.PrepCode.childparams.isAddNewHidden = true;
            this.PrepCodeEllipsis.openModal();
        }
        this.markAsPristine();
    }
    public add(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(this.promptContent, null, this.addXHR.bind(this), null));
    }
    public update(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(this.promptContent, null, this.updateXHR.bind(this), null));
    }
    public delete(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(this.promptContentDel, null, this.deleteXHR.bind(this), null));
    }

    public createXHRObj(action: number): any {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, action.toString());
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        let len = this.controls.length;
        for (let i = 0; i < len; i++) {
            let ctrlName: string = this.controls[i].name;
            let ctrlValue: any = this.getControlValue(this.controls[i].name, true);
            if (this.controls[i].type === MntConst.eTypeCheckBox) {
                ctrlValue = this.utils.convertCheckboxValueToRequestValue(ctrlValue);
            }
            formData[ctrlName] = ctrlValue;
        }

        return { search: search, formData: formData };
    }
    public addXHR(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let obj: any = this.createXHRObj(1);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, obj.search, obj.formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.markAsPristine();
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.logger.log('Add:', data);
                this.setControlValue('PrepCode', this.getControlValue('PrepCode'));
                this.setControlValue('PrepUsedRowID', data.PrepUsedRow);
                this.setControlValue('PrepUsedRowID', data.PrepUsed);
                this.ellipsis.PrepCode.childparams.isAddNewHidden = false;

                let urlStrArr = this.location.path().split('?');
                let urlStr = urlStrArr[0];
                let qParams = 'parentMode=Search'
                    + '&ContractNumber=' + this.getControlValue('ContractNumber')
                    + '&PremiseNumber=' + this.getControlValue('PremiseNumber')
                    + '&ProductCode=' + this.getControlValue('ProductCode')
                    + '&ServiceVisitRowID=' + this.getControlValue('ServiceVisitRowID')
                    + '&PrepUsedRowID=' + this.getControlValue('PrepUsedRowID')
                    + '&PrepCode=' + this.getControlValue('PrepCode');
                this.location.replaceState(urlStr, qParams);

                let tempModal: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.SavedSuccessfully);
                tempModal.closeCallback = this.defaultToUpdateMode.bind(this);
                this.modalAdvService.emitMessage(tempModal);
            }
        });
    }
    public updateXHR(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let obj: any = this.createXHRObj(2);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, obj.search, obj.formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.markAsPristine();
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.cachedData = obj.formData;
                this.logger.log('Update:', data);
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
            }
        });
    }
    public deleteXHR(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '3');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        formData['RowID'] = this.getControlValue('PrepUsedRowID');

        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.markAsPristine();
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.location.back();
            }
        });

    }
    public defaultToUpdateMode(): void {
        this.pageMode = MntConst.eModeUpdate;
        this.parentMode = 'Search';
        this.onLoad(true);
    }

    public onClickAddTargetPest(): any {
        this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.PageNotDeveloped)); //Page not in list of 577 screens
        // this.navigate('Add', 'Service/iCABSeTargetedPestsMaintenance.htm');
    }

    //Generic Functions
    public focusSave(obj: any): void {
        this.riTab.focusNextTab(obj);
    }
    public markAsPristine(): void {
        for (let i = 0; i < this.controls.length; i++) {
            this.uiForm.controls[this.controls[i].name].markAsPristine();
        }
        this.formPristine();
    }
}
