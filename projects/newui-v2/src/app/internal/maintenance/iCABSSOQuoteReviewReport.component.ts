import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Injectable } from '@angular/core';

import { Utils } from '../../../shared/services/utility';
import { ServiceConstants } from '../../../shared/constants/service.constants';
import { HttpService } from '../../../shared/services/http-service';
import { ModalAdvService } from '../../../shared/components/modal-adv/modal-adv.service';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';

@Injectable()
export class SOQuoteReviewReportComponent {
    constructor(
        private utils: Utils,
        private xhr: HttpService,
        private serviceConstants: ServiceConstants,
        private modalAdvService: ModalAdvService
    ) { }

    public loadPrintContent(mode: string, dlContractROWID: string): void {
        let searchParams: QueryParams = new QueryParams();
        searchParams.set(this.serviceConstants.Action, '0');
        searchParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        searchParams.set('Mode', mode);
        searchParams.set('dlContractROWID', dlContractROWID);

        let xhrParams: any = {
            module: 'advantage',
            method: 'prospect-to-contract/maintenance',
            operation: 'Sales/iCABSSOQuoteReviewReport',
            search: searchParams
        };

        this.xhr.xhrGet(xhrParams.method, xhrParams.module, xhrParams.operation, xhrParams.search).then((data) => {
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                window.open(data.url, '_blank');
            }
        });
    }
}
