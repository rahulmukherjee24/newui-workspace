import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy, Renderer } from '@angular/core';
import { Subscription } from 'rxjs';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { BCompanySearchComponent } from './../search/iCABSBCompanySearch';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { IGenericEllipsisControl } from '@shared/components/ellipsis-generic/ellipsis-generic';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { MntConst } from './../../../shared/services/riMaintenancehelper';

@Component({
    templateUrl: 'iCABSBCompanyMaintenance.html'
})

export class CompanyMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('companyDropDown') companyDropDown: BCompanySearchComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('mandatePrefix') public mandatePrefix;
    @ViewChild('defaultCompanyInd') public defaultCompanyInd;
    @ViewChild('companyAddressLine3') public companyAddressLine3;
    @ViewChild('companyAddressLine4') public companyAddressLine4;
    @ViewChild('companyAddressLine5') public companyAddressLine5;
    @ViewChild('creditorID') public creditorID;
    @ViewChild('minimumCompanyInvoiceNumber') public minimumCompanyInvoiceNumber;
    @ViewChild('formCompanyCode') public formCompanyCode;
    @ViewChild('formCompanyDesc') public formCompanyDesc;
    private promptTitle: string = '';
    private queryParams: any = {
        operation: 'Business/iCABSBCompanyMaintenance',
        module: 'structure',
        method: 'it-functions/admin'
    };
    private disableFields: string = '';
    private isAfterSave: boolean = false;
    private pageMessageModal: ICabsModalVO;
    private taxSubscription: Subscription;
    private taxCodeSubscription: Subscription;
    public isFirstItemSelected: boolean = true;
    public isInsideAdd: boolean = false;
    public isBtnAdd: boolean = true;
    public isBtnSave: boolean = false;
    public isBtnDelete: boolean = false;
    public isBtnCancel: boolean = false;
    public isMakemandatory: boolean = false;
    public isMakeDisabled: boolean = true;
    public pageId: string = '';
    public companyInputParams: any = {};
    public isCompanyAddressLine3Block: boolean = true;
    public isCompanyLockBoxAddressLine3Block: boolean = true;
    public controls: any = [
        { name: 'CompanyCode', type: MntConst.eTypeCode, required: true },
        { name: 'CompanyDesc', type: MntConst.eTypeText, required: true },
        { name: 'DefaultCompanyInd', type: MntConst.eTypeCheckBox },
        { name: 'AccountSortCode', type: MntConst.eTypeText },
        { name: 'DomesticBankAccountNumber', type: MntConst.eTypeTextFree },
        { name: 'InternationalBankAccountNumber', type: MntConst.eTypeText },
        { name: 'SeparateCompanyInvoices', type: MntConst.eTypeTextFree },
        { name: 'AllowMixedAccountsIND', type: MntConst.eTypeCheckBox },
        { name: 'UseDefaultCompanyInvoiceRange', type: MntConst.eTypeCheckBox },
        { name: 'CompanyAddressLine1', type: MntConst.eTypeTextFree },
        { name: 'CompanyAddressLine2', type: MntConst.eTypeTextFree },
        { name: 'CompanyAddressLine3', type: MntConst.eTypeTextFree },
        { name: 'CompanyAddressLine4', type: MntConst.eTypeTextFree },
        { name: 'CompanyAddressLine5', type: MntConst.eTypeTextFree },
        { name: 'CompanyPostCode', type: MntConst.eTypeText },
        { name: 'CompanyTelephone', type: MntConst.eTypeText },
        { name: 'CompanyFax', type: MntConst.eTypeText },
        { name: 'CompanyEmail', type: MntConst.eTypeTextFree },
        { name: 'CompanyWebsite', type: MntConst.eTypeTextFree },
        { name: 'CompanyRepresentativeName', type: MntConst.eTypeText },
        { name: 'CompanyContactName', type: MntConst.eTypeText },
        { name: 'CompanyContactDepartment', type: MntConst.eTypeText },
        { name: 'CompanyGroupName', type: MntConst.eTypeTextFree },
        { name: 'CompanyRegistrationTown', type: MntConst.eTypeTextFree },
        { name: 'CompanyRegistrationNumber', type: MntConst.eTypeTextFree },
        { name: 'CompanyVATNumber', type: MntConst.eTypeTextFree },
        { name: 'CompanyTypeText', type: MntConst.eTypeTextFree },
        { name: 'CompanyActivityText', type: MntConst.eTypeText },
        { name: 'CompanyAgreementText', type: MntConst.eTypeTextFree },
        { name: 'CreditorID', type: MntConst.eTypeTextFree, required: true },
        { name: 'MandatePrefix', type: MntConst.eTypeTextFree },
        { name: 'NAVCompanyCode', type: MntConst.eTypeTextFree },
        { name: 'MinimumCompanyInvoiceNumber', type: MntConst.eTypeInteger, required: true },
        { name: 'InvoiceMessageLine1', type: MntConst.eTypeTextFree },
        { name: 'InvoiceMessageLine2', type: MntConst.eTypeTextFree },
        { name: 'InvoiceMessageLine3', type: MntConst.eTypeTextFree },
        { name: 'InvoiceMessageLine4', type: MntConst.eTypeTextFree },
        { name: 'InvoiceMessageLine5', type: MntConst.eTypeTextFree },
        { name: 'InvoiceMessageLine6', type: MntConst.eTypeTextFree },
        { name: 'InvoiceMessageLine7', type: MntConst.eTypeTextFree },
        { name: 'InvoiceMessageLine8', type: MntConst.eTypeTextFree },
        { name: 'InvoiceMessageLine9', type: MntConst.eTypeTextFree },
        { name: 'InvoiceMessageLine10', type: MntConst.eTypeTextFree },
        { name: 'CompanyLockBoxAddressLine1', type: MntConst.eTypeTextFree },
        { name: 'CompanyLockBoxAddressLine2', type: MntConst.eTypeTextFree },
        { name: 'CompanyLockBoxAddressLine3', type: MntConst.eTypeTextFree },
        { name: 'CompanyLockBoxAddressLine4', type: MntConst.eTypeTextFree },
        { name: 'CompanyLockBoxAddressLine5', type: MntConst.eTypeTextFree },
        { name: 'CompanyLockBoxPostCode', type: MntConst.eTypeTextFree },

        { name: 'AcquisitionDate', type: MntConst.eTypeDate },
        { name: 'APICode' },
        { name: 'APICodeDesc' },
        { name: 'TaxCode', type: MntConst.eTypeText },
        { name: 'TaxCodeDesc', type: MntConst.eTypeText }

    ];

    public dropdownConfig: any = {
        apiCodeSearch: {
            active: { id: '', text: '' },
            isDisabled: false,
            inputParams: {
                'parentMode': 'LookUp',
                'businessCode': '',
                'countryCode': ''
            }
        }
    };

    public companyDefault: Object = {
        id: '',
        text: ''
    };

    public inputParams: any = {
        'parentMode': 'LookUp',
        'businessCode': '',
        'countryCode': ''
    };
    public TaxCodeSearchConfig: IGenericEllipsisControl = {
        autoOpen: false,
        ellipsisTitle: 'Tax Code Search',
        configParams: {
            table: '',
            shouldShowAdd: false,
            parentMode: 'LookUp'
        },
        httpConfig: {
            operation: 'System/iCABSSTaxCodeSearch',
            module: 'tax',
            method: 'bill-to-cash/search'
        },
        tableColumn: [
            { title: 'Tax Code', name: 'TaxCode', type: MntConst.eTypeCode },
            { title: 'Description', name: 'TaxCodeDesc', type: MntConst.eTypeText },
            { title: 'System Default', name: 'TaxCodeDefaultInd', type: MntConst.eTypeCheckBox }
        ],
        disable: false
    };

    public uiDisplay: any = {
        tab: {
            tab1: { visible: true, active: true },
            tab2: { visible: true, active: false },
            tab3: { visible: true, active: false },
            tab4: { visible: true, active: false },
            tab5: { visible: true, active: false }
        }
    };

    constructor(injector: Injector, private renderer: Renderer) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBCOMPANYMAINTENANCE;
        this.pageTitle = this.browserTitle = 'Company Maintenance';
        this.companyInputParams[this.serviceConstants.CountryCode] = this.utils.getCountryCode();
        this.companyInputParams[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        this.companyInputParams['parentMode'] = 'LookUp';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.formMode = this.c_s_MODE_UPDATE;
        this.getSysCharDtetails();
    }

    ngOnDestroy(): void {
        if (this.taxSubscription) {
            this.taxSubscription.unsubscribe();
        }
        if (this.taxCodeSubscription) {
            this.taxCodeSubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }

    /**
     * This function validates the form for required fields
     */
    private beforeSave(): boolean {
        for (let i in this.uiForm.controls) {
            if (this.uiForm.controls.hasOwnProperty(i)) {
                this.uiForm.controls[i].markAsTouched();
                if (!this.uiForm.controls[i].enabled) {
                    this.uiForm.controls[i].clearValidators();
                }
                this.uiForm.controls[i].updateValueAndValidity();
            }
        }
        this.uiForm.updateValueAndValidity();
        return this.uiForm.valid;
    }

    private getSysCharDtetails(): any {
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharDisableFirstCapitalOnAddressContact,
            this.sysCharConstants.SystemCharEnableAddressLine3,
            this.sysCharConstants.SystemCharAddressLine4Required,
            this.sysCharConstants.SystemCharAddressLine5Required,
            this.sysCharConstants.SystemCharEnableSEPAFinanceMandate
        ];

        let sysCharIP: any = {
            module: this.queryParams.module,
            operation: this.queryParams.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };

        this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            let record = data.records;
            this.pageParams.vSCCapitalFirstLtr = record[0]['Required'];
            this.pageParams.vSCEnableAddressLine3 = record[1]['Required'];
            this.pageParams.vSCAddressLine3Logical = record[1]['Logical'];
            this.pageParams.vSCAddressLine4Required = record[2]['Required'];
            this.pageParams.vSCAddressLine5Required = record[3]['Required'];
            this.pageParams.vSCMandatePrefixRequired = record[4]['Required'];
            if (!this.pageParams.vSCEnableAddressLine3) {
                if (this.disableFields !== '') {
                    this.disableFields = this.disableFields + ',' + 'DisableAddressLine3';
                } else {
                    this.disableFields = this.disableFields + '' + 'DisableAddressLine3';
                }

            }
            this.windowOnLoad();
        });
    }
    /**
     * This function gets called when user confirms delete operation
     * @param instance
     */
    private proceedDelete(): void {
        if (this.formMode === this.c_s_MODE_UPDATE) {
            this.isFirstItemSelected = true;
            this.formMode = 'DELETE';
            let postSearchParams: any = this.getURLSearchParamObject();
            postSearchParams.set(this.serviceConstants.Action, '3');
            let postParams: any = {};
            postParams.Table = 'Company';
            postParams.ROWID = this.pageParams.rowID;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if ((data['hasError'])) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully));
                            this.uiForm.reset();
                            this.companyInputParams['countryCode'] = this.utils.getCountryCode();
                            this.companyInputParams['businessCode'] = this.utils.getBusinessCode();
                            this.companyInputParams['parentMode'] = 'LookUp';
                            this.companyDropDown.getData();
                            this.formMode = 'DELETE';
                            this.checkData(this.companyDropDown.requestdata);
                        }
                    },
                    (error) => {
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        }
    }

    /**
     * This function gets called when user saves a company detail; this triggers the API request
     * @param instance
     */
    private confirmCallBack(): void {
        let postSearchParams: any = this.getURLSearchParamObject();
        if (this.formMode === this.c_s_MODE_ADD) {
            postSearchParams.set(this.serviceConstants.Action, '1');
        }
        else {
            postSearchParams.set(this.serviceConstants.Action, '2');
        }
        let postParams: any = {};
        postParams.Table = 'Company';
        postParams.ROWID = this.pageParams.rowID;
        postParams.CompanyDesc = this.getControlValue('CompanyDesc');
        postParams.CompanyCode = this.getControlValue('CompanyCode');
        postParams.DefaultCompanyInd = this.getControlValue('DefaultCompanyInd');
        postParams.APICode = this.getControlValue('APICode') || '?';
        postParams.AcquisitionDate = this.getControlValue('AcquisitionDate');
        postParams.SeparateCompanyInvoices = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('SeparateCompanyInvoices'));
        postParams.AccountSortCode = this.getControlValue('AccountSortCode');
        postParams.AllowMixedAccountsIND = this.getControlValue('AllowMixedAccountsIND');
        postParams.UseDefaultCompanyInvoiceRange = this.getControlValue('UseDefaultCompanyInvoiceRange');
        postParams.CompanyAddressLine1 = this.getControlValue('CompanyAddressLine1');
        postParams.CompanyAddressLine2 = this.getControlValue('CompanyAddressLine2');
        postParams.CompanyLockBoxAddressLine1 = this.getControlValue('CompanyLockBoxAddressLine1');
        postParams.CompanyLockBoxAddressLine2 = this.getControlValue('CompanyLockBoxAddressLine2');
        postParams.CompanyLockBoxAddressLine3 = this.getControlValue('CompanyLockBoxAddressLine3');
        postParams.CompanyLockBoxAddressLine4 = this.getControlValue('CompanyLockBoxAddressLine4');
        postParams.CompanyLockBoxAddressLine5 = this.getControlValue('CompanyLockBoxAddressLine5');
        postParams.CompanyLockBoxPostcode = this.getControlValue('CompanyLockBoxPostCode');
        postParams.CompanyAddressLine3 = this.getControlValue('CompanyAddressLine3');
        postParams.CompanyAddressLine4 = this.getControlValue('CompanyAddressLine4');
        postParams.CompanyAddressLine5 = this.getControlValue('CompanyAddressLine5');
        postParams.CompanyPostCode = this.getControlValue('CompanyPostCode');
        postParams.CompanyTelephone = this.getControlValue('CompanyTelephone');
        postParams.CompanyFax = this.getControlValue('CompanyFax');
        postParams.CompanyEmail = this.getControlValue('CompanyEmail');
        postParams.CompanyWebsite = this.getControlValue('CompanyWebsite');
        postParams.CompanyRepresentativeName = this.getControlValue('CompanyRepresentativeName');
        postParams.CompanyContactName = this.getControlValue('CompanyContactName');
        postParams.CompanyContactDepartment = this.getControlValue('CompanyContactDepartment');
        postParams.CompanyGroupName = this.getControlValue('CompanyGroupName');
        postParams.MinimumCompanyInvoiceNumber = this.getControlValue('MinimumCompanyInvoiceNumber');
        postParams.CompanyAgreementText = this.getControlValue('CompanyAgreementText');
        postParams.CompanyRegistrationNumber = this.getControlValue('CompanyRegistrationNumber');
        postParams.CompanyRegistrationTown = this.getControlValue('CompanyRegistrationTown');
        postParams.CompanyActivityText = this.getControlValue('CompanyActivityText');
        postParams.CompanyTypeText = this.getControlValue('CompanyTypeText');
        postParams.CompanyVATNumber = this.getControlValue('CompanyVATNumber');
        postParams.TaxCode = this.getControlValue('TaxCode');
        postParams.DomesticBankAccountNumber = this.getControlValue('DomesticBankAccountNumber');
        postParams.InternationalBankAccountNumber = this.getControlValue('InternationalBankAccountNumber');
        postParams.InvoiceMessageLine1 = this.getControlValue('InvoiceMessageLine1');
        postParams.InvoiceMessageLine2 = this.getControlValue('InvoiceMessageLine2');
        postParams.InvoiceMessageLine3 = this.getControlValue('InvoiceMessageLine3');
        postParams.InvoiceMessageLine4 = this.getControlValue('InvoiceMessageLine4');
        postParams.InvoiceMessageLine5 = this.getControlValue('InvoiceMessageLine5');
        postParams.InvoiceMessageLine6 = this.getControlValue('InvoiceMessageLine6');
        postParams.InvoiceMessageLine7 = this.getControlValue('InvoiceMessageLine7');
        postParams.InvoiceMessageLine8 = this.getControlValue('InvoiceMessageLine8');
        postParams.InvoiceMessageLine9 = this.getControlValue('InvoiceMessageLine9');
        postParams.InvoiceMessageLine10 = this.getControlValue('InvoiceMessageLine10');
        postParams.CreditorID = this.getControlValue('CreditorID');
        postParams.MandatePrefix = this.getControlValue('MandatePrefix');
        postParams.NAVCompanyCode = this.getControlValue('NAVCompanyCode');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if ((data['hasError'])) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                        this.renderTab(1);
                        this.isBtnAdd = this.isBtnDelete = true;
                        this.isInsideAdd = false;
                        this.setControlValue('CompanyDesc', this.getControlValue('CompanyDesc'));
                        this.companyInputParams['countryCode'] = this.utils.getCountryCode();
                        this.companyInputParams['businessCode'] = this.utils.getBusinessCode();
                        this.companyInputParams['parentMode'] = 'LookUp';
                        this.companyDropDown.getData();
                        this.companyDefault = {
                            id: this.getControlValue('CompanyCode'),
                            text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
                        };

                        this.isAfterSave = true;
                        this['uiForm'].markAsPristine();
                        this.isFirstItemSelected = false;
                        if (this.formMode === this.c_s_MODE_ADD) {
                            this.formMode = this.c_s_MODE_UPDATE;
                            let data: any = {
                                companyCode: this.getControlValue('CompanyCode'),
                                companyDesc: this.getControlValue('CompanyDesc')
                            };
                            this.onCompanyChange(data);
                        }
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private windowOnLoad(): void {
        this.setErrorStatusOfFormElement('MandatePrefix', false);
        this.setErrorStatusOfFormElement('CompanyAddressLine4', false);
        this.setErrorStatusOfFormElement('CompanyAddressLine5', false);
        this.isFirstItemSelected = true;
        if (this.pageParams.vSCEnableAddressLine3) {
            this.isCompanyAddressLine3Block = true;
            this.isCompanyLockBoxAddressLine3Block = true;
        }
        else {
            this.isCompanyAddressLine3Block = false;
            this.isCompanyLockBoxAddressLine3Block = false;
        }
        this.dropdownConfig.apiCodeSearch.active = {
            id: '',
            text: ''
        };
        this.dropdownConfig.apiCodeSearch.isDisabled = false;
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.companyDefault = {
                id: this.getControlValue('CompanyCode'),
                text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
            };
        }
    }

    /**
     * This function validates the active tab for form errors
     */
    private validateTabs(): void {
        if (this.uiDisplay['tab'].tab1.active) {
            this.utils.makeTabsRedById(['grdGeneral']);
        } else if (this.uiDisplay['tab'].tab2.active) {
            this.utils.makeTabsRedById(['grdAddress']);
        } else if (this.uiDisplay['tab'].tab3.active) {
            this.utils.makeTabsRedById(['grdRegistration']);
        } else if (this.uiDisplay['tab'].tab4.active) {
            this.utils.makeTabsRedById(['grdInvoicing']);
        } else if (this.uiDisplay['tab'].tab5.active) {
            this.utils.makeTabsRedById(['grdLockBoxAddress']);
        }
    }


    private fillAPIDesc(code: number): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let lookupDetails: Array<any> = [{
            'table': 'APICode',
            'query': {
                'BusinessCode': this.businessCode(),
                'APICode': code
            },
            'fields': ['APICodeDesc']
        }];
        this.LookUp.lookUpPromise(lookupDetails).then((data) => {
            if (data.length > 0) {
                let apiCodeData: Array<any> = data;
                if (apiCodeData[0][0]) {
                    this.setControlValue('APICodeDesc', apiCodeData[0][0].APICodeDesc);
                    this.dropdownConfig.apiCodeSearch.active = {
                        id: this.getControlValue('APICode'),
                        text: this.getControlValue('APICode') + ' - ' + this.getControlValue('APICodeDesc')
                    };
                } else {
                    this.setControlValue('APICodeDesc', '');
                    this.dropdownConfig.apiCodeSearch.active = {
                        id: '',
                        text: ''
                    };
                }
            }
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    private fillTaxCodeLookUp(code: number): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let lookupDetails: Array<any> = [{
            'table': 'TaxCode',
            'query': {
                'BusinessCode': this.businessCode(),
                'TaxCode': code
            },
            'fields': ['TaxCodeDesc']
        }];
        this.LookUp.lookUpPromise(lookupDetails).then((data) => {
            if (data.length > 0) {
                let taxCodeDescData: Array<any> = data;
                if (taxCodeDescData[0][0]) {
                    this.setControlValue('TaxCodeDesc', taxCodeDescData[0][0].TaxCodeDesc);
                }
            }
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    /**
     * This function gets called before save operation; it checks if MandatePrefix is required, if yes then it is not empty and user enters it
     */
    private riMaintenanceBeforeSave(): void {
        if (this.getControlValue('MandatePrefix') && (this.getControlValue('MandatePrefix').trim()).length !== 3) {
            this.renderTab(3);
            this.pageMessageModal = new ICabsModalVO(MessageConstant.PageSpecificMessage.prefixLength);
            this.pageMessageModal.title = MessageConstant.Message.WarningTitle;
            this.modalAdvService.emitMessage(this.pageMessageModal);
            setTimeout(() => {
                if (this.mandatePrefix) {
                    let focus = new CustomEvent('focus', { bubbles: true });
                    this.renderer.invokeElementMethod(this.mandatePrefix.nativeElement, 'focus', [focus]);
                    this.setErrorStatusOfFormElement('MandatePrefix', true);
                }
            }, 0);
        }
        else {
            if (!this.getControlValue('MinimumCompanyInvoiceNumber') && this.getControlValue('MinimumCompanyInvoiceNumber') !== 0) {
                this.renderTab(4);
                setTimeout(() => {
                    if (this.minimumCompanyInvoiceNumber) {
                        let focus = new CustomEvent('focus', { bubbles: true });
                        this.renderer.invokeElementMethod(this.minimumCompanyInvoiceNumber.nativeElement, 'focus', [focus]);
                    }
                }, 0);
                this.utils.makeTabsRed(4);
                return;
            }
            this.utils.makeTabsNormal();
            this.promptTitle = MessageConstant.Message.ConfirmRecord;
            this.modalAdvService.emitPrompt(new ICabsModalVO(this.promptTitle, null, this.confirmCallBack.bind(this)));
        }
    }

    private setErrorStatusOfFormElement(frmCtrlName: string, flag: boolean): void {
        this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, frmCtrlName, flag);
        if (flag) {
            this.riExchange.riInputElement.markAsError(this.uiForm, frmCtrlName);
        }
    }

    /**
     * This function validates form fields and displays appropriate messages
     */
    private riMaintenanceBeforeConfirm(): void {
        // Tab Mandatory Fields

        if (!this.getControlValue('CompanyAddressLine4') && this.pageParams.vSCAddressLine4Required) {
            this.setErrorStatusOfFormElement('CompanyAddressLine4', true);
            this.renderTab(2);
            setTimeout(() => {
                if (this.companyAddressLine4) {
                    let focus = new CustomEvent('focus', { bubbles: true });
                    this.renderer.invokeElementMethod(this.companyAddressLine4.nativeElement, 'focus', [focus]);
                }
            }, 0);
            this.utils.makeTabsRed(2);

            return;
        }
        if (!this.getControlValue('CompanyAddressLine5') && this.pageParams.vSCAddressLine5Required) {
            this.setErrorStatusOfFormElement('CompanyAddressLine5', true);
            this.renderTab(2);
            setTimeout(() => {
                if (this.companyAddressLine5) {
                    let focus = new CustomEvent('focus', { bubbles: true });
                    this.renderer.invokeElementMethod(this.companyAddressLine5.nativeElement, 'focus', [focus]);
                }
            }, 0);
            this.utils.makeTabsRed(2);

            return;
        }
        if (!this.getControlValue('CompanyAddressLine3') && this.pageParams.vSCAddressLine3Logical) {
            this.renderTab(2);
            setTimeout(() => {
                if (this.companyAddressLine3) {
                    let focus = new CustomEvent('focus', { bubbles: true });
                    this.renderer.invokeElementMethod(this.companyAddressLine3.nativeElement, 'focus', [focus]);
                }
            }, 0);
            this.utils.makeTabsRed(2);

            return;
        }

        if (!this.getControlValue('CreditorID')) {
            this.renderTab(3);
            setTimeout(() => {
                if (this.creditorID) {
                    let focus = new CustomEvent('focus', { bubbles: true });
                    this.renderer.invokeElementMethod(this.creditorID.nativeElement, 'focus', [focus]);
                }
            }, 0);
            this.utils.makeTabsRed(3);

            return;
        }
        if (!this.getControlValue('MandatePrefix') && this.pageParams.vSCMandatePrefixRequired) {
            this.renderTab(3);
            setTimeout(() => {
                if (this.mandatePrefix) {
                    let focus = new CustomEvent('focus', { bubbles: true });
                    this.renderer.invokeElementMethod(this.mandatePrefix.nativeElement, 'focus', [focus]);
                }
            }, 0);
            this.utils.makeTabsRed(3);
            return;
        }
        else {
            this.riMaintenanceBeforeSave();
        }
    }


    public renderTab(tabindex: number, isEvent?: boolean): void {
        if (isEvent) {
            this.validateTabs();
        }
        switch (tabindex) {
            case 1:
                this.uiDisplay.tab.tab1.active = true;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = false;
                this.uiDisplay.tab.tab4.active = false;
                this.uiDisplay.tab.tab5.active = false;
                break;
            case 2:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = true;
                this.uiDisplay.tab.tab3.active = false;
                this.uiDisplay.tab.tab4.active = false;
                this.uiDisplay.tab.tab5.active = false;
                break;
            case 3:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = true;
                this.uiDisplay.tab.tab4.active = false;
                this.uiDisplay.tab.tab5.active = false;
                break;
            case 4:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = false;
                this.uiDisplay.tab.tab4.active = true;
                this.uiDisplay.tab.tab5.active = false;
                break;
            case 5:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = false;
                this.uiDisplay.tab.tab4.active = false;
                this.uiDisplay.tab.tab5.active = true;
                break;
        }
    }

    public onCompanyChange(data: any): void {
        this.setControlValue('TaxCodeDesc', '');
        this.setControlValue('CompanyCode', data.companyCode || data.CompanyCode);
        this.setControlValue('CompanyDesc', data.companyDesc || data.CompanyDesc);
        if (this.getControlValue('CompanyCode')) {
            let searchPost: QueryParams;
            searchPost = this.getURLSearchParamObject();
            searchPost.set(this.serviceConstants.Action, '0');
            let postParams: any = {};
            postParams.Table = 'Company';
            postParams.Mode = 'Company';
            postParams.BusinessCode = this.businessCode();
            postParams.CompanyCode = this.getControlValue('CompanyCode');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module,
                this.queryParams.operation, searchPost, postParams)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this['uiForm'].markAsPristine();
                            this.isMakeDisabled = false;
                            this.isBtnAdd = this.isBtnSave = this.isBtnDelete = this.isBtnCancel = true;
                            this.setControlValue('APICode', data.APICode);
                            this.fillAPIDesc(data.APICode);
                            this.dropdownConfig.apiCodeSearch.isDisabled = false;
                            this.TaxCodeSearchConfig.disable = false;
                            this.pageParams.rowID = data.ttCompany;
                            this.setControlValue('CompanyDesc', data.CompanyDesc);
                            this.setControlValue('AccountSortCode', data.AccountSortCode);
                            this.setControlValue('AcquisitionDate', data.AcquisitionDate);
                            this.setControlValue('AllowMixedAccountsIND', data.AllowMixedAccountsInd);
                            this.setControlValue('CompanyActivityText', data.CompanyActivityText);
                            this.setControlValue('CompanyAddressLine1', data.CompanyAddressLine1);
                            this.setControlValue('CompanyAddressLine2', data.CompanyAddressLine2);
                            this.setControlValue('CompanyAddressLine3', data.CompanyAddressLine3);
                            this.setControlValue('CompanyAddressLine4', data.CompanyAddressLine4);
                            this.setControlValue('CompanyAddressLine5', data.CompanyAddressLine5);
                            this.setControlValue('CompanyAgreementText', data.CompanyAgreementText);
                            this.setControlValue('CompanyContactDepartment', data.CompanyContactDepartment);
                            this.setControlValue('CompanyEmail', data.CompanyEmail);
                            this.setControlValue('CompanyFax', data.CompanyFax);
                            this.setControlValue('CompanyGroupName', data.CompanyGroupName);
                            this.setControlValue('CompanyContactName', data.CompanyContactName);
                            this.setControlValue('CompanyImage', data.CompanyImage);
                            this.setControlValue('CompanyLockBoxAddressLine1', data.CompanyLockBoxAddressLine1);
                            this.setControlValue('CompanyLockBoxAddressLine2', data.CompanyLockBoxAddressLine2);
                            this.setControlValue('CompanyLockBoxAddressLine3', data.CompanyLockBoxAddressLine3);
                            this.setControlValue('CompanyLockBoxAddressLine4', data.CompanyLockBoxAddressLine4);
                            this.setControlValue('CompanyLockBoxAddressLine5', data.CompanyLockBoxAddressLine5);
                            this.setControlValue('CompanyLockBoxPostCode', data.CompanyLockBoxPostcode);
                            this.setControlValue('CompanyPostCode', data.CompanyPostCode);
                            this.setControlValue('CompanyRegistrationNumber', data.CompanyRegistrationNumber);
                            this.setControlValue('CompanyRegistrationTown', data.CompanyRegistrationTown);
                            this.setControlValue('CompanyRepresentativeName', data.CompanyRepresentativeName);
                            this.setControlValue('CompanyTelephone', data.CompanyTelephone);
                            this.setControlValue('CompanyTypeText', data.CompanyTypeText);
                            this.setControlValue('CompanyVATNumber', data.CompanyVATNumber);
                            this.setControlValue('CompanyWebsite', data.CompanyWebsite);
                            this.setControlValue('CreditorID', data.CreditorID);
                            this.setControlValue('DateStamp', data.DateStamp);
                            this.setControlValue('DefaultCompanyInd', data.DefaultCompanyInd);
                            this.setControlValue('DomesticBankAccountNumber', data.DomesticBankAccountNumber);
                            this.setControlValue('GenerationNo', data.GenerationNo);
                            this.setControlValue('InstallationGenNo', data.InstallationGenNo);
                            this.setControlValue('InternationalBankAccountNumber', data.InternationalBankAccountNumber);
                            this.setControlValue('InvoiceMessageLine1', data.InvoiceMessageLine1);
                            this.setControlValue('InvoiceMessageLine2', data.InvoiceMessageLine2);
                            this.setControlValue('InvoiceMessageLine3', data.InvoiceMessageLine3);
                            this.setControlValue('InvoiceMessageLine4', data.InvoiceMessageLine4);
                            this.setControlValue('InvoiceMessageLine5', data.InvoiceMessageLine5);
                            this.setControlValue('InvoiceMessageLine6', data.InvoiceMessageLine6);
                            this.setControlValue('InvoiceMessageLine7', data.InvoiceMessageLine7);
                            this.setControlValue('InvoiceMessageLine8', data.InvoiceMessageLine8);
                            this.setControlValue('InvoiceMessageLine9', data.InvoiceMessageLine9);
                            this.setControlValue('InvoiceMessageLine10', data.InvoiceMessageLine10);
                            this.setControlValue('MandatePrefix', data.MandatePrefix);
                            this.setControlValue('MinimumCompanyInvoiceNumber', data.MinimumCompanyInvoiceNumber);
                            this.setControlValue('NAVCompanyCode', data.NAVCompanyCode);
                            this.setControlValue('RecordID', data.RecordID);
                            if (!data.SeparateCompanyInvoices) {
                                this.setControlValue('SeparateCompanyInvoices', '');
                            }
                            this.setControlValue('SequenceNo', data.SequenceNo);
                            this.setControlValue('TaxCode', data.TaxCode);
                            if (data.TaxCode) {
                                this.fillTaxCodeLookUp(data.TaxCode);
                            }
                            this.setControlValue('TimeStamp', data.TimeStamp);
                            this.setControlValue('UseDefaultCompanyInvoiceRange', data.UseDefaultCompanyInvoiceRange);
                            this.setControlValue('UserCode', data.UserCode);
                            this.setControlValue('VADDServiceType', data.VADDServiceType);
                            this.formMode = this.c_s_MODE_UPDATE;

                        }
                    },
                    (error) => {
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, data.fullError));
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    }
                );
        }
    }

    /**
     * This function gets called when keyup event is triggered on add button
     * @param event
     */
    public addOnKeyUp(event: Object): void {
        this.validateTabs();
        this.utils.tabSwitchOnTab(event);
    }

    /**
     * This function gets called when keyup event is triggered on save button
     * @param event
     */
    public saveOnKeyUp(event: Object): void {
        if (this.formMode === this.c_s_MODE_ADD) {
            this.validateTabs();
            this.utils.tabSwitchOnTab(event);
        }
    }

    /**
     * API Code data received from dropdown
     */
    public onAPICodeSearchReceived(data: any): void {
        if (data) {
            this.setControlValue('APICode', data.APICode);
            this.uiForm.controls['APICode'].markAsDirty();
        }
    }

    public acquisitionDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('AcquisitionDate', value.value);
            this.uiForm.controls['AcquisitionDate'].markAsDirty();
        }
    }

    public onTaxCodeChange(): void {
        if (this.getControlValue('TaxCode')) {
            let postSearchParams = this.getURLSearchParamObject();
            postSearchParams.set(this.serviceConstants.Action, '6');
            let postParams: any = {};
            postParams.PostDesc = 'TaxCode';
            postParams.TaxCode = this.getControlValue('TaxCode');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.taxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
                .subscribe(
                    (data) => {
                        if ((data['hasError'])) {
                            this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                            this.setControlValue('TaxCodeDesc', '');
                        } else {
                            if (data['TaxCodeDesc']) {
                                this.setControlValue('TaxCodeDesc', data['TaxCodeDesc']);
                            }
                            else {
                                this.setControlValue('TaxCode', '');
                                this.setControlValue('TaxCodeDesc', '');
                            }
                        }
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    },
                    (error) => {
                        this.errorService.emitError(error);
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        }
        else {
            this.setControlValue('TaxCode', '');
            this.setControlValue('TaxCodeDesc', '');
        }
    }

    public onTaxCodeReceived(data: any): void {
        if (data) {
            this.riExchange.riInputElement.SetValue(this.uiForm, 'TaxCode', data.TaxCode);
            this.riExchange.riInputElement.SetValue(this.uiForm, 'TaxCodeDesc', data.TaxCodeDesc);
            this.uiForm.controls['TaxCode'].markAsDirty();
        }
    }

    /**
     * This function is a handler for save operation
     */
    public saveOnClick(): void {
        let result = this.beforeSave();
        if (result) {
            if (!this.getControlValue('CompanyCode')) {
                setTimeout(() => {
                    this.formCompanyCode.nativeElement.focus();
                }, 0);
            }
            if (!this.getControlValue('CompanyDesc')) {
                setTimeout(() => {
                    this.formCompanyDesc.nativeElement.focus();
                }, 0);
            }

            this.riMaintenanceBeforeConfirm();
        }
        else {
            this.utils.makeTabsRedById();
        }

    }
    public cancelOnClick(): void {
        this.setControlValue('TaxCodeDesc', '');
        this.companyDropDown.getData();
        this.utils.makeTabsNormal();
        this.setErrorStatusOfFormElement('MandatePrefix', false);
        this.setErrorStatusOfFormElement('CompanyAddressLine4', false);
        this.setErrorStatusOfFormElement('CompanyAddressLine5', false);
        this['uiForm'].markAsPristine();
        if (this.formMode === this.c_s_MODE_ADD) {
            this.isInsideAdd = false;
            this.formMode = this.c_s_MODE_ADD;
            this.isBtnAdd = this.isBtnDelete = true;
            this.checkData(this.companyDropDown.requestdata);
        }
        else {
            let data: Object = {
                CompanyCode: this.getControlValue('CompanyCode'),
                CompanyDesc: this.getControlValue('CompanyDesc')
            };
            this.onCompanyChange(data);
        }

    }

    public checkData(data: any): void {
        if (!data.length) {
            this.isInsideAdd = true;
            this.isBtnAdd = this.isBtnDelete = false;
            this.isBtnSave = this.isBtnCancel = true;
            this.formMode = this.c_s_MODE_ADD;
            this.uiForm.reset();
            setTimeout(() => {
                this.formCompanyCode.nativeElement.focus();
            }, 0);
        }
        if (data.length > 0 && data[0].CompanyDesc && data[0].CompanyCode && this.formMode === this.c_s_MODE_UPDATE && this.isAfterSave === false) {
            if (this.getControlValue('CompanyCode')) {
                this.setControlValue('CompanyDesc', this.getControlValue('CompanyDesc'));
                this.companyDefault = {
                    id: this.getControlValue('CompanyCode'),
                    text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
                };
            }
            else {
                this.setControlValue('CompanyCode', data[0].CompanyCode);
                this.setControlValue('CompanyDesc', data[0].CompanyDesc);
            }
            let dataset: Object = {
                CompanyCode: this.getControlValue('CompanyCode'),
                CompanyDesc: this.getControlValue('CompanyDesc')
            };

            this.onCompanyChange(dataset);
        }
        if (this.formMode === 'DELETE') {
            this.isInsideAdd = false;
            this.isBtnAdd = this.isBtnDelete = true;
            this.formMode = this.c_s_MODE_UPDATE;
            if (data.length > 0 && data[0].CompanyDesc && data[0].CompanyCode) {
                this.setControlValue('CompanyCode', data[0].CompanyCode);
                this.setControlValue('CompanyDesc', data[0].CompanyDesc);
                let dataset: Object = {
                    CompanyCode: this.getControlValue('CompanyCode'),
                    CompanyDesc: this.getControlValue('CompanyDesc')
                };
                this.companyDefault = {
                    id: this.getControlValue('CompanyCode'),
                    text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
                };

                this.onCompanyChange(dataset);
            }
            else {
                this.isInsideAdd = true;
                this.isBtnAdd = this.isBtnDelete = false;
                this.isBtnSave = this.isBtnCancel = true;
                this.formMode = this.c_s_MODE_ADD;
                this.uiForm.reset();
                setTimeout(() => {
                    this.formCompanyCode.nativeElement.focus();
                }, 0);
            }
        }
        if (this.formMode === this.c_s_MODE_ADD) {
            this.formMode = this.c_s_MODE_UPDATE;
            if (data.length > 0 && data[0].CompanyDesc && data[0].CompanyCode) {
                this.setControlValue('CompanyCode', data[0].CompanyCode);
                this.setControlValue('CompanyDesc', data[0].CompanyDesc);
            }
            this.companyDefault = {
                id: this.getControlValue('CompanyCode'),
                text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
            };
            let dataset: Object = {
                CompanyCode: this.getControlValue('CompanyCode'),
                CompanyDesc: this.getControlValue('CompanyDesc')
            };

            this.onCompanyChange(dataset);

        }
    }

    public addOnClick(): void {
        this.isInsideAdd = true;
        this.isBtnAdd = this.isBtnDelete = false;
        this.formMode = this.c_s_MODE_ADD;
        this.uiForm.reset();
        setTimeout(() => {
            this.formCompanyCode.nativeElement.focus();
        }, 0);
        this.dropdownConfig.apiCodeSearch.active = {
            id: '',
            text: ''
        };
    }

    public deleteOnClick(): void {
        this.promptTitle = MessageConstant.Message.DeleteRecord;
        this.modalAdvService.emitPrompt(new ICabsModalVO(this.promptTitle, null, this.proceedDelete.bind(this)));
    }

    public checkFormDirty(): void {
        if (!this.uiForm.dirty) {
            this.addOnClick();
        } else {
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.RouteAway, null, this.addOnClick.bind(this), null));
        }
    }

}
