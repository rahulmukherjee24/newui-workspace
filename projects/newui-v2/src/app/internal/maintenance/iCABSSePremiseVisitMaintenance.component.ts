import { Component, OnInit, OnDestroy, Injector, ViewChild, EventEmitter } from '@angular/core';
import { QueryParams } from '../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { ContractSearchComponent } from '../../internal/search/iCABSAContractSearch';
import { PremiseSearchComponent } from '../../internal/search/iCABSAPremiseSearch';
import { EmployeeSearchComponent } from '../../internal/search/iCABSBEmployeeSearch';

@Component({
    templateUrl: 'iCABSSePremiseVisitMaintenance.html'
})

export class PremiseVisitMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private queryParams: any = {
        operation: 'Service/iCABSSePremiseVisitMaintenance',
        module: 'visits',
        method: 'service-delivery/grid'
    };

    public setFocusPremiseContactName = new EventEmitter<boolean>();
    public setFocusEmployeeCode = new EventEmitter<boolean>();
    public pageId: string = '';
    public batchSubmittedText: string;
    public isShowBatchSubmitted: boolean = false;
    public controls: Array<any> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: true, type: MntConst.eTypeText },
        { name: 'EmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceDateStart', type: MntConst.eTypeDate },
        { name: 'ActualStartTime', type: MntConst.eTypeTime },
        { name: 'ActualEndTime', type: MntConst.eTypeTime },
        { name: 'PremiseContactName', type: MntConst.eTypeText },
        { name: 'JobTitleNumber', type: MntConst.eTypeInteger },
        { name: 'JobTitleDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ReasonNumber', type: MntConst.eTypeInteger },
        { name: 'ReasonDesc', type: MntConst.eTypeText },
        { name: 'VisitReference', type: MntConst.eTypeText },
        { name: 'AttemptedPrintInd', type: MntConst.eTypeCheckBox },
        { name: 'SuccessfulPrintInd', type: MntConst.eTypeCheckBox },
        { name: 'NoPremiseVisitReasonCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'NoPremiseVisitReasonDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'NoBarcodeReasonCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'NoBarcodeReasonDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseContactSignatureURL', type: MntConst.eTypeTextFree },
        { name: 'PremiseContactPrintedNameURL', type: MntConst.eTypeTextFree },
        { name: 'PremiseContactReason', type: MntConst.eTypeText },
        { name: 'RepDestSelect', type: MntConst.eTypeText },
        { name: 'chkConsumablesOnly', type: MntConst.eTypeCheckBox },
        { name: 'Menu', type: MntConst.eTypeText },
        { name: 'PremiseVisitRowID', type: MntConst.eTypeText },
        { name: 'GotSignature', type: MntConst.eTypeCheckBox },
        { name: 'PremiseVisitNumber', type: MntConst.eTypeInteger },
        { name: 'OldEmployeeCode', type: MntConst.eTypeCode },
        { name: 'OldServiceDateStart', type: MntConst.eTypeDate },
        { name: 'ErrorMessage', type: MntConst.eTypeText },
        { name: 'DisableEmployeeUpdate', type: MntConst.eTypeCheckBox }
    ];
    public dropdown: any = {
        noSignatureReasonSearch: {
            columns: ['ReasonNumber', 'ReasonDesc'],
            active: { id: '', text: '' },
            inputParams: {
                module: 'service',
                method: 'service-delivery/search',
                operation: 'Business/iCABSBNoSignatureReasonSearch'
            }
        }
    };
    public ellipsisParams: any = {
        contractNumber: {
            childConfigParams: {
                parentMode: 'LookUp',
                CurrentContractTypeURLParameter: ''
            },
            contentComponent: ContractSearchComponent,
            isHideIcon: true
        },
        premiseNumber: {
            childConfigParams: {
                parentMode: 'LookUp',
                CurrentContractTypeURLParameter: '',
                ContractNumber: '',
                ContractName: ''
            },
            contentComponent: PremiseSearchComponent,
            isHideIcon: true
        },
        employee: {
            childConfigParams: {
                parentMode: 'LookUp-Service'
            },
            contentComponent: EmployeeSearchComponent,
            isHideIcon: false
        },
        common: {
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            }
        }
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPREMISEVISITMAINTENANCE;

        this.browserTitle = this.pageTitle = 'Premises Visit Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private onWindowLoad(): void {
        this.setPageDefaults();
        this.getSysCharDetails();
        if (this.getControlValue('PremiseVisitRowID')) {
            this.fetchRecord();
        }
        this.setControlValue('RepDestSelect', 'direct');
    }

    private setPageDefaults(): void {
        this.pageParams.isSCEnableVisitRef = false;
        this.pageParams.isSCEnableNoServiceReasons = false;
        this.pageParams.isSCEnableBarcodes = false;
        this.pageParams.isShowSignature = true;
        this.pageParams.isShowReasonNumber = true;
        this.pageParams.isShowReasonDesc = true;
        this.pageParams.isShowPremiseContactSignature = true;
        this.pageParams.isShowPremiseContactPrintedName = true;
        this.pageParams.premiseContactSignatureSrc = '';
        this.pageParams.premiseContactPrintedNameSrc = '';
        this.pageParams.isShowPremiseContactPrintedNameTitle = true;
        this.pageParams.isDisableEmployeeUpdate = false;

        this.pageParams.currentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
        this.pageParams.currentContractTypeURLParameter = this.riExchange.getCurrentContractTypeUrlParam();
        this.setControlValue('chkConsumablesOnly', true);
        this.ellipsisParams.contractNumber.childConfigParams.CurrentContractTypeURLParameter = this.pageParams.currentContractTypeURLParameter;
        this.ellipsisParams.premiseNumber.childConfigParams.CurrentContractTypeURLParameter = this.pageParams.currentContractTypeURLParameter;

        if (this.parentMode === 'Summary') {
            this.setControlValue('PremiseVisitRowID', this.riExchange.getParentAttributeValue('PremiseVisitRowID'));
            this.disableControl('ContractNumber', true);
            this.disableControl('PremiseNumber', true);
            this.ellipsisParams.contractNumber.isHideIcon = true;
            this.ellipsisParams.premiseNumber.isHideIcon = true;
        } else {
            this.disableControl('ContractNumber', false);
            this.disableControl('PremiseNumber', false);
            this.ellipsisParams.contractNumber.isHideIcon = false;
            this.ellipsisParams.premiseNumber.isHideIcon = false;
        }
    }

    private getSysCharDetails(): void {
        //SysChar
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharEnableEntryOfVisitRefInVisitEntry,
            this.sysCharConstants.SystemCharEnableNoServiceReasons,
            this.sysCharConstants.SystemCharEnableBarcodes
        ];
        let sysCharIP: any = {
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };
        this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            let record = data.records;
            this.pageParams.isSCEnableVisitRef = record[0]['Required'];
            this.pageParams.isSCEnableNoServiceReasons = record[1]['Required'];
            this.pageParams.isSCEnableBarcodes = record[2]['Required'];
        });
    }

    private fetchRecord(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set('PremiseVisitRowID', this.getControlValue('PremiseVisitRowID'));
        search.set('ContractNumber', '');
        search.set('PremiseNumber', '');
        search.set(this.serviceConstants.Action, '0');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('DisableEmployeeUpdate', data.DisableEmployeeUpdate === 'yes');
                    this.setControlValue('GotSignature', data.GotSignature === 'yes');
                    this.setControlValue('SuccessfulPrintInd', data.SuccessfulPrintInd === 'yes');
                    this.setControlValue('AttemptedPrintInd', data.AttemptedPrintInd === 'yes');
                    for (let key in data) {
                        if (key) {
                            if (!(key === 'DisableEmployeeUpdate' || key === 'GotSignature' || key === 'SuccessfulPrintInd' || key === 'AttemptedPrintInd')) {
                                this.setControlValue(key, data[key]);
                            }
                        }
                    }
                    this.getLookupData('A');
                    this.onAfterFetch();
                    this.onBeforeUpdate();
                    this.formPristine();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private onAfterFetch(): void {
        if (this.getControlValue('GotSignature', true)) {
            this.pageParams.isShowSignature = true;
            this.pageParams.isShowReasonNumber = false;
            this.pageParams.isShowReasonDesc = false;
            this.pageParams.isShowPremiseContactSignature = false;
            this.pageParams.isShowPremiseContactPrintedName = false;

            if (this.getControlValue('PremiseContactSignatureURL')) {
                this.pageParams.premiseContactSignatureSrc = this.getControlValue('PremiseContactSignatureURL');
            } else {
                this.pageParams.premiseContactSignatureSrc = '';
            }

            if (this.getControlValue('PremiseContactPrintedNameURL')) {
                this.pageParams.premiseContactPrintedNameSrc = this.getControlValue('PremiseContactPrintedNameURL');
                this.pageParams.isShowPremiseContactPrintedNameTitle = true;
            } else {
                this.pageParams.premiseContactPrintedNameSrc = '';
                this.pageParams.isShowPremiseContactPrintedNameTitle = false;
            }
        } else {
            this.pageParams.isShowSignature = false;
            if (this.getControlValue('ReasonNumber')) {
                this.pageParams.isShowReasonNumber = true;
                this.pageParams.isShowReasonDesc = false;
            } else {
                this.pageParams.isShowReasonNumber = false;
                this.pageParams.isShowReasonDesc = true;
            }
        }

        if (this.pageParams.premiseContactSignatureSrc !== '') {
            this.pageParams.isShowPremiseContactSignature = true;
            this.pageParams.isShowSignature = true;
        }

        if (this.pageParams.premiseContactPrintedNameSrc !== '') {
            this.pageParams.isShowPremiseContactPrintedName = true;
        }
    }

    private onBeforeUpdate(): void {
        this.pageParams.isDisableEmployeeUpdate = this.getControlValue('DisableEmployeeUpdate', true);

        this.disableControl('EmployeeCode', this.pageParams.isDisableEmployeeUpdate);
        this.disableControl('ServiceDateStart', this.pageParams.isDisableEmployeeUpdate);
        this.disableControl('ActualStartTime', this.pageParams.isDisableEmployeeUpdate);
        this.disableControl('ActualEndTime', this.pageParams.isDisableEmployeeUpdate);

        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EmployeeCode', !this.pageParams.isDisableEmployeeUpdate);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ServiceDateStart', !this.pageParams.isDisableEmployeeUpdate);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ActualStartTime', !this.pageParams.isDisableEmployeeUpdate);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ActualEndTime', !this.pageParams.isDisableEmployeeUpdate);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PremiseContactName', true);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'JobTitleNumber', true);

        if (this.pageParams.isDisableEmployeeUpdate) {
            this.setFocusPremiseContactName.emit(true);
        } else {
            this.setFocusEmployeeCode.emit(true);
        }
    }

    private saveRecord(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();

        formData['PremiseVisitRowID'] = this.getControlValue('PremiseVisitRowID');
        formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        formData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        formData[this.serviceConstants.EmployeeCode] = this.getControlValue('EmployeeCode');
        formData['JobTitleNumber'] = this.getControlValue('JobTitleNumber');
        formData['VisitReference'] = this.getControlValue('VisitReference');
        formData['ServiceDateStart'] = this.getControlValue('ServiceDateStart');
        formData['ActualStartTime'] = this.getControlValue('ActualStartTime');
        formData['ActualEndTime'] = this.getControlValue('ActualEndTime');
        formData['PremiseContactName'] = this.getControlValue('PremiseContactName');
        formData['PremiseContactReason'] = this.getControlValue('PremiseContactReason');
        formData['ReasonNumber'] = this.getControlValue('ReasonNumber');
        formData['PremiseContactSignatureURL'] = this.getControlValue('PremiseContactSignatureURL');
        formData['PremiseContactPrintedNameURL'] = this.getControlValue('PremiseContactPrintedNameURL');
        formData['AttemptedPrintInd'] = this.getControlValue('AttemptedPrintInd') ? 'yes' : 'no';
        formData['SuccessfulPrintInd'] = this.getControlValue('SuccessfulPrintInd') ? 'yes' : 'no';
        formData['NoPremiseVisitReasonCode'] = this.getControlValue('NoPremiseVisitReasonCode');
        formData['NoBarcodeReasonCode'] = this.getControlValue('NoBarcodeReasonCode');
        formData['GotSignature'] = this.getControlValue('GotSignature') ? 'yes' : 'no';
        formData['PremiseVisitNumber'] = this.getControlValue('PremiseVisitNumber');
        formData['OldEmployeeCode'] = this.getControlValue('OldEmployeeCode');
        formData['OldServiceDateStart'] = this.getControlValue('OldServiceDateStart');
        formData['DisableEmployeeUpdate'] = this.getControlValue('DisableEmployeeUpdate') ? 'yes' : 'no';

        search.set(this.serviceConstants.Action, '2');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    if (this.pageParams.isSCEnableVisitRef && data.VisitReferenceWarning !== '') {
                        let modalVO: ICabsModalVO = new ICabsModalVO(data.VisitReferenceWarning);
                        modalVO.closeCallback = this.callCloseBackForSave.bind(this);
                        this.modalAdvService.emitMessage(modalVO);
                    }
                    else {
                        this.callCloseBackForSave();
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private warnChanges(): void {
        if (this.getControlValue('EmployeeCode') !== this.getControlValue('OldEmployeeCode') ||
            this.getControlValue('ServiceDateStart') !== this.getControlValue('OldServiceDateStart')) {
            let formData: Object = {};
            let search: QueryParams = this.getURLSearchParamObject();

            search.set(this.serviceConstants.Action, '6');

            formData[this.serviceConstants.Function] = 'WarnChanges';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        if (data.ErrorMessage !== '') {
                            let modalVO: ICabsModalVO = new ICabsModalVO(data.ErrorMessage);
                            modalVO.closeCallback = this.callCloseBackForWarnChanges.bind(this);
                            this.modalAdvService.emitMessage(modalVO);
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        } else {
            if (this.riExchange.validateForm(this.uiForm)) {
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveRecord.bind(this)));
            }
        }
    }

    private getSecondsSinceMidnight(d: any): number {
        let e = new Date(d);
        return d - e.setHours(0, 0, 0, 0);
    }

    public getLookupData(callType: string): void {
        let lookupIPDetails: Array<any> = [];
        let contractIndex: number = -1;
        let premiseIndex: number = -1;
        let employeeIndex: number = -1;
        let jobTitleIndex: number = -1;
        let noSignatureReasonIndex: number = -1;
        let noBarcodeReasonIndex: number = -1;

        if ((callType === 'A' || callType === 'C') && this.getControlValue('ContractNumber')) {
            lookupIPDetails.push({
                'table': 'Contract',
                'query': {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'BusinessCode': this.businessCode()
                },
                'fields': ['ContractNumber', 'ContractName']
            });
            contractIndex = lookupIPDetails.length - 1;
        }

        if ((callType === 'A' || callType === 'P') && this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber')) {
            lookupIPDetails.push({
                'table': 'Premise',
                'query': {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'PremiseNumber': this.getControlValue('PremiseNumber'),
                    'BusinessCode': this.businessCode()
                },
                'fields': ['PremiseNumber', 'PremiseName']
            });
            premiseIndex = lookupIPDetails.length - 1;
        }

        if ((callType === 'A' || callType === 'E') && this.getControlValue('EmployeeCode')) {
            lookupIPDetails.push({
                'table': 'Employee',
                'query': {
                    'EmployeeCode': this.getControlValue('EmployeeCode'),
                    'BusinessCode': this.businessCode()
                },
                'fields': ['EmployeeCode', 'EmployeeSurname']
            });
            employeeIndex = lookupIPDetails.length - 1;
        }

        if ((callType === 'A' || callType === 'J') && this.getControlValue('JobTitleNumber')) {
            lookupIPDetails.push({
                'table': 'JobTitle',
                'query': {
                    'JobTitleNumber': this.getControlValue('JobTitleNumber'),
                    'BusinessCode': this.businessCode()
                },
                'fields': ['JobTitleNumber', 'JobTitleDesc']
            });
            jobTitleIndex = lookupIPDetails.length - 1;
        }

        if ((callType === 'A' || callType === 'NSR') && this.getControlValue('ReasonNumber')) {
            lookupIPDetails.push({
                'table': 'NoSignatureReason',
                'query': {
                    'ReasonNumber': this.getControlValue('ReasonNumber'),
                    'BusinessCode': this.businessCode()
                },
                'fields': ['ReasonNumber', 'ReasonDesc']
            });
            noSignatureReasonIndex = lookupIPDetails.length - 1;
        }

        if ((callType === 'A' || callType === 'NBR') && this.getControlValue('NoBarcodeReasonCode')) {
            lookupIPDetails.push({
                'table': 'NoBarcodeReason',
                'query': {
                    'NoBarcodeReasonCode': this.getControlValue('NoBarcodeReasonCode'),
                    'BusinessCode': this.businessCode()
                },
                'fields': ['NoBarcodeReasonCode', 'NoBarcodeReasonDesc']
            });
            noBarcodeReasonIndex = lookupIPDetails.length - 1;
        }

        if (lookupIPDetails.length > 0) {
            this.LookUp.lookUpPromise(lookupIPDetails).then((data) => {
                let contractDetail: any = null;
                let premiseDetail: any = null;
                let employeeDetail: any = null;
                let jobTitleDetail: any = null;
                let noSignatureReasonDetail: any = null;
                let noBarcodeReasonDetail: any = null;

                if ((callType === 'A' || callType === 'C') && contractIndex > -1 && data && data.length > 0 && data[contractIndex].length > 0) { contractDetail = data[contractIndex][0]; }
                if ((callType === 'A' || callType === 'P') && premiseIndex > -1 && data && data.length > 0 && data[premiseIndex].length > 0) { premiseDetail = data[premiseIndex][0]; }
                if ((callType === 'A' || callType === 'E') && employeeIndex > -1 && data && data.length > 0 && data[employeeIndex].length > 0) { employeeDetail = data[employeeIndex][0]; }
                if ((callType === 'A' || callType === 'J') && jobTitleIndex > -1 && data && data.length > 0 && data[jobTitleIndex].length > 0) { jobTitleDetail = data[jobTitleIndex][0]; }
                if ((callType === 'A' || callType === 'NSR') && noSignatureReasonIndex > -1 && data && data.length > 0 && data[noSignatureReasonIndex].length > 0) { noSignatureReasonDetail = data[noSignatureReasonIndex][0]; }
                if ((callType === 'A' || callType === 'NBR') && noBarcodeReasonIndex > -1 && data && data.length > 0 && data[noBarcodeReasonIndex].length > 0) { noBarcodeReasonDetail = data[noBarcodeReasonIndex][0]; }

                if (callType === 'A' || callType === 'C') {
                    if (contractDetail && contractDetail.ContractName) {
                        this.setControlValue('ContractName', contractDetail.ContractName);

                        this.ellipsisParams.premiseNumber.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
                        this.ellipsisParams.premiseNumber.childConfigParams.ContractName = this.getControlValue('ContractName');
                    } else {
                        this.setControlValue('ContractName', '');

                        this.ellipsisParams.premiseNumber.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
                        this.ellipsisParams.premiseNumber.childConfigParams.ContractName = this.getControlValue('ContractName');
                    }
                }

                if (callType === 'A' || callType === 'P') {
                    if (premiseDetail && premiseDetail.PremiseName) {
                        this.setControlValue('PremiseName', premiseDetail.PremiseName);
                    } else {
                        this.setControlValue('PremiseName', '');
                    }
                }

                if (callType === 'A' || callType === 'E') {
                    if (employeeDetail && employeeDetail.EmployeeSurname) {
                        this.setControlValue('EmployeeSurname', employeeDetail.EmployeeSurname);
                    } else {
                        this.setControlValue('EmployeeSurname', '');
                    }
                }

                if (callType === 'A' || callType === 'J') {
                    if (jobTitleDetail && jobTitleDetail.JobTitleDesc) {
                        this.setControlValue('JobTitleDesc', jobTitleDetail.JobTitleDesc);
                    } else {
                        this.setControlValue('JobTitleDesc', '');
                    }
                }

                if (callType === 'A' || callType === 'NSR') {
                    if (noSignatureReasonDetail && noSignatureReasonDetail.ReasonDesc) {
                        this.dropdown.noSignatureReasonSearch['active'] = {
                            id: noSignatureReasonDetail.ReasonNumber,
                            text: noSignatureReasonDetail.ReasonNumber + ' - ' + noSignatureReasonDetail.ReasonDesc
                        };
                    } else {
                        this.setControlValue('ReasonDesc', '');
                    }
                }

                if (callType === 'A' || callType === 'NBR') {
                    if (noBarcodeReasonDetail && noBarcodeReasonDetail.NoBarcodeReasonDesc) {
                        this.setControlValue('NoBarcodeReasonDesc', noBarcodeReasonDetail.NoBarcodeReasonDesc);
                    } else {
                        this.setControlValue('NoBarcodeReasonDesc', '');
                    }
                }
            });
        }
    }

    public onNoSignatureReasonSearchDataRecieved(event: any): void {
        this.setControlValue('ReasonNumber', event.ReasonNumber || '');
    }

    public onMenuChange(selectedValue: string): void {
        this.setControlValue('Menu', '');
        this.riExchange.riInputElement.MarkAsPristine(this.uiForm, 'Menu');

        switch (selectedValue) {
            case 'ScannedBarcodes':
                // this.navigate('PremiseVisit', 'Service/iCABSSeScannedBarcodesGrid.htm');
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotDeveloped));
                break;
        }
    }

    // Populate data from ellipsis
    public onEllipsisDataReceived(type: string, event: any): void {
        switch (type) {
            case 'ContractNumber':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ContractNumber');
                this.setControlValue('ContractNumber', event.ContractNumber);
                this.setControlValue('ContractName', event.ContractName);
                break;
            case 'Premises':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'PremiseNumber');
                this.setControlValue('PremiseNumber', event.PremiseNumber);
                this.setControlValue('PremiseName', event.PremiseName);
                break;
            case 'Employee':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'EmployeeCode');
                this.setControlValue('EmployeeCode', event.EmployeeCode);
                this.setControlValue('EmployeeSurname', event.EmployeeSurname);
                break;
        }
    }

    // Form submit event
    public onSubmit(event: any): void {
        this.warnChanges();
    }

    // close call back for modalVO emit prompt
    public callCloseBackForWarnChanges(): void {
        setTimeout(function (): void {
            if (this.riExchange.validateForm(this.uiForm)) {
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveRecord.bind(this)));
            }
        }.bind(this), 1000);
    }

    // close call back for modalVO emit prompt
    public callCloseBackForSave(): void {
        setTimeout(function (): void {
            this.fetchRecord();
            this.formPristine();
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
        }.bind(this), 1000);
    }

    public onCancelClick(): void {
        this.fetchRecord();
        this.formPristine();
    }

    public onSubmitReportGenerationClick(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let arrParameterName: Array<string> = [], arrParameterValue: Array<string> = [];
        let pipeSpace: any = String.fromCharCode(5);

        arrParameterName.push('BusinessCode'); arrParameterValue.push(this.businessCode());
        arrParameterName.push('BranchNumber'); arrParameterValue.push(this.utils.getBranchCode());
        arrParameterName.push('PremiseVisitRowID'); arrParameterValue.push(this.getControlValue('PremiseVisitRowID'));
        arrParameterName.push('RepDestSelect'); arrParameterValue.push(this.getControlValue('RepDestSelect'));
        switch (this.getControlValue('RepDestSelect')) {
            case 'direct':
                arrParameterName.push('RepManDest'); arrParameterValue.push('batch|ReportID');
                break;
            case 'EmailAccount':
            case 'EmailContract':
            case 'EmailPremises':
                arrParameterName.push('RepManDest'); arrParameterValue.push('email|User');
                break;
        }
        arrParameterName.push('ConsumablesOnly'); arrParameterValue.push(this.getControlValue('chkConsumablesOnly'));

        search.set(this.serviceConstants.Action, '0');
        search.set('Description', 'Delivery Receipt');
        search.set('ProgramName', 'iCABSDeliveryReceipt.p');
        search.set('StartDate', this.globalize.parseDateToFixedFormat(new Date()).toString());
        search.set('StartTime', this.utils.hmsToSeconds(this.utils.Time()));
        search.set('Report', 'report');
        search.set('ParameterName', arrParameterName.join(pipeSpace));
        search.set('ParameterValue', arrParameterValue.join(pipeSpace));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.isShowBatchSubmitted = true;
                this.batchSubmittedText = data.fullError;
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }
}
