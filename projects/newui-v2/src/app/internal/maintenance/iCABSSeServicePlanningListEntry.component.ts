import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { OnInit, OnDestroy, Component, Injector, AfterContentInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { GlobalizeService } from '../../../shared/services/globalize.service';
import { BranchServiceAreaSearchComponent } from '../../../app/internal/search/iCABSBBranchServiceAreaSearch';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';

@Component({
    templateUrl: 'iCABSSeServicePlanningListEntry.html'
})

export class ServicePlanningListEntryComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    /**
     * Private Variables
     */
    private globalizeService: GlobalizeService;
    private branchName: any;
    private branchCode: any;
    private currentBusinessCode: any;

    //Subscription Variables
    private branchServiceArea: Subscription;
    private submitReportGeneration: Subscription;

    //API Variables
    private queryParams = {
        method: 'service-planning/maintenance',
        module: 'planning',
        operation: 'Service/iCABSSeServicePlanningListEntry'
    };
    /**
     * Public Variables
     */
    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'BusinessCode', type: MntConst.eTypeCode },
        { name: 'BusinessDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'BranchNumber', type: MntConst.eTypeInteger },
        { name: 'BranchName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'DateFrom', disabled: false, required: false, type: MntConst.eTypeDate },
        { name: 'DateTo', disabled: false, required: true, type: MntConst.eTypeDate }
    ];
    //Ellipsis Config Variables
    public ellipsis = {
        branchServiceArea: {
            isAutoOpenSearch: false,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp',
                'showAddNew': false,
                'ServiceBranchNumber': '',
                'BranchName': ''
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            component: BranchServiceAreaSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        }
    };
    //Field Disable Variables
    public fieldDisable = {
        exportWeeklyPlan: false
    };
    /**
     * Class Constructor With Dependency Injections
     */
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNINGLISTENTRY;
        this.pageTitle = this.browserTitle = 'Service Planning List';
    }
    /**
     * On Init Life Cycle Hook Inittate Funcationality
     */
    ngOnInit(): void {
        super.ngOnInit();
        this.branchName = this.utils.getBranchText(this.utils.getBranchCode()).split(' - ');
        this.branchCode = this.utils.getBranchCode();
        this.currentBusinessCode = this.utils.getBusinessCode();

    }
    /**
     * After View Initialize Life Cycle Hook To Do After Page Load Operations
     */
    ngAfterContentInit(): void {
        this.windowOnLoad();
    }
    /**
     * On Destroy Life Cycle Hook To Unsubscribe Subscription
     */
    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.branchServiceArea) {
            this.branchServiceArea.unsubscribe();
        }
        if (this.submitReportGeneration) {
            this.submitReportGeneration.unsubscribe();
        }
    }
    /**
     * Method to do Operations After Window Load
     * @params: params: void
     * @return: : void
     */
    private windowOnLoad(): void {
        let dateNew = new Date(),
            yearCurrent = dateNew.getFullYear(),
            monthCurrent = dateNew.getMonth();
        this.ellipsis.branchServiceArea.childConfigParams.BranchName = this.branchName[1];
        this.ellipsis.branchServiceArea.childConfigParams.ServiceBranchNumber = this.branchCode;
        this.setControlValue('BusinessCode', this.businessCode);
        this.setControlValue('BusinessDesc', this.utils.getBusinessText(this.currentBusinessCode));
        this.setControlValue('BranchNumber', this.branchCode);
        this.setControlValue('BranchName', this.utils.getBranchText(this.branchCode));
        this.setControlValue('DateFrom', new Date(yearCurrent, monthCurrent, 1));
        this.setControlValue('DateTo', new Date(yearCurrent, monthCurrent + 1, 0));
    }
    /**
     * Method to update date value on change of From Date field
     * @params: event: any
     * @return: : void
     */
    public onFromDateChange(event: any): void {
        this.setControlValue('DateFrom', event.value);
    }
    /**
     * Method to update date value on change of From To field
     * @params: event: any
     * @return: : void
     */
    public onToDateChange(event: any): void {
        this.setControlValue('DateTo', event.value);
    }
    /**
     * Method to update BranchServiceAreaCode,BranchServiceAreaDesc On BranchServiceAreaCode ellipsis data return
     * @params: data: any
     * @return: : void
     */
    public onBranchServiceAreaCodeRecieved(data: any): void {
        this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
        this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
    }
    /**
     * Method to Get Branch Service Aread Description
     * @params: params: void
     * @return: void
     */
    public onBranchServiceAreaCodeChange(): void {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '0');
            search.set('PostDesc', 'BranchServiceArea');
            search.set('BranchNumber', this.branchCode);
            search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));

            this.ajaxSource.next(this.ajaxconstant.START);
            this.branchServiceArea = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            this.setControlValue('BranchServiceAreaDesc', '');
                        } else {
                            this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        this.setControlValue('BranchServiceAreaDesc', '');
                    });
        } else {
            this.setControlValue('BranchServiceAreaDesc', '');
        }
    }
    /**
     * Method to Submit Report Generation Form And Open Form PDF In New Window
     * @params: params: void
     * @return: void
     */
    public onSubmitReportGenerationClick(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            let search: any = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '0');
            search.set('BranchNumber', this.branchCode);
            search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
            search.set('DateFrom', this.getControlValue('DateFrom'));
            search.set('DateTo', this.getControlValue('DateTo'));

            this.ajaxSource.next(this.ajaxconstant.START);
            this.submitReportGeneration = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            window.open(data.url, '_blank');
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }
    /**
     * Method to Run Batch Process For Weekly Service Extract and Sent Email The Executor
     * @params: params: void
     * @return: void
     */
    public onExportWeeklyPlanClick(): void {
        let pipeSpace: any = String.fromCharCode(5),
            strDescription: any = 'Weekly Service Extract',
            strProgramName: any = 'iCABSServicingExports.p',
            strStartDate: any = this.utils.formatDate(new Date()),
            strStartTime: any = this.utils.hmsToSeconds(this.utils.Time()),
            strParamName: any = 'BusinessCode' + pipeSpace + 'BranchNumber' + pipeSpace + 'Mode' + pipeSpace + 'DateFrom' + pipeSpace + 'DateTo',
            strParamValue: any = this.currentBusinessCode + pipeSpace + this.branchCode + pipeSpace + 'Weekly' + pipeSpace + this.getControlValue('DateFrom') + pipeSpace + this.getControlValue('DateTo');

        let search: any = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        search.set('ProgramName', strProgramName);
        search.set('Description', strDescription);
        search.set('StartDate', strStartDate);
        search.set('StartTime', strStartTime);
        search.set('ParameterName', strParamName);
        search.set('ParameterValue', strParamValue);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.submitReportGeneration = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        this.fieldDisable.exportWeeklyPlan = true;
    }
}
