import { OnInit, Injector, OnDestroy, ViewChild, Component } from '@angular/core';
import { QueryParams } from '../../../shared/services/http-params-wrapper';

import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';

@Component({
    templateUrl: 'iCABSSeDebriefMaintenance.html'
})

export class SeDebriefMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    public isUpdateMode: boolean = false;
    private search: QueryParams = new QueryParams();
    private xhrParams: any = {
        operation: 'Service/iCABSSeDebriefMaintenance',
        module: 'sam',
        method: 'service-delivery/maintenance'
    };

    public pageId: string = '';
    public totalRecords: number = 1;
    public pageCurrent: number = 1;
    public controls = [
        { name: 'BranchServiceAreaCode', readonly: true, disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'DaysWorkNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'EmployeeSurname', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'EmployeeCode', readonly: true, disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'DebriefUserCode', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'DebriefNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'DebriefFromDate', readonly: true, disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'DebriefToDate', readonly: true, disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'PlannedVisitsNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'CompletedVisitsNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'ManualVisitsNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'MissedVisitsNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'UnactionedVisitsNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'AddedVisitsNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'CancelledVisitsNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'RejectedVisitsNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'ExpectedScansNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'CapturedScansNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'RegisteredScansNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'RejectedScansNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'ExpectedSignaturesNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'CapturedSignaturesNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'RejectedSignaturesNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'LeadsNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'AlertsNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'ActionsNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'VehicleInspectedInd', readonly: true, disabled: true, required: false, type: MntConst.eTypeCheckBox },
        { name: 'VehicleStartMileage', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'VehicleEndMileage', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'BranchNumber', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'DebriefTypeCode', readonly: true, disabled: true, required: false, type: MntConst.eTypeCode }
    ];

    public uiDisplay: any = {
        tab: {
            tab1: { visible: true, active: true },
            tab2: { visible: true, active: false }
        }
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEDEBRIEFMAINTENANCE;
        this.browserTitle = 'Debrief';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = 10;
        if (this.parentMode === 'Debrief') {
            this.setControlValue('BranchNumber', this.utils.getBranchCode());
            this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
            this.setControlValue('BranchServiceAreaDesc', this.riExchange.getParentHTMLValue('BranchServiceAreaDesc'));
            this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
            this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname'));
            this.setControlValue('DebriefNumber', this.riExchange.getParentHTMLValue('DebriefNumber'));
            this.setControlValue('VehicleInspectedInd', this.riExchange.getParentHTMLValue('VehicleInspectedInd'));
            this.setControlValue('VehicleStartMileage', this.riExchange.getParentHTMLValue('VehicleStartMileage'));
            this.setControlValue('VehicleEndMileage', this.riExchange.getParentHTMLValue('VehicleEndMileage'));
            this.setControlValue('DebriefFromDate', this.riExchange.getParentHTMLValue('DebriefFromDate'));
            this.setControlValue('DebriefToDate', this.riExchange.getParentHTMLValue('DebriefToDate'));
            //Cannot Debrief into the future, soif DebriefDate is greater than TODAY, it back to TODAY
            if (new Date(this.getControlValue('DebriefToDate')) > new Date()) {
                this.setControlValue('DebriefToDate', new Date());
            }
            this.fetchRecord();
        }
        this.buildGrid();
        this.riGridBeforeExecute();
        /**
         * If DebriefNumber is populated we are looking at an existing Debrief,
         * which cannot be amended, otherwise allow Save to create new Debrief
         */
        if (!this.fieldHasValue('DebriefNumber')) {
            this.isUpdateMode = true;
            this.disableControl('DebriefTypeCode', false);
            this.setControlValue('DebriefTypeCode', '1');
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('ServicePlanNumber', 'Grid', 'ServicePlanNumber', MntConst.eTypeInteger, 10, false);
        this.riGrid.AddColumnAlign('ServicePlanNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServicePlanStartDate', 'Grid', 'ServicePlanStartDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServicePlanStartDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServicePlanEndDate', 'Grid', 'ServicePlanEndDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServicePlanEndDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServicePlanStatusDesc', 'Grid', 'ServicePlanStatusDesc', MntConst.eTypeText, 30);
        this.riGrid.AddColumnAlign('ServicePlanStatusDesc', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('UnactionedItems', 'Grid', 'UnactionedItems', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('UnactionedItems', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ClosedInd', 'Grid', 'ClosedInd', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('ClosedInd', MntConst.eAlignmentCenter);
        this.riGrid.Complete();
        this.pageParams.gridHandle = this.utils.randomSixDigitString();
    }

    private riGridBeforeExecute(): void {
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '2');
        this.search.set('BranchNumber', this.utils.getBranchCode());
        this.search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        this.search.set('EmployeeCode', this.getControlValue('EmployeeCode'));
        this.search.set('DebriefNumber', this.getControlValue('DebriefNumber'));
        this.search.set('DebriefToDate', this.getControlValue('DebriefToDate'));
        this.search.set(this.serviceConstants.GridMode, '0');
        this.search.set(this.serviceConstants.GridHandle, this.pageParams.gridHandle);
        this.search.set(this.serviceConstants.GridCacheRefresh, 'True');
        this.search.set(this.serviceConstants.PageSize, '10');
        this.search.set(this.serviceConstants.GridPageCurrent, this.pageCurrent.toString());
        this.search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        this.search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, this.search)
            .subscribe(
            (data) => {
                if (data) {
                    this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                    this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.Execute(data);
                    }
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            error => {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                this.totalRecords = 1;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private fetchRecord(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '6');
        let formData = {
            Function: 'GetFields',
            BranchNumber: this.utils.getBranchCode(),
            EmployeeCode: this.getControlValue('EmployeeCode'),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            DebriefFromDate: this.getControlValue('DebriefFromDate'),
            DebriefToDate: this.getControlValue('DebriefToDate')
        };
        if (this.fieldHasValue('DebriefNumber')) {
            formData['DebriefNumber'] = this.getControlValue('DebriefNumber');
        }
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('DebriefTypeCode', data['DebriefTypeCode']);
                    this.setControlValue('PlannedVisitsNumber', data['PlannedVisitsNumber']);
                    this.setControlValue('CompletedVisitsNumber', data['CompletedVisitsNumber']);
                    this.setControlValue('ManualVisitsNumber', data['ManualVisitsNumber']);
                    this.setControlValue('MissedVisitsNumber', data['MissedVisitsNumber']);
                    this.setControlValue('UnactionedVisitsNumber', data['UnactionedVisitsNumber']);
                    this.setControlValue('AddedVisitsNumber', data['AddedVisitsNumber']);
                    this.setControlValue('CancelledVisitsNumber', data['CancelledVisitsNumber']);
                    this.setControlValue('RejectedVisitsNumber', data['RejectedVisitsNumber']);
                    this.setControlValue('ExpectedScansNumber', data['ExpectedScansNumber']);
                    this.setControlValue('CapturedScansNumber', data['CapturedScansNumber']);
                    this.setControlValue('RegisteredScansNumber', data['RegisteredScansNumber']);
                    this.setControlValue('RejectedScansNumber', data['RejectedScansNumber']);
                    this.setControlValue('ExpectedSignaturesNumber', data['ExpectedSignaturesNumber']);
                    this.setControlValue('CapturedSignaturesNumber', data['CapturedSignaturesNumber']);
                    this.setControlValue('RejectedSignaturesNumber', data['RejectedSignaturesNumber']);
                    this.setControlValue('LeadsNumber', data['LeadsNumber']);
                    this.setControlValue('AlertsNumber', data['AlertsNumber']);
                    this.setControlValue('ActionsNumber', data['ActionsNumber']);
                    this.setControlValue('DaysWorkNumber', data['DaysWorkNumber']);
                    this.setControlValue('DebriefUserCode', data['DebriefUserCode']);
                    if (this.isUpdateMode && !this.fieldHasValue('DebriefTypeCode')) {
                        this.setControlValue('DebriefTypeCode', '1');
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
            });
    }

    private cancelServicePlan(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '6');
        let formData = {
            Function: 'CancelServicePlan',
            ServicePlanRowID: this.getAttribute('ServicePlanRowID')
        };
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGridBeforeExecute();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
            });
    }

    private saveServicePlan(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '2');
        let formData = {
            Function: 'GetFields',
            BranchNumber: this.utils.getBranchCode(),
            EmployeeCode: this.getControlValue('EmployeeCode'),
            DebriefNumber: this.getControlValue('DebriefNumber'),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            DebriefTypeCode: this.getControlValue('DebriefTypeCode'),
            VehicleInspectedInd: this.getControlValue('VehicleInspectedInd'),
            VehicleStartMileage: this.getControlValue('VehicleStartMileage'),
            VehicleEndMileage: this.getControlValue('VehicleEndMileage'),
            PlannedVisitsNumber: this.getControlValue('PlannedVisitsNumber'),
            CompletedVisitsNumber: this.getControlValue('CompletedVisitsNumber'),
            ManualVisitsNumber: this.getControlValue('ManualVisitsNumber'),
            MissedVisitsNumber: this.getControlValue('MissedVisitsNumber'),
            UnactionedVisitsNumber: this.getControlValue('UnactionedVisitsNumber'),
            AddedVisitsNumber: this.getControlValue('AddedVisitsNumber'),
            CancelledVisitsNumber: this.getControlValue('CancelledVisitsNumber'),
            RejectedVisitsNumber: this.getControlValue('RejectedVisitsNumber'),
            ExpectedScansNumber: this.getControlValue('ExpectedScansNumber'),
            CapturedScansNumber: this.getControlValue('CapturedScansNumber'),
            RegisteredScansNumber: this.getControlValue('RegisteredScansNumber'),
            RejectedScansNumber: this.getControlValue('RejectedScansNumber'),
            ExpectedSignaturesNumber: this.getControlValue('ExpectedSignaturesNumber'),
            CapturedSignaturesNumber: this.getControlValue('CapturedSignaturesNumber'),
            RejectedSignaturesNumber: this.getControlValue('RejectedSignaturesNumber'),
            LeadsNumber: this.getControlValue('LeadsNumber'),
            AlertsNumber: this.getControlValue('AlertsNumber'),
            ActionsNumber: this.getControlValue('ActionsNumber'),
            DebriefFromDate: this.getControlValue('DebriefFromDate'),
            DebriefToDate: this.getControlValue('DebriefToDate'),
            DaysWorkNumber: this.getControlValue('DaysWorkNumber'),
            DebriefUserCode: this.getControlValue('DebriefUserCode')
        };
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('DebriefNumber', data['DebriefNumber']);
                    this.setControlValue('DebriefUserCode', data['DebriefUserCode']);
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                    this.isUpdateMode = false;
                    this.disableControl('DebriefTypeCode', true);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
            });
    }

    public tbodyGridOnDblClick(event: any): void {
        if (event.srcElement.style.cursor !== true) {  // hand
            this.setAttribute('ServicePlanRowID', this.riGrid.Details.GetAttribute('ClosedInd', 'AdditionalProperty'));
            this.cancelServicePlan();
        }
    }

    public renderTab(tabindex: number, markRed?: boolean): void {
        let elem = document.querySelector('.nav-tabs').children;
        for (let i = 0; i < elem.length; i++) {
            this.utils.removeClass(elem[i], 'active');
            this.utils.removeClass(document.querySelector('.tab-content').children[i], 'active');
        }
        switch (tabindex) {
            case 1:
                this.uiDisplay.tab.tab1.active = true;
                this.uiDisplay.tab.tab2.active = false;
                break;
            case 2:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = true;
                break;
        }
        this.pageParams.tabIndex = tabindex;
        this.utils.addClass(elem[tabindex - 1], 'active');
        this.utils.addClass(document.querySelector('.tab-content').children[tabindex - 1], 'active');
    }

    public refresh(): void {
        this.riGridBeforeExecute();
    }

    public riGridSort(event: any): void {
        this.riGridBeforeExecute();
    }

    public getCurrentPage(event: any): void {
        if (this.pageCurrent !== event.value) {
            this.pageCurrent = event.value;
            this.riGridBeforeExecute();
        }
    }

    public handleCancelClick(): void {
        this.isUpdateMode = false;
        if (!this.fieldHasValue('DebriefTypeCode')) {
            this.setControlValue('DebriefTypeCode', '1');
        }
        this.disableControl('DebriefTypeCode', true);
    }

    public handleSaveClick(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveServicePlan.bind(this)));
    }
}
