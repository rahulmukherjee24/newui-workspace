import 'rxjs/add/operator/takeWhile';
import { Component, OnInit, OnDestroy, Injector, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { Subscription } from 'rxjs/Subscription';

import { BaseComponent } from '@base/BaseComponent';
import { BranchServiceAreaSearchComponent } from '@internal/search/iCABSBBranchServiceAreaSearch';
import { ContractSearchComponent } from '@internal/search/iCABSAContractSearch';
import { EmployeeSearchComponent } from '@internal/search/iCABSBEmployeeSearch';
import { GlobalizeService } from '@shared/services/globalize.service';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { InternalGridSearchModuleRoutes } from '@base/PageRoutes';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PremiseSearchComponent } from '@internal/search/iCABSAPremiseSearch';
import { StaticUtils } from '@shared/services/static.utility';

@Component({
    templateUrl: 'iCABSSeManualWasteConsignmentNote.html'
})

export class SeManualWasteConsignmentNoteComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private currentContractType: string = '';
    private currentContractTypeLabel: string = '';
    private currentContractTypeURLParameter: string = '';
    private currentMode: string = 'normal';
    private headerParams: any = {
        method: 'service-delivery/maintenance',
        operation: 'Service/iCABSSeManualWasteConsignmentNote',
        module: 'waste'
    };
    private isAlive: boolean = true;
    private isEligbleForNormalMode: boolean = true;
    private isWasteConsignmentVoid: boolean = false;
    private lookUpSubscription: Subscription;
    private manualWasteConsignmentNote: string = '';
    private postSearchParams: QueryParams = new QueryParams();
    private search: QueryParams = new QueryParams();
    private systemUniqueNumber: string = '';
    private wasteConsignmentNoteHeaderROWID: string = '';
    private wasteConsignmentVoid: string = '';

    public branchServiceAreaSearchComponent: any;
    public controls = [
        { name: 'BranchName', disabled: true },
        { name: 'BranchServiceAreaCode', disabled: true, required: true },
        { name: 'BranchServiceAreaDesc', disabled: true },
        { name: 'ConsigneeAddressLine1', disabled: true },
        { name: 'ConsigneeAddressLine2', disabled: true },
        { name: 'ConsigneeAddressLine3', disabled: true },
        { name: 'ConsigneeAddressLine4', disabled: true },
        { name: 'ConsigneeAddressLine5', disabled: true },
        { name: 'ConsigneeName', disabled: true },
        { name: 'ConsigneePostCode', disabled: true },
        { name: 'ContractName', disabled: true, required: true },
        { name: 'ContractNumber', disabled: false, required: true },
        { name: 'DepotAddressLine1', disabled: true },
        { name: 'DepotAddressLine2', disabled: true },
        { name: 'DepotAddressLine3', disabled: true },
        { name: 'DepotAddressLine4', disabled: true },
        { name: 'DepotAddressLine5', disabled: true },
        { name: 'DepotName', disabled: true },
        { name: 'DepotNumber', disabled: true, required: true },
        { name: 'DepotPostCode', disabled: true },
        { name: 'EmployeeCode', disabled: true },
        { name: 'EmployeeForename', disabled: true },
        { name: 'EmployeeSurname', disabled: true },
        { name: 'PremiseName', disabled: true, required: true },
        { name: 'PremiseNumber', disabled: false, required: true },
        { name: 'PrenotificationNumber', disabled: true },
        { name: 'RegulatoryAuthorityName', disabled: true },
        { name: 'RegulatoryAuthorityNumber', disabled: true, required: true },
        { name: 'ServiceBranchNumber', disabled: true },
        { name: 'SICCode', disabled: true },
        { name: 'SICDescription', disabled: true },
        { name: 'VehicleRegistration', disabled: true },
        { name: 'VisitDueDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'wasteConsignmentNoteHeaderROWID' },
        { name: 'WasteConsignmentNoteNumber', disabled: false, required: true },
        { name: 'WasteConsignmentVoid', disabled: true },
        { name: 'WasteOperationCode', disabled: true },
        { name: 'WasteRegulatoryPremiseRef', disabled: true },
        { name: 'WasteTransferTypeCode', disabled: true, required: true },
        { name: 'WasteTransferTypeDesc', disabled: true }
    ];
    public contractSearchComponent: any;
    public ellipsisConfig: any = {
        contractSearchParams: {
            parentMode: 'LookUp'
        },
        premiseSearchParams: {
            parentMode: 'LookUp',
            ContractNumber: '',
            ContractName: ''
        },
        branchServiceAreaSearchParams: {
            parentMode: 'LookUp-Waste',
            ServiceBranchNumber: '',
            BranchName: ''
        },
        employeeSearchParams: {
            parentMode: 'LookUp-Waste',
            serviceBranchNumber: ''
        },
        WasteTransferTypeEllipsisConfig: {
            autoOpen: false,
            ellipsisTitle: 'Waste Transfer Type Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp'
            },
            httpConfig: {
                operation: 'Business/iCABSBWasteTransferTypeSearch',
                module: 'waste',
                method: 'service-delivery/search'
            },
            tableColumn: [
                { title: 'Waste Transfer Type Code', name: 'WasteTransferTypeCode', type: MntConst.eTypeCode },
                { title: 'Description', name: 'WasteTransferTypeDesc', type: MntConst.eTypeText },
                { title: 'Waste Consignment Note Required', name: 'WasteConsignmentNoteRequiredIn', type: MntConst.eTypeCheckBox }
            ],
            disable: false
        },
        depotSearchParams: {
            ellipsisTitle: 'Depot Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp'
            },
            httpConfig: {
                operation: 'Business/iCABSBDepotSearch',
                module: 'depot',
                method: 'service-delivery/search'
            },
            tableColumn: [
                { title: 'Depot Number', name: 'DepotNumber', type: MntConst.eTypeInteger },
                { title: 'Depot Name', name: 'DepotName', type: MntConst.eTypeText }
            ],
            disable: false
        }
    };
    public employeeSearchComponent: any;
    public isAddDisabled: boolean = false;
    public isBranchServiceAreaDisabled: boolean = false;
    public isContractNumberDisabled: boolean = false;
    public isDepotSearchDisabled: boolean = false;
    public isDropDownDisabled: boolean = true;
    public isEmployeeSearchDisabled: boolean = false;
    public isFormSaveDisabled: boolean = false;
    public isPremiseNumberDisabled: boolean = false;
    public isSaveCancelOptionDisabled: boolean = true;
    public isVisitDueDateDisabled: boolean = true;
    public lastUpdatedVisitDueDate: Date = null;
    public pageId: string = '';
    public premiseSearchComponent: any;
    public uiDisplay: any = {
        tab: {
            tab1: { visible: true, active: true },
            tab2: { visible: true, active: false },
            tab3: { visible: true, active: false }
        }
    };
    public visitDueDate: Date = null;

    constructor(injector: Injector, private el: ElementRef,
        public globalize: GlobalizeService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEMANUALWASTECONSIGNMENTNOTE;
        this.pageTitle = this.browserTitle = 'Waste Consignment Note Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.contractSearchComponent = ContractSearchComponent;
        this.premiseSearchComponent = PremiseSearchComponent;
        this.branchServiceAreaSearchComponent = BranchServiceAreaSearchComponent;
        this.employeeSearchComponent = EmployeeSearchComponent;
        if (this.getControlValue('WasteConsignmentNoteNumber')) {
            this.fetchWasteConsignmentNoteNumberDetails();
        }
        this.windowOnload();

        this.activatedRouteSubscription = this.activatedRoute.queryParams.subscribe(
            (param: any) => {
                if (param) {
                    if (param.hasOwnProperty('mode') && param['mode'] === 'void') {
                        this.pageParams.isVoidMode = true;
                        this.disableControl('WasteConsignmentVoid', false);
                    } else {
                        this.pageParams.isVoidMode = false;
                        this.disableControl('WasteConsignmentVoid', true);
                    }
                }
            });
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.doCleanUp();
    }

    /** Clean the memory and unsubscribe all subscriptions */
    private doCleanUp(): void {
        this.serviceConstants = null;
        this.httpService = null;
        this.errorService = null;
        this.utils = null;
        this.ajaxSource = null;
        this.isAlive = false;
        this.lookUpSubscription = null;
    }

    /** Lookup for waste details */
    private lookUpManualWasteDetails(): void {
        let lookupIP: any = [
            {
                'table': 'Contract',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ContractNumber': this.getControlValue('ContractNumber')
                },
                'fields': ['ContractName']
            },
            {
                'table': 'Premise',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'PremiseNumber': this.getControlValue('PremiseNumber'),
                    'ContractNumber': this.getControlValue('ContractNumber')
                },
                'fields': ['PremiseName']
            },
            {
                'table': 'WasteTransferType',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'WasteTransferTypeCode': this.getControlValue('WasteTransferTypeCode')
                },
                'fields': ['WasteTransferTypeDesc']
            },
            {
                'table': 'Branch',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'BranchNumber': this.getControlValue('ServiceBranchNumber')
                },
                'fields': ['BranchName']
            },
            {
                'table': 'BranchServiceArea',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'BranchNumber': this.getControlValue('ServiceBranchNumber'),
                    'BranchServiceAreaCode': this.getControlValue('BranchServiceAreaCode')
                },
                'fields': ['BranchServiceAreaDesc']
            },
            {
                'table': 'RegulatoryAuthority',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'RegulatoryAuthorityNumber': this.getControlValue('RegulatoryAuthorityNumber')
                },
                'fields': ['RegulatoryAuthorityName']
            }
        ];
        // "takeWhile" is not a function error hence removing for the time being IUI-23076
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data[0] && data[0][0]) {
                this.setControlValue('ContractName', data[0][0]['ContractName']);
            }
            if (data[1] && data[1][0]) {
                this.setControlValue('PremiseName', data[1][0]['PremiseName']);
            }
            if (data[2] && data[2][0]) {
                this.setControlValue('WasteTransferTypeDesc', data[2][0]['WasteTransferTypeDesc']);
            }
            if (data[3] && data[3][0]) {
                this.setControlValue('BranchName', data[3][0]['BranchName']);
            }
            if (data[4] && data[4][0]) {
                this.setControlValue('BranchServiceAreaDesc', data[4][0]['BranchServiceAreaDesc']);
            }
            if (data[5] && data[5][0]) {
                this.setControlValue('RegulatoryAuthorityName', data[5][0]['RegulatoryAuthorityName']);
            }
            this.ellipsisConfig.branchServiceAreaSearchParams.ServiceBranchNumber = this.getControlValue('ServiceBranchNumber');
            this.ellipsisConfig.branchServiceAreaSearchParams.BranchName = this.getControlValue('BranchName');
            this.ellipsisConfig.employeeSearchParams.serviceBranchNumber = this.getControlValue('ServiceBranchNumber');
        });
    }

    private windowOnload(): void {
        this.buildTabs();
        this.isBranchServiceAreaDisabled = true;
        this.isEmployeeSearchDisabled = true;
        this.ellipsisConfig.WasteTransferTypeEllipsisConfig.disable = true;
        this.isDepotSearchDisabled = true;
    }

    /** Render the tabs */
    private buildTabs(): void {
        this.renderTab(1);
    }

    /** Set the fields value after data fetch */
    private setFieldsValue(data: any): void {
        if (data) {
            this.setControlValue('ServiceBranchNumber', data['ServiceBranchNumber']);
            this.setControlValue('BranchServiceAreaCode', data['BranchServiceAreaCode']);
            this.setControlValue('EmployeeCode', data['EmployeeCode']);
            this.setControlValue('EmployeeForename', data['EmployeeForename']);
            this.setControlValue('EmployeeSurname', data['EmployeeSurname']);
            this.setControlValue('RegulatoryAuthorityNumber', data['RegulatoryAuthorityNumber']);
            this.setControlValue('WasteTransferTypeCode', data['WasteTransferTypeCode']);
            this.setControlValue('WasteRegulatoryPremiseRef', data['WasteRegulatoryPremiseRef']);
            this.setControlValue('PrenotificationNumber', data['PrenotificationNumber']);
            this.setControlValue('SICCode', data['SICCode']);
            this.setControlValue('SICDescription', data['SICDescription']);
            this.setControlValue('VisitDueDate', data['VisitDueDate']);
            this.visitDueDate = new Date(data['VisitDueDate']);
            this.lastUpdatedVisitDueDate = new Date(data['VisitDueDate']);
            this.setControlValue('VehicleRegistration', data['VehicleRegistration']);
            this.setControlValue('ConsigneeName', data['ConsigneeName']);
            this.setControlValue('ConsigneeAddressLine1', data['ConsigneeAddressLine1']);
            this.setControlValue('ConsigneeAddressLine2', data['ConsigneeAddressLine2']);
            this.setControlValue('ConsigneeAddressLine3', data['ConsigneeAddressLine3']);
            this.setControlValue('ConsigneeAddressLine4', data['ConsigneeAddressLine4']);
            this.setControlValue('ConsigneeAddressLine5', data['ConsigneeAddressLine5']);
            this.setControlValue('ConsigneePostCode', data['ConsigneePostCode']);
            this.setControlValue('DepotNumber', data['DepotNumber']);
            this.setControlValue('DepotName', data['DepotName']);
            this.setControlValue('DepotAddressLine1', data['DepotAddressLine1']);
            this.setControlValue('DepotAddressLine2', data['DepotAddressLine2']);
            this.setControlValue('DepotAddressLine3', data['DepotAddressLine3']);
            this.setControlValue('DepotAddressLine4', data['DepotAddressLine4']);
            this.setControlValue('DepotAddressLine5', data['DepotAddressLine5']);
            this.setControlValue('DepotPostCode', data['DepotPostCode']);
            this.setControlValue('WasteOperationCode', data['WasteOperationCode']);
            if (data.hasOwnProperty('WasteConsignmentVoid')) {
                this.setControlValue('WasteConsignmentVoid', StaticUtils.convertResponseValueToCheckboxInput(data['WasteConsignmentVoid']));
                this.setFormSaveMode();
            }
        }
    }

    /** Get the default values */
    private getDefaultValues(): void {
        let formData: Object = {};
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
        formData[this.serviceConstants.Function] = 'DefaultValues';
        formData['BusinessCode'] = this.businessCode();
        this.postSearchParams.set(this.serviceConstants.Action, '6');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    }
                    else {
                        this.setControlValue('BranchName', e['BranchName']);
                        this.setControlValue('BranchServiceAreaCode', e['BranchServiceAreaCode']);
                        this.setControlValue('ConsigneeAddressLine1', e['ConsigneeAddressLine1']);
                        this.setControlValue('ConsigneeAddressLine2', e['ConsigneeAddressLine2']);
                        this.setControlValue('ConsigneeAddressLine3', e['ConsigneeAddressLine3']);
                        this.setControlValue('ConsigneeAddressLine4', e['ConsigneeAddressLine4']);
                        this.setControlValue('ConsigneeAddressLine5', e['ConsigneeAddressLine5']);
                        this.setControlValue('ConsigneeName', e['ConsigneeName']);
                        this.setControlValue('ConsigneePostCode', e['ConsigneePostCode']);
                        this.setControlValue('DepotAddressLine1', e['DepotAddressLine1']);
                        this.setControlValue('DepotAddressLine2', e['DepotAddressLine2']);
                        this.setControlValue('DepotAddressLine3', e['DepotAddressLine3']);
                        this.setControlValue('DepotAddressLine4', e['DepotAddressLine4']);
                        this.setControlValue('DepotAddressLine5', e['DepotAddressLine5']);
                        this.setControlValue('DepotName', e['DepotName']);
                        this.setControlValue('DepotNumber', e['DepotNumber']);
                        this.setControlValue('DepotPostCode', e['DepotPostCode']);
                        this.setControlValue('EmployeeCode', e['EmployeeCode']);
                        this.setControlValue('PremiseName', e['PremiseName']);
                        this.setControlValue('RegulatoryAuthorityNumber', e['RegulatoryAuthorityNumber']);
                        this.setControlValue('SICCode', e['SICCode']);
                        this.setControlValue('SICDescription', e['SICDescription']);
                        this.setControlValue('ServiceBranchNumber', e['ServiceBranchNumber']);
                        this.setControlValue('VehicleRegistration', e['VehicleRegistration']);
                        let d: any = new Date();
                        let day: any = (d.getDate() < 10) ? ('0' + d.getDate()) : d.getDate();
                        let month0: any = d.getMonth() + 1;
                        let month: any = (month0 < 10) ? ('0' + month0) : month0;
                        let year: any = d.getFullYear();
                        let visitDueDate: string = day + '/' + month + '/' + year;
                        this.lastUpdatedVisitDueDate = new Date(visitDueDate);
                        this.setControlValue('VisitDueDate', visitDueDate);
                        this.visitDueDate = new Date(visitDueDate);
                        this.setControlValue('WasteOperationCode', e['WasteOperationCode']);
                        this.setControlValue('WasteRegulatoryPremiseRef', e['WasteRegulatoryPremiseRef']);
                        this.setControlValue('WasteTransferTypeCode', e['WasteTransferTypeCode']);
                        this.getSICDescription();
                        this.getWasteTransferTypeDesc();
                        this.ellipsisConfig.branchServiceAreaSearchParams.ServiceBranchNumber = this.getControlValue('ServiceBranchNumber');
                        this.ellipsisConfig.branchServiceAreaSearchParams.BranchName = this.getControlValue('BranchName');
                        this.ellipsisConfig.employeeSearchParams.serviceBranchNumber = this.getControlValue('ServiceBranchNumber');
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    /** Get the SIC Description */
    private getSICDescription(): void {
        let formData: Object = {};
        formData['SICCode'] = this.getControlValue('SICCode');
        formData[this.serviceConstants.Function] = 'GetSICDescription';
        formData['BusinessCode'] = this.businessCode();
        this.postSearchParams.set(this.serviceConstants.Action, '6');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    }
                    else {
                        this.setControlValue('SICDescription', e['SICDescription']);
                        this.getRegulatoryAuthorityName();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private getRegulatoryAuthorityName(): void {
        let formData: Object = {};
        formData['RegulatoryAuthorityNumber'] = this.getControlValue('RegulatoryAuthorityNumber');
        formData[this.serviceConstants.Function] = 'GetRegulatoryAuthorityName';
        formData['BusinessCode'] = this.businessCode();
        this.postSearchParams.set(this.serviceConstants.Action, '6');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    } else {
                        this.setControlValue('RegulatoryAuthorityName', e['RegulatoryAuthorityName']);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private switchToAddMode(): void {
        this.currentMode = 'add';
        this.isAddDisabled = true;
        this.isSaveCancelOptionDisabled = false;
        this.isDropDownDisabled = true;
        this.clearAllFields();
        this.riExchange.riInputElement.Enable(this.uiForm, 'ContractNumber');
        this.riExchange.riInputElement.Enable(this.uiForm, 'PremiseNumber');
        this.isContractNumberDisabled = false;
        this.isPremiseNumberDisabled = false;
    }

    private switchToUpdateMode(): void {
        this.currentMode = 'update';
        this.isAddDisabled = false;
        this.isSaveCancelOptionDisabled = false;
        this.isDropDownDisabled = false;
        this.riExchange.riInputElement.Disable(this.uiForm, 'ContractNumber');
        this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseNumber');
        this.riExchange.riInputElement.Disable(this.uiForm, 'WasteConsignmentNoteNumber');
        this.isContractNumberDisabled = true;
        this.isPremiseNumberDisabled = true;
        this.beforeAddUpdate();
    }

    private beforeAddUpdate(): void {
        this.riExchange.riInputElement.Enable(this.uiForm, 'BranchServiceAreaCode');
        this.riExchange.riInputElement.Enable(this.uiForm, 'EmployeeCode');
        this.riExchange.riInputElement.Enable(this.uiForm, 'WasteTransferTypeCode');
        this.isVisitDueDateDisabled = false;
        this.riExchange.riInputElement.Enable(this.uiForm, 'VehicleRegistration');
        this.riExchange.riInputElement.Enable(this.uiForm, 'DepotNumber');
        this.riExchange.riInputElement.Enable(this.uiForm, 'ConsigneeName');
        this.riExchange.riInputElement.Enable(this.uiForm, 'ConsigneeAddressLine1');
        this.riExchange.riInputElement.Enable(this.uiForm, 'ConsigneeAddressLine2');
        this.riExchange.riInputElement.Enable(this.uiForm, 'ConsigneeAddressLine3');
        this.riExchange.riInputElement.Enable(this.uiForm, 'ConsigneeAddressLine4');
        this.riExchange.riInputElement.Enable(this.uiForm, 'ConsigneeAddressLine5');
        this.riExchange.riInputElement.Enable(this.uiForm, 'ConsigneePostCode');
        this.riExchange.riInputElement.Enable(this.uiForm, 'DepotNumber');
        // Enable the ellipsis
        this.isBranchServiceAreaDisabled = false;
        this.isEmployeeSearchDisabled = false;
        this.ellipsisConfig.WasteTransferTypeEllipsisConfig.disable = false;
        this.isDepotSearchDisabled = false;
    }

    private clearAllFields(): void {
        let uiElementName: string = '';
        this.isVisitDueDateDisabled = true;
        this.visitDueDate = this.lastUpdatedVisitDueDate;
        for (let i: number = 0; i < this.controls.length; i++) {
            uiElementName = this.controls[i]['name'];
            this.setControlValue(uiElementName, '');
            this.riExchange.riInputElement.Disable(this.uiForm, uiElementName);
        }
        // Disable the ellipsis
        this.isBranchServiceAreaDisabled = true;
        this.isEmployeeSearchDisabled = true;
        this.ellipsisConfig.WasteTransferTypeEllipsisConfig.disable = true;
        this.isDepotSearchDisabled = true;
    }

    private switchToNormalMode(): void {
        this.currentMode = 'normal';
        this.isAddDisabled = false;
        this.isSaveCancelOptionDisabled = true;
        this.isDropDownDisabled = true;
        this.clearAllFields();
        this.riExchange.riInputElement.Enable(this.uiForm, 'ContractNumber');
        this.riExchange.riInputElement.Enable(this.uiForm, 'PremiseNumber');
        this.riExchange.riInputElement.Enable(this.uiForm, 'WasteConsignmentNoteNumber');
        this.isContractNumberDisabled = false;
        this.isPremiseNumberDisabled = false;
        this.visitDueDate = null;
        this.lastUpdatedVisitDueDate = null;
    }

    public getDepotValues(): void {
        let depotNumber: string = this.getControlValue('DepotNumber');
        if (depotNumber !== '') {
            let formData: Object = {};
            formData['DepotNumber'] = depotNumber;
            formData[this.serviceConstants.Function] = 'GetDepotValues';
            formData['BusinessCode'] = this.businessCode();
            this.postSearchParams.set(this.serviceConstants.Action, '6');
            this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
            this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
                .takeWhile(() => this.isAlive).subscribe(
                    (e) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (e.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        }
                        else {
                            this.setControlValue('DepotName', e['DepotName']);
                            this.setControlValue('DepotAddressLine1', e['DepotAddressLine1']);
                            this.setControlValue('DepotAddressLine2', e['DepotAddressLine2']);
                            this.setControlValue('DepotAddressLine3', e['DepotAddressLine3']);
                            this.setControlValue('DepotAddressLine4', e['DepotAddressLine4']);
                            this.setControlValue('DepotAddressLine5', e['DepotAddressLine5']);
                            this.setControlValue('DepotPostCode', e['DepotPostCode']);
                            this.setControlValue('WasteOperationCode', e['WasteOperationCode']);
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        } else {
            this.setControlValue('DepotName', '');
            this.setControlValue('DepotAddressLine1', '');
            this.setControlValue('DepotAddressLine2', '');
            this.setControlValue('DepotAddressLine3', '');
            this.setControlValue('DepotAddressLine4', '');
            this.setControlValue('DepotAddressLine5', '');
            this.setControlValue('DepotPostCode', '');
            this.setControlValue('WasteOperationCode', '');
        }
    }

    public getEmployeeName(): void {
        let employeeCode: string = this.getControlValue('EmployeeCode');
        if (employeeCode !== '') {
            let formData: Object = {};
            formData['EmployeeCode'] = employeeCode;
            formData[this.serviceConstants.Function] = 'GetEmployeeName';
            formData['BusinessCode'] = this.businessCode();
            this.postSearchParams.set(this.serviceConstants.Action, '6');
            this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
            this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
                .takeWhile(() => this.isAlive).subscribe(
                    (e) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (e.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        }
                        else {
                            this.setControlValue('EmployeeForename', e['EmployeeForename']);
                            this.setControlValue('EmployeeSurname', e['EmployeeSurname']);
                            this.setControlValue('VehicleRegistration', e['VehicleRegistration']);
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        } else {
            this.setControlValue('EmployeeForename', '');
            this.setControlValue('EmployeeSurname', '');
            this.setControlValue('VehicleRegistration', '');
        }
    }

    public renderTab(tabindex: number): void {
        switch (tabindex) {
            case 1:
                this.uiDisplay.tab.tab1.active = true;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = false;

                break;
            case 2:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = true;
                this.uiDisplay.tab.tab3.active = false;
                break;
            case 3:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = true;
                break;
        }
    }

    public visitDueDateSelectedValue(data: any): void {
        if (data && data.value) {
            this.setControlValue('VisitDueDate', data.value);
        }
    }

    public getContractNameFromNumber(): void {
        if (this.getControlValue('ContractNumber') === '') {
            this.setControlValue('ContractName', '');
            return;
        }
        this.setControlValue('ContractNumber', this.utils.numberPadding(this.getControlValue('ContractNumber'), 8));
        let formData: Object = {};
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData[this.serviceConstants.Function] = 'GetContractName';
        formData['BusinessCode'] = this.businessCode();
        this.postSearchParams.set(this.serviceConstants.Action, '6');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    }
                    else {
                        if (e['ContractName']) {
                            this.setControlValue('ContractName', e['ContractName']);
                            this.ellipsisConfig.premiseSearchParams.ContractNumber = this.getControlValue('ContractNumber');
                            this.ellipsisConfig.premiseSearchParams.ContractName = e['ContractName'];
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public premiseNameFromNumber(): void {
        if (this.getControlValue('PremiseNumber') === '') {
            this.setControlValue('PremiseName', '');
            return;
        }
        let formData: Object = {};
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
        formData[this.serviceConstants.Function] = 'GetPremiseName';
        formData['BusinessCode'] = this.businessCode();
        this.postSearchParams.set(this.serviceConstants.Action, '6');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    }
                    else {
                        if (e['PremiseName']) {
                            this.setControlValue('PremiseName', e['PremiseName']);
                            if (this.currentMode === 'add') {
                                this.getDefaultValues();
                                this.beforeAddUpdate();
                            }
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public fetchWasteConsignmentNoteNumberDetails(): void {
        this.formPristine();
        if (this.pageParams.isVoidMode) {
            this.getVoidWasteConsginmentDetails();
            this.isSaveCancelOptionDisabled = false;
            this.currentMode = 'update';
            return;
        }
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, this.pageParams.isVoidMode ? '6' : '0');
        this.search.set('ContractNumber', this.getControlValue('ContractNumber'));
        this.search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        this.search.set('WasteConsignmentNoteNumber', this.getControlValue('WasteConsignmentNoteNumber'));
        this.search.set(this.serviceConstants.Function, 'GetPremiseName');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module,
            this.headerParams.operation, this.search).takeWhile(() => this.isAlive)
            .subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        this.riExchange.riInputElement.Enable(this.uiForm, 'WasteConsignmentNoteNumber');
                    } else {
                        this.setFieldsValue(e);
                        this.lookUpManualWasteDetails();
                        this.switchToUpdateMode();
                        this.wasteConsignmentNoteHeaderROWID = e['WasteConsignmentNoteHeader'];
                        this.setControlValue('wasteConsignmentNoteHeaderROWID', e['WasteConsignmentNoteHeader']);
                        this.systemUniqueNumber = e['SystemUniqueNumber'];
                        this.manualWasteConsignmentNote = e['ManualWasteConsignmentNote'];
                        this.wasteConsignmentVoid = e['WasteConsignmentVoid'];
                        this.getVoidWasteConsginmentDetails();
                    }

                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private getVoidWasteConsginmentDetails(): void {
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '0');
        this.search.set('ContractNumber', this.getControlValue('ContractNumber'));
        this.search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        this.search.set('WasteConsignmentNoteNumber', this.getControlValue('WasteConsignmentNoteNumber'));
        this.search.set('VoidMode', 'true');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module,
            this.headerParams.operation, this.search).takeWhile(() => this.isAlive)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.riExchange.riInputElement.Enable(this.uiForm, 'WasteConsignmentNoteNumber');
                    } else {
                        this.setFieldsValue(data);
                        this.wasteConsignmentNoteHeaderROWID = data['WasteConsignmentNoteHeader'];
                        this.setControlValue('wasteConsignmentNoteHeaderROWID', data['WasteConsignmentNoteHeader']);
                        this.systemUniqueNumber = data['SystemUniqueNumber'];
                        this.isDropDownDisabled = false;
                    }

                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public onContractNumberSelectFromEllipsis(data: any): void {
        if (data) {
            if (this.getControlValue('ContractNumber') !== data['ContractNumber']) {
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ContractNumber');
            }
            this.setControlValue('ContractNumber', data['ContractNumber']);
            this.setControlValue('ContractName', data['ContractName']);
            this.ellipsisConfig.premiseSearchParams.ContractNumber = data['ContractNumber'];
            this.ellipsisConfig.premiseSearchParams.ContractName = data['ContractName'];
        }
    }

    public onPremiseNumberSelectFromEllipsis(data: any): void {
        if (data) {
            if (this.getControlValue('PremiseNumber') !== data['PremiseNumber']) {
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'PremiseNumber');
            }
            this.setControlValue('PremiseNumber', data['PremiseNumber']);
            this.setControlValue('PremiseName', data['PremiseName']);
            if (this.currentMode === 'add') {
                this.premiseNameFromNumber();
            }
        }
    }

    public onBranchServiceAreaSelectFromEllipsis(data: any): void {
        if (data) {
            if (this.getControlValue('BranchServiceAreaCode') !== data['BranchServiceAreaCode']) {
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'BranchServiceAreaCode');
            }
            this.setControlValue('BranchServiceAreaCode', data['BranchServiceAreaCode']);
            this.setControlValue('BranchServiceAreaDesc', data['BranchServiceAreaDesc']);
            this.setControlValue('EmployeeCode', data['EmployeeCode']);
            this.getEmployeeName();
        }
    }

    public onEmployeeSearchSelectFromEllipsis(data: any): void {
        if (data) {
            if (this.getControlValue('EmployeeCode') !== data['EmployeeCode']) {
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'EmployeeCode');
            }
            this.setControlValue('EmployeeCode', data['EmployeeCode']);
            this.getEmployeeName();
        }
    }

    public onWasteTransferTypeSearchSelectFromEllipsis(data: any): void {
        if (data) {
            if (this.getControlValue('WasteTransferTypeCode') !== data['WasteTransferTypeCode']) {
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'WasteTransferTypeCode');
            }
            this.setControlValue('WasteTransferTypeCode', data['WasteTransferTypeCode']);
            this.setControlValue('WasteTransferTypeDesc', data['WasteTransferTypeDesc']);
        }
    }

    public onDepotSearchSelectFromEllipsis(data: any): void {
        if (data) {
            if (this.getControlValue('DepotNumber') !== data['DepotNumber']) {
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'DepotNumber');
            }
            this.setControlValue('DepotNumber', data['DepotNumber']);
            this.setControlValue('DepotName', data['DepotName']);
            this.getDepotValues();
        }
    }

    public canDeactivate(): Observable<boolean> {
        this.routeAwayGlobals.setDirtyFlag(this.el.nativeElement.querySelector('form').classList.contains('ng-dirty'));
        return this.routeAwayComponent.canDeactivate();
    }

    public onCancelClicked(): void {
        if (this.isEligbleForNormalMode) {
            this.clearAllFields();
            this.switchToNormalMode();
            this.uiForm.controls['ContractNumber'].markAsUntouched();
            this.uiForm.controls['PremiseNumber'].markAsUntouched();
            this.uiForm.controls['WasteConsignmentNoteNumber'].markAsUntouched();
        } else {
            this.setControlValue('ContractNumber', this.search.get('ContractNumber'));
            this.setControlValue('PremiseNumber', this.search.get('PremiseNumber'));
            this.setControlValue('WasteConsignmentNoteNumber', this.search.get('WasteConsignmentNoteNumber'));
            this.fetchWasteConsignmentNoteNumberDetails();
        }
    }

    public onAddClicked(): void {
        this.switchToAddMode();
    }

    public onChangeFn(event: any): void {
        let target: any = event.target || event.srcElement || event.currentTarget;
        let value = target.value;
        if (value === 'WasteConsignmentNoteDetail') {
            this.navigate('ManualWasteConsignmentNote', InternalGridSearchModuleRoutes.ICABSSEWASTECONSIGNMENTNOTEGRID, {
                SystemUniqueNumber: this.systemUniqueNumber
            });
        }
    }

    /** Update the Manual Waste Consignment Details */
    public updateWasteConsignmentDetails(): void {
        let formData: Object = {};
        formData['WasteConsignmentNoteHeaderROWID'] = this.wasteConsignmentNoteHeaderROWID;
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
        formData['WasteConsignmentNoteNumber'] = this.getControlValue('WasteConsignmentNoteNumber');
        formData[this.serviceConstants.Function] = 'GetPremiseName';
        formData['BusinessCode'] = this.businessCode();
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['RegulatoryAuthorityNumber'] = this.getControlValue('RegulatoryAuthorityNumber');
        formData['SICCode'] = this.getControlValue('SICCode');
        formData['SICDescription'] = this.getControlValue('SICDescription');
        formData['WasteTransferTypeCode'] = this.getControlValue('WasteTransferTypeCode');
        formData['ConsigneeName'] = this.getControlValue('ConsigneeName');
        formData['ConsigneeAddressLine1'] = this.getControlValue('ConsigneeAddressLine1');
        formData['ConsigneeAddressLine2'] = this.getControlValue('ConsigneeAddressLine2');
        formData['ConsigneeAddressLine3'] = this.getControlValue('ConsigneeAddressLine3');
        formData['ConsigneeAddressLine4'] = this.getControlValue('ConsigneeAddressLine4');
        formData['ConsigneeAddressLine5'] = this.getControlValue('ConsigneeAddressLine5');
        formData['ConsigneePostCode'] = this.getControlValue('ConsigneePostCode');
        formData['ServiceBranchNumber'] = this.getControlValue('ServiceBranchNumber');
        formData['DepotNumber'] = this.getControlValue('DepotNumber');
        formData['DepotName'] = this.getControlValue('DepotName');
        formData['DepotAddressLine1'] = this.getControlValue('DepotAddressLine1');
        formData['DepotAddressLine2'] = this.getControlValue('DepotAddressLine2');
        formData['DepotAddressLine3'] = this.getControlValue('DepotAddressLine3');
        formData['DepotAddressLine4'] = this.getControlValue('DepotAddressLine4');
        formData['DepotAddressLine5'] = this.getControlValue('DepotAddressLine5');
        formData['DepotPostCode'] = this.getControlValue('DepotPostCode');
        formData['WasteOperationCode'] = this.getControlValue('WasteOperationCode');
        formData['ManualWasteConsignmentNote'] = this.manualWasteConsignmentNote;
        formData['VisitDueDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('VisitDueDate'));
        formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        formData['EmployeeSurname'] = this.getControlValue('EmployeeSurname');
        formData['EmployeeForename'] = this.getControlValue('EmployeeForename');
        formData['VehicleRegistration'] = this.getControlValue('VehicleRegistration');
        formData['SystemUniqueNumber'] = this.systemUniqueNumber;
        formData['PrenotificationNumber'] = this.getControlValue('PrenotificationNumber');
        formData['WasteRegulatoryPremiseRef'] = this.getControlValue('WasteRegulatoryPremiseRef');
        formData['WasteConsignmentVoid'] = this.getControlValue('WasteConsignmentVoid');
        formData['VoidMode'] = this.getControlValue('WasteConsignmentVoid');
        this.postSearchParams.set(this.serviceConstants.Action, '2');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    }
                    else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                        this.isEligbleForNormalMode = false;
                        this.el.nativeElement.querySelector('form').classList.remove('ng-dirty');
                        this.lastUpdatedVisitDueDate = new Date(this.getControlValue('VisitDueDate'));
                        this.setFormSaveMode();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    /** Add the consignment details */
    public addConsignmentDetails(): void {
        let formData: Object = {};
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
        formData['WasteConsignmentNoteNumber'] = this.getControlValue('WasteConsignmentNoteNumber');
        formData[this.serviceConstants.Function] = 'GetNextWasteConsignmentNoteNumber';
        formData['BusinessCode'] = this.businessCode();
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['RegulatoryAuthorityNumber'] = this.getControlValue('RegulatoryAuthorityNumber');
        formData['SICCode'] = this.getControlValue('SICCode');
        formData['SICDescription'] = this.getControlValue('SICDescription');
        formData['WasteTransferTypeCode'] = this.getControlValue('WasteTransferTypeCode');
        formData['ConsigneeName'] = this.getControlValue('ConsigneeName');
        formData['ConsigneeAddressLine1'] = this.getControlValue('ConsigneeAddressLine1');
        formData['ConsigneeAddressLine2'] = this.getControlValue('ConsigneeAddressLine2');
        formData['ConsigneeAddressLine3'] = this.getControlValue('ConsigneeAddressLine3');
        formData['ConsigneeAddressLine4'] = this.getControlValue('ConsigneeAddressLine4');
        formData['ConsigneeAddressLine5'] = this.getControlValue('ConsigneeAddressLine5');
        formData['ConsigneePostCode'] = this.getControlValue('ConsigneePostCode');
        formData['ServiceBranchNumber'] = this.getControlValue('ServiceBranchNumber');
        formData['DepotNumber'] = this.getControlValue('DepotNumber');
        formData['DepotName'] = this.getControlValue('DepotName');
        formData['DepotAddressLine1'] = this.getControlValue('DepotAddressLine1');
        formData['DepotAddressLine2'] = this.getControlValue('DepotAddressLine2');
        formData['DepotAddressLine3'] = this.getControlValue('DepotAddressLine3');
        formData['DepotAddressLine4'] = this.getControlValue('DepotAddressLine4');
        formData['DepotAddressLine5'] = this.getControlValue('DepotAddressLine5');
        formData['DepotPostCode'] = this.getControlValue('DepotPostCode');
        formData['WasteOperationCode'] = this.getControlValue('WasteOperationCode');
        formData['ManualWasteConsignmentNote'] = this.manualWasteConsignmentNote;
        formData['VisitDueDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('VisitDueDate'));
        formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        formData['EmployeeSurname'] = this.getControlValue('EmployeeSurname');
        formData['EmployeeForename'] = this.getControlValue('EmployeeForename');
        formData['VehicleRegistration'] = this.getControlValue('VehicleRegistration');
        formData['SystemUniqueNumber'] = this.systemUniqueNumber;
        formData['PrenotificationNumber'] = this.getControlValue('PrenotificationNumber');
        formData['WasteRegulatoryPremiseRef'] = this.getControlValue('WasteRegulatoryPremiseRef');
        formData['WasteConsignmentVoid'] = this.wasteConsignmentVoid;
        formData['VoidMode'] = this.wasteConsignmentVoid;
        this.postSearchParams.set(this.serviceConstants.Action, '1');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    }
                    else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                        this.search.set('ContractNumber', this.getControlValue('ContractNumber'));
                        this.search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
                        this.search.set('WasteConsignmentNoteNumber', this.getControlValue('WasteConsignmentNoteNumber'));
                        this.isEligbleForNormalMode = false;
                        this.switchToUpdateMode();
                        this.el.nativeElement.querySelector('form').classList.remove('ng-dirty');
                        this.lastUpdatedVisitDueDate = new Date(this.getControlValue('VisitDueDate'));
                        this.fetchWasteConsignmentNoteNumberDetails();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public getNextWasteConsignmentNoteNumber(): void {
        let formData: Object = {};
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
        formData['VisitDueDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('VisitDueDate'));
        formData[this.serviceConstants.Function] = 'GetNextWasteConsignmentNoteNumber';
        formData['BusinessCode'] = this.businessCode();
        this.postSearchParams.set(this.serviceConstants.Action, '6');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    }
                    else {
                        if (e['WasteConsignmentNoteNumber']) {
                            this.setControlValue('WasteConsignmentNoteNumber', e['WasteConsignmentNoteNumber']);
                            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.addConsignmentDetails.bind(this)));
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public onSaveClicked(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            if (this.currentMode === 'update') {
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.updateWasteConsignmentDetails.bind(this)));
            } else if (this.currentMode === 'add') {
                this.getNextWasteConsignmentNoteNumber();
            }
        }
    }

    public getBranchServiceAreaDesc(): void {
        if (this.getControlValue('BranchServiceAreaCode') === '') {
            this.setControlValue('BranchServiceAreaDesc', '');
            return;
        }
        let formData: Object = {};
        formData['ServiceBranchNumber'] = this.getControlValue('ServiceBranchNumber');
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData[this.serviceConstants.Function] = 'GetBranchServiceAreaDesc';
        formData['BusinessCode'] = this.businessCode();
        this.postSearchParams.set(this.serviceConstants.Action, '6');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    }
                    else {
                        if (e['BranchServiceAreaDesc']) {
                            this.setControlValue('BranchServiceAreaDesc', e['BranchServiceAreaDesc']);
                            this.getEmployeeName();
                            this.ellipsisConfig.branchServiceAreaSearchParams.ServiceBranchNumber = this.getControlValue('ServiceBranchNumber');
                            this.ellipsisConfig.branchServiceAreaSearchParams.BranchName = this.getControlValue('BranchName');
                            this.setControlValue('EmployeeCode', e['EmployeeCode']);
                            this.getEmployeeName();
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public getWasteTransferTypeDesc(): void {
        if (this.getControlValue('WasteTransferTypeCode') === '') {
            this.setControlValue('WasteTransferTypeDesc', '');
            return;
        }
        let formData: Object = {};
        formData['WasteTransferTypeCode'] = this.getControlValue('WasteTransferTypeCode');
        formData[this.serviceConstants.Function] = 'GetWasteTransferTypeDesc';
        formData['BusinessCode'] = this.businessCode();
        this.postSearchParams.set(this.serviceConstants.Action, '6');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    }
                    else {
                        if (e['WasteTransferTypeDesc']) {
                            this.setControlValue('WasteTransferTypeDesc', e['WasteTransferTypeDesc']);
                            this.getEmployeeName();
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private setFormSaveMode(): void {
        if (this.pageParams.isVoidMode) {
            this.isFormSaveDisabled = this.getControlValue('WasteConsignmentVoid');
            this.disableControl('WasteConsignmentVoid', this.isFormSaveDisabled);
        }
    }

    public onPrintClick(): void {
        let formData: Object = {};

        formData['WasteConsignmentNote'] = this.getControlValue('wasteConsignmentNoteHeaderROWID');
        formData[this.serviceConstants.Function] = 'Single';
        formData['BusinessCode'] = this.businessCode();

        this.postSearchParams = new QueryParams();
        this.postSearchParams.set(this.serviceConstants.Action, '0');
        this.postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.postSearchParams, formData).subscribe(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else if (data['url']) {
                window.open(data['url'], '_blank');
            }
        }, error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }
}
