import { Component, OnInit, OnDestroy, Injector, ViewChild, AfterContentInit } from '@angular/core';

import { BranchServiceAreaSearchComponent } from '@internal/search/iCABSBBranchServiceAreaSearch';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { EmployeeSearchComponent } from '../search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { Subscription } from 'rxjs/Rx';
import { InternalMaintenanceModuleRoutes } from '@app/base/PageRoutes';

@Component({
    templateUrl: 'iCABSSEUnreturnedConsignmentNotesGrid.html',
    providers: [CommonLookUpUtilsService]
})


export class UnreturnedConsignmentNotesGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public commonGridFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public lookUpSubscription: Subscription;
    public pageId: string;

    public gridConfig: Record<string, number> = {
        itemsPerPage: 10,
        totalItem: 1
    };
    private xhrParams: Record<string, string> = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Service/iCABSSEUnreturnedConsignmentNotesGrid'
    };

    public readonly consignmentNote: Array<Object> = [
        { key: 'All', value: 'all' },
        { key: 'Automatic', value: 'automatic' },
        { key: 'Manual', value: 'manual' }
    ];

    public readonly returnedStatus: Array<Object> = [
        { key: 'All', value: 'all' },
        { key: 'Non Returned', value: 'non-returned' },
        { key: 'Returned', value: 'returned' }
    ];

    public readonly noteStatus: Array<Object> = [
        { key: 'All', value: 'all' },
        { key: 'Non Void', value: 'non-void' },
        { key: 'New', value: 'new' },
        { key: 'Incomplete', value: 'incomplete' },
        { key: 'Complete', value: 'complete' },
        { key: 'Void', value: 'void' }
    ];

    public controls: IControls[] = [
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'EmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'FromDate', required: true, type: MntConst.eTypeDate },
        { name: 'ManualWasteConsignmentNote', type: MntConst.eTypeText, value: 'all' },
        { name: 'PaperworkReceivedDate', type: MntConst.eTypeDate },
        { name: 'ReturnedStatus', type: MntConst.eTypeText, value: 'non-returned' },
        { name: 'ToDate', required: true, type: MntConst.eTypeDate },
        { name: 'ToDate', required: true, type: MntConst.eTypeDate },
        { name: 'WasteConsignmentNoteNo', type: MntConst.eTypeCode },
        { name: 'WasteConsignmentNoteStatus', type: MntConst.eTypeText, value: 'non-void' },
        { name: 'WasteTransferTypeCode', type: MntConst.eTypeCode },
        { name: 'WasteTransferTypeDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'WasteTransferTypeDesc', type: MntConst.eTypeText }
    ];

    public ellipsis: Record<string, Record<string, Object>> = {
        serviceAreaEllipsis: {
            childConfigParams: {
                'parentMode': 'LookUp-UnreturnedConsignmentNotes'
            },
            component: BranchServiceAreaSearchComponent
        },
        employee: {
            showBranchLevel: false,
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: EmployeeSearchComponent
        },
        WasteTransferTypeEllipsisConfig: {
            autoOpen: false,
            ellipsisTitle: 'Waste Transfer Type Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp'
            },
            httpConfig: {
                operation: 'Business/iCABSBWasteTransferTypeSearch',
                module: 'waste',
                method: 'service-delivery/search'
            },
            tableColumn: [
                { title: 'Waste Transfer Type Code', name: 'WasteTransferTypeCode', type: MntConst.eTypeCode },
                { title: 'Description', name: 'WasteTransferTypeDesc', type: MntConst.eTypeText },
                { title: 'Waste Consignment Note Required', name: 'WasteConsignmentNoteRequiredIn', type: MntConst.eTypeCheckBox }
            ],
            disable: false
        }
    };


    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSSEMANUALWASTECONSIGNMENTNOTE;
        this.pageTitle = this.browserTitle = 'Unreturned Consignment Notes';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.buildGrid();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (this.isReturning()) {
            this.populateGrid();
        } else {
            this.getValues();

            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;
        }
    }
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('DocumentNumber', 'WasteConsignmentNote', 'DocumentNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('DocumentNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ConsignmentNoteNumber', 'WasteConsignmentNote', 'ConsignmentNoteNumber', MntConst.eTypeCode, 20);
        this.riGrid.AddColumnAlign('ConsignmentNoteNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractNumber', 'WasteConsignmentNote', 'ContractNumber', MntConst.eTypeCode, 15);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNumber', 'WasteConsignmentNote', 'PremiseNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'WasteConsignmentNote', 'PremiseName', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('PremisePostCode', 'WasteConsignmentNote', 'PremisePostCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('PremisePostCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitDueDate', 'WasteConsignmentNote', 'VisitDueDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('VisitDueDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ManualConsignmentNote', 'WasteConsignmentNote', 'ManualConsignmentNote', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('ManualConsignmentNote', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Received', 'WasteConsignmentNote', 'Received', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('Received', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ReceivedDate', 'WasteConsignmentNote', 'ReceivedDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ReceivedDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Print', 'WasteConsignmentNote', 'Print', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('Print', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('DocumentNumber', true);
        this.riGrid.AddColumnOrderable('ConsignmentNoteNumber', true);
        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('PremisePostCode', true);
        this.riGrid.AddColumnOrderable('VisitDueDate', true);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        this.setRequiredStatus('PaperworkReceivedDate', false);
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PaperworkReceivedDate', false);

        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: Record<string, any> = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        formData[this.serviceConstants.BranchNumber] = this.utils.getBranchCode();
        formData['FromDate'] = this.getControlValue('FromDate');
        formData['ToDate'] = this.getControlValue('ToDate');
        formData['WasteTransferTypeCode'] = this.getControlValue('WasteTransferTypeCode');
        formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        formData['WasteConsignmentNoteNumber'] = this.getControlValue('WasteConsignmentNoteNo');
        formData['ManualWasteConsignmentNote'] = this.getControlValue('ManualWasteConsignmentNote');
        formData['ReturnedStatus'] = this.getControlValue('ReturnedStatus');
        formData['WasteConsignmentNoteStatus'] = this.getControlValue('WasteConsignmentNoteStatus');
        formData['SelectedRowId'] = '';
        formData['PaperworkReceivedDate'] = this.getControlValue('PaperworkReceivedDate');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGridFunction.onRefreshClick();
        }
    }

    public getValues(): void {
        this.commonLookup.getSystemParameterEndOfWeekDay().then(data => {
            let SystemParameter = data[0];
            let vEndofWeekDay: number;
            if (SystemParameter && SystemParameter.SystemParameterEndOfWeekDay < 7) {
                vEndofWeekDay = SystemParameter.SystemParameterEndOfWeekDay;
            } else {
                vEndofWeekDay = 1;
            }
            this.setControlValue('FromDate', this.utils.removeDays(new Date(this.utils.getEndofWeekDate(vEndofWeekDay)), 13));
            this.setControlValue('ToDate', this.utils.formatDate(new Date()));
        });
    }

    public onWasteTransferDataRecieved(data: Object): void {
        if (data) {
            this.setControlValue('WasteTransferTypeCode', data['WasteTransferTypeCode']);
            this.setControlValue('WasteTransferTypeDesc', data['WasteTransferTypeDesc']);
        }
    }

    public onEmployeeDataReceived(data: Object): void {
        if (data) {
            this.setControlValue('EmployeeCode', data['EmployeeCode']);
            this.setControlValue('EmployeeSurname', data['EmployeeSurname']);
        }
    }

    public onDataReceivedServiceSearch(data: Object): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data['BranchServiceAreaCode']);
            this.setControlValue('BranchServiceAreaDesc', data['BranchServiceAreaDesc']);
            this.setControlValue('EmployeeCode', data['EmployeeCode']);
            this.onChangeCommon('Employee');
        }
    }

    public onChangeCommon(field: string): void {
        switch (field) {
            case 'Employee':
                let employee = this.getControlValue('EmployeeCode');
                if (!employee) {
                    this.setControlValue('EmployeeSurname', '');
                } else {
                    this.ajaxSource.next(this.ajaxconstant.START);
                    this.commonLookup.getEmployeeSurname(employee).then(data => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data && data[0] && data[0][0]) {
                            this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
                        } else {
                            this.setControlValue('EmployeeSurname', '');
                            this.setControlValue('EmployeeCode', '');
                            this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Employee');
                        }
                    }).catch(error => {
                        this.displayMessage(error);
                    });
                }
                break;

            case 'WasteControl':
                let wasteControlCode = this.getControlValue('WasteTransferTypeCode');
                if (!wasteControlCode) {
                    this.setControlValue('WasteTransferTypeDesc', '');
                } else {
                    let lookupIP = [{
                        'table': 'WasteTransferType',
                        'query': { 'WasteTransferTypeCode': wasteControlCode, 'BusinessCode': this.utils.getBusinessCode() },
                        'fields': ['WasteTransferTypeDesc']
                    }];
                    this.ajaxSource.next(this.ajaxconstant.START);
                    this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data && data[0].length > 0) {
                            this.setControlValue('WasteTransferTypeDesc', data[0][0].WasteTransferTypeDesc);
                        } else {
                            this.setControlValue('WasteTransferTypeCode', '');
                            this.setControlValue('WasteTransferTypeDesc', '');
                            this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Waste Transfer Type Code');
                        }
                    });
                }
                break;

            case 'ServiceArea':
                let serviceArea = this.getControlValue('BranchServiceAreaCode');
                if (!serviceArea) {
                    this.setControlValue('BranchServiceAreaDesc', '');
                } else {
                    this.ajaxSource.next(this.ajaxconstant.START);
                    this.commonLookup.getBranchServiceAreaDescWithEmp(serviceArea, this.utils.getBranchCode()).then(data => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data && data[0] && data[0][0]) {
                            console.log(data[0][0]);
                            this.setControlValue('BranchServiceAreaDesc', data[0][0].BranchServiceAreaDesc);
                            this.setControlValue('EmployeeCode', data[0][0]['EmployeeCode']);
                            this.onChangeCommon('Employee');
                        } else {
                            this.setControlValue('BranchServiceAreaDesc', '');
                            this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Service Area');
                        }
                    }).catch(error => {
                        this.displayMessage(error);
                    });
                }
                break;
        }
    }

    public onGridBodyDoubleClick(): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'Print':
                this.wasteConsignmentNotePrint();
                break;
            case 'Received':
                if (this.getControlValue('PaperworkReceivedDate')) {
                    let gridQuery: QueryParams = this.getURLSearchParamObject();
                    let formData: Record<string, any> = {};

                    gridQuery.set(this.serviceConstants.Action, '2');

                    formData['PostDesc'] = 'UpdateReceiveDate';
                    formData['RowID'] = this.riGrid.Details.GetAttribute('Received', 'rowid');
                    formData['PaperworkReceivedDate'] = this.getControlValue('PaperworkReceivedDate');

                    this.ajaxSource.next(this.ajaxconstant.START);
                    this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (this.hasError(data)) {
                            this.displayMessage(data);
                        } else {
                            this.wasteConsignmentNoteDrillDown();
                        }
                    }).catch((error) => {
                        this.displayMessage(error);
                    });

                } else {
                    this.displayMessage('Paperwork Received Date must be entered');
                    this.setRequiredStatus('PaperworkReceivedDate', true);
                    this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PaperworkReceivedDate', true);
                }
                break;
            case 'ConsignmentNoteNumber':
                this.wasteConsignmentNoteDrillDown();
                break;
        }
    }

    private wasteConsignmentNoteDrillDown(): any {
        let contract = this.riGrid.Details.GetValue('ContractNumber').split('/');
        if (contract[1]) {
            this.setAttribute('ContractTypePrefix', contract[0]);
            this.setAttribute('ContractNumber', contract[1]);
            this.setAttribute('ContractName', this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty'));
            this.setAttribute('PremiseNumber', this.riGrid.Details.GetValue('PremiseNumber'));

        } else {
            this.setAttribute('ContractTypePrefix', '');
            this.setAttribute('ContractNumber', '');
            this.setAttribute('ContractName', '');
            this.setAttribute('PremiseNumber', '');
        }

        this.setAttribute('PremiseName', this.riGrid.Details.GetValue('PremiseName'));
        this.setAttribute('WasteConsignmentNoteNumber', this.riGrid.Details.GetValue('ConsignmentNoteNumber'));
        this.setAttribute('SystemUniqueNumber', this.riGrid.Details.GetAttribute('ConsignmentNoteNumber', 'additionalproperty'));
        this.navigate('', InternalMaintenanceModuleRoutes.ICABSSEUNRETURNEDCONSIGNMENTNOTESDETAILGRID);
    }

    private wasteConsignmentNotePrint(): void {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set('countryCode', this.countryCode());
        search.set('Function', 'Single');
        search.set('WasteConsignmentNote', this.riGrid.Details.GetAttribute('Received', 'rowid'));

        this.httpService.fetchReportURLGET({
            operation: this.xhrParams.operation,
            search: search
        }).then(_data => {
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }
}
