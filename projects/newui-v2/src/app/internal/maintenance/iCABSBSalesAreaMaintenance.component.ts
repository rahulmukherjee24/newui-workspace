import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { SalesAreaSearchComponent } from './../search/iCABSBSalesAreaSearch.component';
import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { EmployeeSearchComponent } from './../search/iCABSBEmployeeSearch';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { InternalGridSearchSalesModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSBSalesAreaMaintenance.html'
})

export class SalesAreaMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private search: QueryParams;

    public pageId: string = '';
    public controls = [
        { name: 'SalesAreaCode', readonly: false, disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'SalesAreaDesc', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'EmployeeCode', readonly: false, disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', readonly: false, disabled: true, required: false, ignoreSubmit: true },
        { name: 'LiveSalesArea', readonly: false, disabled: false, required: false, type: MntConst.eTypeCheckBox },
        { name: 'ROWID', readonly: false, disabled: false, required: false },
        { name: 'Mode', readonly: false, disabled: false, required: false, ignoreSubmit: true },
        { name: 'menu', readonly: false, disabled: false, required: false, ignoreSubmit: true },
        { name: 'save', readonly: false, disabled: false, required: false, ignoreSubmit: true },
        { name: 'cancel', readonly: false, disabled: false, required: false, ignoreSubmit: true },
        { name: 'delete', readonly: false, disabled: false, required: false, ignoreSubmit: true }
    ];
    public xhrParams = {
        module: 'contract-admin',
        method: 'contract-management/admin',
        operation: 'Business/iCABSBSalesAreaMaintenance'
    };
    public employeeSearchParams: any = {
        'parentMode': 'LookUp-Sales'
    };
    public lookUpSubscription: Subscription;
    public branchServiceAreaSearchParams: any = {
        showCloseButton: true,
        showHeader: true,
        showAddNew: false,
        autoOpenSearch: false,
        parentMode: 'LookUp-All'
    };
    public branchServiceAreaComponent = SalesAreaSearchComponent;
    public employeeSearchComponent = EmployeeSearchComponent;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBSALESAREAMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Sales Area Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        if (this.lookUpSubscription) {
            this.lookUpSubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }

    ngAfterViewInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
        } else {
            this.windowOnLoad();
        }
        this.setControlValue('menu', 'Options');
    }

    private windowOnLoad(): void {
        switch (this.parentMode) {
            case 'SalesAreaAdd':
                this.pageParams.CurrentMode = MntConst.eModeAdd;
                this.disableControl('SalesAreaCode', false);
                this.disableControl('menu', true);
                this.disableControl('delete', true);
                break;
            case 'SalesAreaUpdate':
                this.pageParams.CurrentMode = MntConst.eModeUpdate;
                this.disableControl('SalesAreaCode', true);
                this.setControlValue('Mode', 'SalesArea');
                this.fetchValuesForUpdateMode();
                break;
        }
    }

    private fetchValuesForUpdateMode(): void {
        let query: any = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set('ROWID', this.riExchange.getParentAttributeValue('RowID'));
        query.set('Mode', this.getControlValue('Mode'));
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, query)
            .subscribe(
                (data) => {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('SalesAreaCode', data['SalesAreaCode']);
                        this.setControlValue('SalesAreaDesc', data['SalesAreaDesc']);
                        this.setControlValue('EmployeeCode', data['EmployeeCode']);
                        this.setControlValue('LiveSalesArea', data['LiveSalesArea']);
                        this.beforeUpdateMode();
                        this.virtualTableEmployee();
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                });
    }

    private virtualTableEmployee(callback?: any): void {
        let lookupIP = [
            {
                'table': 'Employee',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'EmployeeCode': this.getControlValue('EmployeeCode')
                },
                'fields': ['EmployeeSurname']
            }];
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            let EmployeeSurname = data[0][0];
            if (EmployeeSurname) {
                this.setControlValue('EmployeeSurname', EmployeeSurname.EmployeeSurname);
                if (callback) {
                    callback.call(this);
                }
            } else {
                this.setControlValue('EmployeeSurname', '');
                this.riExchange.riInputElement.markAsError(this.uiForm, 'EmployeeSurname');
                this.riExchange.riInputElement.markAsError(this.uiForm, 'EmployeeCode');
            }
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    private beforeAdd(): void {
        this.pageParams.trInformation = false;
    }

    private beforeUpdateMode(): void {
        this.pageParams.blnEmployeeChanged = false;
        this.pageParams.strOldEmployeeCode = this.getControlValue('EmployeeCode');
    }

    private submitReportRequest(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '6');
        let formData = {
            'SalesAreaRowID': this.riExchange.getParentAttributeValue('RowID'),
            'OldEmployeeCode': this.pageParams.strOldEmployeeCode
        };

        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.BatchProcessSubmitted);
                        modalVO.closeCallback = this.modalClose.bind(this);
                        this.modalAdvService.emitMessage(modalVO);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                });
    }

    private processSave(): void {
        let action: number = 2;
        if (this.pageParams.CurrentMode === MntConst.eModeAdd) {
            action = 1;
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, action.toString());
        let formData = {
            'SalesAreaDesc': this.getControlValue('SalesAreaDesc'),
            'EmployeeCode': this.getControlValue('EmployeeCode'),
            'LiveSalesArea': this.getControlValue('LiveSalesArea', true) ? true : false
        };

        if (this.pageParams.CurrentMode === MntConst.eModeAdd) {
            formData['BranchNumber'] = this.utils.getBranchCode();
            formData['SalesAreaCode'] = this.getControlValue('SalesAreaCode');
        } else {
            formData['ROWID'] = this.riExchange.getParentAttributeValue('RowID');
        }

        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.uiForm.markAsPristine();
                        if (this.pageParams.CurrentMode === MntConst.eModeUpdate) {
                            if (this.pageParams.blnEmployeeChanged) {
                                this.submitReportRequest();
                            } else {
                                this.location.back();
                            }
                        } else {
                            this.location.back();
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                });
    }

    private processDelete(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '3');
        let formData = {
            'ROWID': this.riExchange.getParentAttributeValue('RowID')
        };

        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.location.back();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                });
    }

    private showSaveConfirmation(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.processSave.bind(this)));
    }

    public riExchangeCBORequest(): void {
        if (this.riExchange.riInputElement.HasChanged(this.uiForm, 'EmployeeCode') &&
            (this.pageParams.CurrentMode === MntConst.eModeUpdate)) {
            this.pageParams.blnEmployeeChanged = true;
        }
    }

    public handleDeleteOnClick(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.processDelete.bind(this)));
    }

    public employeeOnChange(obj: any): void {
        this.setControlValue('EmployeeCode', obj.EmployeeCode);
        this.setControlValue('EmployeeSurname', obj.EmployeeSurname);
        this.uiForm.controls['EmployeeCode'].markAsDirty();
        this.riExchangeCBORequest();
    }

    public setBranchServiceArea(event: any): void {
        this.setControlValue('SalesAreaCode', event.BranchServiceAreaCode);
        this.setControlValue('SalesAreaDesc', event.BranchServiceAreaDesc);
        this.uiForm.controls['SalesAreaCode'].markAsDirty();
        this.riExchangeCBORequest();
    }

    public cmdPostCodeOnclick(): void {
        this.navigate('Search', InternalGridSearchSalesModuleRoutes.ICABSBSALESAREAPOSTCODEGRID);
    }

    public menuOnchange(): void {
        this.uiForm.controls['menu'].markAsPristine();
        switch (this.getControlValue('menu')) {
            case 'PostCode':
                this.cmdPostCodeOnclick();
                break;
        }
        this.setControlValue('menu', 'Options');
    }

    public modalClose(): void {
        this.location.back();
    }

    public handleCancelOnClick(): void {
        if (this.pageParams.CurrentMode === MntConst.eModeUpdate) {
            this.windowOnLoad();
            this.uiForm.markAsPristine();
        } else {
            this.location.back();
        }
    }

    public handleSaveOnClick(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            if (this.pageParams.blnEmployeeChanged) {
                this.virtualTableEmployee(this.showSaveConfirmation);
            } else {
                this.showSaveConfirmation();
            }
        }
    }

}
