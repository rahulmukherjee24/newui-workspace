import { BaseComponent } from '../../../app/base/BaseComponent';
import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { GlobalNotificationsService } from '@shared/services/global.notifications.service';
import { HttpRequestOptions, HttpServiceUrl } from '@base/httpRequestOptions';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ProductServiceGroupSearchComponent } from '../../internal/search/iCABSBProductServiceGroupSearch.component';
import { RestService, HTTP_METHOD } from '@shared/services/rest-service';

@Component({
    templateUrl: 'iCABSBProductServiceGroupMaintenance.html',
    styles: [`
        .red-bdr span {border-color: transparent !important;}
        .colorBack16711680 {
            background-color: #FF0000;
            color: #000000;
        }
        .colorBack65280 {
            background-color: #00FF00;
            color: #000000;
        }
        .colorBack255 {
            background-color: #0000FF;
            color: #000000;
        }
        .colorBack16753920 {
            background-color: #FFA500;
            color: #000000;
        }
        .colorBack16776960 {
            background-color: #FFFF00;
            color: #000000;
        }
        .colorBack8388736 {
            background-color: #800080;
            color: #000000;
        }
        .colorBack15631086 {
            background-color: #FFC0CB;
            color: #000000;
        }
        .colorBack128 {
            background-color: #000080;
            color: #000000;
        }
        .colorBack16711935 {
            background-color: #FF00FF;
            color: #000000;
        }
        .colorBack8421504 {
            background-color: #808080;
            color: #000000;
        }
        .colorBack0 {
            background-color: #000000;
            color: #FFFFFF;
        }
        .colorBack16777215 {
            background-color: #FFFFFF;
            color: #000000;
        }
        .colorBack123 {
            background-color: #FFFFFF;
            color: #000000;
        }

        .colorFont16711680 {
            background-color: #FFFFFF;
            color: #FF0000;
        }
        .colorFont65280 {
            background-color: #FFFFFF;
            color: #00FF00;
        }
        .colorFont255 {
            background-color: #FFFFFF;
            color: #0000FF;
        }
        .colorFont16753920 {
            background-color: #FFFFFF;
            color: #FFA500;
        }
        .colorFont16776960 {
            background-color: #FFFFFF;
            color: #FFFF00;
        }
        .colorFont8388736 {
            background-color: #FFFFFF;
            color: #800080;
        }
        .colorFont15631086 {
            background-color: #FFFFFF;
            color: #FFC0CB;
        }
        .colorFont128 {
            background-color: #FFFFFF;
            color: #000080;
        }
        .colorFont16711935 {
            background-color: #FFFFFF;
            color: #FF00FF;
        }
        .colorFont8421504 {
            background-color: #FFFFFF;
            color: #808080;
        }
        .colorFont0 {
            background-color: #FFFFFF;
            color: #000000;
        }
        .colorFont16777215 {
            background-color: #000000;
            color: #FFFFFF;
        }
        .colorFont123 {
            background-color: #FFFFFF;
            color: #000000;
        }
    `]
})

export class ProductServiceGroupMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('productservice') productservice: ProductServiceGroupSearchComponent;
    @ViewChild('productServiceGroupCode') productServiceGroupCode;

    public selectedSelBackColor: any = 'colorBack123';
    public selectedFontColor: any = 'colorFont123';
    public pageId: string = '';
    public selColourOptions: Array<any> = [
        { value: 16711680, text: 'Red' },
        { value: 65280, text: 'Green' },
        { value: 255, text: 'Blue' },
        { value: 16753920, text: 'Orange' },
        { value: 16776960, text: 'Yellow' },
        { value: 8388736, text: 'Purple' },
        { value: 15631086, text: 'Violet' },
        { value: 128, text: 'Navy' },
        { value: 16711935, text: 'Pink' },
        { value: 8421504, text: 'Grey' },
        { value: 0, text: 'Black' },
        { value: 16777215, text: 'White' }
    ];
    public isAddMode: boolean = false;
    public isUpdated: boolean = false;
    public isValidateProductCode: boolean = false;
    public isValidateProductDesc: boolean = false;
    public productGroupServiceSearchParams: any = {
        params: {
            parentMode: 'LookUp'
        }
    };
    public selectedActive: any = {
        id: '',
        text: ''
    };
    public ProductServiceGroupSearchComponent = ProductServiceGroupSearchComponent;
    public requestOptions: HttpRequestOptions;
    public controls = [
        { name: 'ProductServiceGroupCode', value: '', required: true, type: MntConst.eTypeCode },
        { name: 'ProductServiceGroupDesc', value: '', required: true, type: MntConst.eTypeText },
        { name: 'ProductServiceTemplate', value: '', required: false, type: MntConst.eTypeText },
        { name: 'SelFontColour', value: '', required: false },
        { name: 'SelBackgroundColour', value: '', required: false },
        { name: 'BackgroundColour', value: '', required: false },
        { name: 'FontColour', value: '', required: false },
        { name: 'rowId', value: '', required: false },
        { name: 'Weight', value: '', required: false },
        { name: 'Volume', value: '', required: false }
    ];

    constructor(private injector: Injector, private restService: RestService, private notification: GlobalNotificationsService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBPRODUCTSERVICEGROUPMAINTENANCE;
        this.pageTitle = this.browserTitle = 'Product Service Group Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.isAddhide = true;
        this.pageParams.isDeleteDisabled = true;
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * Receive API data from API code search
     */

    private resetForm(): void {
        this.clearControls([]);
    }

    private fetchData(): void {
        this.requestOptions = HttpRequestOptions
            .create(this.injector, HttpServiceUrl.PRODUCT_SERVICE_GROUP)
            .addPathParam(this.utils.getCountryCode())
            .addPathParam(this.utils.getBusinessCode())
            .addPathParam(this.getControlValue('ProductServiceGroupCode'))
            .addMethodType(HTTP_METHOD.METHOD_TYPE_GET);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.restService.executeRequest(this.requestOptions)
            .then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    for (let contrl in data) {
                        if (data.hasOwnProperty(contrl)) {
                            this.setControlValue(contrl, data[contrl]);
                            switch (contrl) {
                                case 'FontColour':
                                    this.setControlValue('SelFontColour', data[contrl]);
                                    this.selectedFontColor = 'colorFont' + data[contrl];
                                    break;
                                case 'BackgroundColour':
                                    this.setControlValue('SelBackgroundColour', data[contrl]);
                                    this.selectedSelBackColor = 'colorBack' + data[contrl];
                                    break;
                                case 'ttProductServiceGroup':
                                    this.setControlValue('rowId', data[contrl]);
                                    break;
                            }
                        }
                    }
                }
            }).catch((error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.notification.displayMessage(error, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
            });
    }

    private beforeAddMode(): void {
        this.resetForm();
    }

    private addProductGroup(formObj: any): void {
        this.requestOptions = HttpRequestOptions
            .create(this.injector, HttpServiceUrl.PRODUCT_SERVICE_GROUP)
            .addPathParam(this.utils.getCountryCode())
            .addPathParam(this.utils.getBusinessCode())
            .addPathParam(this.getControlValue('ProductServiceGroupCode'))
            .addBodyParams(formObj)
            .addMethodType(HTTP_METHOD.METHOD_TYPE_POST);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.restService.executeRequest(this.requestOptions)
            .then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.notification.displayMessage(data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    return;
                }
                this.notification.displayMessage(MessageConstant.Message.SavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                this.selectedFontColor = 'colorFont123';
                this.selectedSelBackColor = 'colorBack123';
                let code_copy = this.getControlValue('ProductServiceGroupCode');
                let desc_copy = this.getControlValue('ProductServiceGroupDesc');
                this.productservice.fetchData();
                this.selectedActive = {
                    id: code_copy,
                    text: code_copy + ' - ' + desc_copy
                };
                this.productservice.active = this.selectedActive;
                this.formPristine();
            }).catch((error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.notification.displayMessage(error, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
            });
    }

    private updateProductGroup(formObj: any): void {
        this.requestOptions = HttpRequestOptions
            .create(this.injector, HttpServiceUrl.PRODUCT_SERVICE_GROUP)
            .addPathParam(this.utils.getCountryCode())
            .addPathParam(this.utils.getBusinessCode())
            .addPathParam(this.getControlValue('ProductServiceGroupCode'))
            .addBodyParams(formObj)
            .addMethodType(HTTP_METHOD.METHOD_TYPE_PUT);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.restService.executeRequest(this.requestOptions)
            .then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.notification.displayMessage(data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    return;
                }
                this.notification.displayMessage(MessageConstant.Message.SavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                const code_copy = this.getControlValue('ProductServiceGroupCode');
                const desc_copy = this.getControlValue('ProductServiceGroupDesc');
                this.selectedActive = {
                    id: code_copy,
                    text: code_copy + ' - ' + desc_copy
                };
                this.productservice.active = this.selectedActive;
                this.productservice.fetchData();
                this.isUpdated = true;
                this.pageParams.mode = 'update';
                this.formPristine();
            }).catch((error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.notification.displayMessage(error, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
            });
    }

    private deleteProductGroup(): void {
        this.requestOptions = HttpRequestOptions
            .create(this.injector, HttpServiceUrl.PRODUCT_SERVICE_GROUP)
            .addPathParam(this.utils.getCountryCode())
            .addPathParam(this.utils.getBusinessCode())
            .addPathParam(this.getControlValue('ProductServiceGroupCode'))
            .addMethodType(HTTP_METHOD.METHOD_TYPE_DELETE);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.restService.executeRequest(this.requestOptions)
            .then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.notification.displayMessage(data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    return;
                }
                this.notification.displayMessage(MessageConstant.Message.RecordDeletedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                this.afterDelete();
                this.formPristine();
            }).catch((error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.notification.displayMessage(error, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
            });
    }

    private promptConfirm(): void {
        let formData: any = {};
        if (this.pageParams.mode !== 'delete') {
            formData['ProductServiceGroupDesc'] = this.getControlValue('ProductServiceGroupDesc');
            formData['ProductServiceTemplate'] = this.getControlValue('ProductServiceTemplate');
            formData['FontColour'] = this.getControlValue('FontColour');
            formData['BackgroundColour'] = this.getControlValue('BackgroundColour');
            formData['Volume'] = this.getControlValue('Volume');
            formData['Weight'] = this.getControlValue('Weight');
            if (this.pageParams.mode === 'update' || this.getControlValue('rowId')) {
                formData['ROWID'] = this.getControlValue('rowId');
                this.updateProductGroup(formData);
            } else {
                this.addProductGroup(formData);
            }
        } else {
            this.deleteProductGroup();
        }
    }

    private afterDelete(): void {
        this.pageParams.isDeleteDisabled = true;
        this.productservice.fetchData();
    }

    private onConfirmCallback(): void {
        this.promptConfirm();
    }

    public getBGClass(value: string): string {
        return 'colorBack' + value;
    }

    public getFontClass(value: string): string {
        return 'colorFont' + value;
    }

    public onProductGroupDataReceived(Obj: any): void {
        this.setControlValue('ProductServiceGroupCode', Obj.ProductServiceGroupCode);
        if (Obj.ProductServiceGroupCode) {
            this.fetchData();
        }
    }

    public getDefaultInfo(event: any): void {
        this.uiForm.controls['ProductServiceGroupCode'].markAsUntouched();
        this.uiForm.controls['ProductServiceGroupDesc'].markAsUntouched();
        if (event.totalRecords) {
            this.pageParams.mode = 'update';
            if (this.isAddMode || this.isUpdated) {
                this.selectedActive = {
                    id: event.newRow.ProductServiceGroupCode,
                    text: event.newRow.ProductServiceGroupCode + ' - ' + event.newRow.ProductServiceGroupDesc
                };
                this.setControlValue('ProductServiceGroupCode', event.newRow.ProductServiceGroupCode);
            } else {
                this.selectedActive = {
                    id: event.firstRow.ProductServiceGroupCode,
                    text: event.firstRow.ProductServiceGroupCode + ' - ' + event.firstRow.ProductServiceGroupDesc
                };
                this.setControlValue('ProductServiceGroupCode', event.firstRow.ProductServiceGroupCode);
            }
            this.isAddMode = false;
            this.isUpdated = false;
            this.pageParams.isAddhide = true;
            this.pageParams.isDeleteDisabled = true;
            this.fetchData();
        } else {
            this.pageParams.mode = 'add';
            this.isAddMode = true;
            this.pageParams.isAddhide = false;
        }
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ProductServiceGroupDesc', this.pageParams.mode === 'add' ? true : false);
    }

    public deleteData(): void {
        this.pageParams.mode = 'delete';
        let promptVO: ICabsModalVO = new ICabsModalVO();
        promptVO.confirmCallback = this.onConfirmCallback.bind(this);
        promptVO.msg = MessageConstant.Message.DeleteRecord;
        this.modalAdvService.emitPrompt(promptVO);
    }

    public onCancelClick(): void {
        if (this.pageParams.mode === 'add') {
            this.productservice.fetchData();
        } else {
            this.fetchData();
        }
    }

    public onAddClick(): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ProductServiceGroupDesc', true);
        this.pageParams.isAddhide = false;
        this.isAddMode = true;
        this.pageParams.mode = 'add';
        this.pageParams.isDeleteDisabled = false;
        this.selectedSelBackColor = 'colorBack123';
        this.selectedFontColor = 'colorFont123';
        this.beforeAddMode();
        setTimeout(() => {
            this.productServiceGroupCode.nativeElement.focus();
        }, 0);
    }

    public saveData(): void {
        if (this.pageParams.mode !== 'add') {
            this.pageParams.mode = 'update';
        }
        let promptVO: ICabsModalVO = new ICabsModalVO();
        promptVO.confirmCallback = this.onConfirmCallback.bind(this);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ProductServiceGroupDesc', this.pageParams.mode === 'add' ? true : false);
        if (this.pageParams.mode !== 'delete') {
            if (this.uiForm.valid && !this.isValidateProductCode && !this.isValidateProductDesc) {
                if (this.getControlValue('FontColour').toString() !== this.getControlValue('BackgroundColour').toString()) {
                    promptVO.msg = MessageConstant.Message.ConfirmRecord;
                    this.modalAdvService.emitPrompt(promptVO);
                } else
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.PageSpecificMessage.colorMisMatch));
            } else {
                this.riExchange.riInputElement.isError(this.uiForm, 'ProductServiceGroupCode');
                this.riExchange.riInputElement.isError(this.uiForm, 'ProductServiceGroupDesc');
            }
        }
    }

    public onSelFontColourOptionsChange(obj: any): void {
        this.setControlValue('FontColour', obj);
        this.selectedFontColor = 'colorFont' + obj;
    }

    public onSelBackgroundColourOptionsChange(obj: any): void {
        this.setControlValue('BackgroundColour', obj);
        this.selectedSelBackColor = 'colorBack' + obj;
    }

    public onProductCodeFieldChange(event: any): void {
        let value = event.target.value;
        if (value.includes('"') || value === '?') {
            this.isValidateProductCode = true;
        } else {
            this.isValidateProductCode = false;
        }
    }

    public onProductDescFieldChange(event: any): void {
        let value = event.target.value;
        if (value.includes('"') || value === '?') {
            this.isValidateProductDesc = true;
        } else {
            this.isValidateProductDesc = false;
        }
    }

}
