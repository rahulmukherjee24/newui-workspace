import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Component, OnInit, Injector, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { VariableService } from './../../../shared/services/variable.service';
import { ContractSearchComponent } from './../search/iCABSAContractSearch';
import { EmployeeSearchComponent } from './../search/iCABSBEmployeeSearch';


@Component({
    templateUrl: 'iCABSCMProspectConversionMaintenance.html'
})
export class ProspectConversionMaintenanceComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private curPageMode: string;
    private uiFormData: any;
    private gLanguageCode: string;
    private rowID: any;

    public pageId: string;
    //Form variables
    public controls: Array<Object> = [
        { name: 'ProspectNumber', disabled: true, required: true, type: MntConst.eTypeInteger },
        { name: 'ProspectName', disabled: true, type: MntConst.eTypeText },
        { name: 'ConvertedToNumber', disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'ConvertedToName', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractTypeCode', required: true },
        { name: 'ContractTypeDesc', type: MntConst.eTypeText },
        { name: 'ConvertedDate', required: true, type: MntConst.eTypeDate },
        { name: 'ConvertedSalesEmployeeCode', required: true, type: MntConst.eTypeCode },
        { name: 'ConvertedSalesEmployeeCodeName', disabled: true, type: MntConst.eTypeText },
        { name: 'ConvertedValue', required: true, type: MntConst.eTypeCurrency }
    ];
    //API variables
    public queryParams: Object = {
        method: 'prospect-to-contract/maintenance',
        module: 'prospect',
        operation: 'ContactManagement/iCABSCMProspectConversionMaintenance'
    };
    //Ellipsis variables
    public ellipsConf: any = {
        convertedToNumber: {
            childConfigParams: {
                'parentMode': 'LookUp-ProspectConversion'
            },
            contentComponent: ContractSearchComponent,
            disabled: false
        },
        salesEmployee: {
            childConfigParams: {
                'parentMode': 'LookUp-ConvertedSales'
            },
            contentComponent: EmployeeSearchComponent
        }
    };
    public modalConfig: Object = {
        backdrop: 'static',
        keyboard: true
    };
    //LookUp varuables
    public ttContractTypeLang = [];

    constructor(injector: Injector, private variableService: VariableService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSCMPROSPECTCONVERSIONMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Prospect Conversion Maintenance';
        this.gLanguageCode = this.riExchange.LanguageCode();
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            this.populateUIFromFormData();
        } else {
            this.pageParams.initFormData = {};
        }
    }

    ngAfterViewInit(): void {
        this.initPage();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private initPage(): void {
        this.rowID = '';
        this.curPageMode = (this.parentMode) ? this.parentMode.toLowerCase() : '';
        switch (this.curPageMode) {
            case this.c_s_MODE_ADD:
                let prospectNumber: any = this.riExchange.getParentHTMLValue('ProspectNumber');
                if (prospectNumber) {
                    this.setControlValue('ProspectNumber', prospectNumber);
                    this.getProspectName(prospectNumber);
                }
                break;
            case this.c_s_MODE_UPDATE:
                this.rowID = this.riExchange.getParentAttributeValue('ProspectConversionRowID');
                if (this.rowID) {
                    this.fetchRecord();
                }
                break;
        }
        this.doLookup();
    }

    private getProspectName(prospectNumber: any): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: any = {};
        search.set(this.serviceConstants.Action, '6');

        //body Params
        formData['PostDesc'] = 'SetProspectName';
        formData['ProspectNumber'] = prospectNumber;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.pageParams.initFormData.ProspectName = data.ProspectName;
                    this.setControlValue('ProspectName', data.ProspectName);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
            );
    }

    private fetchRecord(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        search.set('ROWID', this.rowID);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.pageParams.initFormData = data;
                    this.populateDataToForm(data);
                    this.getProspectName(data.ProspectNumber);
                    this.fetchContactJobDetails(data.ConvertedToNumber);
                    this.fetchEmployeeDetails(data.ConvertedSalesEmployeeCode);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
            );
    }
    private populateDataToForm(data: any): void {
        if (data) {
            this.setControlValue('ProspectNumber', data.ProspectNumber);
            this.setControlValue('ConvertedToNumber', data.ConvertedToNumber);
            this.setControlValue('ContractTypeCode', data.ContractTypeCode);
            if (data.ConvertedDate) {
                this.setControlValue('ConvertedDate', this.utils.formatDate(data.ConvertedDate));
            }
            this.setControlValue('ConvertedSalesEmployeeCode', data.ConvertedSalesEmployeeCode);
            this.setControlValue('ConvertedValue', data.ConvertedValue);
            this.setControlValue('ProspectNumber', data.ProspectNumber);
            this.setControlValue('ProspectNumber', data.ProspectNumber);
            this.setControlValue('ProspectNumber', data.ProspectNumber);
            this.disableControl('ConvertedToNumber', true);
            this.ellipsConf.convertedToNumber.disabled = true;
        }
    }

    public convertedToNumberOnChange(e: any): void {
        let val: any = e.target.value;
        this.setControlValue('ContractTypeCode', '');
        this.setControlValue('ConvertedToName', '');
        this.validateContractNumber(val);
    }
    private validateContractNumber(convertedToNumber: any): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: any = {};
        search.set(this.serviceConstants.Action, '6');

        //body Params
        formData['Function'] = 'ValidateContractNumber';
        formData['ConvertedToNumber'] = convertedToNumber;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    if (data.IsValidContract === 'yes') {
                        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ContractTypeCode');
                        this.setControlValue('ContractTypeCode', data.ContractTypeCode);
                        this.fetchContactJobDetails(convertedToNumber);
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
            );
    }
    public convertedSalesEmployeeCodeOnChange(e: any): void {
        let val: any = e.target.value;
        this.fetchEmployeeDetails(val);
    }

    //Start: Loockup functionality
    private prepareLookupReqData(type?: string, data?: any): any {
        let reqData: any;
        switch (type) {
            case 'ConvertedToName':
                reqData = [{
                    'table': 'Contract',
                    'query': { 'BusinessCode': this.businessCode(), 'ContractNumber': data },
                    'fields': ['ContractName']
                }];
                break;
            case 'ConvertedSalesEmployeeCodeName':
                reqData = [{
                    'table': 'Employee',
                    'query': { 'BusinessCode': this.businessCode(), 'EmployeeCode': data },
                    'fields': ['EmployeeSurname']
                }];
                break;
            default:
                reqData = [{
                    'table': 'ContractTypeLang',
                    'query': { 'LanguageCode': this.gLanguageCode, 'BusinessCode': this.businessCode() },
                    'fields': ['ContractTypeCode', 'ContractTypeDesc']
                }];
                break;
        }
        return reqData;
    }
    public doLookup(): void {
        let reqData = this.prepareLookupReqData();
        this.LookUp.lookUpPromise(reqData).then(
            data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    if (data && data[0].length > 0) {
                        let ContractTypeLang: any[] = data[0];
                        ContractTypeLang.forEach(o => {
                            this.ttContractTypeLang.push({
                                'contractTypeCode': o.ContractTypeCode,
                                'contractTypeDesc': o.ContractTypeDesc,
                                'ttContractTypeLang': o.ttContractTypeLang
                            });
                        });
                    }
                }
            }
        ).catch(
            error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
            );
    }
    public fetchContactJobDetails(contractNumber: any): void {
        let reqData = this.prepareLookupReqData('ConvertedToName', contractNumber);
        this.LookUp.lookUpPromise(reqData).then(
            data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    let value: any = '';
                    if (data && data[0].length > 0) {
                        value = data[0][0].ContractName;
                    }
                    this.setControlValue('ConvertedToName', value);
                }
            }
        ).catch(
            error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
            );
    }
    public fetchEmployeeDetails(employeeCode: any): void {
        let reqData = this.prepareLookupReqData('ConvertedSalesEmployeeCodeName', employeeCode);
        this.LookUp.lookUpPromise(reqData).then(
            data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    let value: any = '';
                    if (data && data[0].length > 0) {
                        value = data[0][0].EmployeeSurname;
                    }
                    this.setControlValue('ConvertedSalesEmployeeCodeName', value);
                }
            }
        ).catch(
            error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
            );
    }
    private saveConfirmLookup(): void {
        let reqData = [{
            'table': 'Prospect',
            'query': { 'ProspectNumber': this.uiFormData.ProspectNumber, 'BusinessCode': this.businessCode() },
            'fields': ['Name']
        }, {
            'table': 'ContractTypeLang',
            'query': { 'LanguageCode': this.gLanguageCode, 'BusinessCode': this.businessCode(), 'ContractTypeCode': this.uiFormData.ContractTypeCode },
            'fields': ['ContractTypeDesc']
        }];
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(reqData).then(
            data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.doSave.bind(this)));
                }
            }
        ).catch(
            error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
            );
    }
    //End: Loockup functionality

    //Start: Ellipsis functionality
    public onConvertedToNumberEllipsisDataReceived(data: any): void {
        if (data) {
            this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ConvertedToNumber');
            this.setControlValue('ConvertedToNumber', data.ContractNumber);
            this.setControlValue('ConvertedToName', data.ContractName);
            this.validateContractNumber(data.ContractNumber);
        }
    }

    public onSalesEmployeeEllipsisDataReceived(data: any): void {
        if (data) {
            this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ConvertedSalesEmployeeCode');
            this.setControlValue('ConvertedSalesEmployeeCode', data.ConvertedSalesEmployeeCode);
            this.setControlValue('ConvertedSalesEmployeeCodeName', data.ConvertedSalesEmployeeSurname);
        }
    }
    //End: Ellipsis functionality

    public onSelecteDatePicker(e: any): void {
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ConvertedDate');
    }

    //Start: Save functionality
    public onClickSaveBtn(): void {
        this.uiFormData = this.uiForm.getRawValue();
        if (this.validateForm()) {
            this.saveConfirmLookup();
        }
    }
    private validateForm(): boolean {
        let isValid: boolean = this.riExchange.validateForm(this.uiForm);
        if (isValid) {
            if (!this.uiFormData.ProspectNumber) {
                isValid = false;
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.ERRORS_ON_PAGE_PROSPECT_NUMBER));
            }
        }
        return isValid;
    }
    private doSave(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: any = {};

        //body Params
        formData['ProspectNumber'] = this.getControlValue('ProspectNumber');
        formData['ContractTypeCode'] = this.getControlValue('ContractTypeCode');
        formData['ConvertedDate'] = this.getControlValue('ConvertedDate');
        formData['ConvertedSalesEmployeeCode'] = this.getControlValue('ConvertedSalesEmployeeCode');
        formData['ConvertedValue'] = this.getControlValue('ConvertedValue');

        if (this.parentMode.toLowerCase() === this.c_s_MODE_ADD) {
            search.set(this.serviceConstants.Action, '1');
            formData['ConvertedToNumber'] = this.getControlValue('ConvertedToNumber');
        } else {
            search.set(this.serviceConstants.Action, '2');
            formData['ROWID'] = this.rowID;
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.uiForm.markAsPristine();
                    setTimeout(() => { this.onClickCancelBtn(); }, 1000);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
            );
    }
    //End: Save functionality

    //Start: Cancel functionality
    public onClickCancelBtn(): void {
        this.variableService.setBackClick(true);
        this.location.back();
    }
    //End: Cancel functionality
}
