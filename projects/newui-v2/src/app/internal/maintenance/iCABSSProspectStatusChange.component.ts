import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Component, ViewChild, OnInit, OnDestroy, Injector, EventEmitter } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { ContractDurationComponent } from './../search/iCABSBContractDurationSearch';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { LostBusinessLanguageSearchComponent } from './../search/iCABSBLostBusinessLanguageSearch.component';
import { LostBusinessDetailSearchComponent } from './../search/iCABSBLostBusinessDetailSearch.component';

@Component({
    templateUrl: 'iCABSSProspectStatusChange.html'
})

export class ProspectStatusChangeComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('contractDurationDropDown') public contractDurationDropDown: ContractDurationComponent;
    @ViewChild('lostbusinesslanguagesearchDropDown') public lostbusinesslanguagesearchDropDown: LostBusinessLanguageSearchComponent;

    private queryParams: any = {
        method: 'prospect-to-contract/maintenance',
        module: 'prospect',
        operation: 'Sales/iCABSSProspectStatusChange'
    };
    private contDurParams: any = {
        method: 'contract-management/search',
        module: 'duration',
        operation: 'Business/iCABSBContractDurationSearch'
    };
    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'ProspectNumber', type: MntConst.eTypeInteger, disabled: true, required: true },
        { name: 'ProspectName', type: MntConst.eTypeTextFree, disabled: true },
        { name: 'ProspectTypeDesc', type: MntConst.eTypeTextFree, disabled: true },
        { name: 'ProspectStatusCode', type: MntConst.eTypeCode, required: true, disabled: true },
        { name: 'ProspectStatusDesc', type: MntConst.eTypeTextFree, disabled: true },
        { name: 'LostBusinessCode', type: MntConst.eTypeCode },
        { name: 'LostBusinessDesc', type: MntConst.eTypeText },
        { name: 'LostBusinessDetailCode', type: MntConst.eTypeCode },
        { name: 'LostBusinessDetailDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'EstimatedValue', type: MntConst.eTypeCurrency },
        { name: 'EstimatedExpiryDate', type: MntConst.eTypeDate },
        { name: 'FollowUpInd', type: MntConst.eTypeCheckBox, required: true, value: false },
        { name: 'FollowUpDate', type: MntConst.eTypeDate },
        { name: 'Services', type: MntConst.eTypeTextFree, disabled: true },
        { name: 'HistoryNarrative', type: MntConst.eTypeTextFree, disabled: true },
        { name: 'SalesEmailInd', type: MntConst.eTypeCheckBox, disabled: true },
        { name: 'ManagerEmailInd', type: MntConst.eTypeCheckBox, disabled: true },
        { name: 'OtherEmailInd', type: MntConst.eTypeCheckBox, disabled: true },
        { name: 'OtherEmailAddress', type: MntConst.eTypeTextFree },
        { name: 'ReasonInd', type: MntConst.eTypeCheckBox, value: false },
        { name: 'ContractDurationCode', type: MntConst.eTypeInteger },
        { name: 'ContractDurationDesc', type: MntConst.eTypeTextFree },
        { name: 'UpdateableInd', type: MntConst.eTypeCheckBox, value: false }
    ];
    public setFocusOnStatusCode = new EventEmitter<boolean>();
    public showCloseButton: boolean = true;
    public showHeader: boolean = true;
    public isTrLostBusiness: boolean = false;
    public isTrLostBusinessDetail: boolean = false;
    public isTrEstimatedValue: boolean = false;
    public isTrContractDurationCode: boolean = false;
    public isTrEstimatedExpiryDate: boolean = false;
    public isTrFollowUpInd: boolean = false;
    public isTrServices: boolean = false;
    public isTrEmail: boolean = false;
    public isTdOtherEmailAddress: boolean = false;
    public isTdFollowUpDate: boolean = false;
    public isTrStatus: boolean = true;
    public isTrSave: boolean = false;
    public inputParams: any = {
        prospectStatusSearchConfig: {
            autoOpen: false,
            ellipsisTitle: 'Prospect Status Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'ContactManagement'
            },
            httpConfig: {
                operation: 'ContactManagement/iCABSCMProspectStatusSearch',
                module: 'prospect',
                method: 'prospect-to-contract/search'
            },
            tableColumn: [
                { title: 'Code', name: 'ProspectStatusCode', type: MntConst.eTypeCode },
                { title: 'Description', name: 'ProspectStatusDesc', type: MntConst.eTypeText },
                { title: 'Converted', name: 'ProspectConvertedInd', type: MntConst.eTypeCheckBox },
                { title: 'User Selectable', name: 'UserSelectableInd', type: MntConst.eTypeCheckBox }
            ],
            disable: true
        },
        contractDuration: {
            parentMode: 'SOPProspectStatusChange',
            businessCode: this.businessCode(),
            countryCode: this.countryCode()
        },
        lostBusinessDetail: {
            parentMode: 'LookUp-Active',
            lostBusinessCode: '',
            component: LostBusinessDetailSearchComponent,
            isDisabled: false
        }
    };
    public dropdown: any = {
        lostBusiness: {
            isRequired: false,
            isDisabled: false,
            isTriggerValidate: false,
            inputParams: {
                parentMode: 'LookUp-Active',
                languageCode: this.riExchange.LanguageCode()
            }
        }
    };
    public contractDurationDefault: any = {
        id: '',
        text: ''
    };
    public activeLostBusinessSearch: any = {
        id: '',
        text: ''
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSPROSPECTSTATUSCHANGE;
        this.browserTitle = this.pageTitle = 'Prospect Status Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        if ((this.parentMode === 'PipelineGrid')) {
            this.setControlValue('ProspectNumber', this.riExchange.getParentHTMLValue('ProspectNumber'));
            this.setControlValue('ProspectName', this.riExchange.getParentHTMLValue('ProspectName'));
        }
        if ((this.parentMode === 'WorkOrderMaintenance')) {
            this.setControlValue('ProspectNumber', this.riExchange.getParentHTMLValue('ProspectNumber'));
            this.setControlValue('ProspectName', this.riExchange.getParentHTMLValue('ProspectName'));
        } else {
            this.setControlValue('ProspectNumber', this.riExchange.getParentHTMLValue('PassProspectNumber'));
        }
        this.getProspectDetails();
    }

    // function to get Prospect details
    private getProspectDetails(): void {
        let postSearchParams: QueryParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '0');
        postSearchParams.set('ProspectNumber', this.getControlValue('ProspectNumber'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('ProspectName', data.ProspectName);
                    this.setControlValue('ProspectTypeDesc', data.ProspectTypeDesc);
                    this.setControlValue('ProspectStatusCode', data.ProspectStatusCode);
                    this.setControlValue('ProspectStatusDesc', data.ProspectStatusDesc);
                    this.setControlValue('LostBusinessCode', data.LostBusinessCode);
                    this.setControlValue('LostBusinessDesc', data.LostBusinessDesc);
                    this.activeLostBusinessSearch = {
                        id: this.getControlValue('LostBusinessCode'),
                        text: this.getControlValue('LostBusinessCode') ? this.getControlValue('LostBusinessCode') + ' - ' + this.getControlValue('LostBusinessDesc') : ''
                    };
                    this.inputParams.lostBusinessDetail.lostBusinessCode = this.getControlValue('LostBusinessCode');
                    this.setControlValue('LostBusinessDetailCode', data.LostBusinessDetailCode);
                    this.setControlValue('LostBusinessDetailDesc', data.LostBusinessDetailDesc);
                    this.setControlValue('EstimatedValue', this.globalize.formatCurrencyToLocaleFormat(data.EstimatedContractValue));
                    this.setControlValue('ContractDurationCode', data.ContractDurationCode);
                    this.setControlValue('EstimatedExpiryDate', this.globalize.formatDateToLocaleFormat(data.EstimatedExpiryDate));
                    this.pageParams['EstimatedExpiryDate'] = this.getControlValue('EstimatedExpiryDate');
                    this.setControlValue('FollowUp', this.utils.convertResponseValueToCheckboxInput(data.FollowUp));
                    this.setControlValue('FollowUpDate', this.globalize.formatDateToLocaleFormat(data.FollowUpDate));
                    this.setControlValue('Services', data.Services);
                    this.pageParams['ProspectNumber'] = this.getControlValue('ProspectNumber');
                    this.pageParams['ProspectName'] = this.getControlValue('ProspectName');
                    this.pageParams['ProspectTypeDesc'] = this.getControlValue('ProspectTypeDesc');
                    this.pageParams['ProspectStatusCode'] = this.getControlValue('ProspectStatusCode');
                    this.pageParams['ProspectStatusDesc'] = this.getControlValue('ProspectStatusDesc');
                    this.showHideFields();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // function to get Contract Duration description
    private getContractDurationDesc(): void {
        let postSearchParams: QueryParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '0');
        postSearchParams.set('ContractDurationCode', this.getControlValue('ContractDurationCode'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.contDurParams.method, this.contDurParams.module, this.contDurParams.operation, postSearchParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    if (data['records'][0]['ContractDurationDesc']) {
                        this.setControlValue('ContractDurationDesc', data['records'][0]['ContractDurationDesc']);
                        this.contractDurationDefault = {
                            text: this.getControlValue('ContractDurationCode') + ' - ' + data['records'][0]['ContractDurationDesc'],
                            id: this.getControlValue('ContractDurationCode')
                        };
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // lookup to get Prospect description
    private lookUpProspectData(): void {
        let lookupIP = [
            {
                'table': 'ProspectStatusLang',
                'query': {
                    'LanguageCode': this.riExchange.LanguageCode(),
                    'BusinessCode': this.businessCode(),
                    'ProspectStatusCode': this.getControlValue('ProspectStatusCode')
                },
                'fields': ['ProspectStatusDesc']
            }
        ];
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpRecord(lookupIP).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                } else {
                    if (data[0] && data[0][0]) {
                        this.setControlValue('ProspectStatusDesc', data[0][0].ProspectStatusDesc);
                        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.confirmOk.bind(this)));
                    } else {
                        this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                    }
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // function to get formdata
    private lookUpData(): void {
        let lookupIP = [
            {
                'table': 'ProspectStatusLang',
                'query': {
                    'LanguageCode': this.riExchange.LanguageCode(),
                    'BusinessCode': this.businessCode(),
                    'ProspectStatusCode': this.getControlValue('ProspectStatusCode')
                },
                'fields': ['ProspectStatusDesc']
            },
            {
                'table': 'LostBusinessLang',
                'query': {
                    'LanguageCode': this.riExchange.LanguageCode(),
                    'BusinessCode': this.businessCode(),
                    'LostBusinessCode': this.getControlValue('LostBusinessCode')
                },
                'fields': ['LostBusinessDesc']
            },
            {
                'table': 'LostBusinessDetailLang',
                'query': {
                    'LanguageCode': this.riExchange.LanguageCode(),
                    'BusinessCode': this.businessCode(),
                    'LostBusinessCode': this.getControlValue('LostBusinessCode'),
                    'LostBusinessDetailCode': this.getControlValue('LostBusinessDetailCode')
                },
                'fields': ['LostBusinessDetailDesc']
            },
            {
                'table': 'ContractDurationLang',
                'query': {
                    'LanguageCode': this.riExchange.LanguageCode(),
                    'BusinessCode': this.businessCode(),
                    'ContractDurationCode': this.getControlValue('ContractDurationCode')
                },
                'fields': ['ContractDurationDesc']
            }
        ];
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpRecord(lookupIP).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                } else {
                    let confirmcall: boolean = false;
                    let prospectData = data[0][0];
                    let lostBusinessData = data[1][0];
                    let lostBusinessDetailData = data[2][0];
                    let contractDurData = data[3][0];
                    if (prospectData) {
                        this.setControlValue('ProspectStatusDesc', prospectData.ProspectStatusDesc);
                    }
                    if (lostBusinessData) {
                        this.setControlValue('LostBusinessDesc', lostBusinessData.LostBusinessDesc);
                        this.activeLostBusinessSearch = {
                            id: this.getControlValue('LostBusinessCode'),
                            text: this.getControlValue('LostBusinessCode') ? this.getControlValue('LostBusinessCode') + ' - ' + this.getControlValue('LostBusinessDesc') : ''
                        };
                    }
                    if (lostBusinessDetailData) {
                        confirmcall = true;
                        this.setControlValue('LostBusinessDetailDesc', lostBusinessDetailData.LostBusinessDetailDesc);
                    } else {
                        confirmcall = false;
                        this.riExchange.riInputElement.markAsError(this.uiForm, 'LostBusinessDetailCode');
                        this.setControlValue('LostBusinessDetailDesc', '');
                    }
                    if (contractDurData) {
                        this.setControlValue('ContractDurationDesc', contractDurData['ContractDurationDesc']);
                        this.contractDurationDefault = {
                            text: this.getControlValue('ContractDurationCode') + ' - ' + contractDurData['ContractDurationDesc'],
                            id: this.getControlValue('ContractDurationCode')
                        };
                    }
                    if (confirmcall) {
                        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.confirmOk.bind(this)));
                    } else {
                        this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                    }
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // function to get Prospect description
    private prospectStatus(): void {
        if ((this.getControlValue('ProspectStatusCode'))) {
            let postParams: Object = {};
            let postSearchParams: QueryParams = this.getURLSearchParamObject();
            postSearchParams.set(this.serviceConstants.Action, '6');
            postParams[this.serviceConstants.Function] = 'GetStatus';
            postParams['ProspectNumber'] = this.getControlValue('ProspectNumber');
            postParams['ProspectStatusCode'] = this.getControlValue('ProspectStatusCode');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('ProspectStatusDesc', data.ProspectStatusDesc);
                        this.setControlValue('Services', data.Services);
                        this.setControlValue('ReasonInd', this.utils.convertResponseValueToCheckboxInput(data.ReasonInd));
                        this.showHideFields();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    // function to show hide fields
    private showHideFields(): void {
        if (this.getControlValue('ReasonInd')) {
            this.isTrLostBusiness = true;
            this.isTrLostBusinessDetail = true;
            this.isTrEstimatedValue = true;
            this.isTrContractDurationCode = true;
            this.setControlValue('ContractDurationCode', '12');
            this.getContractDurationDesc();
            this.calcExpiryDate();
            this.isTrEstimatedExpiryDate = true;
            this.isTrFollowUpInd = true;
            this.isTrServices = true;
            this.dropdown.lostBusiness.isRequired = true;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'LostBusinessCode', true);
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'LostBusinessDetailCode', true);
        } else {
            this.isTrLostBusiness = false;
            this.isTrLostBusinessDetail = false;
            this.isTrEstimatedValue = false;
            this.isTrContractDurationCode = false;
            this.isTrEstimatedExpiryDate = false;
            this.isTrFollowUpInd = false;
            this.isTrServices = false;
            this.isTdFollowUpDate = false;
            this.dropdown.lostBusiness.isRequired = false;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'LostBusinessCode', false);
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'LostBusinessDetailCode', false);
        }
    }

    // function to get Expiry date
    private calcExpiryDate(): void {
        if (this.getControlValue('ContractDurationCode')) {
            let postParams: Object = {};
            let postSearchParams: QueryParams = this.getURLSearchParamObject();
            postSearchParams.set(this.serviceConstants.Action, '6');
            postParams[this.serviceConstants.Function] = 'CalculateExpiryDate';
            postParams['ContractDurationCode'] = this.getControlValue('ContractDurationCode');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.setControlValue('EstimatedExpiryDate', this.globalize.formatDateToLocaleFormat(data.EstimatedExpiryDate));
                            this.setControlValue('FollowUpDate', this.globalize.formatDateToLocaleFormat(data.EstimatedExpiryDate));
                            this.pageParams['EstimatedExpiryDate'] = data.EstimatedExpiryDate;
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }

    // Confirm call back
    private confirmOk(): void {
        let postParams: Object = {};
        let postSearchParams: QueryParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '1');
        postParams['ProspectNumber'] = this.getControlValue('ProspectNumber');
        postParams['ProspectName'] = this.getControlValue('ProspectName');
        postParams['ProspectTypeDesc'] = this.getControlValue('ProspectTypeDesc');
        postParams['ProspectStatusCode'] = this.getControlValue('ProspectStatusCode');
        postParams['ProspectStatusDesc'] = this.getControlValue('ProspectStatusDesc');
        postParams['LostBusinessCode'] = this.getControlValue('LostBusinessCode');
        postParams['LostBusinessDetailCode'] = this.getControlValue('LostBusinessDetailCode');
        postParams['EstimatedValue'] = this.globalize.parseCurrencyToFixedFormat(this.getControlValue('EstimatedValue'));
        postParams['ContractDurationCode'] = this.getControlValue('ContractDurationCode');
        postParams['ContractDurationDesc'] = this.getControlValue('ContractDurationDesc');
        postParams['EstimatedExpiryDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EstimatedExpiryDate'));
        postParams['FollowUpInd'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('FollowUpInd'));
        postParams['FollowUpDate'] = this.isTdFollowUpDate ? this.globalize.parseDateToFixedFormat(this.getControlValue('FollowUpDate')) : '';
        postParams['Services'] = this.getControlValue('Services');
        postParams['HistoryNarrative'] = this.getControlValue('HistoryNarrative');
        postParams['ReasonInd'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('ReasonInd'));
        postParams['SalesEmailInd'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('SalesEmailInd'));
        postParams['ManagerEmailInd'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('ManagerEmailInd'));
        postParams['OtherEmailInd'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('OtherEmailInd'));
        postParams['OtherEmailAddress'] = this.getControlValue('OtherEmailAddress');
        postParams['UpdateableInd'] = this.getControlValue('UpdateableInd');
        postParams['dlContractRowID'] = this.getControlValue('dlContractRowID');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.formPristine();
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                        this.initMode();
                        this.getProspectDetails();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    // Propect status code ellipsis event
    public onProspectStatusDataReceived(data: any): void {
        this.uiForm.controls['ProspectStatusCode'].markAsDirty();
        this.setControlValue('ProspectStatusCode', data.ProspectStatusCode);
        this.setControlValue('ProspectStatusDesc', data.ProspectStatusDesc);
        this.prospectStatus();
    }

    // Propect status code onchange event
    public onProspectStatusCodeChange(data: any): void {
        this.prospectStatus();
    }

    // follwup checkbox event
    public onFollowUpChange(data: any): void {
        if (this.getControlValue('FollowUpInd')) {
            this.isTdFollowUpDate = true;
        } else {
            this.isTdFollowUpDate = false;
        }
    }

    // other email checkbox event
    public onOtherEmailIndChange(data: any): void {
        if (this.getControlValue('OtherEmailInd')) {
            this.isTdOtherEmailAddress = true;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'OtherEmailAddress', true);
        } else {
            this.isTdOtherEmailAddress = false;
            this.setControlValue('OtherEmailAddress', '');
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'OtherEmailAddress', false);
        }
    }

    // contract duration dropdown select event
    public onReceivedContractDuration(data: any): void {
        this.setControlValue('ContractDurationCode', data['ContractDurationCode']);
        this.setControlValue('ContractDurationDesc', data['ContractDurationDesc']);
        this.calcExpiryDate();
    }

    // Expiry date change event
    public onSelectedDateValue(value: any): void {
        if (value && value.value) {
            this.setControlValue('EstimatedExpiryDate', value.value);
        }
    }

    //Lostbusiness dropdown event
    public onLostBusinessLanguageSearch(data: any): void {
        if (data['LostBusinessLang.LostBusinessCode'] !== this.getControlValue('LostBusinessCode')) {
            this.uiForm.controls['LostBusinessCode'].markAsDirty();
            this.setControlValue('LostBusinessCode', data['LostBusinessLang.LostBusinessCode']);
            this.setControlValue('LostBusinessDesc', data['LostBusinessLang.LostBusinessDesc']);
            this.setControlValue('LostBusinessDetailCode', '');
            this.setControlValue('LostBusinessDetailDesc', '');
            this.inputParams.lostBusinessDetail.lostBusinessCode = this.getControlValue('LostBusinessCode');
        } else {
            this.uiForm.controls['LostBusinessCode'].markAsPristine();
        }
    }

    //LostbusinessDetail ellipsis event
    public onLostBusinessDetailLangDataReceived(data: any): void {
        this.setControlValue('LostBusinessDetailCode', data['LostBusinessDetailCode']);
        this.setControlValue('LostBusinessDetailDesc', data['LostBusinessDetailDesc']);
    }

    //Status button click event
    public onStatusClick(): void {
        this.isTrStatus = false;
        this.isTrSave = true;
        this.isTrEmail = true;
        this.setControlValue('ProspectStatusCode', '');
        this.setControlValue('ProspectStatusDesc', '');
        this.riExchange.riInputElement.Enable(this.uiForm, 'ProspectStatusCode');
        this.setFocusOnStatusCode.emit(true);
        this.inputParams['prospectStatusSearchConfig'].disable = false;
        this.riExchange.riInputElement.Enable(this.uiForm, 'SalesEmailInd');
        this.riExchange.riInputElement.Enable(this.uiForm, 'ManagerEmailInd');
        this.riExchange.riInputElement.Enable(this.uiForm, 'OtherEmailInd');
        this.riExchange.riInputElement.Enable(this.uiForm, 'HistoryNarrative');
    }

    //Save button click event
    public saveData(): void {
        if (this.isTrLostBusiness && this.isTrLostBusinessDetail) {
            this.dropdown.lostBusiness.isTriggerValidate = true;
        }
        if (this.riExchange.validateForm(this.uiForm)) {
            if (this.isTrLostBusiness && this.isTrLostBusinessDetail) {
                if (this.getControlValue('LostBusinessCode')) {
                    this.lookUpData();
                }
            } else {
                this.lookUpProspectData();
            }
        }
    }

    //Cancel button click event
    public cancelData(data: any): void {
        this.uiForm.reset();
        this.initMode();
        this.isTrEmail = false;
        this.setControlValue('ProspectNumber', this.pageParams['ProspectNumber']);
        this.setControlValue('ProspectName', this.pageParams['ProspectName']);
        this.setControlValue('ProspectTypeDesc', this.pageParams['ProspectTypeDesc']);
        this.setControlValue('ProspectStatusCode', this.pageParams['ProspectStatusCode']);
        this.showHideFields();
    }

    //Initial page mode to disable fields
    public initMode(): void {
        this.isTrStatus = true;
        this.isTrSave = false;
        this.uiForm.controls['ProspectStatusCode'].markAsUntouched();
        this.riExchange.riInputElement.Disable(this.uiForm, 'ProspectStatusCode');
        this.inputParams['prospectStatusSearchConfig'].disable = true;
        this.riExchange.riInputElement.Disable(this.uiForm, 'SalesEmailInd');
        this.riExchange.riInputElement.Disable(this.uiForm, 'ManagerEmailInd');
        this.riExchange.riInputElement.Disable(this.uiForm, 'OtherEmailInd');
        this.riExchange.riInputElement.Disable(this.uiForm, 'HistoryNarrative');
        this.setControlValue('HistoryNarrative', '');
        this.setControlValue('FollowUpInd', false);
        this.setControlValue('SalesEmailInd', false);
        this.setControlValue('ManagerEmailInd', false);
        this.setControlValue('OtherEmailInd', false);
        this.isTdOtherEmailAddress = false;
        this.setControlValue('OtherEmailAddress', '');
        this.dropdown.lostBusiness.isRequired = false;
        this.dropdown.lostBusiness.isTriggerValidate = false;
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'OtherEmailAddress', false);
        this.contractDurationDefault = {
            id: '',
            text: ''
        };
        this.activeLostBusinessSearch = {
            id: '',
            text: ''
        };
        this.setControlValue('ReasonInd', false);
    }
}
