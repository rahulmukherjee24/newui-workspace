import { OnInit, OnDestroy, ViewChild, Component, Injector, AfterViewInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { InternalSearchModuleRoutes, InternalMaintenanceModuleRoutes, ServiceDeliveryModuleRoutes, AppModuleRoutes } from './../../base/PageRoutes';
import { BranchServiceAreaSearchComponent } from '../../../app/internal/search/iCABSBBranchServiceAreaSearch';
import { EmployeeSearchComponent } from '../../../app/internal/search/iCABSBEmployeeSearch';

@Component({
    templateUrl: 'iCABSBBranchServiceAreaMaintenance.html'
})

export class BranchServiceAreaMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    //Subscription Variables
    private sysCharSubscription: Subscription;
    private subUpdateBranchService: Subscription;
    private subAddBranchServiceArea: Subscription;
    private subDeleteBranchServiceArea: Subscription;
    private subOnServiceAreaCode: Subscription;
    private subFetchEmployeeSurname: Subscription;
    //SysChar Variables
    private isVbEnablePlanSequenceNumber: boolean;
    private isVbEnableWED: boolean;
    private isVbEnableBSAEmployee: boolean;
    //API Variables
    private queryParams: any = {
        method: 'service-delivery/admin',
        module: 'structure',
        operation: 'Business/iCABSBBranchServiceAreaMaintenance'
    };
    //Page Level Variables
    private isReturningStatus: boolean;
    private isSetPageParams: boolean;
    //Public Variables
    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'BranchServiceAreaCode', disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'EmployeeCode', disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TechRetentionInd', disabled: false, required: false, type: MntConst.eTypeCheckBox },
        { name: 'ServiceAreaNoOfCalls', disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'ServiceAreaNoOfExchanges', disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'ServiceAreaWED', disabled: false, required: false, type: MntConst.eTypeDecimal1 },
        { name: 'DepotNumber', disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'LiveServiceArea', disabled: false, required: false, type: MntConst.eTypeCheckBox },
        { name: 'MultiBranchArea', disabled: false, required: false, type: MntConst.eTypeCheckBox },
        { name: 'WeeklySequenceNumberRepeat', disabled: false, required: false, type: MntConst.eTypeInteger },
        //Hidden Fields
        { name: 'oriEmployeeCode' },
        { name: 'MessageDesc' },
        { name: 'BranchNumber' },
        { name: 'BranchName' }
    ];
    //Ellipsis
    public ellipsisConfig: any = {
        branchServiceArea: {
            isDisabled: false,
            isShowCloseButton: true,
            isShowHeader: true,
            parentMode: 'LookUp-All',
            isShowAddNew: false,
            component: BranchServiceAreaSearchComponent
        },
        employeeCode: {
            disabled: false,
            showCloseButton: true,
            showHeader: true,
            parentMode: 'LookUp-Service-All',
            showAddNew: false,
            component: EmployeeSearchComponent
        },
        depotNumber: {
            ellipsisTitle: 'Depot Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp'
            },
            httpConfig: {
                operation: 'Business/iCABSBDepotSearch',
                module: 'depot',
                method: 'service-delivery/search'
            },
            tableColumn: [
                { title: 'Depot Number', name: 'DepotNumber', type: MntConst.eTypeInteger },
                { title: 'Depot Name', name: 'DepotName', type: MntConst.eTypeText }
            ],
            disable: false
        }
    };
    //Field Disable Variables
    public isFieldDisable: any = {
        saveButton: true,
        cancelButton: true,
        deleteButton: true,
        addButton: true,
        menuOption: false
    };
    //Field Hidden Variable
    public isFieldNotHidden: any = {
        deleteButton: false,
        addButton: false,
        serviceAreaWED: false,
        weeklySequenceNumberRepeat: false,
        trTechRetentionInd: false
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBBRANCHSERVICEAREAMAINTENANCE;
        this.pageTitle = this.browserTitle = 'Service Area Maintenance';
    }

    /**
     * On Init Life Cycle Hook Inittate Funcationality
     */
    ngOnInit(): void {
        super.ngOnInit();
        this.triggerFetchSysChar();
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.isReturningStatus = true;
            this.setFormMode(this.pageParams.formMode);
            if (this.pageParams.isVbEnableWED) {
                this.isFieldNotHidden.serviceAreaWED = true;
                this.disableControl('ServiceAreaWED', true);
            }
            if (this.pageParams.isVbEnablePlanSequenceNumber) {
                this.isFieldNotHidden.weeklySequenceNumberRepeat = true;
            }
        }
    }
    /**
     * After View Initialize Life Cycle Hook To Do After Page Load Operations
     */
    ngAfterViewInit(): void {
        if (this.isReturningStatus) {
            switch (this.pageParams.formMode) {
                case this.c_s_MODE_SELECT:
                    this.parentMode = this.pageParams.parentMode;
                    this.selectModeView();
                    break;
                case this.c_s_MODE_UPDATE:
                    this.parentMode = this.pageParams.parentMode;
                    this.updateModeView();
                    this.resetPageParameters();
                    break;
                case this.c_s_MODE_ADD:
                    this.parentMode = this.pageParams.parentMode;
                    this.onAddModeView();
                    break;
            }
        } else {
            this.windowOnLoad();
        }
    }
    /**
     * On Destroy Life Cycle Hook To Unsubscribe Subscription
     */
    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.sysCharSubscription) {
            this.sysCharSubscription.unsubscribe();
        }
        if (this.subUpdateBranchService) {
            this.subUpdateBranchService.unsubscribe();
        }
        if (this.subAddBranchServiceArea) {
            this.subAddBranchServiceArea.unsubscribe();
        }
        if (this.subDeleteBranchServiceArea) {
            this.subDeleteBranchServiceArea.unsubscribe();
        }
        if (this.subOnServiceAreaCode) {
            this.subOnServiceAreaCode.unsubscribe();
        }
        if (this.subFetchEmployeeSurname) {
            this.subFetchEmployeeSurname.unsubscribe();
        }
    }
    /**
     * Method to do Operations After Window Load
     * @params: params: void
     * @return: : void
     */
    private windowOnLoad(): void {
        this.isSetPageParams = true;
        switch (this.parentMode) {
            case 'BranchServiceAreaAdd':
                this.setFormMode(this.c_s_MODE_ADD);
                this.onAddModeView();
                break;
            case 'BranchServiceAreaUpdate':
                this.setFormMode(this.c_s_MODE_UPDATE);
                this.pageParams.branchServiceRowID = this.riExchange.getParentHTMLValue('rowID');
                this.fetchBranchServieAreaDetails();
                this.updateModeView();
                break;
            default:
                break;
        }
        this.disableControl('ServiceAreaNoOfCalls', true);
        this.disableControl('ServiceAreaNoOfExchanges', true);
        if (this.isVbEnableWED) {
            this.isFieldNotHidden.serviceAreaWED = true;
            this.disableControl('ServiceAreaWED', true);
        }
        if (this.isVbEnablePlanSequenceNumber) {
            this.isFieldNotHidden.weeklySequenceNumberRepeat = true;
        }
        this.riExchange.getParentHTMLValue('BranchNumber');
        this.riExchange.getParentHTMLValue('BranchName');
    }
    /**
     * Method to Set Form Data to Page Parameters
     * @params: params: void
     * @return: : void
     */
    private setPageParameters(): void {
        this.pageParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        this.pageParams.BranchServiceAreaDesc = this.getControlValue('BranchServiceAreaDesc');
        this.pageParams.EmployeeCode = this.getControlValue('EmployeeCode');
        this.pageParams.EmployeeSurname = this.getControlValue('EmployeeSurname');
        this.pageParams.TechRetentionInd = this.getControlValue('TechRetentionInd') ? this.getControlValue('TechRetentionInd') : '';
        this.pageParams.ServiceAreaNoOfExchanges = this.getControlValue('ServiceAreaNoOfExchanges');
        this.pageParams.ServiceAreaNoOfCalls = this.getControlValue('ServiceAreaNoOfCalls');
        this.pageParams.ServiceAreaWED = this.getControlValue('ServiceAreaWED') >= 0 ? this.getControlValue('ServiceAreaWED') : '';
        this.pageParams.LiveServiceArea = this.getControlValue('LiveServiceArea');
        this.pageParams.MultiBranchArea = this.getControlValue('MultiBranchArea');
        this.pageParams.DepotNumber = this.getControlValue('DepotNumber');
        this.pageParams.WeeklySequenceNumberRepeat = this.getControlValue('WeeklySequenceNumberRepeat') >= 0 ? this.getControlValue('WeeklySequenceNumberRepeat') : '';
        this.pageParams.oriEmployeeCode = this.getControlValue('oriEmployeeCode');
    }
    /**
     * Method to Set Previous Form Values From Page Parameters
     * @params: params: void
     * @return: : void
     */
    private resetPageParameters(): void {
        this.setControlValue('BranchServiceAreaCode', this.pageParams.BranchServiceAreaCode);
        this.setControlValue('BranchServiceAreaDesc', this.pageParams.BranchServiceAreaDesc);
        this.setControlValue('EmployeeCode', this.pageParams.EmployeeCode);
        this.setControlValue('EmployeeSurname', this.pageParams.EmployeeSurname);
        this.setControlValue('TechRetentionInd', this.pageParams.TechRetentionInd);
        this.setControlValue('ServiceAreaNoOfExchanges', this.pageParams.ServiceAreaNoOfExchanges);
        this.setControlValue('ServiceAreaNoOfCalls', this.pageParams.ServiceAreaNoOfCalls);
        this.setControlValue('ServiceAreaWED', this.pageParams.ServiceAreaWED);
        this.setControlValue('LiveServiceArea', this.pageParams.LiveServiceArea);
        this.setControlValue('MultiBranchArea', this.pageParams.MultiBranchArea);
        this.setControlValue('DepotNumber', this.pageParams.DepotNumber);
        this.setControlValue('WeeklySequenceNumberRepeat', this.pageParams.WeeklySequenceNumberRepeat);
        this.setControlValue('oriEmployeeCode', this.pageParams.oriEmployeeCode);
    }
    /**
     * Method to Set Select Mode View
     * @params: params: void
     * @return: : void
     */
    private selectModeView(): void {
        this.setFormMode(this.c_s_MODE_SELECT);
        this.pageParams.formMode = this.c_s_MODE_SELECT;
        this.uiForm.reset();
        this.disableControls([]);
        this.disableControl('BranchServiceAreaCode', false);
        this.disableControl('BranchServiceAreaDesc', true);
        this.ellipsisConfig.branchServiceArea.isDisabled = false;
        this.ellipsisConfig.employeeCode.disabled = true;
        this.ellipsisConfig.depotNumber.disable = true;
        this.isFieldDisable.saveButton = true;
        this.isFieldDisable.cancelButton = true;
        this.isFieldDisable.addButton = false;
        this.isFieldNotHidden.addButton = true;
        this.isFieldDisable.deleteButton = true;
        this.isFieldNotHidden.deleteButton = true;
        this.pageParams.saveClicked = false;
        this.isFieldDisable.menuOptions = true;
    }
    /**
     * Method to Set Update Mode View
     * @params: params: void
     * @return: : void
     */
    private updateModeView(): void {
        this.setFormMode(this.c_s_MODE_UPDATE);
        this.pageParams.formMode = this.c_s_MODE_UPDATE;
        this.disableControl('BranchServiceAreaCode', true);
        this.disableControl('BranchServiceAreaDesc', false);
        this.ellipsisConfig.branchServiceArea.isDisabled = false;
        this.disableControl('EmployeeCode', false);
        this.disableControl('EmployeeSurname', true);
        this.ellipsisConfig.employeeCode.disabled = false;
        if (this.isFieldNotHidden.trTechRetentionInd) {
            this.disableControl('TechRetentionInd', false);
        }
        this.disableControl('ServiceAreaNoOfCalls', true);
        this.disableControl('ServiceAreaNoOfExchanges', true);
        if (this.isFieldNotHidden.serviceAreaWED) {
            this.disableControl('ServiceAreaWED', true);
        }
        this.disableControl('DepotNumber', false);
        this.ellipsisConfig.depotNumber.disable = false;
        this.disableControl('LiveServiceArea', false);
        this.disableControl('MultiBranchArea', false);
        if (this.isFieldNotHidden.weeklySequenceNumberRepeat) {
            this.disableControl('WeeklySequenceNumberRepeat', false);
        }
        this.isFieldDisable.saveButton = false;
        this.isFieldDisable.cancelButton = false;
        if (this.parentMode === 'BranchServiceAreaAdd') {
            this.isFieldDisable.addButton = false;
            this.isFieldNotHidden.addButton = true;
        } else {
            this.isFieldDisable.addButton = true;
            this.isFieldNotHidden.addButton = false;
        }
        this.isFieldDisable.deleteButton = false;
        this.isFieldNotHidden.deleteButton = true;
        this.pageParams.saveClicked = false;
        this.isFieldDisable.menuOptions = false;
    }
    /**
     * Method to Set Add Mode
     * @params: params: void
     * @return: : void
     */
    private onAddModeView(): void {
        this.setFormMode(this.c_s_MODE_ADD);
        this.pageParams.formMode = this.c_s_MODE_ADD;
        this.uiForm.reset();
        this.disableControl('BranchServiceAreaCode', false);
        this.disableControl('BranchServiceAreaDesc', false);
        this.ellipsisConfig.branchServiceArea.isDisabled = false;
        this.disableControl('EmployeeCode', false);
        this.disableControl('EmployeeSurname', true);
        this.ellipsisConfig.employeeCode.disabled = false;
        if (this.isFieldNotHidden.trTechRetentionInd) {
            this.disableControl('TechRetentionInd', false);
        }
        this.disableControl('ServiceAreaNoOfCalls', true);
        this.disableControl('ServiceAreaNoOfExchanges', true);
        if (this.isFieldNotHidden.serviceAreaWED) {
            this.disableControl('ServiceAreaWED', true);
        }
        this.disableControl('DepotNumber', false);
        this.ellipsisConfig.depotNumber.disable = false;
        this.disableControl('LiveServiceArea', false);
        this.disableControl('MultiBranchArea', false);
        if (this.isFieldNotHidden.weeklySequenceNumberRepeat) {
            this.disableControl('WeeklySequenceNumberRepeat', false);
        }
        this.isFieldDisable.saveButton = false;
        this.isFieldDisable.cancelButton = false;
        this.isFieldDisable.addButton = true;
        this.isFieldNotHidden.addButton = false;
        this.isFieldDisable.deleteButton = true;
        this.isFieldNotHidden.deleteButton = false;
        this.pageParams.saveClicked = false;
        this.isFieldDisable.menuOptions = true;
    }
    /**
     * Method to Set Delete Mode
     * @params: params: void
     * @return: : void
     */
    private deleteModeView(): void {
        this.setFormMode(this.c_s_MODE_DELETE);
        this.pageParams.formMode = this.c_s_MODE_DELETE;
        this.uiForm.reset();
        if (!this.isFieldNotHidden.addButton) {
            this.setControlValue('ServiceAreaNoOfCalls', 0);
            this.setControlValue('ServiceAreaNoOfExchanges', 0);
            this.setControlValue('ServiceAreaWED', 0.0);
        }
        this.disableControls([]);
        this.ellipsisConfig.branchServiceArea.isDisabled = true;
        this.ellipsisConfig.employeeCode.disabled = true;
        this.ellipsisConfig.depotNumber.disable = true;
        this.isFieldDisable.saveButton = true;
        this.isFieldDisable.cancelButton = true;
        this.isFieldDisable.addButton = true;
        this.isFieldNotHidden.addButton = false;
        this.isFieldDisable.deleteButton = true;
        this.isFieldNotHidden.deleteButton = true;
        this.pageParams.saveClicked = false;
        this.isFieldDisable.menuOptions = true;
    }
    /**
     * Method to Set Cancel Mode
     * @params: params: void
     * @return: : void
     */
    private cancelModeView(): void {
        this.formPristine();
        switch (this.formMode) {
            case this.c_s_MODE_ADD:
                this.selectModeView();
                break;
            case this.c_s_MODE_UPDATE:
                this.resetPageParameters();
                this.fetchEmployeeSurname();
                break;
            case this.c_s_MODE_SELECT:
                this.selectModeView();
                break;
            case this.c_s_MODE_DELETE:
                this.deleteModeView();
                break;
        }
    }
    /**
     * Method to Get SysChar Parameters
     * @params: params: void
     * @return: String of SysChar
     */
    private sysCharParameters(): string {
        let sysCharList: any = [
            this.sysCharConstants.SystemCharEnableRouteOptimisationSoftwareIntegration, //SysChar Number: 2260
            this.sysCharConstants.SystemCharEnableWED,  //SysChar Number: 3370
            this.sysCharConstants.SystemCharBranchServiceAreaEmployees  //SysChar Number: 3360
        ];
        return sysCharList.join(',');
    }
    /**
     * Method to Get SysChar Number
     * @params: params: void
     * @return: SysChar Number
     */
    private fetchSysChar(sysCharNumbers: any): any {
        let querySysChar: any = this.getURLSearchParamObject();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.SystemCharNumber, sysCharNumbers);
        return this.httpService.sysCharRequest(querySysChar);
    }
    /**
     * Method to Fetch SysChar Details
     * @params: params: void
     * @return: any
     */
    private triggerFetchSysChar(): any {
        let sysCharNumbers: any = this.sysCharParameters();
        this.sysCharSubscription = this.fetchSysChar(sysCharNumbers).subscribe((data) => {
            this.onSysCharDataReceive(data);
        });
    }
    /**
     * Method to Assign SysChar Details to SpeedScript Variables
     * And to Do Other Functionality Based On Syschar
     * @params: params: data: any
     * @return: void
     */
    private onSysCharDataReceive(data: any): void {
        if (data.records && data.records.length > 0) {
            this.isVbEnablePlanSequenceNumber = this.pageParams.isVbEnablePlanSequenceNumber = data.records[0].Logical; //SysChar Number: 2260
            this.isVbEnableWED = this.pageParams.isVbEnableWED = data.records[1].Required;//SysChar Number: 3370
            this.isVbEnableBSAEmployee = data.records[2].Required;//SysChar Number: 3360
        }
        if (!this.isReturningStatus) {
            this.windowOnLoad();
        }
    }
    /**
     * Method to Choose Mode of Form Save
     * @params: void
     * @return: : void
     */
    private executeSaveForm(): void {
        switch (this.formMode) {
            case this.c_s_MODE_ADD:
                let promptVO1: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.addBranchServiceArea.bind(this));
                this.modalAdvService.emitPrompt(promptVO1);
                break;
            case this.c_s_MODE_UPDATE:
                let promptVO2: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.updateBranchServiceArea.bind(this));
                this.modalAdvService.emitPrompt(promptVO2);
                break;
            default:
                break;
        }
    }
    /**
     * Method to Add New Branch Service Aread Details
     * @params: params: void
     * @return: void
     */
    public addBranchServiceArea(): void {
        let formData: Object = {};

        let search: any = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '1');
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['BranchServiceAreaDesc'] = this.getControlValue('BranchServiceAreaDesc');
        formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        formData['LiveServiceArea'] = this.getControlValue('LiveServiceArea');
        formData['MultiBranchArea'] = this.getControlValue('MultiBranchArea');
        formData['DepotNumber'] = this.getControlValue('DepotNumber') ? this.getControlValue('DepotNumber') : '~~IGNORE~~';
        if (this.isFieldNotHidden.weeklySequenceNumberRepeat) {
            formData['WeeklySequenceNumberRepeat'] = this.getControlValue('WeeklySequenceNumberRepeat');
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.subAddBranchServiceArea = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.pageParams.branchServiceRowID = data.ttBranchServiceArea;
                        this.pageParams.formMode = this.c_s_MODE_UPDATE;
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                        this.setPageParameters();
                        this.updateModeView();
                        this.formPristine();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }
    /**
     * Method to Update Branch Service Area Details
     * @params: params: void
     * @return: void
     */
    public updateBranchServiceArea(): void {
        let formData: Object = {};
        let search: any = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        formData['ROWID'] = this.pageParams.branchServiceRowID;
        formData['BranchServiceAreaDesc'] = this.getControlValue('BranchServiceAreaDesc');
        formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        formData['LiveServiceArea'] = this.getControlValue('LiveServiceArea') ? 'true' : 'false';
        formData['MultiBranchArea'] = this.getControlValue('MultiBranchArea');
        formData['DepotNumber'] = this.getControlValue('DepotNumber') ? this.getControlValue('DepotNumber') : '~~IGNORE~~';
        if (this.isFieldNotHidden.weeklySequenceNumberRepeat) {
            formData['WeeklySequenceNumberRepeat'] = this.getControlValue('WeeklySequenceNumberRepeat');
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.subUpdateBranchService = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                        this.setPageParameters();
                        this.updateModeView();
                        this.formPristine();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }
    /**
     * Method to do Functionality On Save Button Click
     * @params: params: void
     * @return: : void
     */
    public saveBranchServiceArea(): void {
        if (this.uiForm.valid) {
            this.pageParams.saveClicked = true;
            this.executeSaveForm();
        } else {
            this.riExchange.validateForm(this.uiForm);
        }
    }
    /**
     * Method to do Functioanlity on Delete Button Click
     * @params: params: void
     * @return: : void
     */
    public deleteBranchServiceArea(): void {
        let promptVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteExistingBranchServiceArea.bind(this));
        this.modalAdvService.emitPrompt(promptVO);
    }
    /**
     * Method to Delete Existing Seasonal Template
     * @params: params: void
     * @return: void
     */
    public deleteExistingBranchServiceArea(): void {
        let formData: Object = {};

        let search: any = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '3');

        formData['ROWID'] = this.pageParams.branchServiceRowID;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.subDeleteBranchServiceArea = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeleted));
                        if (!this.isFieldNotHidden.addButton) {
                            this.setFormMode(this.c_s_MODE_DELETE);
                            this.deleteModeView();
                            this.formPristine();
                        } else {
                            this.selectModeView();
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }
    /**
     * Method to Navigate to Pages On Option Dropdown Selection
     * @params: params: void
     * @return: void
     */
    public optionBranchServiceAreaChange(event: any): void {
        switch (event) {
            case 'Postcode':
                this.navigate('AddPostcode', InternalSearchModuleRoutes.ICABSBBRANCHPCODESERVICEGRPENTRYSEARCH, {
                    parentMode: 'AddPostcode'
                });
                break;
            case 'ProductServiceGroup':
                this.navigate('AddProduct', InternalMaintenanceModuleRoutes.ICABSBBRANCHPRODSERVICEGRPENTRYMAINTENANCE);
                break;
            case 'Employees':
                this.navigate('AddProduct', AppModuleRoutes.SERVICEDELIVERY + ServiceDeliveryModuleRoutes.ICABSBBRANCHSERVICEAREAEMPLOYEEGRID);
                break;
        }
    }
    /**
     * Method to Populate BranchServiceAreaCode Data on Ellipsis Data Received
     * @params: params: void
     * @return: : void
     */
    public onServiceAreaReceived(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
            this.updateModeView();
            this.fetchBranchServieAreaDescription();
            if (this.parentMode !== 'BranchServiceAreaAdd') {
                this.uiForm.controls['BranchServiceAreaCode'].markAsDirty();
            }
        }
    }
    /**
     * Method to Populate EmployeeCode Data on Ellipsis Data Received
     * @params: params: void
     * @return: : void
     */
    public onEmployeeCodeReceived(data: any): void {
        if (data) {
            this.setControlValue('EmployeeCode', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
            this.fetchEmployeeSurname();
            this.uiForm.controls['EmployeeCode'].markAsDirty();
        }
    }
    /**
     * Method to Populate DepotNumber Data on Ellipsis Data Received
     * @params: params: void
     * @return: : void
     */
    public onDepotSearchReceived(data: any): void {
        if (data) {
            this.setControlValue('DepotNumber', data.DepotNumber);
            this.uiForm.controls['DepotNumber'].markAsDirty();
        }
    }
    /**
     * Method to Fetch Branch Service Area Description and Other Details
     * On Tab Out
     * @params: params: void
     * @return: : void
     */
    public fetchBranchServieAreaDescription(): void {
        if (this.getControlValue('BranchServiceAreaCode') && this.formMode !== this.c_s_MODE_ADD) {
            let lookupIP: any = [
                {
                    'table': 'BranchServiceArea',
                    'query': {
                        'BranchServiceAreaCode': this.getControlValue('BranchServiceAreaCode'),
                        'BranchNumber': this.utils.getBranchCode(),
                        'BusinessCode': this.businessCode()
                    },
                    'fields': ['BranchServiceAreaDesc', 'EmployeeCode', 'LiveServiceArea', 'MultiBranchArea', 'DepotNumber', 'WeeklySequenceNumberRepeat']
                }
            ];
            this.LookUp.lookUpRecord(lookupIP, 500).subscribe((data) => {
                let lookUpRecord: any = data;
                if (lookUpRecord[0][0]) {
                    this.setControlValue('BranchServiceAreaDesc', lookUpRecord[0][0].BranchServiceAreaDesc);
                    this.setControlValue('EmployeeCode', lookUpRecord[0][0].EmployeeCode);
                    this.setControlValue('LiveServiceArea', lookUpRecord[0][0].LiveServiceArea);
                    this.setControlValue('MultiBranchArea', lookUpRecord[0][0].MultiBranchArea);
                    this.setControlValue('DepotNumber', lookUpRecord[0][0].DepotNumber);
                    if (this.isFieldNotHidden.weeklySequenceNumberRepeat) {
                        this.setControlValue('WeeklySequenceNumberRepeat', lookUpRecord[0][0].WeeklySequenceNumberRepeat);
                    }
                    this.setControlValue('oriEmployeeCode', this.getControlValue('EmployeeCode'));
                    this.pageParams.branchServiceRowID = lookUpRecord[0][0].ttBranchServiceArea;
                    this.fetchEmployeeSurname();
                    this.fetchPremisesUnitsWEDS();
                }
            });
        }
    }
    /**
     * Method to Fetch Branch Service Area Description and Other Details
     * On Page Load
     * @params: params: void
     * @return: : void
     */
    public fetchBranchServieAreaDetails(): void {
        if (this.pageParams.branchServiceRowID) {
            let search: any = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '0');
            search.set('ROWID', this.pageParams.branchServiceRowID);

            this.ajaxSource.next(this.ajaxconstant.START);
            this.subFetchEmployeeSurname = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
                            this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
                            this.setControlValue('EmployeeCode', data.EmployeeCode);
                            this.setControlValue('LiveServiceArea', data.LiveServiceArea);
                            this.setControlValue('MultiBranchArea', data.MultiBranchArea);
                            this.setControlValue('DepotNumber', data.DepotNumber);
                            if (this.isFieldNotHidden.weeklySequenceNumberRepeat) {
                                this.setControlValue('WeeklySequenceNumberRepeat', data.WeeklySequenceNumberRepeat);
                            }
                            this.setControlValue('oriEmployeeCode', this.getControlValue('EmployeeCode'));
                            this.pageParams.branchServiceRowID = data.ttBranchServiceArea;
                            this.fetchEmployeeSurname();
                            this.fetchPremisesUnitsWEDS();
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }
    /**
     * Method to Fetch Employee Surname and Other Details After
     * Getting Employee Code From fetchBranchServieAreaDescription Function
     * Or On Save Click
     * @params: params: void
     * @return: : void
     */
    public fetchEmployeeSurname(): void {
        if ((this.getControlValue('EmployeeCode') !== this.getControlValue('oriEmployeeCode')) && (this.formMode === this.c_s_MODE_UPDATE)) {
            this.isFieldNotHidden.trTechRetentionInd = true;
            this.setControlValue('TechRetentionInd', true);
        } else {
            this.isFieldNotHidden.trTechRetentionInd = false;
        }
        if (this.getControlValue('EmployeeCode')) {
            let search: any = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '0');
            search.set('EmployeeCode', this.getControlValue('EmployeeCode'));

            this.ajaxSource.next(this.ajaxconstant.START);
            this.subFetchEmployeeSurname = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }
    /**
     * Method to Fetch Employee Surname and Other Details After
     * @params: params: void
     * @return: : void
     */
    public fetchPremisesUnitsWEDS(): void {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let search: any = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '0');
            search.set('ServiceBranchNumber', this.utils.getBranchCode());
            search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));

            this.ajaxSource.next(this.ajaxconstant.START);
            this.subFetchEmployeeSurname = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.setControlValue('ServiceAreaNoOfExchanges', data.NoOfExchanges);
                            this.setControlValue('ServiceAreaNoOfCalls', data.NoOfCalls);
                            this.setControlValue('ServiceAreaWED', data.WEDValue);
                            if (this.isSetPageParams) {
                                this.setPageParameters();
                                this.isSetPageParams = false;
                            }
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }
    /**
     * Method to Execute Funcationality On Add Button Click
     * @params: params: void
     * @return: : void
     */
    public onBranchServiceAreaAdd(): void {
        this.onAddModeView();
    }
    /**
     * Method to Execute Funcationality On Cancel Button Click
     * @params: params: void
     * @return: : void
     */
    public onBranchServiceAreaCancel(): void {
        this.cancelModeView();
    }
}
