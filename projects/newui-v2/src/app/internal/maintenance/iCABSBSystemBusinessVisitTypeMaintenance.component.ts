import { Component, OnInit, Injector, OnDestroy, ViewChild, AfterContentInit } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { GenericEllipsisEvent, IGenericEllipsisControl } from '@shared/components/ellipsis-generic/ellipsis-generic';
import { MntConst } from '@shared/services/riMaintenancehelper';

@Component({
    templateUrl: 'iCABSBSystemBusinessVisitTypeMaintenance.html'
})

export class SystemBusinessVisitTypeMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private headerParams: any = {
        method: 'service-delivery/admin',
        module: 'service',
        operation: 'Business/iCABSBSystemBusinessVisitTypeMaintenance'
    };
    private messageProperties: ICabsModalVO = new ICabsModalVO();

    public pageId: string = '';
    public controls = [
        { name: 'SystemVisitTypeCode', required: true },
        { name: 'SystemVisitTypeDesc', disabled: true },
        { name: 'VisitTypeCode', required: true },
        { name: 'VisitTypeDesc' },
        { name: 'ROWID' }
    ];
    public visitTypeConfig: any = {
        isRequired: true,
        isTriggerValidate: false,
        active: { id: '', text: '' },
        arrData: [],
        inputParams: {
            parentMode: 'LookUp'
        }
    };
    public systemVisitTypeSearchConfig: IGenericEllipsisControl = {
        autoOpen: false,
        ellipsisTitle: 'System Business Visit Type Search',
        configParams: {
            table: '',
            shouldShowAdd: true,
            parentMode: 'LookUp'
        },
        httpConfig: {
            operation: 'Business/iCABSBSystemBusinessVisitTypeSearch',
            module: 'service',
            method: 'service-delivery/search'
        },
        tableColumn: [
            { title: 'System Visit Type Code', name: 'SystemVisitTypeCode', type: MntConst.eTypeCode, size: 6 },
            { title: 'Visit Type Code', name: 'VisitTypeCode', type: MntConst.eTypeText, size: 6, required: 'Required' },
            { title: 'Visit Type Description', name: 'VisitTypeDesc', type: MntConst.eTypeText, size: 20, required: 'Virtual' }
        ]
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBSYSTEMBUSINESSVISITTYPEMAINTENANCE;
        this.pageTitle = this.browserTitle = 'System/Business Visit Type Maintenance';
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.changeFormMode(this.c_s_MODE_SELECT);
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private displayMessage(type: string, message: string, fullError?: string): void {
        this.messageProperties = new ICabsModalVO();
        this.messageProperties.msg = message;
        this.messageProperties.fullError = fullError;

        this.modalAdvService['emit' + type](this.messageProperties);
    }

    private getDescriptions(): void {
        let lookupQuery: Array<any> = [];

        if (this.getControlValue('SystemVisitTypeCode')) {
            lookupQuery.push({
                'table': 'SystemVisitTypeLang',
                'query': {
                    'SystemVisitTypeCode': this.getControlValue('SystemVisitTypeCode'),
                    'BusinessCode': this.businessCode(),
                    'LanguageCode': this.riExchange.LanguageCode()
                },
                'fields': ['SystemVisitTypeDesc']
            });
        }

        if (this.formMode !== this.c_s_MODE_ADD && this.getControlValue('VisitTypeCode')) {
            lookupQuery.push({
                'table': 'VisitType',
                'query': {
                    'VisitTypeCode': this.getControlValue('VisitTypeCode'),
                    'BusinessCode': this.businessCode()
                },
                'fields': ['VisitTypeDesc']
            });
        }

        if (!lookupQuery.length) {
            return;
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupQuery).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data[0] && data[0].length) {
                this.pageParams['SystemVisitTypeDesc'] = data[0][0].SystemVisitTypeDesc;
                this.setControlValue('SystemVisitTypeDesc', data[0][0].SystemVisitTypeDesc);
            }
            if (data[1] && data[1].length) {
                this.pageParams['VisitTypeDesc'] = data[1][0].VisitTypeDesc;
                this.setControlValue('VisitTypeDesc', data[1][0].VisitTypeDesc);

                this.visitTypeConfig.active = {
                    id: this.getControlValue('VisitTypeCode'),
                    text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
                };
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            throw error;
        });
    }

    private changeFormMode(formMode: string): void {
        this.setFormMode(formMode);
        switch (formMode) {
            case this.c_s_MODE_SELECT:
                this.systemVisitTypeSearchConfig.autoOpen = true;
                break;
            case this.c_s_MODE_ADD:
                this.uiForm.markAsDirty();
                this.resetForm();
                break;
        }
    }

    private saveData(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};
        let postParam: string = this.formMode === this.c_s_MODE_ADD ? 'SystemVisitTypeCode' : 'ROWID';

        search.set(this.serviceConstants.Action, this.formMode === this.c_s_MODE_ADD ? '1' : '2');

        formData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
        formData[postParam] = this.getControlValue(postParam);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage || data.fullError) {
                    this.displayMessage('Error', data.errorMessage, data.fullError);
                    return;
                }
                this.setControlValue('ROWID', data.ttSystemBusinessVisitType);
                if (this.formMode === this.c_s_MODE_ADD) {
                    this.pageParams['SystemVisitTypeCode'] = this.getControlValue('SystemVisitTypeCode');
                    this.pageParams['SystemVisitTypeDesc'] = this.getControlValue('SystemVisitTypeDesc');
                    this.pageParams['ROWID'] = data.ttSystemBusinessVisitType;
                    this.pageParams['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
                    this.pageParams['VisitTypeDesc'] = this.getControlValue('VisitTypeDesc');
                }
                this.displayMessage('Message', MessageConstant.Message.RecordSavedSuccessfully);
                this.formPristine();
                this.systemVisitTypeSearchConfig.autoOpen = false;
                this.changeFormMode(this.c_s_MODE_UPDATE);
            }).catch(error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                throw error;
            });
    }

    private deleteData(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        search.set(this.serviceConstants.Action, '3');

        formData['ROWID'] = this.getControlValue('ROWID');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage || data.fullError) {
                    this.displayMessage('Error', data.errorMessage, data.fullError);
                    return;
                }
                this.displayMessage('Message', MessageConstant.Message.RecordDeletedSuccessfully);
                this.changeFormMode(this.c_s_MODE_SELECT);
                this.formPristine();
            }).catch(error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                throw error;
            });
    }

    private resetForm(): void {
        if (this.uiForm.pristine) {
            return;
        }
        for (let control in this.uiForm.controls) {
            if (this.uiForm.controls.hasOwnProperty(control)) {
                this.setControlValue(control, this.formMode !== this.c_s_MODE_UPDATE ? '' : this.pageParams[control]);
            }
        }
        if (this.formMode !== this.c_s_MODE_UPDATE) {
            this.visitTypeConfig.active = { id: '', text: '' };
        } else {
            this.visitTypeConfig.active = {
                id: this.getControlValue('VisitTypeCode'),
                text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
            };
        }
        this.formPristine();
    }

    public onSystemVisitTypeCodeChange(): void {
        let search: QueryParams = this.getURLSearchParamObject();

        if (this.formMode === this.c_s_MODE_ADD) {
            this.getDescriptions();
            return;
        }

        search.set(this.serviceConstants.Action, '0');
        search.set('SystemVisitTypeCode', this.getControlValue('SystemVisitTypeCode'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrGet(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage) {
                    this.displayMessage('Error', data.errorMessage, data.fullError);
                    return;
                }
                this.pageParams['SystemVisitTypeCode'] = data.SystemVisitTypeCode;
                this.setControlValue('SystemVisitTypeCode', data.SystemVisitTypeCode);
                this.pageParams['ROWID'] = data.ttSystemBusinessVisitType;
                this.setControlValue('ROWID', data.ttSystemBusinessVisitType);
                this.pageParams['VisitTypeCode'] = data.VisitTypeCode;
                this.setControlValue('VisitTypeCode', data.VisitTypeCode);

                this.formPristine();
                this.getDescriptions();
            }).catch(error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                throw error;
            });

        if (this.formMode === this.c_s_MODE_SELECT) {
            this.changeFormMode(this.c_s_MODE_UPDATE);
        }
    }

    public onVisitTypeChange(data: any): void {
        this.setControlValue('VisitTypeCode', data.VisitTypeCode);
        this.setControlValue('VisitTypeDesc', data.VisitTypeDesc);
        this.uiForm.markAsDirty();
    }

    public onSave(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.messageProperties = new ICabsModalVO();
            this.messageProperties.title = MessageConstant.Message.ConfirmTitle;
            this.messageProperties.msg = MessageConstant.Message.ConfirmRecord;
            this.messageProperties.confirmCallback = () => {
                this.saveData();
            };
            this.modalAdvService.emitPrompt(this.messageProperties);
        }
    }

    public onDelete(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.messageProperties = new ICabsModalVO();
            this.messageProperties.title = MessageConstant.Message.ConfirmTitle;
            this.messageProperties.msg = MessageConstant.Message.DeleteRecord;
            this.messageProperties.confirmCallback = () => {
                this.deleteData();
            };
            this.modalAdvService.emitPrompt(this.messageProperties);
        }
    }

    public onCancel(): void {
        this.resetForm();
        if (this.formMode === this.c_s_MODE_ADD) {
            this.changeFormMode(this.c_s_MODE_SELECT);
        }
    }

    public onSystemVisitTypeDataReceived(data: any): void {
        if (data[GenericEllipsisEvent.add]) {
            this.changeFormMode(this.c_s_MODE_ADD);
            return;
        }

        this.pageParams['SystemVisitTypeCode'] = data.SystemVisitTypeCode;
        this.pageParams['ROWID'] = data.ttSystemBusinessVisitType;
        this.pageParams['VisitTypeCode'] = data.VisitTypeCode;
        this.setControlValue('SystemVisitTypeCode', data.SystemVisitTypeCode);
        this.setControlValue('ROWID', data.ttSystemBusinessVisitType);
        this.setControlValue('VisitTypeCode', data.VisitTypeCode);
        this.formPristine();

        this.changeFormMode(this.c_s_MODE_UPDATE);

        this.getDescriptions();
    }
}
