/**
 * The SeServicePlanningDetailHgComponent component to manage service planing details add/update
 * @author icabs ui team
 * @version 1.0
 * @since   12/13/2017
 */
import { Component, OnInit, OnDestroy, Injector, ViewChild, QueryList, ViewChildren, ElementRef, HostListener } from '@angular/core';
import { QueryParams } from '../../../shared/services/http-params-wrapper';

import { Observable } from 'rxjs/Rx';

import { BaseComponent } from './../../base/BaseComponent';
import { CBBConstants } from './../../../shared/constants/cbb.constants';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { DropdownStaticComponent } from './../../../shared/components/dropdown-static/dropdownstatic';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { EmployeeSearchComponent } from './../../../app/internal/search/iCABSBEmployeeSearch';
import { AppModuleRoutes, ProspectToContractModuleRoutes, ServicePlanningModuleRoutes, InternalMaintenanceApplicationModuleRoutes, InternalGridSearchModuleRoutes } from './../../base/PageRoutes';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
    templateUrl: 'iCABSSeServicePlanningDetailHg.html',
    styles: [`
    .ptb3 {padding-bottom: 3px; padding-top: 3px;}
    .vnormalbackcolour {background-color: #eee;}
    .vclashcolour {background-color: #00BFFF;}
    .vClosedColour {background-color: #41383C;}
    .orangeButton { background: orange;color:#000; }
    .RedButton { background: red;color:#000; }
    `]
})

// Class definition starts here
export class SeServicePlanningDetailHgComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('defaultSelectDropdown') defaultSelectDropdown: DropdownStaticComponent;
    @ViewChildren('selectDayDropdown') selectDayDropdown: QueryList<DropdownStaticComponent>;

    private vTimeSeparator: string = '';
    private queryParams: any = {
        operation: 'Service/iCABSSeServicePlanningDetailHg',
        module: 'planning',
        method: 'service-planning/maintenance'
    };
    private islAppointmentRequired: boolean = false;
    private vRefreshDuration: any;
    private storeData: Object = {};

    public pageId: string = '';
    public vSCRowCount: number = 0;
    public vSCRowCountArray: Array<any> = [];
    public isVbEnableBSE: boolean = false;
    public defaultDayOptions: Array<any> = [];
    public isDefaultDayDisabled: boolean = false;
    public selectDayDropdownObj: Array<any> = [];
    public infoText: Array<string> = [];
    public dynamicText: Array<string> = [];
    public btnText: any = {
        showBtn: 'Show Comments',
        splitBtn: 'Split Service',
        unplanBtn: 'Unplan All',
        viewBtn: 'View Details',
        confirmBtn: 'Confirm Appointment'
    };
    public isDurationRequired: Array<boolean> = [];
    public hideObj: any = {
        divComments: true,
        tdContractHasExpired: true,
        addButton: false
    };
    public premiseInfo: string = '';
    public ellipsisConfig: Object = {
        showCloseButton: true,
        showHeader: true,
        modalConfig: {
            backdrop: 'static',
            keyboard: true
        },
        employee: []
    };
    public isAddDisabled: boolean = false;

    public controls = [
        { name: 'ContractNumber', disabled: false, required: false, value: '', commonValidator: true },
        { name: 'ContractName', disabled: false, required: false, value: '' },
        { name: 'PremiseNumber', disabled: false, required: false, value: '', type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: false, required: false, value: '' },
        { name: 'WeekNumber', disabled: false, required: false, value: '', type: MntConst.eTypeInteger },
        { name: 'ProductCode', disabled: false, required: false, value: '' },
        { name: 'ProductDesc', disabled: false, required: false, value: '' },
        { name: 'DefaultDay', disabled: false, required: false, value: '' },
        { name: 'OriginalVisitDuration', disabled: false, required: false, value: '' },
        { name: 'DefaultStartTime', disabled: false, required: false, value: '', type: MntConst.eTypeTime },
        { name: 'PlannedVisitDuration', disabled: false, required: false, value: '' },
        { name: 'StartDate', disabled: false, required: false, value: '', type: MntConst.eTypeDate },
        { name: 'EndDate', disabled: false, required: false, value: '', type: MntConst.eTypeDate },
        { name: 'ServiceCoverNumber', disabled: false, required: false, value: '' },
        { name: 'PlanVisitNumber', disabled: false, required: false, value: '' },
        { name: 'BranchNumber', disabled: false, required: false, value: '' },
        { name: 'BranchServiceAreaCode', disabled: false, required: false, value: '' },
        { name: 'ServicePlanNumber', disabled: false, required: false, value: '' },
        { name: 'ServicePlanStartDate', disabled: false, required: false, value: '' },
        { name: 'DiaryDate', disabled: false, required: false, value: '' },
        { name: 'DiaryEmployeeCode', disabled: false, required: false, value: '' },
        { name: 'DiaryEmployeeSurname', disabled: false, required: false, value: '' },
        { name: 'FirstWeekDay', disabled: false, required: false, value: '' },
        { name: 'PlanVisitRowID', disabled: false, required: false, value: '' },
        { name: 'GetWarnMessage', disabled: false, required: false, value: '' },
        { name: 'EmployeeCode', disabled: false, required: false, value: '' },
        { name: 'EmployeeSurname', disabled: false, required: false, value: '' },
        { name: 'PlanVisitRowIDNew', disabled: false, required: false, value: '' },
        { name: 'Comments', disabled: false, required: false, value: '' }
    ];

    constructor(injector: Injector, private el: ElementRef, private localStore: LocalStorageService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNINGDETAILHG;
        this.pageTitle = this.browserTitle = 'Service Planning Detail';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.cbbService.setCountryCode(this.riExchange.getParentHTMLValue(CBBConstants.c_s_URL_PARAM_COUNTRYCODE), true);
        this.cbbService.setBusinessCode(this.riExchange.getParentHTMLValue(CBBConstants.c_s_URL_PARAM_BUSINESSCODE), false, true);
        this.cbbService.setBranchCode(this.riExchange.getParentHTMLValue(CBBConstants.c_s_URL_PARAM_BRANCHCODE), true);
        this.setSystemCharacters();
        this.localStore.clear('ServicePlanningDetailHG');
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * This method is being used to control +/- keypress functionality
     * @param void
     * @return void
     */
    @HostListener('document:keypress', ['$event']) keyboardInput(event: KeyboardEvent): void {
        let sdate: Date = new Date(this.getControlValue('StartDate')), edate: Date = new Date(this.getControlValue('EndDate'));
        switch (event.keyCode) {
            case 43:
                this.setControlValue('StartDate', new Date(sdate.getFullYear(), sdate.getMonth(), sdate.getDate() + 7));
                this.setControlValue('EndDate', new Date(edate.getFullYear(), edate.getMonth(), edate.getDate() + 7));
                this.updateAllEmployees();
                this.setControlValue('GetWarnMessage', 'Yes');
                this.getLatestWeekNumber();
                break;
            case 45:
                this.setControlValue('StartDate', new Date(sdate.getFullYear(), sdate.getMonth(), sdate.getDate() - 7));
                this.setControlValue('EndDate', new Date(edate.getFullYear(), edate.getMonth(), edate.getDate() - 7));
                this.updateAllEmployees();
                this.setControlValue('GetWarnMessage', 'Yes');
                this.getLatestWeekNumber();
                break;
        }
    }


    /**
     * This method is used to initialize all parameters during page load
     * @param void
     * @return void
     */
    private init(): void {
        this.setFormMode(this.c_s_MODE_UPDATE);
        this.createRows();
        this.setControlValue('StartDate', this.riExchange.getParentHTMLValue('StartDate'));
        this.setControlValue('PlanVisitRowID', this.riExchange.getParentHTMLValue('PlanVisitRowID'));
        this.setControlValue('EndDate', this.riExchange.getParentHTMLValue('EndDate'));
        this.setControlValue('WeekNumber', this.riExchange.getParentHTMLValue('WeekNumber'));
        this.fetchPlanVisit();
        this.disableCols();
        this.buildWeekday();
        this.refreshAppConfirmed();
    }

    /**
     * This method is used to reset form data
     * @param void
     * @return void
     */
    private resetData(): void {
        this.uiForm.reset();
        this.isAddDisabled = false;
        this.riMaintenanceBeforeUpdate();
        this.setControlValue('StartDate', this.riExchange.getParentHTMLValue('StartDate'));
        this.setControlValue('PlanVisitRowID', this.riExchange.getParentHTMLValue('PlanVisitRowID'));
        this.setControlValue('EndDate', this.riExchange.getParentHTMLValue('EndDate'));
        this.setControlValue('WeekNumber', this.riExchange.getParentHTMLValue('WeekNumber'));
        this.fetchPlanVisit();
        this.refreshAppConfirmed();
        for (let index = 1; index <= this.vSCRowCount; index++) {
            this.markControlAsUnTouched('Duration' + index);
        }
    }

    /**
     * This method is used to disable fields
     * @param void
     * @return void
     */
    private disableCols(): void {
        this.disableControl('ContractNumber', true);
        this.disableControl('ContractName', true);
        this.disableControl('PremiseNumber', true);
        this.disableControl('PremiseName', true);
        this.disableControl('ProductCode', true);
        this.disableControl('ProductDesc', true);

        this.disableControl('ServicePlanNumber', true);
        this.disableControl('BranchNumber', true);
        this.disableControl('BranchServiceAreaCode', true);
        this.disableControl('PlannedVisitDuration', true);
        this.disableControl('OriginalVisitDuration', true);
        this.disableControl('StartDate', true);
        this.disableControl('WeekNumber', true);
        this.disableControl('EndDate', true);
        for (let index = 1; index <= this.vSCRowCount; index++) {
            this.disableControl('Booked' + index, true);
            this.disableControl('TechnicianDesc' + index, true);
        }
    }

    /**
     * This method used to initialize rows dynamic columns
     * @param void
     * @return void
     */
    private createRows(): void {
        this.vSCRowCountArray = []; let dropdownObj: Object = {}, employeeObj: Object = {};
        this.isDurationRequired = [];
        for (let index = 1; index <= this.vSCRowCount; index++) {
            this.controls.push({ name: 'Technician' + index, disabled: false, required: false, value: '', commonValidator: true });
            this.controls.push({ name: 'TechnicianDesc' + index, disabled: false, required: false, value: '' });
            this.controls.push({ name: 'SelectDay' + index, disabled: false, required: false, value: '' });
            this.controls.push({ name: 'Duration' + index, disabled: false, required: false, value: '', type: MntConst.eTypeTime });
            this.controls.push({ name: 'StartTime' + index, disabled: false, required: false, value: '', type: MntConst.eTypeTime });
            this.controls.push({ name: 'Booked' + index, disabled: false, required: false, value: '' });
            this.controls.push({ name: 'ClashDiaryDate' + index, disabled: false, required: false, value: '' });
            this.controls.push({ name: 'Day' + index, disabled: false, required: false, value: '' });
            this.vSCRowCountArray.push(index);
            dropdownObj = {
                selectDayOptions: [],
                isSelectDayDisabled: false
            };
            employeeObj = {
                contentComponent: EmployeeSearchComponent,
                disabled: false,
                inputParamsEmployeeSearch: {
                    parentMode: 'LookUp-Service-All'
                }
            };
            this.ellipsisConfig['employee'].push(employeeObj);
            this.infoText[index] = '';
            this.dynamicText[index] = '';
            this.selectDayDropdownObj.push(dropdownObj);
            this.isDurationRequired.push(false);
        }
        this.riExchange.renderForm(this.uiForm, this.controls);
        this.setCommonAttributes();
    }

    /**
     * This method used to set system characters in Component variables
     * @param void
     * @return void
     */
    private setSystemCharacters(): void {
        let sysCharList: Array<number> = [
            this.sysCharConstants.SystemCharBranchServiceAreaEmployees
        ];
        let sysNumbers: string = sysCharList.join(',');
        let dataParam: Array<Object> = [{
            'table': 'riCountry',
            'query': { 'riCountry': this.countryCode() },
            'fields': ['riTimeSeparator']
        }];
        let observableBatch = [this.lookUpRecord(dataParam, 0), this.fetchSysChar(sysNumbers)];
        this.ajaxSource.next(this.ajaxconstant.START);
        Observable.forkJoin(
            observableBatch).subscribe((data) => {
                try {
                    if (data[0]['results'].length > 0) {
                        this.vTimeSeparator = data[0]['results'][0][0]['riTimeSeparator'];
                    }
                    this.vSCRowCount = data[1]['records'][0]['Integer'];
                    this.isVbEnableBSE = data[1]['records'][0]['Required'];
                } catch (error) {
                    this.logger.warn(error);
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.init();
            });
    }

    /**
     * This method used to set descriptions using service call
     * @param void
     * @return void
     */
    private populateDescriptions(): void {
        let dataParamA: Array<Object> = [{
            'table': 'PlanVisit',
            'query': {
                'ContractNumber': this.getControlValue('ContractNumber'),
                'PremiseNumber': this.getControlValue('PremiseNumber'),
                'BusinessCode': this.businessCode(),
                'ProductCode': this.getControlValue('ProductCode'),
                'ServiceCoverNumber': this.getControlValue('ServiceCoverNumber'),
                'PlanVisitNumber': this.getControlValue('PlanVisitNumber')
            },
            'fields': ['ServicePlanNumber', 'BranchNumber', 'BranchServiceAreaCode', 'PlannedVisitDuration', 'OriginalVisitDuration']
        },
        {
            'table': 'Contract',
            'query': {
                'ContractNumber': this.getControlValue('ContractNumber'),
                'BusinessCode': this.businessCode()
            },
            'fields': ['ContractName']
        }];
        let dataParamB: Array<Object> = [{
            'table': 'Premise',
            'query': {
                'ContractNumber': this.getControlValue('ContractNumber'),
                'PremiseNumber': this.getControlValue('PremiseNumber'),
                'BusinessCode': this.businessCode()
            },
            'fields': ['PremiseName']
        },
        {
            'table': 'Product',
            'query': {
                'ProductCode': this.getControlValue('ProductCode'),
                'BusinessCode': this.businessCode()
            },
            'fields': ['ProductDesc']
        }];
        let observableBatch = [this.lookUpRecord(dataParamA, 0), this.lookUpRecord(dataParamB, 0)];
        this.ajaxSource.next(this.ajaxconstant.START);
        Observable.forkJoin(
            observableBatch).subscribe((data) => {
                try {
                    this.setControlValue('BranchNumber', data[0]['results'][0][0]['BranchNumber']);
                    this.setControlValue('BranchServiceAreaCode', data[0]['results'][0][0]['BranchServiceAreaCode']);
                    this.setControlValue('OriginalVisitDuration', data[0]['results'][0][0]['OriginalVisitDuration']);
                    this.setControlValue('PlannedVisitDuration', data[0]['results'][0][0]['PlannedVisitDuration']);
                    this.setControlValue('ServicePlanNumber', data[0]['results'][0][0]['ServicePlanNumber']);
                    this.setControlValue('ContractName', data[0]['results'][1][0]['ContractName']);
                    this.setControlValue('PremiseName', data[1]['results'][0][0]['PremiseName']);
                    this.setControlValue('ProductDesc', data[1]['results'][1][0]['ProductDesc']);
                } catch (error) {
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
                }

                if (this.isVbEnableBSE && this.riExchange.getParentHTMLValue('Status') === 'no') {
                    this.buildCombos('Add');
                } else {
                    this.buildCombos('Select');
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    /**
     * This method to service call for system characters
     * @param any
     * @return any observable to subsribe system characters response
     */
    private fetchSysChar(sysCharNumbers: any): any {
        let querySysChar: QueryParams = new QueryParams();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.BusinessCode, this.businessCode());
        querySysChar.set(this.serviceConstants.CountryCode, this.countryCode());
        querySysChar.set(this.serviceConstants.SystemCharNumber, sysCharNumbers);
        return this.httpService.sysCharRequest(querySysChar);
    }

    /**
     * This method to service call for look up table data
     * @param any
     * @return any observable to subsribe table data response
     */
    private lookUpRecord(data: any, maxresults: any): any {
        let queryLookUp: QueryParams = new QueryParams();
        queryLookUp.set(this.serviceConstants.Action, '0');
        queryLookUp.set(this.serviceConstants.BusinessCode, this.businessCode());
        queryLookUp.set(this.serviceConstants.CountryCode, this.countryCode());
        if (maxresults) {
            queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        }
        return this.httpService.lookUpRequest(queryLookUp, data);
    }

    /**
     * This method to service call get service planing detail data
     * @param void
     * @return void
     */
    private fetchPlanVisit(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        search.set('ROWID', this.getControlValue('PlanVisitRowID'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    try {
                        for (let field in data) {
                            if (data.hasOwnProperty(field)) {
                                if (this.uiForm.controls[field]) {
                                    this.setControlValue(field, data[field]);
                                }
                            }
                        }
                        setTimeout(() => {
                            this.populateDescriptions();
                        }, 10);
                    } catch (e) {
                        this.modalAdvService.emitError(new ICabsModalVO(e['errorMessage'], e['fullError']));
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
            );
    }

    /**
     * This method to build weekday dropdown
     * @param void
     * @return void
     */
    private buildWeekday(): void {
        let startDayValue: number = 0, strWeekday: string = '', arrWeekday: Array<any> = [];
        startDayValue = (this.globalize.parseDateStringToDate(this.getControlValue('StartDate')) as Date).getDay() + 1;
        strWeekday = 'Saturday|Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday|Monday|Tuesday|Wednesday|Thursday|Friday';
        arrWeekday = strWeekday.split('|');
        for (let index of this.vSCRowCountArray) {
            for (let iDaysBuildLoop = 0; iDaysBuildLoop <= 6; iDaysBuildLoop++) {
                this.buildWeekdayOption(arrWeekday[startDayValue + iDaysBuildLoop], iDaysBuildLoop, index);
            }
        }
        this.defaultSelectDropdown.selectedItem = this.defaultDayOptions[0].value;
        this.setControlValue('DefaultDay', this.defaultDayOptions[0].value);
    }

    /**
     * This method to add weekday option value
     * @param strText this is parameter for option text
     * @param strValue this is parameter for option value
     * @param intObjectloop this is parameter for index of controls
     * @return void
     */
    private buildWeekdayOption(strText: string, strValue: any, intObjectloop: any): void {
        let optionObj: Object = { value: strValue, text: strText };
        this.selectDayDropdownObj[intObjectloop - 1].selectDayOptions.push(optionObj);
        if (intObjectloop === 1) {
            this.defaultDayOptions.push(optionObj);
        }

    }

    /**
     * This method to fetch and update control vlaue
     * @param mode this is type /mode of current state(select/add/update)
     * @return void
     */
    private buildCombos(mode: string): void {
        let codeReturn: any, descReturn: any, dayReturn: any, arr: any, elleArr: any, ct: any, vPlanVisitRowID: any, closeReturn: any, clashDiaryDateReturn: any, bookedReturn: any, durationReturn: any, vFunction: any, startTimeReturn: any, durationListReturn: any, clashReturn: any, ClashDiaryDateReturn: any, ClosedReturn: any;
        let search: QueryParams = this.getURLSearchParamObject();
        vPlanVisitRowID = this.getControlValue('PlanVisitRowID');
        vFunction = 'BuildCombos' + mode;
        this.refreshDates();
        let postData: Object = {};
        postData[this.serviceConstants.Function] = vFunction;
        postData['PlannedVisitDuration'] = this.getControlValue('PlannedVisitDuration') || '00:00';
        postData['OriginalVisitDuration'] = this.getControlValue('OriginalVisitDuration');
        postData['StartDate'] = this.getControlValue('StartDate');
        postData['PlanVisitRowID'] = vPlanVisitRowID;
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    try {
                        codeReturn = data.TechListCodes.split('\n');
                        descReturn = data.TechListDescs.split('\n');
                        dayReturn = data.DayListCodes.split('\n');
                        startTimeReturn = data.StartTime.split('\n');
                        durationListReturn = data.DurationList.split('\n');
                        durationReturn = data.Duration.split('\n');
                        bookedReturn = data.Booked.split('\n');
                        clashReturn = data.Clash.split('\n');
                        closeReturn = data.Closed.split('\n');
                        clashDiaryDateReturn = data.ClashDiaryDate.split('\n');
                        for (let i = 1; i <= this.vSCRowCount; i++) {
                            this.setControlValue('Technician' + i, codeReturn[i - 1]);
                            if (clashReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#TechnicianTD' + i).classList.add('vclashcolour');
                            } else if (closeReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#TechnicianTD' + i).classList.add('vClosedColour');
                            } else {
                                this.el.nativeElement.querySelector('#TechnicianTD' + i).classList.add('vnormalbackcolour');
                            }
                            this.setControlValue('TechnicianDesc' + i, descReturn[i - 1]);
                            if (this.getControlValue('TechnicianDesc' + i)) {
                                this.setRequiredStatus('Duration' + i, true);
                                this.isDurationRequired[i - 1] = true;
                            }
                            this.setControlValue('Booked' + i, bookedReturn[i - 1]);
                            if (clashReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#BookedTD' + i).classList.add('vclashcolour');
                            } else if (closeReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#BookedTD' + i).classList.add('vClosedColour');
                            } else {
                                this.el.nativeElement.querySelector('#BookedTD' + i).classList.add('vnormalbackcolour');
                            }
                            this.setControlValue('StartTime' + i, startTimeReturn[i - 1]);
                            if (clashReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#StartTimeTD' + i).classList.add('vclashcolour');
                            } else if (closeReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#StartTimeTD' + i).classList.add('vClosedColour');
                            } else {
                                this.el.nativeElement.querySelector('#StartTimeTD' + i).classList.add('vnormalbackcolour');
                            }

                            if (this.isVbEnableBSE || this.riExchange.getParentHTMLValue('Status') === 'yes') {
                                this.setControlValue('SelectDay' + i, dayReturn[i - 1]);
                                this.selectDayDropdown.toArray()[i - 1].selectedItem = dayReturn[i - 1];
                            } else {
                                this.setControlValue('SelectDay' + i, this.defaultSelectDropdown.selectedItem);
                                this.selectDayDropdown.toArray()[i - 1].selectedItem = this.defaultSelectDropdown.selectedItem;
                            }
                            if (clashReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#SelectDayTD' + i).classList.add('vclashcolour');
                            } else if (closeReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#SelectDayTD' + i).classList.add('vClosedColour');
                            } else {
                                this.el.nativeElement.querySelector('#SelectDayTD' + i).classList.add('vnormalbackcolour');
                            }
                            this.setControlValue('Duration' + i, durationReturn[i - 1]);
                            if (clashReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#DurationTD' + i).classList.add('vclashcolour');
                            } else if (closeReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#DurationTD' + i).classList.add('vClosedColour');
                            } else {
                                this.el.nativeElement.querySelector('#DurationTD' + i).classList.add('vnormalbackcolour');
                            }

                            if (clashReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#InfoImage' + i).style.display = '';
                                this.infoText[i] = 'Diary Clash: ';
                                this.dynamicText = clashReturn[i - 1];
                            } else if (closeReturn[i - 1]) {
                                this.el.nativeElement.querySelector('#InfoImage' + i).style.display = '';
                                this.infoText[i] = 'Premise is on a Closed Calender Template';
                                this.dynamicText[i] = '';
                            } else {
                                this.el.nativeElement.querySelector('#InfoImage' + i).style.display = 'none';
                                this.setControlValue('ClashDiaryDate' + i, '');
                            }
                        }
                        this.setControlValue('OriginalVisitDuration', data.OriginalVisitDuration);
                        if (data.AppointmentConfirmed === 'yes') {
                            this.setAppointmentRequired('yes', 'co');
                        }
                        if (data.AppointmentRequired === 'yes') {
                            this.setAppointmentRequired('yes', 'req');
                        }
                        this.hideObj['tdContractHasExpired'] = true;
                        if (data.ContractExpired === 'yes') {
                            this.hideObj['tdContractHasExpired'] = false;
                        }
                        this.hideObj['divComments'] = true;
                        this.btnText['showBtn'] = 'Show Comments';
                        this.setControlValue('Comments', '');
                        if (data.Comments) {
                            this.hideObj['divComments'] = false;
                            this.btnText['showBtn'] = 'Hide Comments';
                            this.setControlValue('Comments', data.Comments);
                        }
                        if (mode === 'Select') {
                            if (codeReturn[0]) {
                                this.hideObj['addButton'] = true;
                            }
                            this.premiseInfo = data.PremiseDrilldown;
                            this.updateDurations();
                        }
                        this.riMaintenanceBeforeUpdate();

                    } catch (error) {
                        this.logger.warn(error);
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method to coloring Appointment button based on current state
     * @param mode is the logical value yes/no
     * @param type is the type of appointment state co/req(Confirm/Required)
     * @return void
     */
    private setAppointmentRequired(mode: string, type?: string): void {
        switch (type) {
            case 'co':
                if (mode === 'yes') {
                    this.islAppointmentRequired = true;
                    this.el.nativeElement.querySelector('#btnConfirmApp').classList.add('orangeButton');
                    this.btnText['confirmBtn'] = 'Confirmed Appointment';
                } else if (mode === 'yes' && !this.islAppointmentRequired) {
                    this.el.nativeElement.querySelector('#btnConfirmApp').classList.remove('orangeButton');
                    this.btnText['confirmBtn'] = 'Confirmed Appointment';
                }
                break;
            case 'req':
                if (mode === 'yes') {
                    this.islAppointmentRequired = true;
                    this.el.nativeElement.querySelector('#btnConfirmApp').classList.add('RedButton');
                    this.btnText['confirmBtn'] = 'Confirmed Appointment';
                }
                break;
        }

    }

    /**
     * This method to refresh dates
     * @param void
     * @return void
     */
    private refreshDates(): void {
        for (let index = 1; index <= this.vSCRowCount; index++) {
            this.setControlValue('Day' + index, this.getControlValue('SelectDay' + index));
        }
    }

    /**
     * This method to update durations of all duration controls
     * @param void
     * @return void
     */
    private updateDurations(): void {
        let vbTimeSec: any, vbDurationHours: any, vbDurationMinutes: any, vEmployeeDurationDurationCount: number = 0, vbtfTimeInMins: any, vbtfTimeOddMins: any, vbtfTimeHours: any, controlValue: any;
        for (let i = 1; i <= this.vSCRowCount; i++) {
            controlValue = this.globalize.formatTimeToLocaleFormat(this.getControlValue('Duration' + i)).toString();
            if (controlValue && controlValue.length > 3) {
                this.setControlValue('Duration' + i, controlValue.substr(0, 2) + this.vTimeSeparator + controlValue.substr(-2, 2));
                vbTimeSec = 0;
                controlValue = this.globalize.formatTimeToLocaleFormat(this.getControlValue('Duration' + i)).toString();
                vbDurationHours = parseInt(controlValue.substr(0, controlValue.length - 3), 10);
                vbDurationMinutes = parseInt(controlValue.substr(-2, 2), 10);
                if (vbDurationHours > 0) {
                    vbTimeSec += (vbDurationHours * 60 * 60);
                }
                if (vbDurationMinutes > 0) {
                    vbTimeSec += (vbDurationMinutes * 60);
                }
                vEmployeeDurationDurationCount += vbTimeSec;
            }
        }
        this.setControlValue('PlannedVisitDuration', this.utils.secondsToHms(vEmployeeDurationDurationCount));

    }

    /**
     * This method to populate data from parent
     * @param void
     * @return void
     */
    private populateFromParent(): void {
        if (this.riExchange.getParentHTMLValue('Status') === 'no') {
            for (let index = 1; index <= this.vSCRowCount; index++) {
                this.setControlValue('Technician' + index, this.localStore.retrieve('LastSavedEmployees')['Technician' + index]);
                this.setControlValue('TechnicianDesc' + index, this.localStore.retrieve('LastSavedEmployees')['TechnicianDesc' + index]);
                this.setControlValue('Day' + index, this.localStore.retrieve('LastSavedEmployees')['Day' + index]);
                this.setControlValue('SelectDay' + index, this.localStore.retrieve('LastSavedEmployees')['Day' + index]);
                this.setControlValue('EmployeeCode', this.getControlValue('Technician' + index));
                this.defaultEmpDefaults(index);
            }
        }
    }

    /**
     * This method to set DefaultDay
     * @param index sequence number of controls
     * @return void
     */
    private defaultEmpDefaults(index: any): void {
        if (this.getControlValue('DefaultDay')) {
            this.setControlValue('SelectDay' + index, this.getControlValue('DefaultDay'));
        }
        this.updateEmployees(this.getControlValue('EmployeeCode'), index, 1);
        if (this.getControlValue('DefaultStartTime')) {
            this.setControlValue('StartTime' + index, this.getControlValue('DefaultStartTime'));
        }
    }

    /**
     * This method to update employee
     * @param employeeCode
     * @param index sequence number
     * @param mode numeric type
     * @return void
     */
    private updateEmployees(employeeCode: string, index: any, mode: any): void {
        let vEmployeeCount: any, vEmployeeDurationDurationCount: any, vbDurationHours: any, vbDurationMinutes: any, vbTimeSec: any, vbOriginalVisitDuration: any, vbtfTimeInMins: any, vbtfTimeOddMins: any, vbtfTimeHours: any;
        let search: QueryParams = this.getURLSearchParamObject();
        vEmployeeCount = 0;
        for (let i = 0; i <= this.vSCRowCount; i++) {
            if (this.getControlValue('Technician' + i)) {
                vEmployeeCount++;
            }
        }
        let postData: Object = {};
        postData[this.serviceConstants.Function] = 'UpdateEmployees';
        postData['PlanVisitRowID'] = this.getControlValue('PlanVisitRowID');
        postData['CountEmployees'] = vEmployeeCount;
        postData['StartDate'] = this.getControlValue('StartDate') ? this.getControlValue('StartDate') : '';
        postData['PlannedVisitDuration'] = this.getControlValue('PlannedVisitDuration');
        postData['OriginalVisitDuration'] = this.getControlValue('OriginalVisitDuration');
        postData['EmployeeCode'] = employeeCode ? employeeCode : '';
        postData['RefreshDuration'] = this.vRefreshDuration ? this.vRefreshDuration : '';
        postData['SelectDay'] = this.getControlValue('SelectDay' + index) ? this.getControlValue('SelectDay' + index) : '';
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    try {
                        if (mode === 1) {
                            this.setControlValue('SelectDay' + index, data.DayListCode);
                            this.selectDayDropdown.toArray()[index - 1].selectedItem = data.DayListCode;
                        }
                        this.setControlValue('Booked' + index, data.Booked);
                        if (!this.vRefreshDuration) {
                            for (let i = 1; i <= vEmployeeCount; i++) {
                                this.setControlValue('Duration' + i, data['Duration' + i]);
                            }
                        }
                        this.updateDurations();
                        if (data.Clash) {
                            this.el.nativeElement.querySelector('#InfoImage' + index).style.display = '';
                            this.infoText[index] = 'Diary Clash: ';
                            this.dynamicText[index] = data.Clash;
                            this.setControlValue('ClashDiaryDate' + index, data['ClashDiaryDate']);
                            this.el.nativeElement.querySelector('#TechnicianTD' + index).classList.add('vclashcolour');
                            this.el.nativeElement.querySelector('#BookedTD' + index).classList.add('vclashcolour');
                            this.el.nativeElement.querySelector('#StartTimeTD' + index).classList.add('vclashcolour');
                            this.el.nativeElement.querySelector('#SelectDayTD' + index).classList.add('vclashcolour');
                            this.el.nativeElement.querySelector('#DurationTD' + index).classList.add('vclashcolour');
                        } else if (data.Closed) {
                            this.el.nativeElement.querySelector('#InfoImage' + index).style.display = '';
                            this.infoText[index] = 'Premise is on a Closed Calender Template';
                            this.dynamicText[index] = '';
                            this.el.nativeElement.querySelector('#TechnicianTD' + index).classList.add('vClosedcolour');
                            this.el.nativeElement.querySelector('#BookedTD' + index).classList.add('vClosedcolour');
                            this.el.nativeElement.querySelector('#StartTimeTD' + index).classList.add('vClosedcolour');
                            this.el.nativeElement.querySelector('#SelectDayTD' + index).classList.add('vClosedcolour');
                            this.el.nativeElement.querySelector('#DurationTD' + index).classList.add('vClosedcolour');
                        } else {
                            this.setControlValue('ClashDiaryDate' + index, '');
                            this.el.nativeElement.querySelector('#InfoImage' + index).style.display = 'none';
                            this.el.nativeElement.querySelector('#TechnicianTD' + index).classList.add('vnormalbackcolour');
                            this.el.nativeElement.querySelector('#BookedTD' + index).classList.add('vnormalbackcolour');
                            this.el.nativeElement.querySelector('#StartTimeTD' + index).classList.add('vnormalbackcolour');
                            this.el.nativeElement.querySelector('#SelectDayTD' + index).classList.add('vnormalbackcolour');
                            this.el.nativeElement.querySelector('#DurationTD' + index).classList.add('vnormalbackcolour');
                        }

                    } catch (error) {
                        this.logger.warn(error);
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method to call before going to add mode
     * @param void
     * @return void
     */
    private riMaintenanceBeforeAddMode(): void {
        this.setFormMode(this.c_s_MODE_ADD);
        for (let i = 1; i <= this.vSCRowCount; i++) {
            this.el.nativeElement.querySelector('#InfoImage' + i).style.display = 'none';
            this.disableControl('SelectDay' + i, false);
        }
        this.disableControl('DefaultDay', false);
        if (this.isVbEnableBSE) {
            this.buildCombos('Add');
        } else {
            this.populateFromParent();
        }
        this.disableCols();
        this.el.nativeElement.querySelector('#Technician1').focus();
    }

    /**
     * This method to call before going to update mode
     * @param void
     * @return void
     */
    private riMaintenanceBeforeUpdate(): void {
        for (let i = 1; i <= this.vSCRowCount; i++) {
            this.el.nativeElement.querySelector('#InfoImage' + i).style.display = 'none';
            this.disableControl('SelectDay' + i, false);
        }
        this.disableControl('DefaultDay', false);
        this.disableCols();
        this.el.nativeElement.querySelector('#Technician1').focus();
        this.populateFromParent();
    }

    /**
     * This method is being called after saved data add/update
     * @param void
     * @return void
     */
    private riMaintenanceBeforeSaveAdd(): void {
        let msgObj: any = new ICabsModalVO(MessageConstant.PageSpecificMessage.selectDayMandatory);
        msgObj.title = MessageConstant.PageSpecificMessage.selectDayTitle;
        for (let i = 1; i <= this.vSCRowCount; i++) {
            if (this.getControlValue('Technician' + i) && isNaN(this.getControlValue('SelectDay' + i))) {
                this.modalAdvService.emitMessage(msgObj);
            }
        }
        this.refreshDates();
        this.setControlValue('PlanVisitRowIDNew', this.getControlValue('PlanVisitRowID'));
    }

    /**
     * This method is being called after OK click of unplan pop up
     * @param void
     * @return void
     */
    private confirmUnplanOk(): void {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject(), vPlanVisitRowID: any;
        vPlanVisitRowID = this.getControlValue('PlanVisitRowID');
        postData[this.serviceConstants.Function] = 'UnPlanAll';
        postData['PlannedVisitDuration'] = this.getControlValue('PlannedVisitDuration');
        postData['OriginalVisitDuration'] = this.getControlValue('OriginalVisitDuration');
        postData['StartDate'] = this.getControlValue('StartDate');
        postData['PlanVisitRowID'] = vPlanVisitRowID;
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    if (this.getControlValue('ServicePlanNumber')) {
                        this.setControlValue('ServicePlanNumber', 0);
                    }
                    for (let i = 1; i <= this.vSCRowCount; i++) {
                        this.setControlValue('Technician' + i, '');
                        this.setControlValue('TechnicianDesc' + i, '');
                        this.setControlValue('Booked' + i, '');
                        this.setControlValue('Duration' + i, '');
                        this.setControlValue('StartTime' + i, '');
                        this.setControlValue('SelectDay' + i, '');
                        this.selectDayDropdown.toArray()[i - 1].selectedItem = '';
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is called after saving data
     * @param void
     * @return void
     */
    private riMaintenanceAfterSave(): void {
        let vbDurationHours: any, vbDurationMinutes: any, vbOriginalVisitDuration: any, vbPlannedVisitDuration: any, vbMsgResult: any, controlValue: any;
        this.setFormMode(this.c_s_MODE_UPDATE);
        this.disableControl('DefaultDay', true);
        this.hideObj['addButton'] = true;
        vbPlannedVisitDuration = 0;
        vbDurationHours = 0;
        vbDurationMinutes = 0;

        controlValue = this.getControlValue('PlannedVisitDuration');
        vbDurationHours = parseInt(controlValue.substr(0, controlValue.length - 3), 10);
        vbDurationMinutes = parseInt(controlValue.substr(-2, 2), 10);
        if (vbDurationHours > 0) {
            vbPlannedVisitDuration += (vbDurationHours * 60 * 60);
        }
        if (vbDurationMinutes > 0) {
            vbPlannedVisitDuration += (vbDurationMinutes * 60);
        }
        controlValue = this.getControlValue('OriginalVisitDuration');
        vbOriginalVisitDuration = 0;
        vbDurationHours = 0;
        vbDurationMinutes = 0;
        vbDurationHours = parseInt(controlValue.substr(0, controlValue.length - 3), 10);
        vbDurationMinutes = parseInt(controlValue.substr(-2, 2), 10);
        if (vbDurationHours > 0) {
            vbOriginalVisitDuration += (vbDurationHours * 60 * 60);
        }
        if (vbDurationMinutes > 0) {
            vbOriginalVisitDuration += (vbDurationMinutes * 60);
        }
        if (vbOriginalVisitDuration > vbPlannedVisitDuration) {
            let msgObj: any = new ICabsModalVO(MessageConstant.PageSpecificMessage.plannedVisitDurationMsg, null, this.splitServiceOnClick.bind(this), this.splitCancelOnClick.bind(this));
            msgObj.title = MessageConstant.PageSpecificMessage.plannedVisitDurationTitle;
            this.modalAdvService.emitPrompt(msgObj);
            this.populateParent();
        } else {
            this.populateParent();
            this.storeData[CBBConstants.c_s_URL_PARAM_COUNTRYCODE] = this.utils.getCountryCode();
            this.storeData[CBBConstants.c_s_URL_PARAM_BUSINESSCODE] = this.utils.getBusinessCode();
            this.storeData[CBBConstants.c_s_URL_PARAM_BRANCHCODE] = this.utils.getBranchCode();
            this.storeData['rowid'] = this.riExchange.getParentHTMLValue('PlanVisitRowID');
            this.localStore.store('ServicePlanningDetailHG', JSON.stringify(this.storeData));
            window.close();
        }
    }

    /**
     * This method is called after saving data and populate parent data
     * @param void
     * @return void
     */
    private populateParent(): void {
        let lastSavedEmployeed: Object = {};
        for (let index = 1; index <= this.vSCRowCount; index++) {
            this.riExchange.setParentHTMLValue('Technician' + index, this.getControlValue('Technician' + index));
            this.riExchange.setParentHTMLValue('TechnicianDesc' + index, this.getControlValue('TechnicianDesc' + index));
            this.riExchange.setParentHTMLValue('Day' + index, this.getControlValue('Day' + index));
            lastSavedEmployeed['Technician' + index] = this.getControlValue('Technician' + index);
            lastSavedEmployeed['TechnicianDesc' + index] = this.getControlValue('TechnicianDesc' + index);
            lastSavedEmployeed['Day' + index] = this.getControlValue('Day' + index);
        }
        if (!this.isVbEnableBSE) {
            if (this.localStore.retrieve('LastSavedEmployees')) {
                this.localStore.clear('LastSavedEmployees');
            }
            this.localStore.store('LastSavedEmployees', lastSavedEmployeed);
        }
    }
    /**
     * This method is called to trigger http service to save data
     * @param void
     * @return void
     */
    private saveServiceCall(): void {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject(), vPlanVisitRowID: any;
        for (let contrl of this.controls) {
            postData[contrl.name] = this.getControlValue(contrl.name);
        }
        if (this.getFormMode() === this.c_s_MODE_UPDATE) {
            search.set(this.serviceConstants.Action, '2');
        } else {
            search.set(this.serviceConstants.Action, '1');
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riMaintenanceAfterSave();
                    this.formPristine();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is used add same class of next element
     * @param elementId is the element id
     * @param index is the sequence number
     * @return void
     */
    private addClassNextElement(elementId: string, index: any): void {
        let prevClassList: Array<any>, nextClassList: Array<any>;
        prevClassList = this.el.nativeElement.querySelector('#' + elementId + index).classList.value.split(' ');
        nextClassList = this.el.nativeElement.querySelector('#' + elementId + (index + 1)).classList.value.split(' ');
        for (let cls of prevClassList) {
            this.el.nativeElement.querySelector('#' + elementId + index).classList.remove(cls);
        }
        for (let cls of nextClassList) {
            this.el.nativeElement.querySelector('#' + elementId + index).classList.add(cls);
        }
    }

    /**
     * This method to set changed value of DefaultDay
     * @param data this is the DefaultDay value from HTML input
     * @return void
     */
    public defaultDayOptionsChange(data: any): void {
        this.setControlValue('DefaultDay', data);
        for (let i = 1; i <= this.vSCRowCount; i++) {
            if (this.getControlValue('Technician' + i)) {
                this.setControlValue('SelectDay' + i, this.getControlValue('DefaultDay'));
                this.selectDayDropdown.toArray()[i - 1].selectedItem = this.getControlValue('DefaultDay');
                this.updateEmployees(this.getControlValue('Technician' + i), i, 2);
            }
        }
        if (data)
            this.uiForm.controls['DefaultDay'].markAsDirty();
    }

    /**
     * This method to update SelectDay value
     * @param data this is the SelectDay value from HTML input
     * @return void
     */
    public selectDayOptionsChange(data: any, index: any): void {
        this.setControlValue('SelectDay' + index, data);
        this.setControlValue('Day' + index, data);
        this.updateEmployees(this.getControlValue('Technician' + index), index, 2);
        if (data)
            this.uiForm.controls['SelectDay' + index].markAsDirty();
    }

    /**
     * This method is being called after add button click
     * @param void
     * @return void
     */
    public addDetailLine(): void {
        this.isAddDisabled = true;
        this.riMaintenanceBeforeAddMode();
    }

    public splitServiceOnClick(): void {
        this.navigate('ServicePlanning', AppModuleRoutes.SERVICEPLANNING + ServicePlanningModuleRoutes.ICABSSESERVICEPLANNINGSPLITSERVICEMAINTENANCEHG, {
            PlanVisitRowID: this.getControlValue('PlanVisitRowID')
        });
    }

    private splitCancelOnClick(): void {
        this.populateParent();
        setTimeout(() => {
            this.location.back();
        }, 0);
    }

    /**
     * This method is being called after clicking viewDetails
     * @param void
     * @return void
     */
    public viewDetailsOnClick(): void {
       //Navigate to screen riFileName=Service/iCABSSeServicePlanningDetailGridHg.htm
       this.navigate('', InternalGridSearchModuleRoutes.ICABSSESERVICEPLANNINGDETAILGRIDHG, { PlanVisitRowID: this.getControlValue('PlanVisitRowID'), PlanVisitNumber: this.getControlValue('PlanVisitNumber') });
    }

    /**
     * This method calls  change of defaultStartTime
     * @param void
     * @return void
     */
    public defaultStartTimeOnChange(): void {
        for (let i = 1; i <= this.vSCRowCount; i++) {
            if (this.getControlValue('Technician' + i)) {
                (this.getControlValue('DefaultStartTime').toString().length === 4) ? this.setControlValue('StartTime' + i, this.globalize.parseTimeToFixedFormat(this.getControlValue('DefaultStartTime'))) : this.setControlValue('StartTime' + i, this.getControlValue('DefaultStartTime'));
            }
        }
    }

    /**
     * This method is being called after clicking unPlanAll
     * @param void
     * @return void
     */
    public unPlanAllOnClick(): void {
        let msgObj: any = new ICabsModalVO(MessageConstant.PageSpecificMessage.unplanMsg, null, this.confirmUnplanOk.bind(this));
        msgObj.title = MessageConstant.PageSpecificMessage.unplanTitle;
        this.modalAdvService.emitPrompt(msgObj);
    }

    /**
     * This method is being called to save comment on blur
     * @param void
     * @return void
     */
    public commentsOndeactivate(): void {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject(), vPlanVisitRowID: any;
        vPlanVisitRowID = this.getControlValue('PlanVisitRowID');
        postData[this.serviceConstants.Function] = 'SaveComments';
        postData['Comments'] = this.getControlValue('Comments');
        postData['PlanVisitRowID'] = vPlanVisitRowID;
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    this.uiForm.controls['Comments'].markAsPristine();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is being called after Comments button click
     * @param void
     * @return void
     */
    public btnCommentsOnClick(): void {
        if (this.hideObj['divComments'] === true) {
            this.btnText['showBtn'] = 'Hide Comments';
            this.hideObj['divComments'] = false;
        } else {
            this.btnText['showBtn'] = 'Show Comments';
            this.hideObj['divComments'] = true;
        }
    }

    /**
     * This method is being used to get employee data from ellipsis
     * @param void
     * @return void
     */
    public onEmployeeDataReceived(data: any, index: any): void {
        this.setControlValue('Technician' + index, data.EmployeeCode);
        this.setControlValue('TechnicianDesc' + index, data.EmployeeSurname);
        this.uiForm.controls['Technician' + index].markAsDirty();
        this.technicianOndeactivate(index);
    }

    /**
     * This method is being used to update all employees
     * @param void
     * @return void
     */
    public updateAllEmployees(): void {
        for (let i = 1; i <= this.vSCRowCount; i++) {
            if (this.getControlValue('Technician' + i)) {
                this.updateEmployees(this.getControlValue('Technician' + i), i, 2);
            }
        }
    }

    /**
     * This method is being used to get last week number
     * @param void
     * @return void
     */
    public getLatestWeekNumber(): void {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject(), vPlanVisitRowID: any;
        vPlanVisitRowID = this.getControlValue('PlanVisitRowID');
        postData[this.serviceConstants.ActionType] = 'GetLatestWeekNumber';
        postData['BranchNumber'] = this.utils.getBranchCode();
        postData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        postData['StartDate'] = this.getControlValue('StartDate');
        postData['EndDate'] = this.getControlValue('EndDate');
        postData['GetWarnMessage'] = this.getControlValue('GetWarnMessage');
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    this.setControlValue('WeekNumber', data.WeekNumber);
                    if (data.WarningMessageDesc) {
                        this.modalAdvService.emitMessage(new ICabsModalVO(data.WarningMessageDesc));
                    }
                    this.setControlValue('WarningMessageDesc', '');
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is being used to navigate from infi image click
     * @param void
     * @return void
     */
    public infoImageClick(index: any): void {
        this.setControlValue('DiaryEmployeeCode', this.getControlValue('Technician' + index));
        this.setControlValue('DiaryEmployeeSurname', this.getControlValue('TechnicianDesc' + index));
        this.setControlValue('DiaryDate', this.getControlValue('ClashDiaryDate' + index));
        if (this.getControlValue('DiaryDate')) {
            this.navigate('ServicePlanningEmployee', AppModuleRoutes.PROSPECTTOCONTRACT + ProspectToContractModuleRoutes.ICABSCMDIARYDAYMAINTENANCE);
        }
    }

    /**
     * This method is being used to update duration
     * @param void
     * @return void
     */
    public durationOnChange(): void {
        this.updateDurations();
        this.vRefreshDuration = false;
    }

    /**
     * This method is being used to get employee descriptions
     * @param index sequence number
     * @return void
     */
    public employeeDescLookup(index: any): void {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject(), vPlanVisitRowID: any;
        vPlanVisitRowID = this.getControlValue('PlanVisitRowID');
        postData[this.serviceConstants.Function] = 'GetEmployeeDesc';
        postData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    try {
                        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                        this.setControlValue('TechnicianDesc' + index, this.getControlValue('EmployeeSurname'));
                        this.updateEmployees(this.getControlValue('EmployeeCode'), index, 1);
                    } catch (error) {
                        this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
                    }
                    this.setControlValue('WarningMessageDesc', '');
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is being used to handle blur of Technician control
     * @param index sequence number
     * @return void
     */
    public technicianOndeactivate(index: any): void {
        this.setControlValue('EmployeeCode', this.getControlValue('Technician' + index));
        if (this.getControlValue('Technician' + index)) {
            this.setRequiredStatus('Duration' + index, true);
            this.isDurationRequired[index - 1] = true;
            this.employeeDescLookup(index);
            this.defaultEmpDefaults(index);
        } else {
            this.updateDuration(index);
        }
    }

    /**
     * This method is being used to update Duration
     * @param index sequence number
     * @return void
     */
    public updateDuration(index: any): void {
        let vEmployeeCount: number = 0, vInfoImage: any;
        for (let i = index; i <= this.vSCRowCount; i++) {
            if (i === this.vSCRowCount) {
                this.setControlValue('Technician' + i, '');
                this.setRequiredStatus('Duration' + i, false);
                this.isDurationRequired[i - 1] = false;
                this.el.nativeElement.querySelector('#TechnicianTD' + i).classList.add('vnormalbackcolour');
                this.setControlValue('TechnicianDesc' + i, '');
                this.setControlValue('SelectDay' + i, (this.globalize.parseDateStringToDate(this.getControlValue('StartDate')) as Date).getDay() + 1);
                this.selectDayDropdown.toArray()[i - 1].selectedItem = this.getControlValue('SelectDay' + i);
                this.el.nativeElement.querySelector('#SelectDayTD' + i).classList.add('vnormalbackcolour');
                this.setControlValue('Booked' + i, '');
                this.el.nativeElement.querySelector('#BookedTD' + i).classList.add('vnormalbackcolour');
                this.setControlValue('StartTime' + i, '');
                this.el.nativeElement.querySelector('#StartTimeTD' + i).classList.add('vnormalbackcolour');
                this.setControlValue('Duration' + i, '');
                this.el.nativeElement.querySelector('#DurationTD' + i).classList.add('vnormalbackcolour');
                this.el.nativeElement.querySelector('#InfoImage' + i).style.display = 'none';
                this.setControlValue('ClashDiaryDate' + i, '');
            } else {
                this.setControlValue('Technician' + i, this.getControlValue('Technician' + (i + 1)));
                if (this.getControlValue('Technician' + i)) {
                    this.setRequiredStatus('Duration' + i, true);
                    this.isDurationRequired[i - 1] = true;
                } else {
                    this.setRequiredStatus('Duration' + i, false);
                    this.isDurationRequired[i - 1] = false;
                }
                this.addClassNextElement('TechnicianTD', i);
                this.setControlValue('TechnicianDesc' + i, this.getControlValue('TechnicianDesc' + (i + 1)));
                this.setControlValue('SelectDay' + i, this.getControlValue('SelectDay' + (i + 1)));
                this.selectDayDropdown.toArray()[i - 1].selectedItem = this.getControlValue('SelectDay' + i);
                this.addClassNextElement('SelectDayTD', i);
                this.setControlValue('StartTime' + i, this.getControlValue('StartTime' + (i + 1)));
                this.addClassNextElement('StartTimeTD', i);
                this.setControlValue('Duration' + i, this.getControlValue('Duration' + (i + 1)));
                this.addClassNextElement('DurationTD', i);
                this.setControlValue('Booked' + i, this.getControlValue('Booked' + (i + 1)));
                this.addClassNextElement('BookedTD', i);
                if (this.el.nativeElement.querySelector('#InfoImage' + (i + 1)).style.display === '') {
                    this.el.nativeElement.querySelector('#InfoImage' + (i + 1)).style.display = 'none';
                    this.el.nativeElement.querySelector('#InfoImage' + i).style.display = '';
                } else {
                    this.el.nativeElement.querySelector('#InfoImage' + i).style.display = 'none';
                }

                this.setControlValue('ClashDiaryDate' + i, this.getControlValue('ClashDiaryDate' + (i + 1)));
            }
        }
        for (let i = 1; i <= this.vSCRowCount; i++) {
            if (this.getControlValue('Technician' + i)) {
                vEmployeeCount++;
            }
        }
        if (vEmployeeCount === 0) {
            this.vRefreshDuration = '';
        }
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject(), vPlanVisitRowID: any;
        vPlanVisitRowID = this.getControlValue('PlanVisitRowID');
        postData[this.serviceConstants.Function] = 'DeleteEmployees';
        postData['CountEmployees'] = vEmployeeCount;
        postData['PlannedVisitDuration'] = this.getControlValue('PlannedVisitDuration');
        postData['OriginalVisitDuration'] = this.getControlValue('OriginalVisitDuration');
        if (!this.isVbEnableBSE) {
            for (let i = 1; i <= this.vSCRowCount; i++) {
                if (this.getControlValue('SelectDay' + i) === '') {
                    this.setControlValue('SelectDay' + i, this.defaultSelectDropdown.selectedItem);
                    this.selectDayDropdown.toArray()[i - 1].selectedItem = this.defaultSelectDropdown.selectedItem;
                }
            }
        }
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    let vbTimeSec: number = 0, controlValue: any, vbDurationHours: any, vbDurationMinutes: any, vEmployeeDurationDurationCount: number = 0, vbOriginalVisitDuration: number = 0;
                    for (let i = 1; i <= vEmployeeCount; i++) {
                        controlValue = parseInt(this.getControlValue('Duration' + i), 10);
                        if (controlValue) {
                            vEmployeeDurationDurationCount += controlValue;
                        }
                    }
                    this.setControlValue('PlannedVisitDuration', this.utils.secondsToHms(vEmployeeDurationDurationCount));
                    controlValue = this.getControlValue('OriginalVisitDuration');
                    vbDurationHours = parseInt(controlValue.substr(0, controlValue.length - 3), 10);
                    vbDurationMinutes = parseInt(controlValue.substr(-2, 2), 10);
                    if (vbDurationHours > 0) {
                        vbOriginalVisitDuration += (vbDurationHours * 60 * 60);
                    }
                    if (vbDurationMinutes > 0) {
                        vbOriginalVisitDuration += (vbDurationMinutes * 60);
                    }
                    if (vEmployeeDurationDurationCount === vbOriginalVisitDuration && this.vRefreshDuration) {
                        for (let i = 1; i <= vEmployeeCount; i++) {
                            this.setControlValue('Duration' + i, data['Duration' + i]);
                        }
                    }
                }

            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is called after clicking confirm appointment
     * @param void
     * @return void
     */
    public btnConfirmAppOnClick(): void {
        if (this.parentMode === 'ServicePlan' || this.getControlValue('ServicePlanNumber') > 0) {
            this.parentMode = 'ServicePlan';

        } else {
            this.parentMode = 'ServicePlanning';
        }
        this.navigate(this.parentMode, InternalMaintenanceApplicationModuleRoutes.ICABSAVISITAPPOINTMENTMAINTENANCE, {
            PlanVisitRowID: this.getControlValue('PlanVisitRowID')
        });
    }

    /**
     * This method is called to refresh current appointment status
     * @param void
     * @return void
     */
    public refreshAppConfirmed(): void {
        let postData: Object = {}, search: QueryParams = this.getURLSearchParamObject(), vPlanVisitRowID: any;
        vPlanVisitRowID = this.getControlValue('PlanVisitRowID');
        postData[this.serviceConstants.Function] = 'RefreshAppConfirmed';
        postData['PlanVisitRowID'] = vPlanVisitRowID;
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    if (data.AppointmentConfirmed === 'yes') {
                        this.setAppointmentRequired('yes', 'co');
                    } else {
                        this.setAppointmentRequired('no', 'co');
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is called after clicking save button
     * @param void
     * @return void
     */
    public saveData(): void {
        this.riMaintenanceBeforeSaveAdd();
        if (this.uiForm.valid) {
            let msgObj: any = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveServiceCall.bind(this));
            this.modalAdvService.emitPrompt(msgObj);
        }
    }

    /**
     * This method is called after clicking cancel button
     * @param void
     * @return void
     */
    public cancelChange(): void {
        this.resetData();
    }
}
// Class END
