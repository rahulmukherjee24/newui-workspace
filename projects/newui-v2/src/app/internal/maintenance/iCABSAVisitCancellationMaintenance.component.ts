import { GcpService } from './../../../GCP/gcp-service';
import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { DropdownStaticComponent } from './../../../shared/components/dropdown-static/dropdownstatic';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { BranchServiceAreaSearchComponent } from '../../internal/search/iCABSBBranchServiceAreaSearch';

@Component({
    templateUrl: 'iCABSAVisitCancellationMaintenance.html'
})

export class VisitCancellationMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('ddReasonCode') ddReasonCode: DropdownStaticComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('promptModal') public promptModal;

    private vResetCancelledToInPlanning: boolean = false;
    private cancelRowId: string = '';
    private allReasonCodes: string = '';
    private allReasonDesc: string = '';
    private passwordCheck: string = '';
    private xhrParams: any = {
        module: 'planning',
        method: 'service-planning/grid',
        operation: 'Application/iCABSAVisitCancellationMaintenance'
    };
    public pageId: string = '';
    public controls: any = [
        { name: 'CanPlanVisits', type: MntConst.eTypeText },
        { name: 'SelReason', type: MntConst.eTypeText },
        { name: 'Password', type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeText },
        { name: 'ResetPlansToInPlanning', type: MntConst.eTypeText },
        { name: 'PlanRemoved', type: MntConst.eTypeText }
    ];
    public isVisible: any = {
        isReason: true,
        isPassword: true,
        isServiceArea: false,
        isResetCancelledPlans: false
    };
    public reasonCodeDropdown: any = [];
    public cancelVisitLabel: string = MessageConstant.PageSpecificMessage.cancelVisitLabel;
    public ellipsisConfig: any = {
        branchServiceArea: {
            isDisabled: false,
            isShowCloseButton: true,
            isShowHeader: true,
            parentMode: 'LookUp-Emp',
            isShowAddNew: false,
            component: BranchServiceAreaSearchComponent
        }
    };
    public promptCallback: any;
    public promptContent: any = '';
    public promptNoCallback: any;
    public promptTitle: any = MessageConstant.PageSpecificMessage.promptTitleOnNavFail;

    constructor(injector: Injector, private gcpService: GcpService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSAVISITCANCELLATIONMAINTENANCE;
        this.browserTitle = MessageConstant.PageSpecificMessage.browserTitle;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageTitle = 'Visit Cancellation Maintenance';
        this.triggerFetchSysChar();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private triggerFetchSysChar(): void {
        let sysCharNumber = this.sysCharConstants.SystemCharEnableResetCancelPlansToInPlanning;
        this.fetchSysChar(sysCharNumber).subscribe((data) => {
            if (data.records && data.records.length > 0) {
                this.vResetCancelledToInPlanning = data.records[0].Required;
            }
            this.windowOnLoad();
        });
    }

    private fetchSysChar(sysCharNumber: any): any {
        let querySysChar: QueryParams = this.getURLSearchParamObject();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.SystemCharNumber, sysCharNumber);
        return this.httpService.sysCharRequest(querySysChar);
    }

    private windowOnLoad(): void {
        this.cancelRowId = this.riExchange.getParentHTMLValue('CancelRowid') ? this.riExchange.getParentHTMLValue('CancelRowid') : '0';
        if (this.parentMode === 'Confirm') {
            this.browserTitle = this.pageTitle = MessageConstant.PageSpecificMessage.browserTitle_1;
            this.utils.setTitle(this.browserTitle);
            this.cancelVisitLabel = MessageConstant.PageSpecificMessage.cancelVisitLabel_1;
            this.isVisible.isReason = false;
            this.isVisible.isPassword = false;
            this.isVisible.isServiceArea = true;
            this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
            this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname'));
            this.disableControl('EmployeeSurname', true);
            if (this.vResetCancelledToInPlanning) {
                this.isVisible.isResetCancelledPlans = true;
            }
        }
        this.fetchReasonCodes();
    }

    private fetchReasonCodes(): void {
        let queryParams: QueryParams = this.getURLSearchParamObject();
        queryParams.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData['Function'] = 'LoadCancelPage';
        this.ajaxSubscription = this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, queryParams, formData).subscribe(
            (e) => {
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.allReasonCodes = e.ReasonCode;
                    this.allReasonDesc = e.ReasonDesc;
                    this.passwordCheck = e.Password;

                    if (this.cancelRowId !== '0') {
                        let vNumVisits: number = this.cancelRowId.split(';').length;
                        this.setControlValue('CanPlanVisits', (vNumVisits).toString());
                    } else {
                        this.setControlValue('CanPlanVisits', '0');
                    }
                    let vcReasonCodeNum: number = this.allReasonCodes.split(';').length;
                    let vcReasonCode: any = this.allReasonCodes.split(';');
                    let vcReasonDescs: any = this.allReasonDesc.split(';');
                    let count: number = 0;
                    while (count < vcReasonCodeNum) {
                        let data: any = {};
                        data = { text: vcReasonDescs[count], value: vcReasonCode[count] };
                        this.reasonCodeDropdown.push(data);
                        count++;
                    }
                    if (this.isVisible.isReason) {
                        this.ddReasonCode.updateSelectedItem();
                    }
                    this.disableControl('CanPlanVisits', true);
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onSave(): void {
        /*
            ITA-617 [Cancel plan (confirm plan screen - individual visits)] : check if syschar 4390 is set then make new  GCP else use old one
        */
        this.ajaxSource.next(this.ajaxconstant.START);
        this.fetchSysChar(4390)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response && response.records && response.records[0]['Required']) {
                    // use new GCP
                    const url = this.gcpService.getReserveStockUrl() + 'cancel/visit?'; // updating navision
                    let searchParams: QueryParams = this.getURLSearchParamObject();

                    searchParams.set('ROWIDList', this.riExchange.getParentHTMLValue('CancelRowid'));
                    searchParams.set('branchServiceAreaList', this.getControlValue('BranchServiceAreaCode'));

                    let form_data: any = {
                        'ROWIDList': this.riExchange.getParentHTMLValue('PlanVisitRowID'),
                        'branchServiceAreaList': this.getControlValue('BranchServiceAreaCode'),
                        'businessCode': this.utils.getBusinessCode() || '',
                        'countryCode': this.utils.getCountryCode() || ''
                    };
                    if (this.parentMode === 'Confirm') {  //ITA-759
                        searchParams.set('function', 'SaveRemovePage');
                        form_data['Function'] = 'SaveRemovePage';
                    } else {
                        searchParams.set('function', 'SaveCancelPage');
                        form_data['Function'] = 'SaveCancelPage';
                    }
                    this.httpService.postData(searchParams, url, form_data).subscribe((e) => {
                        if (e.hasError) {
                            this.showDialog(this.onSave);

                        } else {
                            this.setControlValue('PlanRemoved', e.PlanRemoved ? e.PlanRemoved : 'Removed Successfully');
                            if (e.PlanRemoved !== 'Complete') {
                                this.modalAdvService.emitMessage(new ICabsModalVO(e.PlanRemoved));
                            } else {
                                let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.PlanVisitsRemoved);
                                modalVO.closeCallback = this.callParentPage.bind(this);
                                this.modalAdvService.emitMessage(modalVO);
                                this.formPristine();
                            }
                        }
                    },
                        (error) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            this.showDialog(this.onSave);
                        });

                } else {
                    // old code
                    if (this.parentMode !== 'Confirm') {
                        let controlPassword: string = this.getControlValue('Password');
                        if (this.passwordCheck !== controlPassword && this.passwordCheck !== controlPassword.toLowerCase() && this.passwordCheck !== controlPassword.toUpperCase()) {
                            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.IncorrectPassword));
                        } else {
                            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.promptConfirmSave.bind(this)));
                        }
                    } else {
                        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.promptConfirmSave.bind(this)));
                    }
                }
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                }
            );

    }

    public promptConfirmSave(): void {
        if (this.parentMode === 'Confirm') {
            this.onBranchServiceAreaChange();
            if (!this.getControlValue('BranchServiceAreaCode')) {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.ServiceAreaSelected));
            } else {
                let queryParams: QueryParams = this.getURLSearchParamObject();
                queryParams.set(this.serviceConstants.Action, '6');
                let formData: any = {};
                formData['Function'] = 'SaveRemovePage';
                formData['Rowid'] = this.cancelRowId;
                formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
                formData['ResetPlansToInPlanning'] = this.getControlValue('ResetPlansToInPlanning');

                this.ajaxSubscription = this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, queryParams, formData).subscribe(
                    (e) => {
                        if (e.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        } else {
                            this.setControlValue('PlanRemoved', e.PlanRemoved);
                            if (e.PlanRemoved !== 'Complete') {
                                this.modalAdvService.emitMessage(new ICabsModalVO(e.PlanRemoved));
                            } else {
                                let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.PlanVisitsRemoved);
                                modalVO.closeCallback = this.callParentPage.bind(this);
                                this.modalAdvService.emitMessage(modalVO);
                                this.formPristine();
                            }
                        }
                    },
                    (error) => {
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
            }
        } else {

            if (this.ddReasonCode.selectedItem === 'None') {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.ReasonSelected));
            } else {
                let params: QueryParams = this.getURLSearchParamObject();
                params.set(this.serviceConstants.Action, '6');
                let formValue: any = {};
                formValue['Function'] = 'SaveCancelPage';
                formValue['Rowid'] = this.cancelRowId;
                formValue['Reason'] = this.ddReasonCode.selectedItem;

                this.ajaxSubscription = this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, params, formValue).subscribe(
                    (e) => {
                        if (e.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        } else {
                            this.setControlValue('PlanRemoved', e.PlanRemoved);
                            let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.PlanVisitsCancelled);
                            modalVO.closeCallback = this.callParentPage.bind(this);
                            this.modalAdvService.emitMessage(modalVO);
                            this.formPristine();
                        }
                    },
                    (error) => {
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
            }
        }
    }

    public callParentPage(): void {
        this.location.back();
    }

    public onBranchServiceAreaChange(): void {
        let queryParams: QueryParams = this.getURLSearchParamObject();
        queryParams.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData['Function'] = 'GetBranchServiceArea';
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        this.ajaxSubscription = this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, queryParams, formData).subscribe(
            (e) => {
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.setControlValue('EmployeeSurname', e.EmployeeSurname);
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onServiceAreaEllipsisDataReceived(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
            this.uiForm.markAsDirty();
        }
    }

    public onReasonCodeChange(): void {
        this.uiForm.markAsDirty();
    }

    public showDialog(fncallback: any, fnnocallback?: any): void {
        this.promptCallback = fncallback;
        this.promptNoCallback = fnnocallback;
            this.promptContent = MessageConstant.PageSpecificMessage.navFailMessage;
            setTimeout(() => { this.promptModal.show(); }, 200);
    }

    public promptYes(): void {
        if (this.promptCallback && typeof this.promptCallback === 'function') {
            this.promptModal.hide();
            this.promptCallback.call(this);
            this.promptNoCallback = null;
        }
    }

}
