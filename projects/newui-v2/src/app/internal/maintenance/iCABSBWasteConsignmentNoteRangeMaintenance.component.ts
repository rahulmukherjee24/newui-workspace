import { Component, Injector, Inject, OnInit, ViewChild, Renderer } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Observable } from 'rxjs/Rx';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { Validators } from '@angular/forms';

import { BaseComponent } from '@base/BaseComponent';
import { ErrorConstant } from '@shared/constants/error.constant';
import { GlobalConstant } from '@shared/constants/global.constant';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import {
    IGenericEllipsisControl,
    EllipsisGenericComponent
} from '@shared/components/ellipsis-generic/ellipsis-generic';
import { IXHRParams } from '@app/base/XhrParams';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { VariableService } from '@shared/services/variable.service';

@Component({
    templateUrl: 'iCABSBWasteConsignmentNoteRangeMaintenance.html'
})

export class WasteConsignmentNoteRangeMaintenanceComponent extends BaseComponent implements OnInit {
    @ViewChild('WasteTransferTypeSearchEllipsis') WasteTransferTypeSearchEllipsis: EllipsisGenericComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    private requestParams: Object = {
        operation: 'Business/iCABSBWasteConsignmentNoteRangeMaintenance',
        module: 'waste',
        method: 'service-delivery/admin'
    };
    public WasteTransferTypeSearchhttpConfig: IXHRParams = {
        operation: 'Business/iCABSBWasteTransferTypeSearch',
        module: 'waste',
        method: 'service-delivery/search'
    };
    private queryParams: Object;
    private search: QueryParams = new QueryParams();
    private queryLookUp: QueryParams = new QueryParams();
    private querySysChar: QueryParams = new QueryParams();

    public pageId: string = '';
    public controls = [
        { name: 'Add', readonly: false, exclude: true, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'BranchName', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'BranchNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'Cancel', readonly: false, exclude: true, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'ConsignmentNoteRangeTypeDesc', readonly: false, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'Delete', readonly: false, exclude: true, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'RangeActive', readonly: false, disabled: false, required: false },
        { name: 'RangeEnd', readonly: false, disabled: false, required: true, type: MntConst.eTypeInteger },
        { name: 'RangeExpiryDate', readonly: false, disabled: false, required: false, type: MntConst.eTypeDate },
        { name: 'RangeNextNumber', readonly: false, disabled: false, required: true, type: MntConst.eTypeInteger },
        { name: 'RangeNumber', readonly: false, disabled: false, required: false },
        { name: 'RangePrefix', readonly: false, disabled: false, required: false, type: MntConst.eTypeTextFree },
        { name: 'RangeStart', readonly: false, disabled: false, required: true, type: MntConst.eTypeInteger },
        { name: 'RegulatoryAuthorityName', readonly: false, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'RegulatoryAuthorityNumber', readonly: false, disabled: false, required: true, type: MntConst.eTypeInteger },
        { name: 'Save', readonly: false, exclude: true, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'WasteConsignmentNoteRangeType', readonly: false, disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'WasteTransferTypeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'WasteTransferTypeDesc', readonly: false, disabled: true, required: false, type: MntConst.eTypeText }
    ];

    public pageParams: Object = {
        fieldVisibility: {
            trMiniumumLevyValue: false
        },
        fieldRequired: {
            RegulatoryAuthorityNumber: true,
            WasteConsignmentNoteRangeType: true,
            RangeStart: true,
            RangeEnd: true,
            RangeNextNumber: true
        },
        functionAdd: true
    };
    public modalConfig: Object = {
        backdrop: 'static',
        keyboard: true
    };
    public ellipsis: Object = {
        regulatoryAuthorityNumber: {
            autoOpen: false,
            ellipsisTitle: 'Regulatory Authority Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp',
                extraParams: {
                    'search.sortby': 'SeasonalTemplateNumber',
                    'BranchNumber': this.utils.getBranchCode()
                }
            },
            httpConfig: {
                operation: 'Business/iCABSBRegulatoryAuthoritySearch',
                module: 'waste',
                method: 'service-delivery/search'
            },
            tableColumn: [
                { title: 'Regulatory Authority Number', name: 'RegulatoryAuthorityNumber' },
                { title: 'Name', name: 'RegulatoryAuthorityName' },
                { title: 'Max Weight Per Day (KG)', name: 'MaximumWeightPerDay' },
                { title: 'Requires Premises Registration', name: 'RequiresPremiseRegistrationInd' },
                { title: 'Requires Service Cover Waste', name: 'RequiresServiceCoverWasteInd' },
                { title: 'Requires Waste Transfer Notes', name: 'RequiresWasteTransferNotesInd' },
                { title: 'Waste Regulatory Carrier Name', name: 'WasteRegulatoryCarrierName' },
                { title: 'Waste Regulatory Registration Number', name: 'WasteRegulatoryRegistrationNum' }
            ],
            disable: false
        },
        inputParamsWasteConsignmentNoteRangeSearchConfig: {
            autoOpen: false,
            ellipsisTitle: 'Waste Consignment Note Range Type Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp-WCNoteRangeType',
                extraParams: {
                    'search.sortby': 'WasteConsignmentNoteRangeType DESC',
                    'jsonSortField': 'WasteConsignmentNoteRangeType',
                    'RegulatoryAuthorityNumber': 0
                }
            },
            httpConfig: {
                operation: 'Business/iCABSBWasteConsignmentNoteRangeTypeSearch',
                module: 'waste',
                method: 'service-delivery/search'
            },
            tableColumn: [
                { title: 'Regulatory Authority Number', name: 'RegulatoryAuthorityNumber', type: MntConst.eTypeInteger },
                { title: 'Waste Transfer Type Code', name: 'WasteConsignmentNoteRangeType', type: MntConst.eTypeCode },
                { title: 'Description', name: 'ConsignmentNoteRangeTypeDesc', type: MntConst.eTypeText },
                { title: 'Description', name: 'RegulatoryAuthorityName', type: MntConst.eTypeText }
            ],
            disable: false
        }
    };
    public WasteTransferTypeEllipsisConfig: IGenericEllipsisControl = {
        autoOpen: false,
        ellipsisTitle: 'Waste Transfer Type Search',
        configParams: {
            table: '',
            shouldShowAdd: false,
            parentMode: 'LookUp'
        },
        tableColumn: [
            { title: 'Waste Transfer Type Code', name: 'WasteTransferTypeCode', type: MntConst.eTypeCode },
            { title: 'Description', name: 'WasteTransferTypeDesc', type: MntConst.eTypeText },
            { title: 'Waste Consignment Note Required', name: 'WasteConsignmentNoteRequiredIn', type: MntConst.eTypeCheckBox }
        ],
        disable: false
    };
    public dropdown: any = {
        branchSearch: {
            inputParams: {
                parentMode: 'LookUp'
            },
            active: {
                id: '',
                text: ''
            },
            isDisabled: false,
            isRequired: false,
            isTriggerValidate: false,
            isFirstItemSelected: true
        }
    };

    constructor(injector: Injector, private renderer: Renderer, @Inject(DOCUMENT) private document: any, private variableService: VariableService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBWASTECONSIGNMENTNOTERANGEMAINTENANCE;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.parentMode = this.riExchange.getParentMode();
        this.utils.setTitle('Waste Consignment Note Range Maintenance');
        this.fetchTranslationContent();
        this.postInit();
    }
    /**
     * This function is called when initialisation completes, it loads default data and process modes
     */
    private postInit(): void {
        if (this.parentMode === 'WCNoteRangeAdd') {
            this.formMode = this.c_s_MODE_ADD;
            this.processMode();
            this.uiForm.controls['RangeNumber'].disable();
            this.setControlValue('RangeActive', true);
        } else if (this.parentMode === 'WCNoteRangeUpdate') {
            this.pageParams['RowID'] = this.riExchange.getParentHTMLValue('RowID') || this.riExchange.getParentHTMLValue('ROWID');
            this.pageParams['functionAdd'] = false;
            this.triggerUpdateMode();
        }
    }
    // This function triggers update mode
    private triggerUpdateMode(): void {
        this.formMode = this.c_s_MODE_UPDATE;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.uiForm.controls['RangeNumber'].enable();
        this.ajaxGet('', { ROWID: this.pageParams['RowID'], action: '0' }).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.processMode();
            if (!data['hasError']) {
                this.formPristine();
                this.populateFormAndParams(data);
                this.populateDropdown(data);
                this.populateLookUpData();
                this.riMaintenanceBeforeUpdate();
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }
    // This function brings focus to branch field
    private riMaintenanceBeforeUpdate(): void {
        setTimeout(() => {
            let focus = new CustomEvent('focus', { bubbles: true });
            this.renderer.invokeElementMethod(this.document.querySelector('#branchSearch .ui-select-toggle'), 'focus', [focus]);
        }, 0);
    }

    /**
     * This function triggers when mode is changed
     */
    private processMode(): void {
        switch (this.formMode) {
            case this.c_s_MODE_ADD:
                this.formReset();
                this.uiForm.controls['RegulatoryAuthorityNumber'].enable();
                this.uiForm.controls['WasteConsignmentNoteRangeType'].enable();
                this.ellipsis['regulatoryAuthorityNumber'].disable = false;
                this.ellipsis['inputParamsWasteConsignmentNoteRangeSearchConfig'].disable = false;
                break;

            case this.c_s_MODE_UPDATE:
                this.formReset();
                this.uiForm.controls['RegulatoryAuthorityNumber'].disable();
                this.uiForm.controls['WasteConsignmentNoteRangeType'].disable();
                this.ellipsis['regulatoryAuthorityNumber'].disable = true;
                this.ellipsis['inputParamsWasteConsignmentNoteRangeSearchConfig'].disable = true;
                break;
        }
    }

    /**
     * This function resets the form to empty
     */
    private formReset(): void {
        this.uiForm.reset();
        this.formPristine();
        this.setTranslatedValues();
        this.dropdown['branchSearch'].active = {
            id: '',
            text: ''
        };
    }

    /**
     * This function toggles disable/enable attribute of action buttons
     */
    private toggleeActionBtn(isDisable: boolean): void {
        if (isDisable) {
            this.uiForm.controls['Save'].disable();
            this.uiForm.controls['Cancel'].disable();
            this.uiForm.controls['Add'].disable();
            this.uiForm.controls['Delete'].disable();
        } else {
            this.uiForm.controls['Save'].enable();
            this.uiForm.controls['Cancel'].enable();
            this.uiForm.controls['Add'].enable();
            this.uiForm.controls['Delete'].enable();
        }
    }

    /**
     * This function sets the translated values of the action buttons
     */
    private setTranslatedValues(): void {
        this.setControlValue('Save', this.pageParams['Save']);
        this.setControlValue('Add', this.pageParams['Add']);
        this.setControlValue('Delete', this.pageParams['Delete']);
        this.setControlValue('Cancel', this.pageParams['Cancel']);
    }

    /**
     * This function populates form data and page params with data recieved from connector
     * @param data
     */
    private populateFormAndParams(data: Object): void {
        if (data && !data['hasError']) {
            for (let i in data) {
                if (data.hasOwnProperty(i)) {
                    data[i] = !this.utils.isFalsy(data[i]) ? data[i].toString().trim() : '';
                    if (this.uiForm.controls[i] !== undefined && this.uiForm.controls[i] !== null) {
                        this.setControlValue(i, data[i].trim());
                        if (data[i].toUpperCase() === GlobalConstant.Configuration.Yes || data[i].toUpperCase() === GlobalConstant.Configuration.True) {
                            this.setControlValue(i, true);
                        } else if (data[i].toUpperCase() === GlobalConstant.Configuration.No || data[i].toUpperCase() === GlobalConstant.Configuration.False) {
                            this.setControlValue(i, false);
                        }
                    }
                    if (typeof this.pageParams[i] !== 'undefined') {
                        this.pageParams[i] = data[i];
                    }
                }
            }
        }
    }
    // This function fetches descriptions via lookup
    private populateLookUpData(): void {
        let data = [{
            table: 'WasteTransferType',
            query: { 'WasteTransferTypeCode': this.getControlValue('WasteTransferTypeCode'), 'BusinessCode': this.utils.getBusinessCode() },
            fields: ['WasteTransferTypeDesc']
        },
        {
            table: 'WasteConsignmentNoteRangeType',
            query: { 'RegulatoryAuthorityNumber': this.getControlValue('RegulatoryAuthorityNumber'), 'WasteConsignmentNoteRangeType': this.getControlValue('WasteConsignmentNoteRangeType'), 'BusinessCode': this.utils.getBusinessCode() },
            fields: ['ConsignmentNoteRangeTypeDesc']
        },
        {
            table: 'RegulatoryAuthority',
            query: { 'RegulatoryAuthorityNumber': this.getControlValue('RegulatoryAuthorityNumber'), 'BusinessCode': this.utils.getBusinessCode() },
            fields: ['RegulatoryAuthorityName']
        }];
        this.lookUpRecord(data, 5).subscribe(
            (e) => {
                if (e['results'] && e['results'].length > 0) {
                    if (e['results'][0].length > 0) {
                        this.setControlValue('WasteTransferTypeDesc', e['results'][0][0]['WasteTransferTypeDesc']);
                    } else {
                        this.setControlValue('WasteTransferTypeDesc', '');
                    }
                    if (e['results'][1].length > 0) {
                        this.setControlValue('ConsignmentNoteRangeTypeDesc', e['results'][1][0]['ConsignmentNoteRangeTypeDesc']);
                    } else {
                        this.setControlValue('ConsignmentNoteRangeTypeDesc', '');
                    }
                    if (e['results'][2].length > 0) {
                        this.setControlValue('RegulatoryAuthorityName', e['results'][2][0]['RegulatoryAuthorityName']);
                    } else {
                        this.setControlValue('RegulatoryAuthorityName', '');
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
            }
        );
    }

    // This function populates the branch dropdown with branch description
    private populateDropdown(data: Object): void {
        if (this.getControlValue('BranchNumber')) {
            let data = [{
                table: 'Branch',
                query: { 'BranchNumber': this.getControlValue('BranchNumber'), 'BusinessCode': this.utils.getBusinessCode() },
                fields: ['BranchName']
            }];
            this.lookUpRecord(data, 5).subscribe(
                (e) => {
                    if (e['results'] && e['results'].length > 0) {
                        if (e['results'][0].length > 0) {
                            this.setControlValue('BranchName', e['results'][0][0]['BranchName']);
                            this.dropdown['branchSearch'].active = {
                                id: this.getControlValue('BranchNumber'),
                                text: this.getControlValue('BranchNumber') + ' - ' + this.getControlValue('BranchName')
                            };
                        } else {
                            this.dropdown['branchSearch'].active = {
                                id: '',
                                text: ''
                            };
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
                }
            );
        } else {
            this.setControlValue('BranchName', '');
            this.dropdown['branchSearch'].active = {
                id: '',
                text: ''
            };
        }
    }

    /**
     * This is an API function for GET requests triggered in the screen
     * @param functionName
     * @param params
     */
    private ajaxGet(functionName: string, params: Object): Observable<any> {
        let requestParams = new QueryParams();
        requestParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        requestParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (functionName !== '') {
            requestParams.set(this.serviceConstants.Action, '6');
            requestParams.set('Function', functionName);
        }
        for (let key in params) {
            if (key) {
                requestParams.set(key, params[key]);
            }
        }
        return this.httpService.makeGetRequest(this.requestParams['method'], this.requestParams['module'], this.requestParams['operation'], requestParams);
    }
    /**
     * This is an API function for POST requests triggered in the screen
     * @param functionName
     * @param params
     * @param formData
     */
    private ajaxPost(functionName: string, params: Object, formData: Object): Observable<any> {
        let requestParams = new QueryParams();
        requestParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        requestParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (functionName !== '') {
            requestParams.set(this.serviceConstants.Action, '6');
            formData['Function'] = functionName;
        }
        for (let key in params) {
            if (key) {
                requestParams.set(key, params[key]);
            }
        }
        return this.httpService.makePostRequest(this.requestParams['method'], this.requestParams['module'], this.requestParams['operation'], requestParams, formData);
    }
    /**
     * This is an API function for Look-up requests triggered in the screen
     * @param data
     * @param maxresults
     */
    private lookUpRecord(data: Object, maxresults?: number): Observable<any> {
        this.queryLookUp.set(this.serviceConstants.Action, '0');
        this.queryLookUp.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        this.queryLookUp.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (maxresults) {
            this.queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        } else {
            this.queryLookUp.set(this.serviceConstants.MaxResults, '100');
        }
        return this.httpService.lookUpRequest(this.queryLookUp, data);
    }

    // This function will be called after delete operation is successful
    private afterDelete(): void {
        this.location.back();
    }

    /**
     * This is batch request to fetch translation
     */
    private fetchTranslationContent(): void {
        this.getTranslatedValuesBatch((data: any) => {
            if (data) {
                this.pageParams['Save'] = data[0];
                this.pageParams['Add'] = data[1];
                this.pageParams['Cancel'] = data[2];
                this.pageParams['Delete'] = data[3];
            }
        },
            ['Save'], ['Add'], ['Cancel'], ['Delete']);
    }

    /**
     * This function triggers before save operation
     */
    private beforeSave(): boolean {
        for (let i in this.uiForm.controls) {
            if (this.uiForm.controls.hasOwnProperty(i)) {
                this.uiForm.controls[i].markAsTouched();
                if (!this.uiForm.controls[i].enabled) {
                    this.uiForm.controls[i].clearValidators();
                }
                this.uiForm.controls[i].updateValueAndValidity();
            }
        }
        this.uiForm.updateValueAndValidity();
        return this.uiForm.valid;
    }

    /**
     * This function triggers after Save service returns
     */
    private afterSave(): void {
        if (this.formMode === this.c_s_MODE_ADD) {
            this.triggerUpdateMode();
        }
    }

    /**
     * This function triggers when add button is clicked
     */
    public addOnClick(): void {
        this.formMode = this.c_s_MODE_ADD;
        this.processMode();
    }
    /**
     * This function triggers when delete button is clicked
     */
    public deleteOnClick(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteOperation.bind(this)));
    }
    /**
     * This function has the core delete functionality
     */
    public deleteOperation(): void {
        let formdata: Object = {
            ROWID: this.pageParams['RowID']
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxPost('', { action: 3 }, formdata).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (!data['hasError']) {
                let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully);
                modalVO.closeCallback = this.afterDelete.bind(this);
                this.modalAdvService.emitMessage(modalVO);
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }
    /**
     * This function triggers when save button is clicked
     */
    public saveOnClick(): void {
        let valid: boolean = true;
        valid = this.beforeSave();
        if (valid) {
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveOperation.bind(this)));
        }
    }

    /**
     * This function is the save operation that gets triggerred when save is clicked
     */
    public saveOperation(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let formData: Object = {};
        let requestParam: Object = {};
        let controlsLength = this.controls.length;
        for (let i = 0; i < controlsLength; i++) {
            if (!this.controls[i]['exclude']) {
                formData[this.controls[i].name] = this.getControlValue(this.controls[i].name);
            }
        }
        if (!formData['WasteTransferTypeCode']) {
            formData['WasteTransferTypeCode'] = '?';
        }
        if (!formData['BranchNumber']) {
            formData['BranchNumber'] = '~~ignore~~';
        }
        if (this.formMode === this.c_s_MODE_ADD) {
            requestParam = {
                action: 1
            };
        } else {
            requestParam = {
                action: 2
            };
            formData['ROWID'] = this.pageParams['RowID'];
        }
        this.ajaxPost('', requestParam, formData).subscribe((data) => {
            if (data['status'] === GlobalConstant.Configuration.Failure) {
                this.modalAdvService.emitError(new ICabsModalVO(data['oResponse']));
            } else {
                if (data['hasError']) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    this.formPristine();
                    let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully);
                    modalVO.closeCallback = this.afterSave.bind(this);
                    this.modalAdvService.emitMessage(modalVO);
                    this.pageParams['RowID'] = data['ttWasteConsignmentNoteRange'];
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }

    /**
     * This function triggers when an option is selected in business dropdown
     */
    public cancelOnClick(): void {
        switch (this.formMode) {
            case this.c_s_MODE_ADD:
                this.variableService.setBackClick(true);
                this.location.back();
                break;

            case this.c_s_MODE_UPDATE:
                this.triggerUpdateMode();
                break;
        }
    }
    // This function gets triggered when RangeExpiryDate is changed is seletced
    public rangeExpiryDateOnChange(value: Object): void {
        if (value && value['value']) {
            this.setControlValue('RangeExpiryDate', value['value']);
        } else {
            this.setControlValue('RangeExpiryDate', '');
        }
        this.uiForm.controls['RangeExpiryDate'].markAsDirty();
    }

    // This function gets triggered when WasteTransferType is changed is seletced
    public wasteTransferTypeOnChange(): void {
        if (this.getControlValue('WasteTransferTypeCode')) {
            let data = [{
                table: 'WasteTransferType',
                query: { 'WasteTransferTypeCode': this.getControlValue('WasteTransferTypeCode'), 'BusinessCode': this.utils.getBusinessCode() },
                fields: ['WasteTransferTypeDesc']
            }];
            this.lookUpRecord(data, 5).subscribe(
                (e) => {
                    if (e['results'] && e['results'].length > 0) {
                        if (e['results'][0].length > 0) {
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'WasteTransferTypeCode', false);
                            this.uiForm.controls['WasteTransferTypeCode'].updateValueAndValidity();
                            this.setControlValue('WasteTransferTypeDesc', e['results'][0][0]['WasteTransferTypeDesc']);
                        } else {
                            this.uiForm.controls['WasteTransferTypeCode'].setErrors({});
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'WasteTransferTypeCode', true);
                            this.setControlValue('WasteTransferTypeDesc', '');
                        }
                    }
                },
                (error) => {
                    // error statement
                }
            );
        } else {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'WasteTransferTypeCode', false);
            this.uiForm.controls['WasteTransferTypeCode'].updateValueAndValidity();
            this.setControlValue('WasteTransferTypeDesc', '');
        }
    }

    // This function gets triggered when WasteConsignmentNote is changed is seletced
    public wasteConsignmentNoteOnChange(): void {
        if (this.getControlValue('WasteConsignmentNoteRangeType')) {
            let data = [{
                table: 'WasteConsignmentNoteRangeType',
                query: { 'RegulatoryAuthorityNumber': this.getControlValue('RegulatoryAuthorityNumber'), 'WasteConsignmentNoteRangeType': this.getControlValue('WasteConsignmentNoteRangeType'), 'BusinessCode': this.utils.getBusinessCode() },
                fields: ['ConsignmentNoteRangeTypeDesc']
            }];
            this.lookUpRecord(data, 5).subscribe(
                (e) => {
                    if (e['results'] && e['results'].length > 0) {
                        if (e['results'][0].length > 0) {
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'WasteConsignmentNoteRangeType', false);
                            this.uiForm.controls['WasteConsignmentNoteRangeType'].setValidators(Validators.required);
                            this.uiForm.controls['WasteConsignmentNoteRangeType'].updateValueAndValidity();
                            this.setControlValue('ConsignmentNoteRangeTypeDesc', e['results'][0][0]['ConsignmentNoteRangeTypeDesc']);
                        } else {
                            this.uiForm.controls['WasteConsignmentNoteRangeType'].setErrors({});
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'WasteConsignmentNoteRangeType', true);
                            this.setControlValue('ConsignmentNoteRangeTypeDesc', '');
                        }
                    }
                },
                (error) => {
                    // error statement
                }
            );
        } else {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'WasteConsignmentNoteRangeType', false);
            this.uiForm.controls['WasteConsignmentNoteRangeType'].setValidators(Validators.required);
            this.uiForm.controls['WasteConsignmentNoteRangeType'].updateValueAndValidity();
            this.setControlValue('ConsignmentNoteRangeTypeDesc', '');
        }
    }

    // This function gets triggered when RegulatoryAuthority is changed is seletced
    public regulatoryAuthorityOnChange(): void {
        if (this.getControlValue('RegulatoryAuthorityNumber')) {
            let data = [{
                table: 'RegulatoryAuthority',
                query: { 'RegulatoryAuthorityNumber': this.getControlValue('RegulatoryAuthorityNumber'), 'BusinessCode': this.utils.getBusinessCode() },
                fields: ['RegulatoryAuthorityName']
            }];
            this.lookUpRecord(data, 5).subscribe(
                (e) => {
                    if (e['results'] && e['results'].length > 0) {
                        if (e['results'][0].length > 0) {
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'RegulatoryAuthorityNumber', false);
                            this.uiForm.controls['RegulatoryAuthorityNumber'].setValidators(Validators.required);
                            this.uiForm.controls['RegulatoryAuthorityNumber'].updateValueAndValidity();
                            this.setControlValue('RegulatoryAuthorityName', e['results'][0][0]['RegulatoryAuthorityName']);
                        } else {
                            this.uiForm.controls['RegulatoryAuthorityNumber'].setErrors({});
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'RegulatoryAuthorityNumber', true);
                            this.setControlValue('RegulatoryAuthorityName', '');
                        }
                        this.wasteConsignmentNoteOnChange();
                        this.ellipsis['inputParamsWasteConsignmentNoteRangeSearchConfig'].configParams.extraParams.regulatoryAuthorityNumber = this.getControlValue('RegulatoryAuthorityNumber');
                    }
                },
                (error) => {
                    // error statement
                }
            );
        } else {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'RegulatoryAuthorityNumber', false);
            this.uiForm.controls['RegulatoryAuthorityNumber'].setValidators(Validators.required);
            this.uiForm.controls['RegulatoryAuthorityNumber'].updateValueAndValidity();
            this.setControlValue('RegulatoryAuthorityName', '');
            this.ellipsis['inputParamsWasteConsignmentNoteRangeSearchConfig'].configParams.extraParams.regulatoryAuthorityNumber = this.getControlValue('RegulatoryAuthorityNumber');
            if (this.getControlValue('WasteConsignmentNoteRangeType')) {
                this.setControlValue('ConsignmentNoteRangeTypeDesc', '');
                this.uiForm.controls['WasteConsignmentNoteRangeType'].setErrors({});
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'WasteConsignmentNoteRangeType', true);
            }
        }
    }
    // This function gets triggered when BranchNumber dropdown is seletced
    public branchOnDataRecieved(data: Object): void {
        if (data) {
            this.setControlValue('BranchNumber', data['BranchNumber']);
            this.setControlValue('BranchName', data['BranchName']);
        } else {
            this.setControlValue('BranchNumber', '');
            this.setControlValue('BranchName', '');
        }
        this.uiForm.controls['BranchNumber'].markAsDirty();
    }
    // This function gets triggered when WasteTransferType ellipsis is seletced
    public regulatoryAuthorityOnDataRecieved(data: Object): void {
        if (data) {
            this.setControlValue('RegulatoryAuthorityNumber', data['RegulatoryAuthorityNumber']);
            this.setControlValue('RegulatoryAuthorityName', data['RegulatoryAuthorityName']);
            this.ellipsis['inputParamsWasteConsignmentNoteRangeSearchConfig'].configParams.extraParams.regulatoryAuthorityNumber = data['RegulatoryAuthorityNumber'];
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'WasteTransferTypeCode', false);
            this.uiForm.controls['RegulatoryAuthorityNumber'].markAsDirty();
        }
    }

    // This function gets triggered when WasteTransferType ellipsis is seletced
    public wasteConsignmentNoteOnDataRecieved(data: Object): void {
        if (data) {
            this.setControlValue('WasteConsignmentNoteRangeType', data['WasteConsignmentNoteRangeType']);
            this.setControlValue('ConsignmentNoteRangeTypeDesc', data['ConsignmentNoteRangeTypeDesc']);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'WasteConsignmentNoteRangeType', false);
            this.uiForm.controls['WasteConsignmentNoteRangeType'].markAsDirty();
        }
    }

    // This function gets triggered when WasteTransferType ellipsis is seletced
    public wasteTransferTypeOnDataRecieved(data: Object): void {
        if (data) {
            this.setControlValue('WasteTransferTypeCode', data['WasteTransferTypeCode']);
            this.setControlValue('WasteTransferTypeDesc', data['WasteTransferTypeDesc']);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'WasteTransferTypeCode', false);
            this.uiForm.controls['WasteTransferTypeDesc'].markAsDirty();
        }
    }

    /**
     * This function triggers candeactive modal
     */
    public canDeactivate(): Observable<boolean> {
        this.routeAwayGlobals.setSaveEnabledFlag(this.uiForm.dirty);
        return this.routeAwayComponent.canDeactivate();
    }

}
