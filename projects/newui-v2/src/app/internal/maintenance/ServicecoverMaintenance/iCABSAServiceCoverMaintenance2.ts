import { Injector } from '@angular/core';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ServiceCoverMaintenanceComponent } from './iCABSAServiceCoverMaintenance.component';
import { MessageConstant } from '../../../../shared/constants/message.constant';

export class ServiceCoverMaintenance2 {

    private context: ServiceCoverMaintenanceComponent;

    constructor(private parent: ServiceCoverMaintenanceComponent, injector: Injector) {
        this.context = parent;
    }

    public riMaintenanceAfterEvent(): void {
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd
            || this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            this.context.setControlValue('chkStockOrder', false);
        }
    }

    public riMaintenanceBeforeUpdateMode(): void {
        this.context.iCABSAServiceCoverMaintenance4.ShowFields();
        //this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdValue');
        this.context.iCABSAServiceCoverMaintenance5.AutoRouteProductInd_onClick();
        if (this.context.isControlChecked('RequireAnnualTimeInd')) {
            this.context.pageParams.uiDisplay.tdAnnualTimeChange = true;
            this.context.pageParams.uiDisplay.tdAnnualTimeChangeLab = true;
            this.context.setControlValue('AnnualTimeChange', '');
        } else {
            this.context.pageParams.uiDisplay.tdAnnualTimeChange = false;
            this.context.pageParams.uiDisplay.tdAnnualTimeChangeLab = false;
        }
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            (this.context.isControlChecked('DisplayLevelInd'))) {
            this.context.setControlValue('origTotalValue', '');
            this.context.setControlValue('NewTotalValue', '');
            this.context.setControlValue('riGridHandle', '');
        }
        this.context.pageParams.SavedReductionQuantity = 0;
        this.context.pageParams.SavedIncreaseQuantity = 0;
        this.context.pageParams.blnQuantityChange = false;
        this.context.pageParams.blnValueChange = false;
        this.context.pageParams.blnServiceVisitAnnivDateChange = false;
        this.context.pageParams.uiDisplay.trInstallationValue = false;
        this.context.setControlValue('InstallationValue', 0);
        switch (this.context.pageParams.currentContractType) {
            case 'C':
                if (this.context.pageParams.blnValueRequired) {
                    if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                        (this.context.isControlChecked('DisplayLevelInd') ||
                            this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                        this.context.isControlChecked('VisitTriggered')) {
                        this.context.pageParams.uiDisplay.tdUnitValueChangeLab = true;
                        this.context.pageParams.uiDisplay.UnitValueChange = true;
                        this.context.riMaintenance.DisableInput('UnitValue');
                    }
                    this.context.pageParams.uiDisplay.tdAnnualValueChangeLab = true;
                    this.context.pageParams.uiDisplay.AnnualValueChange = true;
                    this.context.riMaintenance.DisableInput('ServiceAnnualValue');
                }
                this.context.setControlValue('InitialValue', '');
                break;
            case 'J':
                //Default LastChangeEffectDt;
                this.context.setControlValue('LastChangeEffectDate', this.context.getRawControlValue('ServiceCommenceDate'));
                break;
        }
        this.context.setControlValue('AnnualTimeChange', '');
        this.context.setControlValue('AnnualValueChange', 0);
        //this.context.riExchange.riInputElement.isCorrect('AnnualValueChange');
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            (this.context.isControlChecked('DisplayLevelInd') ||
                this.context.isControlChecked('InvoiceUnitValueRequired')) &&
            this.context.isControlChecked('VisitTriggered')) {
            this.context.setControlValue('UnitValueChange', 0);
            this.context.riExchange.riInputElement.isCorrect(this.context.uiForm, 'UnitValueChange');
        }
        //When coming from Client Retention, get LostBusinessRequestNumber from Parent (needed in trigger);
        if (this.context.parentMode === 'ContactUpdate') {
            this.context.setControlValue('LostBusinessRequestNumber', this.context.riExchange.getParentHTMLValue('LostBusinessRequestNumber'));
        }
        if (this.context.utils.getBranchCode().toString() !== this.context.getControlValueAsString('ServiceBranchNumber')) {
            if (this.context.utils.getBranchCode().toString() !== this.context.getControlValueAsString('NegBranchNumber')) {
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'ServiceSalesEmployee');
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SalesEmployeeText');
            }
            // this.context.riExchange.riInputElement.Disable( this.context.uiForm, 'BranchServiceAreaCode') - #52629;
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'BranchServiceAreaSeqNo');
        }
        this.context.riMaintenance.DisableInput('TrialPeriodInd');
        this.context.iCABSAServiceCoverMaintenance7.ToggleTrialPeriodStatus();
        if (this.context.pageParams.vbEnableInstallsRemovals === true) {
            this.context.riMaintenance.DisableInput('RemovalValue');
        }
        if (this.context.pageParams.currentContractType !== 'P') {
            this.context.iCABSAServiceCoverMaintenance7.DOWSentriconToggle();
        }
        if (!this.context.pageParams.hasUpdatedWindowType && (this.context.isReturning() || this.context.pageParams.hasFetchedWindowsType)) {
            return;
        }
        this.context.pageParams.hasUpdatedWindowType = false;
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.PostDataAddFunction('GetWindowsType');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.riMaintenance.PostDataAdd('ServiceCoverRowID', this.context.pageParams.ServiceCoverRowID, MntConst.eModeUpdate);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.pageParams.hasFetchedWindowsType = true;
            for (let iLoop = 1; iLoop <= 7; iLoop++) {
                this.context.setControlValue('selQuickWindowSet' + iLoop, data['WindowType0' + iLoop]);
                if (data['WindowType0' + iLoop] === 'C') {
                    this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'WindowStart' + this.context.ZeroPadInt(iLoop, 2));
                    this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'WindowEnd' + this.context.ZeroPadInt(iLoop, 2));
                    this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'WindowStart' + this.context.ZeroPadInt(iLoop + 7, 2));
                    this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'WindowEnd' + this.context.ZeroPadInt(iLoop + 7, 2));
                } else {
                    this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowStart' + this.context.ZeroPadInt(iLoop, 2));
                    this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowEnd' + this.context.ZeroPadInt(iLoop, 2));
                    this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowStart' + this.context.ZeroPadInt(iLoop + 7, 2));
                    this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowEnd' + this.context.ZeroPadInt(iLoop + 7, 2));
                }
            }
            this.context.iCABSAServiceCoverMaintenance2.WindowPreferredIndChanged();
            this.context.renderTab(1);
            //this.context.riExchange.riInputElement.FocusEx('LastChangeEffectDate', true);
            this.context.pageParams.dtLastChangeEffectDate.required = true;
            this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'LastChangeEffectDate', false);
            this.context.setRequiredStatus('LastChangeEffectDate',this.context.pageParams.dtLastChangeEffectDate.required);
            if (this.context.LastChangeEffectDatePicker) {
                this.context.LastChangeEffectDatePicker.validateDateField();
            }
            this.context.pageParams.initialForm = this.context.createControlObjectFromForm();
            this.context.storePageParams();
            this.context.getUserAuthorityData();
            this.context.statusChangeSubscription = this.context.uiForm.statusChanges.subscribe(data => {
                if (this.context.uiForm.dirty) {
                    for (let key in this.context.uiForm.controls) {
                        if (key && (key !== 'menu') && this.context.uiForm.controls.hasOwnProperty(key) &&
                            this.context.uiForm.controls[key].dirty) {
                            this.context.formIsDirty = true;
                            if (this.context.statusChangeSubscription) {
                                this.context.statusChangeSubscription.unsubscribe();
                            }
                        }
                    }
                }
            });
            this.context.utils.getFirstFocusableFieldForTab(1);
        }, 'POST', 6, false);
    }

    // BEFORE UPDT (Hide flds)
    public riMaintenanceBeforeUpdate(): void {
        if (this.context.getRawControlValue('DepositCanAmend') !== 'Y') {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'DepositDate');
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'DepositAmount');
        }
        if (this.context.pageParams.vbEnableDepositProcessing &&
            this.context.getRawControlValue('DepositExists') === 'Y') {
            this.context.pageParams.uiDisplay.trDepositLineAdd = true;
        }
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'DepositAmountApplied');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'DepositPostedDate');
        if (this.context.pageParams.currentContractType === 'J') {
            this.context.pageParams.uiDisplay.trEffectiveDate = false;
        } else {
            this.context.setControlValue('LastChangeEffectDate', '');
            this.context.setDateToFields('LastChangeEffectDate', '');
            this.context.pageParams.uiDisplay.trEffectiveDate = true;
            this.context.iCABSAServiceCoverMaintenance2.SelServiceBasis_OnChange();
            this.context.iCABSAServiceCoverMaintenance8.SelSubjectToUplift_onChange();
            this.context.iCABSAServiceCoverMaintenance7.SelUpliftVisitPosition_onChange();
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelServiceBasis');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'selHardSlotType');
            //this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'cmdHardSlotCalendar');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'cmdDiaryView');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelSubjectToUplift');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelUpliftVisitPosition');
            if (!this.context.getRawControlValue('SelAutoPattern')) {
                this.context.setControlValue('SelAutoPattern', 'D');
            }
            this.context.iCABSAServiceCoverMaintenance5.SelAutoPattern_Onchange();
        }
        this.context.iCABSAServiceCoverMaintenance7.HideQuickWindowSet(!this.context.pageParams.blnAccess);
        if (this.context.getRawControlValue('VisitCycleInWeeks') === '1') {
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'VisitsPerCycle');
        }
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelectCompositeProductcode');
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'selTaxCode');
        if (this.context.pageParams.vbDefaultTaxCodeProductExpenseReq === true) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
        }
        if (this.context.pageParams.vbDefaultTaxCodeOnServiceCoverMaintReq === true &&
            this.context.pageParams.vbDefaultTaxCodeOnServiceCoverMaintLog === true) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
        }
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'ServiceAnnualTime');
        this.context.pageParams.uiDisplay.trOutstandingRemovals = false;
        this.context.pageParams.uiDisplay.trRemovalEmployee = false;
        this.context.pageParams.uiDisplay.trOutstandingInstallations = false;
        this.context.pageParams.uiDisplay.trInstallationEmployee = false;
        this.context.setRequiredStatus('InstallationEmployeeCode', false);
        this.context.iCABSAServiceCoverMaintenance7.ToggleSeasonalDates();
        this.context.iCABSAServiceCoverMaintenance7.EnableSeasonalChanges(false);
        if (this.context.isControlChecked('SeasonalServiceInd')) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'ServiceVisitAnnivDate');
        }
        if (this.context.pageParams.vbEnableEntitlement && this.context.pageParams.currentContractType === 'C') {
            this.context.iCABSAServiceCoverMaintenance3.ToggleEntitlementRequired();
        }
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            this.context.isControlChecked('DisplayLevelInd')) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'ExpiryDate');
            if (this.context.riExchange.URLParameterContains('PendingReduction')) {
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    (this.context.isControlChecked('DisplayLevelInd') ||
                        this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                    this.context.isControlChecked('VisitTriggered')) {
                    this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'UnitValue');
                }
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'ServiceAnnualValue');
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'ServiceQuantity');
            }
        }
        if (!this.context.pageParams.vbEnableServiceCoverDepreciation) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'DepreciationPeriod');
        }
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'chkFOC');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'FOCInvoiceStartDate');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'FOCProposedAnnualValue');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'ServiceCommenceDate');
        this.context.renderTab(1);
        if (this.context.isControlChecked('CapableOfUplift')
            && this.context.getRawControlValue('SubjectToUplift')) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'UpliftTemplateNumber');
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelUpliftVisitPosition');
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelSubjectToUplift');
        }
        this.context.iCABSAServiceCoverMaintenance2.riMaintenanceBeforeUpdateMode();
    }

    public BranchServiceAreaSeqNoOnChange(): void {
        //if ( Seq No < 4 add leading 0s to disp as time format;
        //if ( Seq No > 4 add leading 0s to make the Seq No 6 long;
        if (!isNaN(parseInt(this.context.getRawControlValue('BranchServiceAreaSeqNo'), 10))) {
            this.context.setControlValue('BranchServiceAreaSeqNo',
                parseInt(this.context.getRawControlValue('BranchServiceAreaSeqNo'), 10));
            this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'BranchServiceAreaSeqNo', false);
            if (this.context.getRawControlValue('BranchServiceAreaSeqNo').length < 4) {
                this.context.setControlValue('BranchServiceAreaSeqNo',
                    this.context.utils.Format(this.context.getRawControlValue('BranchServiceAreaSeqNo'), '0000'));
            } else {
                if (this.context.getRawControlValue('BranchServiceAreaSeqNo').length > 4) {
                    this.context.setControlValue('BranchServiceAreaSeqNo',
                        this.context.utils.Format(this.context.getRawControlValue('BranchServiceAreaSeqNo'), '000000'));
                }
            }
        } else {
            this.context.setControlValue('BranchServiceAreaSeqNo', '');
        }
    }

    public CustomerAvailTemplateIDOnChange(): void {
        this.UpdateTemplate();
    }

    public UpdateTemplate(): void {
        if (this.context.getRawControlValue('CustomerAvailTemplateID') !== null) {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntryRequests.p';
            this.context.riMaintenance.PostDataAddFunction('CustomerAvailTemplate');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.PostDataAddFromField('CustomerAvailTemplateID', MntConst.eTypeInteger);
            for (let i: number = 1; i < 15; i++) {
                this.context.riMaintenance.ReturnDataAdd('WindowStart' + this.context.ZeroPadInt(i, 2), MntConst.eTypeTime);
                this.context.riMaintenance.ReturnDataAdd('WindowEnd' + this.context.ZeroPadInt(i, 2), MntConst.eTypeTime);
            }
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('CustomerAvailTemplateDesc', data['CustomerAvailTemplateDesc']);
                for (let idx: number = 1; idx < 15; idx++) {
                    let suffix: string = idx < 10 ? '0' + idx : idx.toString();
                    this.context.setControlValue('WindowStart' + suffix, data['WindowStart' + suffix]);
                    this.context.setControlValue('WindowEnd' + suffix, data['WindowEnd' + suffix]);
                    this.context.riExchange.riInputElement.ReadOnly(this.context.uiForm, 'WindowStart' + suffix, true);
                    this.context.riExchange.riInputElement.ReadOnly(this.context.uiForm, 'WindowEnd' + suffix, true);
                    if (idx < 7) {
                        this.context.setControlValue('selQuickWindowSet' + idx, 'C');
                    }
                }
            }, 'POST');
        }
    }

    public ServiceBasis_OnChange(): void {
        this.context.setControlValue('SelServiceBasis', this.context.getRawControlValue('ServiceBasis'));
    }

    public FollowTemplateIndOnClick(): void {
        this.context.setRequiredStatus('AnnualCalendarTemplateNumber', this.context.isControlChecked('FollowTemplateInd'));
    }

    public SelServiceBasis_OnChange(): void {
        if (this.context.getRawControlValue('SelServiceBasis') === 'T') {
            this.context.setControlValue('AnnualCalendarInd', true);
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd ||
                (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate &&
                    !this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'SelServiceBasis'))) {
                this.context.setControlValue('FollowTemplateInd', true);
                if (this.context.pageParams.vbEnableWeeklyVisitPattern) {
                    this.context.pageParams.blnUseVisitCycleValues = false;
                    this.context.iCABSAServiceCoverMaintenance4.ShowFields();
                }
            }
            if (this.context.isControlChecked('FollowTemplateInd')) {
                this.context.setRequiredStatus('AnnualCalendarTemplateNumber', true);
            }
            //Blnk Values;
            this.context.setControlValue('SeasonalServiceInd', false);
            this.context.pageParams.uiDisplay.trFollowTemplate = true;
            this.context.pageParams.uiDisplay.trAnnualCalendarTemplateFields = true;
            this.context.pageParams.uiDisplay.trHardSlotType = false;
            this.context.pageParams.uiDisplay.cmdHardSlotCalendar = false;
            this.context.setControlValue('HardSlotInd', false);
            this.context.setRequiredStatus('HardSlotType', false);
            this.context.iCABSAServiceCoverMaintenance7.SeasonalServiceInd_onclick();
        } else if (this.context.getRawControlValue('SelServiceBasis') === 'S') { //Seasonal Template
            this.context.setControlValue('SeasonalServiceInd', true);
            this.context.setControlValue('AnnualCalendarInd', false);
            this.context.setControlValue('FollowTemplateInd', false);
            this.context.setControlValue('HardSlotInd', false);
            this.context.setControlValue('AnnualCalendarTemplateNumber', '');
            this.context.setControlValue('CalendarTemplateName', '');
            this.context.setRequiredStatus('AnnualCalendarTemplateNumber', false);
            this.context.setRequiredStatus('HardSlotType', false);
            this.context.pageParams.uiDisplay.trFollowTemplate = false;
            this.context.pageParams.uiDisplay.trAnnualCalendarTemplateFields = false;
            this.context.pageParams.uiDisplay.trHardSlotType = false;
            this.context.pageParams.uiDisplay.cmdHardSlotCalendar = false;
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd ||
                (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate &&
                    !this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'SelServiceBasis'))) {
                if (this.context.pageParams.vbEnableWeeklyVisitPattern) {
                    this.context.pageParams.blnUseVisitCycleValues = false;
                    this.context.iCABSAServiceCoverMaintenance4.ShowFields();
                }
            }
            this.context.iCABSAServiceCoverMaintenance7.SeasonalServiceInd_onclick();
        } else if (this.context.getRawControlValue('SelServiceBasis') === 'H') {
            this.context.setControlValue('SeasonalServiceInd', false);
            this.context.setControlValue('AnnualCalendarInd', false);
            this.context.setControlValue('FollowTemplateInd', false);
            this.context.setControlValue('HardSlotInd', true);
            this.context.setControlValue('AnnualCalendarTemplateNumber', '');
            this.context.setControlValue('CalendarTemplateName', '');
            this.context.setRequiredStatus('AnnualCalendarTemplateNumber', false);
            this.context.setRequiredStatus('HardSlotType', true);
            this.context.pageParams.uiDisplay.trFollowTemplate = false;
            this.context.pageParams.uiDisplay.trAnnualCalendarTemplateFields = false;
            this.context.pageParams.uiDisplay.cmdHardSlotCalendar = true;
            this.context.pageParams.uiDisplay.trHardSlotType = true;
            this.context.iCABSAServiceCoverMaintenance7.SeasonalServiceInd_onclick();
            if ((this.context.riMaintenance.CurrentMode === MntConst.eModeAdd && this.context.getRawControlValue('HardSlotType') === '')
                || (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate && this.context.getRawControlValue('HardSlotType') === '')) {
                this.context.setControlValue('selHardSlotType', 'D');
                this.context.iCABSAServiceCoverMaintenance2.selHardSlotType_OnChange();
            }
        } else {
            this.context.setControlValue('SeasonalServiceInd', false);
            this.context.setControlValue('AnnualCalendarInd', false);
            this.context.setControlValue('FollowTemplateInd', false);
            this.context.setControlValue('HardSlotInd', false);
            this.context.setControlValue('AnnualCalendarTemplateNumber', '');
            this.context.setControlValue('CalendarTemplateName', '');
            this.context.setRequiredStatus('AnnualCalendarTemplateNumber', false);
            this.context.setRequiredStatus('HardSlotType', false);
            this.context.pageParams.uiDisplay.trFollowTemplate = false;
            this.context.pageParams.uiDisplay.trAnnualCalendarTemplateFields = false;
            this.context.pageParams.uiDisplay.trHardSlotType = false;
            this.context.pageParams.uiDisplay.cmdHardSlotCalendar = false;
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd ||
                (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate &&
                    !this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'SelServiceBasis'))) {
                if (this.context.pageParams.vbEnableWeeklyVisitPattern) {
                    this.context.pageParams.blnUseVisitCycleValues = true;
                    this.context.iCABSAServiceCoverMaintenance4.ShowFields();
                }
            }
            this.context.iCABSAServiceCoverMaintenance7.SeasonalServiceInd_onclick();
        }
        if (this.context.pageParams.vbActiveElement === 'SelServiceBasis') {
            this.context.renderTab(5);
        }
    }

    public selHardSlotType_OnChange(): void {
        this.context.setControlValue('HardSlotType', this.context.getRawControlValue('selHardSlotType'));
        this.context.setRequiredStatus('HardSlotVisitTime', this.context.getRawControlValue('selHardSlotType') === 'S');
        this.context.iCABSAServiceCoverMaintenance2.cmdHardSlotCalendarSet();
    }

    public HardSlotType_OnChange(): void {
        this.context.setControlValue('selHardSlotType', this.context.getRawControlValue('HardSlotType'));
        this.context.iCABSAServiceCoverMaintenance2.selHardSlotType_OnChange();
    }

    public cmdHardSlotCalendarSet(): void {
        if ((this.context.getRawControlValue('selHardSlotType') === 'S')
            && !this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'cmdHardSlotCalendar')) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdHardSlotCalendar');
        }
    }

    public HardSlotVisitTimeOnChange(): void {
        if ((this.context.getRawControlValue('hardSlotVisitTime') !== 'S')
            && !this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'cmdHardSlotCalendar')) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdHardSlotCalendar');
        }
    }

    public GetVisitCycleValues(): void {

        this.context.riMaintenance.clear();
        if (this.context.getRawControlValue('ServiceVisitFrequency')) {
            this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.riMaintenance.PostDataAddFunction('GetVisitCycleValues');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.PostDataAddFromField('ServiceVisitFrequency', MntConst.eTypeInteger);
            this.context.riMaintenance.ReturnDataAdd('VisitCycleInWeeks', MntConst.eTypeInteger);
            this.context.riMaintenance.ReturnDataAdd('VisitsPerCycle', MntConst.eTypeInteger);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                if (!data['ErrorMessage']) {
                    this.context.setControlValue('VisitCycleInWeeks', data['VisitCycleInWeeks']);
                    this.context.setControlValue('VisitsPerCycle', data['VisitsPerCycle']);
                    this.context.setControlValue('VFPNumberOfWeeks', this.context.getRawControlValue('VisitCycleInWeeks'));
                    this.context.setControlValue('VFPNumberOfVisitsPerWeek', this.context.getRawControlValue('VisitsPerCycle'));
                    //this.context.riExchange.riInputElement.markAsError(this.context.uiForm, 'ServiceVisitFrequency');
                    this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'VisitCycleInWeeksOverrideNote');
                    this.context.setRequiredStatus('VisitCycleInWeeksOverrideNote', false);
                } else {
                    //this.context.riExchange.riInputElement.markAsError(this.context.uiForm, 'ServiceVisitFrequency');
                    this.context.setControlValue('ServiceVisitFrequencyCopy', this.context.getRawControlValue('ServiceVisitFrequency'));
                    this.context.setControlValue('VisitCycleInWeeks', '');
                    this.context.setControlValue('VisitsPerCycle', '');
                    this.context.setControlValue('CalculatedVisits', '');
                }
            }, 'POST');
        } else {
            this.context.riExchange.riInputElement.markAsError(this.context.uiForm, 'ServiceVisitFrequency');
        }
    }

    public VisitCycleInWeeks_OnChange(): void {
        let visitCycleInWeeks: any = this.context.getRawControlValue('VisitCycleInWeeks');
        if (visitCycleInWeeks === '0') {
            this.context.setControlValue('VisitCycleInWeeks', '');
        }
        if (visitCycleInWeeks === '1') {
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'VisitsPerCycle');
        } else {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'VisitsPerCycle');
            if (!visitCycleInWeeks) {
                this.context.setControlValue('VisitsPerCycle', '');
            } else {
                this.context.setControlValue('VisitsPerCycle', '1');
            }
        }
        this.VisitCycleValueChanges();
    }

    public VisitsPerCycle_OnChange(): void {
        if (this.context.getRawControlValue('VisitsPerCycle') === '0') {
            this.context.setControlValue('VisitsPerCycle', '');
        }
        this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
        this.VisitCycleValueChanges();
    }

    public VisitCycleValueChanges(): void {
        this.context.pageParams.uiDisplay.tdNumberOfVisitsWarning = false;
        this.context.setControlValue('CalculatedVisits', '');
        //if ( !the values in the VFP table;
        if ((this.context.getRawControlValue('VisitCycleInWeeks') !==
            this.context.getRawControlValue('VFPNumberOfWeeks')) ||
            (this.context.getRawControlValue('VisitsPerCycle') !==
                this.context.getRawControlValue('VFPNumberOfVisitsPerWeek'))) {
            //..but the same as the already save values;
            if ((this.context.pageParams.SavedVisitCycleInWeeks === this.context.getRawControlValue('VisitCycleInWeeks')) &&
                (this.context.pageParams.SavedVisitsPerCycle === this.context.getRawControlValue('VisitsPerCycle'))
                && this.context.riMaintenance.CurrentMode !== MntConst.eModeAdd) {
                this.context.setControlValue('VisitCycleInWeeksOverrideNote', this.context.pageParams.SavedVisitCycleInWeeksOverrideNote);
                this.context.setControlValue('CalculatedVisits', this.context.pageParams.SavedCalculatedVisits);
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'VisitCycleInWeeksOverrideNote');
                this.context.setRequiredStatus('VisitCycleInWeeksOverrideNote', false);
                let visitFrequencyWarningMessage: any = this.context.getRawControlValue('VisitFrequencyWarningMessage');
                if (visitFrequencyWarningMessage !== '') {
                    this.context.pageParams.uiDisplay.tdNumberOfVisitsWarning = true;
                    this.context.pageParams.uiDisplay.VisitFrequencyWarningColour = this.context.getRawControlValue('VisitFrequencyWarningColour');
                    this.context.pageParams.uiDisplay.tdNumberOfVisitsWarning_innerText = visitFrequencyWarningMessage;
                } else {
                    this.context.pageParams.uiDisplay.tdNumberOfVisitsWarning = false;
                }
                //...and !the already saved values;
            } else {
                this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'VisitCycleInWeeksOverrideNote');
                this.context.setControlValue('VisitCycleInWeeksOverrideNote', '');
                this.context.setRequiredStatus('VisitCycleInWeeksOverrideNote', true);
                //this.context.riExchange.riInputElement.Focus('VisitCycleInWeeksOverrideNote');
            }
            //if ( the values are the same as in the VPF table for this frequency;
        } else {
            this.context.setControlValue('VisitCycleInWeeksOverrideNote', '');
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'VisitCycleInWeeksOverrideNote');
            this.context.setRequiredStatus('VisitCycleInWeeksOverrideNote', false);
        }
    }

    public selQuickWindowSet_onchange(id: string): void {
        let srcValue = this.context.getRawControlValue(id);
        let srcRow = parseInt(this.context.utils.Right(window.event.srcElement.id, 1), 10);
        if (srcValue === 'C') {
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'WindowStart' + this.context.ZeroPadInt(srcRow, 2));
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'WindowEnd' + this.context.ZeroPadInt(srcRow, 2));
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'WindowStart' + this.context.ZeroPadInt(srcRow + 7, 2));
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'WindowEnd' + this.context.ZeroPadInt(srcRow + 7, 2));
        } else {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowStart' + this.context.ZeroPadInt(srcRow, 2));
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowEnd' + this.context.ZeroPadInt(srcRow, 2));
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowStart' + this.context.ZeroPadInt(srcRow + 7, 2));
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowEnd' + this.context.ZeroPadInt(srcRow + 7, 2));
        }

        switch (srcValue) {
            case 'P':
                this.context.setControlValue('WindowStart' + this.context.ZeroPadInt(srcRow, 2),
                    this.context.getRawControlValue('PremiseWindowStart' + this.context.ZeroPadInt(srcRow, 2)));
                this.context.setControlValue('WindowEnd' + this.context.ZeroPadInt(srcRow, 2),
                    this.context.getRawControlValue('PremiseWindowEnd' + this.context.ZeroPadInt(srcRow, 2)));
                this.context.setControlValue('WindowStart' + this.context.ZeroPadInt(srcRow + 7, 2),
                    this.context.getRawControlValue('PremiseWindowStart' + this.context.ZeroPadInt(srcRow + 7, 2)));
                this.context.setControlValue('WindowEnd' + this.context.ZeroPadInt(srcRow + 7, 2),
                    this.context.getRawControlValue('PremiseWindowEnd' + this.context.ZeroPadInt(srcRow + 7, 2)));
                break;
            case 'D':
                this.context.setControlValue('WindowStart' + this.context.ZeroPadInt(srcRow, 2),
                    this.context.getRawControlValue('DefaultWindowStart' + this.context.ZeroPadInt(srcRow + srcRow - 1, 2)));
                this.context.setControlValue('WindowEnd' + this.context.ZeroPadInt(srcRow, 2),
                    this.context.getRawControlValue('DefaultWindowEnd' + this.context.ZeroPadInt(srcRow + srcRow - 1, 2)));
                this.context.setControlValue('WindowStart' + this.context.ZeroPadInt(srcRow + 7, 2),
                    this.context.getRawControlValue('DefaultWindowStart' + this.context.ZeroPadInt(srcRow + srcRow, 2)));
                this.context.setControlValue('WindowEnd' + this.context.ZeroPadInt(srcRow + 7, 2),
                    this.context.getRawControlValue('DefaultWindowEnd' + this.context.ZeroPadInt(srcRow + srcRow, 2)));
                break;
            case 'U':
                this.context.setControlValue('WindowStart' + this.context.ZeroPadInt(srcRow, 2), '00:00');
                this.context.setControlValue('WindowEnd' + this.context.ZeroPadInt(srcRow, 2), '00:00');
                this.context.setControlValue('WindowStart' + this.context.ZeroPadInt(srcRow + 7, 2), '00:00');
                this.context.setControlValue('WindowEnd' + this.context.ZeroPadInt(srcRow + 7, 2), '00:00');
                break;
            case 'A':
                this.context.setControlValue('WindowStart' + this.context.ZeroPadInt(srcRow, 2), '00:00');
                this.context.setControlValue('WindowEnd' + this.context.ZeroPadInt(srcRow, 2), '11:59');
                this.context.setControlValue('WindowStart' + this.context.ZeroPadInt(srcRow + 7, 2), '12:00');
                this.context.setControlValue('WindowEnd' + this.context.ZeroPadInt(srcRow + 7, 2), '23:59');
                break;
        }
    }

    public UpdateDefaultTimes(vbDefaultName: string): void {

        for (let iLoop = 1; iLoop <= 7; iLoop++) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowStart' + this.context.ZeroPadInt(iLoop, 2));
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowEnd' + this.context.ZeroPadInt(iLoop, 2));
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowStart' + this.context.ZeroPadInt(iLoop + 7, 2));
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'WindowEnd' + this.context.ZeroPadInt(iLoop + 7, 2));
            if (vbDefaultName === 'Default') {
                this.context.setControlValue('WindowStart' + this.context.ZeroPadInt(iLoop, 2),
                    this.context.getRawControlValue(vbDefaultName + 'WindowStart' + this.context.ZeroPadInt(iLoop + iLoop - 1, 2)));
                this.context.setControlValue('WindowEnd' + this.context.ZeroPadInt(iLoop, 2),
                    this.context.getRawControlValue(vbDefaultName + 'WindowEnd' + this.context.ZeroPadInt(iLoop + iLoop - 1, 2)));
                this.context.setControlValue('WindowStart' + this.context.ZeroPadInt(iLoop + 7, 2),
                    this.context.getRawControlValue(vbDefaultName + 'WindowStart' + this.context.ZeroPadInt(iLoop + iLoop, 2)));
                this.context.setControlValue('WindowEnd' + this.context.ZeroPadInt(iLoop + 7, 2),
                    this.context.getRawControlValue(vbDefaultName + 'WindowEnd' + this.context.ZeroPadInt(iLoop + iLoop, 2)));
            }
        }
    }

    public WindowPreferredIndChanged(): void {
        this.context.pageParams.blnAnyPreferred = false;
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd ||
            this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            for (let iLoop = 1; iLoop <= 7; iLoop++) {
                if (this.context.isControlChecked('WindowPreferredInd0' + iLoop)) {
                    this.context.pageParams.blnAnyPreferred = true;
                }
            }
            if (this.context.pageParams.blnAnyPreferred) {
                this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'PreferredDayOfWeekReasonCode');
                this.context.setRequiredStatus('PreferredDayOfWeekReasonCode', true);
            } else {
                this.context.setControlValue('PreferredDayOfWeekReasonCode', '');
                this.context.setControlValue('PreferredDayOfWeekReasonLangDesc', '');
                this.context.setDropDownComponentValue('PreferredDayOfWeekReasonCode', 'PreferredDayOfWeekReasonLangDesc');
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'PreferredDayOfWeekReasonCode');
                this.context.setRequiredStatus('PreferredDayOfWeekReasonCode', false);
            }
        }
    }

    public PreferredDayOfWeekReasonCodeonchange(): void {
        if (this.context.getRawControlValue('PreferredDayOfWeekReasonCode') !== '0' &&
            this.context.getRawControlValue('PreferredDayOfWeekReasonCode') !== '') {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = 'iCABSExchangeFunctions.p';
            this.context.riMaintenance.PostDataAdd('PostDesc', 'PreferredDayOfWeekReason', MntConst.eModeUpdate);
            this.context.PostDataAddFromField('PreferredDayOfWeekReasonCode', MntConst.eTypeInteger);
            this.context.riMaintenance.PostDataAdd('LanguageCode', this.context.riExchange.LanguageCode(), MntConst.eTypeCode);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('PreferredDayOfWeekReasonLangDesc', data['PreferredDayOfWeekReasonDesc']);
            });
        } else {
            this.context.setControlValue('PreferredDayOfWeekReasonCode', '');
            this.context.setControlValue('PreferredDayOfWeekReasonLangDesc', '');
        }
    }

    public LinkedProductCodeOnChange(): void {
        // Reset Cache Value So When A User Selects A Prod (may even be changed back to the || iginal),;
        // The Grid Will Be Rebuilt Forcing The User To Reselect The Components;
        this.context.setControlValue('ComponentGridCacheTime', this.context.utils.Time());
        this.context.setControlValue('LinkedServiceCoverNumber', '');
        this.context.setControlValue('LinkedServiceVisitFreq', '');
        this.context.setControlValue('LinkedProductDesc', '');
        this.context.linkedServiceCoverSearchParams.ProductCode = this.context.getRawControlValue('ProductCode');
        this.context.linkedServiceCoverSearchParams.LinkedProductCode = this.context.getRawControlValue('LinkedProductCode');
        this.context.linkedServiceCoverSearchParams.DispenserInd = this.context.getRawControlValue('DispenserInd');
        this.context.linkedServiceCoverSearchParams.ConsumableInd = this.context.getRawControlValue('ConsumableInd');
        this.context.calledFromOnChange = true;
        this.context.linkedServiceCoverSearch.openModal();
    }

    public ProcessTimeString(vbTimeField: any): void {
        this.context.pageParams.vbError = false;
        this.context.pageParams.vbTimeFormat = '##00' + this.context.pageParams.vbTimeSeparator + '##';
        switch (vbTimeField) {
            case 'StandardTreatmentTime':
                this.context.pageParams.vbTime = this.context.getRawControlValue('StandardTreatmentTime').
                    replace(this.context.pageParams.vbTimeSeparator, '');
                break;
            case 'InitialTreatmentTime':
                this.context.pageParams.vbTime = this.context.getRawControlValue('InitialTreatmentTime').
                    replace(this.context.pageParams.vbTimeSeparator, '');
                break;
            case 'ServiceAnnualTime':
                this.context.pageParams.vbTime = this.context.getRawControlValue('ServiceAnnualTime').
                    replace(this.context.pageParams.vbTimeSeparator, '');
                break;
            case 'SalesPlannedTime':
                this.context.pageParams.vbTime = this.context.getRawControlValue('SalesPlannedTime').
                    replace(this.context.pageParams.vbTimeSeparator, '');
                break;
            case 'ActualPlannedTime':
                this.context.pageParams.vbTime = this.context.getRawControlValue('ActualPlannedTime').
                    replace(this.context.pageParams.vbTimeSeparator, '');
                break;
        }

        try {
            let re = /^[0-9]{4,5}$/;
            let reg = new RegExp(re);
            if (reg.test(this.context.pageParams.vbTime)) {
                if (!isNaN(parseInt(this.context.pageParams.vbTime, 10))) {
                    this.context.pageParams.vbError = false;
                } else {
                    this.context.pageParams.vbError = true;
                }
            } else {
                this.context.pageParams.vbError = true;
            }
        } catch (e) {
            this.context.pageParams.vbError = true;
        }

        if (!this.context.pageParams.vbError && ((this.context.pageParams.vbTime.length < 4) || (this.context.pageParams.vbTime.length > 7))) {
            this.context.pageParams.vbError = true;
        } else if (!this.context.pageParams.vbError) {
            if (!this.context.pageParams.vbError && parseInt(this.context.pageParams.vbTime, 10) === 0) {
                this.context.pageParams.vbError = true;
            }
            if (!this.context.pageParams.vbError) {
                this.context.pageParams.vbDurationHours = this.context.utils.mid(this.context.pageParams.vbTime, 1, this.context.pageParams.vbTime.length - 2);
                this.context.pageParams.vbDurationMinutes = this.context.utils.Right(this.context.pageParams.vbTime, 2);
                if (this.context.pageParams.vbDurationMinutes > 59) {
                    this.context.showAlert(MessageConstant.PageSpecificMessage.vbDurationMinutes, 2);
                    this.context.pageParams.vbError = true;
                } else {
                    this.context.pageParams.vbTimeSec = (this.context.pageParams.vbDurationHours * 60 * 60) + (this.context.pageParams.vbDurationMinutes * 60);
                }
            }
        }

        switch (vbTimeField) {
            case 'StandardTreatmentTime':
                //TODO Format time
                if (!this.context.pageParams.vbError) {
                    this.context.setControlValue(vbTimeField,
                        this.context.pageParams.vbDurationHours + ':' + this.context.pageParams.vbDurationMinutes);
                    this.context.uiForm.controls[vbTimeField].setErrors(null);
                } else {
                    this.context.setControlValue(vbTimeField, '');
                    this.context.uiForm.controls[vbTimeField].setErrors({ 'incorrect': true });
                    this.context.pageParams.vbError = false;
                }
                break;
            case 'InitialTreatmentTime':
                if (!this.context.pageParams.vbError) {
                    this.context.setControlValue('InitialTreatmentTime',
                        this.context.pageParams.vbDurationHours + ':' + this.context.pageParams.vbDurationMinutes);
                    this.context.uiForm.controls[vbTimeField].setErrors(null);
                } else {
                    this.context.setControlValue('InitialTreatmentTime', '');
                    this.context.pageParams.vbError = false;
                }
                break;
            case 'ServiceAnnualTime':
                if (!this.context.pageParams.vbError) {
                    this.context.setControlValue('ServiceAnnualTime',
                        this.context.pageParams.vbDurationHours + ':' + this.context.pageParams.vbDurationMinutes);
                    this.context.uiForm.controls[vbTimeField].setErrors(null);
                } else {
                    this.context.setControlValue('ServiceAnnualTime', '');
                    this.context.uiForm.controls[vbTimeField].setErrors({ 'incorrect': true });
                    this.context.pageParams.vbError = false;
                }
                break;
            case 'SalesPlannedTime':
                if (!this.context.pageParams.vbError) {
                    this.context.setControlValue('SalesPlannedTime',
                        this.context.pageParams.vbDurationHours + ':' + this.context.pageParams.vbDurationMinutes);
                    this.context.uiForm.controls[vbTimeField].setErrors(null);
                } else {
                    this.context.setControlValue('SalesPlannedTime', '');
                    this.context.uiForm.controls[vbTimeField].setErrors({ 'incorrect': true });
                    this.context.pageParams.vbError = false;
                }
                break;
            case 'ActualPlannedTime':
                if (!this.context.pageParams.vbError) {
                    this.context.setControlValue(vbTimeField,
                        this.context.pageParams.vbDurationHours + ':' + this.context.pageParams.vbDurationMinutes);
                    this.context.uiForm.controls[vbTimeField].setErrors(null);
                } else {
                    this.context.setControlValue(vbTimeField, '');
                    this.context.uiForm.controls[vbTimeField].setErrors({ 'incorrect': true });
                    this.context.pageParams.vbError = false;
                }
                break;
        }
    }

    public CalculateEntitlementServiceQuantity(): void {
        if (this.context.pageParams.uiDisplay.trEntitlementServiceQuantity) {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntryRequests.p';
            this.context.riMaintenance.PostDataAddAction('6');
            this.context.riMaintenance.PostDataAddFunction('GetServiceVisitQuantity');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.PostDataAddFromField('ServiceVisitFrequency', MntConst.eTypeInteger);
            this.context.PostDataAddFromField('EntitlementAnnualQuantity', MntConst.eTypeInteger);
            this.context.riMaintenance.ReturnDataAdd('EntitlementServiceVisitQuantity', MntConst.eTypeInteger);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('EntitlementServiceQuantity', data['EntitlementServiceVisitQuantity']);
            }, 'POST');
        }
    }

    public AnnualValueChangeonBlur(): void {
        if (this.context.isFieldValid('AnnualValueChange')) {
            if (this.context.getRawControlValue('AnnualValueChange')) {
                if (!this.context.pageParams.vbPriceChangeOnlyInd &&
                    this.context.getControlValueAsString('ServiceVisitFrequency') === this.context.pageParams.vbServiceVisitFrequency &&
                    this.context.getControlValueAsString('ServiceQuantity') === this.context.pageParams.vbServiceQuantity &&
                    this.context.getRawControlValue('AnnualValueChange') !== '0') {
                    this.context.pageParams.uiDisplay.trPriceChangeOnly = true;
                    this.context.setControlValue('PriceChangeOnlyInd', true);
                    this.context.pageParams.vbPriceChangeOnlyInd = true;
                    //TODO
                    // if (InStr(document.ActiveElement.id, 'riGrid-') = 0) {
                    //     if (this.context.pageParams.uiDisplay.tdLostBusiness = '') {
                    //         //Call ritab.TabFocusToHTMLElement('LostBusinessCode');
                    //     } else {
                    //         //Call riTab.TabFocusToHTMLElement('PriceChangeOnlyInd');
                    //     }
                    // }
                } else if (this.context.pageParams.vbPriceChangeOnlyInd &&
                    (this.context.getRawControlValue('ServiceVisitFrequency').toString() !== this.context.pageParams.vbServiceVisitFrequency ||
                        this.context.getRawControlValue('ServiceQuantity').toString() !== this.context.pageParams.vbServiceQuantity)) {
                    this.context.pageParams.uiDisplay.trPriceChangeOnly = false;
                    this.context.pageParams.PriceChangeOnlyInd = false;
                    this.context.pageParams.vbPriceChangeOnlyInd = false;
                }
                this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
            }
        }
    }

    public ServiceQuantityLabel(): void {
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            (this.context.isControlChecked('DisplayLevelInd'))) {
            this.context.pageParams.spanServiceQuantityLab_innerText = 'Quantity of Displays';
            this.context.pageParams.spanUnconfirmedDeliveryQtyLab_innerText = 'Quantity of Displays';
        } else {
            this.context.pageParams.spanServiceQuantityLab_innerText = 'Service Quantity';
            this.context.pageParams.spanUnconfirmedDeliveryQtyLab_innerText = 'Quantity';
        }
    }
}
