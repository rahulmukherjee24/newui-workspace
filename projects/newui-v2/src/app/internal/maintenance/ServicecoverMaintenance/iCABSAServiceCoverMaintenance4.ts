import { MessageConstant } from './../../../../shared/constants/message.constant';
import { Injector } from '@angular/core';
import { ServiceCoverMaintenanceComponent } from './iCABSAServiceCoverMaintenance.component';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';

export class ServiceCoverMaintenance4 {

    private context: ServiceCoverMaintenanceComponent;

    constructor(private parent: ServiceCoverMaintenanceComponent, injector: Injector) {
        this.context = parent;
    }

    public cmdCalcInstalment_OnClick(): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntry.p';
        this.context.riMaintenance.PostDataAddAction('6');
        this.context.riMaintenance.PostDataAddFunction('CalcBudgetInstalments');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeTextFree);
        this.context.PostDataAddFromField('PremiseNumber', MntConst.eTypeInteger);
        this.context.PostDataAddFromField('ProductCode', MntConst.eTypeCode);
        this.context.PostDataAddFromField('ServiceAnnualValue', MntConst.eTypeDecimal1);
        this.context.PostDataAddFromField('BudgetNumberInstalments', MntConst.eTypeInteger);
        this.context.riMaintenance.ReturnDataAdd('BudgetNumberInstalments', MntConst.eTypeInteger);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.setControlValue('BudgetNumberInstalments', data['BudgetNumberInstalments']);
            this.context.setControlValue('BudgetInstalAmount', data['BudgetInstalAmount']);
            this.context.setControlValue('BudgetBalance', data['BudgetBalance']);
            this.context.setControlValue('BudgetTermsDesc', data['BudgetTermsDesc']);
        }, 'POST');
    }

    public ShowHideBudgetBilling(): void {
        if (this.context.getRawControlValue('InvoiceTypeNumber') !== (undefined || null)) {  //IUI-22924
            let shouldShwInvoiceLines: boolean = (this.context.getRawControlValue('InvoiceTypeNumber') === '26');
            if (shouldShwInvoiceLines) {
                this.context.iCABSAServiceCoverMaintenance6.GetBudgetInstalmentDetails();
            }
            for (let idx: number = 1; idx < 6; idx++) {
                this.context.pageParams.uiDisplay['trBudgetBillingLine' + idx] = shouldShwInvoiceLines;
            }
        }
    }

    // C B O REQUEST;
    public riExchange_CBORequest(showSpinner?: boolean): void {
        if (!this.context || !this.context.httpService || (this.context.riMaintenance.CurrentMode !== MntConst.eModeAdd &&
            this.context.riMaintenance.CurrentMode !== MntConst.eModeUpdate)) {
            return;
        }
        this.context.pageParams.FunctionString = '';
        this.context.pageParams.strPostVars = '';
        this.context.pageParams.strFunctions = '';
        this.context.pageParams.blnContractNumber = false;
        this.context.pageParams.blnPremiseNumber = false;
        this.context.pageParams.blnProductCode = false;
        this.context.pageParams.blnNewPremise = false;
        this.context.pageParams.blnFieldShowList = false;
        this.context.pageParams.blnServiceCommenceDate = false;
        this.context.pageParams.blnServiceQuantity = false;
        this.context.pageParams.blnLastChangeEffectDate = false;
        this.context.pageParams.blnNextInvoiceStartDate = false;
        this.context.pageParams.blnNextInvoiceEndDate = false;
        this.context.pageParams.blnServiceAnnualValue = false;
        this.context.pageParams.blnServiceBranchNumber = false;
        this.context.pageParams.blnBranchServiceAreaCode = false;
        this.context.pageParams.blnAnnualValueChange = false;
        this.context.pageParams.blnEntitlementQuantity = false;
        this.context.pageParams.blnEntitlementPrice = false;
        this.context.pageParams.blnTrialPeriodEndDate = false;
        this.context.pageParams.blnSeasonalTemplateNumber = false;
        this.context.pageParams.blnBusinessOrigin = false;
        this.context.pageParams.blnContractTrialInd = false;
        this.context.pageParams.blnWasteTransferTypeCode = false;
        this.context.pageParams.blnLeadEmployee = false;
        this.context.pageParams.blnMinimumDuration = false;
        this.context.pageParams.blnExcludeUnConfirmedValues = false;
        this.context.pageParams.blnUpliftTemplateNumber = false;
        this.context.pageParams.blnRMMCategory = false;

        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetBusinessDefaultWindows,';
        this.context.logger.log(' IN CBO', this.context.riMaintenance.CurrentMode);
        // 1.Decide which functions need to run;
        switch (this.context.riMaintenance.CurrentMode) {
            case MntConst.eModeAdd:
                if (this.context.pageParams.vbEnableTimePlanning && this.context.hasControlChanged('ServiceQuantity')) {
                    this.context.iCABSAServiceCoverMaintenance8.UpdateSPT();
                    this.context.iCABSAServiceCoverMaintenance7.APTChangedAccordingToQuantity();
                }

                if (this.context.hasControlChanged('CustomerAvailTemplateID')) {
                    this.context.iCABSAServiceCoverMaintenance2.UpdateTemplate();
                }

                if (this.context.getRawControlValue('ContractNumber') &&
                    this.context.getRawControlValue('PremiseNumber')) {
                    if (this.context.hasControlChanged('ContractNumber') ||
                        this.context.hasControlChanged('PremiseNumber')) {
                        this.context.setControlValue('NewPremise', this.context.parentMode === 'Premise-Add' || this.context.pageParams.currentContractType === 'J' ? 'yes' : 'no');
                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetServiceDefaultValues,';
                        this.context.pageParams.blnContractNumber = true;
                        this.context.pageParams.blnPremiseNumber = true;
                        this.context.pageParams.blnNewPremise = true;
                        if (!this.context.getRawControlValue('InstallationEmployeeCode')) {
                            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'DefaultInstallationEmployee,';
                        }
                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetPremiseWindows,';
                    } //CN PN changed;

                    if (this.context.getRawControlValue('ProductCode') &&
                        this.context.hasControlChanged('ProductCode')) {
                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetServiceDetails,GetShowFields,GetWasteTransferType,';
                        this.context.pageParams.blnContractNumber = true;
                        this.context.pageParams.blnPremiseNumber = true;
                        this.context.pageParams.blnProductCode = true;
                        this.context.pageParams.blnServiceBranchNumber = true;
                        this.context.pageParams.blnFieldShowList = true;
                        if (this.context.getRawControlValue('ServiceCommenceDate')) {
                            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetDates,';
                            this.context.pageParams.blnServiceCommenceDate = true;
                        }
                    }
                    if ((this.context.getRawControlValue('ProductCode') &&
                        this.context.hasControlChanged('ProductCode')) ||
                        (this.context.getRawControlValue('WasteTransferTypeCode') &&
                            this.context.hasControlChanged('WasteTransferTypeCode'))) {

                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'CheckWasteConsignmentNoteRequired,';
                        this.context.pageParams.blnContractNumber = true;
                        this.context.pageParams.blnPremiseNumber = true;
                        this.context.pageParams.blnProductCode = true;
                        this.context.pageParams.blnWasteTransferTypeCode = true;
                    }
                    // 26/05/06 PG #23032 Get Virtuals loses focus, so stop it being called when user;
                    // tabs off the (normally) last fld.  Why is this being called so often anyway?;
                }
                //Default ServiceVisitAnnivDt,NextInvoiceStartDt && validate ServiceCommenceDt is an anniversary dt;
                if (this.context.hasControlChanged('ServiceCommenceDate') &&
                    this.context.getRawControlValue('ServiceCommenceDate')) {
                    this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetDates,WarnAnniversaryCommenceDate,';
                    this.context.pageParams.blnContractNumber = true;
                    this.context.pageParams.blnServiceCommenceDate = true;
                    if (this.context.pageParams.vbEnableServiceCoverDispLev) {
                        this.context.pageParams.blnMinimumDuration = true;
                    }
                }
                // 05/05/05 PG SRS 41 Get inv dts for FOC;
                if (this.context.hasControlChanged('FOCInvoiceStartDate') &&
                    this.context.getRawControlValue('FOCInvoiceStartDate')) {
                    this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetDates,WarnFOCInvoiceStartDate,GetFOCWarning,';
                    this.context.pageParams.blnContractNumber = true;
                    this.context.pageParams.blnServiceCommenceDate = true;
                }
                //Validate ServiceAnnualValue is more than min value && less than max value on Bus file;
                if (this.context.hasControlChanged('ServiceAnnualValue') &&
                    this.context.getRawControlValue('ServiceAnnualValue') && this.context.pageParams.blnValueRequired) {
                    this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'WarnValueRange,';
                    //MSA - QRS603(UK) - Inv ServiceCovers on Jobs at same time;
                    if (this.context.pageParams.currentContractType === 'J') {
                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetInvoiceOnFirstVisitIndAdd,';
                        this.context.pageParams.blnContractNumber = true;
                        this.context.pageParams.blnPremiseNumber = true;
                    }
                    this.context.pageParams.blnServiceAnnualValue = true;
                }
                if (this.context.isControlChecked('SeasonalServiceInd')) {
                    //Get template dtls when template no has chngd;
                    if (this.context.hasControlChanged('SeasonalTemplateNumber') &&
                        this.context.getRawControlValue('SeasonalTemplateNumber')) {
                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetSeasonalTemplateInformation,GetSeasonalTemplateDates,';
                        this.context.pageParams.blnSeasonalTemplateNumber = true;
                        this.context.pageParams.blnLastChangeEffectDate = true;
                    }
                }
                this.context.iCABSAServiceCoverMaintenance4.CBO_Contd(showSpinner);
                break;
            case MntConst.eModeUpdate:
                this.context.logger.log(' INSIDE CBO 1', this.context.riMaintenance.CurrentMode);
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    (this.context.isControlChecked('DisplayLevelInd') || this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                    this.context.isControlChecked('VisitTriggered') && this.context.riExchange.URLParameterContains('PendingReduction') &&
                    this.context.hasControlChanged('LastChangeEffectDate')) {
                    this.context.setControlValue('UnitValueChange', '0');
                    this.context.setControlValue('AnnualValueChange', '0');
                }
                if (this.context.hasControlChanged('CustomerAvailTemplateID')) {
                    this.context.iCABSAServiceCoverMaintenance2.UpdateTemplate();
                }
                //Qty has changed;
                if (this.context.hasControlChanged('ServiceQuantity')) {
                    if (!this.context.getRawControlValue('SavedServiceQuantity')) {
                        this.context.setControlValue('SavedServiceQuantity', '0');
                    }

                    if (this.context.CInt('ServiceQuantity') > this.context.CInt('SavedServiceQuantity')) {
                        this.context.setControlValue('OutstandingInstallations',
                            this.context.CInt('ServiceQuantity') - this.context.CInt('SavedServiceQuantity'));
                        this.context.pageParams.SavedIncreaseQuantity = this.context.CInt('OutstandingInstallations');
                        this.context.pageParams.SavedReductionQuantity = 0;
                        if (this.context.pageParams.vbDisplayLevelInstall && this.context.isControlChecked('InstallationRequired')) {
                            this.context.setControlValue('OutstandingInstallations', '0');
                        }
                        //if Qty has reduced, default the o/standing removals fld to the reduction qty;
                    } else if (this.context.CInt('ServiceQuantity') < this.context.CInt('SavedServiceQuantity')) {
                        this.context.setControlValue('OutstandingRemovals',
                            this.context.CInt('SavedServiceQuantity') - this.context.CInt('ServiceQuantity'));
                        this.context.pageParams.SavedReductionQuantity = this.context.CInt('OutstandingRemovals');
                        this.context.pageParams.SavedIncreaseQuantity = 0;
                    }
                    if ((this.context.getRawControlValue('ProductCode') &&
                        this.context.hasControlChanged('ProductCode')) ||
                        (this.context.getRawControlValue('WasteTransferTypeCode') &&
                            this.context.hasControlChanged('WasteTransferTypeCode'))) {
                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'CheckWasteConsignmentNoteRequired,';
                        this.context.pageParams.blnContractNumber = true;
                        this.context.pageParams.blnPremiseNumber = true;
                        this.context.pageParams.blnProductCode = true;
                        this.context.pageParams.blnWasteTransferTypeCode = true;
                    }
                }

                if (this.context.getRawControlValue('ContractNumber') &&
                    this.context.getRawControlValue('PremiseNumber')) {
                    this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetPremiseWindows,';
                    this.context.pageParams.blnContractNumber = true;
                    this.context.pageParams.blnPremiseNumber = true;
                }
                //Default //Removal// employee;
                //if( in //Updt// mode && one of the following:  a) Service Area changed;
                //                                             b) Service Qty changed;
                //                                             c) O/standing Removals Qty changed;
                //AND o/standing Removals is less than the ttl reduction made (i.e. some units have already been removed);
                if ((this.context.hasControlChanged('BranchServiceAreaCode')
                    || this.context.hasControlChanged('ServiceQuantity')
                    || this.context.hasControlChanged('OutstandingRemovals'))) {
                    if (isNaN(parseInt(this.context.getRawControlValue('OutstandingRemovals'), 10))) {
                        this.context.setControlValue('OutstandingRemovals', '0');
                    }
                    if ((this.context.pageParams.SavedReductionQuantity > 0 && this.context.pageParams.SavedReductionQuantity > parseInt(this.context.getRawControlValue('OutstandingRemovals'), 10))
                        && !this.context.pageParams.vbDisplayLevelInstall) {
                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'DefaultRemovalEmployee,';
                        this.context.pageParams.blnServiceBranchNumber = true;
                        this.context.pageParams.blnBranchServiceAreaCode = true;
                    }
                }
                //Also do equivalent for Installations;
                if ((this.context.hasControlChanged('BranchServiceAreaCode')
                    || this.context.hasControlChanged('ServiceQuantity')
                    || this.context.hasControlChanged('OutstandingInstallations'))) {
                    if (isNaN(parseInt(this.context.getRawControlValue('OutstandingInstallations'), 10))) {
                        this.context.setControlValue('OutstandingInstallations', '0');
                    }
                    if ((this.context.pageParams.SavedIncreaseQuantity > 0 && this.context.pageParams.SavedIncreaseQuantity > parseInt(this.context.getRawControlValue('OutstandingInstallations'), 10))
                        && !this.context.pageParams.vbDisplayLevelInstall) {
                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'DefaultInstallationEmployee,';
                        this.context.pageParams.blnServiceBranchNumber = true;
                        this.context.pageParams.blnBranchServiceAreaCode = true;
                    }
                }
                //Calc ServiceAnnualValue due to AnnulaValueChange && Qty/Freq at LastChangeEffectDt;
                if (this.context.pageParams.currentContractType === 'C' && ((this.context.hasControlChanged('LastChangeEffectDate') && this.context.getRawControlValue('LastChangeEffectDate'))
                    || (this.context.hasControlChanged('AnnualValueChange') && this.context.getRawControlValue('AnnualValueChange'))
                    || (this.context.hasControlChanged('ServiceVisitFrequency') && this.context.getRawControlValue('ServiceVisitFrequency'))
                    || (this.context.hasControlChanged('ServiceQuantity') && this.context.getRawControlValue('ServiceQuantity')))) {
                    this.context.pageParams.strPostVars = this.context.pageParams.strPostVars + '&ServiceCoverRowID=' + this.context.riMaintenance.GetRowID('ServiceCoverROWID');
                    if (this.context.hasControlChanged('LastChangeEffectDate') && this.context.getRawControlValue('LastChangeEffectDate')) {
                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetServiceValuesAtEffectDate,';
                        if (this.context.isControlChecked('SeasonalServiceInd')) {
                            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetSeasonValuesAtEffectDate,';
                        }
                    }
                    if (this.context.hasControlChanged('LastChangeEffectDate') &&
                        this.context.getRawControlValue('LastChangeEffectDate')) {
                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'WarnAnniversaryDate,';
                        if (this.context.pageParams.blnEnableDowSentricon) {
                            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'CalcDOWSentriconValues,';
                        }
                    }
                    let isPendingReduction: boolean = this.context.riExchange.URLParameterContains('PendingReduction');
                    this.context.pageParams.blnExcludeUnConfirmedValues = isPendingReduction;
                    this.context.setControlValue('PendingReduction', isPendingReduction ? 'PendingReduction' : '');
                    this.context.pageParams.blnLastChangeEffectDate = true;
                    this.context.pageParams.blnAnnualValueChange = true;
                    if (this.context.hasControlChanged('AnnualValueChange') &&
                        this.context.getRawControlValue('AnnualValueChange')) {
                        this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetAnnualValueAtEffectDate,WarnValueRange,';
                        if (this.context.pageParams.vbEnableTimePlanning) {
                            let msg1 = 'This Service Cover has an Actual Planned Time of ';
                            let msg2 = '. Please change accordingly if required.';
                            this.context.utils.getTranslatedval('This Service Cover has an Actual Planned Time of ').then((res: string) => {
                                if (res) {
                                    msg1 = res;
                                }
                            });

                            this.context.utils.getTranslatedval('. Please change accordingly if required.').then((res: string) => {
                                if (res) {
                                    msg2 = res;
                                }
                            });
                            this.context.messageType = 'warning';
                            this.context.alertMessage = {
                                msg: msg1 + this.context.getControlValue('ActualPlannedTime') + msg2,
                                timestamp: (new Date()).getMilliseconds()
                            };
                            this.context.iCABSAServiceCoverMaintenance4.CBO_Update_contd(false);
                        } else {
                            this.context.iCABSAServiceCoverMaintenance4.CBO_Update_contd(showSpinner);
                        }
                    } else {
                        this.context.iCABSAServiceCoverMaintenance4.CBO_Update_contd(showSpinner);
                    }
                } else {
                    this.context.iCABSAServiceCoverMaintenance4.CBO_Update_contd(showSpinner);
                }
        } //CurrentMode
    }

    public CBO_Update_contd(showSpinner?: boolean): void {
        this.context.logger.log(' INSIDE CBO 2', this.context.riMaintenance.CurrentMode);
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate
            && (this.context.hasControlChanged('AnnualValueChange') && this.context.getRawControlValue('AnnualValueChange'))) {
            this.context.pageParams.blnValueChange = true;
        }
        //Default Inv on First Visit indicator depeneding on value of Job;
        if (this.context.pageParams.currentContractType === 'J'
            && this.context.hasControlChanged('ServiceAnnualValue') && this.context.getRawControlValue('ServiceAnnualValue')) {
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                if (!this.context.pageParams.strPostVars) {
                    this.context.pageParams.strPostVars = this.context.pageParams.strPostVars + '&ServiceCoverRowID=' + this.context.getControlValue('ServiceCoverROWID')
                        + '&JobInvoiceFirstVisitValue=' + (this.context.pageParams.JobInvoiceFirstVisitValue || '');
                } else {
                    this.context.pageParams.strPostVars = this.context.pageParams.strPostVars + '&JobInvoiceFirstVisitValue=' + (this.context.pageParams.JobInvoiceFirstVisitValue || '');
                }
            }
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'WarnValueRange,GetInvoiceOnFirstVisitInd,';
            this.context.pageParams.blnServiceAnnualValue = true;
            this.context.pageParams.blnValueChange = true; //This will be used to enable display entry;
        }
        // if( Trial Period End Dt has changed, allow change of Trial Period Chrg;
        if (this.context.hasControlChanged('TrialPeriodEndDate')) {
            // Check Trial Period End Dt against value retrieved from record - if( dif(ferent;
            // allow change to value fld. if( the same, reset value to value from record and;
            // disable fld.;
            if (this.context.getRawControlValue('TrialPeriodEndDate') === this.context.pageParams.dtTrialPeriodEndDateRecordValue) {
                this.context.riMaintenance.DisableInput('TrialPeriodChargeValue');
                this.context.setControlValue('TrialPeriodChargeValue', this.context.pageParams.deTrialPeriodChargeRecordValue);
            } else {
                this.context.riMaintenance.EnableInput('TrialPeriodChargeValue');
            }
        }
        if (this.context.hasControlChanged('ServiceVisitAnnivDate')) {
            this.context.pageParams.blnServiceVisitAnnivDateChange = true;
        }
        if (this.context.isControlChecked('SeasonalServiceInd')) {
            if (this.context.hasControlChanged('SeasonalTemplateNumber') && this.context.getRawControlValue('SeasonalTemplateNumber')) {
                this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetSeasonalTemplateInformation,GetSeasonalTemplateDates,';
                this.context.pageParams.blnSeasonalTemplateNumber = true;
                this.context.pageParams.blnLastChangeEffectDate = true;
            }
        }
        this.context.iCABSAServiceCoverMaintenance4.CBO_Contd(showSpinner);
    }

    public CBO_Contd(showSpinner?: boolean): void {
        if (!(this.context.initialLoad && this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) && ((this.context.hasControlChanged('ProductCode') && this.context.getRawControlValue('ProductCode'))
            || (this.context.hasControlChanged('LastChangeEffectDate') && this.context.getRawControlValue('LastChangeEffectDate'))
            || this.context.hasControlChanged('ServiceQuantity'))) {
            if (this.context.pageParams.strPostVars && this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                this.context.pageParams.strPostVars = this.context.pageParams.strPostVars + '&ServiceCoverRowID=' + this.context.riMaintenance.GetRowID('ServiceCoverROWID');
            }
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'CalculateInstallationValue,';
            this.context.pageParams.blnProductCode = true;
            this.context.pageParams.blnServiceQuantity = true;
            this.context.pageParams.blnLastChangeEffectDate = true;
        }
        //Recalc Inv Val if( Service Commence Dt/Effect Dt/Annual Val chnged;
        if (!(this.context.initialLoad && this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) && ((this.context.hasControlChanged('ServiceCommenceDate') && this.context.getRawControlValue('ServiceCommenceDate'))
            || (this.context.hasControlChanged('LastChangeEffectDate') && this.context.getRawControlValue('LastChangeEffectDate'))
            || (this.context.hasControlChanged('ServiceAnnualValue') && this.context.getRawControlValue('ServiceAnnualValue'))
            || (this.context.hasControlChanged('AnnualValueChange') && this.context.getRawControlValue('AnnualValueChange'))
            || (this.context.hasControlChanged('ProductCode') && this.context.getRawControlValue('ProductCode'))
            || (this.context.hasControlChanged('FOCProposedAnnualValue') && this.context.getRawControlValue('FOCProposedAnnualValue')))) { // SRS 041;
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetNextInvoiceValue,';
            this.context.pageParams.blnContractNumber = true;
            this.context.pageParams.blnPremiseNumber = true;
            this.context.pageParams.blnProductCode = true;
            this.context.pageParams.blnLastChangeEffectDate = true;
            this.context.pageParams.blnServiceAnnualValue = true;
            this.context.pageParams.blnNextInvoiceStartDate = true;
            if (this.context.getRawControlValue('NextInvoiceEndDate')) {
                this.context.pageParams.blnNextInvoiceEndDate = true;
            }
        }
        //Get Service Employee/Seq No if( Branch service area code chngd;
        if (this.context.hasControlChanged('BranchServiceAreaCode') &&
            this.context.getRawControlValue('BranchServiceAreaCode')) {
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetServiceAreaDefaults,';
            this.context.pageParams.blnServiceBranchNumber = true;
            this.context.pageParams.blnBranchServiceAreaCode = true;
            this.context.pageParams.blnContractNumber = true;
            this.context.pageParams.blnPremiseNumber = true;
            //Reset Seq No if( changing Area;
            this.context.setControlValue('BranchServiceAreaSeqNo', '0000');
        }
        if (this.context.hasControlChanged('EntitlementPricePerUnit') && this.context.getRawControlValue('EntitlementPricePerUnit')) {
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'WarnEntitlementPrice,';
            this.context.pageParams.blnEntitlementPrice = true;
        }
        if (this.context.hasControlChanged('UpliftTemplateNumber') && this.context.getRawControlValue('UpliftTemplateNumber')) {
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetUpliftTemplateName,';
            this.context.pageParams.blnUpliftTemplateNumber = true;
        }
        if ((this.context.hasControlChanged('EntitlementAnnualQuantity') &&
            this.context.getRawControlValue('EntitlementAnnualQuantity'))
            || (this.context.hasControlChanged('EntitlementNextAnnualQuantity') &&
                this.context.getRawControlValue('EntitlementNextAnnualQuantity'))) { // #19864;
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'WarnEntitlementQuantity,';
            this.context.pageParams.blnEntitlementQuantity = true;
        }
        if (this.context.hasControlChanged('TrialPeriodEndDate') &&
            this.context.getRawControlValue('TrialPeriodEndDate')) {
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetTrialPeriodInvoiceDates,';
            this.context.pageParams.blnContractNumber = true;
            this.context.pageParams.blnTrialPeriodEndDate = true;
            this.context.pageParams.blnServiceCommenceDate = true;
        }
        if (this.context.hasControlChanged('BusinessOriginCode') &&
            this.context.getRawControlValue('BusinessOriginCode')) { // 29/02/08 PG #34956;
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetBusinessOrigin,';
            this.context.pageParams.blnBusinessOrigin = true;
        }
        if (this.context.hasControlChanged('RMMCategoryCode') &&
            this.context.getRawControlValue('RMMCategoryCode')) { // 29/02/08 PG #34956;
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetRMMCategory,';
            this.context.pageParams.blnRMMCategory = true;
        }
        if (this.context.hasControlChanged('LeadEmployee') &&
            this.context.fieldHasValue('LeadEmployee')) {
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetLeadEmployee,';
            this.context.pageParams.blnLeadEmployee = true;
        }
        if (this.context.pageParams.uiDisplay.tdEntitlement
            && (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate || this.context.riMaintenance.CurrentMode === MntConst.eModeAdd)
            && this.context.hasControlChanged('ServiceQuantity')) {
            this.context.setControlValue('EntitlementAnnualQuantity', this.context.getRawControlValue('ServiceQuantity'));
            this.context.setControlValue('EntitlementNextAnnualQuantity', this.context.getRawControlValue('ServiceQuantity'));
            this.context.iCABSAServiceCoverMaintenance2.CalculateEntitlementServiceQuantity();
        }
        if (this.context.pageParams.uiDisplay.trServiceQuantity
            && (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate || this.context.riMaintenance.CurrentMode === MntConst.eModeAdd)
            && this.context.hasControlChanged('EntitlementAnnualQuantity')) {
            this.context.setControlValue('ServiceQuantity', this.context.getRawControlValue('EntitlementAnnualQuantity'));
        }
        if (this.context.pageParams.blnUseVisitCycleValue) {
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetVisitCycleInWeeks,';
        }
        if (this.context.hasControlChanged('Contract') ||
            this.context.getRawControlValue('ContractNumber') ||
            !this.context.getRawControlValue('ContractTrialPeriodInd')) {
            this.context.pageParams.blnContractTrialInd = true;
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'GetContractTrialInd,';
        }

        //* 2. Bld the request && execute it;
        if (this.context.pageParams.strFunctions) {
            this.context.riMaintenance.CustomBusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=' + this.context.utils.Left(this.context.pageParams.strFunctions,
                this.context.utils.len(this.context.pageParams.strFunctions) - 1) + this.context.pageParams.strPostVars;
            this.context.riMaintenance.CBORequestAddCS('BusinessCode', MntConst.eTypeCode);

            if (this.context.pageParams.blnContractNumber && this.context.getRawControlValue('ContractNumber')) {
                this.context.riMaintenance.CBORequestAdd('ContractNumber');
            }
            if (this.context.pageParams.blnPremiseNumber && this.context.getRawControlValue('PremiseNumber')) {
                this.context.riMaintenance.CBORequestAdd('PremiseNumber');
            }
            if (this.context.pageParams.blnProductCode && this.context.getRawControlValue('ProductCode')) {
                this.context.riMaintenance.CBORequestAdd('ProductCode');
            }
            if (this.context.pageParams.blnNewPremise && this.context.getRawControlValue('NewPremise')) {
                this.context.riMaintenance.CBORequestAdd('NewPremise');
            }
            if (this.context.pageParams.blnFieldShowList && this.context.getRawControlValue('FieldShowList')) {
                this.context.riMaintenance.CBORequestAdd('FieldShowList');
            }
            if (this.context.pageParams.blnServiceCommenceDate && this.context.getRawControlValue('ServiceCommenceDate')) {
                this.context.riMaintenance.CBORequestAdd('ServiceCommenceDate');
            }
            if (this.context.pageParams.blnServiceQuantity && this.context.getRawControlValue('ServiceQuantity')) {
                this.context.riMaintenance.CBORequestAdd('ServiceQuantity');
            }
            if (this.context.pageParams.blnLastChangeEffectDate && this.context.getRawControlValue('LastChangeEffectDate')) {
                this.context.riMaintenance.CBORequestAdd('LastChangeEffectDate');
            }
            if (this.context.pageParams.blnNextInvoiceStartDate && this.context.getRawControlValue('NextInvoiceStartDate')) {
                this.context.riMaintenance.CBORequestAdd('NextInvoiceStartDate');
            }
            if (this.context.pageParams.blnNextInvoiceEndDate && this.context.getRawControlValue('NextInvoiceEndDate')) {
                this.context.riMaintenance.CBORequestAdd('NextInvoiceEndDate');
            }
            if (this.context.pageParams.blnServiceAnnualValue && this.context.getRawControlValue('ServiceAnnualValue')) {
                this.context.riMaintenance.CBORequestAdd('ServiceAnnualValue');
            }
            if (this.context.pageParams.blnServiceBranchNumber && this.context.getRawControlValue('ServiceBranchNumber')) {
                this.context.riMaintenance.CBORequestAdd('ServiceBranchNumber');
            }
            if (this.context.pageParams.blnBranchServiceAreaCode && this.context.getRawControlValue('BranchServiceAreaCode')) {
                this.context.riMaintenance.CBORequestAdd('BranchServiceAreaCode');
            }
            if (this.context.pageParams.blnAnnualValueChange && this.context.getRawControlValue('AnnualValueChange')) {
                this.context.riMaintenance.CBORequestAdd('AnnualValueChange');
            }
            if (this.context.pageParams.blnEntitlementPrice && this.context.getRawControlValue('EntitlementPricePerUnit')) {
                this.context.riMaintenance.CBORequestAdd('EntitlementPricePerUnit');
            }
            if (this.context.pageParams.blnEntitlementQuantity) {
                if (this.context.getRawControlValue('EntitlementAnnualQuantity')) {
                    this.context.riMaintenance.CBORequestAdd('EntitlementAnnualQuantity');
                }
                if (this.context.getRawControlValue('EntitlementNextAnnualQuantity')) {
                    this.context.riMaintenance.CBORequestAdd('EntitlementNextAnnualQuantity');
                }
            }
            if (this.context.pageParams.blnServiceSpecialInstructions && this.context.getRawControlValue('ServiceSpecialInstructions')) {
                this.context.riMaintenance.CBORequestAdd('ServiceSpecialInstructions');
            }
            if (this.context.pageParams.blnTrialPeriodEndDate && this.context.getRawControlValue('TrialPeriodEndDate')) {
                this.context.riMaintenance.CBORequestAdd('TrialPeriodEndDate');
            }
            if (this.context.pageParams.blnSeasonalTemplateNumber && this.context.getRawControlValue('SeasonalTemplateNumber')) {
                this.context.riMaintenance.CBORequestAdd('SeasonalTemplateNumber');
            }
            if (this.context.pageParams.blnBusinessOrigin && this.context.getRawControlValue('BusinessOriginCode')) {
                this.context.riMaintenance.CBORequestAdd('BusinessOriginCode');
            }
            if (this.context.pageParams.blnContractTrialInd && this.context.getRawControlValue('ContractNumber')) {
                this.context.riMaintenance.CBORequestAdd('ContractPeriodTrialInd');
            }
            if (this.context.pageParams.blnWasteTransferTypeCode && this.context.getRawControlValue('WasteTransferTypeCode')) {
                this.context.riMaintenance.CBORequestAdd('WasteTransferTypeCode');
            }
            if (this.context.pageParams.blnLeadEmployee && this.context.getRawControlValue('LeadEmployee')) {
                this.context.riMaintenance.CBORequestAdd('LeadEmployee');
            }
            if (this.context.pageParams.blnMinimumDuration && this.context.getRawControlValue('MinimumDuration')) {
                this.context.riMaintenance.CBORequestAdd('MinimumDuration');
            }
            if (this.context.pageParams.blnExcludeUnConfirmedValues && this.context.getRawControlValue('PendingReduction')) {
                this.context.riMaintenance.CBORequestAdd('PendingReduction');
            }
            if (this.context.pageParams.blnUpliftTemplateNumber && this.context.getRawControlValue('UpliftTemplateNumber')) {
                this.context.riMaintenance.CBORequestAdd('UpliftTemplateNumber');
            }
            if (this.context.pageParams.blnRMMCategory && this.context.getRawControlValue('RMMCategoryCode')) {
                this.context.riMaintenance.CBORequestAdd('RMMCategoryCode');
            }
            // 05/05/05 PG SRS 41 Add FoC fields, required for several of the functions;


            if (this.context.isControlChecked('chkFOC')) {
                this.context.riMaintenance.CBORequestAdd('chkFOC');
                // Allow CBORequest to proceed even if( FOC fields haven//t been entered, in case the user;
                // ) { unchecks the FOC flag (leaving potentially invalid invoice dts for example);
                if (this.context.getRawControlValue('FOCInvoiceStartDate')) {
                    this.context.riMaintenance.CBORequestAdd('FOCInvoiceStartDate');
                }
                if (this.context.getRawControlValue('FOCProposedAnnualValue')) {
                    this.context.riMaintenance.CBORequestAdd('FOCProposedAnnualValue');
                }
            }
            this.context.riMaintenance.CBORequestExecute(this.context, function (data: any): any {
                if (data.hasError) {
                    this.context.showAlert(data.errorMessage, 0, data.fullError);
                } else {
                    //this.context.riMaintenance.renderResponseForCtrl(this.context, data);
                    for (let id in data) {
                        if (id) {
                            let respData = data[id];
                            if (respData) {
                                let ctrlType = this.context.riMaintenance.getControlType(this.context.controls, id, 'type');
                                if (ctrlType) {
                                    respData = this.context.riMaintenance.respDataFormatUI(ctrlType, respData);
                                    if (ctrlType === MntConst.eTypeDate) {
                                        this.context.riMaintenance.updateDateField(this.context, respData, id);
                                    }
                                    this.context.riExchange.updateCtrl(this.context.controls, id, 'value', respData);
                                    this.context.setControlValue(id, respData);
                                } else {
                                    this.context.riExchange.updateCtrl(this.context.controls, id, 'value', respData);
                                    this.context.setControlValue(id, respData);
                                }
                            }
                        }
                    }
                    if (this.context.initialLoad && this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                        this.context.setControlValue('ClosedTemplateName', this.context.getControlValue('TemplateName'));
                        this.context.setControlValue('TemplateName', '');
                    }
                }
                this.iCABSAServiceCoverMaintenance4.postProcessing();

            }, showSpinner);
        } else {
            this.context.iCABSAServiceCoverMaintenance4.postProcessing();
        }
    }

    public postProcessing(): void {
        if (this.context.pageParams.strFunctions) {
            if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                (this.context.isControlChecked('DisplayLevelInd') || this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                this.context.isControlChecked('VisitTriggered')) {
                if (this.context.pageParams.blnServiceAnnualValue && this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                    this.context.iCABSAServiceCoverMaintenance6.CalculateUnitValue();
                }
            }
            if (this.context.getRawControlValue('PendingReduction')) {
                this.context.setControlValue('PendingReduction', '');
            }
            this.context.riMaintenance.CustomBusinessObject = this.context.pageParams.strEntryProcedure;

            //* 3. Do any after request processing;
            //override o/standing install to 0;
            if (this.context.pageParams.vbEnableTrialPeriodServices) {   // SRS040A Use syschar flag;
                if (this.context.isControlChecked('ContractTrialPeriodInd')) {
                    this.context.pageParams.uiDisplay.tdTrialPeriodInd = false;
                    this.context.pageParams.uiDisplay.tdContractTrialPeriodInd = true;
                    this.context.setControlValue('ContractTrialPeriodInd', true);
                    this.context.riMaintenance.DisableInput('ContractTrialPeriodInd');
                } else {
                    this.context.pageParams.dtTrialPeriodEndDateRecordValue = this.context.getRawControlValue('TrialPeriodEndDate');
                    this.context.pageParams.deTrialPeriodChargeRecordValue = this.context.getRawControlValue('TrialPeriodChargeValue');
                    this.context.pageParams.uiDisplay.tdTrialPeriodInd = true;
                    //riMaintenance.DisableInput('TrialPeriodInd');
                    this.context.pageParams.uiDisplay.tdContractTrialPeriodInd = false;
                    // richy - following call causes Strange TabOrder;
                    this.context.iCABSAServiceCoverMaintenance7.ToggleTrialPeriodStatus();
                }
            }
            if (this.context.getRawControlValue('VisitCycleInWeeks') === '0') {
                this.context.setControlValue('VisitCycleInWeeks', '');
                this.context.setControlValue('VisitsPerCycle', '');
                this.context.setControlValue('VisitCycleInWeeksOverrideNote', '');
            }
            if (this.context.getRawControlValue('PreferredDayOfWeekReasonCode') === '0') {
                this.context.setControlValue('PreferredDayOfWeekReasonCode', '');
                this.context.setDropDownComponentValue('PreferredDayOfWeekReasonCode', 'PreferredDayOfWeekReasonLangDesc');
            }
            if (this.context.hasControlChanged('VisitCycleInWeeks') || this.context.hasControlChanged('VisitsPerCycle')) {
                this.context.setControlValue('VisitCycleInWeeksOverrideNote', '');
            }
            if (this.context.pageParams.blnFieldShowList && !this.context.getRawControlValue('ErrorMessage')) {
                this.context.iCABSAServiceCoverMaintenance4.ShowFields();
            }
            //Is this product valid for stock order (product sales) ?;
            this.context.pageParams.uiDisplay.trChkStockOrder = (this.context.getRawControlValue('StockOrderAllowed') === 'yes')
                && this.context.riMaintenance.CurrentMode === MntConst.eModeAdd;
            if (this.context.hasControlChanged('SeasonalTemplateNumber')) {
                if (this.context.getRawControlValue('SeasonalTemplateNumber')) {
                    this.context.setControlValue('SeasonalBranchUpdate', true);
                    this.context.iCABSAServiceCoverMaintenance7.EnableSeasonalChanges(false);
                    this.context.iCABSAServiceCoverMaintenance7.ShowSeasonalRows();
                }
            }
            // 31/08/05 PG SRS041A;
            if (this.context.hasControlChanged('FOCInvoiceStartDate') &&
                this.context.getRawControlValue('FOCMessageText')) {
                // Warn the user that service commence dt will be chngd;
                //Call MsgBox(FOCMessageText.value, this.context.pageParams.vbOKOnly);
                this.context.showAlert(this.context.getRawControlValue('FOCMessageText'), 2);
            }
            if (this.context.pageParams.blnBusinessOrigin) {
                this.context.iCABSAServiceCoverMaintenance3.LeadEmployeeDisplay();
                if (this.context.isControlChecked('LeadInd')) {
                    //this.context.riExchange.riInputElement.focus('LeadEmployee');
                } else {
                    this.context.setControlValue('LeadEmployee', '');
                }
                this.context.iCABSAServiceCoverMaintenance3.BusinessOriginDetailDisplay();
            }
            if (this.context.pageParams.strFunctions.indexOf('InheritFromPremise,') > 0) {
                this.context.iCABSAServiceCoverMaintenance3.SetQuickWindowSetValue('P');
            }
            if (this.context.pageParams.strFunctions.indexOf('GetServiceDefaultValues,') > 0) {
                if (this.context.isControlChecked('PremiseDefaultTimesInd')) {
                    this.context.iCABSAServiceCoverMaintenance3.SetQuickWindowSetValue('P');
                    this.context.iCABSAServiceCoverMaintenance2.UpdateDefaultTimes('Premise');
                } else {
                    this.context.iCABSAServiceCoverMaintenance3.SetQuickWindowSetValue('D');
                    this.context.iCABSAServiceCoverMaintenance2.UpdateDefaultTimes('Default');
                }
            }

            this.context.iCABSAServiceCoverMaintenance3.CheckWasteTransferRequired();

            if (!this.context.pageParams.vbShowPremiseWasteTab) {
                this.context.setRequiredStatus('WasteTransferTypeCode', false);
            }
            if (this.context.pageParams.vbEnableServiceCoverDispLev && this.context.isControlChecked('DisplayLevelInd') &&
                this.context.getRawControlValue('ReplacementIncludeInd')) {
                this.context.iCABSAServiceCoverMaintenance3.SetReplacementIncInd();
            }
            if (this.context.getRawControlValue('ProductCode') &&
                this.context.hasControlChanged('ProductCode')) {
                this.context.pageParams.uiDisplay.tdLineOfService_innerhtml = this.context.getRawControlValue('LOSName');
                this.context.pageParams.uiDisplay.tdLineOfService = true;
                this.context.iCABSAServiceCoverMaintenance2.ServiceQuantityLabel();
            }
        } //this.context.pageParams.strFunctions !== true;
        if (this.context.hasControlChanged('ServiceQuantity')) {
            if (isNaN(parseInt(this.context.getRawControlValue('ServiceQuantity'), 10))) {
                this.context.setControlValue('ServiceQuantity', '0');
            }

            switch (this.context.riMaintenance.CurrentMode) {
                case MntConst.eModeAdd:
                    this.context.pageParams.blnQuantityChange = true;
                    if (this.context.isControlChecked('InstallationRequired')) {
                        if (this.context.pageParams.vbEnableServiceCoverDispLev
                            && this.context.pageParams.vbEnableDeliveryRelease
                            && this.context.isControlChecked('DisplayLevelInd')) {
                            this.context.pageParams.uiDisplay.trOutstandingInstallations = false;
                            this.context.setControlValue('OutstandingInstallations', this.context.getRawControlValue('ServiceQuantity'));
                        } else {
                            this.context.pageParams.uiDisplay.trOutstandingInstallations = true;
                            //(#45032 - set to 0 if( display level sys char is on);
                            if (!this.context.pageParams.vbDisplayLevelInstall) {
                                this.context.setControlValue('OutstandingInstallations', this.context.getRawControlValue('ServiceQuantity'));
                            } else {
                                this.context.setControlValue('OutstandingInstallations', '0');
                                this.context.pageParams.uiDisplay.trInstallationEmployee = true;
                                this.context.setRequiredStatus('InstallationEmployeeCode', true);
                            }
                        }
                        this.context.pageParams.uiDisplay.trInstallationValue = true;
                        this.context.pageParams.uiDisplay.trRemovalValue = true;
                        this.context.pageParams.SavedIncreaseQuantity = parseInt(this.context.getRawControlValue('ServiceQuantity'), 10);
                    }
                    break;
                case MntConst.eModeUpdate:
                    this.context.pageParams.blnQuantityChange = (parseInt(this.context.getRawControlValue('ServiceQuantity'), 10) > parseInt(this.context.getRawControlValue('SavedServiceQuantity'), 10))
                        || (parseInt(this.context.getRawControlValue('ServiceQuantity'), 10) < parseInt(this.context.getRawControlValue('SavedServiceQuantity'), 10));
                    if (this.context.isControlChecked('InstallationRequired')) {
                        if (!this.context.getRawControlValue('SavedServiceQuantity')) {
                            this.context.setControlValue('SavedServiceQuantity', '0');
                        }
                        if (parseInt(this.context.getRawControlValue('ServiceQuantity'), 10) >
                            parseInt(this.context.getRawControlValue('SavedServiceQuantity'), 10)) {
                            this.context.pageParams.uiDisplay.trOutstandingInstallations = !(this.context.pageParams.vbEnableServiceCoverDispLev
                                && this.context.pageParams.vbEnableDeliveryRelease
                                && this.context.isControlChecked('DisplayLevelInd'));
                            this.context.pageParams.uiDisplay.trOutstandingRemovals = false;
                            this.context.pageParams.uiDisplay.trRemovalEmployee = false;
                            //if( InstallationValue is required && qty increased, show InstallationValue fld;
                            if (this.context.getRawControlValue('FieldHideList').indexOf('InstallationValue') === -1) {
                                this.context.pageParams.uiDisplay.trInstallationValue = true;
                            }
                            //if( Qty has reduced, hide Install Employee, show Removal Employee;
                        }
                    } else {
                        if (parseInt(this.context.getRawControlValue('ServiceQuantity'), 10) < parseInt(this.context.getRawControlValue('SavedServiceQuantity'), 10)) {
                            this.context.pageParams.uiDisplay.trOutstandingInstallations = false;
                            this.context.pageParams.uiDisplay.trInstallationEmployee = false;
                            this.context.setRequiredStatus('InstallationEmployeeCode', false);
                            if (this.context.pageParams.vbEnableServiceCoverDispLev && this.context.isControlChecked('DisplayLevelInd')) {
                                this.context.pageParams.uiDisplay.trOutstandingRemovals = false;
                            } else {
                                this.context.pageParams.uiDisplay.trOutstandingRemovals = true;
                            }
                            this.context.pageParams.uiDisplay.trInstallationValue = false;  //No need for InstallationValue if( qty reduction;
                        } else {  //Qty has been changed but ) { changed back so stayed the same qty, so hide both Install/Removal Emp;
                            this.context.pageParams.uiDisplay.trOutstandingInstallations = false;
                            this.context.pageParams.uiDisplay.trInstallationEmployee = false;
                            this.context.setRequiredStatus('InstallationEmployeeCode', false);
                            this.context.pageParams.uiDisplay.trOutstandingRemovals = false;
                            this.context.pageParams.uiDisplay.trRemovalEmployee = false;
                            this.context.pageParams.uiDisplay.trInstallationValue = false;
                        }
                    }
            }
            if (this.context.pageParams.vbEnableWorkLoadIndex) {
                if (this.context.getRawControlValue('ServiceQuantity') !== '0') {
                    let a = parseInt(this.context.getRawControlValue('WorkLoadIndex'), 10);
                    let b = parseInt(this.context.getRawControlValue('ServiceQuantity'), 10);
                    this.context.setControlValue('WorkLoadIndexTotal', a * b);
                } else {
                    this.context.setControlValue('WorkLoadIndexTotal', this.context.getRawControlValue('WorkLoadIndex'));
                }
            }
        }  //ServiceQty has changed;
        //if( O/standing Installations changed ) { show Installation Employee dtls;
        if (this.context.hasControlChanged('OutstandingInstallations')
            || (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate && this.context.hasControlChanged('ServiceQuantity'))) {
            if (!this.context.getRawControlValue('OutstandingInstallations')) {
                this.context.setControlValue('OutstandingInstallations', '0');
            }
            let isOutstandingInstallations: boolean = parseInt(this.context.pageParams.SavedIncreaseQuantity, 10) >
                parseInt(this.context.getRawControlValue('OutstandingInstallations'), 10);
            this.context.pageParams.uiDisplay.trInstallationEmployee = isOutstandingInstallations;
            this.context.setRequiredStatus('InstallationEmployeeCode', isOutstandingInstallations);
        }
        //if( O/standing Removals changed ) { show Removal Employee details;
        if (this.context.hasControlChanged('OutstandingRemovals')) {
            if (!this.context.getRawControlValue('OutstandingRemovals')) {
                this.context.setControlValue('OutstandingRemovals', '0');
            }
            this.context.pageParams.uiDisplay.trRemovalEmployee = this.context.pageParams.SavedReductionQuantity > parseInt(this.context.getRawControlValue('OutstandingRemovals'), 10);
        }
        //Seasonal Service;
        if (this.context.isControlChecked('SeasonalServiceInd')) {
            // if( SeasonalTemplateNumber is blank, allow changes;
            if (this.context.hasControlChanged('SeasonalTemplateNumber')) {
                if (!this.context.getRawControlValue('SeasonalTemplateNumber')) {
                    this.context.setControlValue('SeasonalBranchUpdate', false);
                    this.context.iCABSAServiceCoverMaintenance7.EnableSeasonalChanges(true);
                    this.context.riMaintenance.DisableInput('SeasonalBranchUpdate');
                } else {
                    if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                        this.context.riMaintenance.EnableInput('SeasonalBranchUpdate');
                    }
                }
            }
            // No of seasons has changed;
            if ((this.context.hasControlChanged('NumberOfSeasons'))
                || (this.context.hasControlChanged('LastChangeEffectDate') && this.context.getRawControlValue('LastChangeEffectDate'))) {
                //if( there is no value in the input box, set it to '1';
                if (!this.context.getRawControlValue('NumberOfSeasons')) {
                    this.context.setControlValue('NumberOfSeasons', '1');
                }
                // Show correct no of rows;
                this.context.iCABSAServiceCoverMaintenance7.ShowSeasonalRows();
            }
        }
        //Enable/disable Copy From Service Cover button;
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd && (this.context.hasControlChanged('ContractNumber')
            || this.context.hasControlChanged('PremiseNumber')) &&
            this.context.getRawControlValue('ContractNumber') &&
            this.context.getRawControlValue('PremiseNumber')) {
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'cmdCopyServiceCover');
        }

        if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            if (this.context.hasControlChanged('AnnualTimeChange')) {
                //Time Format;
                this.context.pageParams.vbNegative = false;
                this.context.pageParams.vbError = false;
                this.context.pageParams.vbTimeFormat = '##00' + this.context.pageParams.vbTimeSeparator + '##';
                this.context.riExchange.riInputElement.isCorrect(this.context.uiForm, 'AnnualTimeChange');
                this.context.pageParams.vbAnnualTimeChange = this.context.getRawControlValue('AnnualTimeChange').replace(this.context.pageParams.vbTimeSeparator, '');
                if (isNaN(parseInt(this.context.pageParams.vbAnnualTimeChange, 10))
                    && this.context.pageParams.vbAnnualTimeChange) {
                    this.context.pageParams.vbError = true;
                }
                if (!this.context.pageParams.vbError && (this.context.utils.len(this.context.pageParams.vbAnnualTimeChange) < 4 || this.context.utils.len(this.context.pageParams.vbAnnualTimeChange) > 8)) {
                    this.context.pageParams.vbError = true;
                } else if (!this.context.pageParams.vbError) {
                    //this.context.setControlValue('AnnualTimeChange', 'riExchange.Functions.Formats.riformat(this.context.pageParams.vbAnnualTimeChange,this.context.pageParams.vbTimeFormat)');
                    if (!this.context.pageParams.vbError && this.context.pageParams.vbAnnualTimeChange === 0) {
                        this.context.pageParams.vbError = true;
                    }
                    if (!this.context.pageParams.vbError) {
                        this.context.pageParams.vbDurationHours = this.context.utils.mid(this.context.pageParams.vbAnnualTimeChange, 1, this.context.utils.len(this.context.pageParams.vbAnnualTimeChange) - 2);
                        this.context.pageParams.vbDurationMinutes = this.context.utils.Right(this.context.pageParams.vbAnnualTimeChange, 2);
                        if (this.context.pageParams.vbDurationHours < 0) {
                            this.context.pageParams.vbDurationHours = this.context.pageParams.vbDurationHours * -1; //Assign negative value back to a positive;
                            this.context.pageParams.vbNegative = true;
                        }
                        if (this.context.pageParams.vbDurationMinutes < 0) {
                            this.context.pageParams.vbDurationMinutes = this.context.pageParams.vbDurationMinutes * -1;
                            this.context.pageParams.vbNegative = true;
                        }
                        if (this.context.pageParams.vbDurationMinutes > 59) {
                            //this.context.pageParams.vbMsgResult = msgbox('<%=riT('Minutes Entered Cannot Be Greater Than 59')%>', this.context.pageParams.vbOKOnly + this.context.pageParams.vbExclamation, '<%=riT('Warning')%>');
                            this.context.showAlert(MessageConstant.PageSpecificMessage.vbDurationMinutes);
                            this.context.pageParams.vbError = true;
                        } else {
                            this.context.pageParams.vbAnnualTimeChangeSec = (this.context.pageParams.vbDurationHours * 60 * 60) + (this.context.pageParams.vbDurationMinutes * 60);
                            if (this.context.pageParams.vbNegative) {
                                this.context.pageParams.vbAnnualTimeChangeSec = this.context.pageParams.vbAnnualTimeChangeSec * -1;
                            }
                        }
                    }
                }

                if (!this.context.pageParams.vbError && this.context.pageParams.vbAnnualTimeChange) {
                    this.context.riExchange.riInputElement.isCorrect(this.context.uiForm, 'AnnualTimeChange');
                    this.context.riMaintenance.clear();
                    this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntryRequests.p';
                    this.context.riMaintenance.PostDataAddAction('6');
                    this.context.riMaintenance.PostDataAddFunction('AnnualTimeChange');
                    this.context.riMaintenance.PostDataAddBusiness();
                    this.context.riMaintenance.PostDataAdd('ServiceAnnualTime', this.context.pageParams.SavedServiceAnnualTime, MntConst.eTypeText);
                    this.context.PostDataAddFromField('AnnualTimeChange', MntConst.eTypeText);
                    this.context.riMaintenance.Execute(this.context, function (data: any): any {
                        this.context.setControlValue('ServiceAnnualTime', data['ServiceAnnualTime']);
                    }, 'POST');
                } else {
                    if (this.context.pageParams.vbAnnualTimeChange) {
                        this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'AnnualTimeChange', true);
                    }
                    this.context.setControlValue('ServiceAnnualTime', this.context.pageParams.SavedServiceAnnualTime);
                }
            }
        }

        if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate && this.context.pageParams.currentContractType === 'C') {
            //if( AnnualValueChange has changed, updt ServiceAnnualValue fld (only on updates && Contracts);
            if (this.context.hasControlChanged('AnnualValueChange') &&
                !this.context.getRawControlValue('LastChangeEffectDate')) {
                /*let val = parseFloat(this.context.pageParams.SavedServiceAnnualValue) + parseFloat(this.context.getRawControlValue('AnnualValueChange'));
                this.context.setControlValue('ServiceAnnualValue',
                    this.context.utils.cCur(val.toString()));*/
                //this.context.riExchange.riInputElement.isCorrect(this.context.uiForm,, 'ServiceAnnualValue');
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    (this.context.isControlChecked('DisplayLevelInd') ||
                        this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                    this.context.isControlChecked('VisitTriggered')) {
                    this.context.iCABSAServiceCoverMaintenance6.CalculateUnitValue();
                }
            }
            if (this.context.parentMode === 'ContactUpdate') {
                if (this.context.riExchange.URLParameterContains('PendingDeletion')) {
                    if (this.context.utils.cCur(this.context.getRawControlValue('AnnualValueChange')) < 0) {
                        this.context.setRequiredStatus('LostBusinessCode', true);
                        this.context.setRequiredStatus('LostBusinessDetailCode', true);
                    } else {
                        this.context.setRequiredStatus('LostBusinessCode', false);
                        this.context.setRequiredStatus('LostBusinessDetailCode', false);
                    }
                }
            }
            //if( Qty/Freq/Val change, show LastChangeEffectDt (only on updt && Contracts);
            if ((this.context.hasControlChanged('ServiceQuantity')
                || this.context.hasControlChanged('ServiceVisitFrequency')
                || this.context.hasControlChanged('ServiceAnnualValue')
                || this.context.hasControlChanged('AnnualValueChange'))) {
                if (this.context.IsNumeric('ServiceQuantity')) {
                    this.context.pageParams.CompServiceQuantity = this.context.CInt('ServiceQuantity');
                } else {
                    this.context.pageParams.CompServiceQuantity = 0;
                }
                if (this.context.IsNumeric('ServiceVisitFrequency')) {
                    this.context.pageParams.CompServiceVisitFrequency = this.context.CInt('ServiceVisitFrequency');
                } else {
                    this.context.pageParams.CompServiceVisitFrequency = 0;
                }
                if (this.context.IsNumeric('ServiceAnnualValue')) {
                    this.context.pageParams.CompServiceAnnualValue = this.context.cCur('ServiceAnnualValue');
                } else {
                    this.context.pageParams.CompServiceAnnualValue = 0;
                }
                if (this.context.pageParams.CompServiceAnnualValue !== this.context.utils.cCur(this.context.pageParams.SavedServiceAnnualValue)
                    || this.context.pageParams.CompServiceVisitFrequency !== this.context.utils.CInt(this.context.pageParams.SavedServiceVisitFrequency)
                    || this.context.pageParams.CompServiceQuantity !== this.context.CInt('SavedServiceQuantity')) {
                    if (this.context.riExchange.riInputElement.isReadOnly(this.context.uiForm, 'LastChangeEffectDate')) {
                        this.context.pageParams.uiDisplay.trEffectiveDate = true;
                        this.context.setControlValue('LastChangeEffectDate', true);
                    }
                    if (this.context.hasControlChanged('ServiceAnnualValue')
                        || this.context.hasControlChanged('AnnualValueChange')) {
                        this.context.pageParams.uiDisplay.trEffectiveDate = true;
                    }
                }
            }
        }  //Mode ===Updt && Contracts;

        if ((this.context.hasControlChanged('ServiceQuantity')
            || this.context.hasControlChanged('ServiceVisitFrequency')
            || this.context.hasControlChanged('ServiceAnnualValue')
            || this.context.hasControlChanged('AnnualValueChange'))) {
            //SJ #35942 Updt MonthlyUnitPrice whenever Qty/Freq/Value is updtd;
            this.context.pageParams.vbQty = 1;
            this.context.pageParams.vbFreq = 1;

            if (this.context.pageParams.vbEnableMonthlyUnitPrice && this.context.getRawControlValue('ServiceAnnualValue')) {
                if (this.context.getRawControlValue('ServiceQuantity')
                    && this.context.getRawControlValue('ServiceQuantity') !== '0') {
                    this.context.pageParams.vbQty = this.context.CInt('ServiceQuantity');
                } else {
                    this.context.pageParams.vbQty = 1;
                }

                if (this.context.getRawControlValue('ServiceVisitFrequency')
                    && this.context.getRawControlValue('ServiceVisitFrequency') !== '0') {
                    this.context.pageParams.vbFreq = this.context.CInt('ServiceVisitFrequency');
                } else {
                    this.context.pageParams.vbFreq = 1;
                }

                if (this.context.pageParams.vbFreq > 12) {
                    this.context.pageParams.vbFreq = 12;
                }

                this.context.pageParams.vbVal = parseFloat(this.context.getControlValue('ServiceAnnualValue'));
                this.context.setControlValue('MonthlyUnitPrice',
                    (this.context.pageParams.vbVal / this.context.pageParams.vbQty / this.context.pageParams.vbFreq));
            }
        }

        //Calc Entitlement Service Qty;
        if (this.context.pageParams.uiDisplay.trEntitlementServiceQuantity
            && (this.context.hasControlChanged('ServiceVisitFrequency')
                || this.context.hasControlChanged('EntitlementAnnualQuantity'))) {
            this.context.iCABSAServiceCoverMaintenance2.CalculateEntitlementServiceQuantity();
        }
        this.context.pageParams.uiDisplay.tdCustomerInfo = this.context.isControlChecked('CustomerInfoAvailable');
        this.context.pageParams.uiDisplay.tdNationalAccount = this.context.isControlChecked('NationalAccountChecked') &&
            this.context.isControlChecked('NationalAccount');

        if (this.context.getRawControlValue('ProductCode') &&
            this.context.getRawControlValue('AverageWeight')) {
            this.context.iCABSAServiceCoverMaintenance3.ShowAverageWeight(false);
        }
        //This will default the indicator but needs to be run after show fields;
        if (this.context.pageParams.uiDisplay.trDOWSentricon
            && this.context.riMaintenance.CurrentMode === MntConst.eModeAdd
            && this.context.getRawControlValue('ProductCode')) {
            this.context.setControlValue('DOWSentriconInd', true);
            this.context.iCABSAServiceCoverMaintenance7.DOWSentriconToggle();
        }
        if (this.context.getRawControlValue('ErrorMessage')) {
            //riExchange.Functions.Information.Description, this.context.getRawControlValue('ErrorMessage'));
            //this.context.riExchange.Information.display();
            this.context.showAlert(this.context.getRawControlValue('ErrorMessage'), 3);
            this.context.setControlValue('ErrorMessage', '');
            this.context.saveClicked = false;
        }
        for (let key in this.context.uiForm.controls) {
            if (key && this.context.uiForm.controls.hasOwnProperty(key) &&
                this.context.uiForm.controls[key].dirty) {
                this.context.uiForm.controls[key].markAsPristine();
                if (!this.context.uiForm.controls[key].invalid &&
                    this.context.uiForm.controls[key].touched && this.context.riMaintenance.CurrentMode !== MntConst.eModeAdd) {
                    this.context.uiForm.controls[key].markAsUntouched();
                }
            }
        }
        if (this.context.saveClicked) {
            this.context.saveClicked = false;
            //this.context.iCABSAServiceCoverMaintenance7.saveRecord();
            this.context.renderTab(1);
            let msgText: Array<string> = [];
            let controlValue: any = this.context.getControlValue('ServiceAnnualValue');
            if (isNaN(this.context.pageParams.OldServiceAnnualValue)) {
                this.context.pageParams.OldServiceAnnualValue = this.context.globalize.parseCurrencyToFixedFormat(this.context.pageParams.OldServiceAnnualValue);
            }
            if (controlValue !== (this.context.pageParams.OldServiceAnnualValue * 1)) {
                msgText.push('The annual value of this service cover has been updated!');
                msgText.push('This change will affect the amounts on all future invoices for this customer from the chosen effective date.');
                msgText.push('Please review this change before proceeding by clicking the Cancel button.');
                msgText.push('Alternatively, If the change is correct, please click the Confirm button');
                msgText.push(' ');
                msgText.push(' ');
                msgText.push(' ');
            }
            msgText.push(MessageConstant.Message.ConfirmRecord);
            this.context.showDialog(msgText,
                this.context.iCABSAServiceCoverMaintenance7.saveRecord, null, msgText.length > 1);
        } else if (this.context.initialLoad) {
            this.context.initialLoad = false;
            this.context.branchServiceAreaSearchParams['ServiceBranchNumber'] = this.context.getControlValue('ServiceBranchNumber');
            this.context.branchServiceAreaSearchParams['BranchName'] = this.context.getControlValue('BranchName');
            this.context.branchServiceAreaSearchParamsVP['ServiceBranchNumber'] = this.context.getControlValue('ServiceBranchNumber');
            this.context.branchServiceAreaSearchParamsVP['BranchName'] = this.context.getControlValue('BranchName');
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                this.context.iCABSAServiceCoverMaintenance1.riMaintenance_AfterFetch();
            } else {
                this.context.iCABSAServiceCoverMaintenance2.riMaintenanceBeforeUpdate();
            }
        } else {
            if (this.context.riMaintenance.CurrentMode !== MntConst.eModeAdd) {
                this.enableFields();
            }
        }
    }

    public enableFields(): void {
        if (this.context.pageParams.currentContractType !== 'J') {
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelServiceBasis');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'cmdDiaryView');
        }
    }

    public ShowFields(): void {
        this.context.pageParams.blnHideOutstandingInstalls = true;
        this.context.pageParams.spanProductCodeLabel_innertext = '';
        //if( Value is required on the Product ) { show values;
        this.context.pageParams.blnValueRequired = this.context.InStr('FieldShowList', 'ServiceAnnualValue') !== -1;
        this.context.pageParams.uiDisplay.trWeighingRequiredInd = this.context.InStr('FieldShowList', 'WeighingRequiredInd') !== -1;
        if (this.context.InStr('FieldShowList', 'SubjectToUplift') !== -1 ||
            this.context.getRawControlValue('SubjectToUplift')) {
            this.context.pageParams.uiDisplay.trUplift = true;
            if (!this.context.getRawControlValue('SubjectToUplift')) {
                this.context.setControlValue('SelSubjectToUplift', 'N');
                this.context.iCABSAServiceCoverMaintenance8.SelSubjectToUplift_onChange();
            } else {
                this.context.setControlValue('SelSubjectToUplift',
                    this.context.getRawControlValue('SubjectToUplift'));
                this.context.iCABSAServiceCoverMaintenance8.SelSubjectToUplift_onChange();
                this.context.iCABSAServiceCoverMaintenance8.GetUpliftStatus();
                this.context.pageParams.tdUpliftStatus_backgroundcolor = 'white';
            }
        } else {
            this.context.pageParams.uiDisplay.trUplift = false;
            this.context.pageParams.uiDisplay.trUpliftCalendar = false;
            this.context.pageParams.tdUpliftStatus_InnerText = '';
            this.context.pageParams.tdUpliftStatus_backgroundcolor = '';
        }
        let isInstallationValue: boolean = this.context.InStr('FieldShowList', 'InstallationValue') !== -1
            && this.context.InStr('FieldShowList', 'ServiceQuantity') !== -1;
        this.context.pageParams.uiDisplay.trInstallationValue = isInstallationValue;
        this.context.pageParams.uiDisplay.trRemovalValue = isInstallationValue;
        let isServiceQuantity: boolean = this.context.pageParams.vbEnableDeliveryRelease &&
            this.context.isControlChecked('DeliveryConfirmationInd') &&
            this.context.pageParams.currentContractType === 'C'
            && this.context.InStr('FieldShowList', 'ServiceQuantity') !== -1 &&
            this.context.InStr('FieldShowList', 'ServiceAnnualValue') !== -1 &&
            !this.context.riExchange.URLParameterContains('PendingReduction');
        this.context.pageParams.uiDisplay.trUnConfirmedLabel = isServiceQuantity;
        this.context.pageParams.uiDisplay.trUnConfirmedEffectiveDate = isServiceQuantity;
        this.context.pageParams.uiDisplay.trUnconfirmedDeliveryQty = isServiceQuantity;
        this.context.pageParams.uiDisplay.trUnconfirmedDeliveryValue = isServiceQuantity;
        if (this.context.pageParams.vbEnableInstallsRemovals) {
            if (this.context.InStr('FieldShowList', 'OutstandingInstallations') !== -1) {
                this.context.pageParams.blnHideOutstandingInstalls = false;
            }
        }
        if (this.context.InStr('FieldShowList', 'ServiceQuantity') === -1) {
            this.context.pageParams.uiDisplay.trServiceQuantity = false;
            this.context.pageParams.blnHideOutstandingInstalls = true;
            this.context.setRequiredStatus('ServiceQuantity', false);
        } else {
            this.context.pageParams.uiDisplay.trServiceQuantity = true;
            if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                this.context.isControlChecked('DisplayLevelInd') &&
                this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate &&
                this.context.riExchange.URLParameterContains('PendingReduction')) {
                this.context.setRequiredStatus('ServiceQuantity', false);
            } else {
                this.context.setRequiredStatus('ServiceQuantity', true);
            }
        }
        let isServiceVisitFrequency: boolean = this.context.InStr('FieldShowList', 'ServiceVisitFrequency') === -1;
        this.context.pageParams.uiDisplay.trServiceVisitAnnivDate = !isServiceVisitFrequency;
        this.context.pageParams.uiDisplay.trServiceVisitFrequency = !isServiceVisitFrequency;
        this.context.setRequiredStatus('ServiceVisitFrequency', !isServiceVisitFrequency);
        this.context.setRequiredStatus('VisitCycleInWeeksOverrideNote', false);
        let isVisitCycleInWeeks: boolean = this.context.InStr('FieldShowList', 'VisitCycleInWeeks') === -1 || this.context.pageParams.currentContractType === 'J' || !this.context.pageParams.blnUseVisitCycleValues;
        this.context.pageParams.uiDisplay.trServiceVisitFrequencyCopy = !isVisitCycleInWeeks;
        this.context.pageParams.uiDisplay.trServiceVisitCycleFields1 = !isVisitCycleInWeeks;
        this.context.pageParams.uiDisplay.trServiceVisitCycleFields2 = !isVisitCycleInWeeks;
        this.context.pageParams.uiDisplay.trServiceVisitCycleFields3 = !isVisitCycleInWeeks;
        this.context.pageParams.uiDisplay.trStaticVisit = !isVisitCycleInWeeks;
        this.context.setRequiredStatus('VisitCycleInWeeks', !isVisitCycleInWeeks);
        this.context.setRequiredStatus('VisitsPerCycle', !isVisitCycleInWeeks);
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd || (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate && this.context.pageParams.blnAccess)) {
            if (this.context.InStr('FieldShowList', 'ServiceAnnualValue') === -1) {
                this.context.pageParams.uiDisplay.trServiceAnnualValue = false;
                this.context.setRequiredStatus('ServiceAnnualValue', false);
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    (this.context.isControlChecked('DisplayLevelInd') ||
                        this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                    this.context.isControlChecked('VisitTriggered')) {
                    this.context.pageParams.uiDisplay.trUnitValue = false;
                }
            } else {
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    (this.context.isControlChecked('DisplayLevelInd') ||
                        this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                    this.context.isControlChecked('VisitTriggered')) {
                    this.context.pageParams.uiDisplay.trUnitValue = true;
                }
                this.context.pageParams.uiDisplay.trServiceAnnualValue = true;
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    this.context.isControlChecked('DisplayLevelInd') &&
                    this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate &&
                    this.context.riExchange.URLParameterContains('PendingReduction')) {
                    this.context.setRequiredStatus('ServiceAnnualValue', false);
                } else {
                    this.context.setRequiredStatus('ServiceAnnualValue', true);
                }
            }
            if (this.context.InStr('FieldShowList', 'AnnualValueChange') === -1) {
                this.context.pageParams.uiDisplay.trAnnualValueChange = false;
                this.context.setRequiredStatus('AnnualValueChange', false);
                this.context.pageParams.valueChange = this.context.controls[27].required = false;
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    (this.context.isControlChecked('DisplayLevelInd') ||
                        this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                    this.context.isControlChecked('VisitTriggered')) {
                    this.context.pageParams.uiDisplay.trUnitValueChange = false;
                }
            } else if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    (this.context.isControlChecked('DisplayLevelInd') ||
                        this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                    this.context.isControlChecked('VisitTriggered')) {
                    this.context.pageParams.uiDisplay.trUnitValueChange = true;
                }
                if (this.context.pageParams.currentContractType === 'C') {
                    this.context.pageParams.uiDisplay.trAnnualValueChange = true;
                    this.context.setRequiredStatus('AnnualValueChange', true);
                }
                this.context.pageParams.valueChange = this.context.controls[27].required = true;
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate &&
                    this.context.isControlChecked('DisplayLevelInd') &&
                    this.context.riExchange.URLParameterContains('PendingReduction')) {
                    this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'AnnualValueChange');
                    if (this.context.isControlChecked('VisitTriggered')) {
                        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'UnitValueChange');
                    }
                }
            }
        }
        if (this.context.InStr('FieldShowList', 'RequireAnnualTimeInd') === -1) {
            this.context.setRequiredStatus('ServiceAnnualTime', false);
            this.context.riExchange.updateCtrl(this.context.controls, 'ServiceAnnualTime', 'required', false);
            this.context.pageParams.uiDisplay.trAnnualTime = false;
        } else {
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                this.context.setRequiredStatus('ServiceAnnualTime', true);
                this.context.riExchange.updateCtrl(this.context.controls, 'ServiceAnnualTime', 'required', true);
            } else {
                this.context.setRequiredStatus('ServiceAnnualTime', false);
                this.context.riExchange.updateCtrl(this.context.controls, 'ServiceAnnualTime', 'required', false);
            }
            this.context.pageParams.uiDisplay.trAnnualTime = true;
            this.context.pageParams.uiDisplay.trStandardTreatmentTime = false;
            this.context.setRequiredStatus('StandardTreatmentTime', false);
        }
        if (this.context.InStr('FieldShowList', 'EFKReplacementMonth') === -1) {
            this.context.pageParams.uiDisplay.trEFKReplacementMonth = false;
            this.context.setRequiredStatus('EFKReplacementMonth', false);
        } else {
            this.context.pageParams.uiDisplay.trEFKReplacementMonth = true;
            if (this.context.pageParams.currentContractType === 'J') {
                this.context.setRequiredStatus('EFKReplacementMonth', false);
            } else {
                this.context.setRequiredStatus('EFKReplacementMonth', true);
            }
        }
        //if( Waste Transfer Fee is required ) { show value && checkboxes;
        if (this.context.InStr('FieldShowList', 'WasteTransferChargeValue') === -1 ||
            !this.context.getRawControlValue('ProductCode') || !this.context.pageParams.vWasteTransferReq) {
            this.context.pageParams.uiDisplay.tdWasteTransfer = false;
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                this.context.pageParams.uiDisplay.trAnnualValueChange = false;
            }
        } else {
            this.context.pageParams.uiDisplay.tdWasteTransfer = true;
            this.context.disableControl('WasteTransferUpdateValueInd', false);
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                this.context.pageParams.uiDisplay.trAnnualValueChange = true;
            }
        }

        this.context.pageParams.uiDisplay.trDeliveryConfirmation = this.context.pageParams.vbSuspendSalesStatPortFig
            && this.context.InStr('FieldShowList', 'DeliveryConfirmation') !== -1
            && this.context.riExchange.getCurrentContractType() === 'C';
        //Show Inv Start/End/Val fields if( value required;
        this.context.pageParams.uiDisplay.trInvoiceType = this.context.pageParams.blnValueRequired;
        this.context.pageParams.uiDisplay.trInvoiceStartDate = this.context.pageParams.blnValueRequired;
        this.context.pageParams.uiDisplay.trInvoiceEndDate = this.context.pageParams.blnValueRequired;
        this.context.pageParams.uiDisplay.trInvoiceValue = this.context.pageParams.blnValueRequired;
        this.context.pageParams.uiDisplay.trForwardDateChangeInd = this.context.pageParams.blnValueRequired;
        this.context.pageParams.uiDisplay.trZeroValueIncInvoice = this.context.pageParams.blnValueRequired;
        //Only show Reneg checkbox if( Contract && value required;
        this.context.pageParams.uiDisplay.trChkRenegContract = this.context.pageParams.blnValueRequired && this.context.pageParams.currentContractType === 'C' &&
            (!this.context.riExchange.URLParameterContains('PendingReduction') || this.context.riExchange.URLParameterContains('PendingDeletion'));
        if (this.context.pageParams.blnHideOutstandingInstalls && this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.pageParams.uiDisplay.trOutstandingInstallations = false;
            this.context.pageParams.uiDisplay.trInstallationEmployee = false;
            this.context.pageParams.uiDisplay.trOutstandingRemovals = false;
            this.context.setRequiredStatus('InstallationEmployeeCode', false);
            this.context.setRequiredStatus('RemovalEmployeeCode', false);
        }
        //if( Ann Entitlement required && valid on this product ) { enable checkbox;
        this.context.setControlValue('EntitlementRequiredInd', !(!this.context.pageParams.vbEnableEntitlement ||
            this.context.InStr('FieldShowList', 'EntitlementRequiredInd') === -1 ||
            !this.context.getRawControlValue('ProductCode')));
        this.context.pageParams.uiDisplay.tdLostBusiness = this.context.riExchange.URLParameterContains('PendingReduction') || this.context.riExchange.URLParameterContains('PendingDeletion');
        this.context.pageParams.uiDisplay.trAverageUnitValue = this.context.InStr('FieldShowList', 'ServiceAnnualValue') > -1 && this.context.InStr('FieldShowList', 'ServiceQuantity') > -1;
        let ismultipleTaxRates: boolean = this.context.pageParams.vbEnableMultipleTaxRates && this.context.InStr('FieldShowList', 'MultipleTaxRates') > -1;
        this.context.pageParams.uiDisplay.trMultipleTaxRates = ismultipleTaxRates;
        this.context.pageParams.uiDisplay.trTaxHeadings = ismultipleTaxRates;
        this.context.pageParams.uiDisplay.trTaxMaterials = ismultipleTaxRates;
        this.context.pageParams.uiDisplay.trTaxLabour = ismultipleTaxRates;
        this.context.pageParams.uiDisplay.trTaxReplacement = ismultipleTaxRates;
        this.context.pageParams.uiDisplay.trConsolidateEqualTaxRates = ismultipleTaxRates;
        if (ismultipleTaxRates) {
            this.context.iCABSAServiceCoverMaintenance3.clickMultipleTaxRatesOnLoad();
        }
        if (this.context.pageParams.vbEnableProductLinking && this.context.InStr('FieldShowList', 'LinkedProduct') > -1) {
            this.context.pageParams.uiDisplay.trLinkedProduct = true;
            this.context.pageParams.uiDisplay.trLinkedServiceCover = true;
            this.context.iCABSAServiceCoverMaintenance7.BuildMenuOptions();
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                this.context.riMaintenance.DisableInput('LinkedProductCode');
                this.context.setRequiredStatus('LinkedProductCode', false);
            } else if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                this.context.setRequiredStatus('LinkedProductCode', this.context.InStr('FieldShowList', 'LinkedProductRequired') > -1);
                this.context.riMaintenance.EnableInput('LinkedProductCode');
            }
        } else {
            this.context.setRequiredStatus('LinkedProductCode', false);
            this.context.pageParams.uiDisplay.trLinkedProduct = false;
            this.context.pageParams.uiDisplay.trLinkedServiceCover = false;
            this.context.iCABSAServiceCoverMaintenance7.BuildMenuOptions();
        }
        this.context.iCABSAServiceCoverMaintenance3.LeadEmployeeDisplay();
        this.context.iCABSAServiceCoverMaintenance3.BusinessOriginDetailDisplay();
        this.context.iCABSAServiceCoverMaintenance3.ToggleEntitlementRequired();
        if (this.context.pageParams.vbDefaultStockReplenishment && this.context.pageParams.uiDisplay.tdEntitlement &&
            this.context.InStr('FieldShowList', 'ServiceVisitFrequency') !== -1) {
            this.context.pageParams.uiDisplay.trEntitlementServiceQuantity = true;
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate || this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'btnDefaultServiceQuantity');
            } else {
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'btnDefaultServiceQuantity');
            }
        } else {
            this.context.pageParams.uiDisplay.trEntitlementServiceQuantity = false;
        }

        if (this.context.isControlChecked('CompositeProductInd')) {
            this.context.pageParams.spanProductCodeLabel_innertext = 'Composite Product Code';
        } else {
            this.context.pageParams.spanProductCodeLabel_innertext = 'Product Code';
        }
        let hasValueCompositeProductCode: boolean = this.context.fieldHasValue('CompositeProductCode');
        this.context.setRequiredStatus('CompositeSequence', hasValueCompositeProductCode);
        this.context.pageParams.uiDisplay.trCompositeProductDetails1 = hasValueCompositeProductCode;
        this.context.pageParams.uiDisplay.trCompositeProductDetails2 = hasValueCompositeProductCode;
        if (hasValueCompositeProductCode) {
            this.context.iCABSAServiceCoverMaintenance3.PopulateCompositeCode();
        }
        this.context.iCABSAServiceCoverMaintenance7.CreateTabs();
        this.context.iCABSAServiceCoverMaintenance5.SetVisitPatternEffectiveDate();
        if (this.context.getRawControlValue('SelTaxCode')) {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.riMaintenance.PostDataAddFunction('GetRequireExemptNumberInd');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.PostDataAddParamFromField('TaxCodeForExemptionInd', 'SelTaxCode', MntConst.eTypeCode);
            this.context.riMaintenance.ReturnDataAdd('RequireExemptNumberInd', MntConst.eTypeCheckBox);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('RequireExemptNumberInd', data['RequireExemptNumberInd']);

                if (this.context.getRawControlValue('RequireExemptNumberInd') === 'yes') {
                    this.context.pageParams.uiDisplay.TaxExemptionNumberLabel = true;
                    this.context.pageParams.TaxExemptionNumber = true;
                    if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd || this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                        this.context.setRequiredStatus('TaxExemptionNumber', true);
                    }
                } else {
                    this.context.pageParams.uiDisplay.TaxExemptionNumberLabel = false;
                    this.context.pageParams.TaxExemptionNumber = false;
                    if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd || this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                        this.context.setControlValue('TaxExemptionNumber', '');
                        this.context.setRequiredStatus('TaxExemptionNumber', false);
                    }
                }
            });
        } else {
            this.context.pageParams.uiDisplay.TaxExemptionNumberLabel = false;
            this.context.pageParams.TaxExemptionNumber = false;
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd || this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                this.context.setControlValue('TaxExemptionNumber', '');
                this.context.setRequiredStatus('TaxExemptionNumber', false);
            }
        }

        if (this.context.pageParams.blnEnableDowSentricon && this.context.InStr('FieldShowList', 'DOWSentricon') > -1) {
            this.context.pageParams.uiDisplay.trDOWSentricon = true;
            this.context.iCABSAServiceCoverMaintenance7.DOWSentriconToggle();
        } else {
            this.context.pageParams.uiDisplay.trDOWSentricon = false;
            this.context.pageParams.uiDisplay.trDOWInstall = false;
            this.context.pageParams.uiDisplay.trDOWProduct = false;
            this.context.pageParams.uiDisplay.trDOWPerimeter = false;
            this.context.pageParams.uiDisplay.trDOWRenewalDate = false;
        }
        let shouldShowPerimeterValueField: boolean = this.context.InStr('FieldShowList', 'PerimeterValue') !== -1
            && !this.context.isControlChecked('DOWSentriconInd');
        this.context.pageParams.uiDisplay.trPerimeterValue = shouldShowPerimeterValueField;
        this.context.setRequiredStatus('PerimeterValue', shouldShowPerimeterValueField);
    }

}
