import { MessageConstant } from '../../../../shared/constants/message.constant';
import { Injector } from '@angular/core';
import { ServiceCoverMaintenanceComponent } from './iCABSAServiceCoverMaintenance.component';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { GoogleMapPagesModuleRoutes } from '../../../base/PageRoutes';

export class ServiceCoverMaintenance5 {

    private context: ServiceCoverMaintenanceComponent;

    constructor(private parent: ServiceCoverMaintenanceComponent, injector: Injector) {
        this.context = parent;
    }

    public selTaxCode_OnChange(): void {
        this.context.setControlValue('TaxCode',
            this.context.getRawControlValue('selTaxCode'));
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.PostDataAddFunction('GetRequireExemptNumberInd');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddParamFromField('TaxCodeForExemptionInd', 'TaxCode', MntConst.eTypeCode);
        this.context.riMaintenance.ReturnDataAdd('RequireExemptNumberInd', MntConst.eTypeCheckBox);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.setControlValue('RequireExemptNumberInd', data['RequireExemptNumberInd']);
            if (this.context.getRawControlValue('RequireExemptNumberInd') === 'yes') {
                this.context.pageParams.uiDisplay.TaxExemptionNumberLabel = true;
                this.context.pageParams.TaxExemptionNumber = true;
                this.context.setRequiredStatus('TaxExemptionNumber', this.context.riMaintenance.CurrentMode === MntConst.eModeAdd ||
                    this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate);
            } else {
                this.context.pageParams.uiDisplay.TaxExemptionNumberLabel = false;
                this.context.pageParams.TaxExemptionNumber = false;
                if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd ||
                    this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                    this.context.setControlValue('TaxExemptionNumber', 'true');
                    this.context.setRequiredStatus('TaxExemptionNumber', false);
                }
            }
        }, 'POST');
    }

    public AddMultipleTaxRates(): void {
        if (this.context.pageParams.vbOverrideMultipleTaxRates) {
            this.context.riMaintenance.AddTableField('MultipleTaxRates', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
            this.context.riMaintenance.AddTableField('ConsolidateEqualTaxRates', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
            this.context.riMaintenance.AddTableField('TaxCodeMaterials', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'LookUp');
            this.context.riMaintenance.AddTableField('TaxCodeLabour', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'LookUp');
            this.context.riMaintenance.AddTableField('TaxCodeReplacement', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'LookUp');
            this.context.riMaintenance.AddTableField('InvoiceTextMaterials', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
            this.context.riMaintenance.AddTableField('InvoiceTextLabour', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
            this.context.riMaintenance.AddTableField('InvoiceTextReplacement', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        } else {
            this.context.riMaintenance.AddTableField('MultipleTaxRates', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.context.riMaintenance.AddTableField('ConsolidateEqualTaxRates', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.context.riMaintenance.AddTableField('TaxCodeMaterials', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.context.riMaintenance.AddTableField('TaxCodeLabour', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.context.riMaintenance.AddTableField('TaxCodeReplacement', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.context.riMaintenance.AddTableField('InvoiceTextMaterials', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.context.riMaintenance.AddTableField('InvoiceTextLabour', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.context.riMaintenance.AddTableField('InvoiceTextReplacement', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        }
        this.context.disableControl('MultipleTaxRates', !this.context.pageParams.vbOverrideMultipleTaxRates);
        this.context.disableControl('ConsolidateEqualTaxRates', !this.context.pageParams.vbOverrideMultipleTaxRates);
        this.context.disableControl('TaxCodeMaterials', !this.context.pageParams.vbOverrideMultipleTaxRates);
        this.context.disableControl('TaxCodeLabour', !this.context.pageParams.vbOverrideMultipleTaxRates);
        this.context.disableControl('TaxCodeReplacement', !this.context.pageParams.vbOverrideMultipleTaxRates);
        this.context.disableControl('InvoiceTextMaterials', !this.context.pageParams.vbOverrideMultipleTaxRates);
        this.context.disableControl('InvoiceTextLabour', !this.context.pageParams.vbOverrideMultipleTaxRates);
        this.context.disableControl('InvoiceTextReplacement', !this.context.pageParams.vbOverrideMultipleTaxRates);
    }

    public cmdHardSlotCalendar_onClick(): void {
        this.context.pageParams.blnAllowHSCalendar = true;
        if (!this.context.getRawControlValue('ProductCode')) {
            this.context.showAlert(MessageConstant.PageSpecificMessage.Product_Code_Is_Required);
            this.context.pageParams.blnAllowHSCalendar = false;
            return;
        } else if (this.context.pageParams.uiDisplay.trServiceVisitFrequency &&
            !this.context.getRawControlValue('ServiceVisitFrequency')) {
            this.context.showAlert(MessageConstant.PageSpecificMessage.Visit_Frequency_Is_Required);
            this.context.pageParams.blnAllowHSCalendar = false;
            return;
        } else if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            if (this.context.pageParams.SavedServiceVisitFrequency !== this.context.getRawControlValue('ServiceVisitFrequency') &&
                !this.context.getRawControlValue('LastChangeEffectDate')) {
                this.context.showAlert(MessageConstant.PageSpecificMessage.Effective_Date_Is_Required);
                this.context.pageParams.blnAllowHSCalendar = false;
                return;
            } else {
                this.context.setControlValue('HardSlotEffectDate',
                    this.context.getRawControlValue('LastChangeEffectDate'));
            }
        } else if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            if (!this.context.getRawControlValue('ServiceCommenceDate')) {
                this.context.showAlert(MessageConstant.PageSpecificMessage.Service_Commence_Date_Is_Required);
                this.context.pageParams.blnAllowHSCalendar = false;
                return;
            } else {
                this.context.setControlValue('HardSlotEffectDate',
                    this.context.getRawControlValue('ServiceCommenceDate'));
            }
        }
        if (!this.context.getRawControlValue('HardSlotType')) {
            this.context.showAlert(MessageConstant.PageSpecificMessage.Hard_Slot_Type_Is_Required);
            this.context.pageParams.blnAllowHSCalendar = false;
            return;
        }
        if (this.context.pageParams.blnAllowHSCalendar) {
            //riExchange.Mode = 'ServiceCover';
            //window.location = '/wsscripts/riHTMLWrapper.p?riFileName=Application/iCABSAServiceCoverHSCalendarGrid.htm<maxwidth>';
            this.context.showAlert(MessageConstant.Message.PageNotCovered, 3);
        }
    }

    public cmdDiaryView_onClick(): void {
        this.context.toDiaryView = true;
        this.context.storePageParams();
        this.context.pageParams.initialForm = this.context.createControlObjectFromForm();
        this.context.pageParams.formDirty = false;
        this.context.pageParams.formDirty = this.context.formIsDirty;
        //riExchange.Mode = 'ServiceCover';
        //window.location = '/wsscripts/riHTMLWrapper.p?rif(ileName=Application/iCABSATechnicianVisitDiaryGrid.htm<maxwidth>';
        this.context.navigate('ServiceCover', GoogleMapPagesModuleRoutes.ICABSATECHNICIANVISITDIARYGRID);
    }
    public AutoRouteProductInd_onClick(): void {
        let autoRouteProductInd = this.context.isControlChecked('AutoRouteProductInd');
        this.context.pageParams.uiDisplay.RoutingExclusionReason = autoRouteProductInd;
        this.context.setRequiredStatus('RoutingExclusionReason', autoRouteProductInd);
    }

    public VisitPatternEffectiveDate_OnChange(): void {
        if (this.context.getRawControlValue('VisitPatternEffectiveDate')) {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.riMaintenance.PostDataAddFunction('CheckPatternEffectiveDate');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.riMaintenance.PostDataAdd('ServiceCoverRowID', this.context.riMaintenance.GetRowID('ServiceCoverROWID'), MntConst.eTypeText);
            this.context.PostDataAddFromField('VisitPatternRowID', MntConst.eTypeText);
            this.context.PostDataAddFromField('VisitPatternEffectiveDate', MntConst.eTypeText);
            for (let iCount = 1; iCount <= 7; iCount++) {
                this.context.riMaintenance.ReturnDataAdd('VisitOnDay' + iCount, MntConst.eTypeCheckBox);
            }
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('VisitPatternRowID', data['VisitPatternRowID']);
                this.context.setControlValue('ChangeDateInd', data['ChangeDateInd']);
                if (!this.context.isControlChecked('ChangeDateInd')) {
                    this.context.setControlValue('AutoPattern', data['AutoPattern']);
                    this.context.setControlValue('AutoAllocation', data['AutoAllocation']);
                    if (this.context.riExchange.riInputElement.GetValue(this.uiForm, 'SelAutoPattern') !== 'P') {
                        this.iCABSAServiceCoverMaintenance5.AutoPattern_OnChange();
                    }
                    this.iCABSAServiceCoverMaintenance5.SelAutoPattern_Onchange();
                    this.iCABSAServiceCoverMaintenance5.AutoAllocation_OnChange();
                    this.iCABSAServiceCoverMaintenance5.SelAutoAllocation_OnChange();
                    for (let iCount = 1; iCount <= 7; iCount++) {
                        this.context.setControlValue('VisitOnDay' + iCount, data['VisitOnDay' + iCount]);
                        this.context.setControlValue('BranchServiceAreaCode' + iCount, data['VisitOnDay' + iCount]);
                    }
                }
            }, 'POST');
        }
    }

    public AutoPattern_OnChange(): void {
        if (!this.context.getRawControlValue('AutoPattern')) {
            this.context.setControlValue('SelAutoPattern', 'D');
        } else {
            this.context.setControlValue('SelAutoPattern',
                this.context.getRawControlValue('AutoPattern'));
        }
    }

    public SelAutoPattern_Onchange(): void {
        this.context.setControlValue('AutoPattern',
            this.context.getRawControlValue('SelAutoPattern'));
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelAutoPattern');
        this.context.iCABSAServiceCoverMaintenance5.SetVisitPatternEffectiveDate();
        if (this.context.getRawControlValue('SelAutoPattern') === 'D') {
            this.context.setControlValue('SelAutoAllocation', 'D');
        }

        if (this.context.getRawControlValue('AutoPattern') === 'D') {
            this.context.setControlValue('AutoAllocation', 'D');
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelAutoAllocation');
            for (let iCount = 1; iCount <= 7; iCount++) {
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'VisitOnDay' + iCount);
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'BranchServiceAreaCode' + iCount);
            }
        } else {
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'VisitPatternEffectiveDate');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelAutoAllocation');
        }
    }

    public AutoAllocation_OnChange(): void {
        this.context.setControlValue('SelAutoAllocation', !this.context.getRawControlValue('AutoAllocation') ? 'D' : this.context.getRawControlValue('AutoAllocation'));
    }

    public SelAutoAllocation_Onchange(): void {
        this.context.setControlValue('AutoAllocation',
            this.context.getRawControlValue('SelAutoAllocation'));
        if (this.context.getRawControlValue('AutoAllocation') === 'D') {
            for (let iCount = 1; iCount <= 7; iCount++) {
                this.context.setControlValue('VisitOnDay' + iCount, false);
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'VisitOnDay' + iCount);
                this.context.setControlValue('BranchServiceAreaCode' + iCount, '');
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'BranchServiceAreaCode' + iCount);
            }
        } else {
            for (let iCount = 1; iCount <= 7; iCount++) {
                this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'VisitOnDay' + iCount);
                this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'BranchServiceAreaCode' + iCount);
            }
        }
    }


    public GetServiceAreaRequired(): void {
        for (let iCount = 1; iCount <= 7; iCount++) {
            this.context.pageParams.blnRequired = this.context.isControlChecked('VisitOnDay' + iCount);
            this.context.setRequiredStatus('BranchServiceAreaCode' + iCount, this.context.pageParams.blnRequired);
            if (!this.context.pageParams.blnRequired) {
                this.context.setControlValue('BranchServiceAreaCode' + iCount, '');
                this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'BranchServiceAreaCode' + iCount, false);
            }
        }
    }

    public ValidateServiceArea(): void {
        for (let iCount = 1; iCount <= 7; iCount++) {
            if (this.context.isControlChecked('VisitOnDay' + iCount) &&
                !this.context.getRawControlValue('BranchServiceAreaCode' + iCount)) {
                this.context.riExchange.riInputElement.isError(this.context.uiForm, 'BranchServiceAreaCode' + iCount);
            }
            if (this.context.getRawControlValue('BranchServiceAreaCode' + iCount)) {
                this.context.riMaintenance.clear();
                this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
                this.context.riMaintenance.PostDataAddFunction('CheckBranchServiceArea');
                this.context.riMaintenance.PostDataAddBusiness();
                this.context.PostDataAddFromField('ServiceBranchNumber', MntConst.eTypeInteger);
                this.context.PostDataAddFromField('BranchServiceAreaCode' + iCount, MntConst.eTypeCode);
                this.context.riMaintenance.Execute(this.context, function (data: any): any {
                    if (data.hasOwnProperty('errorMessage')) {
                        this.context.riExchange.riInputElement.SetErrorStatus('BranchServiceAreaCode' + iCount, true);
                        this.context.riMaintenance.CancelEvent = true;
                    }
                }, 'POST');
            }
        }
    }

    public ValidateVisitPattern(): void {
        if (this.context.getRawControlValue('AutoPattern') === 'D'
            && this.context.getRawControlValue('AutoAllocation') === 'D') {
            for (let iCount = 1; iCount <= 7; iCount++) {
                if (this.context.isControlChecked('VisitOnDay' + iCount)) {
                    this.context.pageParams.blnVisitCount = this.context.pageParams.blnVisitCount + 1;
                }
            }

            this.context.setControlValue('VisitOnDayCount', this.context.pageParams.blnVisitCount);
            if (this.context.getRawControlValue('VisitsPerCycle') !== this.context.getRawControlValue('VisitOnDayCount')) {
                //this.context.setControlValue('MessageDisplay', ''Visit patterns are out of synchronisation && need to be updated.Do you want to continue?')%>'' );
                //if (MsgBox(MessageDisplay.value, this.context.pageParams.vbYesNo + this.context.pageParams.vbQuestion + this.context.pageParams.vbDefaultButton1, '<% ===riT('Visit Pattern Records')%>') = this.context.pageParams.vbNo) {
                //riMaintenance.CancelEvent = true;
                //} else {
                this.context.setControlValue('SelAutoPattern', 'D');
                this.context.iCABSAServiceCoverMaintenance5.SelAutoPattern_Onchange();
                this.context.iCABSAServiceCoverMaintenance5.GetServiceAreaRequired();
                //}
            }
        }
    }

    public SetVisitPatternEffectiveDate(): void {
        if (this.context.pageParams.blnUseVisitCycleValues &&
            this.context.pageParams.currentContractType === 'C' &&
            this.context.InStr('FieldShowList', 'VisitCycleInWeeks') !== -1) {
            if (this.context.getRawControlValue('SelAutoAllocation') === 'D' ||
                this.context.getRawControlValue('SelAutoPattern') === 'D') {
                if ((this.context.riMaintenance.CurrentMode === MntConst.eModeAdd || this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) &&
                    !this.context.getRawControlValue('VisitPatternEffectiveDate')) {
                    this.context.setControlValue('VisitPatternEffectiveDate', this.context.utils.Today());
                    setTimeout(function (): void {
                        this.context.setDateToFields('VisitPatternEffectiveDate', this.context.globalize.formatDateToLocaleFormat(new Date()) as string);
                    }.bind(this.context), 100);
                    this.context.setControlValue('SelAutoAllocation', 'D');
                    this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelAutoAllocation');
                    for (let iCount = 1; iCount <= 7; iCount++) {
                        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'VisitOnDay' + iCount);
                        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'BranchServiceAreaCode' + iCount);
                    }
                }
                this.context.iCABSAServiceCoverMaintenance5.DefaultVisitPattern();
            }
        }
    }

    public DefaultVisitPattern(): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.PostDataAddFunction('GetDefaultVisitPattern');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.riMaintenance.PostDataAdd('ServiceCoverRowID', this.context.riMaintenance.GetRowID('ServiceCoverROWID'), MntConst.eTypeText);
        for (let iCount = 1; iCount <= 7; iCount++) {
            this.context.riMaintenance.ReturnDataAdd('VisitOnDay' + iCount, MntConst.eTypeCheckBox);
        }
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            for (let iCount = 1; iCount <= 7; iCount++) {
                this.context.setControlValue('VisitOnDay' + iCount, data['VisitOnDay' + iCount]);
                this.context.setControlValue('BranchServiceAreaCode' + iCount, data['BranchServiceAreaCode' + iCount]);
            }
        }, 'POST', 6, false);
    }

}
