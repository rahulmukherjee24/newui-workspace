import { InternalGridSearchSalesModuleRoutes, InternalGridSearchApplicationModuleRoutes, InternalGridSearchServiceModuleRoutes, InternalMaintenanceSalesModuleRoutesConstant, InternalMaintenanceApplicationModuleRoutesConstant, InternalMaintenanceModuleRoutes, BIReportsRoutes } from './../../../base/PageRoutes';
import { ContractActionTypes } from './../../../actions/contract';
import { Injector } from '@angular/core';
import { ServiceCoverMaintenanceComponent } from './iCABSAServiceCoverMaintenance.component';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ContractManagementModuleRoutes } from '../../../base/PageRoutes';
import { MessageConstant } from '../../../../shared/constants/message.constant';

export class ServiceCoverMaintenance3 {

    private context: ServiceCoverMaintenanceComponent;

    constructor(private parent: ServiceCoverMaintenanceComponent, injector: Injector) {
        this.context = parent;
    }

    public InvTypeSel_OnChange(): void {
        this.context.setControlValue('InvoiceTypeNumber', this.context.getRawControlValue('InvTypeSel'));
        this.context.iCABSAServiceCoverMaintenance4.ShowHideBudgetBilling();

        if (this.context.pageParams.vbEnableServiceCoverDispLev) {
            this.context.iCABSAServiceCoverMaintenance6.IsVisitTriggered();
            if (this.context.isControlChecked('VisitTriggered')) {
                this.context.setControlValue('UnitValue', '0');
                this.context.setControlValue('UnitValueChange', '0');
                this.context.pageParams.uiDisplay.trUnitValue = false;
                this.context.pageParams.uiDisplay.trUnitValueChange = false;
            } else {
                if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {  // Add;
                    this.context.pageParams.uiDisplay.trUnitValue = true;
                } else {
                    this.context.pageParams.uiDisplay.trUnitValueChange = true;       // Update
                }
            }
        } else {
            this.context.setControlValue('UnitValue', '0');
            this.context.setControlValue('UnitValueChange', '0');
            this.context.pageParams.uiDisplay.trUnitValue = false;
            this.context.pageParams.uiDisplay.trUnitValueChange = false;
        }
    }

    public ServiceVisitFrequency_OnChange(event?: any): void {
        if (this.context.pageParams.blnUseVisitCycleValues) {
            this.context.iCABSAServiceCoverMaintenance2.GetVisitCycleValues();
            this.context.setControlValue('ServiceVisitFrequencyCopy',
                this.context.getRawControlValue('ServiceVisitFrequency'));
        }
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            (this.context.isControlChecked('DisplayLevelInd') ||
                this.context.isControlChecked('InvoiceUnitValueRequired')) &&
            this.context.isControlChecked('VisitTriggered')) {
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                if (this.context.getRawControlValue('UnitValue') !== '0'
                    && this.context.getRawControlValue('UnitValue') !== null) {
                    this.context.iCABSAServiceCoverMaintenance6.CalculateAnnualValue();
                }
            } else {
                if (this.context.getRawControlValue('UnitValueChange')) {
                    this.context.iCABSAServiceCoverMaintenance6.CalculateAnnualValueChange();
                }
            }
        }
        this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
    }

    public ClosedCalendarTemplateNumber_onchange(obj: any): void {
        this.context.setControlValue('ClosedTemplateName', '');
    }

    public AnnualCalendarTemplateNumber_onChange(obj: any): void {
        this.context.setControlValue('CalendarTemplateName', '');
    }

    public ContractNumber_onchange(): void {
        if (this.context.hasControlChanged('ContractNumber')) {
            this.context.isPremiseInvalid = false;
            if (!this.context.pageParams.FullAccess) {
                this.context.accessSubscription = this.context.utils.getUserAccessType().subscribe(data => {
                    this.context.pageParams.FullAccess = data;
                }, error => {
                    this.context.pageParams.FullAccess = 'Restricted';
                });
            }
            if (this.context.getControlValue('ContractNumber')) {
                this.context.setControlValue('ContractNumber', this.context.utils.fillLeadingZeros(this.context.getControlValue('ContractNumber'), 8));
                this.context.disableControl('PremiseNumber', false);
            } else {
                this.context.disableControl('PremiseNumber', true);
            }
            this.context.getSysCharDtetails(true);
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd || this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                this.context.iCABSAServiceCoverMaintenance3.GetDefaultTaxCode();
                this.context.iCABSAServiceCoverMaintenance3.BuildTaxCodeCombo();
            }
            this.context.serviceCoverSearchParams.ContractNumber = this.context.getControlValue('ContractNumber');
            this.context.inputParamsAccountPremise.ContractNumber = this.context.getControlValue('ContractNumber');
            this.context.linkedServiceCoverSearchParams.ContractNumber = this.context.getControlValue('ContractNumber');
        }
        document.querySelector('#PremiseNumber')['focus']();
    }

    public PremiseNumber_onchange(): void {
        this.context.lookUpForPremise();
        if (this.context.getControlValue('PremiseNumber')) {
            this.context.serviceCoverSearchParams.PremiseNumber = this.context.getControlValue('PremiseNumber');
            this.context.linkedServiceCoverSearchParams.PremiseNumber = this.context.getControlValue('PremiseNumber');
            this.context.disableControl('ProductCode', false);
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                this.context.disableControl('cmdCopyServiceCover', false);
            }
        } else {
            this.context.disableControl('ProductCode', true);
        }
        document.querySelector('#ProductCode')['focus']();
    }

    public TaxCodeMaterials_onChange(): void {
        if (!this.context.fieldHasValue('TaxCodeMaterials')) {
            this.context.setControlValue('TaxCodeMaterialsDesc', '');
        } else {
            this.context.riMaintenance.BusinessObject = 'iCABSExchangeFunctions.p';
            this.context.riMaintenance.clear();
            this.context.riMaintenance.PostDataAdd('PostDesc', 'TaxCode', MntConst.eTypeText);
            this.context.PostDataAddParamFromField('TaxCode', 'TaxCodeMaterials', MntConst.eTypeText);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('TaxCodeMaterialsDesc', data['TaxCodeDesc']);
            }, 'GET', 0);
            this.context.setControlValue('TaxCodeMaterialsDesc', '');
        }
    }

    public TaxCodeLabour_onChange(): void {
        if (!this.context.fieldHasValue('TaxCodeLabour')) {
            this.context.setControlValue('TaxCodeLabourDesc', '');
        } else {
            this.context.riMaintenance.BusinessObject = 'iCABSExchangeFunctions.p';
            this.context.riMaintenance.clear();
            this.context.riMaintenance.PostDataAdd('PostDesc', 'TaxCode', MntConst.eTypeText);
            this.context.PostDataAddParamFromField('TaxCode', 'TaxCodeLabour', MntConst.eTypeText);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('TaxCodeLabourDesc', data['TaxCodeDesc']);
            }, 'GET', 0);
            this.context.setControlValue('TaxCodeLabourDesc', '');
        }
    }

    public TaxCodeReplacement_onChange(): void {
        if (!this.context.fieldHasValue('TaxCodeReplacement')) {
            this.context.setControlValue('TaxCodeReplacementDesc', '');
        } else {
            this.context.riMaintenance.BusinessObject = 'iCABSExchangeFunctions.p';
            this.context.riMaintenance.clear();
            this.context.riMaintenance.PostDataAdd('PostDesc', 'TaxCode', MntConst.eTypeText);
            this.context.PostDataAddParamFromField('TaxCode', 'TaxCodeReplacement', MntConst.eTypeText);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('TaxCodeReplacementDesc', data['TaxCodeDesc']);
            }, 'GET', 0);

            this.context.setControlValue('TaxCodeReplacementDesc', '');
        }
    }

    public GetDefaultTaxCode(): void {
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.clear();
        this.context.riMaintenance.PostDataAddFunction('GetDefaultTaxCode');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeCode);
        this.context.riMaintenance.PostDataAdd('ContractTypeCode', this.context.riExchange.getCurrentContractType(), MntConst.eTypeCode);
        this.context.PostDataAddFromField('ProductCode', MntConst.eTypeCode);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.setControlValue('TaxCode', data['TaxCode']);
            this.context.setControlValue('TaxDesc', data['TaxDesc']);
            this.context.setControlValue('selTaxCode', this.context.getRawControlValue('TaxCode'));
            if (this.context.pageParams.vbDefaultTaxCodeOnServiceCoverMaintLog) {
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
                this.context.pageParams.selTaxCode = [];
                let obj = {
                    text: data['TaxCode'] + ' - ' + data['TaxDesc'],
                    value: data['TaxCode']
                };
                this.context.pageParams.selTaxCode.push(obj);
            }
        }, 'POST');
    }

    public EntitlementAnnualQuantity_OnChange(): void {
        this.context.setControlValue('EntitlementNextAnnualQuantity',
            this.context.getRawControlValue('EntitlementAnnualQuantity'));
        this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
    }

    public DepotNumber_onChange(): void {
        if (this.context.getRawControlValue('DepotNumber') &&
            !this.context.riExchange.riInputElement.isError(this.context.uiForm, 'DepotNumber')) {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.riMaintenance.PostDataAddFunction('GetDepotName');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.PostDataAddFromField('DepotNumber', MntConst.eTypeInteger);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('DepotName', data['DepotName']);
            }, 'POST');
        } else {
            this.context.setControlValue('DepotName', '');
        }
    }

    public SetQuickWindowSetValue(strValue: string): void {
        for (let iLoop = 1; iLoop <= 7; iLoop++) {
            this.context.setControlValue('selQuickWindowSet' + iLoop, strValue);
        }
    }

    public RenegOldContract_onchange(): void {
        if (this.context.getControlValue('RenegOldContract')) {
            this.context.setControlValue('RenegOldContract', this.context.utils.fillLeadingZeros(this.context.getControlValue('RenegOldContract'), 8));
        }
        this.context.inputRenegPremiseSearch.ContractNumber = this.context.getControlValue('RenegOldContract');
    }

    public InitialTreatmentTime_OnChange(): void {
        this.context.iCABSAServiceCoverMaintenance2.ProcessTimeString('InitialTreatmentTime');
    }

    public StandardTreatmentTime_OnChange(): void {
        this.context.iCABSAServiceCoverMaintenance2.ProcessTimeString('StandardTreatmentTime');
    }

    public ServiceAnnualTime_OnChange(): void {
        this.context.iCABSAServiceCoverMaintenance2.ProcessTimeString('ServiceAnnualTime');
    }

    public AnnualTimeChange_OnChange(): void {
        if (this.context.formatTime(this.context.getControlValue('AnnualTimeChange'), 'AnnualTimeChange')) {
            this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
        }
    }

    public SalesPlannedTime_OnChange(): void {
        this.context.iCABSAServiceCoverMaintenance2.ProcessTimeString('SalesPlannedTime');
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.setControlValue('ActualPlannedTime',
                this.context.getRawControlValue('SalesPlannedTime'));
        }
    }

    public ActualPlannedTime_OnChange(): void {
        this.context.iCABSAServiceCoverMaintenance2.ProcessTimeString('ActualPlannedTime');
    }

    public MinimumDuration_OnChange(): void {
        if (!this.context.getRawControlValue('MinimumDuration') ||
            !this.context.getRawControlValue('ServiceCommenceDate')) {
            this.context.setControlValue('ExpiryDate', '');
            this.context.pageParams.dtExpiryDate.value = null;
        } else {
            //this.context.setControlValue('ExpiryDate', 'DateAdd('m',MinimumDuration.value), ServiceCommenceDate.value);
            let months = parseInt(this.context.getControlValue('MinimumDuration'), 10);
            let temp = this.context.globalize.parseDateStringToDate(this.context.getControlValue('ServiceCommenceDate'));
            if (typeof temp !== 'boolean') {
                let newdate = new Date(temp.setMonth(temp.getMonth() + months));
                this.context.pageParams.dtExpiryDate.value = newdate;
            }
        }
    }
    public LeadEmployeeDisplay(): void {
        if (this.context.isControlChecked('LeadInd')) {
            this.context.pageParams.uiDisplay.tdLeadEmployeeLabel = true;
            this.context.pageParams.LeadEmployee = true;
            this.context.pageParams.LeadEmployeeSurname = true;
            this.context.setRequiredStatus('LeadEmployee', !(this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate));
        } else if (!this.context.pageParams.LeadEmployee) {
            this.context.pageParams.uiDisplay.tdLeadEmployeeLabel = false;
            this.context.pageParams.LeadEmployee = false;
            this.context.pageParams.LeadEmployeeSurname = false;
            this.context.setControlValue('LeadEmployee', '');
            this.context.setControlValue('LeadEmployeeSurname', '');
            this.context.setRequiredStatus('LeadEmployee', false);
        }
    }

    public PopulateCompositeCode(): void {
        let ValArray;
        let DescArray;
        this.context.pageParams.selTaxCode = [];
        let CompositeCodeList = this.context.getRawControlValue('CompositeCodeList');
        let CompositeDescList = this.context.getRawControlValue('CompositeDescList');
        if (CompositeCodeList && CompositeDescList) {
            ValArray = CompositeCodeList.split('^');
            DescArray = CompositeDescList.split('^');
            for (let i = 0; i < ValArray.length; i++) {
                let obj = {
                    text: ValArray[0] + ' - ' + DescArray[0],
                    value: ValArray[0]
                };
                this.context.pageParams.selTaxCode.push(obj);
                this.context.setControlValue('SelectCompositeProductCode', this.context.getRawControlValue('CompositeProductCode'));
            }
        }
    }

    public DefaultFromProspect(): void {
        if (this.context.riExchange.getParentHTMLValue('ProspectNumber')) {
            this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntryRequests.p';
            this.context.riMaintenance.clear();
            this.context.riMaintenance.PostDataAddFunction('DefaultFromProspect');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.riMaintenance.PostDataAdd('ProspectNumber', this.context.riExchange.getParentHTMLValue('ProspectNumber'), MntConst.eTypeInteger);
            this.context.riMaintenance.ReturnDataAdd('LeadInd', MntConst.eTypeCheckBox);
            this.context.riMaintenance.ReturnDataAdd('DetailRequiredInd', MntConst.eTypeCheckBox);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('BusinessOriginCode', data['BusinessOriginCode']);
                this.context.setControlValue('BusinessOriginDesc', data['BusinessOriginDesc']);
                this.context.setControlValue('BusinessOriginDetailCode', data['BusinessOriginDetailCode']);
                this.context.setControlValue('BusinessOriginDetailDesc', data['BusinessOriginDetailDesc']);
                this.context.setControlValue('LeadEmployee', data['LeadEmployee']);
                this.context.setControlValue('LeadEmployeeSurname', data['LeadEmployeeSurname']);
                this.context.setBusinessOriginDropDownValue();
                let isLeadInd: boolean = (this.context.LCase(data['LeadInd']) === 'yes' || this.context.LCase(data['LeadInd']) === 'true');
                let isDetailRequiredInd: boolean = (this.context.LCase(data['DetailRequiredInd']) === 'yes' || this.context.LCase(data['DetailRequiredInd']) === 'true');
                this.context.setControlValue('LeadInd', isLeadInd);
                this.context.setControlValue('DetailRequiredInd', isDetailRequiredInd);
            }, 'POST');
            this.context.setControlValue('BusinessOriginCode', '');
            this.context.setControlValue('BusinessOriginDesc', '');
            this.context.setBusinessOriginDropDownValue();
            this.context.setControlValue('BusinessOriginDetailCode', '');
            this.context.setControlValue('BusinessOriginDetailDesc', '');
            this.context.setControlValue('LeadEmployee', '');
            this.context.setControlValue('LeadEmployeeSurname', '');
            this.context.setControlValue('LeadInd', false);
            this.context.setControlValue('DetailRequiredInd', false);
        }
    }

    public menu_onchange(event: any): void {

        if (this.context.riMaintenance.CurrentMode !== MntConst.eModeAdd) {
            switch (event) {

                case 'ContactManagement':
                    this.context.iCABSAServiceCoverMaintenance3.cmdContactManagement_onclick();
                    break;
                case 'ContactManagementSearch':
                    this.context.navigate('ServiceCover', 'ccm/callcentersearch',
                        {
                            'AccountNumber': this.context.getControlValue('AccountNumber'),
                            'AccountName': this.context.getControlValue('AccountName'),
                            'ContractNumber': this.context.getControlValue('ContractNumber'),
                            'ContractName': this.context.getControlValue('ContractName'),
                            'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                            'PremiseName': this.context.getControlValue('PremiseName'),
                            'ProductCode': this.context.getControlValue('ProductCode'),
                            'ProductDesc': this.context.getControlValue('ProductDesc'),
                            'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                            'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID')
                        });
                    break;
                case 'PlanVisit':
                    this.context.iCABSAServiceCoverMaintenance3.cmdPlanVisit_onclick();
                    break;
                case 'PlanVisitTabular':
                    this.context.iCABSAServiceCoverMaintenance3.cmdPlanVisitTabular_onclick();
                    break;
                case 'StaticVisit':
                    this.context.iCABSAServiceCoverMaintenance3.cmdStaticVisit_onclick();
                    break;
                case 'ServiceDetail':
                    if (this.context.pageParams.blnAccess) {
                        this.context.iCABSAServiceCoverMaintenance3.cmdServiceDetail_onclick();
                    }
                    break;
                case 'DOWServiceValue':
                    if (this.context.pageParams.blnAccess && this.context.getRawControlValue('DOWSentriconInd') === 'true' ? true : false) {
                        this.context.iCABSAServiceCoverMaintenance7.cmdDOWServiceValue_onclick();
                    }
                    break;
                case 'ServiceValue':
                    if (this.context.pageParams.blnAccess) {
                        this.context.iCABSAServiceCoverMaintenance3.cmdServiceValue_onclick();
                    }
                    break;
                case 'VisitHistory':
                    if (this.context.pageParams.blnAccess) {
                        this.context.iCABSAServiceCoverMaintenance3.cmdVisitHistory_onclick();
                    }
                    break;
                case 'Service Recommendations':
                    if (this.context.pageParams.blnAccess) {
                        this.context.iCABSAServiceCoverMaintenance3.cmdServiceRecommendations_onclick();    // PDA Enhancements;
                    }
                    break;
                case 'History':
                    if (this.context.pageParams.blnAccess) {
                        this.context.iCABSAServiceCoverMaintenance3.cmdHistory_onclick();
                    }
                    break;
                case 'ProRata':
                    if (this.context.pageParams.blnAccess) {
                        this.context.iCABSAServiceCoverMaintenance3.cmdProRata_onclick();
                    }
                    break;
                case 'Location':
                    if (this.context.pageParams.blnAccess) {
                        this.context.iCABSAServiceCoverMaintenance3.cmdLocation_onclick();
                    }
                    break;
                case 'SalesStatsAdjustment':
                    if (this.context.pageParams.blnAccess) {
                        this.context.iCABSAServiceCoverMaintenance3.cmdSalesStatsAdjustment_onclick();
                    }
                    break;
                case 'InvoiceHistory':
                    if (this.context.pageParams.blnAccess) {
                        this.context.iCABSAServiceCoverMaintenance3.cmdInvoiceHistory_onclick();
                    }
                    break;
                case 'StateOfService':
                    if (this.context.pageParams.blnAccess) {
                        this.context.iCABSAServiceCoverMaintenance3.cmdStateOfService_onclick();
                    }
                    break;
                case 'EventHistory':
                    this.context.iCABSAServiceCoverMaintenance3.cmdEventHistory_onclick();
                    break;
                case 'Contract':
                    this.context.iCABSAServiceCoverMaintenance3.cmdContract_onclick();
                    break;
                case 'Premise':
                    this.context.iCABSAServiceCoverMaintenance3.cmdPremise_onclick();
                    break;
                case 'SeasonalService':
                    this.context.iCABSAServiceCoverMaintenance3.cmdSeasonalService_onclick();
                    break;
                case 'ServiceCalendar':
                    this.context.iCABSAServiceCoverMaintenance3.cmdServiceCalendar_onclick();
                    break;
                case 'CustomerInformation':
                    this.context.iCABSAServiceCoverMaintenance3.cmdCustomerInformation_onclick();
                    break;
                case 'AnnualCalendar':
                    this.context.iCABSAServiceCoverMaintenance3.cmdAnnualCalendar_onclick();
                    break;
                case 'SeasonalMaint':
                    this.context.iCABSAServiceCoverMaintenance3.cmdSeasonalMaint_onclick();
                    break;
                case 'ServiceVisitPlanning':
                    this.context.iCABSAServiceCoverMaintenance3.cmdServiceVisitPlanning_onclick();
                    break;
                case 'LinkedProducts':
                    this.context.iCABSAServiceCoverMaintenance3.cmdLinkedProducts_onclick();
                    break;
                case 'ServiceCoverWaste':
                    this.context.iCABSAServiceCoverMaintenance3.cmdServiceCoverWaste_onclick();
                    break;
                case 'WasteConsignmentNoteHistory':
                    this.context.iCABSAServiceCoverMaintenance3.cmdWasteConsignmentNoteHistory_onclick();
                    break;
                case 'ClosedCalendar':
                    this.context.iCABSAServiceCoverMaintenance3.cmdClosedCalendar_onclick();
                    break;
                case 'ServiceCoverDisplays':
                    this.context.iCABSAServiceCoverMaintenance6.cmdServiceCoverDisplays_onclick();
                    break;
                case 'VisitTolerances':
                    this.context.iCABSAServiceCoverMaintenance6.cmdVisitTolerances_onclick();
                    break;
                case 'InfestationTolerances':
                    this.context.iCABSAServiceCoverMaintenance6.cmdInfestationTolerances_onclick();
                    break;
                case 'TeleSalesOrderLine':
                    this.context.iCABSAServiceCoverMaintenance6.cmdTeleSalesOrderLine_onclick();
                    break;
                case 'Qualif(ications':
                    this.context.iCABSAServiceCoverMaintenance6.cmdQualifications_onclick();
                    break;
                case 'TreatmentPlan':
                    this.context.iCABSAServiceCoverMaintenance3.cmdTreatmentPlan_onclick();
                    break;
                case 'Entitlement':
                    this.onCmdServiceCoverEntitlementClick();
                    break;
            }
        } else {
            this.context.showAlert(MessageConstant.PageSpecificMessage.cannotNavInAddMode);
        }
        this.context.setControlValue('menu', 'Options');
    }

    //******************************************************
    //* Menu options/Buttons/Check boxes (onclick actions) *
    //******************************************************

    public SeasonalBranchUpdate_onclick(): void {
        if (!this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'SeasonalBranchUpdate')) {
            this.context.iCABSAServiceCoverMaintenance7.EnableSeasonalChanges(!this.context.isControlChecked('SeasonalBranchUpdate'));
        }
    }

    public SeasonalFromWeekChange(ipFromWeek: any, ipFromYear: any, opFromDate: any): void {
        this.context.iCABSAServiceCoverMaintenance7.SeasonalWeekChange('GetWeekStartFromWeek', ipFromWeek, ipFromYear, opFromDate);
    }

    public SeasonalToWeekChange(ipFromWeek: any, ipFromYear: any, opFromDate: any): void {
        this.context.iCABSAServiceCoverMaintenance7.SeasonalWeekChange('GetWeekEndFromWeek', ipFromWeek, ipFromYear, opFromDate);
    }

    public cmdCustomerInformation_onclick(): void {
        this.context.navigate('ServiceCover', InternalGridSearchServiceModuleRoutes.ICABSACUSTOMERINFORMATIONSUMMARY.URL_2, {
            'contractNumber': this.context.getControlValue('ContractNumber'),
            'contractName': this.context.getControlValue('ContractName'),
            'accountNumber': this.context.getControlValue('AccountNumber'),
            'accountName': this.context.getControlValue('AccountName'),
            'currentContractType': this.context.riExchange.getCurrentContractType()
        });
    }

    public tdCustomerInfo_onclick(): void {
        this.cmdCustomerInformation_onclick();
    }

    public cmdContactManagement_onclick(): void {

        if (this.context.pageParams.lRegContactCentreReview) {
            this.context.store.dispatch({
                type: ContractActionTypes.SAVE_DATA,
                payload: {
                    'AccountNumber': this.context.getControlValue('AccountNumber'),
                    'AccountName': this.context.getControlValue('AccountName'),
                    'ContractNumber': this.context.getControlValue('ContractNumber'),
                    'ContractName': this.context.getControlValue('ContractName'),
                    'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                    'PremiseName': this.context.getControlValue('PremiseName'),
                    'ProductCode': this.context.getControlValue('ProductCode'),
                    'ProductDesc': this.context.getControlValue('ProductDesc'),
                    'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                    'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                    'currentContractType': this.context.riExchange.getCurrentContractType()
                }
            });
            this.context.navigate('ServiceCover', 'ccm/centreReview');
        } else {
            this.context.store.dispatch({
                type: ContractActionTypes.SAVE_DATA,
                payload: {
                    'AccountNumber': this.context.getControlValue('AccountNumber'),
                    'AccountName': this.context.getControlValue('AccountName'),
                    'ContractNumber': this.context.getControlValue('ContractNumber'),
                    'ContractName': this.context.getControlValue('ContractName'),
                    'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                    'PremiseName': this.context.getControlValue('PremiseName'),
                    'ProductCode': this.context.getControlValue('ProductCode'),
                    'ProductDesc': this.context.getControlValue('ProductDesc'),
                    'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                    'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                    'currentContractType': this.context.riExchange.getCurrentContractType()
                }
            });
            this.context.navigate('ServiceCover', 'ccm/contact/search');
        }
    }

    public cmdHistory_onclick(): void {
        /*this.context.store.dispatch({
            type: ContractActionTypes.SAVE_DATA,
            payload: {
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'ContractRowID': this.context.getControlValue('ServiceCoverROWID'),
                'currentContractType': this.context.riExchange.getCurrentContractType(),
                'countryCode': this.context.utils.getCountryCode()
            }
        });*/
        this.context.navigate('ServiceCover', InternalGridSearchSalesModuleRoutes.ICABSACONTRACTHISTORYGRID, {
            'currentContractTypeURLParameter': this.context.riExchange.getCurrentContractType(),
            'AccountNumber': this.context.getControlValue('AccountNumber'),
            'AccountName': this.context.getControlValue('AccountName'),
            'ContractNumber': this.context.getControlValue('ContractNumber'),
            'ContractName': this.context.getControlValue('ContractName'),
            'PremiseNumber': this.context.getControlValue('PremiseNumber'),
            'PremiseName': this.context.getControlValue('PremiseName'),
            'ProductCode': this.context.getControlValue('ProductCode'),
            'ProductDesc': this.context.getControlValue('ProductDesc'),
            'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
            'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
            'currentContractType': this.context.riExchange.getCurrentContractType(),
            'countryCode': this.context.utils.getCountryCode()
        });
    }

    public cmdServiceValue_onclick(): void {
        this.context.navigate('ServiceCoverAll', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVALUEGRID,
            {
                'ParentMode': 'ServiceCoverAll',
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'currentContractType': this.context.riExchange.getCurrentContractType()
            });
    }

    public cmdLocation_onclick(): void {
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            this.context.isControlChecked('DisplayLevelInd')) {
            this.context.navigate('Premise-Allocate', InternalGridSearchSalesModuleRoutes.ICABSAEMPTYPREMISELOCATIONSEARCHGRID);
        } else {
            if (this.context.pageParams.vbEnableDetailLocations &&
                this.context.isControlChecked('LocationsEnabled')) {
                this.context.navigate('ProductDetailSC', InternalGridSearchServiceModuleRoutes.ICABSASERVICECOVERDETAILLOCATIONENTRYGRID,
                    {
                        'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID')
                    });
            } else {
                this.context.navigate('Premise-Allocate', 'application/premiseLocationAllocation');
            }
        }
    }

    public cmdVisitHistory_onclick(): void {
        this.context.store.dispatch({
            type: ContractActionTypes.SAVE_DATA,
            payload: {
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'ServiceVisitAnnivDate': this.context.getControlValue('ServiceVisitAnnivDate'),
                'ServiceVisitFrequency': this.context.getControlValue('ServiceVisitFrequency'),
                'currentContractType': this.context.riExchange.getCurrentContractType()
            }
        });
        this.context.navigate('ServiceCover', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVISITSUMMARY);
    }

    public cmdServiceDetail_onclick(): void {
        this.context.navigate('ServiceCover', 'application/ServiceCoverDetails', {
            'AccountNumber': this.context.getControlValue('AccountNumber'),
            'AccountName': this.context.getControlValue('AccountName'),
            'ContractNumber': this.context.getControlValue('ContractNumber'),
            'ContractName': this.context.getControlValue('ContractName'),
            'PremiseNumber': this.context.getControlValue('PremiseNumber'),
            'PremiseName': this.context.getControlValue('PremiseName'),
            'ProductCode': this.context.getControlValue('ProductCode'),
            'ProductDesc': this.context.getControlValue('ProductDesc'),
            'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
            'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
            'ServiceVisitAnnivDate': this.context.getControlValue('ServiceVisitAnnivDate'),
            'currentContractType': this.context.riExchange.getCurrentContractType()
        });
    }

    public cmdProRata_onclick(): void {
        this.context.navigate('ServiceCover', InternalGridSearchSalesModuleRoutes.ICABSAPRORATACHARGESUMMARY, {
            'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
            'currentContractType': this.context.riExchange.getCurrentContractType()
        });
    }

    public cmdInvoiceHistory_onclick(): void {
        //riExchange.Mode = 'Product';
        //location = '/wsscripts/riHTMLWrapper.p?rif(ileName=Application/iCABSAContractInvoiceGrid.htm<maxwidth>' + CurrentContractTypeURLParameter;
        this.context.navigate('Product', 'billtocash/contract/invoice',
            {
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'currentContractType': this.context.riExchange.getCurrentContractType()
            });
    }

    public cmdStateOfService_onclick(): void {
        this.context.navigate('SOS', BIReportsRoutes.ICABSSESTATEOFSERVICENATACCOUNTGRID,
            {
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName')
            });
    }

    public cmdValue_onclick(): void {
        this.context.navigate('ServiceCoverAll', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVALUEGRID,
            {
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'currentContractType': this.context.riExchange.getCurrentContractType()
            });
    }

    public cmdPlanVisit_onclick(): void {
        this.context.navigate('ServiceCover', InternalGridSearchSalesModuleRoutes.ICABSAPLANVISITGRIDYEAR,
            {
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'currentContractType': this.context.riExchange.getCurrentContractType(),
                'CurrentContractTypeURLParameter': this.context.riExchange.getCurrentContractTypeUrlParam()
            });
    }

    public cmdTreatmentPlan_onclick(): void {
        // iCABSATreatmentPlan
        this.context.showAlert(MessageConstant.Message.PageNotCovered, 3);
    }

    public cmdPlanVisitTabular_onclick(): void {
        if (this.context.pageParams.vEnableTabularView) {
            this.context.navigate('ServiceCover', InternalGridSearchServiceModuleRoutes.ICABSAPLANVISITTABULAR,
                {
                    'AccountNumber': this.context.getControlValue('AccountNumber'),
                    'AccountName': this.context.getControlValue('AccountName'),
                    'ContractNumber': this.context.getControlValue('ContractNumber'),
                    'ContractName': this.context.getControlValue('ContractName'),
                    'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                    'PremiseName': this.context.getControlValue('PremiseName'),
                    'ProductCode': this.context.getControlValue('ProductCode'),
                    'ProductDesc': this.context.getControlValue('ProductDesc'),
                    'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                    'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                    'CurrentContractTypeURLParameter': this.context.riExchange.getCurrentContractTypeUrlParam()
                });
        }
    }

    public cmdStaticVisit_onclick(): void {
        this.context.navigate('ServiceCover', InternalGridSearchSalesModuleRoutes.ICABSASTATICVISITGRIDYEAR,
            {
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'currentContractType': this.context.riExchange.getCurrentContractType()
            });
    }

    public cmdServiceRecommendations_onclick(): void {
        this.context.navigate('ServiceCover', InternalGridSearchServiceModuleRoutes.ICABSARECOMMENDATIONGRID.URL_1,
            {
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'currentContractType': this.context.riExchange.getCurrentContractType()
            });
    }

    public cmdSalesStatsAdjustment_onclick(): void {
        this.context.navigate('ServiceCover', InternalGridSearchSalesModuleRoutes.ICABSSSALESSTATISTICSSERVICEVALUEGRID.CONTRACT,
            {
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'CurrentContractTypeURLParameter': this.context.riExchange.getCurrentContractType()
            });
    }

    public cmdSeasonalService_onclick(): void {
        this.context.navigate('ServiceCover', InternalGridSearchApplicationModuleRoutes.ICABSASERVICECOVERSEASONGRID,
            {
                'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID')
            });
    }

    public onCmdServiceCoverEntitlementClick(): void {
        this.context.pageParams.reloadOnReturn = true;
        this.context.navigate('Entitlement', InternalMaintenanceModuleRoutes.ICABSAENTITLEMENTMAINTENENCE);
    }
    public cmdServiceVisitPlanning_onclick(): void {
        if (this.context.isControlChecked('RequiresManualVisitPlanningInd')) {
            this.context.attributes.Mode = 'Update';
            //riExchange.Mode = 'ServiceCover';
            //location = '/wsscripts/riHTMLWrapper.p?rif(ileName=Application/iCABSAServiceVisitPlanningGrid.htm<maxwidth>';
            this.context.navigate('ServiceCover', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVISITPLANNINGGRID,
                {
                    'ContractNumber': this.context.getControlValue('ContractNumber'),
                    'ContractName': this.context.getControlValue('ContractName'),
                    'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                    'PremiseName': this.context.getControlValue('PremiseName'),
                    'ProductCode': this.context.getControlValue('ProductCode'),
                    'ProductDesc': this.context.getControlValue('ProductDesc'),
                    'CalendarUpdateAllowed': this.context.getControlValue('CalendarUpdateAllowed'),
                    'ServiceVisitAnnivDate': this.context.getControlValue('ServiceVisitAnnivDate'),
                    'ServiceAnnualValue': this.context.getControlValue('ServiceAnnualValue'),
                    'LastChangeEffectDate': this.context.getControlValue('LastChangeEffectDate'),
                    'ServiceCover': this.context.getControlValue('ServiceCoverROWID')
                });

        } else {
            this.context.showAlert(MessageConstant.Message.productNotRequired, 3);
        }
    }

    public cmdLinkedProducts_onclick(): void {
        this.context.attributes.Mode = 'Update';
        //riExchange.Mode = 'ServiceCover';
        //location = '/wsscripts/riHTMLWrapper.p?rif(ileName=Application/iCABSALinkedProductsGrid.htm<maxwidth>' + CurrentContractTypeURLParameter;

        this.context.navigate('ServiceCover', InternalGridSearchSalesModuleRoutes.ICABSALINKEDPRODUCTSGRID,
            {
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'DispenserInd': this.context.getControlValue('DispenserInd'),
                'ConsumableInd': this.context.getControlValue('ConsumableInd'),
                'ServiceVisitFrequency': this.context.getControlValue('ServiceVisitFrequency'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'currentContractType': this.context.riExchange.getCurrentContractType()
            });


    }

    public cmdServiceCalendar_onclick(): void {
        if (!this.context.isControlChecked('RequiresManualVisitPlanningInd')) {
            if (this.context.isControlChecked('AnnualCalendarInd')) {
                this.context.navigate('ServiceCover', InternalGridSearchApplicationModuleRoutes.ICABSASERVICECOVERCALENDARDATEGRID,
                    {
                        'AccountNumber': this.context.getControlValue('AccountNumber'),
                        'AccountName': this.context.getControlValue('AccountName'),
                        'ContractNumber': this.context.getControlValue('ContractNumber'),
                        'ContractName': this.context.getControlValue('ContractName'),
                        'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                        'PremiseName': this.context.getControlValue('PremiseName'),
                        'ProductCode': this.context.getControlValue('ProductCode'),
                        'ProductDesc': this.context.getControlValue('ProductDesc'),
                        'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                        'ServiceCover': this.context.getControlValue('ServiceCoverROWID'),
                        'CalendarUpdateAllowed': this.context.getControlValue('CalendarUpdateAllowed'),
                        'ServiceVisitFrequency': this.context.getControlValue('ServiceVisitFrequency'),
                        'currentContractType': this.context.riExchange.getCurrentContractType()
                    });
            }
        }
    }

    public cmdClosedCalendar_onclick(): void {
        if (!this.context.isControlChecked('RequiresManualVisitPlanningInd')) {
            //riExchange.Mode = 'ServiceCover';
            //location = '/wsscripts/riHTMLWrapper.p?rif(ileName=Application/iCABSAServiceCoverClosedDateGrid.htm<maxwidth>';
            this.context.navigate('ServiceCover', InternalGridSearchApplicationModuleRoutes.ICABSASERVICECOVERCLOSEDDATEGRID);
        }
    }

    public cmdAnnualCalendar_onclick(): void {
        if (!this.context.isControlChecked('RequiresManualVisitPlanningInd')) {
            //riExchange.Mode = 'ServiceCover';
            //location = '/wsscripts/riHTMLWrapper.p?rif(ileName=Application/iCABSAServiceCoverCalendarDatesMaintenance.htm<maxwidth>';
            this.context.navigate('ServiceCover', InternalMaintenanceSalesModuleRoutesConstant.ICABSASERVICECOVERCALENDARDATESMAINTENANCE);

        }
    }

    public cmdSeasonalMaint_onclick(): void {
        if (!this.context.isControlChecked('RequiresManualVisitPlanningInd')) {
            //riExchange.Mode = 'ServiceCover';
            //location = '/wsscripts/riHTMLWrapper.p?rif(ileName=Application/iCABSAServiceCoverSeasonalDatesMaintenance.htm<maxwidth>';
            this.context.navigate('ServiceCover', InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERSEASONALDATESMAINTENANCE);
        }
    }

    public cmdServiceCoverWaste_onclick(): void {
        this.context.navigate('ServiceCover', InternalGridSearchApplicationModuleRoutes.ICABSASERVICECOVERWASTEGRID);
    }

    public cmdWasteConsignmentNoteHistory_onclick(): void {
        const queryParams: Record<string, string> = {
            parentMode: 'Product',
            ContractNumber: this.parent.getControlValue('ContractNumber') || '',
            ContractName: this.parent.getControlValue('ContractName') || '',
            PremiseNumber: this.parent.getControlValue('PremiseNumber') || '',
            PremiseName: this.parent.getControlValue('PremiseName') || '',
            ProductCode: this.parent.getControlValue('ProductCode') || ''
        };
        const path: string = ContractManagementModuleRoutes.ICABSBWASTECONSIGNNOTEHISTORYGRIDFULL;

        this.parent.navigate('Product', path, queryParams); //parentmode would be product when scm page
    }

    public chkRenegContract_onclick(): void {
        if (!this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'chkRenegContract')) {
            if (this.context.isControlChecked('chkRenegContract')) {
                this.context.pageParams.uiDisplay.tdRenegOldContract = true;
                this.context.setRequiredStatus('RenegOldContract', true);
                this.context.setRequiredStatus('RenegOldPremise', true);
                this.context.setRequiredStatus('RenegOldValue', true);
                //if( Update Mode ) { blank out Contract/Premise/Value to be re-entered;
                if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                    this.context.setControlValue('RenegOldContract', '');
                    this.context.setControlValue('RenegOldPremise', '');
                    this.context.setControlValue('RenegOldValue', '');
                }
            } else {
                this.context.pageParams.uiDisplay.tdRenegOldContract = false;
                this.context.setRequiredStatus('RenegOldContract', false);
                this.context.setRequiredStatus('RenegOldPremise', false);
                this.context.setRequiredStatus('RenegOldValue', false);
            }
        }
    }

    public chkFOC_onclick(): void {
        if (!this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'chkFOC')) {
            if (this.context.isControlChecked('chkFOC')) {
                this.context.pageParams.uiDisplay.tdFOCInvoiceStartDate = true;
                this.context.pageParams.uiDisplay.tdFOCProposedAnnualValue = true;
                this.context.setRequiredStatus('FOCInvoiceStartDate', true);
                this.context.setRequiredStatus('FOCProposedAnnualValue', true);
                this.context.setControlValue('ServiceAnnualValue', '0');
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'ServiceAnnualValue');
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    this.context.isControlChecked('DisplayLevelInd')) {
                    this.context.setControlValue('UnitValue', '0');
                    this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'UnitValue');
                }
                // Disable Trial Period Indicator;
                this.context.setControlValue('TrialPeriodInd', false);
                this.context.iCABSAServiceCoverMaintenance7.ToggleTrialPeriodStatus();
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'TrialPeriodInd');
            } else {
                this.context.pageParams.uiDisplay.tdFOCInvoiceStartDate = false;
                this.context.pageParams.uiDisplay.tdFOCProposedAnnualValue = false;
                this.context.setRequiredStatus('FOCInvoiceStartDate', false);
                this.context.setRequiredStatus('FOCProposedAnnualValue', false);
                this.context.setControlValue('FOCInvoiceStartDate', '');
                this.context.setControlValue('FOCProposedAnnualValue', '');
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    this.context.isControlChecked('DisplayLevelInd')) {
                    this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'UnitValue');
                }
                this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'ServiceAnnualValue');
                // Enable Trial Period Indicator if( this is a Contract;
                if (this.context.riExchange.getCurrentContractType() === 'C') {
                    this.context.pageParams.uiDisplay.tdTrialPeriodInd = true;
                    this.context.iCABSAServiceCoverMaintenance7.ToggleTrialPeriodStatus();
                    this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'TrialPeriodInd');
                }
            }
        }
    }

    public cmdEventHistory_onclick(): void {
        this.context.navigate('ServiceCover', InternalGridSearchServiceModuleRoutes.ICABSCMCUSTOMERCONTACTHISTORYGRID);
    }

    public cmdContract_onclick(): void {
        let url = '/contractmanagement/maintenance/contract';
        switch (this.context.riExchange.getCurrentContractType()) {
            case 'P':
                url = ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE;
                break;
            case 'J':
                url = ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE;
                break;
            default:
            case 'C':
                url = ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE;
                break;
        }
        /*this.context.store.dispatch({
            type: ContractActionTypes.SAVE_DATA,
            payload: {
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'ServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'ServiceVisitAnnivDate': this.context.getControlValue('ServiceVisitAnnivDate'),
                'currentContractType': this.context.riExchange.getCurrentContractType()
            }
        });*/
        this.context.navigate('ServiceCover', url,
            {
                'ContractNumber': this.context.getControlValue('ContractNumber')
            });
    }

    public cmdPremise_onclick(): void {
        this.context.navigate('ServiceCover', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
            'contracttypecode': this.context.riExchange.getCurrentContractType()
        });
    }

    public cmdCopyServiceCover_onclick(): void {
        //this.context.navigate('ServiceCoverCopy', 'application/serviceCoverSearch');
        this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
        this.context.serviceCoverCopy.openModal();
    }

    public cmdRefreshDisplayVal_onclick(): void {
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            this.context.isControlChecked('DisplayLevelInd')) {
            this.context.iCABSAServiceCoverMaintenance1.CalDisplayValues();
        }
    }

    public ToggleEntitlementRequired(): void {
        if (!(this.context.isControlChecked('EntitlementRequiredInd'))) {
            if (this.context.riExchange.getCurrentContractType() === 'C') {
                this.context.setRequiredStatus('EntitlementAnnivDate', false);    // RT402;
                this.context.pageParams.dtEntitlementAnnivDate.required = false;
                this.context.setRequiredStatus('EntitlementAnnualQuantity', false);
                this.context.setRequiredStatus('EntitlementNextAnnualQuantity', false); // #19864;
                this.context.setRequiredStatus('EntitlementPricePerUnit', false);
                this.context.setRequiredStatus('EntitlementInvoiceTypeCode', false); // SRS 19;
                this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'EntitlementAnnivDate', false);       // RT402;
                this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'EntitlementAnnualQuantity', false);
                this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'EntitlementNextAnnualQuantity', false); // #19864;
                this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'EntitlementPricePerUnit', false);
                this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'EntitlementInvoiceTypeCode', false); // SRS 19;
                this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'EntitlementServiceQuantity', false);
            }
            this.context.pageParams.uiDisplay.tdEntitlement = false;
            this.context.pageParams.uiDisplay.trEntitlementInvoice = false; // SRS 19;
            this.context.iCABSAServiceCoverMaintenance3.MinCommitQtyToggle();
        } else {
            if (!this.context.getRawControlValue('EntitlementAnnivDate')) {  // SRS 19 Set default date' );
                this.context.setDateToFields('EntitlementAnnivDate',
                    this.context.getRawControlValue('ServiceVisitAnnivDate'));
            }
            if (this.context.riExchange.getCurrentContractType() === 'C') {
                this.context.setRequiredStatus('EntitlementAnnivDate', true);    // RT402;
                this.context.pageParams.dtEntitlementAnnivDate.required = true;
                this.context.setRequiredStatus('EntitlementAnnualQuantity', true);
                this.context.setRequiredStatus('EntitlementNextAnnualQuantity', true); // #19864;
                this.context.setRequiredStatus('EntitlementPricePerUnit', true);
                this.context.setRequiredStatus('EntitlementInvoiceTypeCode', true);  // SRS 19;
                this.context.setRequiredStatus('EntitlementServiceQuantity', false);
            }
            this.context.iCABSAServiceCoverMaintenance3.MinCommitQtyToggle();
            this.context.pageParams.uiDisplay.tdEntitlement = true;
            this.context.pageParams.uiDisplay.trEntitlementInvoice = true;  // SRS 19;
        }
    }

    public MinCommitQtyToggle(): void {
        if (this.context.isControlChecked('MinCommitQty')) {
            if (this.context.pageParams.uiDisplay.trServiceVisitFrequency) {
                this.context.setRequiredStatus('ServiceVisitFrequency', false);
            }

            if (this.context.pageParams.uiDisplay.trServiceAnnualValue) {
                this.context.setRequiredStatus('ServiceAnnualValue', false);
            }

            if (this.context.pageParams.uiDisplay.trServiceQuantity) {
                this.context.setRequiredStatus('ServiceQuantity', false);
            }

            this.context.pageParams.spanEntitlementAnnivDateLab_innerText = 'Minimum Commitment Anniversary Date';
            this.context.pageParams.spanEntitlementAnnualQuantityLab_innerText = 'Minimum Commitment Quantity';
            this.context.pageParams.spanEntitlementNextAnnualQuantityLab_innerText = 'Next Year\'s Minimum Commitment Quantity';
        } else {
            if (this.context.pageParams.uiDisplay.trServiceVisitFrequency) {
                this.context.setRequiredStatus('ServiceVisitFrequency', true);
            }
            if (this.context.pageParams.uiDisplay.trServiceAnnualValue) {
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    this.context.isControlChecked('DisplayLevelInd') &&
                    this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate && this.context.riExchange.URLParameterContains('PendingReduction')) {
                    this.context.setRequiredStatus('ServiceAnnualValue', false);
                } else {
                    this.context.setRequiredStatus('ServiceAnnualValue', true);
                }
            }
            if (this.context.pageParams.uiDisplay.trServiceQuantity === true) {
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    this.context.isControlChecked('DisplayLevelInd') &&
                    this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate && this.context.riExchange.URLParameterContains('PendingReduction')) {
                    this.context.setRequiredStatus('ServiceQuantity', false);
                } else {
                    this.context.setRequiredStatus('ServiceQuantity', true);
                }
            }
            this.context.pageParams.spanEntitlementAnnivDateLab_innerText = 'Entitlement Anniversary Date';
            this.context.pageParams.spanEntitlementAnnualQuantityLab_innerText = 'Annual Entitlement Quantity';
            this.context.pageParams.spanEntitlementNextAnnualQuantityLab_innerText = 'Next Year\'s Entitlement Quantity';
        }
    }

    public btnDefaultServiceQuantity_onClick(): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntryRequests.p';
        this.context.riMaintenance.PostDataAddAction('6');
        this.context.riMaintenance.PostDataAddFunction('GetServiceVisitQuantity');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ServiceVisitFrequency', MntConst.eTypeInteger);
        this.context.PostDataAddFromField('EntitlementAnnualQuantity', MntConst.eTypeInteger);
        this.context.riMaintenance.ReturnDataAdd('EntitlementServiceVisitQuantity', MntConst.eTypeInteger);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.setControlValue('EntitlementServiceQuantity', data['EntitlementServiceVisitQuantity']);
        }, 'POST');
    }

    public MultipleTaxRates_onClick(): void {
        if (this.context.pageParams.vbOverrideMultipleTaxRates && this.context.riMaintenance.CurrentMode !== MntConst.eModeNormal) {
            if (this.context.isControlChecked('MultipleTaxRates')) {
                this.context.pageParams.uiDisplay.trTaxHeadings = true;
                this.context.pageParams.uiDisplay.trTaxMaterials = true;
                this.context.pageParams.uiDisplay.trTaxLabour = true;
                this.context.pageParams.uiDisplay.trTaxReplacement = true;
                this.context.pageParams.uiDisplay.trConsolidateEqualTaxRates = true;
                if (this.context.getRawControlValue('ProductCode') && this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                    this.context.iCABSAServiceCoverMaintenance6.GetDefaultMultipleTaxRates();
                }
            } else {
                this.context.pageParams.uiDisplay.trTaxHeadings = false;
                this.context.pageParams.uiDisplay.trTaxMaterials = false;
                this.context.pageParams.uiDisplay.trTaxLabour = false;
                this.context.pageParams.uiDisplay.trTaxReplacement = false;
                this.context.pageParams.uiDisplay.trConsolidateEqualTaxRates = false;
                this.context.setControlValue('TaxCodeMaterials', '');
                this.context.setControlValue('TaxCodeMaterialsDesc', '');
                this.context.setControlValue('InvoiceTextMaterials', '');
                this.context.setControlValue('TaxCodeLabour', '');
                this.context.setControlValue('TaxCodeLabourDesc', '');
                this.context.setControlValue('InvoiceTextLabour', '');
                this.context.setControlValue('TaxCodeReplacement', '');
                this.context.setControlValue('TaxCodeReplacementDesc', '');
                this.context.setControlValue('InvoiceTextReplacement', '');
                this.context.setControlValue('ConsolidateEqualTaxRates', '');
            }
        }
    }

    // Fix For IUI-3680 - Called Click Event Of Multiple Tax Rates So That It Hides The Dependent Fields
    public clickMultipleTaxRatesOnLoad(): void {
        if (this.context.riMaintenance.CurrentMode !== MntConst.eModeNormal) {
            if (this.context.isControlChecked('MultipleTaxRates')) {
                this.context.pageParams.uiDisplay.trTaxHeadings = true;
                this.context.pageParams.uiDisplay.trTaxMaterials = true;
                this.context.pageParams.uiDisplay.trTaxLabour = true;
                this.context.pageParams.uiDisplay.trTaxReplacement = true;
                this.context.pageParams.uiDisplay.trConsolidateEqualTaxRates = true;
                if (this.context.getRawControlValue('ProductCode') && this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                    this.context.iCABSAServiceCoverMaintenance6.GetDefaultMultipleTaxRates();
                }
            } else {
                this.context.pageParams.uiDisplay.trTaxHeadings = false;
                this.context.pageParams.uiDisplay.trTaxMaterials = false;
                this.context.pageParams.uiDisplay.trTaxLabour = false;
                this.context.pageParams.uiDisplay.trTaxReplacement = false;
                this.context.pageParams.uiDisplay.trConsolidateEqualTaxRates = false;
                this.context.setControlValue('TaxCodeMaterials', '');
                this.context.setControlValue('TaxCodeMaterialsDesc', '');
                this.context.setControlValue('InvoiceTextMaterials', '');
                this.context.setControlValue('TaxCodeLabour', '');
                this.context.setControlValue('TaxCodeLabourDesc', '');
                this.context.setControlValue('InvoiceTextLabour', '');
                this.context.setControlValue('TaxCodeReplacement', '');
                this.context.setControlValue('TaxCodeReplacementDesc', '');
                this.context.setControlValue('InvoiceTextReplacement', '');
                this.context.setControlValue('ConsolidateEqualTaxRates', '');
            }
        }
    }

    public riTab_TabFocusAfterComponent(): void {
        //if (this.context.pageParams.CurrentTabid === 'grdComponent') {
        this.context.pageParams.uiDisplay.trComponentGrid = true;
        this.context.pageParams.uiDisplay.trComponentGridControls = true;
        this.context.pageParams.uiDisplay.trComponentControls = true;
        this.context.iCABSAServiceCoverMaintenance3.BuildComponentGrid();
        this.context.iCABSAServiceCoverMaintenance3.riComponentGrid_BeforeExecute();
        // } else {
        //     this.context.pageParams.uiDisplay.trComponentGrid = false;
        //     this.context.pageParams.uiDisplay.trComponentGridControls = false;
        //     this.context.pageParams.uiDisplay.trComponentControls = false;
        // }
    }

    public riTab_TabFocusAfterDisplays(): void {
        if (this.context.getRawControlValue('CurrentServiceCoverRowID')) {
            //make sure values are reassigned after save;
            if (!this.context.getRawControlValue('riGridHandle')) {
                this.context.pageParams.riDisplayGrid_RequestCacheRefresh = true;
            } else {
                this.context.pageParams.riDisplayGrid_RequestCacheRefresh = false;
            }
            this.context.iCABSAServiceCoverMaintenance3.BuildDisplayGrid();
            this.context.iCABSAServiceCoverMaintenance3.riDisplayGrid_BeforeExecute();
        }
    }

    public riComponentGrid_BeforeExecute(): void {
        let search = this.context.getURLSearchParamObject();
        this.context.riComponentGrid.UpdateHeader = false;
        this.context.riComponentGrid.UpdateBody = true;
        this.context.riComponentGrid.UpdateFooter = true;
        search.set('action', '2');
        search.set('ContractNumber', this.context.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.context.getControlValue('PremiseNumber'));
        search.set('ProductCode', this.context.getControlValue('ProductCode'));
        search.set('ServiceCoverROWID', this.context.getControlValue('ServiceCoverROWID'));
        search.set('CompositeFrequency', this.context.getControlValue('ServiceVisitFrequency'));
        search.set('CompositeQuantity', this.context.getControlValue('ServiceQuantity'));
        search.set('CompositeValue', this.context.getControlValue('ServiceAnnualValue'));
        search.set('GridCacheTime', this.context.getControlValue('ComponentGridCacheTime'));
        search.set('riGridMode', '0');
        search.set('riGridHandle', (Math.floor(Math.random() * 900000) + 100000).toString());
        search.set('PageSize', '10');
        search.set('PageCurrent', this.context.pageParams.riComponentGridPageCurrent.toString());
        search.set('HeaderClickedColumn', this.context.riComponentGrid.HeaderClickedColumn);
        let sortOrder = 'Descending';
        if (!this.context.riComponentGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        search.set('riSortOrder', sortOrder);

        this.context.ajaxSource.next(this.context.ajaxconstant.START);
        this.context.httpService.makeGetRequest(this.context.xhrParams.method, this.context.xhrParams.module,
            this.context.xhrParams.operation, search)
            .subscribe(
                (data) => {
                    if (data) {
                        this.context.pageParams.riComponentGridPageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                        this.context.pageParams.riComponentTotalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        //   this.context.riComponentGrid.Update = true;
                        this.context.riComponentGrid.UpdateBody = true;
                        this.context.riComponentGrid.UpdateFooter = true;
                        this.context.riComponentGrid.UpdateHeader = true;
                        if (data && data.errorMessage) {
                            this.context.showAlert(data, 0, data.fullError);
                        } else {
                            this.context.riComponentGrid.Execute(data);
                        }
                    }
                    this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
                },
                error => {
                    this.context.pageParams.riComponentTotalRecords = 1;
                    this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
                });
    }

    public riComponentGrid_UpdateExecute(): void {
        let search = this.context.getURLSearchParamObject();
        this.context.riComponentGrid.UpdateHeader = false;
        this.context.riComponentGrid.UpdateBody = true;
        this.context.riComponentGrid.UpdateFooter = true;
        search.set('action', '2');
        search.set('ContractNumber', this.context.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.context.getControlValue('PremiseNumber'));
        search.set('ProductCode', this.context.getControlValue('ProductCode'));
        search.set('ServiceCoverROWID', this.context.getControlValue('ServiceCoverROWID'));
        search.set('CompositeFrequency', this.context.getControlValue('ServiceVisitFrequency'));
        search.set('CompositeQuantity', this.context.getControlValue('ServiceQuantity'));
        search.set('CompositeValue', this.context.getControlValue('ServiceAnnualValue'));
        search.set('ROWID', this.context.getAttribute('ROWID'));
        search.set('GridCacheTime', this.context.getControlValue('ComponentGridCacheTime'));
        search.set('riGridMode', '0');
        search.set('riGridHandle', (Math.floor(Math.random() * 900000) + 100000).toString());
        search.set('PageSize', '10');
        search.set('PageCurrent', this.context.pageParams.riComponentGridPageCurrent.toString());
        search.set('HeaderClickedColumn', this.context.riComponentGrid.HeaderClickedColumn);
        let sortOrder = 'Descending';
        if (!this.context.riComponentGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        search.set('riSortOrder', sortOrder);

        this.context.ajaxSource.next(this.context.ajaxconstant.START);
        this.context.httpService.makeGetRequest(this.context.xhrParams.method, this.context.xhrParams.module,
            this.context.xhrParams.operation, search)
            .subscribe(
                (data) => {
                    if (data) {
                        this.context.iCABSAServiceCoverMaintenance3.riComponentGrid_BeforeExecute();
                    }
                },
                error => {
                    this.context.pageParams.riComponentTotalRecords = 1;
                    this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
                });
    }

    public riComponentGrid_BodyOnClick(event: any): void {
        if (!this.context.pageParams.cmdSRAGenerateText.disabled) { // Only allow when adding/updating etc...;
            this.context.iCABSAServiceCoverMaintenance3.SelectedRowFocus(event.srcElement.parentElement.parentElement.children[0].children[0]);
        }
    }

    public riDisplayGrid_BeforeExecute(): void {
        //don//t refresh the grid when using the refresh button;
        // if (riDisplayGrid.Mode !== 3 && riDisplayGrid.Action === 2 && not this.pageParams.vb Abandon ) {
        //     riDisplayGrid.RequestCacheRefresh = false;
        // }
        // //make sure values are reassigned after save;
        // if (riGridHandle.value === true) {
        //     riDisplayGrid.RequestCacheRefresh = true;
        // }
        // riDisplayGrid.BusinessObjectPostData = 'BusinessCode=' & riExchange.Functions.ClientSideValues.Fetch('BusinessCode') & _;
        // '&ContractNumber=' & ContractNumber.value & _;
        // '&PremiseNumber=' & PremiseNumber.value & _;
        // '&ProductCode=' & ProductCode.value & _;
        // '&ServiceCoverROWID=' & this.context.riMaintenance.GetRowID('ServiceCover') & _;
        // '&ServiceCoverItemRowID=' & grdServiceCoverItem.getAttribute('ServiceCoverItemRowID') & _;
        // '&DeleteSCItem=' & GetLogicalStringValue(blnDeleteServiceCoverItem) & _;
        // '&EffectiveDate=' & LastChangeEffectDate.value & _;
        // '&GridType=Main';
        // if (riDisplayGrid.Update) {
        //     riDisplayGrid.StartRow = grdServiceCoverItem.getAttribute('Row');
        //     riDisplayGrid.StartColumn = 0;
        //     riDisplayGrid.RowID = grdServiceCoverItem.getAttribute('ServiceCoverItemRowID');
        //     riDisplayGrid.UpdateHeader = false;
        //     riDisplayGrid.UpdateBody = true;
        //     riDisplayGrid.UpdateFooter = false;
        //     this.pageParams.vb Update                   = true;
        // }
        let search = this.context.getURLSearchParamObject();
        this.context.riDisplayGrid.UpdateHeader = false;
        this.context.riDisplayGrid.UpdateBody = true;
        this.context.riDisplayGrid.UpdateFooter = true;
        let RequestCacheRefresh = false;
        if (!this.context.pageParams.riGridHandlevalue) {
            this.context.pageParams.riGridHandlevalue = (Math.floor(Math.random() * 900000) + 100000).toString();
            this.context.setControlValue('riGridHandle', this.context.pageParams.riGridHandlevalue);
            RequestCacheRefresh = true;
        }
        search.set('action', '2');
        search.set('ContractNumber', this.context.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.context.getControlValue('PremiseNumber'));
        search.set('ProductCode', this.context.getControlValue('ProductCode'));
        search.set('ServiceCoverROWID', this.context.getControlValue('ServiceCoverROWID'));
        search.set('ServiceCoverItemRowID', this.context.getAttribute('ServiceCoverItemRowID') ? this.context.getAttribute('ServiceCoverItemRowID') : '');
        search.set('DeleteSCItem', this.context.pageParams.blnDeleteServiceCoverItem ? 'true' : 'false');
        search.set('EffectiveDate', this.context.getControlValue('LastChangeEffectDate') ? this.context.getControlValue('LastChangeEffectDate') : '');
        search.set('GridType', 'Main');
        search.set('riGridMode', '0');
        search.set('riGridHandle', this.context.pageParams.riGridHandlevalue);
        search.set('PageSize', '10');
        if (RequestCacheRefresh) {
            search.set('riCacheRefresh', 'true');
        }
        search.set('PageCurrent', this.context.pageParams.riDisplayGridPageCurrent.toString());
        search.set('HeaderClickedColumn', this.context.riDisplayGrid.HeaderClickedColumn);
        let sortOrder = 'Descending';
        if (!this.context.riDisplayGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        search.set('riSortOrder', sortOrder);

        this.context.ajaxSource.next(this.context.ajaxconstant.START);
        this.context.httpService.makeGetRequest(this.context.xhrParams.method, this.context.xhrParams.module,
            this.context.xhrParams.operation, search)
            .subscribe(
                (data) => {
                    if (data) {
                        this.context.pageParams.DisplayGridPageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                        this.context.pageParams.riDisplayGridTotalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        //this.context.riDisplayGrid.Update = true;
                        this.context.riDisplayGrid.UpdateBody = true;
                        this.context.riDisplayGrid.UpdateFooter = true;
                        this.context.riDisplayGrid.UpdateHeader = true;
                        if (data && data.errorMessage) {
                            this.context.showAlert(data, 0, data.fullError);
                        } else {
                            this.context.riDisplayGrid.Execute(data);
                            this.context.iCABSAServiceCoverMaintenance3.riDisplayGrid_AfterExecute(data);
                        }
                    }
                    this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
                },
                error => {
                    this.context.pageParams.riDisplayGridTotalRecords = 1;
                    this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
                });
    }

    public riDisplayGrid_UpdateExecute(): void {
        let search = this.context.getURLSearchParamObject();
        this.context.riDisplayGrid.UpdateHeader = false;
        this.context.riDisplayGrid.UpdateBody = true;
        this.context.riDisplayGrid.UpdateFooter = true;
        search.set('action', '2');
        search.set('ContractNumber', this.context.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.context.getControlValue('PremiseNumber'));
        search.set('ProductCode', this.context.getControlValue('ProductCode'));
        search.set('ServiceCoverROWID', this.context.getControlValue('ServiceCoverROWID'));
        search.set('ServiceCoverItemRowID', this.context.getAttribute('ServiceCoverItemRowID') ? this.context.getAttribute('ServiceCoverItemRowID') : '');
        search.set('DeleteSCItem', this.context.pageParams.blnDeleteServiceCoverItem ? 'True' : 'False');
        search.set('EffectiveDate', this.context.getControlValue('LastChangeEffectDate') ? this.context.getControlValue('LastChangeEffectDate') : '');
        search.set('GridType', 'Main');
        search.set('ROWID', this.context.getAttribute('ServiceCoverItemRowID') ? this.context.getAttribute('ServiceCoverItemRowID') : '');
        search.set('riGridMode', '0');
        search.set('riGridHandle', this.context.pageParams.riGridHandlevalue);
        search.set('PageSize', '10');
        search.set('PageCurrent', this.context.pageParams.riDisplayGridPageCurrent.toString());
        search.set('HeaderClickedColumn', this.context.riDisplayGrid.HeaderClickedColumn);
        let sortOrder = 'Descending';
        if (!this.context.riDisplayGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        search.set('riSortOrder', sortOrder);
        this.context.ajaxSource.next(this.context.ajaxconstant.START);
        this.context.httpService.makeGetRequest(this.context.xhrParams.method, this.context.xhrParams.module,
            this.context.xhrParams.operation, search)
            .subscribe(
                (data) => {
                    if (data) {
                        //this.context.riDisplayGrid.Update = true;
                        this.context.riDisplayGrid.UpdateBody = true;
                        this.context.riDisplayGrid.UpdateFooter = true;
                        this.context.riDisplayGrid.UpdateHeader = true;
                        if (data && data.errorMessage) {
                            this.context.showAlert(data, 0, data.fullError);
                        } else {
                            this.context.iCABSAServiceCoverMaintenance3.riDisplayGrid_AfterUpdate();
                        }
                    }
                    this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
                },
                error => {
                    this.context.pageParams.riDisplayGridTotalRecords = 1;
                    this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
                });
    }

    public riDisplayGrid_UpdateEditExecute(): void {
        let search = this.context.getURLSearchParamObject();
        this.context.riDisplayGrid.UpdateHeader = false;
        this.context.riDisplayGrid.UpdateBody = true;
        this.context.riDisplayGrid.UpdateFooter = true;
        search.set('action', '2');
        search.set('ItemDescriptionRowID', this.context.getAttribute('ServiceCoverItemRowID') ? this.context.getAttribute('ServiceCoverItemRowID') : '');
        search.set('ItemDescription', this.context.riDisplayGrid.Details.GetValue('ItemDescription'));
        search.set('Component1', this.context.riDisplayGrid.Details.GetValue('Component1'));
        search.set('Component2', this.context.riDisplayGrid.Details.GetValue('Component2'));
        search.set('Component3', this.context.riDisplayGrid.Details.GetValue('Component3'));
        search.set('CommenceDate', this.context.getControlValue('ServiceCommenceDate') ? this.context.getControlValue('ServiceCommenceDate') : '');
        search.set('WED', this.context.riDisplayGrid.Details.GetValue('WED'));
        search.set('AnnualValue', this.context.riDisplayGrid.Details.GetValue('AnnualValue'));
        search.set('GridMaterialsValueRowID', this.context.getAttribute('ServiceCoverItemRowID') ? this.context.getAttribute('ServiceCoverItemRowID') : '');
        search.set('GridMaterialsValue', this.context.globalize.parseCurrencyToFixedFormat(this.context.riDisplayGrid.Details.GetValue('GridMaterialsValue')));
        search.set('GridLabourValueRowID', this.context.getAttribute('ServiceCoverItemRowID') ? this.context.getAttribute('ServiceCoverItemRowID') : '');
        search.set('GridLabourValue', this.context.globalize.parseCurrencyToFixedFormat(this.context.riDisplayGrid.Details.GetValue('GridLabourValue')));
        search.set('GridReplacementValueRowID', this.context.getAttribute('ServiceCoverItemRowID') ? this.context.getAttribute('ServiceCoverItemRowID') : '');
        search.set('GridReplacementValue', this.context.globalize.parseCurrencyToFixedFormat(this.context.riDisplayGrid.Details.GetValue('GridReplacementValue')));
        search.set('ContractNumber', this.context.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.context.getControlValue('PremiseNumber'));
        search.set('ProductCode', this.context.getControlValue('ProductCode'));
        search.set('ContractNumber', this.context.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.context.getControlValue('PremiseNumber'));
        search.set('ProductCode', this.context.getControlValue('ProductCode'));
        search.set('ServiceCoverROWID', this.context.getControlValue('ServiceCoverROWID'));
        search.set('ServiceCoverItemRowID', this.context.getAttribute('ServiceCoverItemRowID') ? this.context.getAttribute('ServiceCoverItemRowID') : '');
        search.set('DeleteSCItem', this.context.pageParams.blnDeleteServiceCoverItem ? 'True' : 'False');
        search.set('EffectiveDate', this.context.getControlValue('LastChangeEffectDate') ? this.context.getControlValue('LastChangeEffectDate') : '');
        search.set('GridType', 'Main');
        search.set('ROWID', this.context.getAttribute('ServiceCoverItemRowID') ? this.context.getAttribute('ServiceCoverItemRowID') : '');
        search.set('riGridMode', '3');
        search.set('riGridHandle', this.context.pageParams.riGridHandlevalue);
        search.set('PageSize', '10');
        search.set('PageCurrent', this.context.pageParams.riDisplayGridPageCurrent.toString());
        search.set('HeaderClickedColumn', this.context.riDisplayGrid.HeaderClickedColumn);
        let sortOrder = 'Descending';
        if (!this.context.riDisplayGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        search.set('riSortOrder', sortOrder);
        this.context.ajaxSource.next(this.context.ajaxconstant.START);
        this.context.httpService.makeGetRequest(this.context.xhrParams.method, this.context.xhrParams.module,
            this.context.xhrParams.operation, search)
            .subscribe(
                (data) => {
                    if (data) {
                        //this.context.riDisplayGrid.Update = true;
                        this.context.riDisplayGrid.UpdateBody = true;
                        this.context.riDisplayGrid.UpdateFooter = true;
                        this.context.riDisplayGrid.UpdateHeader = true;
                        if (data && data.errorMessage) {
                            this.context.showAlert(data, 0, data.fullError);
                        } else {
                            this.context.riExchange.updateCtrl(this.context.controls, 'riGridHandle', 'ignoreSubmit', false);
                            this.context.iCABSAServiceCoverMaintenance3.riDisplayGrid_AfterUpdate();
                        }
                    }
                    this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
                },
                error => {
                    this.context.pageParams.riDisplayGridTotalRecords = 1;
                    this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
                });
    }

    public riDisplayGrid_AfterUpdate(): void {
        this.context.iCABSAServiceCoverMaintenance3.CalcDisplayValueChanges();
        this.context.pageParams.vbUpdate = false;
        //this.context.riDisplayGrid.Update = false;
        this.context.pageParams.blnDeleteServiceCoverItem = false;
        setTimeout(() => {
            this.context.iCABSAServiceCoverMaintenance3.riDisplayGrid_BeforeExecute();
        }, 100);

    }

    public riDisplayGrid_AfterExecute(data: any): void {
        this.context.pageParams.blnGridHasData = (data.length > 0);
        if (this.context.pageParams.blnGridHasData) {
            //this.context.pageParams.riGridHandlevalue = this.context.riDisplayGrid.Details.GetAttribute('Component1', 'AdditionalProperty');
            if (this.context.pageParams.vbUpdate) {
                this.context.iCABSAServiceCoverMaintenance3.CalcDisplayValueChanges();
                this.context.pageParams.vbUpdate = false;
            }
        }
        //this.context.riDisplayGrid.Update = false;
        this.context.pageParams.blnDeleteServiceCoverItem = false;
    }

    public SelectedRowFocus(rsrcElement: any): void {
        rsrcElement.select();
        this.context.setAttribute('Row', rsrcElement.parentElement.parentElement.parentElement.sectionRowIndex);
        this.context.setAttribute('Cell', rsrcElement.parentElement.parentElement.cellIndex);
        this.context.setAttribute('ROWID', rsrcElement.getAttribute('RowID'));
        rsrcElement.focus();
    }

    public riComponentGrid_BodyOnDblClick(event: any): void {
        if (!this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'cmdComponentSelAll')) { // Only allow when adding/updating etc...;
            if (this.context.riComponentGrid.CurrentColumnName === 'ComponentSelected') {
                this.context.iCABSAServiceCoverMaintenance3.SelectedRowFocus(event.srcElement.parentElement.parentElement.parentElement.children[0].children[0].children[0]);
                //    this.context.riComponentGrid.Update = true;
                this.context.iCABSAServiceCoverMaintenance3.riComponentGrid_UpdateExecute();
            }
            this.context.iCABSAServiceCoverMaintenance3.SelectedRowFocus(event.srcElement.parentElement.parentElement.parentElement.children[0].children[0].children[0]);
        }
    }

    public CreateComponentGrid(): void {
        this.context.riComponentGrid.DefaultBorderColor = 'ADD8E6';
        this.context.riComponentGrid.DefaultTextColor = '0000FF';
        this.context.riComponentGrid.PageSize = 10;
        this.context.riComponentGrid.FunctionPaging = true;
        this.context.riComponentGrid.HighlightBar = true;
    }

    public BuildComponentGrid(): void {
        this.context.riComponentGrid.Clear();
        this.context.riComponentGrid.AddColumn('ComponentProduct', 'Component', 'ComponentProduct', MntConst.eTypeCode, 6, false);
        this.context.riComponentGrid.AddColumn('ComponentDescription', 'Component', 'ComponentDescription', MntConst.eTypeTextFree, 20, false);
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.riComponentGrid.AddColumn('ComponentQuantity', 'Component', 'ComponentQuantity', MntConst.eTypeInteger, 5, false);
            //if( Not Pricing By Header ) { Show Component Pricing;
            //   if( CompositePricingType.value !== '1' ) { - JBARN Decided Not Required;
            this.context.riComponentGrid.AddColumn('ComponentAnnualValue', 'Component', 'ComponentAnnualvalue', MntConst.eTypeCurrency, 15, false);
            //    }
            this.context.riComponentGrid.AddColumn('ComponentExists', 'Component', 'ComponentExists', MntConst.eTypeImage, 1, false);
        }
        this.context.riComponentGrid.AddColumn('ServiceCoverQuantity', 'Component', 'ServiceCoverQuantity', MntConst.eTypeInteger, 5, false);
        this.context.riComponentGrid.AddColumn('ServiceCoverFrequency', 'Component', 'ServiceCoverFrequency', MntConst.eTypeInteger, 5, false);
        // if( CompositePricingType.value !== '1' || this.context.riMaintenance.CurrentMode !== eModeAdd ) { REMOVED - JBARN Decided Not Required;
        //if this.context.riMaintenance.CurrentMode !== eModeAdd ) {
        this.context.riComponentGrid.AddColumn('ServiceCoverAnnualValue', 'Component', 'ServiceCoverAnnualvalue', MntConst.eTypeCurrency, 15, false);
        //}
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) { // When Updating Only Show CURRENT Service Cover Settings;
            this.context.riComponentGrid.AddColumn('ComponentSelected', 'Component', 'ComponentSelected', MntConst.eTypeImage, 1, false);
        }
        this.context.riComponentGrid.Complete();
    }

    public CreateDisplayGrid(): void {
        this.context.riDisplayGrid.DefaultBorderColor = 'DDDDDD';
        this.context.riDisplayGrid.PageSize = 10;
        this.context.riDisplayGrid.FunctionPaging = true;
        this.context.riDisplayGrid.HighlightBar = true;
        this.context.riDisplayGrid.FunctionUpdateSupport = true; // Updateable Grid
    }

    public BuildDisplayGrid(): void {
        this.context.riDisplayGrid.Clear();
        this.context.riDisplayGrid.AddColumn('ItemDescription', 'Display', 'ItemDescription', MntConst.eTypeCode, 20);
        this.context.riDisplayGrid.AddColumn('Component1', 'Display', 'Component1', MntConst.eTypeText, 20);
        this.context.riDisplayGrid.AddColumn('Component2', 'Display', 'Component2', MntConst.eTypeText, 20);
        this.context.riDisplayGrid.AddColumn('Component3', 'Display', 'Component3', MntConst.eTypeText, 20);
        this.context.riDisplayGrid.AddColumn('GridMaterialsValue', 'Display', 'GridMaterialsValue', MntConst.eTypeCurrency, 8);
        this.context.riDisplayGrid.AddColumn('GridLabourValue', 'Display', 'GridLabourValue', MntConst.eTypeCurrency, 8);
        this.context.riDisplayGrid.AddColumn('GridReplacementValue', 'Display', 'GridReplacementValue', MntConst.eTypeCurrency, 8);
        this.context.riDisplayGrid.AddColumn('CommenceDate', 'Display', 'CommenceDate', MntConst.eTypeDate, 10);
        this.context.riDisplayGrid.AddColumn('WED', 'Display', 'WED', MntConst.eTypeInteger, 6);
        this.context.riDisplayGrid.AddColumn('AnnualValue', 'Display', 'AnnualValue', MntConst.eTypeCurrency, 6);
        this.context.riDisplayGrid.AddColumn('DeletedDate', 'Display', 'DeletedDate', MntConst.eTypeDate, 10);
        this.context.riDisplayGrid.AddColumn('DeleteServiceCoverItem', 'Display', 'DeleteServiceCoverItem', MntConst.eTypeImage, 1, true);
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            this.context.riDisplayGrid.AddColumnUpdateSupport('GridMaterialsValue', true);
            this.context.riDisplayGrid.AddColumnUpdateSupport('GridLabourValue', true);
            this.context.riDisplayGrid.AddColumnUpdateSupport('GridReplacementValue', true);
        }
        this.context.riDisplayGrid.Complete();
    }

    public tbodySCDispGrid_onDblClick(event: any): void {
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            if (this.context.riDisplayGrid.CurrentColumnName === 'DeleteServiceCoverItem' && event.srcElement.parentElement.parentElement.tagName
                && event.srcElement.parentElement.parentElement.tagName.toUpperCase() === 'TD') {
                if (!this.context.getControlValue('LastChangeEffectDate')) {
                    this.context.iCABSAServiceCoverMaintenance3.DisplayValueError('');
                    this.context.messageModalCallback = this.context.renderTab(1);
                } else {
                    this.context.iCABSAServiceCoverMaintenance3.UpdateDisplayDelete(event);
                }
            }
        }
    }

    public UpdateDisplayDelete(event: any): void {
        let oTR = event.srcElement.parentElement.parentElement;
        if (oTR.tagName && oTR.tagName.toUpperCase() === 'TD') {
            oTR = event.srcElement.parentElement.parentElement.parentElement;
        }
        //this.context.riDisplayGrid.Update = true;
        this.context.setAttribute('ServiceCoverItemRowID', oTR.children[0].children[0].children[0].getAttribute('RowID'));
        this.context.setAttribute('Row', oTR.sectionRowIndex);
        this.context.pageParams.blnDeleteServiceCoverItem = true;
        this.context.iCABSAServiceCoverMaintenance3.riDisplayGrid_UpdateExecute();
    }

    public riDisplayGrid_BodyColumnFocus(event: any): void {
        let currentColumnName = this.context.riDisplayGrid.CurrentColumnName;
        if (currentColumnName === 'GridMaterialsValue' || currentColumnName === 'GridLabourValue' || currentColumnName === 'GridReplacementValue') {
            let selectedColumnName = currentColumnName.indexOf('GridMaterialsValue') !== -1 ?
                'Materials' : currentColumnName.indexOf('GridLabourValue') !== -1 ? 'Labour' : 'Replacement';

            this.context.setAttribute('Orig' + selectedColumnName + 'Value', this.context.riDisplayGrid.Details.GetAttribute(currentColumnName, 'AdditionalProperty'));
            this.context.setAttribute('ServiceCoverItemRowID', this.context.riDisplayGrid.Details.GetAttribute(currentColumnName, 'RowID'));
            this.context.setAttribute('Row', this.context.riDisplayGrid.CurrentRow);
            this.context['prev' + selectedColumnName + 'Value'] = event.srcElement.value;
            this.context.riDisplayGrid.Mode = MntConst.eModeUpdate;
        }
    }

    public riDisplayGrid_BodyColumnLostFocus(event: any): void {
        this.context.riDisplayGrid.Mode = MntConst.eModeNormal;
        let currentElemetVal = event.srcElement.value;

        let currentColumnName = this.context.riDisplayGrid.CurrentColumnName;
        if (currentColumnName === 'GridMaterialsValue' || currentColumnName === 'GridLabourValue' || currentColumnName === 'GridReplacementValue') {

            let selectedColumnName = currentColumnName.indexOf('GridMaterialsValue') !== -1 ?
                'Materials' : currentColumnName.indexOf('GridLabourValue') !== -1 ? 'Labour' : 'Replacement';

            if (this.context['prev' + selectedColumnName + 'Value'] !== currentElemetVal) {

                if (!this.isNumericValue(currentElemetVal)) {
                    event.srcElement.value = this.context.globalize.formatCurrencyToLocaleFormat(this.context.getAttribute('Orig' + selectedColumnName + 'Value'));
                } else if (this.context.CDbl(selectedColumnName) > this.context.CDbl(this.context.getAttribute('Orig' + selectedColumnName + 'Value'))
                    || ((!this.context.getControlValue('LastChangeEffectDate')) && this.context.CDbl(selectedColumnName)
                        !== this.context.CDbl(this.context.getAttribute('Orig' + selectedColumnName + 'Value')))) {
                    this.context.iCABSAServiceCoverMaintenance3.DisplayValueError(selectedColumnName);
                    event.srcElement.value = this.context.getAttribute('Orig' + selectedColumnName + 'Value');
                } else {
                    let NewTotal = this.context.CDbl(this.context.globalize.parseCurrencyToFixedFormat(this.context.riDisplayGrid.Details.GetValue('GridMaterialsValue')).toString()) +
                        this.context.CDbl(this.context.globalize.parseCurrencyToFixedFormat(this.context.riDisplayGrid.Details.GetValue('GridLabourValue')).toString()) +
                        this.context.CDbl(this.context.globalize.parseCurrencyToFixedFormat(this.context.riDisplayGrid.Details.GetValue('GridReplacementValue')).toString());

                    this.context.riDisplayGrid.Details.SetValue('AnnualValue', this.context.globalize.formatCurrencyToLocaleFormat(parseFloat(NewTotal).toFixed(2)));

                    this.context.iCABSAServiceCoverMaintenance3.riDisplayGrid_UpdateEditExecute();
                }
            }
        }
    }

    public isNumericValue(val: any): boolean {
        val = this.context.globalize.parseCurrencyToFixedFormat(val);
        try {
            return !isNaN(parseInt(val.toString(), 10));
        } catch (e) {
            return false;
        }
    }

    public CalcDisplayValueChanges(): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverReduceDisplayGrid.p';
        this.context.riMaintenance.PostDataAddAction('6');
        this.context.riMaintenance.PostDataAddFunction('TotalDisplayValues');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeText);
        this.context.PostDataAddFromField('PremiseNumber', MntConst.eTypeInteger);
        this.context.PostDataAddFromField('ProductCode', MntConst.eTypeText);
        this.context.riMaintenance.PostDataAdd('ServiceCoverROWID', this.context.riExchange.getParentAttributeValue('ServiceCover'), MntConst.eTypeText);
        this.context.PostDataAddParamFromField('EffectiveDate', 'LastChangeEffectDate', MntConst.eTypeDate);
        this.context.riMaintenance.PostDataAdd('GridType', 'Main', MntConst.eTypeText);
        this.context.riMaintenance.PostDataAdd('riGridHandle', this.context.pageParams.riGridHandlevalue, MntConst.eTypeTextFree);
        this.context.riMaintenance.ReturnDataAdd('Quantity', MntConst.eTypeInteger);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.setControlValue('NewTotalValue', data['DisplayTotal']);
            this.context.setControlValue('OrigTotalValue', data['OrigTotal']);
            this.context.setControlValue('ServiceAnnualValue', data['DisplayTotal']);
            this.context.setControlValue('AnnualValueChange', (parseFloat(data['DisplayTotal']) - parseFloat(data['OrigTotal'])).toFixed(2));
            this.context.attributes.AnnualValueChange_riChange = true;
            this.context.setControlValue('ServiceQuantity', data['Quantity']);
            this.context.riExchange.riInputElement.MarkAsDirty(this.context.uiForm, 'ServiceAnnualValue');
            this.context.riExchange.riInputElement.MarkAsDirty(this.context.uiForm, 'ServiceQuantity');
            this.context.riExchange.riInputElement.MarkAsDirty(this.context.uiForm, 'AnnualValueChange');
            this.context.attributes.ServiceQuantity_riChange = true;
            this.context.iCABSAServiceCoverMaintenance2.AnnualValueChangeonBlur();
        }, 'POST');
    }

    public DisplayValueError(ipErrorType: any): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverReduceDisplayGrid.p';
        this.context.riMaintenance.PostDataAddAction('6');
        this.context.riMaintenance.PostDataAddFunction('DisplayValueError');
        this.context.riMaintenance.PostDataAdd('ErrorType', ipErrorType, MntConst.eTypeText);
        this.context.PostDataAddFromField('LastChangeEffectDate', MntConst.eTypeDate);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            if (data['ErrorMessage']) {
                this.context.showAlert(data['ErrorMessage'], 0, data.fullError);
            }
        }, 'POST');
    }

    public cmdComponentSelAll_OnClick(): void {
        this.MultiSetComponents(true);
    }

    public cmdComponentDesAll_OnClick(): void {
        this.MultiSetComponents(false);
    }

    public MultiSetComponents(lSelectAll: any): void {
        let cFunction;
        cFunction = lSelectAll ? 'SelectAll' : 'DeselectAll';
        this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverComponentGrid.p';
        this.context.riMaintenance.clear();
        this.context.riMaintenance.PostDataAddAction('6');
        this.context.riMaintenance.PostDataAddFunction(cFunction);
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeText);
        this.context.PostDataAddFromField('PremiseNumber', MntConst.eTypeText);
        this.context.PostDataAddFromField('ProductCode', MntConst.eTypeText);
        this.context.PostDataAddParamFromField('CompositeFrequency', 'ServiceVisitFrequency', MntConst.eTypeText);
        this.context.PostDataAddParamFromField('CompositeQuantity', 'ServiceQuantity', MntConst.eTypeText);
        this.context.PostDataAddParamFromField('CompositeValue', 'ServiceAnnualValue', MntConst.eTypeText);
        this.context.PostDataAddParamFromField('GridCacheTime', 'ComponentGridCacheTime', MntConst.eTypeText);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.iCABSAServiceCoverMaintenance3.riComponentGrid_BeforeExecute();
        }, 'POST');
    }

    public BusinessOriginDetailDisplay(): void {
        if (this.context.isControlChecked('DetailRequiredInd')) {
            this.context.pageParams.uiDisplay.trBusinessOriginDetailCode = true;
            this.context.setRequiredStatus('BusinessOriginDetailCode', true);
            if (!this.context.getControlValue('BusinessOriginDetailDesc')) {
                this.context.iCABSAServiceCoverMaintenanceVTs.callVTBusinessOriginDetailCode();
            }
        } else if (!this.context.pageParams.businessOriginDetailCode) {
            this.context.pageParams.uiDisplay.trBusinessOriginDetailCode = false;
            this.context.setControlValue('BusinessOriginDetailCode', '');
            this.context.setControlValue('BusinessOriginDetailDesc', '');
            this.context.setRequiredStatus('BusinessOriginDetailCode', false);
        }
    }

    public CheckWasteTransferRequired(): void {
        this.context.setRequiredStatus('WasteTransferTypeCode', this.context.isControlChecked('RequiresWasteTransferType'));
    }

    public ShowAverageWeight(showSpinner?: boolean): void {
        if (this.context.pageParams.hasFetchedAverageWeight || this.context.isReturning()) {
            return;
        }
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.clear();
        this.context.riMaintenance.PostDataAddFunction('GetAverageWeightStatus');
        this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeText);
        this.context.PostDataAddFromField('PremiseNumber', MntConst.eTypeInteger);
        this.context.PostDataAddFromField('ProductCode', MntConst.eTypeText);
        this.context.riMaintenance.ReturnDataAdd('NoAverageWeight', MntConst.eTypeCheckBox);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.pageParams.hasFetchedAverageWeight = true;
            this.context.pageParams.uiDisplay.trAverageWeight = !data['NoAverageWeight'];
            if (this.context.pageParams.uiDisplay.trAverageWeight) {
                if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd || this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                    if (this.context.pageParams.vbEnableServiceCoverAvgWeightReq) {
                        if (this.context.pageParams.vbEnableServiceCoverAvgWeightText) {
                            this.context.iCABSAServiceCoverMaintenance3.EnableAvgWeightForUser();
                        } else {
                            this.context.riMaintenance.EnableInput('AverageWeight');
                        }
                    } else {
                        this.context.riMaintenance.DisableInput('AverageWeight');
                    }
                }
            }
        }, 'POST', undefined, showSpinner);
    }

    public EnableAvgWeightForUser(): void {
        let bEnableAverageWeightForUser;
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.clear();
        this.context.riMaintenance.PostDataAddAction('6');
        this.context.riMaintenance.PostDataAddFunction('EnableAverageWeight');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.riMaintenance.PostDataAdd('EnableAverageWeightText', this.context.pageParams.vbEnableServiceCoverAvgWeightText, MntConst.eTypeText);
        this.context.riMaintenance.ReturnDataAdd('EnableAverageWeight', MntConst.eTypeCheckBox);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            bEnableAverageWeightForUser = data['EnableAverageWeight'];
            if (bEnableAverageWeightForUser) {
                this.context.riMaintenance.EnableInput('AverageWeight');
            } else {
                this.context.riMaintenance.DisableInput('AverageWeight');
            }
        }, 'POST');
    }

    public BuildTaxCodeCombo(): void {
        let strReturn, Arr, Pair;
        if (!this.context || !this.context.utils || this.context.pageParams.hasBuiltTaxCombo || this.context.isReturning()) {
            return;
        }
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntryRequests.p';
        this.context.riMaintenance.PostDataAddFunction('BuildTaxCodeCombo');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.pageParams.hasBuiltTaxCombo = true;
            this.context.pageParams.selTaxCode = [];
            strReturn = data['TaxCodeList'];
            if (strReturn) {
                Arr = strReturn.split(String.fromCharCode(1));
                for (let i = 0; i < Arr.length; i++) {
                    Pair = Arr[i].split('|');
                    let obj = {
                        text: Pair[0] + ' - ' + Pair[1],
                        value: Pair[0]
                    };
                    this.context.pageParams.selTaxCode.push(obj);
                }
                setTimeout(function (): void {
                    this.context.setControlValue('selTaxCode', this.context.getControlValue('TaxCode'));
                }.bind(this), 1000);
            }
        }, 'POST', 6, false);
    }

    public SetReplacementIncInd(): void {
        switch (this.context.getRawControlValue('ReplacementIncludeInd')) {

            case 'yes':
                this.context.pageParams.rsPlantReplaceIndyes = true;
                break;
            case 'no':
                this.context.pageParams.rsPlantReplaceIndno = true;
                break;
            case 'Mixed':
                this.context.pageParams.rsPlantReplaceIndmixed = true;
                break;
        }
    }

    public riExchange_UpdateHTMLDocument(): void {

        if (this.context.parentMode === 'ServiceCoverCopy') {
            this.ServiceVisitFrequency_OnChange();

            if (this.context.pageParams.blnUseVisitCycleValues) {
                this.context.setControlValue('VisitCycleInWeeks', this.context.getRawControlValue('CopiedVisitCycleInWeeks'));
                this.context.iCABSAServiceCoverMaintenance2.VisitCycleInWeeks_OnChange();
                this.context.setControlValue('VisitsPerCycle', this.context.getRawControlValue('CopiedVisitsPerCycle'));
                this.context.iCABSAServiceCoverMaintenance2.VisitsPerCycle_OnChange();
                this.context.setControlValue('VisitCycleInWeeksOverrideNote', this.context.getRawControlValue('CopiedVisitCycleInWeeksOverrideNote'));
            }

        }

    }

}
