import { Location } from '@angular/common';
import { Component, OnInit, AfterContentInit, Injector, ViewChild } from '@angular/core';

import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MUserSearchComponent } from '@app/internal/search/riMUserSearch.component';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { ModalAdvService } from '@shared/components/modal-adv/modal-adv.service';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSSTeamUserMaintenance.html',
    providers: [CommonLookUpUtilsService]
})
export class TeamUSerMaintenanceComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    public controls: IControls[] = [
        { name: 'AdministratorInd', type: MntConst.eTypeCheckBox },
        { name: 'DefaultTeamInd', type: MntConst.eTypeCheckBox },
        { name: 'ROWID' },
        { name: 'TeamClosureSecurityLevel', value: '0' },
        { name: 'TeamID', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'TeamSystemName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'UserCode', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'UserName', disabled: true, required: false, type: MntConst.eTypeText }
    ];
    public pageId: string = '';
    public pageTitle: string;
    public pageMode: string;
    public updateMode: boolean = false;
    public ellipsis: Record<string, any> = {
        riMUserSearch: {
            inputParams: {
                parentMode: 'Search',
                showAddNew: false
            },
            autoOpen: false,
            showHeader: true,
            isEllipsisDisabled: true,
            contentComponent: MUserSearchComponent
        }
    };
    public modalConfig: Object = {
        backdrop: 'static',
        keyboard: true
    };

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService, private modalAdvService: ModalAdvService, private location: Location) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSTEAMUSERMAINTENANCE;
        this.browserTitle = this.pageTitle = `Team Member Maintenance`;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.enableAddMode();
        if (this.parentMode === 'UpdateUserToTeam' || this.parentMode === 'UpdateTeamToUser') {
            this.setControlValue('ROWID', this.riExchange.getParentAttributeValue('UserCodeRowID'));
            this.ellipsis.riMUserSearch.isEllipsisDisabled = true;
            this.ellipsis.riMUserSearch.autoOpen = false;
            this.pageMode = MntConst.eModeUpdate;
            this.updateMode = true;
            this.isRequesting = true;
            this.disableControl('UserCode', true);
            this.commonLookup.getTeamUserName(this.getControlValue('ROWID')).then((data) => {
                if (data[0][0]) {
                    this.isRequesting = false;
                    for (let item in data[0][0]) {
                        if (item) {
                            this.setControlValue(item, data[0][0][item]);
                            this.pageParams[item] = data[0][0][item];
                        }
                    }
                    this.onUserCodeChange();
                    this.onTeamChange();
                }
            });
        }
    }

    public riMUserSearchOnDataRecieved(data: any): void {
        for (let item in data) {
            if (item)
                this.setControlValue(item, data[item]);
        }
    }

    public enableAddMode(): void {
        this.pageMode = MntConst.eModeAdd;
        if (this.parentMode === 'AddUserToTeam' || this.parentMode === 'UpdateUserToTeam') {
            this.resetonAddMode();
            let disArray: IdisableArray[] = [
                { formControlName: 'TeamID', disableType: true },
                { formControlName: 'UserCode', disableType: false },
                { formControlName: 'TeamClosureSecurityLevel', disableType: false },
                { formControlName: 'DefaultTeamInd', disableType: false },
                { formControlName: 'AdministratorInd', disableType: false }
            ];
            for (let item of disArray) {
                this.disableControl(item.formControlName, item.disableType);
            }
            this.riExchange.getParentHTMLValue('TeamID');
            this.riExchange.getParentHTMLValue('TeamSystemName');
            this.ellipsis.riMUserSearch.isEllipsisDisabled = false;
            setTimeout(() => {
                this.ellipsis.riMUserSearch.autoOpen = true;
            }, 500);
        }
        if (this.parentMode === 'AddTeamToUser' || this.parentMode === 'UpdateTeamToUser') {
            this.disableControl('UserCode', true);
            this.disableControl('TeamID', false);
            this.riExchange.getParentHTMLValue('UserCode');
            this.riExchange.getParentHTMLValue('UserName');
        }
    }

    public onAddClick(): void {
        this.enableAddMode();
    }

    public onCancelClick(): void {
        this.uiForm.markAsPristine();
        if (this.parentMode === 'AddUserToTeam' && this.pageMode === MntConst.eModeAdd && !this.pageParams.UserCode) {
            this.location.back();
        } else {
            this.pageMode = MntConst.eModeUpdate;
            for (let item in this.pageParams) {
                if (item)
                    this.setControlValue(item, this.pageParams[item]);
            }
            this.onUserCodeChange();
            this.onTeamChange();
            this.ellipsis.riMUserSearch.isEllipsisDisabled = true;
            this.ellipsis.riMUserSearch.autoOpen = false;
            this.disableControl('UserCode', true);
        }
    }

    public onSaveClick(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, () => {
            this.addOrUpdateOrDeleteItem();
        }));

    }

    public resetonAddMode(): void {
        this.setControlValue('UserCode', '');
        this.setControlValue('UserName', '');
        this.setControlValue('TeamClosureSecurityLevel', '0');
        this.setControlValue('DefaultTeamInd', false);
        this.setControlValue('AdministratorInd', false);
    }

    public onUserCodeChange(): void {
        this.isRequesting = true;
        this.commonLookup.getUserName(this.getControlValue('UserCode')).then((data) => {
            if (data[0][0]) {
                this.isRequesting = false;
                this.setControlValue('UserName', data[0][0].UserName);
                this.pageParams['UserName'] = data[0][0].UserName;
            }
            else
                this.setControlValue('UserName', '');
        });
    }

    public onTeamChange(): void {
        this.isRequesting = true;
        this.commonLookup.getTeamName(this.getControlValue('TeamID')).then((data) => {
            this.isRequesting = false;
            if (data[0][0]) {
                this.setControlValue('TeamSystemName', data[0][0].TeamSystemName);
                this.pageParams['TeamSystemName'] = data[0][0].TeamSystemName;
            }
            else
                this.setControlValue('TeamSystemName', '');
        });
    }

    public onDelete(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, () => {
            this.pageMode = MntConst.eModeDelete;
            this.addOrUpdateOrDeleteItem();
        }));

    }

    private addOrUpdateOrDeleteItem(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, this.pageMode === MntConst.eModeAdd ? 1 : this.pageMode === MntConst.eModeUpdate ? 2 : 3);
            let formData: Object = {};

            this.controls.forEach(control => {
                if (control.type !== MntConst.eTypeCheckBox) {
                    formData = {
                        ...formData,
                        [control.name]: this.getControlValue(control.name, control.type === MntConst.eTypeCheckBox)
                    };
                }
            });
            if (this.pageMode !== MntConst.eModeDelete) {
                formData['DefaultTeamInd'] = this.getControlValue('DefaultTeamInd') ? 'yes' : 'no';
                formData['AdministratorInd'] = this.getControlValue('AdministratorInd') ? 'yes' : 'no';
            }
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('ccm/admin', 'team', 'System/iCABSSTeamUserMaintenance', search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.displayMessage(data);
                    } else {
                        this.uiForm.markAsPristine();
                        this.setControlValue('ROWID', data.ttTeamUser);
                        if (this.pageMode === MntConst.eModeAdd || this.pageMode === MntConst.eModeUpdate) {
                            this.pageMode = MntConst.eModeUpdate;
                            this.ellipsis.riMUserSearch.isEllipsisDisabled = true;
                            this.updateMode = true;
                            this.disableControl('UserCode', true);
                            for (let item in data) {
                                if (item !== 'ttTeamUser' && item !== 'RecordID' && item !== 'DateStamp' && item !== 'TimeStamp') {
                                    this.pageParams[item] = data[item];
                                }
                            }
                            this.setControlValue('DefaultTeamInd', data['DefaultTeamInd']);
                            this.displayMessage(MessageConstant.Message.SavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                        } else {
                            this.displayMessage(MessageConstant.Message.RecordDeletedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                            this.location.back();
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }


}

interface IdisableArray {
    formControlName: string;
    disableType: boolean;
}
