import { OnInit, Injector, Component, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { Location } from '@angular/common';

import { CommonMaintenanceFunction } from '@app/base/CommonMaintenanceFunction';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { EmployeeSearchComponent } from '../search/iCABSBEmployeeSearch';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { RouteAwayComponent } from '@shared/components/route-away/route-away';

@Component({
    templateUrl: 'iCABSBBranchServiceAreaEmployeeMaintenance.html'
})

export class BranchServiceAreaEmployeeMaintenanceComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit {

    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;

    public pageId: string;
    public controls: IControls[] = [
        { name: 'BusinessCode', type: MntConst.eTypeCode },
        { name: 'BranchNumber', type: MntConst.eTypeCode, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'EmployeeCode', type: MntConst.eTypeCode, required: true },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'BranchServiceAreaDesc', type: MntConst.eTypeText, disabled: true }
    ];
    private tempBranchServiceAreaEmployeeData: Record<string, string>;
    private xhrParams: IXHRParams = {
        method: 'table/entry',
        module: 'table-entry',
        operation: 'TableEntry'
    };
    public commonMaintenanceFunction: CommonMaintenanceFunction;
    public employeeEllipsisConfig = {
        childparams: {
            'parentMode': 'LookUp-Service-All'
        },
        component: EmployeeSearchComponent,
        disabled: false
    };
    public MntConst = MntConst;

    constructor(injector: Injector, private location: Location) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBBRANCHSERVICEAREAEMPLOYEEMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Branch Service Area Employee Maintenance';
        this.commonMaintenanceFunction = new CommonMaintenanceFunction(this, injector);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.pageMode = MntConst.eModeUpdate;
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
        this.riExchange.getParentHTMLValue('BranchServiceAreaDesc');
        this.riExchange.getParentHTMLValue('BranchName');
        this.riExchange.getParentHTMLValue('BranchNumber');
        if (this.parentMode === 'GridBranchServiceAreaEmployeeAdd') {
            this.commonMaintenanceFunction.setPageMode(
                MntConst.eModeAdd, null, ['BranchServiceAreaCode', 'BranchServiceAreaDesc', 'BranchName', 'BranchNumber']
            );
        } else if (this.parentMode === 'GridBranchServiceAreaEmployeeUpdate') {
            this.setControlValue('EmployeeCode', this.riExchange.getParentAttributeValue('EmployeeCode'));
            this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('EmployeeSurname'));
            this.disableControl('EmployeeCode', true);
            this.employeeEllipsisConfig.disabled = true;
            this.tempBranchServiceAreaEmployeeData = this.uiForm.getRawValue();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public onBtnClick(action: string): void {
        switch (action) {
            case 'Add':
                this.disableControl('EmployeeCode', false);
                this.employeeEllipsisConfig.disabled = false;
                this.commonMaintenanceFunction.onAddClick(
                    false, null, ['BranchServiceAreaCode', 'BranchServiceAreaDesc', 'BranchName', 'BranchNumber']
                );
                break;
            case 'Save':
                this.commonMaintenanceFunction.onSaveClick(
                    this.saveBranchServiceAreaEmployeeRecord.bind(this)
                );
                break;
            case 'Cancel':
                this.commonMaintenanceFunction.onCancelClick(
                    this.tempBranchServiceAreaEmployeeData,
                    () => {
                        this.formPristine();
                        this.location.back();
                    }
                );
                break;
            case 'Delete':
                this.commonMaintenanceFunction.onDeleteClick(
                    this.deleteBranchServiceAreaEmployeeRecord.bind(this)
                );
                break;
        }
    }

    private saveBranchServiceAreaEmployeeRecord(): void {
        const search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.pageParams.pageMode === MntConst.eModeAdd ? 1 : 2);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, this.getCommonFormData()).then((data) => {
            this.isRequesting = false;
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data.errorMessage || data.fullError || data);
                return;
            }
            this.formPristine();
            this.disableControl('EmployeeCode', true);
            this.employeeEllipsisConfig.disabled = true;
            this.tempBranchServiceAreaEmployeeData = this.uiForm.getRawValue();
            this.commonMaintenanceFunction.setPageMode(MntConst.eModeUpdate, this.tempBranchServiceAreaEmployeeData);
            this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
        }).catch((error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage(error);
        });
    }

    private deleteBranchServiceAreaEmployeeRecord(): void {
        const search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, 3);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, this.getCommonFormData()).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.formPristine();
            if (this.hasError(data)) {
                this.displayMessage(data.errorMessage || data.fullError || data);
                return;
            }
            this.displayMessage(MessageConstant.Message.RecordDeletedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
            this.location.back();
        }).catch((error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage(error);
        }).finally(() => {
            this.commonMaintenanceFunction.setPageMode(MntConst.eModeUpdate, this.tempBranchServiceAreaEmployeeData);
        });
    }

    private getCommonFormData(): Record<string, string | boolean> {
        return {
            table: 'BranchServiceAreaEmployee',
            [this.serviceConstants.MethodType]: 'maintenance',
            UserCode: this.utils.getUserCode(),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            BranchNumber: this.getControlValue('BranchNumber'),
            EmployeeCode: this.getControlValue('EmployeeCode'),
            BusinessCode: this.businessCode()
        };
    }

    public onEmployeeDataReceived(data: any): void {
        if (data) {
            this.setControlValue('EmployeeCode', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
    }
}
