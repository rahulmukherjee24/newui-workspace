import { Component, OnInit, Injector, ViewChild, OnDestroy, EventEmitter, AfterContentInit } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import { BaseComponent } from '../../base/BaseComponent';
import { PageIdentifier } from '../../base/PageIdentifier';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSAServiceVisitPlanningMaintenance.html'
})

export class ServiceVisitPlanningMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private vbTimeSeparator: string = ':';
    private actionAfterSave: string;
    private actionAfterDelete: string;

    public riMaintenanceFunctionDelete: boolean;
    public riMaintenanceFunctionUpdate: boolean;
    public riMaintenanceFunctionAdd: boolean;
    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'ContractNumber', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'PremiseNumber', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ProductCode', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'ProductDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'CalendarDate', disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'PlannedQuantity', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'PlannedValue', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'VisitDuration', disabled: false, type: MntConst.eTypeText, commonValidator: true },
        { name: 'ServiceVisitText', disabled: false, required: false, type: MntConst.eTypeTextFree, commonValidator: true },
        // hidden
        { name: 'ServiceCoverNumber', type: MntConst.eTypeInteger },
        { name: 'RequiresVisitDetailTextInd' },
        { name: 'QuantityRequired' },
        { name: 'BranchNumber' },
        { name: 'CalendarMode' },
        { name: 'ServiceAnnualHours' },
        { name: 'ServiceAnnualValue' },
        { name: 'RequireAnnualTimeInd' },
        { name: 'InitialVisit' }
    ];
    public queryParams: any = {
        module: 'service',
        method: 'service-delivery/maintenance',
        operation: 'Application/iCABSAServiceVisitPlanningMaintenance'
    };

    public lookUpSubscription: Subscription;
    public httpSubscription: Subscription;

    /* ========== Set Focus On =============== */
    public setFocusOnVisitDuration = new EventEmitter<boolean>();

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
        } else {
            this.windowOnload();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.lookUpSubscription) {
            this.lookUpSubscription.unsubscribe();
        }
        if (this.httpSubscription) {
            this.httpSubscription.unsubscribe();
        }
        this.routeAwayGlobals.resetRouteAwayFlags();
    }

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSASERVICEVISITPLANNINGMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Service Visit Planning Maintenance';
    }

    private windowOnload(): void {
        this.setCurrentContractType();
        this.riCountryLookUpCall();

        this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber') || '');
        this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber') || '');
        this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode') || '');
        this.setControlValue('CalendarMode', this.riExchange.getParentHTMLValue('CalendarMode') || '');
        this.setControlValue('RequireAnnualTimeInd', this.riExchange.getParentHTMLValue('RequireAnnualTimeInd'));
        this.setControlValue('CalendarDate', this.riExchange.getParentAttributeValue('CalDate'));
        this.setControlValue('ServiceCoverNumber', this.riExchange.getParentAttributeValue('ServiceCoverNumber'));
        if (this.getControlValue('RequireAnnualTimeInd')) {
            this.pageParams.vbFieldState = true;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'VisitDuration', true);
        } else {
            this.pageParams.vbFieldState = false;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'VisitDuration', false);
        }

        if (this.riExchange.getParentMode() === 'Add') {
            this.riMaintenanceAddMode();
        } else {
            this.riMaintenanceUpdateMode();
            this.setAttribute('RowID', this.riExchange.getParentAttributeValue('RowID') || '');
            this.riMaintenanceFetchRecord();
            this.getDefaultValues();
        }
        this.riMaintenanceExecMode(this.getFormMode());
    }
    // set current page data
    private setCurrentContractType(): void {
        this.pageParams.currentContractType = this.riExchange.getCurrentContractType();
        this.pageParams.currentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
    }
    // lookUp Table Data Fetch
    private riCountryLookUpCall(): void {
        let lookupIP: Array<any> = [
            {
                'table': 'riCountry',
                'query': {
                    'riCountrySelected': true
                },
                'fields': ['riTimeSeparator']
            }
        ];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupIP).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                if (data && data.length > 0) {
                    if (data[0] && data[0].length > 0) {
                        if (data[0][0].riTimeSeparator) this.vbTimeSeparator = data[0][0].riTimeSeparator;
                    }
                } else {
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                }
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }
    // get page defauld data
    private getDefaultValues(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');

        let postDataAdd: Object = {};
        postDataAdd['Function'] = 'GetDefaultValues';
        postDataAdd['ContractNumber'] = this.getControlValue('ContractNumber');
        postDataAdd['PremiseNumber'] = this.getControlValue('PremiseNumber');
        postDataAdd['ProductCode'] = this.getControlValue('ProductCode');
        postDataAdd['ServiceCoverNumber'] = this.getControlValue('ServiceCoverNumber');
        postDataAdd['CalendarDate'] = this.getControlValue('CalendarDate');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postDataAdd)
            .subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.markAsPristine();
                    this.setControlValue('ContractName', e.ContractName);
                    this.setControlValue('PremiseName', e.PremiseName);
                    this.setControlValue('ProductDesc', e.ProductDesc);
                    this.setControlValue('VisitDuration', e.VisitDuration);
                    this.setControlValue('PlannedValue', e.PlannedValue);
                    this.setControlValue('PlannedQuantity', e.PlannedQuantity);
                    this.setControlValue('RequiresVisitDetailTextInd', e.RequiresVisitDetailTextInd === 'no' ? false : true); // checked
                    this.setControlValue('QuantityRequired', e.QuantityRequired === 'no' ? false : true); // checked
                    this.setControlValue('ServiceAnnualHours', e.ServiceAnnualHours);
                    this.setControlValue('ServiceAnnualValue', e.ServiceAnnualValue);
                    this.setControlValue('InitialVisit', e.InitialVisit === 'no' ? false : true); // checked
                    if (this.getControlValue('QuantityRequired')) {
                        this.pageParams.tdPlannedQuantity = true;
                    }
                    if (this.getControlValue('InitialVisit')) {
                        this.riMaintenanceFunctionDelete = false;
                        if (!this.getControlValue('RequiresVisitDetailTextInd')) {
                            this.riMaintenanceFunctionUpdate = false;
                        }
                        this.pageParams.tdInitialVisit = true;
                    } else {
                        this.riMaintenanceFunctionDelete = true;
                    }
                    if (this.getControlValue('RequiresVisitDetailTextInd')) {
                        this.pageParams.trServiceVisitText = true;
                    }
                    if (this.getControlValue('RequireAnnualTimeInd')) {
                        this.pageParams.tdVisitDuration = true;
                    }
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }
    // reload HTML doc
    private updateParentHTMLDocument(): void {
        this.riMaintenanceAddVirtualTableCommit();
    }
    // reset control
    private resetControl(): void {
        this.riExchange.resetCtrl(this.controls);
        this.riExchange.renderForm(this.uiForm, this.controls);
        this.markAsPristine();
    }

    private markAsPristine(): void {
        this.controls.forEach((i) => {
            this.uiForm.controls[i.name].markAsPristine();
            this.uiForm.controls[i.name].markAsUntouched();
        });
    }

    /* ------- riMaintenance Logic ---------- */
    private riMaintenanceExecMode(mode: string): void {
        switch (mode) {
            case 'add':
                this.riMaintenanceFunctionAdd = true;
                this.riMaintenanceBeforeAdd();
                break;
            case 'update':
                this.riMaintenanceFunctionAdd = false;
                this.riMaintenanceBeforeUpdate();
                break;
            case 'delete':
                this.riMaintenanceFunctionAdd = false;
                break;
        }
    }

    private riMaintenanceExecContinue(mode: string): void {
        switch (mode) {
            case 'eModeSaveAdd':
                this.updateParentHTMLDocument();
                this.confirm();
                break;
            case 'eModeSaveUpdate':
                this.updateParentHTMLDocument();
                this.confirm();
                break;
            case 'eModeDelete':
                this.confirmDelete();
                break;
            case 'add':
                this.riMaintenanceAfterSave();
                break;
            case 'update':
                this.riMaintenanceAfterSave();
                break;
            case 'delete':
                this.riMaintenanceAfterDelete();
                break;
        }
    }
    private riMaintenanceSelectMode(): void {
        this.setFormMode(this.c_s_MODE_SELECT);
    }

    private riMaintenanceUpdateMode(): void {
        this.setFormMode(this.c_s_MODE_UPDATE);
    }

    private riMaintenanceAddMode(): void {
        this.setFormMode(this.c_s_MODE_ADD);
    }

    private riMaintenanceDeleteMode(): void {
        this.setFormMode(this.c_s_MODE_DELETE);
    }

    private riMaintenanceFetchRecord(): void {
        this.riMaintenanceAddTableCommit();
    }
    private riMaintenanceAddTableCommit(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        search.set('ContractNumber', this.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        search.set('ProductCode', this.getControlValue('ProductCode'));
        search.set('ServiceCoverNumber', this.getControlValue('ServiceCoverNumber'));
        search.set('CalendarDate', this.getControlValue('CalendarDate'));
        search.set('ROWID', this.getAttribute('RowID'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
            .subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.setControlValue('ServiceVisitText', e.ServiceVisitText);
                    this.setControlValue('ContractNumber', e.ContractNumber);
                    this.setControlValue('PremiseNumber', e.PremiseNumber);
                    this.setControlValue('ProductCode', e.ProductCode);
                    this.setControlValue('ServiceCoverNumber', e.ServiceCoverNumber);
                    this.setControlValue('CalendarDate', e.CalendarDate);
                    this.riMaintenanceAddVirtualTableCommit();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        this.riMaintenanceAddVirtualTableCommit();
    }
    private riMaintenanceAddVirtualTableCommit(): void {
        let lookupIP = [
            {
                'table': 'Contract',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ContractNumber': this.getControlValue('ContractNumber')
                },
                'fields': ['ContractName']
            },
            {
                'table': 'Premise',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'PremiseNumber': this.getControlValue('PremiseNumber')
                },
                'fields': ['PremiseName']
            },
            {
                'table': 'ServiceCover',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'PremiseNumber': this.getControlValue('PremiseNumber'),
                    'ProductCode': this.getControlValue('ProductCode'),
                    'ServiceCoverNumber': this.getControlValue('ServiceCoverNumber')
                },
                'fields': ['ServiceBranchNumber']
            },
            {
                'table': 'Product',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ProductCode': this.getControlValue('ProductCode')
                },
                'fields': ['ProductDesc', 'RequiresVisitDetailTextInd', 'QuantityRequired']
            }
        ];

        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            let contract = data[0][0];
            let premise = data[1][0];
            let serviceCover = data[2][0];
            let product = data[3][0];
            if (data && data.length > 0) {
                if (contract) {
                    this.setControlValue('ContractName', contract['ContractName']);
                }
                if (premise) {
                    this.setControlValue('PremiseName', premise['PremiseName']);
                }
                if (serviceCover) {
                    this.setControlValue('ServiceBranchNumber', serviceCover['ServiceBranchNumber']);
                }
                if (product) {
                    this.setControlValue('ProductDesc', product['ProductDesc']);
                    this.setControlValue('RequiresVisitDetailTextInd', product['RequiresVisitDetailTextInd']);
                    this.setControlValue('QuantityRequired', product['QuantityRequired']);
                }
            }
        });
    }
    private riMaintenanceBeforeAdd(): void {
        this.getDefaultValues();
    }
    private riMaintenanceBeforeUpdate(): void {
        if (this.getControlValue('RequireAnnualTimeInd')) {
            this.disableControl('VisitDuration', false);
            this.setFocusOnVisitDuration.emit(true);
        }
    }
    private riMaintenanceAfterSave(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.actionAfterSave);

        let postDataAdd: Object = {};
        postDataAdd['ContractNumber'] = this.getControlValue('ContractNumber');
        postDataAdd['PremiseNumber'] = this.getControlValue('PremiseNumber');
        postDataAdd['ProductCode'] = this.getControlValue('ProductCode');
        postDataAdd['ServiceCoverNumber'] = this.getControlValue('ServiceCoverNumber');
        postDataAdd['CalendarDate'] = this.getControlValue('CalendarDate');
        postDataAdd['ServiceVisitText'] = this.getControlValue('ServiceVisitText');
        postDataAdd['PlannedQuantity'] = this.getControlValue('PlannedQuantity');
        postDataAdd['PlannedValue'] = this.getControlValue('PlannedValue');
        postDataAdd['VisitDuration'] = this.getControlValue('VisitDuration');
        if (this.getFormMode() === this.c_s_MODE_UPDATE) {
            postDataAdd['ServiceCoverCalendarDateROWID'] = this.getAttribute('RowID');
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postDataAdd)
            .subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.markAsPristine();
                    this.location.back();
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }
    private riMaintenanceAfterDelete(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.actionAfterDelete);

        let formdata: Object = {};
        formdata['ServiceCoverCalendarDateROWID'] = this.getAttribute('RowID');
        formdata['ContractNumber'] = this.getControlValue('ContractNumber');
        formdata['PremiseNumber'] = this.getControlValue('PremiseNumber');
        formdata['ProductCode'] = this.getControlValue('ProductCode');
        formdata['ServiceCoverNumber'] = this.getControlValue('ServiceCoverNumber');
        formdata['CalendarDate'] = this.getControlValue('CalendarDate');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formdata)
            .subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.markAsPristine();
                    this.location.back();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    /* ---- page logic ----*/
    public save(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            switch (this.getFormMode()) {
                case 'add':
                    this.actionAfterSave = '1';
                    this.riMaintenanceExecContinue(MntConst.eModeSaveAdd);
                    break;
                case 'update':
                    this.actionAfterSave = '2';
                    this.riMaintenanceExecContinue(MntConst.eModeSaveUpdate);
                    break;
            }
        }
    }

    public delete(): void {
        this.riMaintenanceDeleteMode();
        this.riExchange.validateForm(this.uiForm);
        if (this.uiForm.valid) {
            this.actionAfterDelete = '3';
            this.riMaintenanceExecContinue(MntConst.eModeDelete);
        }
    }

    public confirm(): any {
        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.confirmed.bind(this));
        this.modalAdvService.emitPrompt(modalVO);
    }

    public confirmDelete(): any {
        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.confirmed.bind(this));
        this.modalAdvService.emitPrompt(modalVO);
    }
    public confirmed(obj: any): any {
        this.riMaintenanceExecContinue(this.getFormMode());
    }
    public cancel(): void {
        this.location.back();
    }

    public visitDurationOnchange(): void {
        let vbNegative: boolean = false;
        let vbError: boolean = false;
        let vbTimeFormat: string = '##00' + this.pageParams.vbTimeSeparator + '##';
        let vbVisitDuration: number = this.getControlValue('VisitDuration').replace(this.vbTimeSeparator, '');
        let vbDurationHours: string;
        let vbDurationMinutes: string;
        let vbVisitDurationSec: number;
        let vbMsgResult: string;
        if (!isFinite(vbVisitDuration)) {
            vbError = true;
        }
        if (!vbError && (vbVisitDuration.toString().length < 4 || vbVisitDuration.toString().length > 7)) {
            vbError = true;
        } else if (!vbError) {
            vbDurationHours = vbVisitDuration.toString().substr(0, vbVisitDuration.toString().length - 2);
            vbDurationMinutes = vbVisitDuration.toString().substr(-2);
            let vbVisitDurationTime: string = vbDurationHours + ':' + vbDurationMinutes;
            this.setControlValue('VisitDuration', vbVisitDurationTime);
            if (!vbError && vbVisitDuration === 0) {
                vbError = true;
            }
            if (!vbError) {
                vbDurationHours = vbVisitDuration.toString().substr(0, vbVisitDuration.toString().length - 2);
                vbDurationMinutes = vbVisitDuration.toString().substr(-2);
                if (parseInt(vbDurationMinutes, 10) > 59) {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.vbMsgResult));
                    vbError = true;
                } else {
                    vbVisitDurationSec = (parseInt(vbDurationHours, 10) * 60 * 60) + (parseInt(vbDurationMinutes, 10) * 60);
                    if (!this.getControlValue('InitialVisit')) {
                        let search: QueryParams = this.getURLSearchParamObject();
                        search.set(this.serviceConstants.Action, '6');

                        let postData: Object = {};
                        postData['Function'] = 'GetPlanValue';
                        postData['AnnualDuration'] = this.getControlValue('ServiceAnnualHours');
                        postData['AnnualValue'] = this.getControlValue('ServiceAnnualValue');
                        postData['PlanDuration'] = vbVisitDurationSec.toString();

                        this.ajaxSource.next(this.ajaxconstant.START);
                        this.httpSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData)
                            .subscribe(
                            (e) => {
                                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                                if (e.hasError) {
                                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                                } else {
                                    this.setControlValue('PlannedValue', e.PlannedValue);
                                }
                            },
                            (error) => {
                                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                            });
                    }
                }
            }
        }
        if (vbError) {
            this.setControlValue('VisitDuration', '');
        }
    }
}
