import { RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from './search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { ContractManagementModuleRoutes } from './../base/PageRoutes';
import { ServiceCoverMaintenanceComponent } from './maintenance/ServicecoverMaintenance/iCABSAServiceCoverMaintenance.component';
import { ServiceCoverPropertiesComponent } from './maintenance/ServicecoverMaintenance/iCABSAServiceCoverMaintenance.properties';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { HttpClientModule } from '@angular/common/http';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class ServiceCoverMaintenanceRootComponent {
    constructor() {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        RouterModule.forChild([
            {
                path: '', component: ServiceCoverMaintenanceRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE_SUB, component: ServiceCoverMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT_SUB, component: ServiceCoverMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCEJOB_SUB, component: ServiceCoverMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCEREDUCE_SUB, component: ServiceCoverMaintenanceComponent, canDeactivate: [RouteAwayGuardService] }
                ]
            }

        ])
    ],

    declarations: [
        ServiceCoverMaintenanceRootComponent,
        ServiceCoverMaintenanceComponent,
        ServiceCoverPropertiesComponent

    ],

    entryComponents: []
})

export class ServiceCoverMaintenanceModule { }

