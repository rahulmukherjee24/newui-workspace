import { Component, ViewContainerRef, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { BranchPcodeServiceGrpEntryMaintenanceComponent } from './maintenance/iCABSBBranchPcodeServiceGrpEntryMaintenance.component';
import { BranchProdServiceGrpEntryMaintenanceComponent } from '../internal/maintenance/iCABSBBranchProdServiceGrpEntryMaintenance.component';
import { BranchServiceAreaGroupDetailMaintenanceComponent } from './maintenance/iCABSBBranchServiceAreaGroupDetailMaintenance.component';
import { CMProspectMaintenanceComponent } from './maintenance/iCABSCMProspectMaintenance.component';
import { DetectorMaintenanceComponent } from './maintenance/iCABSBDetectorMaintenance.component';
import { EntitlementMaintenanceComponent } from '../internal/maintenance/iCABSAEntitlementMaintenance.component';
import { InfestationLevelMaintenanceComponent } from '../service-delivery/TableMaintenanceBusiness/InfestationLevel/iCABSBInfestationLevelMaintenance.component';
import { InternalMaintenanceModuleRoutesConstant, InternalMaintenanceModuleRoutes } from '../base/PageRoutes';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { InternalSearchModule } from './search.module';
import { NotificationGroupEmployeeMaintenanceComponent } from './maintenance/iCABSSNotificationGroupEmployeeMaintenance.component';
import { NotificationTemplateDetailMaintenanceComponent } from './maintenance/iCABSSNotificationTemplateDetailMaintenance.component';
import { PreparationMixMaintenanceComponent } from './maintenance/iCABSBPreparationMixMaintenance.component';
import { ReturnedPaperworkMaintenanceComponent } from './maintenance/iCABSBReturnedPaperworkMaintenance.component';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SeManualWasteConsignmentNoteComponent } from './maintenance/iCABSSeManualWasteConsignmentNote.component';
import { UnreturnedConsignmentNotesDetailGridComponent } from './maintenance/iCABSSEUnreturnedConsignmentNotesDetailGrid.component';
import { UnreturnedConsignmentNotesGridComponent } from './maintenance/iCABSSEUnreturnedConsignmentNotesGrid.component';
import { ServiceVisitPlanningMaintenanceComponent } from '../internal/maintenance/iCABSAServiceVisitPlanningMaintenance.component';
import { SharedModule } from './../../shared/shared.module';
import { SystemBusinessVisitTypeMaintenanceComponent } from '../../app/internal/maintenance/iCABSBSystemBusinessVisitTypeMaintenance.component';
import { WasteConsignmentNoteRangeMaintenanceComponent } from './maintenance/iCABSBWasteConsignmentNoteRangeMaintenance.component';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class InternalMaintenanceComponent {
    constructor(viewContainerRef: ViewContainerRef) {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        RouterModule.forChild([
            {
                path: '', component: InternalMaintenanceComponent, children: [
                    { path: InternalMaintenanceModuleRoutes.ICABSBSYSTEMBUSINESSVISITTYPEMAINTENANCE, component: SystemBusinessVisitTypeMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSBWASTECONSIGNMENTNOTERANGEMAINTENANCE, component: WasteConsignmentNoteRangeMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSCMPROSPECTMAINTENANCE, component: CMProspectMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSBRETURNEDPAPERWORKMAINTENANCE, component: ReturnedPaperworkMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSBBRANCHSERVICEAREAGROUPDETAILMAINTENANCE, component: BranchServiceAreaGroupDetailMaintenanceComponent },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSSEMANUALWASTECONSIGNMENT.URL1, component: SeManualWasteConsignmentNoteComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSSEMANUALWASTECONSIGNMENT.URL2, component: SeManualWasteConsignmentNoteComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSSEUNRETURNEDCONSIGNMENTNOTESGRID, component: UnreturnedConsignmentNotesGridComponent },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSSEUNRETURNEDCONSIGNMENTNOTESDETAILGRID, component: UnreturnedConsignmentNotesDetailGridComponent },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSBBRANCHPCODESERVICEGRPENTRYMAINTENANCE, component: BranchPcodeServiceGrpEntryMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSSNOTIFICATIONTEMPLATEDETAILMAINTENANCE, component: NotificationTemplateDetailMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSBPREPARATIONMIXMAINTENANCE, component: PreparationMixMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSBDETECTORMAINTENANCE, component: DetectorMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSSNOTIFICATIONGROUPEMPLOYEEMAINTENANCE, component: NotificationGroupEmployeeMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSBINFESTATIONLEVELMAINTENANCE, component: InfestationLevelMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSBBRANCHPRODSERVICEGRPENTRYMAINTENANCE, component: BranchProdServiceGrpEntryMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSASERVICEVISITPLANNINGMAINTENANCE, component: ServiceVisitPlanningMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalMaintenanceModuleRoutesConstant.ICABSAENTITLEMENTMAINTENENCE, component: EntitlementMaintenanceComponent, canDeactivate: [RouteAwayGuardService] }
                ]
            }
        ])
    ],

    declarations: [
        InternalMaintenanceComponent,
        ReturnedPaperworkMaintenanceComponent,
        SystemBusinessVisitTypeMaintenanceComponent,
        WasteConsignmentNoteRangeMaintenanceComponent,
        CMProspectMaintenanceComponent,
        BranchPcodeServiceGrpEntryMaintenanceComponent,
        BranchServiceAreaGroupDetailMaintenanceComponent,
        SeManualWasteConsignmentNoteComponent,
        UnreturnedConsignmentNotesGridComponent,
        UnreturnedConsignmentNotesDetailGridComponent,
        PreparationMixMaintenanceComponent,
        NotificationTemplateDetailMaintenanceComponent,
        DetectorMaintenanceComponent,
        NotificationGroupEmployeeMaintenanceComponent,
        InfestationLevelMaintenanceComponent,
        BranchProdServiceGrpEntryMaintenanceComponent,
        ServiceVisitPlanningMaintenanceComponent,
        EntitlementMaintenanceComponent
    ],
    exports: [],

    entryComponents: []
})


export class InternalMaintenanceModule { }
