import { Component, ViewContainerRef, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { InternalSearchModule } from './search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { SharedModule } from './../../shared/shared.module';
import { InternalGridSearchModuleRoutesConstant } from '../base/PageRoutes';
import { SummaryWorkloadGridMonthBranchComponent } from './grid-search/iCABSSESummaryWorkloadGridMonthBranch.component';
import { ServiceAreaRezoneGridComponent } from './grid-search/iCABSBServiceAreaRezoneGrid.component';
import { ReturnedPaperWorkTypeGridComponent } from './grid-search/iCABSARReturnedPaperWorkTypeGrid.component';
import { PreparationMixGridComponent } from './grid-search/ICABSBPreparationMixGrid.component';
import { ServicePlanningDetailGridHgComponent } from './grid-search/iCABSSeServicePlanningDetailGridHg.component';
import { WasteConsignmentNoteGridComponent } from './grid-search/iCABSSeWasteConsignmentNoteGrid.component';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class InternalGridSearchComponent {
    constructor(viewContainerRef: ViewContainerRef) { }
}


@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        RouterModule.forChild([
            {
                path: '', component: InternalGridSearchComponent, children: [
                    { path: InternalGridSearchModuleRoutesConstant.ICABSBSERVICEAREAREZONEGRID, component: ServiceAreaRezoneGridComponent },
                    { path: InternalGridSearchModuleRoutesConstant.ICABSSESUMMARYWORKLOADGRIDMONTHBRANCH.URL_1, component: SummaryWorkloadGridMonthBranchComponent },
                    { path: InternalGridSearchModuleRoutesConstant.ICABSSESUMMARYWORKLOADGRIDMONTHBRANCH.URL_2, component: SummaryWorkloadGridMonthBranchComponent },
                    { path: InternalGridSearchModuleRoutesConstant.ICABSARRETURNEDPAPERWORKTYPEGRID, component: ReturnedPaperWorkTypeGridComponent },
                    { path: InternalGridSearchModuleRoutesConstant.ICABSBPREPARATIONMIXGRID, component: PreparationMixGridComponent },
                    { path: InternalGridSearchModuleRoutesConstant.ICABSSESERVICEPLANNINGDETAILGRIDHG, component: ServicePlanningDetailGridHgComponent },
                    { path: InternalGridSearchModuleRoutesConstant.ICABSSEWASTECONSIGNMENTNOTEGRID, component: WasteConsignmentNoteGridComponent }
                ]
            }
        ])
    ],

    declarations: [
        InternalGridSearchComponent,
        SummaryWorkloadGridMonthBranchComponent,
        ReturnedPaperWorkTypeGridComponent,
        PreparationMixGridComponent,
        ServiceAreaRezoneGridComponent,
        ServicePlanningDetailGridHgComponent,
        WasteConsignmentNoteGridComponent
    ]
})


export class InternalGridSearchModule { }
