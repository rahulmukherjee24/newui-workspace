import { Component, OnInit, OnDestroy, Injector, ElementRef, ViewChild, AfterViewInit } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { MessageConstant } from '../../../../shared/constants/message.constant';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { PrepChargeRateSearchComponent } from '../../../../app/internal/search/iCABSBPrepChargeRateSearch.component';
import { PreparationSearchComponent } from '../../../../app/internal/search/iCABSBPreparationSearch.component';
import { EllipsisComponent } from '../../../../shared/components/ellipsis/ellipsis';
import { RouteAwayComponent } from '../../../../shared/components/route-away/route-away';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';


@Component({
    templateUrl: 'iCABSBPrepChargeRateMaintenance.html'
})

export class PrepChargeRateMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('prepChargeRateSearchComponent') public prepChargeRateSearchComponent: EllipsisComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;

    private queryParams: any = {
        operation: 'Business/iCABSBPrepChargeRateMaintenance',
        module: 'rates',
        method: 'bill-to-cash/admin'
    };

    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'PrepCode', disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'PrepDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ChargeRateCode', disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'ChargeRateDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'PrepPriceEffectDate', disabled: true, required: true, type: MntConst.eTypeDate },
        { name: 'PrepPrice', disabled: true, required: true, type: MntConst.eTypeDecimal4 },
        { name: 'ROWID' }
    ];

    /**
     * iCABSBChargeRateSearch dropdown implementation details
     */
    public chargeRateSearchColumns: Array<string> = ['ChargeRateCode', 'ChargeRateDesc'];
    public inputParamsChargeRateSearchDropdown: any = {
        module: 'rates',
        method: 'bill-to-cash/search',
        operation: 'Business/iCABSBChargeRateSearch'
    };
    public onChargeRateSearchDataRecieved(event: any): void {
        this.setControlValue('ChargeRateCode', event.ChargeRateCode);
        this.setControlValue('ChargeRateDesc', event.ChargeRateDesc);
        if (this.formMode === this.c_s_MODE_SELECT) this.enableEffectiveDate();
    }

    public isSaveDisabled: boolean = true;
    public isCancelDisabled: boolean = true;
    public isSearchDisabled: boolean = false;
    public isDeleteDisabled: boolean = true;
    public promptConfirmContent: any;

    public ellipsisConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public ellipsisQueryParams: any = {
        prepCodeEllipsis: {
            autoOpen: false,
            childConfigParams: {
                'parentMode': 'LookUp',
                'showAddNew': false
            },
            contentComponent: PreparationSearchComponent, /** page to be opened iCABSBPreparationSearch.html */
            searchModalRoute: '',
            disabled: false
        },
        prepChargeRateEllipsis: {
            autoOpen: false,
            childConfigParams: {
                parentMode: 'LookUp',
                showAddNew: true,
                PrepCode: ''
            },
            contentComponent: PrepChargeRateSearchComponent, /** page to be opened iCABSBPrepChargeRateSearch.html */
            searchModalRoute: '',
            disabled: false
        }
    };

    public dropdown: any = {
        chargeRateSearch: {
            isRequired: true,
            isDisabled: true,
            isTriggerValidate: true,
            isActive: {
                id: '',
                text: ''
            },
            params: {
                module: 'rates',
                method: 'bill-to-cash/search',
                operation: 'Business/iCABSBChargeRateSearch'
            },
            displayFields: ['ChargeRateCode', 'ChargeRateDesc']
        }
    };

    constructor(injector: Injector, private el: ElementRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBPREPCHARGERATEMAINTENANCE;
        this.pageTitle = this.browserTitle = 'Preparation Charge Rate Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.onSelectMode();
    }

    ngAfterViewInit(): void {
        this.ellipsisQueryParams.prepChargeRateEllipsis.autoOpenSearch = true;
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /* landing in select mode */
    public onSelectMode(): void {
        this.setFormMode(this.c_s_MODE_SELECT);
        this.disableControl('PrepCode', false);
        this.dropdown.chargeRateSearch.isDisabled = true;
        this.ellipsisQueryParams.prepCodeEllipsis.disabled = false;
        this.dropdown.chargeRateSearch.isActive = { id: '', text: '' };
        this.disableControl('PrepPriceEffectDate', true);
        this.disableControl('PrepPrice', true);
        this.pageParams.isSaveDisabled = true;
        this.pageParams.isCancelDisabled = true;
        this.pageParams.isSearchDisabled = false;
        this.pageParams.isDeleteDisabled = true;
    }

    /* landing in update mode */
    public onUpdateMode(): void {
        this.setFormMode(this.c_s_MODE_UPDATE);
        this.disableControl('PrepCode', true);
        this.dropdown.chargeRateSearch.isDisabled = true;
        this.ellipsisQueryParams.prepCodeEllipsis.disabled = true;
        this.disableControl('PrepPriceEffectDate', true);
        this.disableControl('PrepPrice', false);
        this.pageParams.isSaveDisabled = false;
        this.pageParams.isCancelDisabled = false;
        this.pageParams.isSearchDisabled = false;
        this.pageParams.isDeleteDisabled = false;
        this.el.nativeElement.querySelector('#PrepPrice').focus();
    }

    /* landing in add mode */
    public onAddMode(): void {
        this.setFormMode(this.c_s_MODE_ADD);
        this.disableControl('PrepCode', false);
        this.dropdown.chargeRateSearch.isDisabled = false;
        this.ellipsisQueryParams.prepCodeEllipsis.disabled = false;
        this.dropdown.chargeRateSearch.isActive = { id: '', text: '' };
        this.disableControl('PrepPriceEffectDate', false);
        this.disableControl('PrepPrice', false);
        this.pageParams.isSaveDisabled = false;
        this.pageParams.isCancelDisabled = false;
        this.pageParams.isSearchDisabled = false;
        this.pageParams.isDeleteDisabled = true;
    }

    public addData(): void {
        this.resetForm();
        this.onAddMode();
    }

    public resetData(): void {
        if (this.formMode === this.c_s_MODE_ADD) {
            this.resetForm();
            this.onAddMode();
        } else if (this.formMode === this.c_s_MODE_UPDATE) {
            this.prepChargeRateFetchData();
        }
        this.formPristine();
    }

    public enableChargeRate(): void {
        if (this.getControlValue('PrepCode') && this.formMode === this.c_s_MODE_SELECT) {
            this.dropdown.chargeRateSearch.isDisabled = false;
        }
    }

    public enableEffectiveDate(): void {
        if (this.getControlValue('ChargeRateCode') && this.formMode === this.c_s_MODE_SELECT) {
            this.disableControl('PrepPriceEffectDate', false);
        }
    }

    public dateFromSelectedValue(value: any): void {
        if (!value || !value.value) {
            return;
        }
        this.setControlValue('PrepPriceEffectDate', value.value);
        if (this.formMode !== this.c_s_MODE_ADD) {
            this.prepChargeRateFetchData();
        }
    }

    public dolookUpCallForDesc(): void {
        let lookupIP: Array<any> = [
            {
                'table': 'Prep',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'PrepCode': this.getControlValue('PrepCode')
                },
                'fields': ['PrepDesc']
            },
            {
                'table': 'ChargeRate',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ChargeRateCode': this.getControlValue('ChargeRateCode')
                },
                'fields': ['ChargeRateDesc']
            }
        ];
        this.lookupDetails(lookupIP);
    }

    public lookupDetails(query: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(query).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.setControlValue('PrepDesc', data[0] && data[0].length ? data[0][0].PrepDesc : '');
            this.setControlValue('ChargeRateDesc', data[1] && data[1].length ? data[1][0].ChargeRateDesc : '');
            this.dropdown.chargeRateSearch.isActive = {
                id: this.getControlValue('ChargeRateCode'),
                text: this.getControlValue('ChargeRateCode') + ' - ' + this.getControlValue('ChargeRateDesc')
            };
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error));
        });
    }

    /* Rreceiving data from ChargeRateCode Ellipsis */
    public onPrepChargeRateDataReceived(data: any): void {
        if (!data.AddMode) {
            if (data.PrepCode) {
                this.setControlValue('PrepCode', data.PrepCode);
            }
            this.setControlValue('ChargeRateCode', data.ChargeRateCode);
            this.setControlValue('PrepPriceEffectDate', data.PrepPriceEffectDate);
            this.dolookUpCallForDesc();
            this.prepChargeRateFetchData();
        } else {
            this.addData();
        }
    }

    /* TODO receiving data from PrepCode Ellipsis */
    public onPrepCodeDataReceived(data: any): void {
        this.setControlValue('PrepCode', data.PrepCode);
        this.setControlValue('PrepDesc', data.PrepDesc);
        this.enableChargeRate();
    }

    public prepChargeRateFetchData(): void {
        let lookupIP: Array<any> = [
            {
                'table': 'PrepChargeRate',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'PrepCode': this.getControlValue('PrepCode'),
                    'ChargeRateCode': this.getControlValue('ChargeRateCode'),
                    'PrepPriceEffectDate': this.getControlValue('PrepPriceEffectDate')
                },
                'fields': ['PrepPrice']
            }
        ];
        this.prepPricelookupDetails(lookupIP);
    }

    public prepPricelookupDetails(query: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(query).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (!data[0].length) {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                this.resetForm();
                this.onSelectMode();
                return;
            }
            this.dolookUpCallForDesc();
            this.setControlValue('PrepPrice', data[0] && data[0].length ? data[0][0].PrepPrice : '');
            this.setControlValue('ROWID', data[0] && data[0].length ? data[0][0].ttPrepChargeRate : '');
            this.onUpdateMode();
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error));
        });
    }

    public searchData(): void {
        if (this.formMode === this.c_s_MODE_SELECT) {
            this.ellipsisQueryParams.prepChargeRateEllipsis.childConfigParams['PrepCode'] = this.getControlValue('PrepCode');
        } else if (this.formMode === this.c_s_MODE_ADD) {
            this.resetForm();
            this.onSelectMode();
        } else {
            this.prepChargeRateFetchData();
            this.ellipsisQueryParams.prepChargeRateEllipsis.childConfigParams['PrepCode'] = '';
        }
        this.prepChargeRateSearchComponent.refreshComponent = true;
        this.prepChargeRateSearchComponent.openModal();
    }

    public saveData(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.promptConfirm.bind(this)));
    }

    public deleteData(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.promptConfirmDelete.bind(this)));
    }

    public promptConfirm(): void {
        this.saveUpdatedData();
    }

    public promptConfirmDelete(): void {
        this.deleteSavedData();
    }

    public saveUpdatedData(): void {
        let postSearchParams: any = this.getURLSearchParamObject();
        let postParams: any = {};
        postParams['PrepPrice'] = this.getControlValue('PrepPrice');
        if (this.formMode === this.c_s_MODE_ADD) {
            this.dolookUpCallForDesc();
            postSearchParams.set(this.serviceConstants.Action, '1');
            postParams['PrepCode'] = this.getControlValue('PrepCode');
            postParams['ChargeRateCode'] = this.getControlValue('ChargeRateCode');
            postParams['PrepPriceEffectDate'] = this.getControlValue('PrepPriceEffectDate');
        } else {
            postSearchParams.set(this.serviceConstants.Action, '2');
            postParams['ROWID'] = this.getControlValue('ROWID');
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                    return;
                }
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                this.setControlValue('ROWID', data.ttPrepChargeRate);
                this.onUpdateMode();
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.errorService.emitError(error);
            });
    }

    public deleteSavedData(): void {
        let search: any = new URLSearchParams();
        search.set(this.serviceConstants.Action, '3');
        search.set('ROWID', this.getControlValue('ROWID'));
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data['fullError']) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullerror']));
                    return;
                }
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeleted));
                this.resetForm();
                this.onSelectMode();
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.errorService.emitError(error);
            });
    }

    public resetForm(): void {
        this.uiForm.reset();
        this.ellipsisQueryParams.prepChargeRateEllipsis.childConfigParams['PrepCode'] = '';
        this.dropdown.chargeRateSearch.isActive = { id: '', text: '' };
        this.disableControl('PrepCode', false);
        this.disableControl('ChargeRateCode', true);
        this.disableControl('PrepPriceEffectDate', true);
        this.disableControl('PrepPrice', true);
    }

}
