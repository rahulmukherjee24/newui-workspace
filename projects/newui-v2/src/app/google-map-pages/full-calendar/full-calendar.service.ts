import { Injectable } from '@angular/core';

@Injectable()
export class FullCalendarService {
    private event: any;

    public get draggedEvent(): any {
        return this.event;
    }

    public set draggedEvent(event: any) {
        this.event = event;
    }
}
