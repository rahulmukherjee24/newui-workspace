import { Component, AfterViewInit, Output, EventEmitter, Input, OnInit } from '@angular/core';

import * as $ from 'jquery';
import * as moment from 'moment';
import * as fullCalendar from 'fullcalendar';

import { FullCalendarConstants } from './full-calendar-config';
import { FullCalendarService } from './full-calendar.service';

@Component({
    selector: 'icabs-full-calendar',
    template: `
        <div class="icabs-full-calendar" id="{{ uniqueId }}"></div>
    `
})

export class FullCalendarComponent implements OnInit, AfterViewInit {
    // Decorated Properties
    @Input() config: Record<string, any> = FullCalendarConstants.c_obj_CALENDAR_CONFIG_DEFAULT;
    @Input() id: number = 0;
    @Output() onCalendarClick: EventEmitter<any>;
    @Output() onViewRender: EventEmitter<any>;
    @Output() onExternalEventDrop: EventEmitter<any>;
    @Output() onCalendarEventDrop: EventEmitter<any>;
    @Output() onCalendarDayClick: EventEmitter<any>;
    @Output() onDragStart: EventEmitter<any>;
    @Output() onOtherCalendarEventDrop: EventEmitter<any>;

    // Properties
    private rootElement: any;
    private firstDay: number;
    private view: any; // Calendar View
    public event: any; // Calendar Event
    public fullCalendar = fullCalendar;
    public uniqueId: string = 'fullcalendar';

    // Constructor
    constructor(private eventService: FullCalendarService) {
        this.onCalendarClick = new EventEmitter();
        this.onViewRender = new EventEmitter();
        this.onExternalEventDrop = new EventEmitter();
        this.onCalendarEventDrop = new EventEmitter();
        this.onCalendarDayClick = new EventEmitter();
        this.onDragStart = new EventEmitter();
        this.onOtherCalendarEventDrop = new EventEmitter();
    }

    // Getters/Setters
    // Current View
    public get currentViewName(): string {
        return this.view.name;
    }

    // Current View Start
    public get currentViewStart(): any {
        return this.view.start;
    }

    // Current View End
    public get currentViewEnd(): any {
        return this.view.end;
    }

    // Current Event ID
    public get eventFullId(): string {
        return this.event.id;
    }

    // Current Event Title
    public get eventTitle(): string {
        return this.event.title;
    }

    // Current Event Start
    public get eventStart(): any {
        return this.event ? this.event.start : this.view.start;
    }

    // Current Event End
    public get eventEnd(): any {
        return this.event ? this.event.end : this.view.end;
    }

    // Current Event - Is All Day
    public get isAllDay(): boolean {
        return this.event.allDay;
    }

    // Set FirstDay
    public set weekFirstDay(day: number) {
        this.firstDay = day;
    }

    public ngOnInit(): void {
        this.uniqueId += this.id;
    }

    public ngAfterViewInit(): void {
        let calendarConfig: any = this.config;
        this.rootElement = $('#' + this.uniqueId);

        // Update Calendar Config With Events
        // Calendar Click Event
        calendarConfig[FullCalendarConstants.c_s_EVENT_CLICK] = (calendarEvent: any, nativeEvent: any, calendarView: any) => {
            this.handleEventClick(calendarEvent, calendarView);
        };

        calendarConfig[FullCalendarConstants.c_s_CALENDAR_DAY_CLICK] = (date: any, jsEvent: any, calendarView: any) => {
            this.handleDayClick(date, calendarView);
        };
        // View Display Event; Executes When A View(Month, Week, Day) Comes Live
        calendarConfig[FullCalendarConstants.c_s_EVENT_VIEWRENDER] = (calendarView: any) => {
            /**
             * View render event will get called twice; Since week start day will get modified.
             * If the view name is same as earlier; Do not execute start day modification
             * This will suppress infinite view render execution
             */
            if (this.view && calendarView.name !== this.view.name) {
                setTimeout(() => {
                    this.rootElement.fullCalendar(FullCalendarConstants.c_s_OPTION, FullCalendarConstants.c_s_FIRST_DAY, this.view.name === FullCalendarConstants.c_s_VIEW_MONTH ? 0 : this.firstDay);
                }, 10);
            }
            this.view = calendarView;
            this.handleViewDisplay(calendarView);
        };
        calendarConfig[FullCalendarConstants.c_s_CALENDAR_EVENTRENDER] = (event) => {
            /**
             * Workaround Copied From Pen - https://codepen.io/subodhghulaxe/pen/myxyJg?editors=0011
             * This Is Required To Make The Functionality OF Dragging Between Calendars Working
             * Reverted The Event Back To Original Position To Calculate And Then Place It To New Position
             * TSLint Rule Disabled Intentiontionally; Lexical Scope Is Throwing Error
             */
            // tslint:disable-next-line: typedef
            $('.fc-draggable').draggable({
                zIndex: 999,
                revert: true,// will cause the event to go back to its original position after drag
                revertDuration: 0
            });
        };
        // Configure Drop For External Events
        calendarConfig[FullCalendarConstants.c_s_EXTERNAL_EVENT_DROP] = (date, allDay, jsEvent) => {
            let sequence: number = $(jsEvent.helper).data('eventIndex');

            if (sequence) {
                this.handleExternalDropEvent($(jsEvent.helper).data('eventIndex'), date);
            } else {
                this.handleOtherCalendarDropEvent(date);
            }
        };
        // Configure Drop For Calendar Events
        calendarConfig[FullCalendarConstants.c_s_CALENDAR_EVENT_DROP] = (event, delta, revertFunc) => {
            this.onCalendarEventDrop.emit({
                calendarEvent: event,
                undo: revertFunc
            });
        };
        // Configure Resize For Calendar Events
        calendarConfig[FullCalendarConstants.c_s_CALENDAR_EVENT_RESIZE] = (event, delta, revertFunc) => {
            revertFunc();
        };

        // Configure Drag Start For Calendar Events
        calendarConfig[FullCalendarConstants.c_s_CALENDAR_EVENT_DRAGSTART] = (event, jsEvent, ui, view) => {
            let id: string = event.source.calendar.el[0].id;
            this.eventService.draggedEvent = event;

            this.onDragStart.emit({
                index: id.replace('fullcalendar', '')
            });
        };

        this.rootElement.fullCalendar(calendarConfig);
        this.view = this.rootElement.fullCalendar('getView');
        this.clear();
    }

    // Private Methods
    // Initialize Object Properties
    private initProperties(view: any, event?: any): void {
        this.view = view;
        this.event = event || null;
    }

    // Calendar Event Click
    private handleEventClick(calendarEvent: any, calendarView: any): void {
        this.initProperties(calendarView, calendarEvent);
        this.onCalendarClick.emit();
    }

    private handleDayClick(date: any, calendarView: any): void {
        this.onCalendarDayClick.emit({
            clickDate: date
        });
    }

    // Calendar View Initialization
    private handleViewDisplay(calendarView: any): void {
        this.initProperties(calendarView);
        this.onViewRender.emit();
    }

    // Emit External Event Drop Event
    private handleExternalDropEvent(sequence: number, date: any): any {
        this.onExternalEventDrop.emit({
            index: sequence,
            dropDate: date
        });
    }

    private handleOtherCalendarDropEvent(date: any): any {
        let event: Record<string, Object> = {
            event: this.eventService.draggedEvent,
            date: date
        };

        this.onOtherCalendarEventDrop.emit(event);
    }

    // Public Methods
    // Clear Calendar
    public clear(): void {
        if (this.rootElement) {
            this.rootElement.fullCalendar(FullCalendarConstants.c_s_EVENT_REMOVEEVENTS);
        }
    }

    // Render Data In Calendar
    public renderData(data: any): void {
        if (data) {
            this.clear();
            this.rootElement.fullCalendar('addEventSource', data);
        }
    }

    // Go To Dates
    public gotoView(view: string, date?: any): void {
        this.rootElement.fullCalendar(FullCalendarConstants.c_s_EVENT_GOTODATE, date);
        this.rootElement.fullCalendar(FullCalendarConstants.c_s_EVENT_CHANGEVIEW, view);
    }

    // Go To Day View
    public gotoDate(date: any): void {
        this.rootElement.fullCalendar(FullCalendarConstants.c_s_EVENT_GOTODATE, date);
        this.rootElement.fullCalendar(FullCalendarConstants.c_s_EVENT_CHANGEVIEW, FullCalendarConstants.c_s_VIEW_DAY);
    }

    // Go To Month View
    public gotoMonth(year: number, month: number): void {
        let date: any = moment().year(year).month(month);
        this.rootElement.fullCalendar(FullCalendarConstants.c_s_EVENT_GOTODATE, date);
    }

    // Current Event Start - Formatted
    public getFormattedViewStart(format?: string): any {
        return this.view.start.format(format || '');
    }

    // Current Event Start - Formatted
    public getFormattedViewEnd(format?: string): any {
        // If Event Does Not Have An End Set Start To End
        let end: any = this.view.end || this.view.start;
        return end.format(format || '');
    }

    // Current Event Start - Formatted
    public getFormattedEventStart(format?: string): any {
        return this.event.start.format(format || '');
    }

    // Current Event Start - Formatted
    public getFormattedEventEnd(format?: string): any {
        // If Event Does Not Have An End Set Start To End
        let end: any = this.event.end || this.event.start;
        return end.format(format || '');
    }
}
