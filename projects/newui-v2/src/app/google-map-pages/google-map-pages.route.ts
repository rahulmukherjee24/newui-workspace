import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoogleMapPagesModuleRoutesConstant } from '@base/PageRoutes';
import { GoogleMapPagesRootComponent } from './google-map-pages.component';
import { RoutingSearchComponent } from './iCABSARoutingSearch';
import { TechnicianVisitDiaryGridComponent } from './TechnicianVisitDiary/iCABSATechnicianVisitDiaryGrid.component';
import { MultiTechnicianVisitDiaryGridComponent } from './MultiServiceAreaTechDiary/iCABSAMultiTechVisitDiaryGrid.component';

const routes: Routes = [
    {
        path: '', component: GoogleMapPagesRootComponent, children: [
            { path: GoogleMapPagesModuleRoutesConstant.ICABSATECHNICIANVISITDIARYGRID, component: TechnicianVisitDiaryGridComponent },
            { path: GoogleMapPagesModuleRoutesConstant.ICABSAROUTINGSEARCH, component: RoutingSearchComponent },
            { path: GoogleMapPagesModuleRoutesConstant.ICABSAMULTITECHVISITDIARYGRID, component: MultiTechnicianVisitDiaryGridComponent }
        ], data: { domain: 'GOOGLEMAP' }
    }
];

export const GoogleMapPagesRootRouteDefinitions: ModuleWithProviders = RouterModule.forChild(routes);
