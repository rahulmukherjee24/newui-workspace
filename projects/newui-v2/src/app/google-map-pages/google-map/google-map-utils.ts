export class GoogleMapConstants {
    // Defaults
    public static readonly c_obj_INIT_DEFAULT: any = {
        zoom: 15
    };
    public static readonly c_obj_TRAVELTIME_DEFAULT: any = {
        travelMode: 'DRIVING'
    };

    // Key Names
    public static readonly c_s_REQPARAM_ORIGIN: string = 'origin';
    public static readonly c_s_REQPARAM_DESTINATION: string = 'destination';
    public static readonly c_s_ERROR: string = 'errorMessage';
    public static readonly c_s_FULL_ERROR: string = 'fullError';
    public static readonly c_s_TRAVEL_TIME: string = 'travelTime';
    public static readonly c_s_CARRY_FORWARD_DATA: string = 'carryForward';
}

export class GoogleMapLocationData {
    private lattitude: any;
    private longitude: any;

    constructor(longitude: number, lattitude: number) {
        this.lattitude = lattitude;
        this.longitude = longitude;
    }

    public get locationLattitude(): any {
        return this.lattitude;
    }

    public get locationLongitude(): any {
        return this.longitude;
    }

    public get locationLatLng(): string {
        return this.lattitude + ', ' + this.longitude;
    }
}
