import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { FullCalendarComponent } from './full-calendar/full-calendar';
import { GoogleMapComponent } from './google-map/google-map';
import { GoogleMapPagesRootComponent } from './google-map-pages.component';
import { GoogleMapPagesRootRouteDefinitions } from './google-map-pages.route';
import { InternalSearchModule } from '@internal/search.module';
import { RoutingSearchComponent } from './iCABSARoutingSearch';
import { SharedModule } from '@shared/shared.module';
import { TechnicianVisitDiaryGridComponent } from './TechnicianVisitDiary/iCABSATechnicianVisitDiaryGrid.component';
import { MultiTechnicianVisitDiaryGridComponent } from './MultiServiceAreaTechDiary/iCABSAMultiTechVisitDiaryGrid.component';
import { LegendModalComponent } from './common/legend-modal/legend-modal.component';
import { SortOptionsModalComponent } from './common/sort-options/sort-options-modal.component';
import { VisitDetailsModalComponent } from './common/visit-details/visit-details-modal.component';
import { FullCalendarService } from './full-calendar/full-calendar.service';

@NgModule({
  imports: [
    HttpClientModule,
    SharedModule,
    InternalSearchModule,
    GoogleMapPagesRootRouteDefinitions
  ],
  exports: [GoogleMapComponent, FullCalendarComponent],
  declarations: [
    GoogleMapPagesRootComponent,
    GoogleMapComponent,
    FullCalendarComponent,
    LegendModalComponent,
    SortOptionsModalComponent,
    VisitDetailsModalComponent,
    TechnicianVisitDiaryGridComponent,
    RoutingSearchComponent,
    MultiTechnicianVisitDiaryGridComponent
  ],
  providers: [
    FullCalendarService
  ]
})
export class GoogleMapPagesModule { }
