export class VisitDetailsModalConstants {
    // Static ReadOnly Commands
    public static readonly c_s_COMMAND_PLAN: string = 'plan';
    public static readonly c_s_COMMAND_VISITS: string = 'visits';
    public static readonly c_s_COMMAND_SERVICECOVER: string = 'servicecovers';
    public static readonly c_s_COMMAND_UNDO: string = 'undo';
    public static readonly c_s_COMMAND_REFRESH: string = 'refresh';
}
