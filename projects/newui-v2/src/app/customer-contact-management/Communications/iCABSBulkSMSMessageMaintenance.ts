import { Component, OnInit, OnDestroy, Injector, ViewChild, AfterContentInit } from '@angular/core';

import { BaseComponent } from '@base/BaseComponent';
import { CCMModuleRoutes } from '@base/PageRoutes';
import { ErrorCallback } from '@base/Callback';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from '@shared/constants/message.constant';
import { OccupationSearchComponent } from '@internal/search/iCABSSOccupationSearch.component';
import { PageIdentifier } from '@base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBulkSMSMessageMaintenance.html'
})

export class BulkSMSMessageMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit, ErrorCallback {
    @ViewChild('errorModal') public errorModal;
    @ViewChild('messageModal') public messageModal;

    public autoOpen: boolean;
    public controls = [
        { name: 'OccupationCode', required: true },
        { name: 'Message', required: true }
    ];
    public inputParamsOccupationalSearch: any = {
        'parentMode': 'LookUp-String',
        'OccupationCodeString': null
    };
    public lvl: string = '';
    public messageContent: string;
    public messageTitle: string;
    public modalTitle: string = '';
    public occSearchComponent = OccupationSearchComponent;
    public occupationCode: string;
    public pageId: string = '';
    public pageTitle: string = '';
    public queryParams: any = {
        operation: 'ContactManagement/iCABSBulkSMSMessageMaintenance',
        module: 'notification',
        methodtype: 'ccm/maintenance',
        action: '1'
    };
    public showErrorHeader: boolean = true;
    public showHeader: boolean = true;
    public showMessageHeader: boolean = true;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBULKSMSMESSAGEMAINTENANCE;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        // Set message call back
        this.window_onload();
        this.setMessageCallback(this);
        this.autoOpen = true;

        // Set error message call back
        //this.setErrorCallback(this);
    }

    public showErrorModal(data: any): void {
        this.errorModal.show(data, true);
    }

    public showMessageModal(data: any): void {
        this.messageModal.show({ msg: data.msg, title: 'Message' }, false);
    }

    public onOccupationDataReturn(data: any): void {
        let val: string = '';
        if (data) {
            val = this.getControlValue('OccupationCode');
            if (val) {
                val = val + ',' + data;
            } else {
                val = data;
            }
            this.setControlValue('OccupationCode', val);
            this.occupationSearchOnChange(val);

            //make CBB disable
            this.setFormMode(this.c_s_MODE_UPDATE);
        }
    }

    public window_onload(): void {
        this.setPageTitle(this.router.url);

    }

    private setPageTitle(url: string): void {
        if (url) {
            if (url.search(CCMModuleRoutes.SENDBULKSMSBUSINESS) !== -1) {
                this.lvl = ' - Business';
            } else if (url.search(CCMModuleRoutes.SENDBULKSMSBRANCH) !== -1) {
                this.lvl = ' - Branch';
            } else if (url.search(CCMModuleRoutes.SENDBULKSMSACCOUNT) !== -1) {
                this.lvl = ' - Account';
            }
            this.pageTitle = this.pageTitle.concat(this.lvl);
            this.getTranslatedValue('Bulk Send SMS Message', null).subscribe((res: string) => {
                this.zone.run(() => {
                    if (res) {
                        this.utils.setTitle(res + this.pageTitle);
                    }
                });

            });
        }
    }

    public occupationSearchOnChange(data: any): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'OccupationCode', true);
        if (data) {
            this.setControlValue('OccupationCode', data);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'OccupationCode', false);
        } else {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'OccupationCode', true);
        }
    }

    public smsMsgOnChange(data: any): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'Message', true);
        if (data.trim().length !== 0) {
            this.setControlValue('Message', data);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'Message', false);
        } else {
            this.setControlValue('Message', '');
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'Message', true);
        }
    }

    public save(): void {
        let occCode = this.getControlValue('OccupationCode');
        let msg = this.getControlValue('Message');
        if (msg.trim().length !== 0 && occCode.trim().length !== 0) {
            this.ajaxSource.next(this.ajaxconstant.START);
            let searchParams: QueryParams = new QueryParams();
            searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
            searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
            if (this.lvl === ' - Branch') {
                searchParams.set(this.serviceConstants.BranchNumber, this.utils.getBranchCode());
            }
            searchParams.set(this.serviceConstants.Action, this.queryParams.action);
            searchParams.set('OccupationCodeString', occCode);
            searchParams.set('SMSMessageText', msg);
            this.httpService.makeGetRequest(this.queryParams.methodtype, this.queryParams.module, this.queryParams.operation, searchParams).subscribe(
                (data) => {
                    if (data.hasError) {
                        this.errorService.emitError(data);
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.messageSent));
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
        }
        else {
            this.setControlValue('Message', '');
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'OccupationCode', true);
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'Message', true);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'OccupationCode', true);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'Message', true);
        }
    }

    public cancel(): void {
        this.setControlValue('OccupationCode', '');
        this.setControlValue('Message', '');
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'OccupationCode', false);
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'Message', false);

        //make CBB enable
        this.setFormMode(this.c_s_MODE_SELECT);

    }

    public occupationCodekey_down(data: any): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'OccupationCode', true);
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'OccupationCode', false);
    }

    public smsMsgkey_down(data: any): void {
        if (data.trim().length !== 0) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'Message', true);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'Message', false);
        }
    }
}

