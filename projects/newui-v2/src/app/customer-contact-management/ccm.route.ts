import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BulkSMSMessageMaintenanceComponent } from './Communications/iCABSBulkSMSMessageMaintenance';
import { CallCentreGridComponent } from './CustomerContact/iCABSCMCallCentreGrid.component';
import { CallCentreGridNewContactComponent } from './CallCentreGridNewContact/iCABSCMCallCentreGridNewContact';
import { CCMModuleRoutes } from '@base/PageRoutes';
import { CCMRootComponent } from './ccm.component';
import { CentreReviewGridComponent } from './CustomerContact/iCABSCMCallCentreReviewGrid';
import { CMCallAnalysisGridComponent } from './ReportsContact/CallAnalysis/iCABSCMCallAnalysisGrid.component';
import { CMCallCentreAssignGridComponent } from './CustomerContact/ContactCentreAssign/iCABSCMCallCentreAssignGrid.component';
import { ContactActionMaintenanceComponent } from './CustomerContact/iCABSCMContactActionMaintenance.component';
import { ContactMediumGridComponent } from './CustomerContact/iCABSContactMediumGrid.Component';
import { ContactRedirectionComponent } from './Communications/iCABSCMContactRedirection.component';
import { ContactRedirectionMaintenanceComponent } from './Communications/iCABSCMContactRedirectionMaintenance.component';
import { ContactSearchGridComponent } from './CustomerContact/iCABSCMCustomerContactSearchGrid';
import { ContactTypeMaintenanceComponent } from './TableMaintenanceSystem/ContactType/iCABSSContactTypeMaintenance.component';
import { CustomerContactCalloutGridComponent } from './CustomerContact/CalloutSearch/iCABSCMCustomerContactCalloutGrid';
import { EmailGridComponent } from './Communications/EmailMessages/iCABSBEmailGrid.component';
import { NotificationGroupMaintenanceComponent } from './TableMaintenanceSystem/Notifications-Groups/iCABSSNotificationGroupMaintenance.component';
import { NotificationTemplateGridComponent } from '@internal/grid-search/iCABSSNotificationTemplateGrid.component';
import { RouteAwayGuardService } from '@shared/services/route-away-guard.service';
import { SMSMessagesGridComponent } from './Communications/SMSMessages/iCABSCMSMSMessagesGrid.component';
import { SNotificationTemplateMaintenanceComponent } from './TableMaintenanceBusiness/LostBusinessDetail/iCABSSNotificationTemplateMaintenance.component';
import { TeleSalesOrderGridComponent } from './Telesales/iCABSATeleSalesOrderGrid';
import { WorkorderReviewGridComponent } from './CustomerContact/WorkOrderReview/iCABSCMWorkorderReviewGrid.component';

const routes: Routes = [
    {
        path: '', component: CCMRootComponent, children: [
            { path: 'centreReview', component: CentreReviewGridComponent },
            { path: 'contact/search', component: ContactSearchGridComponent },
            { path: CCMModuleRoutes.ICABSCMCALLCENTREGRID, component: CallCentreGridComponent },
            { path: 'business/email', component: EmailGridComponent },
            { path: CCMModuleRoutes.ICABSCMSMSMESSAGESGRID, component: SMSMessagesGridComponent },
            { path: CCMModuleRoutes.ICABSCONTACTMEDIUMGRID, component: ContactMediumGridComponent },
            { path: CCMModuleRoutes.ICABSCMCONTACTACTIONMAINTENANCE, component: ContactActionMaintenanceComponent }, // not found
            { path: CCMModuleRoutes.ICABSCMCALLANALYSISGRID, component: CMCallAnalysisGridComponent },
            { path: CCMModuleRoutes.SENDBULKSMSBUSINESS, component: BulkSMSMessageMaintenanceComponent },
            { path: CCMModuleRoutes.SENDBULKSMSBRANCH, component: BulkSMSMessageMaintenanceComponent },
            { path: CCMModuleRoutes.SENDBULKSMSACCOUNT, component: BulkSMSMessageMaintenanceComponent },
            { path: CCMModuleRoutes.ICABSCMCONTACTREDIRECTION, component: ContactRedirectionComponent },
            { path: CCMModuleRoutes.ICABSCMCONTACTREDIRECTIONMAINTENANCE, component: ContactRedirectionMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: CCMModuleRoutes.ICABSCMCALLCENTREASSIGNGRID, component: CMCallCentreAssignGridComponent },
            { path: CCMModuleRoutes.ICABSCMWORKORDERREVIEWGRID, component: WorkorderReviewGridComponent },
            { path: CCMModuleRoutes.ICABSATELESALESORDERGRID, component: TeleSalesOrderGridComponent },
            { path: CCMModuleRoutes.ICABSCMCALLCENTREGRIDNEWCONTACT, component: CallCentreGridNewContactComponent, canDeactivate: [RouteAwayGuardService] }, // not found
            { path: CCMModuleRoutes.ICABSCMCUSTOMERCONTACTCALLOUTGRID, component: CustomerContactCalloutGridComponent },
            { path: CCMModuleRoutes.ICABSSNOTIFICATIONGROUPMAINTENANCE, component: NotificationGroupMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: CCMModuleRoutes.ICABSSCONTACTTYPEMAINTENANCE, component: ContactTypeMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: CCMModuleRoutes.ICABSSNOTIFICATIONTEMPLATEMAINTENANCE, component: SNotificationTemplateMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: CCMModuleRoutes.ICABSSNOTIFICATIONTEMPLATEGRID, component: NotificationTemplateGridComponent }  // not found
        ], data: { domain: 'CUSTOMER CONTACT MANAGEMENT' }
    }
];

export const CCMRouteDefinitions: ModuleWithProviders = RouterModule.forChild(routes);
