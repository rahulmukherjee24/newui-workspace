import { GlobalNotificationsService } from './../shared/services/global.notifications.service';
import { InvoiceActionTypes } from './actions/invoice';
import { AccountMaintenanceActionTypes } from './actions/account-maintenance';
import { CallCenterActionTypes } from './actions/call-centre-search';
import { ContractActionTypes } from './actions/contract';
import { ActionTypes } from './actions/account';
import { ErrorConstant } from './../shared/constants/error.constant';
import { GlobalConstant } from './../shared/constants/global.constant';
import { DatepickerComponent } from './../shared/components/datepicker/datepicker';
import { MessageConstant } from './../shared/constants/message.constant';
import { environment } from './../environments/environment';
import { ICabsModalVO } from './../shared/components/modal-adv/modal-adv-vo';
import { StaticUtils } from './../shared/services/static.utility';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { GlobalizeService } from './../shared/services/globalize.service';
import { AuthService } from './../shared/services/auth.service';
import { HttpClient } from '@angular/common/http';
import { VariableService } from './../shared/services/variable.service';
import { SubjectService } from './../shared/services/subject.service';
import { Utils } from './../shared/services/utility';
import { Router, NavigationEnd, NavigationStart, NavigationCancel, GuardsCheckEnd } from '@angular/router';
import { RiExchange } from './../shared/services/riExchange';
import { Store } from '@ngrx/store';
import { ModalAdvService } from './../shared/components/modal-adv/modal-adv.service';
import { Component, OnInit, ViewContainerRef, Injector, ViewChild, NgZone, AfterContentInit } from '@angular/core';
import { Location } from '@angular/common';
import { DeepLinkService } from '@shared/services/deeplink.service';
import { FocusService } from '@shared/services/focus.service';

// Declare ga function as ambient
declare const ga: Function;
@Component({
    selector: 'icabs-app',
    template: `
        <div *ngIf="isMobile && !isPortrait" class="mobile-mode-warning-wrapper">
            <div class="warning-content">
                <div><img alt="Rentokil Initial plc - link to home page" src="/assets/images/logo.svg"></div>
                <div class="mt20">{{warningOnLandScape | translate }}</div>
            </div>
        </div>

            <icabs-header [displayUser]=true *ngIf="showHeader" [hidden]="isMobile"></icabs-header>
            <router-outlet></router-outlet>
            <footer class="parent-footer" [ngClass]="isMobile ? 'onMobile' : '' ">
                <div class="custom-footer">&copy; 2016 Rentokil Initial PLC and subject to conditions in the legal statement.
                <a href="#"><span>Home</span></a> |
                <a href="https://rentokilinitial.service-now.com/navpage.do/" target="_blank"><span>Service Now</span></a>
                </div>
            </footer>
            <icabs-spinner class='lazy-spinner' #spinner></icabs-spinner>
            <icabs-modal #errorModal="child" [(showHeader)]="showErrorHeader" [config]="{backdrop: 'static'}"></icabs-modal>
            <icabs-modal-adv #modalAdv="child"  [ngClass]="isMobile ? 'onMobile' : '' "></icabs-modal-adv>
            <div class="ajax-overlay" #overlay style="display: none;"></div>
            <icabs-custom-alert #customAlert [alertMessage]="notification.message" [messageType]="notification.type"></icabs-custom-alert>

    `
})

export class AppComponent implements OnInit, AfterContentInit {
    @ViewChild('errorModal') public errorModal;
    @ViewChild('modalAdv') public modalAdv;
    @ViewChild('overlay') public overlay;
    @ViewChild('spinner') public spinner;
    private viewContainerRef: ViewContainerRef;
    private pagesWithHeaderHidden: Array<any> = [
        '/',
        '/application/login',
        '/application/setup'
    ];
    public showHeader: boolean = false;
    private routeArray = [];
    public showErrorHeader: boolean = true;
    public url = '';
    public isMobile: boolean = false;
    public isPortrait: boolean = true;
    public warningOnLandScape: string = 'This page is NOT SUPPORTING Landscape Mode, Please switch to Portrait Mode......';

    //Inject classes
    public modalAdvService: ModalAdvService;

    public constructor(
        injector: Injector,
        viewContainerRef: ViewContainerRef,
        private store: Store<any>,
        private riExchange: RiExchange,
        private router: Router,
        private util: Utils,
        private subjectService: SubjectService,
        private zone: NgZone,
        private variableService: VariableService,
        private location: Location,
        private http: HttpClient,
        private authService: AuthService,
        private globalize: GlobalizeService,
        private _ls: LocalStorageService,
        private sessionStorage: SessionStorageService,
        public notification: GlobalNotificationsService,
        public deeplink: DeepLinkService,
        private focusService: FocusService
    ) {
        this.injectServices(injector);
        //Code Below tracks page route for Google Analytic-->starts
        router.events.distinctUntilChanged((previous: any, current: any) => {
            if (current instanceof NavigationEnd) {
                return previous.url === current.url;
            }
            return true;
        }).subscribe((x: any) => {
            let email: string = this.util.getEmail();
            ga('set', 'userId', StaticUtils.getSHA256Hash({ value: email }));
            ga('set', 'page', x.url);
            ga('send', 'pageview');
        });
        this.subjectService.getObservable().subscribe((error) => {
            if (error !== 0) {
                this.setDisplay(false);
                this.router.navigate([], { skipLocationChange: true, preserveQueryParams: true });
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.TomcatErrorMessage, error.msg));
            }
        });

        //Code Below tracks page route for Google Analytic-->ends
        this.viewContainerRef = viewContainerRef;

        // Code Added To Clear The NavStack For The Navigtaions Where RouteGaurd Is Not Present
        this.router.events.pairwise().filter(event => event[0] instanceof NavigationEnd).subscribe((event) => {
            let previousURL: string = event[0]['url'];
            let currentURL: string = event[1]['url'];
            let hasNoRouteGaurd: any = (StaticUtils.getParameterByName(previousURL, 'noroutegaurd') && !this.variableService.getMenuClick()) || StaticUtils.getParameterByName(currentURL, 'noroutegaurd');

            if (hasNoRouteGaurd && this.variableService.getMenuClick()) {
                this.riExchange.clearNavigationData();
            }

            this.variableService.setMenuClick(false);
        });

        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (navigator && typeof navigator['onLine'] !== 'undefined' && navigator.onLine === false && environment['NODE_ENV'] !== 'DEV') {
                    this.triggerNetworkMessage();
                }
                this.url = this.router.url;
                this.setDisplay(true);
                this.showHeader = false;

                if (this.pagesWithHeaderHidden.indexOf(event['url']) < 0 && this.authService.isSignedIn()) {
                    this.showHeader = true;
                } else {
                    this.showHeader = false;
                }
                DatepickerComponent.dateInstance = [];
                DatepickerComponent.dateInstanceCounter = 0;
                if (this.variableService.getMenuClick() === true || this.variableService.getAutoCompleteSelection() === true) {
                    this.clearStore();
                    this.variableService.setContractStoreData({});
                    this.riExchange.clearNavigationData();
                    this.riExchange.clearRouterParams();
                }
            }
            if (event instanceof NavigationCancel) {
                this.setDisplay(false);
            }
            /**
             * Check Added For The Case When:
             *  - Navigation Menu Clicked
             *  - Routing Away Is Activated
             * This Block Will Clean The Navigation Stack Data And Last Grid Data
             * Important For The Pages Where RouteGuard Is Not Registered; E.g. - Report Pages
             */
            if (event instanceof GuardsCheckEnd) {
                if (event.shouldActivate && (this.variableService.getMenuClick() || this.variableService.getAutoCompleteSelection())) {
                    this.riExchange.clearNavigationData();
                    this.sessionStorage.clear('last-grid');
                }
            }
            if (event instanceof NavigationEnd) {
                const shouldSetFocus = event['url'].indexOf('/postlogin') === -1 && event['url'] !== '/';
                this.focusService.resetElemToBeFocused(shouldSetFocus);
                if (this.isMobile) {
                    this.riExchange.setRouterParams(event['url'].split('?')[1]);
                }

                this.util.setCurrentUrl(event['url']);
                this.setDisplay(false);
                setTimeout(() => {
                    window.scrollTo(0, 0);
                }, 0);
                if (this.url !== event['url']) {
                    if (event['url'].indexOf('/postlogin') !== -1 || event['url'] === '/') {
                        this.clearStore();
                        this.variableService.setContractStoreData({});
                        this.riExchange.clearNavigationData();
                        this.riExchange.clearRouterParams();
                    }
                }
                this.showHeader = false;
                if (this.pagesWithHeaderHidden.indexOf(event['url']) < 0 || this.pagesWithHeaderHidden.indexOf(event.urlAfterRedirects) < 0) {
                    this.showHeader = true;
                } else {
                    this.showHeader = false;
                }
                this.variableService.setLogoutClick(false);
                this.variableService.setBackClick(false);
                this.variableService.setAutoCompleteSelection(false);

                let n = this.location.path().indexOf('?');
                this.routeArray.push(this.location.path().substring(0, n !== -1 ? n : this.location.path().length));
                this.routeArray = this.routeArray.filter((item, i, ar) => { return ar.indexOf(item) === i; });
                if (this.routeArray.length > Number(environment['SCREEN_MEMORY_THRESOLD']) && this.location.path().indexOf(GlobalConstant.Configuration.Home) !== -1) {
                    window.location.reload();
                }
            }

            if (event instanceof NavigationCancel) {
                this.url = this.router.url;
                this.variableService.setMenuClick(false);
                this.variableService.setAutoCompleteSelection(false);
                this.variableService.setBackClick(false);
                this.util.resetOptions();
            }
        });
        this.modalAdvService.getObservableSource().subscribe((data) => {
            if (data) {
                this.modalAdv.show(data);
            }
        });
    }

    ngOnInit(): void {
        if (navigator && typeof navigator['onLine'] !== 'undefined') {
            window.addEventListener('offline', (e) => {
                this.triggerNetworkMessage();
            });
            window.addEventListener('online', (e) => {
                this.errorModal.hide();
            });
        }
        this.authService.handleClientLoad(false, false);
        if (!this.authService.isSignedIn()) {
            this.deeplink.initDeepLink();
            this.authService.signOut();
            return;
        }
        if (this.authService.isSignedIn()) {
            let email: string = this.util.getEmail();
            this.globalize.init();
            window['ga']('set', 'userId', StaticUtils.getSHA256Hash({ value: email }));
        }
        if (!this._ls.retrieve('quicklinks')) {
            this.authService.signOut();
        }
    }

    ngAfterContentInit(): void {
        setTimeout(() => {
            this.isMobile = navigator.userAgent.indexOf('Mobile') >= 0;
            this.isPortrait = this.riExchange.isPortrait();
            if (this.isMobile) {
                let body = document.getElementsByTagName('body');
                body[0].className += ' onMobile';
                window.addEventListener('orientationchange', () => {
                    this.isPortrait = screen.orientation['type'].indexOf('portrait') >= 0;
                    this.isMobile = this.riExchange.isMobile(); // ita-1001
                }, false);
            }
        }, 500);
    }

    private triggerNetworkMessage(): void {
        this.zone.run(() => {
            this.errorModal.show({
                error: {
                    title: 'Error',
                    message: ErrorConstant.Message.InternetFail
                }
            }, true);
        });
    }

    private setDisplay(visible: boolean): void {
        if (visible) {
            this.overlay.nativeElement['style'].display = 'block';
            this.spinner.isRunning = visible;
            // IUI-13134 - JavaScript is used since angular halts the change detection process during lazy loading of modules causing the throbber not to come up
            document.querySelector('icabs-app .lazy-spinner .spinner')['style'].display = 'block';
            document.querySelector('icabs-app .ajax-overlay')['style'].display = 'block';
        } else {
            this.overlay.nativeElement['style'].display = 'none';
            this.spinner.isRunning = visible;
            document.querySelector('icabs-app .lazy-spinner .spinner')['style'].display = 'none';
            document.querySelector('icabs-app .ajax-overlay')['style'].display = 'none';
        }
    }

    private injectServices(injector: Injector): void {
        this.modalAdvService = injector.get(ModalAdvService);
    }

    public clearStore(): void {
        this.store.dispatch({ type: ActionTypes.CLEAR_ACCOUNT_DATA });
        this.store.dispatch({ type: ContractActionTypes.CLEAR_ALL });
        this.store.dispatch({ type: CallCenterActionTypes.CLEAR_ALL });
        this.store.dispatch({ type: AccountMaintenanceActionTypes.CLEAR_ALL });
        this.store.dispatch({ type: InvoiceActionTypes.CLEAR });
    }
}
