export interface RenegsUpdateRenegInterface {
    advQuoteRef?: string;
    BusinessCode?: string;
    ContractAnnualValue?: any;
    ContractCommenceDate?: any;
    ContractName: string;
    ContractNumber: string;
    CurrentPremises?: string;
    dlStatusCode?: string;
    EffectiveDate?: any;
    ExistingAccountInd?: string;
    ExistingContractInd?: string;
    InvoiceAnnivDate?: any;
    InvoiceFrequencyCode?: string;
    NegBranchNumber?: string;
    OldContractCommenceDate?: any;
    RenegAccountName?: string;
    RenegAccountNumber?: string;
    RenegType: string;
    ScannedContractName?: string;
    ScannedContractNumber?: string;
    SubScreenCodeList?: string;
    SubScreenDescList?: string;
    ToAccountName?: string;
    ToAccountNumber?: string;
}
