
interface PageData {
    pageNumber?: number;
    lastPageNumber?: number;
}

interface Title {
    title: string;
    textColor?: string;
    toolTip?: string;
}

interface Cell {
    text: string;
    textColor?: string;
    backgroundColor?: string;
    toolTip?: string;
    colSpan?: number;
}

interface Header {
    title: Title[];
    cells: Cell[];
}

interface Cell2 {
    rowID?: string;
    text: string;
    textColor?: string;
    borderColor?: string;
    backgroundColor?: string;
    cellColor?: string;
    additionalData?: string;
    drillDown?: boolean;
    toolTip?: string;
    colSpan: number;
    imageWidth?: any;
    imageHeight?: any;
}

interface Body {
    cells: Cell2[];
}

export interface RenegsGridInterface {
    pageData: PageData;
    header: Header;
    body: Body;
    footer?: any;
}



