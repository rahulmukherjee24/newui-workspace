export interface RenegsFilterOptionsInterface {
    value: string;
    title: string;
    selected?: any;
}
