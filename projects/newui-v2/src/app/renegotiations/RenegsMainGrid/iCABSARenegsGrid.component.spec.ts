import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenegsGridComponent } from './iCABSARenegsGrid.component';

describe('RenegsGridComponent', () => {
    let component: RenegsGridComponent;
    let fixture: ComponentFixture<RenegsGridComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RenegsGridComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RenegsGridComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
