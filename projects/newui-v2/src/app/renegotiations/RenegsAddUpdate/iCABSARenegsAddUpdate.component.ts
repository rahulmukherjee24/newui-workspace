import { Component, Injector, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../base/BaseComponent';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from '../../base/PageIdentifier';

import { ContractSearchComponent } from './../../internal/search/iCABSAContractSearch';
import { RenegotiationRoutes } from './../../base/PageRoutes';
import { RenegsUpdateRenegInterface } from './../RenegsInterfaces/iCABSARenegsUpdateReneg.interface';
import { RenegsConstants } from '../iCABSARenegs.constant';
import { RenegsFormControlsInterface } from '../RenegsInterfaces/iCABSARenegsFormControls.interface';
import { RenegsXHRParamsInterface } from './../RenegsInterfaces/iCABSARenegsXHRParams.interface';
@Component({
    selector: 'icabs-renegs-add-update',
    templateUrl: 'iCABSARenegsAddUpdate.component.html'
})

export class RenegsAddUpdateComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('contractSearch') public contractSearch;
    @ViewChild('customAlert') public customAlert;
    @ViewChild('accountNameField') public accountNameField;
    @ViewChild('contractNameField') public contractNameField;
    @ViewChild('EffectiveDate') public EffectiveDate: any;

    private delayBy: number = 1500;
    private existingAccountMandatory: boolean = false;
    private existingContractMandatory: boolean = false;
    private xhrParams: RenegsXHRParamsInterface = {
        module: 'contract-admin',
        method: 'contract-management/maintenance',
        operation: 'Application/iCABSRenegMaintenance'
    };

    public contractSearchComponent = ContractSearchComponent;
    public controls: RenegsFormControlsInterface[] = [
        { name: 'AccountNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'AccountName', type: MntConst.eTypeText, disabled: true },
        { name: 'AmendmentRef', type: MntConst.eTypeText, disabled: true },
        { name: 'CommenceDate', type: MntConst.eTypeDate, required: true },
        { name: 'ContractNumber', type: MntConst.eTypeText, required: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'contracttypecode', type: MntConst.eTypeText, disabled: true },
        { name: 'CurrentPremises', type: MntConst.eTypeText, disabled: true },
        { name: 'EffectiveDate', type: MntConst.eTypeDate, required: false },
        { name: 'UseExistAccount', type: MntConst.eTypeText },
        { name: 'ExistAccountNumber', type: MntConst.eTypeText, required: this.existingAccountMandatory },
        { name: 'ExistAccountNumberDetails', type: MntConst.eTypeText },
        { name: 'ExistCommDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'UseExistContract', type: MntConst.eTypeText },
        { name: 'ExistContractNumber', type: MntConst.eTypeText, required: this.existingContractMandatory },
        { name: 'ExistContractNumberDetails', type: MntConst.eTypeText },
        { name: 'InvoiceAnnivDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'InvoiceFreq', type: MntConst.eTypeText, disabled: true },
        { name: 'NegBranch', type: MntConst.eTypeText, disabled: true },
        { name: 'RenegsFilter' },
        { name: 'RenegType', required: true, value: '1' },
        { name: 'Value', type: MntConst.eTypeCurrency, disabled: true }
    ];
    public customAlertMsg: any;
    public disableForm: boolean = false;
    public enableAutoOpen: boolean = false;
    public inputParamsContractSearch: any = {
        'parentMode': 'adnewreneg',
        'accountNumber': '',
        'currentContractType': 'C',
        'enableAccountSearch': false
    };
    public messageType: string;
    public pageId: string;
    public pageTitle: string;
    public parentMode: string;
    public renegsConstants: RenegsConstants = RenegsConstants;
    public renegType: string;
    public updateReneg: boolean = false;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARENEGADDUPDATE;
        this.browserTitle = this.pageTitle = RenegsConstants.pageTitleAddReneg;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.parentMode = this.riExchange.getParentMode() || 'addNewReneg';
        this.setupPageParams();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private changeFormFieldsStateInUpdateMode(): void {
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'AccountNumber', true);
        this.enableControls(['AccountNumber']);
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'AccountName', true);
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'AmendmentRef', true);
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'ContractNumber', true);
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'ContractName', true);
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'CurrentPremises', true);
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'ExistCommDate', true);
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'InvoiceAnnivDate', true);
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'InvoiceFreq', true);
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'NegBranch', true);
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'RenegType', true);
        this.riExchange.riInputElement.ReadOnly(this.uiForm, 'Value', true);

        let searchupdateParams: QueryParams = this.getURLSearchParamObject();
        searchupdateParams.set('action', '0');
        searchupdateParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchupdateParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchupdateParams.set('advQuoteRef', this.riExchange.getParentHTMLValue('advQuoteRef'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, searchupdateParams).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data['hasError'] || data['errorMessage']) {
                this.notifyOnAPIError(data);

            } else {
                this.disableControls(['UseExistAccount', 'UseExistContract', 'CommenceDate', 'EffectiveDate']);
                this.setControlValue('AmendmentRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('ContractName', data.ContractName);
                this.setControlValue('contracttypecode', data.ContractTypeCode);
                this.setControlValue('AccountNumber', data.RenegAccountNumber);
                this.setControlValue('AccountName', data.RenegAccountName);
                this.setControlValue('ExistCommDate', data.OldContractCommenceDate);
                this.setControlValue('NegBranch', data.NegBranchNumber);
                this.setControlValue('Value', data.ContractAnnualValue);
                this.setControlValue('CurrentPremises', data.CurrentPremises);
                this.setControlValue('InvoiceAnnivDate', data.InvoiceAnnivDate);
                this.setControlValue('InvoiceFreq', data.InvoiceFrequencyCode);
                this.setControlValue('CommenceDate', data.ContractCommenceDate);

                this.setControlValue('EffectiveDate', data.EffectiveDate);

                if (data.RenegType.toUpperCase() === 'RNGBOTH') {
                    this.setControlValue('RenegType', 1);


                } else if (data.RenegType.toUpperCase() === 'RNGCOPY') {  //EffectiveDate blank and disbaled if RngCopy only
                    this.setControlValue('RenegType', 2);
                    this.setControlValue('EffectiveDate', '');
                    this.riExchange.riInputElement.Disable(this.uiForm, 'EffectiveDate');

                } else {
                    this.setControlValue('RenegType', 3);
                }

                if (data.ExistingAccountInd.toLowerCase() === 'no' || data.ExistingAccountInd === '') {
                    this.setControlValue('UseExistAccount', 0);

                } else {
                    this.existingAccountMandatory = true;
                    this.setControlValue('UseExistAccount', 1);
                    this.setControlValue('ExistAccountNumber', data.ToAccountNumber);
                    this.setControlValue('ExistAccountNumberDetails', data.ToAccountName);
                }

                if (data.ExistingContractInd.toLowerCase() === 'no' || data.ExistingContractInd === '') {
                    this.setControlValue('UseExistContract', 0);

                } else {
                    this.existingContractMandatory = true;
                    this.setControlValue('UseExistContract', 1);
                    this.setControlValue('ExistContractNumber', data.ScannedContractNumber);
                    this.setControlValue('ExistContractNumberDetails', data.ScannedContractName);
                }
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.disableForm = true;
            this.notifyOnAPIError(error);
        });
    }

    private checkPageState(): void {
        if (this.parentMode && this.parentMode.toLowerCase() === 'addnewreneg') {
            this.pageTitle = RenegsConstants.pageTitleAddReneg;
            this.updateReneg = false;
            this.enableAutoOpen = true;
            this.renegType = '';
        } else {
            this.pageTitle = RenegsConstants.pageTitleUpdateReneg;
            this.updateReneg = true;
            this.enableAutoOpen = false;
            this.renegType = 'RNGCOPY';
            this.changeFormFieldsStateInUpdateMode();
        }
    }

    private fetchFieldDetails(value: any, e: any): void {
        // remove fetchAccountName() and fetchContractName() from old code
        let queryLookUp: QueryParams = new QueryParams();
        const numValue: string = (e.target.value).toString();
        const fieldType: string = (value.toLowerCase()) === 'account' ? 'AccountNumber' : 'ContractNumber';
        const fields: string[] = (value.toLowerCase()) === 'account' ? ['AccountName', 'AccountName'] : ['ContractName', 'ContractName', 'AccountName', 'AccountName'];

        if (!numValue || !numValue.length) {
            return;
        }

        let lookupData = [{
            'table': value,
            'query': {
                'BusinessCode': this.pageParams.BusinessCode
            },
            'fields': fields
        }];
        lookupData[0]['query'][fieldType] = numValue;

        queryLookUp.set(this.serviceConstants.Action, '0');
        queryLookUp.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        queryLookUp.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        queryLookUp.set(this.serviceConstants.MaxResults, '5');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.lookUpRequest(queryLookUp, lookupData)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data && data['results'] && data['results'][0].length) {
                    (fieldType) === 'AccountNumber' ? this.setControlValue('ExistAccountNumberDetails', data['results'][0][0]['AccountName']) : this.setControlValue('ExistContractNumberDetails', data['results'][0][0]['ContractName']);

                } else {
                    (fieldType) === 'AccountNumber' ? this.setControlValue('ExistAccountNumberDetails', '') : this.setControlValue('ExistContractNumberDetails', '');
                    this.notifyOnAPIError(data);
                }
                this.disableForm = false;
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.disableForm = true;
                this.notifyOnAPIError(error);
            });
    }

    private getEffectiveDate(): any {
        let date: any;
        this.renegType === 'RNGCOPY' ? date = this.globalize.parseDateToFixedFormat(this.getControlValue('CommenceDate')) : date = this.globalize.parseDateToFixedFormat(this.getControlValue('EffectiveDate'));
        return date;
    }
    public onFetchContractDetails(newVal: any): void {
        if (!newVal.target.value) {
            return;
        }

        let queryParams: QueryParams = new QueryParams();
        queryParams.set('action', '0');
        queryParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        queryParams.set(this.serviceConstants.CountryCode, this.countryCode());
        queryParams.set('ContractTypeCode', this.riExchange.getCurrentContractType());
        queryParams.set(this.serviceConstants.ContractNumber, newVal.target.value);
        queryParams.set(this.serviceConstants.PageSize, '1');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest('contract-management/maintenance', 'contract', 'Application/iCABSAContractMaintenance', queryParams)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response['hasError'] || response['errorMessage']) {
                    this.notifyOnAPIError({ errorMessage: (response['errorMessage'] || RenegsConstants.NoRecord) });
                    this.uiForm.reset();
                } else {
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'AccountName', response['AccountName']);
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'AccountNumber', response['AccountNumber']);
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'ContractName', response['ContractName']);
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'CurrentPremises', response['CurrentPremises']);
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'ExistCommDate', response['ContractCommenceDate']);
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'InvoiceAnnivDate', response['InvoiceAnnivDate']);
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'InvoiceFreq', response['InvoiceFrequencyCode']);
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'NegBranch', response['NegBranchNumber']);
                }
            },
                (error) => {
                    this.notifyOnAPIError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private generateAPIPayloadRenegMaintenance(addNew: boolean): RenegsUpdateRenegInterface {

        let payload: RenegsUpdateRenegInterface = {
            advQuoteRef: (addNew) ? this.getControlValue('AmendmentRef') || '' : this.riExchange.getParentHTMLValue('advQuoteRef') || '',
            ContractAnnualValue: this.globalize.parseCurrencyToFixedFormat(this.getControlValue('Value')) || '',
            ContractCommenceDate: this.globalize.parseDateToFixedFormat(this.getControlValue('CommenceDate')),
            ContractName: this.getControlValue('ContractName'),
            ContractNumber: this.getControlValue('ContractNumber'),
            CurrentPremises: this.getControlValue('CurrentPremises') || '',
            dlStatusCode: '',
            EffectiveDate: this.getEffectiveDate(),
            ExistingAccountInd: (addNew) ? '' : this.getControlValue('UseExistAccount') ? 'yes' : 'no' || '',
            ExistingContractInd: (addNew) ? '' : this.getControlValue('UseExistContract') ? 'yes' : 'no' || '',
            InvoiceAnnivDate: this.globalize.parseDateToFixedFormat(this.getControlValue('InvoiceAnnivDate')),
            InvoiceFrequencyCode: this.getControlValue('InvoiceFreq'),
            NegBranchNumber: this.getControlValue('NegBranch'),
            OldContractCommenceDate: (addNew) ? '' : this.globalize.parseDateToFixedFormat(this.getControlValue('ExistCommDate')),
            RenegAccountName: this.getControlValue('AccountName'),
            RenegAccountNumber: this.getControlValue('AccountNumber'),
            RenegType: this.getControlValue('RenegType') === '1' ? 'RNGBOTH' : this.getControlValue('RenegType') === '2' ? 'RNGCOPY' : 'RNGNEW',
            ScannedContractName: this.getControlValue('ExistContractNumberDetails'),
            ScannedContractNumber: this.getControlValue('ExistContractNumber'),
            SubScreenCodeList: '',
            SubScreenDescList: '',
            ToAccountName: (addNew) ? '' : this.getControlValue('ExistAccountNumberDetails'),
            ToAccountNumber: (addNew) ? '' : this.getControlValue('ExistAccountNumber')
        };

        if (!addNew) { payload['BusinessCode'] = this.businessCode(); }  //only for update mode
        return payload;
    }

    private onAddNewReneg(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, this.pageParams.actionOnAddNew);
        let payload: RenegsUpdateRenegInterface = this.generateAPIPayloadRenegMaintenance(true);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams['method'], this.xhrParams['module'], this.xhrParams['operation'], searchParams, payload)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);

                if (response.hasError || response.errorMessage || response.fullError) {
                    this.notifyOnAPIError(response);

                } else {
                    //success
                    setTimeout(() => {
                        this.uiForm.reset();
                        this.navigate('', RenegotiationRoutes.ICABSARENEGSGRIDFULLPATH);
                    }, 500);
                }
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.notifyOnAPIError(error);
                });
    }

    public onUpdateCompletion(): void {
        this.location.back();
    }

    private notifyOnAPIError(error: any): void {
        this.messageType = ((error['hasError'] || error['errorMessage'])) ? RenegsConstants.msgTypeError : RenegsConstants.msgTypeWarn;
        this.customAlertMsg = {
            msg: error.errorMessage + ' ' + error.fullError,
            timestamp: (new Date()).getMilliseconds()
        };
    }

    private setupPageParams(): void {
        this.pageParams.actionOnAddNew = '1';
        this.pageParams.actionOnUpdate = '2';
        this.pageParams.BusinessCode = this.businessCode();
        this.pageParams.CountryCode = this.countryCode();
        this.checkPageState();
    }

    private onUpdateNewReneg(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, this.pageParams.actionOnUpdate);
        let payload: RenegsUpdateRenegInterface = this.generateAPIPayloadRenegMaintenance(false);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams['method'], this.xhrParams['module'], this.xhrParams['operation'], searchParams, payload)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response.hasError || response.errorMessage || response.fullError) {
                    this.notifyOnAPIError(response);

                } else {
                    //success
                    this.onUpdateCompletion();
                }
            },
                (error) => {
                    this.notifyOnAPIError(error);
                });
    }

    public onDateChange(field: string, evt: any): void {
        if (field === 'EffectiveDate') { this.setControlValue('CommenceDate', evt.value); }
        this.uiForm.controls[field].updateValueAndValidity();
    }

    public onDeleteReneg(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '3');
        let payload = {
            'BusinessCode': this.pageParams.BusinessCode,
            advQuoteRef: this.getControlValue('AmendmentRef')
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams['method'], this.xhrParams['module'], this.xhrParams['operation'], searchParams, payload)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response.hasError || response.errorMessage || response.fullError) {
                    this.notifyOnAPIError(response);

                } else {
                    //success
                    this.onUpdateCompletion();
                }
            },
                (error) => {
                    this.notifyOnAPIError(error);
                });
    }

    public onChangeRenegType(evt: any): void {
        if (evt.toString() === '2') {  //2 = rngcopy
            this.renegType = 'RNGCOPY';
            this.riExchange.riInputElement.SetValue(this.uiForm, 'EffectiveDate', '');
            this.riExchange.riInputElement.SetValue(this.uiForm, 'CommenceDate', new Date());
            this.riExchange.riInputElement.Disable(this.uiForm, 'EffectiveDate');
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EffectiveDate', false);

        } else {
            this.renegType = '';
            this.riExchange.riInputElement.Enable(this.uiForm, 'EffectiveDate');
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EffectiveDate', true);
        }
    }

    public onSelectChange(evt: any): void {
        let renegTypeParam = (this.getControlValue('RenegType') === 3 || this.getControlValue('RenegType') === 1) ? 'new' : 'copy';
        switch (evt) {
            case '0':
                this.navigate('accountDetailsUpdate', RenegotiationRoutes.ICABSARENEGSACCOUNTDETAILSFULLPATH, {
                    advQuoteRef: this.riExchange.getParentHTMLValue('advQuoteRef'),
                    accountNumber: this.getControlValue('AccountNumber')
                });
                break;

            case '1':
                this.navigate('contractDetailsUpdate', RenegotiationRoutes.ICABSARENEGSCONTRACTDETAILSFULLPATH, {
                    advQuoteRef: this.riExchange.getParentHTMLValue('advQuoteRef')
                });
                break;

            case '2':
                this.pageParams.gridCurPage = '1';
                this.navigate('premisesGrid', RenegotiationRoutes.ICABSARENEGSPREMISESGRIDPATH, {
                    advQuoteRef: this.riExchange.getParentHTMLValue('advQuoteRef'),
                    contractNumber: this.getControlValue('ContractNumber'),
                    contractName: this.getControlValue('ContractName') || '',
                    premisesNumber: this.getControlValue('CurrentPremises') || '',
                    renegType: renegTypeParam
                });
                break;

            case '3':
                this.navigate('serviceCoverGrid', RenegotiationRoutes.ICABSARENEGSSERVICECOVERGRIDFULLPATH, {
                    amendmentRef: this.riExchange.getParentHTMLValue('advQuoteRef'),
                    contractNumber: this.getControlValue('ContractNumber'),
                    contractName: this.getControlValue('ContractName') || '',
                    premisesNumber: this.getControlValue('CurrentPremises') || ''
                });
                break;
        }

    }

    public onSaveUpdateReneg(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            if (this.parentMode && this.parentMode.toLowerCase() === 'addnewreneg') { // add new mode
                this.onAddNewReneg();

            } else { // update mode
                this.onUpdateNewReneg();
            }
        }
    }

    public onSetContractNumber(event: any): void {
        this.uiForm.reset();
        this.riExchange.riInputElement.SetValue(this.uiForm, 'AccountName', event.AccountName);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'AccountNumber', event.AccountNumber);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'ExistCommDate', event.ContractCommenceDate);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'ContractName', event.ContractName);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'ContractNumber', event.ContractNumber);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'InvoiceAnnivDate', event.InvoiceAnnivDate);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'InvoiceFreq', event.InvoiceFrequencyCode);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'NegBranch', event.NegBranchNumber);
    }

    public toggleExistAccount(event: any): void {
        let ExistAccountNumber = this.uiForm.get('ExistAccountNumber');
        if (event.target.checked) {
            ExistAccountNumber.enable();
            this.disableForm = true;
            this.accountNameField.nativeElement.focus();

        } else {
            ExistAccountNumber.disable();
            this.disableForm = false;
            this.setControlValue('ExistAccountNumber', '');
            this.setControlValue('ExistAccountNumberDetails', '');
        }
    }

    public toggleExistContract(event: any): void {
        let ExistContractNumber = this.uiForm.get('ExistContractNumber');
        if (event.target.checked) {
            ExistContractNumber.enable();
            this.disableForm = true;
            this.contractNameField.nativeElement.focus();

        } else {
            ExistContractNumber.disable();
            this.setControlValue('ExistContractNumber', '');
            this.setControlValue('ExistContractNumberDetails', '');
        }
    }
}
