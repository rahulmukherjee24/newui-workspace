import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';

import { RenegotiationsRouteModule } from './renegotiations.route';
import { RenegsAccountDetailsComponent } from './RenegsAccountDetails/iCABSARenegsAccountDetails.component';
import { RenegsAddUpdateComponent } from './RenegsAddUpdate/iCABSARenegsAddUpdate.component';
import { RenegsContractDetailsComponent } from './RenegsContractDetails/iCABSARenegsContractDetails.component';
import { RenegsGridComponent } from './RenegsMainGrid/iCABSARenegsGrid.component';
import { RenegsPremisesGridComponent } from './RenegsPremiesesGrid/iCABSARenegsPremisesGrid.component';
import { RenegsPremisesMaintenanceComponent } from './RenegsPremiesesMaintenance/iCABSARenegsPremisesMaintenance.component';
import { RenegsRootComponent } from './iCABSARenegsRoot.component';
import { RenegsServiceCoverGridComponent } from './RenegsServiceCoverGrid/iCABSARenegsServiceCoverGrid.component';

@NgModule({
    declarations: [
        RenegsAddUpdateComponent,
        RenegsContractDetailsComponent,
        RenegsGridComponent,
        RenegsRootComponent,
        RenegsAccountDetailsComponent,
        RenegsServiceCoverGridComponent,
        RenegsPremisesMaintenanceComponent,
        RenegsPremisesGridComponent

    ],
    entryComponents: [],
    exports: [RenegsRootComponent, RenegsGridComponent],
    imports: [
        HttpClientModule,
        SharedModule,
        SearchEllipsisBusinessModule,
        RenegotiationsRouteModule

    ],
    providers: []
})
export class RenegotiationsModule { }
