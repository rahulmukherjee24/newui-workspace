import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenegsAccountDetailsComponent } from './iCABSARenegsAccountDetails.component';

describe('RenegsAccountDetailsComponent', () => {
    let component: RenegsAccountDetailsComponent;
    let fixture: ComponentFixture<RenegsAccountDetailsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RenegsAccountDetailsComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RenegsAccountDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
