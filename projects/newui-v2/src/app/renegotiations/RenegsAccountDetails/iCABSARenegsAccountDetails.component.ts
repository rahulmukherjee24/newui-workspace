import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { AfterContentInit, Component, Injector, OnInit, OnDestroy, ViewChild } from '@angular/core';

import { BaseComponent } from '../../base/BaseComponent';
import { GroupAccountNumberComponent } from '../../internal/search/iCABSSGroupAccountNumberSearch';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from '../../base/PageIdentifier';
import { RenegsAccountDetailsInterface } from './../RenegsInterfaces/iCABSARenegsAccountDetails.interface';
import { RenegsConstants } from '../iCABSARenegs.constant';
import { RenegsFormControlsInterface } from '../RenegsInterfaces/iCABSARenegsFormControls.interface';
import { RenegsXHRParamsInterface } from './../RenegsInterfaces/iCABSARenegsXHRParams.interface';

@Component({
    selector: 'icabs-renegs-account-details',
    templateUrl: 'iCABSARenegsAccountDetails.component.html'
})

export class RenegsAccountDetailsComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('accountNameField') public accountNameField;
    @ViewChild('contractNameField') public contractNameField;
    @ViewChild('contractSearch') public contractSearch;
    @ViewChild('customAlert') public customAlert;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private queryParams: any = {
        operation: 'System/iCABSSGroupAccountMaintenance',
        module: 'group-account',
        method: 'contract-management/maintenance'
    };
    private xhrParams: RenegsXHRParamsInterface = {
        module: 'account',
        method: 'contract-management/maintenance',
        operation: 'Application/iCABSARenegAccountMaintenance'
    };

    public controls: RenegsFormControlsInterface[] = [
        { name: 'AccountName', type: MntConst.eTypeText, disabled: false },
        { name: 'AccountNumber', type: MntConst.eTypeText, disabled: false },
        { name: 'AddressLine1', type: MntConst.eTypeText, disabled: false, required: true },
        { name: 'AddressLine2', type: MntConst.eTypeText, disabled: false },
        { name: 'AddressLine3', type: MntConst.eTypeText, disabled: false },
        { name: 'AmendmentRef', type: MntConst.eTypeText, disabled: false },
        { name: 'ContactName', type: MntConst.eTypeText, disabled: false, required: true },
        { name: 'County', type: MntConst.eTypeText, disabled: false },
        { name: 'Department', type: MntConst.eTypeText, disabled: false },
        { name: 'Email', type: MntConst.eTypeText, disabled: false },
        { name: 'Fax', type: MntConst.eTypeText, disabled: false },
        { name: 'GroupAccountName', type: MntConst.eTypeText, disabled: false },
        { name: 'GroupAccountNumber', type: MntConst.eTypeText, disabled: false },
        { name: 'MobileNumber', type: MntConst.eTypeText, disabled: false },
        { name: 'Position', type: MntConst.eTypeText, disabled: false, required: true },
        { name: 'PostCode', type: MntConst.eTypeText, disabled: false, required: true },
        { name: 'Telephone', type: MntConst.eTypeText, disabled: false, required: true },
        { name: 'Town', type: MntConst.eTypeText, disabled: false, required: true }
    ];
    public customAlertMsg: any;
    public groupAccountSearchComponent = GroupAccountNumberComponent;
    public inputParamsGroupAccount: any = {
        'parentMode': 'LookUp',
        'showAddNew': false,
        'countryCode': '',
        'businessCode': '',
        'showCountryCode': false,
        'showBusinessCode': false,
        'searchValue': ''
    };
    public isDisableForm: boolean = false;
    public isNewAccount: boolean = true;
    public messageType: string;
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public pageId: string;
    public pageTitle: string;
    public parentMode: string;
    public queryLookUp: QueryParams = new QueryParams();
    public renegsConstants: RenegsConstants = RenegsConstants;
    public searchModalRoute: string;
    public showHeader: boolean = true;
    public hasCloseButton: boolean = true;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARENEGACCOUNTDETAILS;
        this.browserTitle = this.pageTitle = RenegsConstants.pageTitleAccountDetails;
    }

    ngAfterContentInit(): void {
        this.changeFormFieldsStateNewAccount();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.parentMode = this.riExchange.getParentMode() || 'accountDetailsUpdate';
    }

    private changeFormFieldsStateNewAccount(): void {
        let searchupdateParams: QueryParams = this.getURLSearchParamObject();
        searchupdateParams.set(this.serviceConstants.Action, '0');
        searchupdateParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchupdateParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchupdateParams.set('advQuoteRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
        searchupdateParams.set('riHTMLPage', 'Application/iCABSARenegAccountMaintenance.htm');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, searchupdateParams).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data['hasError'] || data['errorMessage']) {
                this.notifyOnAPIError(data);
            } else {
                this.isNewAccount = (data.AccountNumber.toLowerCase().startsWith('new')) ? true : false;
                if (this.isNewAccount) {
                    this.disableControls(['AddressLine1', 'AddressLine2', 'AddressLine3', 'ContactName', 'County', 'Department', 'Email', 'Fax', 'GroupAccountName', 'GroupAccountNumber', 'MobileNumber', 'Position', 'PostCode', 'Telephone', 'Town']);
                }
                else {
                    this.disableControls([]);
                }
                this.setControlValue('AmendmentRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
                this.setControlValue('AccountNumber', data.AccountNumber);
                this.setControlValue('AccountName', data.AccountName);
                this.setControlValue('AddressLine1', data.AccountAddressLine1);
                this.setControlValue('AddressLine2', data.AccountAddressLine2);
                this.setControlValue('AddressLine3', data.AccountAddressLine3);
                this.setControlValue('ContactName', data.AccountContactName);
                this.setControlValue('County', data.AccountAddressLine5);
                this.setControlValue('Department', data.AccountContactDepartment);
                this.setControlValue('Email', data.AccountContactEmail);
                this.setControlValue('Fax', data.AccountContactFax);
                this.setControlValue('GroupAccountName', data.GroupName);
                this.setControlValue('GroupAccountNumber', data.GroupAccountNumber);
                this.setControlValue('MobileNumber', data.AccountContactMobile);
                this.setControlValue('Position', data.AccountContactPosition);
                this.setControlValue('PostCode', data.AccountPostcode);
                this.setControlValue('Telephone', data.AccountContactTelephone);
                this.setControlValue('Town', data.AccountAddressLine4);
            }
        }, (error) => {
            this.notifyOnAPIError(error);
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    private generateAPIPayloadForAccountDetails(): RenegsAccountDetailsInterface {
        let payload: RenegsAccountDetailsInterface = {
            AccountAddressLine1: this.getControlValue('AddressLine1'),
            AccountAddressLine2: this.getControlValue('AddressLine2'),
            AccountAddressLine3: this.getControlValue('AddressLine3'),
            AccountAddressLine4: this.getControlValue('AddressLine4'),
            AccountAddressLine5: this.getControlValue('AddressLine5'),
            AccountContactDepartment: this.getControlValue('Department'),
            AccountContactEmail: this.getControlValue('Email'),
            AccountContactFax: this.getControlValue('Fax'),
            AccountContactMobile: this.getControlValue('MobileNumber'),
            AccountContactName: this.getControlValue('ContactName'),
            AccountContactPosition: this.getControlValue('Position'),
            AccountContactTelephone: this.getControlValue('Telephone'),
            AccountName: this.getControlValue('AccountName'),
            AccountNumber: this.getControlValue('AccountNumber'),
            AccountPostcode: this.getControlValue('PostCode'),
            advQuoteRef: this.getControlValue('AmendmentRef'),
            BusinessCode: this.businessCode(),
            groupaccountnumber: this.getControlValue('GroupAccountNumber'),
            UpdateableInd: ''
        };
        return payload;
    }

    private notifyOnAPIError(error: any): void {
        this.messageType = ((error['hasError'] || error['errorMessage'])) ? RenegsConstants.msgTypeError : RenegsConstants.msgTypeWarn;
        this.customAlertMsg = {
            msg: error.errorMessage + ' ' + error.fullError,
            timestamp: (new Date()).getMilliseconds()
        };
    }

    public onUpdateCompletion(): void {
        this.location.back();
    }

    public onUpdateAccountDetails(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchParams.set('advQuoteRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
        searchParams.set('riHTMLPage', 'Application/iCABSARenegContractMaintenance.htm');
        let payload: RenegsAccountDetailsInterface = this.generateAPIPayloadForAccountDetails();
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams['method'], this.xhrParams['module'], this.xhrParams['operation'], searchParams, payload)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response.hasError || response.errorMessage || response.fullError) {
                    this.notifyOnAPIError(response);
                } else {
                    //success
                    this.onUpdateCompletion();
                }
            },
                (error) => {
                    this.notifyOnAPIError(error);
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    public lookUpRecord(data: Object, maxresults: number): any {
        this.queryLookUp.set(this.serviceConstants.Action, '0');
        this.queryLookUp.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.queryLookUp.set(this.serviceConstants.CountryCode, this.countryCode());
        if (maxresults) {
            this.queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        }
        return this.httpService.lookUpRequest(this.queryLookUp, data);
    }

    public onGroupAccountDataReceived(event: any, data: any): void {
        if (data) {
            this.setControlValue('GroupAccountNumber', data.GroupAccountNumber);
            this.setControlValue('GroupAccountName', data.GroupName);
        } else {
            this.setControlValue('GroupAccountNumber', '');
            this.setControlValue('GroupAccountName', '');
        }
    }

    public onGroupAccountBlur(event: any): void {
        if (this.getControlValue('GroupAccountNumber')) {
            let data = [{
                'table': 'GroupAccount',
                'query': { 'GroupAccountNumber': this.uiForm.controls['GroupAccountNumber'].value },
                'fields': ['GroupAccountNumber', 'GroupName']
            }];
            this.ajaxSource.next(this.ajaxconstant.START);
            this.lookUpRecord(data, 5).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e['results'] && e['results'].length > 0) {
                        if (e['results'][0].length > 0) {
                            this.setControlValue('GroupAccountName', e['results'][0][0]['GroupName']);
                        } else {
                            this.setControlValue('GroupAccountName', '');
                        }
                    } else {
                        this.setControlValue('GroupAccountName', '');
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('GroupAccountName', '');
                }
            );
        } else {
            this.setControlValue('GroupAccountName', '');
        }
    }
}
