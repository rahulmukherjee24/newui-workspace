import { AfterContentInit, Component, Injector, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../base/BaseComponent';
import { ContractDurationComponent } from './../../internal/search/iCABSBContractDurationSearch';
import { IGenericEllipsisControl,EllipsisGenericComponent } from '@shared/components/ellipsis-generic/ellipsis-generic';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from '../../base/PageIdentifier';
import { RenegsAddNewContractRenegInterface } from '../RenegsInterfaces/iCABSARenegsAddNewContractReneg.interface';
import { RenegsConstants } from '../iCABSARenegs.constant';
import { RenegsFormControlsInterface } from '../RenegsInterfaces/iCABSARenegsFormControls.interface';
import { RenegsXHRParamsInterface } from '../RenegsInterfaces/iCABSARenegsXHRParams.interface';
@Component({
    selector: 'icabs-renegs-contact-details',
    templateUrl: 'iCABSARenegsContractDetails.component.html'
})

export class RenegsContractDetailsComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('accountNameField') public accountNameField;
    @ViewChild('contractNameField') public contractNameField;
    @ViewChild('InvoiceFreqSearch') public invoiceFreqSearch: EllipsisGenericComponent;
    @ViewChild('PaymentMethodSearch') public paymentMethodSearch: EllipsisGenericComponent;
    @ViewChild('MinDurationSearch') public minDurationSearch: ContractDurationComponent;
    @ViewChild('customAlert') public customAlert;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private xhrParams: RenegsXHRParamsInterface = {
        module: 'contract',
        method: 'contract-management/maintenance',
        operation: 'Application/iCABSARenegContractMaintenance'
    };

    public controls: RenegsFormControlsInterface[] = [
        { name: 'AgreementNo', type: MntConst.eTypeText },
        { name: 'AmendmentRef', type: MntConst.eTypeCode },
        { name: 'CompanyRegNo', type: MntConst.eTypeText },
        { name: 'ContinousContract' },
        { name: 'ContactName', type: MntConst.eTypeText, required: true },
        { name: 'ContractNumber', type: MntConst.eTypeText },
        { name: 'Department', type: MntConst.eTypeText },
        { name: 'Email', type: MntConst.eTypeText },
        { name: 'Fax', type: MntConst.eTypeCode },
        { name: 'InvoiceFreq', type: MntConst.eTypeCode, required: true },
        { name: 'InvoiceFreqDetails', type: MntConst.eTypeText, disabled: true },
        { name: 'LandLine', type: MntConst.eTypeText, required: true },
        { name: 'MinDuration', type: MntConst.eTypeText },
        { name: 'MobileNo', type: MntConst.eTypeText },
        { name: 'Name', type: MntConst.eTypeText, required: true },
        { name: 'PaymentMethod', type: MntConst.eTypeCode, required: true },
        { name: 'PaymentMethodDetails', type: MntConst.eTypeText, disabled: true },
        { name: 'Position', type: MntConst.eTypeDate, required: true },
        { name: 'RenewalAgreementNo', type: MntConst.eTypeText },
        { name: 'TaxRegNo', type: MntConst.eTypeCode },
        { name: 'TaxRegNo2', type: MntConst.eTypeCode }
    ];
    public customAlertMsg: any;
    public isNewContract: boolean = true;
    public isCompanyRegistration: boolean = false;
    public contractDurationComponent = ContractDurationComponent;
    public enableAutoOpen: boolean = true;
    public inputParamsInvoiceFrequency: any = { 'parentMode': 'LookUp', 'countryCode': this.utils.getCountryCode(), 'businessCode': this.utils.getBusinessCode() };
    public paymentTypeSearchConfig: IGenericEllipsisControl = {
        ellipsisTitle: 'Payment Type Search',
        configParams: {
            table: '',
            shouldShowAdd: false,
            parentMode: 'LookUp',
            rowmetadata: [
                { name: 'ReferenceRequired', type: 'img' },
                { name: 'MandateRequired', type: 'img' },
                { name: 'InvoiceSuspendInd', type: 'img' },
                { name: 'DefaultInd', type: 'img' }
            ],
            extraParams: {
                'search.sortby': 'PaymentTypeCode'
            }
        },
        httpConfig: {
            operation: 'Business/iCABSBPaymentTypeSearch',
            module: 'payment',
            method: 'bill-to-cash/search'
        },
        tableColumn: [
            { title: 'Code', name: 'PaymentTypeCode' },
            { title: 'Description', name: 'PaymentDesc' },
            { title: 'Reference Required', name: 'ReferenceRequired' },
            { title: 'Mandate Required', name: 'MandateRequired' },
            { title: 'Suspend Invoice', name: 'InvoiceSuspendInd' },
            { title: 'Default', name: 'DefaultInd' }
        ],
        disable: false
    };
    public inputParamsInvoiceFrequencyConfig: IGenericEllipsisControl = {
        autoOpen: true,
        ellipsisTitle: 'Invoice Frequency Search',
        configParams: {
            table: '',
            shouldShowAdd: false,
            parentMode: 'LookUp',
            rowmetadata: [
                { name: 'InvoiceFrequencyDefaultInd', type: 'img' },
                { name: 'InvoiceChargeInd', type: 'img' }
            ]
        },
        httpConfig: {
            operation: 'Business/iCABSBBusinessInvoiceFrequencySearch',
            module: 'invoicing',
            method: 'bill-to-cash/search'
        },
        tableColumn: [
            { title: 'Code', name: 'InvoiceFrequencyCode' },
            { title: 'Description', name: 'InvoiceFrequencyDesc' },
            { title: 'Business Default', name: 'InvoiceFrequencyDefaultInd' },
            { title: 'Invoice Charge', name: 'InvoiceChargeInd' }
        ],
        disable: false
    };
    public messageType: string;
    public pageId: string;
    public pageTitle: string;
    public renegsConstants: RenegsConstants = RenegsConstants;
    public showHeader: boolean = true;
    public showCloseButton: boolean = true;
    public ellipsisQueryParams: Object = {
        paymentTerm: {
            parentMode: 'Lookup'
        },
        inputParamsPaymentTermSearch: {
            parentMode: 'LookUp-Active'
        }
    };
    public c_o_ELLIPSIS_MODAL_CONFIG = {
        backdrop: 'static',
        keyboard: true
    };
    public contractDurationDefault: any = {
        id: '',
        text: ''
    };
    public contractDuration = {  //ITa-343
        parentMode: 'LookUp-ContractMinDuration',
        businessCode: this.businessCode(),
        countryCode: this.countryCode()
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARENEGCONTRACTDETAILS;
        this.browserTitle = this.pageTitle = RenegsConstants.pageTitleContractDetails;
    }

    ngAfterContentInit(): void {
        this.changeFormFieldsStateInUpdateMode();

    }

    ngOnInit(): void {
        super.ngOnInit();
        this.setupPageParams();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private changeFormFieldsStateInUpdateMode(): void {
        let searchupdateParams: QueryParams = this.getURLSearchParamObject();
        searchupdateParams.set(this.serviceConstants.Action, '0');
        searchupdateParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchupdateParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchupdateParams.set('advQuoteRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
        searchupdateParams.set('riHTMLPage', 'Application/iCABSARenegContractMaintenance.htm');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, searchupdateParams).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data['hasError'] || data['errorMessage']) {
                this.notifyOnAPIError(data);
            } else {
                let continuousInd: boolean = (data.ContinuousInd === 'yes') ? true : false;
                this.isNewContract = (data.ContractNumber.toLowerCase().startsWith('new') ) ? true : false;
                if (this.isNewContract) {
                    this.disableControls(['AgreementNo', 'CompanyRegNo', 'ContinousContract', 'ContactName', 'Department', 'Email', 'Fax', 'InvoiceFreq', 'InvoiceFreqDetails', 'LandLine', 'MinDuration', 'MinDurationDetails', 'MobileNo', 'Name', 'PaymentMethod', 'PaymentMethodDetails', 'Position', 'RenewalAgreementNo', 'TaxRegNo', 'TaxRegNo2']);
                }
                else {
                    this.disableControls([]);
                }

                this.fetchSysChar(880).subscribe((data) => {
                    this.isCompanyRegistration = (data.records[0].Required) ? true : false;
                });

                this.setControlValue('AgreementNo', data.AgreementNumber);
                this.setControlValue('AmendmentRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
                this.setControlValue('CompanyRegNo', data.CompanyRegistrationNumber);
                this.setControlValue('ContactName', data.ContractContactName);
                this.setControlValue('ContinousContract', continuousInd);
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('Department', data.ContractContactDepartment);
                this.setControlValue('Email', data.ContractContactEmail);
                this.setControlValue('Fax', data.ContractContactFax);
                this.setControlValue('InvoiceFreq', data.InvoiceFrequencyCode);
                //lookup call for InvoiceFreqDetails
                if (data.InvoiceFrequencyCode.length) { this.fetchLookupFieldDetails('invoice', data.InvoiceFrequencyCode); }

                this.setControlValue('InvoiceFreqDetails', data.InvoiceFrequencyCode);
                this.setControlValue('LandLine', data.ContractContactTelephone);
                this.minDurationSearch.setCustomActiveLocal(data.MinimumDurationCode);

                this.setControlValue('MobileNo', data.ContractContactMobile);
                this.setControlValue('Name', data.ContractName);
                this.setControlValue('PaymentMethod', data.PaymentTypeCode);
                //lookup call for PaymentMethodDetails
                if (data.PaymentTypeCode.length) { this.fetchLookupFieldDetails('payment', data.PaymentTypeCode); }

                this.setControlValue('PaymentMethodDetails', data.PaymentTermCode);
                this.setControlValue('Position', data.ContractContactPosition);
                this.setControlValue('RenewalAgreementNo', data.RenewalAgreementNumber);
                this.setControlValue('TaxRegNo', data.CompanyVATNumber);
                this.setControlValue('TaxRegNo2', data.CompanyVATNumber2);
            }
        }, (error) => {
            this.notifyOnAPIError(error);
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    private fetchSysChar(sysCharNumber: any): any {
        let querySysChar: QueryParams = this.getURLSearchParamObject();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.SystemCharNumber, sysCharNumber);
        return this.httpService.sysCharRequest(querySysChar);
    }

    public fetchLookupFieldDetails(onField: any, e: any): void {
        const numValue: string = (e.target) ? (e.target.value).toString() : e.toString();
        let fields: string[];
        let tableName: string;
        let query: any = {};
        if (onField.toLowerCase() === 'invoice') {
            fields = ['InvoiceFrequencyCode', 'InvoiceFrequencyDesc'];
            tableName = 'SystemInvoiceFrequency';
            query['InvoiceFrequencyCode'] = numValue;

        } else if (onField.toLowerCase() === 'payment') {
            fields = ['PaymentTypeCode', 'PaymentDesc'];
            tableName = 'PaymentType';
            query['PaymentTypeCode'] = numValue;
        }

        if (!numValue || !numValue.length) {
            return;
        }

        let lookupData = [{
            'table': tableName,
            'query': query,
            'fields': fields
        }];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.lookUpRecord(lookupData)
            .subscribe((data: any) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data && data['results']) {
                    if (data['results'][0].length) {
                        (onField) === 'invoice' ? this.setControlValue('InvoiceFreqDetails', data['results'][0][0]['InvoiceFrequencyDesc']) : this.setControlValue('PaymentMethodDetails', data['results'][0][0]['PaymentDesc']);
                    } else {
                        (onField) === 'invoice' ? this.resetFormField(['InvoiceFreq', 'InvoiceFreqDetails']) : this.resetFormField(['PaymentMethod', 'PaymentMethodDetails']);
                    }

                } else {
                    (onField) === 'invoice' ? this.resetFormField(['InvoiceFreq', 'InvoiceFreqDetails']) : this.resetFormField(['PaymentMethod', 'PaymentMethodDetails']);
                    this.notifyOnAPIError(data);
                }
            }, (error: any) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.notifyOnAPIError(error);
            });
    }

    private generateAPIPayloadForAddNewContract(): RenegsAddNewContractRenegInterface {
        let getContinuousIndValue = this.getControlValue('UseExistAccount');
        let payload: RenegsAddNewContractRenegInterface = {
            advQuoteRef: this.getControlValue('AmendmentRef'),
            AgreementNumber: this.getControlValue('AgreementNo'),
            BusinessCode: this.businessCode(),
            CompanyRegistrationNumber: this.getControlValue('CompanyRegNo'),
            CompanyVATNumber: this.getControlValue('TaxRegNo'),
            CompanyVATNumber2: this.getControlValue('TaxRegNo2'),
            ContinuousInd: this.getControlValue('ContinousContract') ? 'yes' : 'no',
            ContractContactDepartment: this.getControlValue('Department'),
            ContractContactEmail: this.getControlValue('Email'),
            ContractContactFax: this.getControlValue('Fax'),
            ContractContactMobile: this.getControlValue('MobileNo'),
            ContractContactName: this.getControlValue('ContactName'),
            ContractContactPosition: this.getControlValue('Position'),
            ContractContactTelephone: this.getControlValue('LandLine'),
            ContractDurationCode: '',
            ContractExpiryDate: '',
            ContractName: this.getControlValue('Name'),
            ContractNumber: this.getControlValue('ContractNumber'),
            InvoiceFrequencyCode: this.getControlValue('InvoiceFreq'),
            MinimumDurationCode: this.getControlValue('MinDuration'),
            MinimumExpiryDate: this.getControlValue('MinDurationDetails'),
            PaymentTypeCode: this.getControlValue('PaymentMethod'),
            RenewalAgreementNumber: this.getControlValue('RenewalAgreementNo'),
            UpdateableInd: ''
        };
        return payload;
    }

    private lookUpRecord(data: any): any {
        let queryLookUp: QueryParams = new QueryParams();
        queryLookUp.set(this.serviceConstants.Action, '0');
        queryLookUp.set(this.serviceConstants.BusinessCode, this.businessCode());
        queryLookUp.set(this.serviceConstants.CountryCode, this.countryCode());
        queryLookUp.set(this.serviceConstants.MaxResults, '1');
        return this.httpService.lookUpRequest(queryLookUp, data);
    }

    private notifyOnAPIError(error: any): void {
        this.messageType = ((error['hasError'] || error['errorMessage'])) ? RenegsConstants.msgTypeError : RenegsConstants.msgTypeWarn;
        this.customAlertMsg = {
            msg: error.errorMessage + ' ' + error.fullError,
            timestamp: (new Date()).getMilliseconds()
        };
    }

    private resetFormField(fields: any[]): void {
        fields.forEach((field) => {
            this.setControlValue(field, '');
        });
    }

    private setupPageParams(): void {
        this.pageParams.actionOnAddNew = '1';
        this.pageParams.actionOnUpdate = '2';
        this.pageParams.BusinessCode = this.businessCode();
        this.pageParams.CountryCode = this.countryCode();
    }

    public onAddNewContractReneg(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchParams.set('advQuoteRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
        searchParams.set('riHTMLPage', 'Application/iCABSARenegContractMaintenance.htm');
        let payload: RenegsAddNewContractRenegInterface = this.generateAPIPayloadForAddNewContract();

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams['method'], this.xhrParams['module'], this.xhrParams['operation'], searchParams, payload)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response.hasError || response.errorMessage || response.fullError) {
                    this.notifyOnAPIError(response);
                } else {
                    //success
                    this.location.back();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.notifyOnAPIError(error);
            });
    }

    public onInvoiceFrequencyDataReceived(data: any): void {
        if (data) {
            this.setControlValue('InvoiceFreqDetails', data['InvoiceFrequencyDesc']);
            this.setControlValue('InvoiceFreq', data['InvoiceFrequencyCode']);
        }
    }
    public onPaymentTermReceived(data: any): void {
        if (data) {
            this.setControlValue('PaymentMethodDetails', data['PaymentDesc']);
            this.setControlValue('PaymentMethod', data['PaymentTypeCode']);
        }
    }
    public onReceivedContractDuration(data: any): void {
        if (data) {
            this.setControlValue('MinDuration', data['MinimumDurationCode']);
        }
    }

    public onFetchDetails(fieldName: string, evt: any): void {  // no in use
        let queryParams: QueryParams = new QueryParams();
        let data: any;
        let xhrParams: RenegsXHRParamsInterface = {
            module: 'contract',
            method: 'contract-management/maintenance',
            operation: 'Application/iCABSARenegContractMaintenance'
        };

        if (fieldName === 'invoice') {
            data = [{
                'table': 'SystemInvoiceFrequency',
                'query': { 'InvoiceFrequencyCode': (evt.target) ? (evt.target.value).toString() : evt.toString() },
                'fields': ['InvoiceFrequencyCode', 'InvoiceFrequencyDesc']
            }];
            queryParams.set('action', '0');
            xhrParams = {
                module: 'invoicing',
                method: 'bill-to-cash/search',
                operation: ' Business/iCABSBBusinessInvoiceFrequencySearch'
            };

        } else if (fieldName === 'payment') {
            data = [{
                'table': 'SystemInvoiceFrequency',
                'query': { 'InvoiceFrequencyCode': (evt.target) ? (evt.target.value).toString() : evt.toString() },
                'fields': ['InvoiceFrequencyCode', 'InvoiceFrequencyDesc']
            }];
            queryParams.set('action', '2');
            xhrParams = {
                module: 'payment',
                method: 'bill-to-cash/search',
                operation: '  Business/iCABSBPaymentTypeSearch'
            };

        } else {
            data = [{
                'table': 'SystemInvoiceFrequency',
                'query': { 'InvoiceFrequencyCode': (evt.target) ? (evt.target.value).toString() : evt.toString() },
                'fields': ['InvoiceFrequencyCode', 'InvoiceFrequencyDesc']
            }];
            queryParams.set(this.serviceConstants.ContractNumber, this.getControlValue('ContractNumber'));
            queryParams.set('ContractTypeCode', this.riExchange.getCurrentContractType());
        }

        queryParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        queryParams.set(this.serviceConstants.CountryCode, this.countryCode());
        queryParams.set('ContractTypeCode', this.riExchange.getCurrentContractType());
        queryParams.set(this.serviceConstants.PageSize, '1');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(xhrParams.method, xhrParams.module, xhrParams.operation, queryParams)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response['hasError'] || response['errorMessage']) {
                    this.notifyOnAPIError({ errorMessage: (response['errorMessage'] || RenegsConstants.NoRecord) });
                    this.uiForm.reset();

                } else {
                    if (fieldName === 'invoice') {
                        this.setControlValue('InvoiceFreqDetails', response.records[0]['SystemInvoiceFrequency.InvoiceFrequencyDesc']);

                    } else if (fieldName === 'payment') {
                        this.setControlValue('PaymentMethodDetails', response.records[0]['PaymentDesc']);

                    }

                }
            }, (error) => {
                // error statement
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.notifyOnAPIError(error);
            });
    }

}
