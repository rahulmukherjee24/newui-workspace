import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenegsContractDetailsComponent } from './iCABSARenegsContractDetails.component';

describe('RenegsAddUpdateComponent', () => {
    let component: RenegsContractDetailsComponent;
    let fixture: ComponentFixture<RenegsContractDetailsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RenegsContractDetailsComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RenegsContractDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
