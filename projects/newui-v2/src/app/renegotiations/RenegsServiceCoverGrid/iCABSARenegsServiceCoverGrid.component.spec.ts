import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenegsServiceCoverGridComponent } from './iCABSARenegsServiceCoverGrid.component';

describe('RenegsAccountDetailsComponent', () => {
    let component: RenegsServiceCoverGridComponent;
    let fixture: ComponentFixture<RenegsServiceCoverGridComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RenegsServiceCoverGridComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RenegsServiceCoverGridComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
