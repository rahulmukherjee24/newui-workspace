import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteAwayGuardService } from './../../shared/services/route-away-guard.service';
import { HttpClientModule } from '@angular/common/http';

import { RenegotiationRoutes } from '../base/PageRoutes';
import { RenegsAccountDetailsComponent } from './RenegsAccountDetails/iCABSARenegsAccountDetails.component';
import { RenegsAddUpdateComponent } from './RenegsAddUpdate/iCABSARenegsAddUpdate.component';
import { RenegsContractDetailsComponent } from './RenegsContractDetails/iCABSARenegsContractDetails.component';
import { RenegsGridComponent } from './RenegsMainGrid/iCABSARenegsGrid.component';
import { RenegsPremisesGridComponent } from './RenegsPremiesesGrid/iCABSARenegsPremisesGrid.component';
import { RenegsPremisesMaintenanceComponent } from './RenegsPremiesesMaintenance/iCABSARenegsPremisesMaintenance.component';
import { RenegsRootComponent } from './iCABSARenegsRoot.component';
import { RenegsServiceCoverGridComponent } from './RenegsServiceCoverGrid/iCABSARenegsServiceCoverGrid.component';

const routes: Routes = [
    {
        path: '', component: RenegsRootComponent, children: [
            { path: RenegotiationRoutes.ICABSARENEGSACCOUNTDETAILS, component: RenegsAccountDetailsComponent, canDeactivate: [RouteAwayGuardService] },
            { path: RenegotiationRoutes.ICABSARENEGSADDUPDATE, component: RenegsAddUpdateComponent, canDeactivate: [RouteAwayGuardService] },
            { path: RenegotiationRoutes.ICABSARENEGSCONTRACTDETAILS, component: RenegsContractDetailsComponent, canDeactivate: [RouteAwayGuardService] },
            { path: RenegotiationRoutes.ICABSARENEGSGRID, component: RenegsGridComponent },
            { path: RenegotiationRoutes.ICABSARENEGSPREMISESGRID, component: RenegsPremisesGridComponent, canDeactivate: [RouteAwayGuardService] },
            { path: RenegotiationRoutes.ICABSARENEGSPREMISESMAINTENANCE, component: RenegsPremisesMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: RenegotiationRoutes.ICABSARENEGSPREMISESMAINTENANCENEW, component: RenegsPremisesMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: RenegotiationRoutes.ICABSARENEGSSERVICECOVERGRID, component: RenegsServiceCoverGridComponent, canDeactivate: [RouteAwayGuardService] }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RenegotiationsRouteModule { }
