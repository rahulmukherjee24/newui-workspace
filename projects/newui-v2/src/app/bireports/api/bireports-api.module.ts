import { ContractsApiExemptGridComponent } from './iCABSAContractsAPIExemptGrid.component';

import { HttpClientModule } from '@angular/common/http';
import { Component, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BIReportsRoutesConstant } from '@base/PageRoutes';
import { SharedModule } from '@shared/shared.module';
import { SearchEllipsisDropdownModule } from '../../internal/search-ellipsis-dropdown.module';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class ApiBIReportsRootComponent {
    constructor() {
    }
}

@NgModule({
    exports: [
    ],
    imports: [
        HttpClientModule,
        SharedModule,
        SearchEllipsisDropdownModule,
        RouterModule.forChild([
            {
                path: '', component: ApiBIReportsRootComponent, children: [
                    { path: BIReportsRoutesConstant.iCABSACONTACTSAPIEXEMPTGRID, component: ContractsApiExemptGridComponent }
                ], data: { domain: 'BI REPORTS API' }
            }

        ])
    ],
    declarations: [
        ApiBIReportsRootComponent,
        ContractsApiExemptGridComponent
    ],
    providers: [

    ]

})

export class ApiBIReportsModule { }
