import { Component, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { BIReportsRoutesConstant } from '@base/PageRoutes';
import { SharedModule } from '@shared/shared.module';
import { ICABSSeStockUsageEstimatesBranchComponent } from './iCABSSeStockUsageEstimatesBranch.component';
import { ICABSSeStockUsageEstimatesComponent } from './iCABSSeStockUsageEstimates.component';
import { ICABSSeStockUsageEstimatesBranchDetailComponent } from './iCABSSeStockUsageEstimatesBranchDetail.component';

@Component({
    template: `<router-outlet></router-outlet>
    `
})
export class StockUsageRootComponent {
    constructor() {

    }
}

@NgModule({
    imports: [
        InternalSearchEllipsisModule,
        InternalSearchModule,
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        SharedModule,
        HttpClientModule,
        RouterModule.forChild([
            {
                path: '', component: StockUsageRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESBRANCH, component: ICABSSeStockUsageEstimatesBranchComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESDETAIL, component: ICABSSeStockUsageEstimatesBranchComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESBUSINESS, component: ICABSSeStockUsageEstimatesComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESREGION, component: ICABSSeStockUsageEstimatesComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESPREMISES, component: ICABSSeStockUsageEstimatesComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESBRANCHDETAIL, component: ICABSSeStockUsageEstimatesBranchDetailComponent }
                ], data: { domain: 'STOCKUSAGEESTIMATES' }
            }

        ])
    ],
    declarations: [
        StockUsageRootComponent,
        ICABSSeStockUsageEstimatesBranchComponent,
        ICABSSeStockUsageEstimatesComponent,
        ICABSSeStockUsageEstimatesBranchDetailComponent
    ]

})

export class StockUsageModule {
}
