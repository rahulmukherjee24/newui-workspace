import { Component, OnInit, AfterContentInit, Injector, ViewChild } from '@angular/core';

import { BIReportsRoutes, ContractManagementModuleRoutes } from './../../base/PageRoutes';
import { CommonGridFunction } from './../../base/CommonGridFunction';
import { CommonLookUpUtilsService } from './../../../shared/services/commonLookupUtils.service';
import { ContractSearchComponent } from './../../internal/search/iCABSAContractSearch';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSSeStockUsageEstimatesBranch.html',
    providers: [CommonLookUpUtilsService]
})

export class ICABSSeStockUsageEstimatesBranchComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public commonGridFunction: CommonGridFunction;
    public environmentalUsages: Array<any>;
    public hasGridData: boolean = false;
    public pageId: string;
    public pageType: string;
    public controls = [
        { name: 'BranchName', readonly: true, disabled: true, type: MntConst.eTypeText, value: this.utils.getBranchTextOnly() },
        { name: 'BranchNumber', readonly: true, disabled: true, type: MntConst.eTypeText, value: this.utils.getBranchCode() },
        { name: 'ContractName', readonly: true, disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', type: MntConst.eTypeText },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EnvironmentMetricInd', type: MntConst.eTypeCheckBox },
        { name: 'MeasuredIn', type: MntConst.eTypeText },
        { name: 'PrepCode', type: MntConst.eTypeCode },
        { name: 'PrepDesc', type: MntConst.eTypeText },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'SelEnvironmentUsage' }
    ];
    public ellipsis: Record<string, Record<string, Object>> = {
        contract: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: ContractSearchComponent
        }
    };

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    public ngOnInit(): void {
        super.ngOnInit();

    }

    public ngAfterContentInit(): void {
        this.pageType = this.riExchange.getParentHTMLValue('pageType') || 'Detail';
        this.pageId = this.pageType === 'Branch' ? PageIdentifier.ICABSSESTOCKUSAGEESTIMATESBRANCH : PageIdentifier.ICABSSESTOCKUSAGEESTIMATESDETAIL;
        super.ngAfterContentInit();
        this.onWindowLoad();
    }

    private onWindowLoad(): void {
        this.pageTitle = this.browserTitle = 'Stock Usage Estimates ' + this.pageType;
        this.pageParams.gridHandle = this.utils.randomSixDigitString();
        this.buildGrid();
        if (this.pageType === 'Detail') {
            this.disableControls(['MeasuredIn']);
        }
        if (this.isReturning()) {
            this.onRiGridRefresh();
            this.commonLookup.getEnvironmentalUsageDetails().then(data => {
                if (data[0].length > 0) {
                    this.environmentalUsages = data[0];
                    this.setControlValue('SelEnvironmentUsage', this.environmentalUsages[0].EnvironmentCode);
                }
            });
        } else {
            this.pageParams.gridConfig = {
                itemsPerPage: 10,
                totalItem: 1
            };
            if (this.pageType === 'Branch') {
                this.commonLookup.setTradingDates(this.uiForm, 'DateFrom', 'DateTo');
                this.commonLookup.getEnvironmentalUsageDetails().then(data => {
                    if (data[0].length > 0) {
                        this.environmentalUsages = data[0];
                        this.setControlValue('SelEnvironmentUsage', this.environmentalUsages[0].EnvironmentCode);
                    }
                });
            } else {
                this.setControlValue('PrepCode', this.riExchange.getParentHTMLValue('PrepCode'));
                this.setControlValue('PrepDesc', this.riExchange.getParentHTMLValue('PrepDesc'));
                this.setControlValue('MeasuredIn', this.riExchange.getParentHTMLValue('MeasuredIn'));
                this.setControlValue('DateFrom', this.riExchange.getParentHTMLValue('DateFrom'));
                this.setControlValue('DateTo', this.riExchange.getParentHTMLValue('DateTo'));
                if (this.parentMode === 'Region') {
                    this.setControlValue('RegionCode', this.riExchange.getParentHTMLValue('RegionCode'));
                    this.setControlValue('RegionDesc', this.riExchange.getParentHTMLValue('RegionDesc'));
                }
                this.onRiGridRefresh();
            }
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        if (this.pageType === 'Branch') {
            this.riGrid.AddColumn('PrepCode', 'Usage', 'PrepCode', MntConst.eTypeCode, 3, true);
            this.riGrid.AddColumnAlign('PrepCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('PrepDesc', 'Usage', 'PrepDesc', MntConst.eTypeText, 30);
            this.riGrid.AddColumn('NumberOfPremises', 'Usage', 'NumberOfPremises', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('Quantity', 'Usage', 'Quantity', MntConst.eTypeDecimal2, 5);
            this.riGrid.AddColumn('MeasuredIn', 'Usage', 'MeasuredIn', MntConst.eTypeText, 5);
            this.riGrid.AddColumn('Value', 'Usage', 'Value', MntConst.eTypeDecimal2, 5);
        } else if (this.riExchange.getParentHTMLValue('parentMode') === 'Contract') {
            this.riGrid.AddColumn('ContractNumber', 'Usage', 'ContractNumber', MntConst.eTypeCode, 10, true);
            this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ContractName', 'Usage', 'ContractName', MntConst.eTypeText, 40);
            this.riGrid.AddColumn('PremiseNumber', 'Usage', 'PremiseNumber', MntConst.eTypeText, 10, true);
            this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ProductCode', 'Usage', 'ProductCode', MntConst.eTypeText, 5);
            this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ServiceDateStart', 'Usage', 'ServiceDateStart', MntConst.eTypeDate, 5);
            this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('gridEmployeeCode', 'Usage', 'gridEmployeeCode', MntConst.eTypeText, 6);
            this.riGrid.AddColumnAlign('gridEmployeeCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('NumberOfPremises', 'Usage', 'NumberOfPremises', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('PrepVolume', 'Usage', 'PrepVolume', MntConst.eTypeDecimal2, 5);
            this.riGrid.AddColumn('PrepValue', 'Usage', 'PrepValue', MntConst.eTypeDecimal2, 5);
        } else {
            this.riGrid.AddColumn('BranchNumber', 'Usage', 'BranchNumber', MntConst.eTypeCode, 10, true);
            this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('BranchName', 'Usage', 'BranchName', MntConst.eTypeText, 40);
            this.riGrid.AddColumn('NumberOfPremises', 'Usage', 'NumberOfPremises', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('PrepVolume', 'Usage', 'PrepVolume', MntConst.eTypeDecimal2, 5);
            this.riGrid.AddColumn('PrepValue', 'Usage', 'PrepValue', MntConst.eTypeDecimal2, 5);
        }

        if (this.getControlValue('EnvironmentMetricInd')) {
            this.riGrid.AddColumn('WeightUsage', 'Usage', 'WeightUsage', MntConst.eTypeDecimal2, 5);
        }
        this.riGrid.Complete();
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    public onValueChange(event: any): void {
        this['commonGridFunction'].resetGrid();
        this.setControlValue('ContractNumber', event.ContractNumber);
        this.setControlValue('ContractName', event.ContractName);
    }

    public onGridBodyDoubleClick(): void {
        let contractType: string = this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty');
        let contractNumber: string = this.riGrid.Details.GetValue('ContractNumber');
        switch (this.riGrid.CurrentColumnName) {
            case 'PrepCode':
                this.navigate('Contract', BIReportsRoutes.ICABSSESTOCKUSAGEESTIMATESDETAIL,
                    {
                        'PrepCode': this.riGrid.Details.GetValue('PrepCode'),
                        'PrepDesc': this.riGrid.Details.GetValue('PrepDesc'),
                        'MeasuredIn': this.riGrid.Details.GetValue('MeasuredIn'),
                        'DateFrom': this.getControlValue('DateFrom'),
                        'DateTo': this.getControlValue('DateTo'),
                        'ContractNumber': this.getControlValue('ContractNumber'),
                        'ViewBy': this.pageType
                    });
                break;
            case 'ContractNumber':
                switch (contractType) {
                    case 'C':
                        this.navigate('StockUsageSearch', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                            'CurrentContractTypeURLParameter': contractType,
                            'ContractNumber': contractNumber
                        });
                        break;
                    case 'J':
                        this.navigate('StockUsageSearch', ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE, {
                            'CurrentContractTypeURLParameter': contractType,
                            'ContractNumber': contractNumber
                        });
                        break;
                    case 'P':
                        this.navigate('StockUsageSearch', ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE, {
                            'CurrentContractTypeURLParameter': contractType,
                            'ContractNumber': contractNumber
                        });
                        break;
                }
                break;
            case 'PremiseNumber':
                this.navigate('StockEstimatesSearch', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    'CurrentContractTypeURLParameter': contractType,
                    'ContractNumber': contractNumber,
                    'PremiseNumber': this.riGrid.Details.GetValue('PremiseNumber')
                });
                break;

            case 'BranchNumber':
                this.navigate('Contract', BIReportsRoutes.ICABSSESTOCKUSAGEESTIMATESBRANCHDETAIL,
                    {
                        'PrepCode': this.getControlValue('PrepCode'),
                        'PrepDesc': this.getControlValue('PrepDesc'),
                        'BranchNumber': this.riGrid.Details.GetValue('BranchNumber'),
                        'BranchName': this.riGrid.Details.GetValue('BranchName')
                    });
                break;
        }
    }

    public populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            let formData: Object = {};

            formData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
            formData['Level'] = this.pageType;
            formData['BranchNumber'] = this.getControlValue('BranchNumber');
            formData['DateFrom'] = this.getControlValue('DateFrom');
            formData['DateTo'] = this.getControlValue('DateTo');
            if (this.pageType === 'Branch') {
                formData['ContractNumber'] = this.getControlValue('ContractNumber');
                formData['SelEnvironmentUsage'] = this.getControlValue('SelEnvironmentUsage');
                formData['EnvironmentMetricInd'] = this.getControlValue('EnvironmentMetricInd') ? 'True' : 'False';
            } else {
                formData['ViewBy'] = this.riExchange.getParentHTMLValue('ViewBy');
                formData['EmployeeCode'] = this.riExchange.getParentHTMLValue('EmployeeCode');
                formData['PrepCode'] = this.riExchange.getParentHTMLValue('PrepCode');
                formData['RegionCode'] = this.getControlValue('RegionCode');
                formData[this.serviceConstants.GridCacheRefresh] = 'True';
            }
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage || 1;
            formData[this.serviceConstants.GridHeaderClickedColumn] = '';
            formData[this.serviceConstants.GridSortOrder] = 'Descending';

            this.ajaxSource.next(this.ajaxconstant.START);

            this.httpService.xhrPost('bi/reports', 'reports', 'Service/iCABSSeStockUsageEstimates', search, formData).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.commonGridFunction.setPageData(data);
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.pageParams.hasGridData = false;
                    this.displayMessage(error);
                });
        }
    }

    public onChange(event: any, type: string): void {
        this['commonGridFunction'].resetGrid();
        switch (type) {
            case 'Contract':
                this.commonLookup.getContractName(this.getControlValue('ContractNumber')).then(data => {
                    if (data && data[0] && data[0].length) {
                        this.setControlValue('ContractName', data[0][0]['ContractName']);
                    } else {
                        this.setControlValue('ContractName', '');
                    }
                }).catch(error => {
                    this.displayMessage(error);
                });
                break;
            case 'Environment':
                this.setControlValue('SelEnvironmentUsage', event);
                break;
        }
    }
}
