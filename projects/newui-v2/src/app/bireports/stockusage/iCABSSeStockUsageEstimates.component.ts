import { Component, OnInit, AfterContentInit, Injector, ViewChild } from '@angular/core';

import { BIReportsRoutes } from '@base/PageRoutes';
import { CommonGridFunction } from '@base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ProductSearchGridComponent } from '@app/internal/search/iCABSBProductSearch';
import { ProductDetailSearchComponent } from '@app/internal/search/iCABSBProductDetailSearch.component';
import { IGenericEllipsisControl } from '@shared/components/ellipsis-generic/ellipsis-generic';

@Component({
    templateUrl: 'iCABSSeStockUsageEstimatesBranch.html',
    providers: [CommonLookUpUtilsService]
})

export class ICABSSeStockUsageEstimatesComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private xhrParams: IXHRParams = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Service/iCABSSeStockUsageEstimates'
    };

    protected controls: IControls[] = [
        { name: 'BusinessCode', disabled: true, required: true, value: this.utils.getBusinessCode(), type: MntConst.eTypeCode },
        { name: 'BusinessDesc', disabled: true, required: true, value: this.utils.getBusinessText(), type: MntConst.eTypeText },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EnvironmentMetricInd', type: MntConst.eTypeCheckBox },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'SelEnvironmentUsage', type: MntConst.eTypeText },
        { name: 'ProductCode', type: MntConst.eTypeCode },
        { name: 'ProductDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ProductDetailCode', type: MntConst.eTypeCode },
        { name: 'ProductDetailDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'RegulatoryAuthorityNumber', type: MntConst.eTypeInteger },
        { name: 'RegulatoryAuthorityName', type: MntConst.eTypeText, disabled: true },
        { name: 'ShowType', type: MntConst.eTypeText, value: 'All' }
    ];

    public commonGridFunction: CommonGridFunction;
    public environmentalUsages: Array<any>;
    public isRegion: boolean = false;
    public pageId: string;
    public pageType: string;

    public ellipsis: Record<string, Record<string, Object | IGenericEllipsisControl>> = {
        product: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: ProductSearchGridComponent
        },
        productDetail: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: ProductDetailSearchComponent
        },
        regulatoryAuthority: {
            autoOpen: false,
            ellipsisTitle: 'Regulatory Authority Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp',
                extraParams: {
                    'BranchNumber': this.utils.getBranchCode()
                }
            },
            httpConfig: {
                operation: 'Business/iCABSBRegulatoryAuthoritySearch',
                module: 'waste',
                method: 'service-delivery/search'
            },
            tableColumn: [
                { title: 'Regulatory Authority Number', name: 'RegulatoryAuthorityNumber' },
                { title: 'Name', name: 'RegulatoryAuthorityName' },
                { title: 'Max Weight Per Day (KG)', name: 'MaximumWeightPerDay' },
                { title: 'Requires Premises Registration', name: 'RequiresPremiseRegistrationInd' },
                { title: 'Requires Service Cover Waste', name: 'RequiresServiceCoverWasteInd' },
                { title: 'Requires Waste Transfer Notes', name: 'RequiresWasteTransferNotesInd' },
                { title: 'Waste Regulatory Carrier Name', name: 'WasteRegulatoryCarrierName' },
                { title: 'Waste Regulatory Registration Number', name: 'WasteRegulatoryRegistrationNum' }
            ],
            disable: false
        }
    };

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageType = this.riExchange.getParentHTMLValue('pageType');

        this.isRegion = this.pageType === 'Region';

        this.pageId = PageIdentifier['ICABSSESTOCKUSAGEESTIMATES' + this.pageType.toUpperCase()];
        this.browserTitle = this.pageTitle = 'Stock Usage Estimates ' + this.pageType;
        this.utils.setTitle(this.pageTitle);

        super.ngAfterContentInit();

        this.getEnvironmentalUsage();
        this.buildGrid();

        if (this.isRegion) {
            this.commonLookup.getRegionDesc().subscribe((data) => {
                this.setControlValue('RegionCode', data[0][0]['RegionCode']);
                this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
            });
        }

        if (this.isReturning()) {
            this.onRiGridRefresh();
        } else {
            this.pageParams.gridConfig = {
                itemsPerPage: 10,
                totalItem: 1,
                gridHandle: this.utils.randomSixDigitString()
            };
            this.commonLookup.setTradingDates(this.uiForm, 'DateFrom', 'DateTo');
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('PrepCode', 'Usage', 'PrepCode', MntConst.eTypeCode, 3, true);
        this.riGrid.AddColumnAlign('PrepCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PrepDesc', 'Usage', 'PrepDesc', MntConst.eTypeText, 30);
        if (this.pageType === 'Premises') {
            this.riGrid.AddColumn('ContractJobNo', 'Usage', 'ContractJobNo', MntConst.eTypeText, 5);
        } else {
            this.riGrid.AddColumn('NumberOfPremises', 'Usage', 'NumberOfPremises', MntConst.eTypeInteger, 5);
        }
        this.riGrid.AddColumn('Quantity', 'Usage', 'Quantity', MntConst.eTypeDecimal2, 5);
        this.riGrid.AddColumn('MeasuredIn', 'Usage', 'MeasuredIn', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('Value', 'Usage', 'Value', MntConst.eTypeDecimal2, 5);
        if (this.pageType === 'Premises') {
            this.riGrid.AddColumn('RegAuthNumber', 'Usage', 'PrepDesc', MntConst.eTypeText, 50);
            this.riGrid.AddColumnScreen('RegAuthNumber', false);
        }

        if (this.getControlValue('EnvironmentMetricInd')) {
            this.riGrid.AddColumn('WeightUsage', 'Usage', 'WeightUsage', MntConst.eTypeDecimal2, 5);
        }

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        this.isRequesting = true;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        let formData: Object = {};

        formData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        formData['Level'] = this.pageType === 'Premises' ? 'Premise' : this.pageType;
        if (this.isRegion) {
            formData[this.serviceConstants.RegionCode] = this.getControlValue('RegionCode');
            formData['ViewBy'] = '';
        } else if (this.pageType === 'Premises') {
            formData['ProductCode'] = this.getControlValue('ProductCode');
            formData['ProductDetailCode'] = this.getControlValue('ProductDetailCode');
            formData['RegulatoryAuthorityNumber'] = this.getControlValue('RegulatoryAuthorityNumber');
            formData['ShowType'] = this.getControlValue('ShowType');
        }
        formData['DateFrom'] = this.getControlValue('DateFrom');
        formData['DateTo'] = this.getControlValue('DateTo');
        formData['SelEnvironmentUsage'] = this.getControlValue('SelEnvironmentUsage');
        formData['EnvironmentMetricInd'] = this.getControlValue('EnvironmentMetricInd') ? 'True' : 'False';

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage || 1;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData[this.serviceConstants.GridSortOrder] = 'Descending';

        this.commonGridFunction.fetchGridData(this.xhrParams, search, formData);
    }

    public onGridBodyDoubleClick(): void {
        if (this.riGrid.CurrentColumnName === 'PrepCode') {
            if (this.isRegion) {
                this.setAttribute('RegionCode', this.getControlValue('RegionCode'));
                this.setAttribute('RegionDesc', this.getControlValue('RegionDesc'));
            }
            this.navigate(this.isRegion ? 'Region' : 'Branch', BIReportsRoutes.ICABSSESTOCKUSAGEESTIMATESDETAIL,
                {
                    'PrepCode': this.riGrid.Details.GetValue('PrepCode'),
                    'PrepDesc': this.riGrid.Details.GetValue('PrepDesc'),
                    'MeasuredIn': this.riGrid.Details.GetValue('MeasuredIn'),
                    'DateFrom': this.getControlValue('DateFrom'),
                    'DateTo': this.getControlValue('DateTo'),
                    'ViewBy': this.pageType === 'Premises' ? 'Business' : this.pageType
                });
        }
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    private getEnvironmentalUsage(): void {
        this.commonLookup.getEnvironmentalUsageDetails().then(data => {
            if (data[0].length > 0) {
                this.environmentalUsages = data[0];
                this.setControlValue('SelEnvironmentUsage', this.environmentalUsages[0].EnvironmentCode);
            }
        });
    }

    public onChange(event: any, changeFor: string): void {
        switch (changeFor) {
            case 'Environment':
                this.commonGridFunction.resetGrid();
                this.setControlValue('SelEnvironmentUsage', event);
                break;
            case 'Product':
            case 'ProductDetail':
                if (event && event.target && event.target.value) {
                    this.commonGridFunction.resetGrid();
                }
                break;
        }
    }

    public onEllipsisDataReceived(data: any, ellipsisFor: string): void {
        switch (ellipsisFor) {
            case 'Product':
                this.commonGridFunction.resetGrid();
                this.setControlValue('ProductCode', data.ProductCode);
                this.setControlValue('ProductDesc', data.ProductDesc);
                break;
            case 'ProductDetail':
                this.commonGridFunction.resetGrid();
                this.setControlValue('ProductDetailCode', data.ProductDetailCode);
                this.setControlValue('ProductDetailDesc', data.ProductDetailDesc);
                break;
            case 'RegulatoryAuthority':
                this.setControlValue('RegulatoryAuthorityNumber', data.RegulatoryAuthorityNumber);
                this.setControlValue('RegulatoryAuthorityName', data.RegulatoryAuthorityName);
                break;
        }
    }
}
