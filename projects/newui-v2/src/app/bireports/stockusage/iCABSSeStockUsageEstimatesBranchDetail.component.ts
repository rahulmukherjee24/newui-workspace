import { Component, OnInit, AfterContentInit, Injector, ViewChild } from '@angular/core';

import { LightBaseComponent } from '@base/BaseComponentLight';
import { PageIdentifier } from '@base/PageIdentifier';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { ContractManagementModuleRoutes } from '@app/base/PageRoutes';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';

@Component({
    templateUrl: 'iCABSSeStockUsageEstimatesBranchDetail.html'
})

export class ICABSSeStockUsageEstimatesBranchDetailComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public commonGridFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public pageId: string;
    public controls: IControls[] = [
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'DateFrom', disabled: true, type: MntConst.eTypeDate },
        { name: 'DateTo', disabled: true, type: MntConst.eTypeDate },
        { name: 'MeasuredIn', type: MntConst.eTypeText },
        { name: 'PrepCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'PrepDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.gridConfig = {
            itemsPerPage: 10,
            totalItem: 1
        };
        this.pageParams.gridHandle = this.utils.randomSixDigitString();
    }

    public ngAfterContentInit(): void {
        this.pageId = PageIdentifier.ICABSSESTOCKUSAGEESTIMATESBRANCHDETAIL;
        this.pageTitle = this.browserTitle = 'Stock Usage Estimates Detail';
        super.ngAfterContentInit();
        this.onWindowLoad();
    }

    private onWindowLoad(): void {
        this.buildGrid();
        if (!this.isReturning()) {
            this.riExchange.getParentHTMLValue('BranchNumber');
            this.riExchange.getParentHTMLValue('BranchName');
            this.riExchange.getParentHTMLValue('PrepCode');
            this.riExchange.getParentHTMLValue('PrepDesc');
            this.riExchange.getParentHTMLValue('MeasuredIn');
            this.riExchange.getParentHTMLValue('DateFrom');
            this.riExchange.getParentHTMLValue('DateTo');
        }
        this.onRiGridRefresh();
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('ContractNumber', 'Usage', 'ContractNumber', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ContractName', 'Usage', 'ContractName', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('PremiseNumber', 'Usage', 'PremiseNumber', MntConst.eTypeText, 10, true);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ProductCode', 'Usage', 'ProductCode', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceDateStart', 'Usage', 'ServiceDateStart', MntConst.eTypeDate, 5);
        this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('EmployeeCode', 'Usage', 'EmployeeCode', MntConst.eTypeText, 6);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('NumberOfPremises', 'Usage', 'NumberOfPremises', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('PrepVolume', 'Usage', 'PrepVolume', MntConst.eTypeDecimal2, 5);
        this.riGrid.AddColumn('PrepValue', 'Usage', 'PrepValue', MntConst.eTypeDecimal2, 5);

        this.riGrid.Complete();
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    public onGridBodyDoubleClick(): void {
        let CurrentContractTypeURLParameter;
        switch (this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty')) {
            case 'C':
                CurrentContractTypeURLParameter = '<contract>';
                break;
            case 'J':
                CurrentContractTypeURLParameter = '<job>';
                break;
            case 'P':
                CurrentContractTypeURLParameter = '<product>';
                break;
        }
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractNumber':
                this.navigate('StockUsageSearch', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE,
                    {
                        'ContractNumber': this.riGrid.Details.GetValue('ContractNumber'),
                        'CurrentContractTypeURLParameter': CurrentContractTypeURLParameter
                    });
                break;
            case 'PremiseNumber':
                this.navigate('StockEstimatesSearch', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE,
                    {
                        'CurrentContractTypeURLParameter': CurrentContractTypeURLParameter,
                        'ContractNumber': this.riGrid.Details.GetValue('ContractNumber'),
                        'PremiseNumber': this.riGrid.Details.GetValue('PremiseNumber')
                    });
                break;
        }
    }

    public populateGrid(): void {
        const xhrParams: IXHRParams = {
            method: 'bi/reports',
            module: 'reports',
            operation: 'Service/iCABSSeStockUsageEstimates'
        };
        if (this.riExchange.validateForm(this.uiForm)) {
            const search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            const formData: Object = {};

            formData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
            formData['Level'] = 'Detail';
            formData['ViewBy'] = 'Branch';
            formData['BusinessCode'] = this.utils.getBusinessCode();
            formData['RegionCode'] = this.riExchange.getParentHTMLValue('RegionCode');
            formData['BranchNumber'] = this.getControlValue('BranchNumber');
            formData['PrepCode'] = this.riExchange.getParentHTMLValue('PrepCode');
            formData['DateFrom'] = this.getControlValue('DateFrom');
            formData['DateTo'] = this.getControlValue('DateTo');
            formData[this.serviceConstants.GridCacheRefresh] = true;
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage || 1;
            formData[this.serviceConstants.GridHeaderClickedColumn] = '';
            formData[this.serviceConstants.GridSortOrder] = 'Descending';

            this.commonGridFunction.fetchGridData(xhrParams, search, formData);
        }
    }
}
