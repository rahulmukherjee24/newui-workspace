import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BIReportsRoutesConstant } from '@base/PageRoutes';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { SharedModule } from '@shared/shared.module';

import { CreditReasonAnalysisComponent } from './iCABSARCreditReasonAnalysis.component';
import { CreditReasonAnalysisDetailComponent } from './iCABSARCreditReasonAnalysisDetail.component';
import { CreditReasonAnalysisDetailDetailComponent } from './iCABSARCreditReasonAnalysisDetailDetail.component';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class CreditAnalysisRootComponent {
    constructor(viewContainerRef: ViewContainerRef) { }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchEllipsisModule,
        InternalSearchModule,
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: CreditAnalysisRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSARCREDITREASONANALYSIS, component: CreditReasonAnalysisComponent },
                    { path: BIReportsRoutesConstant.ICABSARCREDITREASONANALYSISBUSINESS, component: CreditReasonAnalysisComponent },
                    { path: BIReportsRoutesConstant.ICABSARCREDITREASONANALYSISREGION, component: CreditReasonAnalysisComponent },
                    { path: BIReportsRoutesConstant.ICABSARCREDITREASONANALYSISDETAIL, component: CreditReasonAnalysisDetailComponent },
                    { path: BIReportsRoutesConstant.ICABSARCREDITREASONANALYSISDETAILDETAIL, component: CreditReasonAnalysisDetailDetailComponent }
                ], data: { domain: 'CREDIT ANALYSIS' }
            }
        ])
    ],
    declarations: [
        CreditAnalysisRootComponent,
        CreditReasonAnalysisComponent,
        CreditReasonAnalysisDetailComponent,
        CreditReasonAnalysisDetailDetailComponent
    ]
})

export class CreditAnalysisModule { }
