
import { Component, OnInit, Injector, ViewChild } from '@angular/core';

import { BranchServiceAreaSearchComponent } from '@app/internal/search/iCABSBBranchServiceAreaSearch';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { ContractSearchComponent } from '@app/internal/search/iCABSAContractSearch';
import { IControls } from '@app/base/ControlsType';
import {
    IGenericEllipsisControl,
    EllipsisGenericComponent
} from '@shared/components/ellipsis-generic/ellipsis-generic';
import { IXHRParams } from '@app/base/XhrParams';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PremiseSearchComponent } from '@app/internal/search/iCABSAPremiseSearch';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';


@Component({
    templateUrl: 'iCABSARActualVsContractualReport.html',
    providers: [CommonLookUpUtilsService]
})

export class ActualVsContractualReportComponent extends LightBaseComponent implements OnInit {

    @ViewChild('WasteTransferTypeSearchEllipsis') WasteTransferTypeSearchEllipsis: EllipsisGenericComponent;
    public controls: IControls[] = [
        { name: 'BranchName', required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', required: true, type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', required: false, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'BreakByContract', type: MntConst.eTypeCheckBox, value: true },
        { name: 'BreakByPremise', type: MntConst.eTypeCheckBox, value: true },
        { name: 'ContractName', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', required: false, type: MntConst.eTypeCode, value: '' },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EWCCode', required: false, type: MntConst.eTypeCode },
        { name: 'EWCDescription', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'Frequency', required: false, type: MntConst.eTypeInteger },
        { name: 'PremiseName', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseNumber', required: false, type: MntConst.eTypeInteger },
        { name: 'RepDest', value: 'direct' },
        { name: 'ReportType', value: 'all' },
        { name: 'VisitIntervalFrom', required: false, type: MntConst.eTypeInteger, value: '1' },
        { name: 'VisitIntervalTo', required: false, type: MntConst.eTypeInteger, value: '4' },
        { name: 'WasteTransferTypeCode', required: false, type: MntConst.eTypeCode },
        { name: 'WasteTransferTypeDesc', required: false, disabled: true, type: MntConst.eTypeText }
    ];
    public pageTitle: string;
    public pageId: string;
    public batchSubmittedText: string = '';
    public ellipsis: Record<string, Record<string, Object>> = {
        contract: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: ContractSearchComponent
        },
        premise: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: PremiseSearchComponent
        },
        serviceArea: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: BranchServiceAreaSearchComponent
        }
    };

    public WasteTransferTypeEllipsisConfig: IGenericEllipsisControl = {
        autoOpen: false,
        ellipsisTitle: 'Waste Transfer Type Search',
        configParams: {
            table: '',
            shouldShowAdd: false,
            parentMode: 'LookUp'
        },
        tableColumn: [
            { title: 'Waste Transfer Type Code', name: 'WasteTransferTypeCode', type: MntConst.eTypeCode },
            { title: 'Description', name: 'WasteTransferTypeDesc', type: MntConst.eTypeText },
            { title: 'Waste Consignment Note Required', name: 'WasteConsignmentNoteRequiredIn', type: MntConst.eTypeCheckBox }
        ],
        disable: false
    };

    public dropDown: Record<string, Record<string, Object>> = {
        branchSearch: {
            params: {
                parentMode: 'LookUp'
            },
            active: { id: '', text: '' },
            isDisabled: true
        }
    };

    constructor(injector: Injector, private sysCharConstants: SysCharConstants, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARACTUALVSCONTRACTUALREPORT;
        this.browserTitle = this.pageTitle = 'Actual Vs Contractual Report';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.setDropDownValue();
        this.windowOnLoad();
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = this.getURLSearchParamObject();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.SystemCharNumber, [this.sysCharConstants.SystemCharShowPremiseWasteTab].toString());
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.records[0])
                this.pageParams.vWasteFilterDisp = data.records[0].Required;
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }

    private windowOnLoad(): void {
        this.getFromAndToDate();
    }

    public WasteTransferTypeSearchhttpConfig: IXHRParams = {
        operation: 'Business/iCABSBWasteTransferTypeSearch',
        module: 'waste',
        method: 'service-delivery/search'
    };

    private getFromAndToDate(): void {
        this.isRequesting = true;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formData = {
            'Function': 'GetQuarterDates'
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'ApplicationReport/iCABSARActualVsContractualReport', search, formData).then(
            (data) => {
                this.isRequesting = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.displayMessage(data);
                    return;
                }
                this.setControlValue('DateFrom', data.FromDate);
                this.setControlValue('DateTo', data.ToDate);
                this.loadSysChars();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.isRequesting = false;
                this.displayMessage(error);
                this.loadSysChars();
            });
    }


    private getPromiseCall(funcNeme: string, displayfeild: string, event: any): void {
        if (event.target.value !== '') {
            this.commonLookup[funcNeme](this.getControlValue(event.target.id), this.utils.getBusinessCode(), this.getControlValue('ContractNumber')).then(data => {
                if (data && data[0] && data[0][0]) {
                    this.setControlValue(displayfeild, data[0][0][displayfeild]);
                    if (event.target.id === 'ContractNumber') {
                        this.ellipsis.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                        this.ellipsis.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                        this.setDropDownValue();

                    }
                }
            }).catch(error => {
                this.displayMessage(error);
            });
        } else {
            this.setControlValue(displayfeild, '');
            if (event.target.id === 'ContractNumber') {
                this.setDropDownValue();
            }
        }
    }

    private setDropDownValue(): void {
        if (this.getControlValue('ContractNumber') === '') {
            this.dropDown.branchSearch.active = {
                id: this.utils.getBranchCode(),
                text: this.utils.getBranchCode() + ' - ' + this.utils.getBranchTextOnly()
            };
            this.setControlValue('BranchNumber', this.utils.getBranchCode());
            this.setControlValue('BranchName', this.utils.getBranchTextOnly());
            this.setControlValue('PremiseNumber', '');
            this.setControlValue('PremiseName', '');
            this.ellipsis.premise.childConfigParams['ContractNumber'] = '';
            this.ellipsis.premise.childConfigParams['ContractName'] = '';
            this.dropDown.branchSearch.isDisabled = true;
        } else {
            this.dropDown.branchSearch.active = {
                id: '',
                text: ''
            };
            this.setControlValue('BranchNumber', '');
            this.setControlValue('BranchName', '');
            this.dropDown.branchSearch.isDisabled = false;
        }
    }

    public onValueChange(event: any, value: string): void {
        switch (value) {
            case 'Contract':
                this.setControlValue('ContractNumber', event.ContractNumber);
                this.setControlValue('ContractName', event.ContractName);
                this.ellipsis.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                this.ellipsis.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                this.setDropDownValue();
                break;
            case 'Premise':
                this.setControlValue('PremiseNumber', event.PremiseNumber);
                this.setControlValue('PremiseName', event.PremiseName);
                break;
            case 'Service Area':
                this.setControlValue('BranchServiceAreaCode', event.BranchServiceAreaCode);
                this.setControlValue('BranchServiceAreaDesc', event.BranchServiceAreaDesc);
                break;
            case 'Waste Transfer':
                this.setControlValue('WasteTransferTypeCode', event.WasteTransferTypeCode);
                this.setControlValue('WasteTransferTypeDesc', event.WasteTransferTypeDesc);
                break;
        }


    }

    public onChnage(event: any): void {
        switch (event.target.id) {
            case 'ContractNumber':
                this.getPromiseCall('getContractName', 'ContractName', event);
                break;
            case 'PremiseNumber':
                this.getPromiseCall('getPremiseName', 'PremiseName', event);
                break;
            case 'BranchServiceAreaCode':
                this.getPromiseCall('getBranchServiceAreaDesc', 'BranchServiceAreaDesc', event);
                break;
            case 'EWCCode':
                this.getPromiseCall('getEWCDesc', 'EWCDescription', event);
                break;
            case 'WasteTransferTypeCode':
                this.getPromiseCall('getWasteTransferDesc', 'WasteTransferTypeDesc', event);
                break;
        }
    }

    public onBranchChange(event: any): void {
        this.setControlValue('BranchNumber', event.BranchNumber);
        this.setControlValue('BranchName', event.BranchName);
    }

    public submitReportGeneration(): void {
        let dateDiff = this.utils.dateDiffInDays(this.getControlValue('DateFrom'), this.getControlValue('DateTo'));
        if (dateDiff < 0) {
            this.riExchange.riInputElement.markAsError(this.uiForm, 'DateTo');
            this.riExchange.riInputElement.markAsError(this.uiForm, 'DateFrom');
            this.uiForm.controls['DateTo'].updateValueAndValidity();
            return;
        }
        if (this.uiForm.valid) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');

            let formData = {
            };
            Object.keys(this.uiForm.controls).forEach(control => {
                if (control === 'BreakByContract' || control === 'BreakByPremise') {
                    formData[control] = this.getControlValue(control) ? 'yes' : 'no';
                } else {
                    formData[control] = this.getControlValue(control);
                }
            });
            formData['RepManDest'] = this.getControlValue('RepDest') === 'direct' ? 'batch|ReportID' : 'email|User';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'ApplicationReport/iCABSARActualVsContractualReport', search, formData).then(
                (data) => {
                    this.isRequesting = false;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (this.hasError(data)) {
                        this.displayMessage(data);
                        return;
                    }
                    this.batchSubmittedText = data.ReturnHTML;
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }
}
