import { Component, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { BIReportsRoutesConstant } from '../../base/PageRoutes';
import { SharedModule } from '../../../shared/shared.module';
import { ActualVsContractualReportComponent } from './iCABSARActualVsContractualReport.component';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
@Component({
    template: `<router-outlet></router-outlet>
    `
})
export class ActualVsContractualBIReportsRootComponent {
    constructor() {

    }
}

@NgModule({
    imports: [
        SharedModule,
        HttpClientModule,
        SearchEllipsisDropdownModule,
        RouterModule.forChild([
            {
                path: '', component: ActualVsContractualBIReportsRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSARACTUALVSCONTRACTUALREPORT, component: ActualVsContractualReportComponent }
                ], data: { domain: 'BI REPORTS ACTUAL VS CONTRACTUAL' }
            }

        ])
    ],
    declarations: [
        ActualVsContractualReportComponent,
        ActualVsContractualBIReportsRootComponent
    ]

})

export class ActualVsContractualBIReportsModule {
}
