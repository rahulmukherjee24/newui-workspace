import { Component, Injector, OnInit, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';

import { ContractManagementModuleRoutes } from '@app/base/PageRoutes';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { StaticUtils } from '@shared/services/static.utility';

@Component({
    templateUrl: 'iCABSARBranchTurnoverDetailGrid.html'
})
export class BranchTurnoverDetailGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected pageId: string;

    public branchTitle: string;
    public columnName: string;
    public hasGridData: boolean = false;
    public showColumnCode: boolean = false;
    public showCompanyCode: boolean = false;


    private xhrParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARBranchTurnoverDetailGrid'
    };

    protected controls: Array<Object> = [
        { name: 'MonthFrom', type: MntConst.eTypeInteger, disabled: true },
        { name: 'MonthTo', type: MntConst.eTypeInteger, disabled: true },
        { name: 'YearFrom', type: MntConst.eTypeInteger, disabled: true },
        { name: 'YearTo', type: MntConst.eTypeInteger, disabled: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'CompanyCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'CompanyDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ColumnCode', type: MntConst.eTypeText, disabled: true },
        { name: 'ColumnDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ServiceTypeCode', type: MntConst.eTypeText, disabled: true },
        { name: 'ServiceTypeDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'MarketSegmentCode', type: MntConst.eTypeText, disabled: true },
        { name: 'MarketSegmentDesc', type: MntConst.eTypeText, disabled: true }
    ];

    public gridConfig: any = {
        totalItem: 1,
        itemsPerPage: 10
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageTitle = 'Turnover Detail';
        this.utils.setTitle(this.pageTitle);
        this.pageId = PageIdentifier['ICABSARBRANCHTURNOVERDETAILGRID'];
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {

        this.branchTitle = this.parentMode === 'BranchTotal' ? 'Branch Total' : 'Branch';
        this.showCompanyCode = !!this.riExchange.getParentHTMLValue('CompanyCode');
        this.showColumnCode = !!this.riExchange.getParentAttributeValue('ColumnCode');
        this.columnName = this.riExchange.getParentHTMLValue('ViewType');

        if (this.isReturning()) {
            this.onRiGridRefresh();
        } else {
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;

            this.setControlValue('ColumnDesc', this.riExchange.getParentAttributeValue('ColumnDesc'));
            this.setControlValue('ColumnCode', this.riExchange.getParentAttributeValue('ColumnCode'));
            this.riExchange.getParentHTMLValue('ServiceTypeDesc');
            this.riExchange.getParentHTMLValue('MarketSegmentDesc');
            this.riExchange.getParentHTMLValue('BranchName');
            this.riExchange.getParentHTMLValue('CompanyDesc');
            this.riExchange.getParentHTMLValue('BranchNumber');
            this.riExchange.getParentHTMLValue('MonthFrom');
            this.riExchange.getParentHTMLValue('MonthTo');
            this.riExchange.getParentHTMLValue('YearFrom');
            this.riExchange.getParentHTMLValue('YearTo');
            this.riExchange.getParentHTMLValue('ServiceTypeCode');
            this.riExchange.getParentHTMLValue('MarketSegmentCode');
            this.riExchange.getParentHTMLValue('CompanyCode');

            super.ngAfterContentInit();
            this.buildGrid();
            this.populateGrid();
        }

    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('AccountNumber', 'InvoiceDetail', 'AccountNumber', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('AccountNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('AccountName', 'InvoiceDetail', 'AccountName', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('AccountName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ColumnCode', 'InvoiceDetail', 'ColumnCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ColumnCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractName', 'InvoiceDetail', 'ContractName', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('ContractName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('PremiseNumber', 'InvoiceDetail', 'PremiseNumber', MntConst.eTypeCode, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'InvoiceDetail', 'PremiseName', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);

        if (this.parentMode === 'BranchTotal') {
            this.riGrid.AddColumn('ServiceTypeCode', 'InvoiceDetail', 'ServiceTypeCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);

            if (this.columnName === 'Expense') {
                this.riGrid.AddColumn('ExpenseCode', 'InvoiceDetail', 'ExpenseCode', MntConst.eTypeCode, 6);
                this.riGrid.AddColumnAlign('ExpenseCode', MntConst.eAlignmentCenter);
            }
        }

        this.riGrid.AddColumn('ProductCode', 'InvoiceDetail', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('TotalValue', 'InvoiceDetail', 'TotalValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('TotalValue', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let strGridData: any = {};
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        gridQuery.set(this.serviceConstants.Action, '2');

        strGridData[this.serviceConstants.BusinessCode] = this.businessCode();
        strGridData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        strGridData['ViewType'] = this.columnName;
        strGridData['ColumnCode'] = this.getControlValue('ColumnCode');
        strGridData['MonthFrom'] = this.getControlValue('MonthFrom');
        strGridData['MonthTo'] = this.getControlValue('MonthTo');
        strGridData['YearFrom'] = this.getControlValue('YearFrom');
        strGridData['YearTo'] = this.getControlValue('YearTo');
        strGridData[this.serviceConstants.LanguageCode] = this.riExchange.LanguageCode();
        strGridData['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
        strGridData['MarketSegmentCode'] = this.getControlValue('MarketSegmentCode');
        strGridData['CompanyCode'] = this.getControlValue('CompanyCode');
        strGridData[this.serviceConstants.Function] = 'BranchDetailForExpense' + StaticUtils.capitalizeFirstLetterAlphaNumberic(this.riExchange.getParentAttributeValue('ColumnClicked'));
        strGridData[this.serviceConstants.Level] = this.parentMode === 'BranchTotal' ? 'detailTotal' : 'detail';

        strGridData[this.serviceConstants.GridMode] = '0';
        strGridData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        strGridData[this.serviceConstants.PageSize] = 10;
        strGridData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        strGridData[this.serviceConstants.GridHeaderClickedColumn] = '';
        strGridData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, strGridData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                this.setPagination();
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    private setPagination(): void {
        setTimeout(() => {
            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
        }, 100);
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.riGrid.RefreshRequired();
            this.populateGrid();
        }
    }

    public getCurrentPage(currentPage: any): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            super.getCurrentPage(currentPage);
        } else {
            this.setPagination();
        }
    }

    public onGridBodyDoubleClick(_event?: any): void {

        let ContractParameterURL: string;
        if (this.riGrid.Details.GetValue('AccountNumber') !== 'TOTAL') {

            switch (this.riGrid.Details.GetAttribute('AccountName', 'AdditionalProperty')) {
                case 'C':
                    ContractParameterURL = ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE;
                    break;
                case 'J':
                    ContractParameterURL = ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE;
                    break;
                case 'P':
                    ContractParameterURL = ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE;
                    break;
            }

            switch (this.riGrid.CurrentColumnName) {
                case 'ColumnCode':
                    this.navigate('PortfolioReports', ContractParameterURL, {
                        'ContractRowID': this.riGrid.Details.GetAttribute('ColumnCode', 'AdditionalProperty')
                    });
                    break;
                case 'AccountNumber':
                    this.navigate('Profitability', ContractManagementModuleRoutes.ICABSAACCOUNTMAINTENANCE, {
                        'Mode': 'Profitability',
                        'AccountRowID': this.riGrid.Details.GetAttribute('AccountNumber', 'AdditionalProperty')
                    });
                    break;
                case 'PremiseNumber':
                    this.navigate('Profitability', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                        'PremiseRowID': this.riGrid.Details.GetAttribute('PremiseNumber', 'AdditionalProperty'),
                        'contracttypecode': this.riGrid.Details.GetAttribute('AccountName', 'AdditionalProperty')
                    });
                    break;
                case 'ProductCode':
                    let rowID = this.riGrid.Details.GetAttribute('ProductCode', 'AdditionalProperty');
                    if (rowID) {
                        this.navigate('TechWorkSummary', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                            ServiceCoverRowID: rowID,
                            currentContractType: this.riGrid.Details.GetAttribute('AccountName', 'AdditionalProperty')
                        });
                    } else {
                        this.displayMessage('No Drilldown Available Due To Missing Service Cover Link', CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                    }
                    break;
            }
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }
}
