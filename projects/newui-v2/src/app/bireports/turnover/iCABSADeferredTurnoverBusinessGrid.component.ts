import { Component, OnInit, AfterContentInit, Injector, ViewChild } from '@angular/core';

import { BIReportsRoutes } from '@app/base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';


@Component({
    templateUrl: 'iCABSADeferredTurnoverBusinessGrid.html'
})

export class DeferredTurnoverBusinessGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {

    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    private hasViewTypeChnage: boolean = false;
    public type: string;
    public pageId: string;
    public pageTitle: string;
    public controls = [
        { name: 'BranchName', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', readonly: true, disabled: true, required: true, type: MntConst.eTypeInteger },
        { name: 'BusinessCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BusinessDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'CompanyCode' },
        { name: 'CompanyDesc' },
        { name: 'lstViewType', value: 'Branch' }
    ];
    public hasGridData: boolean = false;

    public dropDown: Record<string, Object> = {
        company: {
            inputParams: {
                'parentMode': 'LookUp',
                businessCode: this.utils.getBusinessCode(),
                countryCode: this.utils.getCountryCode()
            },
            active: {
                id: '',
                text: ''
            }
        }
    };

    constructor(injector: Injector, private sysCharConstants: SysCharConstants) {
        super(injector);
        this.browserTitle = this.pageTitle = 'Deferred Turnover';

    }

    ngOnInit(): void {
        super.ngOnInit();

    }

    ngAfterContentInit(): void {
        this.type = this.riExchange.getParentHTMLValue('type');
        this.pageId = this.type === 'branch' ? PageIdentifier.ICABSADEFERREDTURNOVERBRANCHGRID : PageIdentifier.ICABSADEFERREDTURNOVERBUSINESSGRID;
        super.ngAfterContentInit();
        this.buildGrid();
        if (!this.isReturning()) {
            this.pageParams.gridConfig = {
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString(),
                gridCacheRefresh: true,
                pageSize: 10
            };
            this.pageParams.gridCurrentPage = 1;
            if (this.type !== 'branch') {
                this.setControlValue('BusinessCode', this.businessCode());
                this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            }
            if (this.parentMode) {
                this.riExchange.getParentHTMLValue('CompanyCode');
                this.riExchange.getParentHTMLValue('CompanyDesc');
                this.setControlValue('BranchNumber', this.riExchange.getParentAttributeValue('BranchNumber'));
                this.setControlValue('BranchName', this.riExchange.getParentAttributeValue('BranchName'));
            } else {
                this.setControlValue('BranchNumber', this.utils.getBranchCode());
                this.setControlValue('BranchName', this.utils.getBranchTextOnly());
            }
            this.loadSysChars();
            if (this.parentMode && this.type === 'branch') {
                this.populateGrid();
            }
        } else {
            this.pageParams.gridConfig.gridCacheRefresh = false;
            this.populateGrid();

        }
        if (this.getControlValue('CompanyCode')) {
            this.dropDown.company['active'] = {
                id: this.getControlValue('CompanyCode'),
                text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
            };
        }
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = this.getURLSearchParamObject();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.SystemCharNumber, [this.sysCharConstants.SystemCharEnableCompanyCode].toString());
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.SCEnableCompanyCode = data.records[0].Required;
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        switch (this.getControlValue('lstViewType')) {
            case 'Branch':
                this.riGrid.AddColumn('BranchNumber', 'DeferredTurnover', 'BranchNumber', MntConst.eTypeText, 6, true);
                this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('BranchName', 'DeferredTurnover', 'BranchName', MntConst.eTypeText, 15, true);
                break;

            case 'Region':
                this.riGrid.AddColumn('RegionCode', 'DeferredTurnover', 'RegionCode', MntConst.eTypeText, 6, true);
                this.riGrid.AddColumnAlign('RegionCode', MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('RegionDesc', 'DeferredTurnover', 'RegionDesc', MntConst.eTypeText, 15, false);
                break;
        }
        this.riGrid.AddColumn('MonthOne', 'DeferredTurnover', 'MonthOne', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthOne', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('MonthTwo', 'DeferredTurnover', 'MonthTwo', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthTwo', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('MonthThree', 'DeferredTurnover', 'MonthThree', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthThree', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('MonthFour', 'DeferredTurnover', 'MonthFour', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthFour', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('MonthFive', 'DeferredTurnover', 'MonthFive', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthFive', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('MonthSix', 'DeferredTurnover', 'MonthSix', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthSix', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('MonthSeven', 'DeferredTurnover', 'MonthSeven', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthSeven', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('MonthEight', 'DeferredTurnover', 'MonthEight', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthEight', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('MonthNine', 'DeferredTurnover', 'MonthNine', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthNine', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('MonthTen', 'DeferredTurnover', 'MonthTen', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthTen', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('MonthEleven', 'DeferredTurnover', 'MonthEleven', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthEleven', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('MonthTwelve', 'DeferredTurnover', 'MonthTwelve', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('MonthTwelve', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('Future', 'DeferredTurnover', 'Future', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('Future', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('Total', 'DeferredTurnover', 'Total', MntConst.eTypeText, 12, true);

        this.riGrid.AddColumnAlign('Total', MntConst.eAlignmentRight);
        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');

            let formData: Object = {
                'Level': this.type === 'branch' ? 'Branch' : 'Business',
                'BusinessCode': this.utils.getBusinessCode(),
                'ViewType': this.getControlValue('lstViewType'),
                'CountryCode': '',
                'CompanyCode': this.getControlValue('CompanyCode'),
                'BranchNumber': this.getControlValue('BranchNumber')
            };
            formData[this.serviceConstants.GridMode] = '2';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'Application/iCABSADeferredTurnoverBranchGrid', search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.hasGridData = true;
                        this.displayMessage(data);
                    } else {
                        this.riGrid.RefreshRequired();
                        this.hasGridData = true;
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                            }, 500);
                        }
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.hasGridData = true;
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }

    public onDataReceived(data: Record<string, Object>): void {
        if (data && data.CompanyCode) {
            this.setControlValue('CompanyCode', data.CompanyCode);
            this.setControlValue('CompanyDesc', data.CompanyDesc);
        }
    }

    public onRiGridRefresh(): void {
        if (this.hasViewTypeChnage) {
            this.pageParams.gridCurrentPage = 1;
            this.hasViewTypeChnage = false;
        }
        this.buildGrid();
        this.populateGrid();
    }

    public getCurrentPage(data: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridConfig.gridCacheRefresh = false;
            super.getCurrentPage(data);
        }
        this.pageParams.gridConfig.gridCacheRefresh = true;
    }

    public onViewTypeChnage(): void {
        this.hasViewTypeChnage = true;
    }

    public onGridBodyDoubleClick(data: any): void {
        let currentCol: string = this.riGrid.CurrentColumnName;
        switch (currentCol) {
            case 'BranchNumber':
                if (this.type !== 'branch') {
                    let branchNumber: string = this.riGrid.Details.GetAttribute('BranchNumber', 'rowid');
                    if (branchNumber === 'Totals') {
                        this.navigate('DeferredTurnoverBranchTotals', BIReportsRoutes.ICABSADEFERREDTURNOVERDETAILGRID, {
                            BranchNumber: 0,
                            MonthNumber: 14
                        });
                    } else {
                        this.navigate('DeferredTurnoverBusiness', BIReportsRoutes.ICABSADEFERREDTURNOVERBRANCHGRID, {
                            BranchNumber: branchNumber,
                            BranchName: this.riGrid.Details.GetValue('BranchName'),
                            type: 'branch'
                        });
                    }
                }
                break;
            case 'RegionCode':
                if (this.type !== 'branch') {
                    let regionCode: string = this.riGrid.Details.GetValue('RegionCode');
                    if (regionCode === 'Totals') {
                        this.navigate('DeferredTurnoverRegionTotals', BIReportsRoutes.ICABSADEFERREDTURNOVERDETAILGRID, {
                            RegionNumber: 0,
                            MonthNumber: 14
                        });
                    } else {
                        this.displayMessage(MessageConstant.Message.ScreenNotReady);
                    }
                }
                break;
            case 'BranchName':
            case 'RegionDesc': break;
            default:
                let queryParams: any = {};
                let monthValue: string = this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'additionalProperty');
                if (monthValue !== 'Total') {
                    if (this.getControlValue('lstViewType') === 'Branch') {
                        queryParams['BranchNumber'] = this.riGrid.Details.GetValue('BranchNumber');
                    } else {
                        queryParams['RegionCode'] = this.riGrid.Details.GetValue('RegionCode');
                    }
                    queryParams['MonthNumber'] = monthValue;
                    this.navigate(this.type === 'branch' ? 'DeferredTurnoverBranch' : 'DeferredTurnoverBusiness', BIReportsRoutes.ICABSADEFERREDTURNOVERDETAILGRID, queryParams);
                }
                break;
        }
    }
}
