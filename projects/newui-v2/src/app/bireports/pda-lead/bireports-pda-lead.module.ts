import { Component, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';


import { LeadSubmittedBranchComponent } from './iCABSCMLeadsSubmittedBranch.component';
import { SharedModule } from '@shared/shared.module';
import { BIReportsRoutesConstant } from '@app/base/PageRoutes';

@Component({
    template: `<router-outlet></router-outlet>
    `
})
export class PDALeadBIReportsRootComponent {
    constructor() {

    }
}

@NgModule({
    imports: [
        SharedModule,
        HttpClientModule,
        RouterModule.forChild([
            {
                path: '', component: PDALeadBIReportsRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSCMLEADSSUBMITTEDBRANCH, component: LeadSubmittedBranchComponent},
                    { path: BIReportsRoutesConstant.ICABSCMLEADSSUBMITTEDBUSINESS, component: LeadSubmittedBranchComponent},
                    { path: BIReportsRoutesConstant.ICABSCMLEADSSUBMITTEDDETAIL, component: LeadSubmittedBranchComponent},
                    { path: BIReportsRoutesConstant.ICABSCMLEADSSUBMITTEDREGION, component: LeadSubmittedBranchComponent}
                ], data: { domain: 'BI REPORTS PDA LEAD' }
            }

        ])
    ],
    declarations: [
        PDALeadBIReportsRootComponent,
        LeadSubmittedBranchComponent
    ]

})

export class PDALeadBIReportsModule {
}
