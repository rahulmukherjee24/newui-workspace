import { OnInit, Injector, Component, ViewChild, AfterViewInit, AfterContentInit, ChangeDetectorRef } from '@angular/core';

import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { PaginationComponent } from '@shared/components/pagination/pagination';


@Component({
    templateUrl: 'iCABSCMLeadsSubmittedBranch.html',
    styles: [`
    :host /deep/ .detail .gridtable tbody tr td:nth-child(1)  input{
        width:46% !important;
    }
    :host /deep/ .not-detail .gridtable tbody tr td:nth-child(1)  input{
        width:25% !important;
    }
  `]
})

export class LeadSubmittedBranchComponent extends LightBaseComponent implements OnInit, AfterViewInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private isChanged: boolean = false;

    public alertMessage: any;
    public controls = [
        { name: 'BranchName', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'DateFilter' },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EmployeeCode', required: false, type: MntConst.eTypeText },
        { name: 'EmployeeSurname', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'FilterBy' },
        { name: 'RegionCode', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'RegionDesc', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ViewBy' }

    ];
    public ellipsis: any = {
        employeeEllipsis: {
            childparams: {
            },
            component: EmployeeSearchComponent
        }
    };
    public hasGridData: boolean = false;
    public isDetail: boolean = false;
    public isRegion: boolean = false;
    public isBusiness: boolean;
    public messageType: string = CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR;
    public pageId: string;
    public pageTitle: string;
    public pageParams: any;

    constructor(injector: Injector, private cdRef: ChangeDetectorRef) {
        super(injector);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.isTotal = false;
        this.pageParams.gridConfig = {
            pageSize: 10,
            totalRecords: 1,
            gridHandle: this.utils.randomSixDigitString(),
            gridCacheRefresh: true
        };
        this.riGrid.DefaultBorderColor = 'ADD8E6';
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
    }

    ngAfterViewInit(): void {
        if (!this.isReturning()) {
            this.pageParams.gridConfig['currentPage'] = 1;
            this.setControlValue('DateFilter', 'Submitted');
            this.setControlValue('ViewBy', 'Employee');
            this.setControlValue('FilterBy', 'Employee');
        } else {
            this.pageParams.gridConfig.gridCacheRefresh = false;
        }
        switch (this.parentMode) {
            case 'Region':
            case 'Business':
                if (!this.isReturning()) {
                    this.riExchange.getParentHTMLValue('RegionCode');
                    this.riExchange.getParentHTMLValue('RegionDesc');
                    this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('BranchNumber'));
                    this.riExchange.getParentHTMLValue('DateFrom');
                    this.riExchange.getParentHTMLValue('DateTo');
                    this.riExchange.getParentHTMLValue('DateFilter');
                    this.riExchange.getParentHTMLValue('EmployeeCode');

                    this.doLookupForEmployee();
                    this.ellipsis.employeeEllipsis.childparams['serviceBranchNumber'] = this.getControlValue('BranchNumber');
                    if (!this.pageParams.isTotal)
                        this.doLookUpForBranch(this.getControlValue('BranchNumber'));
                }
                this.onRiGridRefresh();
                break;
            case 'Branch':
                if (!this.isReturning()) {
                    if (this.riExchange.getParentHTMLValue('Total') === 'true') {
                        this.pageParams.isTotal = true;
                    }
                    this.riExchange.getParentHTMLValue('RegionCode');
                    this.riExchange.getParentHTMLValue('RegionDesc');
                    this.riExchange.getParentHTMLValue('DateFilter');
                    this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('BranchNumber'));
                    if (this.riExchange.getParentHTMLValue('EmployeeCode') === 'TOTAL') {
                        this.setControlValue('EmployeeCode', '');
                    } else {
                        this.doLookupForEmployee();
                    }
                    if (!this.pageParams.isTotal)
                        this.doLookUpForBranch(this.getControlValue('BranchNumber'));
                    this.setControlValue('FilterBy', this.riExchange.getParentHTMLValue('ViewBy'));
                    this.riExchange.getParentHTMLValue('DateFrom');
                    this.riExchange.getParentHTMLValue('DateTo');
                    this.ellipsis.employeeEllipsis.childparams['serviceBranchNumber'] = this.getControlValue('BranchNumber');
                }
                this.onRiGridRefresh();
                break;
            default:
                if (!this.isReturning()) {
                    let date: Date = new Date();
                    let formDate: Date = new Date(date.getFullYear(), date.getMonth(), 1);
                    this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(formDate) as string);
                    this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(date) as string);
                    this.setControlValue('BranchNumber', this.utils.getBranchCode());
                    this.setControlValue('BranchName', this.utils.getBranchTextOnly(this.utils.getBranchCode()));
                    if (this.isRegion) {
                        this.doLookUpForBranch(this.utils.getBranchCode());
                    }
                } else {
                    this.onRiGridRefresh();
                }
                break;
        }
        this.setEmployeeChildParams();
        this.cdRef.detectChanges();
    }

    ngAfterContentInit(): void {
        this.pageParams.isTotal = this.riExchange.getParentHTMLValue('BranchNumber') === 'Total';
        this.isRegion = this.riExchange.getParentHTMLValue('type') === 'region';
        this.isDetail = (this.riExchange.getParentHTMLValue('type') === 'branch' || this.riExchange.getParentHTMLValue('type') === 'business' || this.riExchange.getParentHTMLValue('type') === 'region') ? false : true;
        this.isBusiness = this.riExchange.getParentHTMLValue('type') === 'business';
        this.pageId = this.isBusiness ? PageIdentifier.ICABSCMLEADSSUBMITTEDBUSINESS : this.isRegion ? PageIdentifier.ICABSCMLEADSSUBMITTEDREGION : this.isDetail ? PageIdentifier.ICABSCMLEADSSUBMITTEDDETAIL : PageIdentifier.ICABSCMLEADSSUBMITTEDBRANCH;
        this.pageTitle = 'PDA Leads' + (!this.isDetail ? ' (' + (this.isRegion ? 'Region' : this.isBusiness ? 'Business' : 'Branch').toString() + ')' : ' Detail');
        this.utils.setTitle(this.pageTitle);
        super.ngAfterContentInit();
    }

    private setEmployeeChildParams(): void {
        this.ellipsis.employeeEllipsis.childparams['parentMode'] = this.getControlValue('FilterBy') === 'Sales' ? 'LookUp-LeadsSales' : 'LookUp-LeadsService';
    }

    private doLookupForEmployee(): void {
        if (this.getControlValue('EmployeeCode') !== '') {
            let lookupIP = [
                {
                    'table': 'Employee',
                    'query': {
                        'BusinessCode': this.businessCode(),
                        'EmployeeCode': this.getControlValue('EmployeeCode')
                    },
                    'fields': ['EmployeeSurname']
                }
            ];
            this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
                if (data && data[0] && data[0].length) {
                    this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
                } else {
                    this.setControlValue('EmployeeSurname', '');
                }
            });
        } else {
            this.setControlValue('EmployeeSurname', '');
        }
    }

    private doLookUpForBranch(branchNumber: string): void {
        let lookupIP = [
            {
                'table': 'Branch',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'BranchNumber': branchNumber
                },
                'fields': ['BranchName', 'RegionCode']
            }
        ];
        this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data[0] && data[0].length) {
                this.setControlValue('BranchName', data[0][0].BranchName);
                this.setControlValue('RegionCode', data[0][0].RegionCode);
                if (this.isRegion) {
                    this.doLookUpForRegion();
                }
            } else {
                this.setControlValue('BranchName', '');
                this.setControlValue('RegionCode', '');
            }
        });
    }

    private doLookUpForRegion(): void {
        let lookupIP = [
            {
                'table': 'Region',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'RegionCode': this.getControlValue('RegionCode')
                },
                'fields': ['RegionDesc']
            }
        ];
        this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data[0] && data[0].length) {
                this.setControlValue('RegionDesc', data[0][0].RegionDesc);
            } else {
                this.setControlValue('RegionDesc', '');
            }
        });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        if (this.isBusiness || this.isRegion) {
            this.riGrid.AddColumn('GridBranchNumber', 'GridBranchNumber', 'GridBranchNumber ', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnOrderable('GridBranchNumber', true);
            this.riGrid.AddColumn('GridBranchName', 'GridBranchName', 'GridBranchName ', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnOrderable('GridBranchName', true);
        } else {
            this.riGrid.AddColumn('GridEmployeeCode', 'GridEmployeeCode', 'GridEmployeeCode ', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnOrderable('GridEmployeeCode', true);

            this.riGrid.AddColumn('GridSurname', 'GridSurname', 'GridSurname ', MntConst.eTypeText, 15);
            this.riGrid.AddColumnOrderable('GridSurname', true);
            if (!this.isDetail)
                this.riGrid.AddColumn('GridBranchNumber', 'GridBranchNumber', 'GridBranchNumber ', MntConst.eTypeInteger, 3);
        }
        if (!this.isDetail) {
            if (this.getControlValue('DateFilter').toLowerCase() === 'submitted') {
                this.riGrid.AddColumn('LeadsSubmitted', 'LeadsSubmitted', 'LeadsSubmitted ', MntConst.eTypeInteger, 6);
                this.riGrid.AddColumnOrderable('LeadsSubmitted', true);
            }
            this.riGrid.AddColumn('LeadsConverted', 'LeadsConverted', 'LeadsConverted ', MntConst.eTypeInteger, 6);
            this.riGrid.AddColumnOrderable('LeadsConverted', true);
        } else {
            this.riGrid.AddColumn('CustomerContactNumber', 'CustomerContactNumber', 'CustomerContactNumber ', MntConst.eTypeInteger, 6);
            this.riGrid.AddColumnOrderable('CustomerContactNumber', true);

            this.riGrid.AddColumn('CustomerName', 'CustomerName', 'CustomerName ', MntConst.eTypeText, 15);
            this.riGrid.AddColumnOrderable('CustomerName', true);

            this.riGrid.AddColumn('CustomerAddress', 'CustomerAddress', 'CustomerAddress ', MntConst.eTypeText, 15);

            this.riGrid.AddColumn('GridSalesEmployeeCode', 'GridSalesEmployeeCode', 'GridSalesEmployeeCode ', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnOrderable('GridSalesEmployeeCode', true);

            this.riGrid.AddColumn('GridSalesSurname', 'GridSalesSurname', 'GridSalesSurname ', MntConst.eTypeText, 15);
            this.riGrid.AddColumnOrderable('GridSalesSurname', true);

            this.riGrid.AddColumn('ConvertedDate', 'ConvertedDate', 'ConvertedDate ', MntConst.eTypeDate, 8);
            this.riGrid.AddColumnOrderable('ConvertedDate', true);
        }

        this.riGrid.AddColumn('ConvertedValue', 'CustomerAddress', 'CustomerAddress ', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnOrderable('ConvertedValue', true);

        this.riGrid.Complete();
    }

    // Populate data into the grid
    private populateGrid(): void {
        if (this.uiForm.valid) {
            if (this.isChanged) {
                this.pageParams.gridConfig['currentPage'] = 1;
            }
            if (this.uiForm.dirty) {
                this.pageParams.gridConfig.gridCacheRefresh = true;
            }
            this.buildGrid();
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            let formData: Object = {
                'Level': this.isRegion ? 'Region' : this.isBusiness ? 'Business' : 'Branch',
                'BranchNumber': this.getControlValue('BranchNumber') === 'Total' && this.riExchange.getParentHTMLValue('type') !== 'branch' ? this.utils.getBranchCode() : this.getControlValue('BranchNumber'),
                'DateFrom': this.getControlValue('DateFrom'),
                'DateTo': this.getControlValue('DateTo'),
                'ViewBy': this.getControlValue('ViewBy'),
                'FilterBy': this.getControlValue('FilterBy'),
                'EmployeeCode': this.getControlValue('EmployeeCode'),
                'RegionCode': this.getControlValue('RegionCode'),
                'DateFilter': this.getControlValue('DateFilter')
            };
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
            formData[this.serviceConstants.PageCurrent] = (this.pageParams.gridCurrentPage) ? this.pageParams.gridCurrentPage : this.pageParams.gridConfig.currentPage.toString();
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest('bi/reports', 'reports', this.isRegion ? 'ContactManagement/iCABSCMLeadsSubmittedRegion' : this.isDetail ? 'ContactManagement/iCABSCMLeadsSubmittedDetail' : this.isBusiness ? 'ContactManagement/iCABSCMLeadsSubmittedBusiness' : 'ContactManagement/iCABSCMLeadsSubmittedBranch', search, formData).subscribe(
                (data) => {
                    this.isChanged = false;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (data.hasError) {
                        this.hasGridData = false;
                        let msgTxt: string = data.errorMessage;
                        msgTxt += data.fullError ? ' - ' + data.fullError : '';
                        this.alertMessage = {
                            msg: msgTxt,
                            timestamp: (new Date()).getMilliseconds()
                        };
                    } else {
                        this.hasGridData = true;
                        this.riGrid.RefreshRequired();
                        this.pageParams.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                            }, 500);
                        }
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.hasGridData = false;
                    this.isRequesting = false;
                    let msgTxt: string = error.errorMessage;
                    msgTxt += error.fullError ? ' - ' + error.fullError : '';
                    this.alertMessage = {
                        msg: msgTxt,
                        timestamp: (new Date()).getMilliseconds()
                    };
                });
        }
    }

    public onDateFilterChange(): void {
        this.isChanged = true;
    }

    public onViewByChange(): void {
        this.isChanged = true;
    }

    public onEmployeeDataReceived(data: any): void {
        this.setControlValue('EmployeeCode', data.EmployeeCode);
        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
    }

    public onFilterByDataChange(): void {
        this.setEmployeeChildParams();
    }

    public onEmployeeChnage(): void {
        this.doLookupForEmployee();
    }

    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public onDateChnage(): void {
        this.riGrid.RefreshRequired();
    }

    public getCurrentPage(event: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridConfig.gridCacheRefresh = false;
            super.getCurrentPage(event);
        }
    }

    public onGridBodyDoubleClick(): void {
        let queryParams: any = {};
        for (let control in this.uiForm.controls) {
            if (true)
                queryParams[control] = this.getControlValue(control);
        }
        if (this.riGrid.CurrentColumnName.toLowerCase() === 'gridemployeecode' && !this.isDetail) {
            let employeeCode: string = this.riGrid.Details.GetValue('GridEmployeeCode');
            if (employeeCode !== '') {
                queryParams['EmployeeCode'] = this.riGrid.Details.GetValue('GridEmployeeCode');
                queryParams['Total'] = this.pageParams.isTotal;
                this.navigate('Branch', BIReportsRoutes.ICABSCMLEADSSUBMITTEDDETAIL, queryParams);
            }

        }
        if (this.riGrid.CurrentColumnName.toLowerCase() === 'gridbranchnumber') {
            let branchValue: string = this.riGrid.Details.GetValue('GridBranchNumber');
            if (branchValue.toUpperCase() !== 'TOTAL' && branchValue !== '') {
                queryParams['type'] = 'branch';
                queryParams['BranchNumber'] = this.riGrid.Details.GetValue('GridBranchNumber');
                this.navigate('Business', BIReportsRoutes.ICABSCMLEADSSUBMITTEDBRANCH, queryParams);
            }
        }
    }

}
