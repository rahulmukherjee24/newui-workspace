import { RiBatchComponent } from './riBatch.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component } from '@angular/core';

import { BIReportsRoutesConstant } from '@base/PageRoutes';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { SharedModule } from '@shared/shared.module';

import { NextInvoiceRunForecaseDetailGridComponent } from './NextInvoiceRunForecast/iCABSARNextInvoiceRunForecastDetailGrid.component';
import { NextInvoiceRunForecastBusinessGridComponent } from './NextInvoiceRunForecast/iCABSARNextInvoiceRunForecastBusinessGrid.component';
import { InvoiceAnalysisGridComponent } from './InvoiceAnalysis/iCABSARInvoiceAnalysisGrid.component';
import { InvoiceAnalysisDetailGridComponent } from './InvoiceAnalysis/iCABSARInvoiceAnalysisDetailGrid.component';
import { TaxCodeReportComponent } from './iCABSARTaxCodeReport.component';
import { TaxCodeSummaryComponent } from './iCABSARTaxCodeSummary.component';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class InvoiceBIReportsRootComponent {

    constructor() {
    }
}

@NgModule({
    exports: [
    ],
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        InternalSearchModule,
        RouterModule.forChild([
            {
                path: '', component: InvoiceBIReportsRootComponent, children: [
                    { path: BIReportsRoutesConstant.NEXTINVOICERUNFORECASTDETAIL, component: NextInvoiceRunForecaseDetailGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARNEXTINVOICERUNFORECASTBUSINESSGRID, component: NextInvoiceRunForecastBusinessGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARINVOICEANALYSISGRID, component: InvoiceAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARINVOICEANALYSISDETAILGRID, component: InvoiceAnalysisDetailGridComponent },
                    { path: BIReportsRoutesConstant.RIBATCHCOMPONENT, component: RiBatchComponent },
                    { path: BIReportsRoutesConstant.ICABSARTAXCODEREPORT, component: TaxCodeReportComponent },
                    { path: BIReportsRoutesConstant.ICABSARTAXCODESUMMARY, component: TaxCodeSummaryComponent },
                    { path: BIReportsRoutesConstant.ICABSARTAXCODESUMMARYDETAIL, component: TaxCodeSummaryComponent },
                    { path: BIReportsRoutesConstant.ICABSARINVOICEANALYSISEMPLOYEEGRID, component: InvoiceAnalysisGridComponent }
                ], data: { domain: 'BI REPORTS INVOICE' }
            }

        ])
    ],
    declarations: [
        InvoiceBIReportsRootComponent,
        NextInvoiceRunForecaseDetailGridComponent,
        NextInvoiceRunForecastBusinessGridComponent,
        InvoiceAnalysisGridComponent,
        InvoiceAnalysisDetailGridComponent,
        RiBatchComponent,
        TaxCodeReportComponent,
        TaxCodeSummaryComponent
    ]

})

export class InvoiceBIReportsModule { }
