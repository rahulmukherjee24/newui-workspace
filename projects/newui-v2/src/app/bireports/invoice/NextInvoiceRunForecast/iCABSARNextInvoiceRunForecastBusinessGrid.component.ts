import { AfterContentInit, Component, OnInit, OnDestroy, Injector, ViewChild } from '@angular/core';

import { BIReportsRoutes } from '@base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { ModalAdvService } from '@shared/components/modal-adv/modal-adv.service';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSARNextInvoiceRunForecastBusinessGrid.html'
})

export class NextInvoiceRunForecastBusinessGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public alertMessage: any;
    public messageType: string;
    public pageId: string;
    public search: QueryParams = this.getURLSearchParamObject();

    public gridConfig = {
        action: 2,
        totalRecords: 1,
        pageSize: 10
    };

    private headerParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARNextInvoiceRunForecastBusinessGrid'
    };

    constructor(injector: Injector, private modalAdvService: ModalAdvService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARNEXTINVOICERUNFORECASTBUSINESSGRID;
        this.utils.setTitle('Next Invoice Run Forecast Select');
        this.pageTitle = 'Next Invoice Run Forecast Select';
    }

    public controls = [
        { name: 'BusinessDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'DisplayLines', required: true, value: this.gridConfig.pageSize, type: MntConst.eTypeInteger }
    ];

    public ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private onWindowLoad(): void {
        if (!this.isReturning()) {
            this.pageParams.gridCurrentPage = 1;
        }
        this.setControlValue('BusinessDesc', this.utils.getBusinessText(this.businessCode(), this.countryCode()));
        this.buildGrid();
        this.populateGrid();
    }

    private buildGrid(): void {
        this.riGrid.HighlightBar = true;
        this.riGrid.Clear();
        this.riGrid.AddColumn('ReportNumber', 'NextInvoiceRunForecast', 'ReportNumber', MntConst.eTypeText, 12, true);
        this.riGrid.AddColumnAlign('ReportNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('GenerationDate', 'NextInvoiceRunForecast', 'GenerationDate', MntConst.eTypeText, 12, false);
        this.riGrid.AddColumnAlign('GenerationDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('GenerationTime', 'NextInvoiceRunForecast', 'GenerationTime', MntConst.eTypeText, 12, false);
        this.riGrid.AddColumnAlign('GenerationTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ForecastDate', 'NextInvoiceRunForecast', 'ForecastDate', MntConst.eTypeDate, 12, false);
        this.riGrid.AddColumnAlign('ForecastDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('RemoveForecast', 'NextInvoiceRunForecast', 'RemoveForecast', MntConst.eTypeImage, 1);
        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        let formData: any = {};
        this.search.set(this.serviceConstants.Action, this.gridConfig.action.toString());
        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        formData[this.serviceConstants.Level] = 'Business';
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        formData[this.serviceConstants.GridCacheRefresh] = true;
        formData[this.serviceConstants.PageSize] = this.getControlValue('DisplayLines').toString();
        formData[this.serviceConstants.PageCurrent] = (this.pageParams.gridCurrentPage) ? this.pageParams.gridCurrentPage : this.pageParams.gridConfig.currentPage.toString();
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.errorMessage) {
                this.messageType = 'error';
                let msgTxt: string = data.errorMessage;
                msgTxt += data.fullError ? ' - ' + data.fullError : '';
                this.alertMessage = {
                    msg: msgTxt,
                    timestamp: (new Date()).getMilliseconds()
                };
                return;
            } else {
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.getControlValue('DisplayLines') : 1;
                if (this.isReturning()) {
                    setTimeout(() => {
                        this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                    }, 500);
                }
                this.riGrid.Execute(data);
            }
        });
    }

    public onRiGridRefresh(): void {
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.populateGrid();
    }

    private removeReport(): void {
        let formData: any = {};
        this.search.set(this.serviceConstants.Action, this.gridConfig.action.toString());
        formData[this.serviceConstants.Function] = 'RemoveForecast';
        formData['ReportNumber'] = this.riGrid.Details.GetValue('ReportNumber');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, this.search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.errorMessage) {
                this.messageType = 'error';
                let msgTxt: string = data.errorMessage;
                msgTxt += data.fullError ? ' - ' + data.fullError : '';
                this.alertMessage = {
                    msg: msgTxt,
                    timestamp: (new Date()).getMilliseconds()
                };
                return;
            } else {
                this.populateGrid();
            }
        });
    }

    public onGridBodyDoubleClick(): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'ReportNumber':
                if (this.riGrid.Details.GetAttribute('ReportNumber', 'AdditionalProperty') === 'yes') {
                    this.navigate('NextInvoiceRunForecastBusiness', BIReportsRoutes.NEXTINVOICERUNFORECASTDETAIL, {
                        'GeneratedDate': this.riGrid.Details.GetValue('GenerationDate'),
                        'GeneratedTime': this.riGrid.Details.GetValue('GenerationTime'),
                        'ForecastDate': this.riGrid.Details.GetValue('ForecastDate'),
                        'ReportNumber': this.riGrid.Details.GetValue('ReportNumber'),
                        'GroupRowID': this.riGrid.Details.GetAttribute('ReportNumber', 'rowid')
                    });
                }
                break;
            case 'RemoveForecast':
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.removeReport.bind(this)));
                break;
        }
    }
}
