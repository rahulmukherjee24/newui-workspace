import { Component, OnInit, Injector, ViewChild, AfterContentInit, OnDestroy } from '@angular/core';

import { BIReportsRoutes } from '@base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSARInvoiceAnalysisGrid.html'
})

export class InvoiceAnalysisGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected pageId: string;
    public pageType: string;

    public companySearch: any = {
        inputParams: {
            parentMode: 'LookUp',
            countryCode: this.countryCode(),
            businessCode: this.businessCode()
        },
        active: { id: '', text: '' }
    };
    public controls: Array<Object> = [
        { name: 'CompanyCode', type: MntConst.eTypeText },
        { name: 'CompanyDesc', type: MntConst.eTypeText },
        { name: 'FilterBy', type: MntConst.eTypeText, value: 'RunNumber' },
        { name: 'ProcessDateFrom', type: MntConst.eTypeDate },
        { name: 'ProcessDateTo', type: MntConst.eTypeDate },
        { name: 'RunNumberFrom', type: MntConst.eTypeInteger, required: true },
        { name: 'RunNumberTo', type: MntConst.eTypeInteger, required: true },
        { name: 'TaxCode', type: MntConst.eTypeText },
        { name: 'ViewBy', type: MntConst.eTypeText },
        { name: 'BranchNumber', type: MntConst.eTypeText},
        { name: 'BranchName', type: MntConst.eTypeText }
    ];
    public filterBy: string = '';
    public gridConfig: any = {
        pageSize: 10,
        totalRecords: 1
    };

    constructor(injector: Injector) {
        super(injector);
    }

    // #region LifeCycle Hooks
    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageType = this.riExchange.getParentHTMLValue('PageType');
        this.pageId = this.pageType === 'Employee' ? PageIdentifier.ICABSARINVOICEANALYSISEMPLOYEEGRID : PageIdentifier.ICABSARINVOICEANALYSISGRID;
        this.browserTitle = this.pageTitle = 'Invoice Analysis By ' + ( this.pageType === 'Employee' ? 'Employee' : 'Branch' );
        super.ngAfterContentInit();
        this.filterBy = this.pageType !== 'Employee' ? this.getControlValue('FilterBy') : this.riExchange.getParentHTMLValue('FilterBy');
        this.pageParams.gridHandle = this.pageParams.gridHandle || this.utils.randomSixDigitString();
        this.buildGrid();
        if (this.isReturning()) {
            this.pageParams.gridCacheRefresh = false;
            this.onRiGridRefresh();

            let companyCode: string = this.getControlValue('CompanyCode');
            if (companyCode) {
                this.companySearch.active = {
                    id: companyCode,
                    text: companyCode + ' - ' + this.getControlValue('CompanyDesc')
                };
            }
        } else {
            this.setControlValue('ViewBy', this.pageType === 'Employee' ? 'NegEmployee' : 'NegBranch');
            if (this.pageType === 'Employee') {
                    this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('BranchNumber'));
                    this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('BranchName'));
                    this.setControlValue('TaxCode', this.riExchange.getParentHTMLValue('TaxCode'));
                    this.setControlValue(this.filterBy + 'From', this.riExchange.getParentHTMLValue('FilterByFrom'));
                    this.setControlValue(this.filterBy + 'To', this.riExchange.getParentHTMLValue('FilterByTo'));
                    this.disableControls(['FilterBy', 'TaxCode', 'RunNumberFrom', 'RunNumberTo', 'ProcessDateFrom', 'ProcessDateTo']);
                    this.onRiGridRefresh();
            }
            this.getTaxCodes();
            this.pageParams.gridCacheRefresh = true;
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    // #endregion

    // #region Private Methods
    private getTaxCodes(): void {
        /*
         * Query Executed On TaxCode Table Instead Of TaxCodeLang,
         * Since Not All Countries Have Populated TaxCodeLang. E.g. - UK
         */
        let queryData: any = [
            {
                'table': 'TaxCode',
                'query': {},
                'fields': ['TaxCode', 'TaxCodeDesc']
            }
        ];
        this.LookUp.lookUpPromise(queryData).then(data => {
            if (this.hasError(data)) {
                this.displayMessage(data);
                return;
            }
            if (data && data[0] && data[0].length) {
                this.pageParams.taxCodes = data[0];
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + 'TaxCode');
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        if (this.pageType !== 'Employee') {
            this.riGrid.AddColumn('BranchNumber', 'InvoiceAnalysis', 'BranchNumber', MntConst.eTypeText, 6, true);
            this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('BranchName', 'InvoiceAnalysis', 'BranchName', MntConst.eTypeText, 14);
            this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentLeft);
        } else {
            this.riGrid.AddColumn('EmployeeCode','InvoiceAnalysis','EmployeeCode',MntConst.eTypeCode,6,true);
            this.riGrid.AddColumnAlign('EmployeeCode',MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('EmployeeName','InvoiceAnalysis','EmployeeName',MntConst.eTypeText,14);
            this.riGrid.AddColumnAlign('EmployeeName',MntConst.eAlignmentLeft);
            this.riGrid.AddColumnOrderable('EmployeeCode',true);
        }

        this.riGrid.AddColumn('InvoiceTotalExclTax', 'InvoiceAnalysis', 'InvoiceTotalExclTax', this.pageType === 'Employee' ? MntConst.eTypeDecimal2 : MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('InvoiceTotalExclTax', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('InvoiceTaxTotal', 'InvoiceAnalysis', 'InvoiceTaxTotal', this.pageType === 'Employee' ? MntConst.eTypeDecimal2 : MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('InvoiceTaxTotal', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('InvoiceTotal', 'InvoiceAnalysis', 'InvoiceTotal', this.pageType === 'Employee' ? MntConst.eTypeDecimal2 : MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('InvoiceTotal', MntConst.eAlignmentRight);

        if (this.pageType !== 'Employee') {
            this.riGrid.AddColumn('InvoiceTotalCount', 'InvoiceAnalysis', 'InvoiceTotalCount', MntConst.eTypeInteger, 4);
            this.riGrid.AddColumnAlign('InvoiceTotalCount', MntConst.eAlignmentRight);
        }

        this.riGrid.AddColumn('CreditTotalExclTax', 'InvoiceAnalysis', 'CreditTotalExclTax', this.pageType === 'Employee' ? MntConst.eTypeDecimal2 : MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('CreditTotalExclTax', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('CreditTaxTotal', 'InvoiceAnalysis', 'CreditTaxTotal', this.pageType === 'Employee' ? MntConst.eTypeDecimal2 : MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('CreditTaxTotal', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('CreditTotal', 'InvoiceAnalysis', 'CreditTotal', this.pageType === 'Employee' ? MntConst.eTypeDecimal2 : MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('CreditTotal', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('NettTotal', 'InvoiceAnalysis', 'NettTotal', this.pageType === 'Employee' ? MntConst.eTypeDecimal2 : MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('NettTotal', MntConst.eAlignmentRight);

        if (this.pageType !== 'Employee') {
            this.riGrid.AddColumnOrderable('BranchNumber', true);
        }

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        let form: any = {};

        gridSearch.set(this.serviceConstants.Action, 2);

        form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        form['BranchNumber'] = this.getControlValue('BranchNumber');
        form['FilterBy'] = this.filterBy;
        if (this.pageType === 'Employee') {
            form['ProcessDateFrom'] = this.getControlValue('ProcessDateFrom');
            form['ProcessDateTo'] = this.getControlValue('ProcessDateTo');
            form['RunNumberFrom'] = this.getControlValue('RunNumberFrom');
            form['RunNumberTo'] = this.getControlValue('RunNumberTo');
        } else {
            form['ViewBy'] = this.getControlValue('ViewBy');
            form[this.filterBy + 'From'] = this.getControlValue(this.filterBy + 'From');
            form[this.filterBy + 'To'] = this.getControlValue(this.filterBy + 'To');
            form['CompanyCode'] = this.getControlValue('CompanyCode');
            form['Detail'] = 'No';
        }
        form['TaxCode'] = this.getControlValue('TaxCode');
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        form[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        form[this.serviceConstants.PageSize] = this.gridConfig.pageSize;
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form['riSortOrder'] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        const operation = this.pageType === 'Employee' ? 'ApplicationReport/iCABSARInvoiceAnalysisEmployeeGrid' : 'ApplicationReport/iCABSARInvoiceAnalysisGrid';
        this.httpService.xhrPost('bi/reports', 'reports', operation, gridSearch, form).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                }, 100);
            }
        }).catch((error) => {
            this.displayMessage(error);
        });
    }
    // #endregion

    // #region Public Methods
    public onChangeFilterBy(): void {
        let orControlMapping: any = {
            ProcessDate: 'RunNumber',
            RunNumber: 'ProcessDate'
        };
        // Set Into A Variable To Avoid Method Execution In Two Places(TS and HTML)
        this.filterBy = this.getControlValue('FilterBy');
        this.setRequiredStatus(this.filterBy + 'From', true);
        this.setRequiredStatus(this.filterBy + 'To', true);
        this.setRequiredStatus(orControlMapping[this.filterBy] + 'From', false);
        this.setRequiredStatus(orControlMapping[this.filterBy] + 'To', false);
    }

    public onChangeCompany(data: any): void {
        for (let key in data) {
            if (data.hasOwnProperty(key)) {
                this.setControlValue(key, data[key]);
            }
        }
    }

    public onRefreshClick(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.pageParams.gridCacheRefresh = true;
            this.onRiGridRefresh();
        }
    }

    // Kept A One Liner Method Since It Is Implementation Of IGridHandler
    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.pageParams.gridCacheRefresh = false;
            super.getCurrentPage(currentPage);
        } else {
            // setPage Method In Pagination Executes Earlier Than Change; Hence Added The setTimeout
            setTimeout(() => {
                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
            }, 100);
        }
    }

    public onGridBodyDoubleClick(_event?: any): void {
        const filterByFrom = this.getControlValue('FilterBy') + 'From';
        const filterByTo = this.getControlValue('FilterBy') + 'To';
        if (this.getControlValue('ViewBy') === 'NegEmployee') {
            this.navigate('Employee', BIReportsRoutes.ICABSARINVOICEANALYSISEMPLOYEEGRID, {
                BranchNumber: this.riGrid.Details.GetValue('BranchNumber'),
                BranchName: this.riGrid.Details.GetValue('BranchName'),
                VATCode: this.getControlValue('TaxCode'),
                FilterBy: this.getControlValue('FilterBy'),
                FilterByFrom: this.getControlValue(filterByFrom),
                FilterByTo: this.getControlValue(filterByTo),
                PageType: 'Employee'
            });
        } else if (this.pageType !== 'Employee') {
            this.navigate('InvoiceAnalysis', BIReportsRoutes.ICABSARINVOICEANALYSISDETAILGRID, {
                BranchNumber: this.riGrid.Details.GetValue('BranchNumber')
            });
        }
    }

    public onHeaderClick(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.pageParams.gridCacheRefresh = false;
            this.onRiGridRefresh();
        }
    }

    public onChangeViewBy(event: any): void {
        this.setControlValue('ViewBy', event.target.value);
    }
    // #endregion
}
