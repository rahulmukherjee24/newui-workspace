import { OnInit, Component, Injector, OnDestroy, AfterContentInit, ViewChild } from '@angular/core';

import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { IXHRParams } from '@app/base/XhrParams';
import {
    IGenericEllipsisControl,
    EllipsisGenericComponent
} from '@shared/components/ellipsis-generic/ellipsis-generic';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { StaticUtils } from '@shared/services/static.utility';

@Component({
    templateUrl: 'iCABSARTaxCodeReport.html'
})

export class TaxCodeReportComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    protected pageId: string;
    @ViewChild('TaxCodeEllipsis') TaxCodeEllipsis: EllipsisGenericComponent;
    public controls: Array<Object> = [
        { name: 'TaxCode', required: true, type: MntConst.eTypeCode },
        { name: 'TaxCodeDesc', disabled: true, required: true, type: MntConst.eTypeText }
    ];

    constructor(injector: Injector) {
        super(injector);
    }
    ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageId = PageIdentifier.ICABSARTAXCODEREPORT;
        this.pageTitle = this.browserTitle = 'Tax Code';
        super.ngAfterContentInit();
    }

    private headerParams: IXHRParams = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARTaxCodeReport'
    };

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    public TaxCodeSearchConfig: IGenericEllipsisControl = {
        autoOpen: false,
        ellipsisTitle: 'Tax Code Search',
        configParams: {
            table: '',
            shouldShowAdd: false,
            parentMode: 'LookUp'
        },
        httpConfig: {
            operation: 'System/iCABSSTaxCodeSearch',
            module: 'tax',
            method: 'bill-to-cash/search'
        },
        tableColumn: [
            { title: 'Tax Code', name: 'TaxCode', type: MntConst.eTypeCode },
            { title: 'Description', name: 'TaxCodeDesc', type: MntConst.eTypeText },
            { title: 'System Default', name: 'TaxCodeDefaultInd', type: MntConst.eTypeCheckBox }
        ],
        disable: false
    };

    public onDefaultTaxChange(data: any): void {
        if (data) {
            this.setControlValue('TaxCode', data.TaxCode);
            this.setControlValue('TaxCodeDesc', data.TaxCodeDesc);
        }
    }

    public generateReport(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        const search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        const formData: Object = {};
        const form: any = {};
        form['Description'] = 'Tax Code Report';
        form['Report'] = 'report';
        form['ProgramName'] = 'iCABSTaxCodeReportGeneration.p';
        form['ParameterName'] = 'BusinessCode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'TaxCode' + StaticUtils.RI_SEPARATOR_VALUE_LIST;
        form['ParameterValue'] = this.utils.getBusinessCode() + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('TaxCode') + StaticUtils.RI_SEPARATOR_VALUE_LIST;
        form['StartDate'] = this.globalize.parseDateToFixedFormat(this.utils.Today());
        form['StartTime'] = StaticUtils.setTimeToMinutes();



        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, form).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage(data, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
        }).catch(error => {
            this.displayMessage(error);
        });
    }
}
