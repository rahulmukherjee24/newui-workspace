import { OnInit, Component, Injector, ViewChild, AfterContentInit } from '@angular/core';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { VariableService } from '@shared/services/variable.service';

import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { BIReportsRoutes, ContractManagementModuleRoutes } from '@app/base/PageRoutes';
import { CommonGridFunction } from '@app/base/CommonGridFunction';

@Component({
    templateUrl: 'iCABSARTaxCodeSummary.html'
})

export class TaxCodeSummaryComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected pageId: string;
    protected controls: IControls[] = [
        { name: 'BusinessCode', type: MntConst.eTypeCode, required: true, disabled: true },
        { name: 'BusinessDesc', type: MntConst.eTypeText, required: true, disabled: true },
        { name: 'TaxCode', type: MntConst.eTypeText, disabled: true },
        { name: 'TaxCodeDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'TaxRate', type: MntConst.eTypeCode, disabled: true },
        { name: 'ForYear', type: MntConst.eTypeInteger, required: true },
        { name: 'ForMonth', type: MntConst.eTypeText, required: true }
    ];
    public commonGridFunction: CommonGridFunction;

    public month: string[] = ['January','February','March','April','May','June','July',
    'August','September','October','November','December'];

    public fetchTrigger: boolean = false;

    private xhrParams: IXHRParams = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARTaxCodeSummary'
    };

    constructor(injector: Injector,private variableService: VariableService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }
    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.browserTitle = this.pageTitle = 'Tax Code Summary';
        const pageType = this.riExchange.getParentHTMLValue('pageType');
        this.pageId = PageIdentifier['ICABSARTAXCODESUMMARY' + pageType.toUpperCase()];
        super.ngAfterContentInit();
        this.window_OnLoad();
    }

    private window_OnLoad(): void {
        if (this.isReturning()) {
            this.pageParams.gridCacheRefresh = false;
            this.buildGrid();
            this.commonGridFunction.onRiGridRefresh();
        } else {
            this.pageParams.hasGridData = false;
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.GridSortOrder = 'Descending';
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.gridConfig = {
                itemsPerPage: 10,
                totalItem: 1
            };
            this.pageSetup();
        }
    }

    private pageSetup(): void {
        if (this.parentMode === 'Detail') {
            this.controls.forEach(control => {
                this.setControlValue(control.name,this.riExchange.getParentAttributeValue(control.name) ||
                this.riExchange.getParentHTMLValue(control.name));
            });
            this.fetchTrigger = true;
            this.buildGrid();
            this.commonGridFunction.onRiGridRefresh();
        } else {
            const date = new Date();
            this.setControlValue('BusinessCode', this.utils.getBusinessCode());
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            this.setControlValue('ForYear', date.getFullYear());
            this.setControlValue('ForMonth', this.month[date.getMonth()]);
        }
    }

    public buildGrid(): void {
        this.riGrid.Clear();
            if (this.parentMode === 'Detail') {
                this.riGrid.AddColumn('InvoiceNumber','TaxCodeSummary','InvoiceNumber',MntConst.eTypeText,12);
                this.riGrid.AddColumnAlign('InvoiceNumber',MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('InvoiceProductionDate','TaxCodeSummary','InvoiceProductionDate',MntConst.eTypeDate,10);
                this.riGrid.AddColumnAlign('InvoiceProductionDate',MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('ContractNumber','TaxCodeSummary','ContractNumber',MntConst.eTypeText,12);
                this.riGrid.AddColumnAlign('ContractNumber',MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('ContractName','TaxCodeSummary','ContractName',MntConst.eTypeText,40);
                this.riGrid.AddColumnAlign('ContractName',MntConst.eAlignmentLeft);
                this.riGrid.AddColumn('PremiseNumber','TaxCodeSummary','PremiseNumber',MntConst.eTypeInteger,3);
                this.riGrid.AddColumnAlign('PremiseNumber',MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('PremiseName','TaxCodeSummary','PremiseName',MntConst.eTypeText,40);
                this.riGrid.AddColumnAlign('PremiseName',MntConst.eAlignmentLeft);
                this.riGrid.AddColumn('ServiceBranch','TaxCodeSummary','ServiceBranch',MntConst.eTypeInteger,3);
                this.riGrid.AddColumnAlign('ServiceBranch',MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('ProductCode','TaxCodeSummary','ProductCode',MntConst.eTypeText,12);
                this.riGrid.AddColumnAlign('ProductCode',MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('ProductDesc','TaxCodeSummary','ProductDesc',MntConst.eTypeText,40);
                this.riGrid.AddColumnAlign('ProductDesc',MntConst.eAlignmentLeft);
                this.addTaxColumn();
                this.riGrid.AddColumn('InvoiceValueInclTax','TaxCodeSummary','InvoiceValueInclTax',MntConst.eTypeCurrency,12);
                this.riGrid.AddColumnAlign('InvoiceValueInclTax',MntConst.eAlignmentRight);
                this.riGrid.AddColumn('InvoiceValueExclTax','TaxCodeSummary','InvoiceValueExclTax',MntConst.eTypeCurrency,12);
                this.riGrid.AddColumnAlign('InvoiceValueExclTax',MntConst.eAlignmentRight);
                this.riGrid.AddColumn('InvoiceTax','TaxCodeSummary','InvoiceTax',MntConst.eTypeCurrency,12);
                this.riGrid.AddColumnAlign('InvoiceTax',MntConst.eAlignmentRight);
            } else {
                this.addTaxColumn();
                const currencyGrid = ['InvoiceTotalInclTax', 'InvoiceTotalExclTax', 'InvoiceTaxTotal', 'CreditTotalInclTax', 'CreditTotalExclTax',
                'CreditTaxTotal', 'NettTotalInclTax', 'NettTotalExclTax', 'NettTaxTotal'];
                currencyGrid.forEach(column => {
                    this.riGrid.AddColumn(column,'TaxCodeSummary',column,MntConst.eTypeCurrency,12);
                    this.riGrid.AddColumnAlign(column,MntConst.eAlignmentRight);
                });
                this.riGrid.AddColumn('TaxPercentage','TaxCodeSummary','TaxPercentage',MntConst.eTypeDecimal2,6);
                this.riGrid.AddColumnAlign('TaxPercentage',MntConst.eAlignmentRight);
            }
        this.riGrid.Complete();
    }

    private addTaxColumn(): void {
        this.riGrid.AddColumn('TaxCode','TaxCodeSummary','TaxCode',MntConst.eTypeText,12);
        this.riGrid.AddColumnAlign('TaxCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('TaxRate','TaxCodeSummary','TaxRate',MntConst.eTypeDecimal2,6);
        this.riGrid.AddColumnAlign('TaxRate',MntConst.eAlignmentRight);
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.pageParams.gridCacheRefresh = true;
        this.commonGridFunction.onRiGridRefresh();
    }

    public populateGrid(): void {
        if (this.uiForm.valid) {
            const queryParams: QueryParams = this.getURLSearchParamObject();
            queryParams.set(this.serviceConstants.Action, '2');
            const formData: Object = {};
            formData['ForYear'] = this.getControlValue('ForYear');
            formData['ForMonth'] = this.month.indexOf(this.getControlValue('ForMonth')) + 1;
            if (this.parentMode === 'Detail') {
                formData['Level'] = this.parentMode;
                formData['TaxCode'] = this.getControlValue('TaxCode');
                formData['TaxRate'] = this.getControlValue('TaxRate');
            }
            formData[this.serviceConstants.BusinessCode] = this.getControlValue('BusinessCode');
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.PageSize] = '10';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            formData[this.serviceConstants.GridCacheRefresh] =  this.pageParams.gridCacheRefresh;
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
            formData[this.serviceConstants.GridSortOrder] = this.pageParams.GridSortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, queryParams, formData).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.commonGridFunction.setPageData(data);
            }).catch(error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.pageParams.hasGridData = false;
                this.displayMessage(error);
            });
        }
    }

    public gridDblClick(): void {
        const columnName = this.riGrid.CurrentColumnName;
        const rowID = this.riGrid.Details.GetAttribute(columnName, 'rowID');
        if (rowID) {
            switch (columnName) {
                case 'NettTaxTotal':
                    this.setAttribute('TaxCode', this.riGrid.Details.GetValue('TaxCode'));
                    this.setAttribute('TaxRate', this.riGrid.Details.GetValue('TaxRate'));
                    this.navigate('Detail', BIReportsRoutes.ICABSARTAXCODESUMMARYDETAIL, { pageType: 'Detail'});
                    break;
                case 'ContractNumber':
                    this.setAttribute('RowID', rowID);
                    this.variableService.setContractStoreData({});
                    const additionalProperty = this.riGrid.Details.GetAttribute(columnName, 'AdditionalProperty');
                    let route;
                    switch (additionalProperty) {
                        case 'C': route = 'ICABSACONTRACTMAINTENANCE'; break;
                        case 'J': route = 'ICABSAJOBMAINTENANCE'; break;
                        case 'P': route = 'ICABSAPRODUCTSALEMAINTENANCE'; break;
                    }
                    this.navigate('InvoiceDetail', ContractManagementModuleRoutes[route], {
                        'currentContractTypeURLParameter': additionalProperty,
                        'ContractNumber': this.riGrid.Details.GetValue('ContractNumber'),
                        'parentMode': 'InvoiceDetail'
                    });
                    break;
            }
        }
    }
}
