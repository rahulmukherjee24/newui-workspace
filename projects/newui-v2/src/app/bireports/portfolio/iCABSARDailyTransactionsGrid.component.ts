import { AfterContentInit, OnInit, Component, Injector, OnDestroy, ViewChild } from '@angular/core';

import { BranchSearchComponent } from '../../../app/internal/search/iCABSBBranchSearch';
import { ContractManagementModuleRoutes } from '@base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { HistoryTypeLanguageSearchComponent } from '@internal/search/iCABSSHistoryTypeLanguageSearch.component';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { UserInformationSearchComponent } from '@internal/search/riMUserInformationSearch.component';

@Component({
    templateUrl: 'iCABSARDailyTransactionsGrid.html'
})

export class DailyTransactionGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public controls = [
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'BranchNumber', type: MntConst.eTypeInteger },
        { name: 'ContractTypeFilter', type: MntConst.eTypeText, value: '' },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true, value: new Date() },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true, value: new Date() },
        { name: 'HistoryTypeCode', type: MntConst.eTypeInteger, value: '' },
        { name: 'HistoryTypeDesc', type: MntConst.eTypeText, disabled: true, value: '' },
        { name: 'ReportTypeFilter', type: MntConst.eTypeText, value: 'All' },
        { name: 'ServiceOnlyView', type: MntConst.eTypeCheckBox },
        { name: 'UserCode', type: MntConst.eTypeText, value: '' },
        { name: 'UserName', type: MntConst.eTypeText, disabled: true, value: '' }
    ];
    public pageId: string = '';
    public alertMessage: any;
    public historyTypeSearchComponent: any = HistoryTypeLanguageSearchComponent;

    public inputParams: any = {
        branchParams: {
            'parentMode': 'LookUp'
        }
    };

    public gridConfig: any = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 1,
        action: '2'
    };


    public inputParamsHistoryCode: any = {
        'parentMode': 'Contract',
        'showAddNew': false
    };

    public ellipsis: any = {
        userInformation: {
            inputParams: {
                parentMode: 'Search',
                showAddNew: false,
                BranchNumber: ''
            },
            contentComponent: UserInformationSearchComponent
        }
    };

    public reportFilterValues: any = [
        { key: 'all', value: 'All' },
        { key: 'Finance', value: 'Finance' },
        { key: 'Sales', value: 'Sales' },
        { key: 'Service', value: 'Service' }

    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARDAILYTRANSACTIONSGRID;
        this.browserTitle = this.pageTitle = 'Daily Transactions';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private onWindowLoad(): void {
        this.branchSearchDropDown.active = {
            id: this.utils.getBranchCode(),
            text: this.utils.getBranchText()
        };
        this.setControlValue('BranchNumber', this.branchSearchDropDown.active.id);
        this.setControlValue('BranchName', this.branchSearchDropDown.active.text);
        if (this.isReturning()) {
            this.onRiGridRefresh();
        }
    }

    /* Code to Build and populate grid */
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        /* The following AddColumn records are only for those columns that will be shown on screen. Export-Only columns are not included*/
        this.riGrid.AddColumn('ProcessedDateTime', 'ContractHistory', 'ProcessedDateTime', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('HistoryTypeDesc', 'HistoryType', 'HistoryTypeDesc', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('ContractNumber', 'ContractHistory', 'ContractNumber', MntConst.eTypeCode, 11);
        this.riGrid.AddColumn('PremiseNumber', 'ContractHistory', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ProductCode', 'ContractHistory', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('OldServiceQuantity', 'ContractHistory', 'OldServiceQuantity', MntConst.eTypeText, 4);
        this.riGrid.AddColumn('NewServiceQuantity', 'ContractHistory', 'NewServiceQuantity', MntConst.eTypeText, 4);
        this.riGrid.AddColumn('OldVisitFrequency', 'ContractHistory', 'OldVisitFrequency', MntConst.eTypeText, 4);
        this.riGrid.AddColumn('NewVisitFrequency', 'ContractHistory', 'NewVisitFrequency', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('NewValue', 'ContractHistory', 'NewValue', MntConst.eTypeCurrency, 8, false);
        this.riGrid.AddColumn('ChangeValue', 'ContractHistory', 'ChangeValue', MntConst.eTypeCurrency, 8, false);
        this.riGrid.AddColumn('EmployeeCode', 'ContractHistory', 'EmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('ChargesLinked', 'ContractHistory', 'ChargesLinked', MntConst.eTypeImage, 1);
        this.riGrid.AddColumn('PaymentTypeCode', 'Contract', 'PaymentTypeCode', MntConst.eTypeText, 16);
        this.riGrid.AddColumn('BranchNumber', 'ContractHistory', 'BranchNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumn('HistoryEffectDate', 'ContractHistory', 'HistoryEffectDate', MntConst.eTypeDate, 13);
        this.riGrid.AddColumn('UserCode', 'ContractHistory', 'UserCode', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('InterCompany', 'ContractHistory', 'InterCompany', MntConst.eTypeImage, 1, false);
        this.riGrid.AddColumnOrderable('ProcessedDateTime', true);
        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('HistoryTypeDesc', true);
        this.riGrid.AddColumnOrderable('UserCode', true);
        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let dateFrom: string = this.getControlValue('DateFrom');
        let dateTo: string = this.getControlValue('DateTo');
        if (!dateFrom || !dateTo) {
            return;
        }
        this.buildGrid();
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());
        let form: any = {};
        form['BranchNumber'] = this.getControlValue('BranchNumber');
        form['DateFrom'] = dateFrom;
        form['DateTo'] = dateTo;
        form['ContractTypeFilter'] = this.getControlValue('ContractTypeFilter');
        form['ServiceOnlyView'] = this.getControlValue('ServiceOnlyView');
        form['HistoryTypeCode'] = this.getControlValue('HistoryTypeCode');
        form['User'] = this.getControlValue('UserCode');
        form['ReportTypeFilter'] = this.getControlValue('ReportTypeFilter');
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        form[this.serviceConstants.GridCacheRefresh] = true;
        form[this.serviceConstants.PageSize] = this.gridConfig.pageSize;
        form[this.serviceConstants.PageCurrent] = (this.pageParams.gridCurrentPage) ? this.pageParams.gridCurrentPage : this.gridConfig.currentPage;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest('bi/reports', 'reports', 'ApplicationReport/iCABSARDailyTransactionsGrid', gridSearch, form)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.riGrid.RefreshRequired();
                    this.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                    if (this.isReturning()) {
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 500);
                    }
                    this.riGrid.Execute(data);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    let msgTxt: string = error.errorMessage;
                    msgTxt += error.fullError ? ' - ' + error.fullError : '';
                    this.alertMessage = {
                        msg: msgTxt,
                        timestamp: (new Date()).getMilliseconds()
                    };
                });
    }

    private dolookUCallForUserCode(userCode: any): void {
        let lookupIP = [
            {
                'table': 'UserInformation',
                'query': {
                    'UserCode': userCode
                },
                'fields': ['UserName']
            }];
        this.LookUp.lookUpPromise(lookupIP).then((data) => {
            let UserInformation = data[0][0];
            if (UserInformation) {
                this.riExchange.riInputElement.SetValue(this.uiForm, 'UserName', UserInformation.UserName);
            }
        }).catch(() => {
            this.alertMessage = {
                msg: MessageConstant.Message.RecordNotFound,
                timestamp: (new Date()).getMilliseconds()
            };
        });
    }

    public onRiGridRefresh(): void {
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    public onGridBodyDoubleClick(event: any): void {
        let routeValues: any = this.riGrid.Details.GetValue('ContractNumber').split('/');
        switch (routeValues[0]) {
            case 'C':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                    'CurrentContractTypeURLParameter': routeValues[0],
                    'ContractNumber': routeValues[1]
                });
                break;
            case 'J':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE, {
                    'CurrentContractTypeURLParameter': routeValues[0],
                    'ContractNumber': routeValues[1],
                    'currentContractType': routeValues[0]
                });
                break;
            case 'P':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE, {
                    'CurrentContractTypeURLParameter': routeValues[0],
                    'ContractNumber': routeValues[1],
                    'currentContractType': routeValues[0]
                });
                break;
            case 'A':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSAACCOUNTMAINTENANCE, {
                    'parentMode': 'DailyTransactions',
                    'AccountNumber': routeValues[1]
                });
                break;
        }
    }

    /* Code for History Type Code and History Type Description Fetch*/

    public onSelectHistoryType(data: any): void {
        this.setControlValue('HistoryTypeCode', data.HistoryTypeCode);
        this.setControlValue('HistoryTypeDesc', data.HistoryTypeDesc);
        this.markControlAsDirty('HistoryTypeCode');
    }

    public onChangeHistoryTypeData(): void {
        let historyTypeCode: string = this.getControlValue('HistoryTypeCode');
        if (!historyTypeCode) {
            this.setControlValue('HistoryTypeDesc', '');
            return;
        }
        let lookupIP = [{
            'table': 'HistoryTypeLang',
            'query': { 'BusinessCode': this.utils.getBusinessCode(), 'LanguageCode': this.riExchange.LanguageCode(), 'HistoryTypeCode': historyTypeCode },
            'fields': ['HistoryTypeCode', 'HistoryTypeDesc']
        }];
        this.LookUp.lookUpPromise(lookupIP).then((data) => {
            if (data && data[0] && data[0].length) {
                this.setControlValue('HistoryTypeCode', data[0][0].HistoryTypeCode);
                this.setControlValue('HistoryTypeDesc', data[0][0].HistoryTypeDesc);
            } else {
                this.alertMessage = {
                    msg: MessageConstant.Message.RecordNotFound,
                    timestamp: (new Date()).getMilliseconds()
                };
            }
        }).catch(() => {
            this.alertMessage = {
                msg: MessageConstant.Message.RecordNotFound,
                timestamp: (new Date()).getMilliseconds()
            };
        });
    }

    /*Code for User Code and User Description Fetch starts*/
    public onUserCodeChange(): void {
        this.setControlValue('UserName', '');
        if (!this.getControlValue('UserCode')) {
            this.setControlValue('UserName', '');
        } else {
            this.dolookUCallForUserCode(this.getControlValue('UserCode'));
        }

    }

    public onUserInformationOnDataRecieved(data: Object): void {
        if (data) {
            this.setControlValue('UserCode', data['UserCode']);
            this.setControlValue('UserName', data['UserName']);
            this.markControlAsDirty('UserCode');
        }
    }

    /* Code for User Code and User Description Fetch ends*/

    public onChangeReportTypeFilter(): void {
        this.setControlValue('HistoryTypeCode', '');
    }

}
