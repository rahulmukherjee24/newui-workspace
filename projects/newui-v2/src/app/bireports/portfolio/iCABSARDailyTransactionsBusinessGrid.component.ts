import { AfterContentInit, OnInit, Component, Injector, OnDestroy, ViewChild } from '@angular/core';
import { CommonGridFunction } from '@base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { ContractManagementModuleRoutes } from '@base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { HistoryTypeLanguageSearchComponent } from '@internal/search/iCABSSHistoryTypeLanguageSearch.component';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { UserInformationSearchComponent } from '@internal/search/riMUserInformationSearch.component';

@Component({
    templateUrl: 'iCABSARDailyTransactionsBusinessGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class DailyTransactionBusinessGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public commonGridFunction: CommonGridFunction;
    public pageId: string = '';

    public controls: IControls[] = [
        { name: 'ContractTypeFilter', type: MntConst.eTypeText },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true, value: new Date() },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true, value: new Date() },
        { name: 'HistoryTypeCode', type: MntConst.eTypeInteger },
        { name: 'HistoryTypeDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ReportTypeFilter', type: MntConst.eTypeText, value: 'All' },
        { name: 'UserCode', type: MntConst.eTypeText },
        { name: 'UserName', type: MntConst.eTypeText, disabled: true }
    ];

    public inputParams: Record<string, Object> = {
        branchParams: {
            'parentMode': 'LookUp'
        }
    };

    private headerParams: IXHRParams = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARDailyTransactionsGridBusiness'
    };

    public ellipsis: Record<string, Record<string, Object>> = {
        userInformation: {
            inputParams: {
                parentMode: 'LookUp',
                showAddNew: false,
                BranchNumber: ''
            },
            contentComponent: UserInformationSearchComponent
        },
        historyInformation: {
            inputParams: {
                parentMode: 'Contract',
                showAddNew: false
            },
            contentComponent: HistoryTypeLanguageSearchComponent
        }
    };

    public reportFilterValues: any = [
        { key: 'All', value: 'All' },
        { key: 'Finance', value: 'Finance' },
        { key: 'Sales', value: 'Sales' },
        { key: 'Service', value: 'Service' }
    ];
    public contractTypeFilterValues: any = [
        { key: 'C', value: 'Contracts' },
        { key: 'J', value: 'Jobs' },
        { key: 'P', value: 'Product Sales' }
    ];

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSARDAILYTRANSACTIONSBUSINESSGRID;
        this.browserTitle = this.pageTitle = 'Daily Transactions';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.buildGrid();
        this.pageParams.gridConfig = {
            itemsPerPage: 10,
            gridHandle: this.utils.randomSixDigitString(),
            totalItem: 1
        };
        if (this.isReturning()) {
            this.riGrid.HeaderClickedColumn = this.pageParams.headerGirdColumn || '';
            this.riGrid.DescendingSort = this.pageParams.GridSortOrder === 'Descending';
            this.commonGridFunction.onRefreshClick();
        } else {
            this.pageParams.currentPage = 1;
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /* Code to Build and populate grid */
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        /* The following AddColumn records are only for those columns that will be shown on screen. Export-Only columns are not included*/
        this.riGrid.AddColumn('ProcessedDateTime', 'ContractHistory', 'ProcessedDateTime', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('HistoryTypeDesc', 'HistoryType', 'HistoryTypeDesc', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('ContractNumber', 'ContractHistory', 'ContractNumber', MntConst.eTypeCode, 11);
        this.riGrid.AddColumn('PremiseNumber', 'ContractHistory', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ProductCode', 'ContractHistory', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('OldServiceQuantity', 'ContractHistory', 'OldServiceQuantity', MntConst.eTypeText, 4);
        this.riGrid.AddColumn('NewServiceQuantity', 'ContractHistory', 'NewServiceQuantity', MntConst.eTypeText, 4);
        this.riGrid.AddColumn('OldVisitFrequency', 'ContractHistory', 'OldVisitFrequency', MntConst.eTypeText, 4);
        this.riGrid.AddColumn('NewVisitFrequency', 'ContractHistory', 'NewVisitFrequency', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('NewValue', 'ContractHistory', 'NewValue', MntConst.eTypeCurrency, 8, false);
        this.riGrid.AddColumn('ChangeValue', 'ContractHistory', 'ChangeValue', MntConst.eTypeCurrency, 8, false);
        this.riGrid.AddColumn('EmployeeCode', 'ContractHistory', 'EmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('ChargesLinked', 'ContractHistory', 'ChargesLinked', MntConst.eTypeImage, 1);
        this.riGrid.AddColumn('PaymentTypeCode', 'Contract', 'PaymentTypeCode', MntConst.eTypeText, 16);
        this.riGrid.AddColumn('BranchNumber', 'ContractHistory', 'BranchNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumn('HistoryEffectDate', 'ContractHistory', 'HistoryEffectDate', MntConst.eTypeDate, 13);
        this.riGrid.AddColumn('UserCode', 'ContractHistory', 'UserCode', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('InterCompany', 'ContractHistory', 'InterCompany', MntConst.eTypeImage, 1, false);
        this.riGrid.AddColumnOrderable('ProcessedDateTime', true);
        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('HistoryTypeDesc', true);
        this.riGrid.AddColumnOrderable('UserCode', true);
        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        const dateFrom: string = this.getControlValue('DateFrom');
        const dateTo: string = this.getControlValue('DateTo');
        const gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, '2');
        const form: any = {};
        form['BusinessCode'] = this.utils.getBusinessCode();
        form['DateFrom'] = dateFrom;
        form['DateTo'] = dateTo;
        if (this.getControlValue('ContractTypeFilter') === 'All') {
            form['ContractTypeFilter'] = '';
        } else {
            form['ContractTypeFilter'] = this.getControlValue('ContractTypeFilter');
        }
        form['HistoryTypeCode'] = this.getControlValue('HistoryTypeCode');
        form['User'] = this.getControlValue('UserCode');
        form['ReportTypeFilter'] = this.getControlValue('ReportTypeFilter');
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        form[this.serviceConstants.GridCacheRefresh] = true;
        form[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage || 1;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, gridSearch, form).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.commonGridFunction.setPageData(data, this.pageParams.gridConfig);
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.hasGridData = false;
            this.displayMessage(error);

        });
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    public onDataRecieved(data: any, type: string): void {
        switch (type) {
            case 'UserCode':
                this.setControlValue('UserCode', data['UserCode']);
                this.setControlValue('UserName', data['UserName']);
                this.markControlAsDirty('UserCode');
                break;
            case 'HistoryTypeCode':
                this.setControlValue('HistoryTypeCode', data.HistoryTypeCode);
                this.setControlValue('HistoryTypeDesc', data.HistoryTypeDesc);
                this.markControlAsDirty('HistoryTypeCode');
        }
    }
    public onChange(type: string): void {
        switch (type) {
            case 'UserCode':
                const userCode = this.getControlValue('UserCode');
                if (!this.getControlValue('UserCode')) {
                    this.setControlValue('UserName', '');
                } else {
                    this.commonLookup.getUserName(userCode).then((data) => {
                        if (data[0][0]) {
                            this.isRequesting = false;
                            this.setControlValue('UserName', data[0][0].UserName);
                        }
                        else
                            this.setControlValue('UserName', '');
                    });
                }
                break;
            case 'HistoryTypeCode':
                const historyTypeCode: string = this.getControlValue('HistoryTypeCode');
                if (!historyTypeCode) {
                    this.setControlValue('HistoryTypeDesc', '');
                    return;
                }
                this.commonLookup.getHistorydetails(historyTypeCode).then((data) => {
                    if (data[0][0]) {
                        this.isRequesting = false;
                        this.setControlValue('HistoryTypeDesc', data[0][0].HistoryTypeDesc);
                    }
                    else
                        this.setControlValue('HistoryTypeDesc', '');
                });
                break;
            case 'ReportTypeFilter':
                this.setControlValue('HistoryTypeCode', '');
                break;
        }
    }

    public onGridBodyDoubleClick(event: any): void {
        const routeValues: any = this.riGrid.Details.GetValue('ContractNumber').split('/');
        switch (routeValues[0]) {
            case 'C':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                    'CurrentContractTypeURLParameter': routeValues[0],
                    'ContractNumber': routeValues[1]
                });
                break;
            case 'J':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE, {
                    'CurrentContractTypeURLParameter': routeValues[0],
                    'ContractNumber': routeValues[1],
                    'currentContractType': routeValues[0]
                });
                break;
            case 'P':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE, {
                    'CurrentContractTypeURLParameter': routeValues[0],
                    'ContractNumber': routeValues[1],
                    'currentContractType': routeValues[0]
                });
                break;
            case 'A':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSAACCOUNTMAINTENANCE, {
                    'parentMode': 'DailyTransactions',
                    'AccountNumber': routeValues[1]
                });
                break;
        }
    }
    public onHeaderClick(): void {
        this.pageParams.headerGirdColumn = this.riGrid.HeaderClickedColumn;
        this.pageParams.GridSortOrder = this.riGrid.SortOrder;
        this.onRiGridRefresh();
    }
}
