import { OnInit, Component, Injector, OnDestroy, ViewChild, AfterContentInit } from '@angular/core';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { EllipsisComponent } from '@shared/components/ellipsis/ellipsis';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';

@Component({
    templateUrl: 'iCABSARPortfolioReport.html',
    providers: [CommonLookUpUtilsService]
})

export class PortfolioReportComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('employeeSearchEllipsis') employeeSearchEllipsis: EllipsisComponent;
    @ViewChild('contractOwner') public contractOwner;

    private muleConfig = {
        method: 'people/admin',
        module: 'employee',
        operation: 'Business/iCABSBEmployeeMaintenance',
        contentType: 'application/x-www-form-urlencoded'
    };
    private sysCharConstants: SysCharConstants;
    private SystemCharEnableInstallsRemovals: boolean = false;
    private SystemCharEnablePestNetOnlineProcessing: boolean = false;
    private xhrParams: any = {
        method: 'bi/reports',
        moduleAPI: 'reports',
        operation: 'ApplicationReport/iCABSARPortfolioReportBusiness'
    };

    public isBranch: boolean = false;
    public isRegion: boolean = false;

    public companyDefault: Object = {
        id: '',
        text: ''
    };
    public companyInputParams: any = {};
    public controls: Array<Object> = [
        { name: 'AtDate', type: MntConst.eTypeDate, required: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'BusinessDesc', type: MntConst.eTypeCode, required: true, disabled: true },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchName', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'CompanyCode', type: MntConst.eTypeText },
        { name: 'CompanyCodeDetail', type: MntConst.eTypeText, disabled: true },
        { name: 'CompanyDesc', type: MntConst.eTypeText },
        { name: 'ContractOwner', type: MntConst.eTypeText },
        { name: 'ContractOwnerSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractTypeCode', type: MntConst.eTypeText },
        { name: 'CountLevel' },
        { name: 'GroupAccountNumber' },
        { name: 'GroupBy' },
        { name: 'GroupName' },
        { name: 'IncludeZeroValue', type: MntConst.eTypeCheckBox, value: false },
        { name: 'NumberOfLines', type: MntConst.eTypeInteger, value: '20' },
        { name: 'GroupCode' }
    ];
    public contractTypeCodeValues: any = [
        { text: 'Contracts', value: 'C' },
        { text: 'Jobs', value: 'J' }
    ];
    public countLevelValues: any = [
        { text: 'Show All Counts', value: 'All' },
        { text: 'Show Contract/Job Count', value: 'Contract' },
        { text: 'Show Premises Count', value: 'Premise' },
        { text: 'Show Service Cover Count', value: 'ServiceCover' }
    ];
    public displayPagination: boolean = false;
    public ellipsisConfig = {
        employeeSearch: {
            autoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp-ContractOwner',
                'currentContractType': '',
                'currentContractTypeURLParameter': '',
                'showAddNew': false,
                'countryCode': '',
                'businessCode': '',
                'negativeBranchNumber': '',
                'negBranchNumber': '',
                'serviceBranchNumber': '',
                'branchNumber': '',
                'salesBranchNumber': '',
                'OccupationCode': '',
                'NewServiceBranchNumber': '',
                'NewNegBranchNumber': '',
                action: 0
            },
            modalConfig: '',
            contentComponent: EmployeeSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        }
    };
    public groupByValues: any = [];
    public gridConfig: any = {
        pageSize: 20,
        action: '2',
        totalRecords: 1,
        riSortOrder: 'Decending'
    };
    public isFirstItemSelected: boolean = true;
    public pageId: string = '';
    public SystemCharContractOwner: boolean = false;
    public SystemCharEnableCompanyCode: boolean = false;

    constructor(injector: Injector, private commonLookupUtil: CommonLookUpUtilsService) {
        super(injector);
        this.browserTitle = this.pageTitle = MessageConstant.PageSpecificMessage.portfolioByBusiness.pageTitle;
        this.sysCharConstants = injector.get(SysCharConstants);

        this.companyInputParams[this.serviceConstants.CountryCode] = this.utils.getCountryCode();
        this.companyInputParams[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        this.companyInputParams['parentMode'] = 'LookUp';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.isBranch = this.riExchange.getParentHTMLValue('type') === 'branch';
        this.isRegion = this.riExchange.getParentHTMLValue('type') === 'region';
        if (this.isBranch || this.isRegion) {
            this.pageId = PageIdentifier['ICABSARPORTFOLIOREPORT' + this.riExchange.getParentHTMLValue('type').toUpperCase()];
        } else {
            this.pageId = PageIdentifier.ICABSARPORTFOLIOREPORTBUSINESS;
        }
        super.ngAfterContentInit();
        if (this.isBranch) {
            this.pageTitle = 'Branch Portfolio';
            this.utils.setTitle(this.pageTitle);
            this.groupByValues = [
                { text: 'By Anniversary Month', value: 'Anniversary' },
                { text: 'By Company', value: 'Company' },
                { text: 'By Contract/Job', value: 'Contract' },
                { text: 'By Customer Type', value: 'CustomerType' },
                { text: 'By Expense Code', value: 'ExpenseCode' },
                { text: 'By Invoice Frequency', value: 'InvoiceFrequency' },
                { text: 'By Invoice Type', value: 'InvoiceType' },
                { text: 'By Market Segment', value: 'MarketSegment' },
                { text: 'By Payment Method', value: 'Payment' },
                { text: 'By Postcode', value: 'Postcode' },
                { text: 'By Product Sales Group', value: 'ProductSalesGroup' },
                { text: 'By Product Service Group', value: 'ProductGroup' },
                { text: 'By Product', value: 'Product' },
                { text: 'By RMM Category', value: 'RMMCategory' },
                { text: 'By Sales Area', value: 'SalesArea' },
                { text: 'By Service Area', value: 'ServiceArea' },
                { text: 'By Service Type', value: 'ServiceType' },
                { text: 'By Town', value: 'Town' },
                { text: 'By Visit Frequency', value: 'VisitFrequency' }
            ];

        } else if (this.isRegion) {
            this.pageTitle = 'Region Portfolio';
            this.utils.setTitle(this.pageTitle);
            this.groupByValues = [
                { value: 'Company' , text: 'By Company' },
                { value: 'Branch' , text: 'By Servicing Branch' },
                { value: 'NegBranch' , text: 'By Negotiating Branch' },
                { value: 'NegEmployee' , text: 'By Negotiating Employee' },
                { value: 'Contract' , text: 'By Contract/Job' },
                { value: 'Anniversary' , text: 'By Anniversary Month' },
                { value: 'InvoiceFrequency' , text: 'By Invoice Frequency' },
                { value: 'Payment' , text: 'By Payment Method' },
                { value: 'Town' , text: 'By Town' },
                { value: 'Postcode' , text: 'By Postcode' },
                { value: 'CustomerType' , text: 'By Customer Type' },
                { value: 'MarketSegment' , text: 'By Market Segment' },
                { value: 'SalesArea' , text: 'By Sales Area' },
                { value: 'ServiceArea' , text: 'By Service Area' },
                { value: 'VisitFrequency' , text: 'By Visit Frequency' },
                { value: 'Product' , text: 'By Product' },
                { value: 'ProductGroup' , text: 'By Product Service Group' },
                { value: 'ProductSalesGroup' , text: 'By Product Sales Group' },
                { value: 'InvoiceType' , text: 'By Invoice Type' },
                { value: 'ServiceType' , text: 'By Service Type' },
                { value: 'ExpenseCode' , text: 'By Expense Code' },
                { value: 'RMMCategory' , text: 'By RMM Category' }
            ];
        } else {
            this.groupByValues = [
                { text: 'By Account Number', value: 'Account' },
                { text: 'By Anniversary Month', value: 'Anniversary' },
                { text: 'By Company', value: 'Company' },
                { text: 'By Contract/Job', value: 'Contract' },
                { text: 'By Customer Type', value: 'CustomerType' },
                { text: 'By Expense Code', value: 'ExpenseCode' },
                { text: 'By Group Account Number', value: 'GroupAccount' },
                { text: 'By Invoice Frequency', value: 'InvoiceFrequency' },
                { text: 'By Invoice Type', value: 'InvoiceType' },
                { text: 'By Market Segment', value: 'MarketSegment' },
                { text: 'By Negotiating Branch', value: 'NegBranch' },
                { text: 'By Negotiating Employee', value: 'NegEmployee' },
                { text: 'By Payment Method', value: 'Payment' },
                { text: 'By Postcode', value: 'Postcode' },
                { text: 'By Product Sales Group', value: 'ProductSalesGroup' },
                { text: 'By Product Service Group', value: 'ProductGroup' },
                { text: 'By Product', value: 'Product' },
                { text: 'By RMM Category', value: 'RMMCategory' },
                { text: 'By Region', value: 'Region' },
                { text: 'By Sales Area', value: 'SalesArea' },
                { text: 'By Service Area', value: 'ServiceArea' },
                { text: 'By Service Type', value: 'ServiceType' },
                { text: 'By Servicing Branch', value: 'Branch' },
                { text: 'By Town', value: 'Town' },
                { text: 'By Visit Frequency', value: 'VisitFrequency' }
            ];
        }
        this.onWindowLoad();
        if (this.isReturning()) {
            if (this.getControlValue('CompanyCode')) {
                this.onCompanyChange({
                    'CompanyCode': this.getControlValue('CompanyCode'),
                    'CompanyDesc': this.getControlValue('CompanyDesc')
                });
            }
            this.pageParams.gridCacheRefresh = false;
            this.isRequesting = true;
            this.onRiGridRefresh();
        } else {
            this.pageParams.gridCacheRefresh = true;
            this.setControlValue('BusinessCode', this.utils.getBusinessCode());
            this.setControlValue('BusinessDesc', this.utils.getBusinessText(this.utils.getBusinessCode()));
            this.setControlValue('ContractTypeCode', this.contractTypeCodeValues[0].value);
            this.setControlValue('GroupBy', this.isBranch ? 'Company' : 'Branch');
            this.setControlValue('CountLevel', this.countLevelValues[2].value);
            this.gridConfig.pageSize = this.getControlValue('NumberOfLines') || 20;
            this.lookUPforBusiness();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private lookUPforBusiness(): void {
        let lookupIP = [
            {
                'table': 'Business',
                'query': {
                    'BusinessCode': this.businessCode()
                },
                'fields': ['SalesTradingYear', 'SalesTradingMonth']
            }
        ];
        this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data[0] && data[0].length) {
                const atDate: Date = new Date(data[0][0].SalesTradingYear, data[0][0].SalesTradingMonth, 0);
                this.setControlValue('AtDate', this.globalize.parseDateToFixedFormat(atDate) as string);

            } else {
                this.setControlValue('AtDate', '');
            }
        });
    }

    private onWindowLoad(): void {
        this.pageParams.gridHandle = this.pageParams.gridHandle || this.utils.randomSixDigitString();
        this.checkSysCharsStatus('SystemCharEnableInstallsRemovals'); // 190
        this.checkSysCharsStatus('SystemCharContractOwner'); // 3510
        this.checkSysCharsStatus('SystemCharEnableCompanyCode'); // 130
        this.checkSysCharsStatus('SystemCharEnablePestNetOnlineProcessing'); // 1370
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchTextOnly());
        if (this.isRegion) {
            this.commonLookupUtil.getRegionDesc().subscribe((data) => {
                if (data && data[0] && data[0][0]) {
                    this.setControlValue('RegionCode', data[0][0]['RegionCode']);
                    this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
                } else {
                    this.displayMessage(MessageConstant.Message.RecordNotFound, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                }
            });
        }
    }

    private checkSysCharsStatus(forSysChar: any): void {
        let querySysChar: QueryParams = new QueryParams();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        querySysChar.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        querySysChar.set(this.serviceConstants.SystemCharNumber, this.sysCharConstants[forSysChar]);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.sysCharRequest(querySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                this[forSysChar] = data['records'][0].Required || false;
                if (this[forSysChar] && forSysChar === 'SystemCharEnablePestNetOnlineProcessing') { this.groupByValues.splice(this.isBranch || this.isRegion ? 9 : 13, 0, { text: 'By PestNetOnline Level', value: 'PestNetLevel' }); }
                else if (this[forSysChar] && forSysChar === 'SystemCharEnableCompanyCode' && this.isRegion && this.parentMode) {
                    this.onCompanyChange({
                        'CompanyCode': this.riExchange.getParentHTMLValue('CompanyCode'),
                        'CompanyDesc': this.riExchange.getParentHTMLValue('CompanyDesc')
                    });
                }
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage(error);
        });
    }

    public onChangeControls(): void {
        this.pageParams.gridCurrentPage = 1;
        this.gridConfig.totalRecords = 0;
        this.riGrid.Clear();
        this.riGrid.RefreshRequired();
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        const contractLevel: string = this.getControlValue('ContractTypeCode').toLowerCase();
        const groupByValues: string = this.getControlValue('GroupBy').toLowerCase();
        const countLevelValues: string = this.getControlValue('CountLevel').toLowerCase();
        const groupByCommonList = ['region','negbranch','branch','company','contract','anniversary','customertype','visitfrequency','invoicetype','rmmcategory','servicetype','pestnetlevel'];
        this.riGrid.AddColumn('GroupCode', 'Portfolio', 'GroupCode', MntConst.eTypeText, 8);
        if (groupByCommonList.includes(groupByValues)) {
            this.riGrid.AddColumnAlign('GroupCode', MntConst.eAlignmentCenter);
        }
        if ((groupByValues === 'servicearea' || groupByValues === 'salesarea') && !this.isBranch) {
            this.riGrid.AddColumn('AreaBranchNumber', 'Portfolio', 'AreaBranchNumber', MntConst.eTypeText, 2);
            this.riGrid.AddColumnAlign('AreaBranchNumber', MntConst.eAlignmentCenter);
            if (!this.isRegion) {
                this.riGrid.AddColumnAlign('NumOfPremises', MntConst.eAlignmentRight);
                this.riGrid.AddColumnAlign('GroupQty', MntConst.eAlignmentRight);
                this.riGrid.AddColumnAlign('GroupExchanges', MntConst.eAlignmentRight);
                this.riGrid.AddColumnAlign('PercentageOfTotal', MntConst.eAlignmentRight);
            }
        }
        if (groupByValues !== 'postcode' && groupByValues !== 'visitfrequency' && groupByValues !== 'town') {
            this.riGrid.AddColumn('GroupDesc', 'Portfolio', 'GroupDesc', MntConst.eTypeText, 20);
        }
        if (groupByValues === 'contract') {
            this.riGrid.AddColumn('AccountNumber', 'Portfolio', 'AccountNumber', MntConst.eTypeText, 20);
        }
        this.riGrid.AddColumn('GroupValue', 'Portfolio', 'GroupValue', MntConst.eTypeDecimal2, 10);
        this.riGrid.AddColumnAlign('GroupValue', MntConst.eAlignmentRight);

        if (countLevelValues === 'all') {
            this.riGrid.AddColumn('NumOfContracts', 'Portfolio', 'NumOfContracts', MntConst.eTypeInteger, 8);
            this.riGrid.AddColumnAlign('NumOfContracts', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('AverageContractValue', 'Portfolio', 'AverageContractValue', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumnAlign('AverageContractValue', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('NumOfPremises', 'Portfolio', 'NumOfPremises', MntConst.eTypeInteger, 8);
            this.riGrid.AddColumnAlign('NumOfPremises', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('AveragePremiseValue', 'Portfolio', 'AveragePremiseValue', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumnAlign('AveragePremiseValue', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('NumOfServiceCovers', 'Portfolio', 'NumOfServiceCovers', MntConst.eTypeInteger, 8);
            this.riGrid.AddColumnAlign('NumOfServiceCovers', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('AverageServiceCoverValue', 'Portfolio', 'AverageServiceCoverValue', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumnAlign('AverageServiceCoverValue', MntConst.eAlignmentRight);
        } else {
            this.riGrid.AddColumn('NumOfPremises', 'Portfolio', 'NumOfPremises', MntConst.eTypeInteger, 8);
            this.riGrid.AddColumnAlign('NumOfPremises', MntConst.eAlignmentRight);

            this.riGrid.AddColumn('AverageValue', 'Portfolio', 'AverageValue', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumnAlign('AverageValue', MntConst.eAlignmentRight);
        }

        this.riGrid.AddColumn('GroupQty', 'Portfolio', 'GroupQty', MntConst.eTypeInteger, 8);
        this.riGrid.AddColumnAlign('GroupQty', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('GroupAvgValue', 'Portfolio', 'GroupAvgValue', MntConst.eTypeDecimal2, 10);
        this.riGrid.AddColumnAlign('GroupAvgValue', MntConst.eAlignmentRight);

        if (this.SystemCharEnableInstallsRemovals) {
            this.riGrid.AddColumn('GroupExchanges', 'Portfolio', 'GroupExchanges', MntConst.eTypeInteger, 8);
            this.riGrid.AddColumnAlign('GroupExchanges', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('GroupAvgExValue', 'Portfolio', 'GroupAvgExValue', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumnAlign('GroupAvgExValue', MntConst.eAlignmentRight);
        }

        this.riGrid.AddColumn('PercentageOfTotal', 'Portfolio', 'PercentageOfTotal', MntConst.eTypeDecimal2, 5);
        this.riGrid.AddColumnAlign('PercentageOfTotal', MntConst.eAlignmentRight);

        if (groupByValues === 'contract') {
            this.riGrid.AddColumn('NegBranchNumber', 'Portfolio', 'NegBranchNumber', MntConst.eTypeInteger, 8);
            this.riGrid.AddColumnAlign('NegBranchNumber', MntConst.eAlignmentCenter);
        }
        this.riGrid.Complete();
    }

    public populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());
        let formData: any = {
            'AtDate': this.getControlValue('AtDate'),
            'businessCode': this.utils.getBusinessCode(),
            'BranchNumber': this.getControlValue('BranchNumber'),
            'CompanyCode': this.getControlValue('CompanyCode'),
            'ContractOwner': this.getControlValue('ContractOwner'),
            'ContractTypeCode': this.getControlValue('ContractTypeCode'),
            'CountLevel': this.getControlValue('CountLevel'),
            'countryCode': this.utils.getCountryCode(),
            'Function': 'Portfolio',
            'GroupBy': this.getControlValue('GroupBy'),
            'IncludeZeroValue': this.getControlValue('IncludeZeroValue') ? 'True' : 'False',
            'InstallationReq': this.SystemCharEnableInstallsRemovals,
            'Level': this.isBranch || this.isRegion ? this.utils.capitalizeFirstLetter(this.riExchange.getParentHTMLValue('type')) : 'Business',
            'PageCurrent': this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1,
            'PageSize': this.getControlValue('NumberOfLines') || this.gridConfig.pageSize,
            'riSortOrder': this.gridConfig.riSortOrder
        };
        if (this.isRegion) {
            formData = {
                ...formData,
                'RegionCode': this.getControlValue('RegionCode')
            };
            delete formData['BranchNumber'];
        }
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.GridMode] = '0';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams['method'], this.xhrParams['moduleAPI'], this.xhrParams['operation'], gridSearch, formData)
            .subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.displayMessage(data);
                } else {
                    this.riGrid.RefreshRequired();

                    this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                    this.displayPagination = true;
                    this.riGrid.Execute(data);
                    setTimeout(() => {
                        this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                    }, 100);
                }
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.displayPagination = false;
                    this.displayMessage(error);
                });
    }

    private fetchOwnerDetailsOnchange(empCode: any): void {
        let queryParams = new QueryParams();
        queryParams.set(this.serviceConstants.Action, '0');
        queryParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        queryParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        queryParams.set(this.serviceConstants.EmployeeCode, empCode);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryParams)
            .subscribe((response: any) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (!response || response['errorMessage']) {
                    this.displayMessage(response);
                    this.setControlValue('ContractOwner', '');
                    this.setControlValue('ContractOwnerSurname', '');
                    this.contractOwner.nativeElement.focus();
                    return;
                }
                const data: any = {
                    EmployeeCode: response.EmployeeCode,
                    EmployeeSurname: response.EmployeeSurname
                };
                this.onEmployeeSearchDataReceived(data);
            },
                (error) => {
                    //handle error
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.displayMessage(error);
                    this.setControlValue('ContractOwner', '');
                    this.setControlValue('ContractOwnerSurname', '');
                    this.contractOwner.nativeElement.focus();
                });

    }

    public onEmployeeSearchDataReceived(data: any): void {
        if (data && data['ContractOwner']) {
            this.setControlValue('ContractOwner', data['ContractOwner']);
            this.setControlValue('ContractOwnerSurname', data['ContractOwnerSurname']);
        }
    }

    public modalHiddenEmployeeSearch(): void { }

    public onRiGridRefresh(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        this.pageParams.gridCacheRefresh = false;
        this.buildGrid();
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridCacheRefresh = false;
            super.getCurrentPage(currentPage);
        }
    }

    public onGridBodyDoubleClick(): void {
        const groupBy: string = this.getControlValue('GroupBy').toLowerCase();
        const groupCode: string = this.riGrid.Details.GetAttribute('GroupCode', 'AdditionalProperty').toLowerCase();

        let queryParams: any = {};
        if (!this.isBranch && !this.isRegion) {
            if (groupBy === 'servicearea' || groupBy === 'salearea') {
                queryParams['BusinessCodeBranchNumber'] = this.riGrid.Details.GetValue('AreaBranchNumber');
            }
            this.setControlValue('GroupCode', (groupCode === 'total') ? 'Totals' : this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName));
            for (let control in this.uiForm.controls) {
                if (true)
                    queryParams[control] = this.getControlValue(control);
            }
            queryParams['BusinessCodeGroupCode'] = this.getControlValue('GroupCode');
            if (this.getControlValue('GroupBy') === 'GroupAccount') {
                // riExchange.Mode = "Business"
                // GroupAccountNumber.value = objTR.children(0).innerText
                // GroupName.value = objTR.children(1).innerText
                // window.location = "/wsscripts/riHTMLWrapper.p?riFileName=ApplicationReport/iCABSARPortfolioReportGroupAccount.htm"
            } else {
                this.navigate('Business', BIReportsRoutes.ICABSARPORTFOLIOREPORTDETAIL, queryParams);
            }
        } else if (this.isRegion) {
            if (this.riGrid.CurrentColumnName === 'GroupCode') {
                if (groupBy !== 'company' && groupCode !== 'total') {
                    this.setAttribute('RegionCodeGroupCode', this.riGrid.Details.GetValue('GroupCode'));
                    this.setAttribute('RegionCodeBranchNumber', this.riGrid.Details.GetValue('AreaBranchNumber'));
                    this.navigate('Region', BIReportsRoutes.ICABSARPORTFOLIOREPORTDETAIL);
                } else if (groupCode !== 'total') {
                    this.navigate('Region', BIReportsRoutes.ICABSARPORTFOLIOREPORTDETAIL);
                }
            }
        } else {
            queryParams['BranchNumberGroupCode'] = this.riGrid.Details.GetValue('GroupCode');
            if (groupBy !== 'company') {
                if (groupBy === 'expensecode') {
                    // if (groupCode === 'TOTAL')
                    this.navigate(groupCode === 'total' ? 'BranchTotal' : 'Branch', BIReportsRoutes.ICABSARPORTFOLIOREPORTDETAIL, queryParams);
                } else {
                    if (groupCode !== 'total') {
                        this.navigate('Branch', BIReportsRoutes.ICABSARPORTFOLIOREPORTDETAIL, queryParams);
                    }
                }
            } else {
                if (groupCode !== 'total')
                    this.navigate('Branch', BIReportsRoutes.ICABSARPORTFOLIOREPORTDETAIL, queryParams);
            }
        }
    }

    public getFieldDetails(e: any): void {
        if (e && e.target && e.target.value) {
            this.fetchOwnerDetailsOnchange(e.target.value);
        }
    }

    public onCompanyChange(data: any): void {
        this.setControlValue('CompanyCode', data['CompanyCode']);
        this.setControlValue('CompanyDesc', data['CompanyDesc']);
        this.companyDefault = {
            id: this.getControlValue('CompanyCode'),  text: this.getControlValue('CompanyDesc')
        };
    }

}
