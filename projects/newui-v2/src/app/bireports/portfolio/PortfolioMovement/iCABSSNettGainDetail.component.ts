import { Component, OnInit, AfterContentInit, Injector, ViewChild } from '@angular/core';

import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';


@Component({
    templateUrl: 'iCABSSNettGainDetail.html'
})

export class NettGainDetailComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private BRANCH: string = 'BRANCH';
    private REGION: string = 'REGION';
    private MARKET_SEGMENT: string = 'MARKET SEGMENT';
    private NEGOTIATING_EMPLOYEE: string = 'NEGOTIATING EMPLOYEE';
    private SALES_EMPLOYEE: string = 'SALES EMPLOYEE';
    private SERVICE_TYPE: string = 'SERVICE TYPE';
    private formObject: any = {};
    public pageId: string;
    public pageTitle: string;
    public controls = [
        { name: 'BranchName', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BusinessCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BusinessDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'CompanyCode' },
        { name: 'CompanyDesc' },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true, disabled: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true, disabled: true },
        { name: 'EmployeeCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'EmployeeName', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'FilterCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'FilterColumn', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'FilterDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'MarketSegmentCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'MarketSegmentDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'RegionCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'RegionDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'ServiceTypeCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'ServiceTypeDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'ViewType' }
    ];

    public hasTrRegion: boolean = true;
    public hasTrMarketSegment: boolean = true;
    public hasTrBranch: boolean = true;
    public hasTrEmployee: boolean = true;
    public hasTrServiceType: boolean = true;
    public dropDown: any = {
        company: {
            inputParams: {
                'parentMode': 'LookUp'
            },
            active: {
                id: '',
                text: ''
            }
        }
    };
    public hasGridData: boolean = false;

    constructor(injector: Injector, private sysCharConstants: SysCharConstants) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSNETTGAINDETAIL;
        this.browserTitle = this.pageTitle = 'Portfolio Movement';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.gridConfig = {
            totalRecords: 1,
            gridHandle: this.utils.randomSixDigitString(),
            gridCacheRefresh: true,
            pageSize: 10
        };
        this.pageParams.gridCurrentPage = 1;
        this.pageParams.showLBColumns = 'N';
        this.riExchange.getParentHTMLValue('DateFrom');
        this.riExchange.getParentHTMLValue('DateTo');
        this.riExchange.getParentHTMLValue('BusinessCode');
        this.riExchange.getParentHTMLValue('BusinessDesc');
        this.riExchange.getParentHTMLValue('CompanyCode');
        this.riExchange.getParentHTMLValue('CompanyDesc');
        this.riExchange.getParentHTMLValue('ViewType');
        this.riExchange.getParentHTMLValue('BranchNumber');
        this.riExchange.getParentHTMLValue('BranchName');
        if (this.getControlValue('CompanyCode'))
            this.dropDown.company.active = {
                id: this.getControlValue('CompanyCode'),
                text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
            };
        this.setFilterColumn();
        this.formObject = {
            'Level': 'detail',
            'BusinessCode': this.getControlValue('BusinessCode'),
            'DateFrom': this.getControlValue('DateFrom'),
            'DateTo': this.getControlValue('DateTo'),
            'StatMode': this.riExchange.getParentHTMLValue('StatMode'),
            'LanguageCode': this.utils.getDefaultLang(),
            'ViewType': this.getControlValue('ViewType'),
            'CompanyCode': this.getControlValue('CompanyCode'),
            'RowID': this.riExchange.getParentAttributeValue('GroupRowID') || '',
            'ReportMode': '',
            'function': 'NettGain',
            'ViewMode': this.riExchange.getParentHTMLValue('ViewMode'),
            'Column': this.riExchange.getParentAttributeValue('Column')
        };
        this.getViewType();
        this.getViewsWithLostBusiness();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
    }

    private getViewType(): void {
        let viewType: string = this.getControlValue('ViewType');
        const groupCode: string = this.riExchange.getParentAttributeValue('GroupCode');
        const groupDesc: string = this.riExchange.getParentAttributeValue('GroupDesc');
        switch (viewType.toUpperCase()) {
            case this.BRANCH:
                this.setControlValue('BranchNumber', groupCode);
                this.setControlValue('BranchName', groupDesc);
                if (parseInt(this.getControlValue('BranchNumber'), 0))
                    this.formObject['BranchNumber'] = this.getControlValue('BranchNumber');
                this.hasTrRegion = false;
                this.hasTrEmployee = false;
                this.hasTrMarketSegment = false;
                this.hasTrServiceType = false;
                break;
            case this.REGION:
                this.setControlValue('RegionCode', groupCode);
                this.setControlValue('RegionDesc', groupDesc);
                this.formObject['RegionCode'] = this.getControlValue('RegionCode');
                this.hasTrBranch = false;
                this.hasTrEmployee = false;
                this.hasTrMarketSegment = false;
                this.hasTrServiceType = false;
                break;
            case this.MARKET_SEGMENT:
                this.setControlValue('MarketSegmentCode', groupCode);
                this.setControlValue('MarketSegmentDesc', groupDesc);
                this.formObject['MarketSegmentCode'] = this.getControlValue('MarketSegmentCode');
                this.hasTrBranch = false;
                this.hasTrEmployee = false;
                this.hasTrServiceType = false;
                break;
            case this.NEGOTIATING_EMPLOYEE:
            case this.SALES_EMPLOYEE:
                this.setControlValue('EmployeeCode', groupCode);
                this.setControlValue('EmployeeName', groupDesc);
                this.formObject['EmployeeCode'] = this.getControlValue('EmployeeCode');
                this.hasTrBranch = this.parentMode === 'branch';
                if (this.hasTrBranch) {
                    this.formObject['BranchNumber'] = this.getControlValue('BranchNumber');
                } else {
                    this.formObject['BranchNumber'] = this.riExchange.getParentAttributeValue('BranchNumber') || '';
                }
                this.hasTrMarketSegment = false;
                this.hasTrRegion = false;
                this.hasTrServiceType = false;
                break;
            case this.SERVICE_TYPE:
                this.setControlValue('ServiceTypeCode', groupCode);
                this.setControlValue('ServiceTypeDesc', groupDesc);
                this.formObject['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
                this.formObject['BranchNumber'] = this.getControlValue('BranchNumber');
                this.hasTrBranch = this.parentMode === 'branch';
                this.formObject['BranchNumber'] = this.hasTrBranch ? this.getControlValue('BranchNumber') : '';
                this.hasTrEmployee = false;
                this.hasTrMarketSegment = false;
                this.hasTrRegion = false;
                break;
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('ChangeProcessed', 'SalesStatistics', 'ChangeProcessed', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('Contract', 'SalesStatistics', 'Contract', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('TierCode', 'Account', 'TierCode', MntConst.eTypeText, 10);
        this.riGrid.AddColumnScreen('TierCode', false);
        this.riGrid.AddColumn('AccountOwner', 'Account', 'AccountOwner', MntConst.eTypeText, 10);
        this.riGrid.AddColumnScreen('AccountOwner', false);
        this.riGrid.AddColumn('PremiseNumber', 'SalesStatistics', 'PremiseNumber', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('BranchNumber', 'SalesStatistics', 'BranchNumber', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('ServiceCommissionEmployee', 'SalesStatistics', 'ServiceCommissionEmployee', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('NegCommissionEmployee', 'SalesStatistics', 'NegCommissionEmployee', MntConst.eTypeText, 10);

        this.riGrid.AddColumn('Product', 'SalesStatistics', 'Product', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('Detail', 'SalesStatistics', 'Detail', MntConst.eTypeText, 10);
        this.riGrid.AddColumnScreen('Detail', false);
        this.riGrid.AddColumn('Quantity', 'SalesStatistics', 'Quantity', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('ValueChangeCode', 'SalesStatistics', 'ValueChangeCode', MntConst.eTypeText, 10);
        if (this.pageParams.showLBColumns === 'Y') {
            this.riGrid.AddColumn('LostBusiness', 'SalesStatistics', 'LostBusiness', MntConst.eTypeText, 10);
            this.riGrid.AddColumn('LostBusinessDetail', 'SalesStatistics', 'LostBusinessDetail', MntConst.eTypeText, 10);
        }

        this.riGrid.AddColumn('ServiceEffectDate', 'SalesStatistics', 'ServiceEffectDate', MntConst.eTypeDate, 10);
        if (this.pageParams.SCEnableMinimumDuration) {
            this.riGrid.AddColumn('MinimumDuration', 'SalesStatistics', 'MinimumDuration', MntConst.eTypeInteger, 10);
            this.riGrid.AddColumnScreen('MinimumDuration', false);
        }

        this.riGrid.AddColumn('SalesStatsValue', 'SalesStatistics', 'SalesStatsValue', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('SalesStatsValue', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('VisitFrequency', 'SalesStatistics', 'VisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnScreen('VisitFrequency', false);

        this.riGrid.Complete();
        this.populateGrid();
    }

    private getViewsWithLostBusiness(): void {
        this.isRequesting = true;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formData = {
            'RequestFunction': 'GetViewsWithLostBusiness',
            'BusinessCode': this.utils.getBusinessCode(),
            'ViewMode': this.riExchange.getParentHTMLValue('ViewMode'),
            'Column': this.riExchange.getParentAttributeValue('Column'),
            'Level': 'NETTGAIN'
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'Sales/iCABSSNettGainDetail', search, formData).then(
            (data) => {
                this.isRequesting = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.displayMessage(data);
                    return;
                }
                this.pageParams.showLBColumns = data.ShowLBColumns;
                this.loadSysChars();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.isRequesting = false;
                this.displayMessage(error);
                this.loadSysChars();
            });
    }


    private populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');

            this.formObject[this.serviceConstants.GridMode] = '2';
            this.formObject[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
            this.formObject[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
            this.formObject[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
            this.formObject[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
            this.formObject[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            this.formObject[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'Sales/iCABSSNettGainDetail', search, this.formObject).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.hasGridData = true;
                        this.displayMessage(data);
                    } else {
                        this.riGrid.RefreshRequired();
                        this.hasGridData = true;
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                            }, 500);
                        }
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.hasGridData = true;
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }

    private loadSysChars(): void {
        this.pageParams.SCEnableCompanyCode = false;
        this.pageParams.SCEnableMinimumDuration = false;
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = this.getURLSearchParamObject();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.SystemCharNumber, [this.sysCharConstants.SystemCharEnableCompanyCode, this.sysCharConstants.SystemCharEnableMinimumDuration].toString());
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                if (data && data.records) {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.pageParams.SCEnableCompanyCode = data.records[0].Required;
                    this.pageParams.SCEnableMinimumDuration = data.records[1].Required;
                    this.buildGrid();
                }
            }
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.buildGrid();
            });
    }

    private setFilterColumn(): void {
        this.setControlValue('FilterColumn', this.riExchange.getParentAttributeValue('ColumnName'));
    }

    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public getCurrentPage(data: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridConfig.gridCacheRefresh = false;
            super.getCurrentPage(data);
        }
        this.pageParams.gridConfig.gridCacheRefresh = true;
    }



}
