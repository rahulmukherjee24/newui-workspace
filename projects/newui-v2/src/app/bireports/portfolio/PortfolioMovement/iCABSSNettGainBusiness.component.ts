import { Injector, OnInit, Component, ViewChild, AfterContentInit } from '@angular/core';

import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
@Component({
    templateUrl: 'iCABSSNettGainBusiness.html',
    providers: [CommonLookUpUtilsService]
})

export class NettgainBusinessComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {

    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private strStatMode: string;
    private hasViewTypeCHnage: boolean = false;
    public pageId: string;
    public pageTitle: string;
    public controls = [
        { name: 'BranchName', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BusinessCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BusinessDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'CompanyCode' },
        { name: 'CompanyDesc' },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true },
        { name: 'EmployeeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ServiceTypeCode' },
        { name: 'ServiceTypeDesc' },
        { name: 'ViewMode', value: 'GroupPortfolio' },
        { name: 'ViewType', value: 'Branch' },
        { name: 'StatMode' }
    ];
    public ellipsis: any = {
        employeeEllipsis: {
            childparams: {
                'parentMode': 'AnyBranch'
            },
            component: EmployeeSearchComponent
        }
    };

    public dropDown: any = {
        company: {
            inputParams: {
                'parentMode': 'LookUp'
            },
            active: {
                id: '',
                text: ''
            }
        },
        serviceType: {
            inputParams: {
                params: {
                    parentMode: 'LookUp'
                }
            },
            active: {
                id: '',
                text: ''
            }
        }
    };


    public isEmployee: boolean = false;
    public hasGridData: boolean = false;
    public isBranch: boolean = false;
    public coulumnMap: Map<string, number> = new Map<string, number>();

    constructor(injector: Injector, private sysCharConstants: SysCharConstants, private lookupUtils: CommonLookUpUtilsService) {
        super(injector);
        this.dropDown.company.inputParams[this.serviceConstants.CountryCode] = this.countryCode();
        this.dropDown.company.inputParams[this.serviceConstants.BusinessCode] = this.businessCode();
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.isBranch = this.riExchange.getParentHTMLValue('type') === 'branch';
        this.pageId = this.isBranch ? PageIdentifier.ICABSSNETTGAINBRANCH : PageIdentifier.ICABSSNETTGAINBUSINESS;
        super.ngAfterContentInit();
        if (this.riExchange.getParentHTMLValue('strStatMode') === 'national') {
            this.strStatMode = 'national';
            this.pageTitle = 'National Account';
            this.utils.setTitle('National Account');
        } else {
            this.strStatMode = 'normal';
            let branchTitle = this.isBranch ? 'Branch ' : '';
            this.pageTitle = branchTitle + 'Portfolio Movement';
            this.utils.setTitle(this.pageTitle);
        }
        this.setControlValue('StatMode', this.strStatMode);
        if (!this.isReturning()) {
            this.pageParams.selectedIndex = 0;
            this.pageParams.gridConfig = {
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString(),
                gridCacheRefresh: true,
                pageSize: 10
            };
            this.pageParams.viewModeList = [];
            if (this.isBranch) {
                this.pageParams.viewTypeList = [
                    { name: 'Branch', value: 'Branch' },
                    { name: 'Sales Employee', value: 'Sales Employee' },
                    { name: 'Service Type', value: 'Service Type' }
                ];
                this.setControlValue('ViewType', 'Sales Employee');
                this.onViewTypeChange();
            } else {
                this.pageParams.viewTypeList = [
                    { name: 'Branch', value: 'Branch' },
                    { name: 'Region', value: 'Region' },
                    { name: 'Negotiating Employee', value: 'Negotiating Employee' },
                    { name: 'Market Segment', value: 'Market Segment' },
                    { name: 'Service Type', value: 'Service Type' }
                ];
            }
            if (this.parentMode === 'Business' && this.isBranch) {
                this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('GroupCode'));
                this.setControlValue('BranchName', this.utils.getBranchTextOnly(this.getControlValue('BranchNumber')));
                this.riExchange.getParentHTMLValue('DateFrom');
                this.riExchange.getParentHTMLValue('DateTo');
            } else {
                this.setControlValue('BranchNumber', this.utils.getBranchCode());
                this.setControlValue('BranchName', this.utils.getBranchTextOnly());
                this.lookupUtils.setTradingDates(this.uiForm, 'DateFrom', 'DateTo');
            }
            this.pageParams.gridCurrentPage = 1;
            this.setControlValue('BusinessCode', this.businessCode());
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            this.loadSysChars();
            this.getViews();
        } else {
            this.pageParams.gridConfig.gridCacheRefresh = false;
            this.buildGrid();
            this.populateGrid();
            if (this.getControlValue('CompanyCode')) {
                this.dropDown.company.active = {
                    id: this.getControlValue('CompanyCode'),
                    text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
                };
            }
            if (this.getControlValue('ServiceTypeCode')) {
                this.dropDown.serviceType.active = {
                    id: this.getControlValue('ServiceTypeCode'),
                    text: this.getControlValue('ServiceTypeCode') + ' - ' + this.getControlValue('ServiceTypeDesc')
                };
            }
        }
    }

    private populateGrid(): void {
        if (this.uiForm.valid) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            let formData: Object = {
                'Level': 'NettGain',
                'BusinessCode': this.utils.getBusinessCode(),
                'ViewType': this.getControlValue('ViewType'),
                'ViewMode': this.getControlValue('ViewMode'),
                'StatMode': this.strStatMode,
                'RequestMode': 'Grid',
                'ReportMode': 'Effective',
                'CompanyCode': this.getControlValue('CompanyCode'),
                'EmployeeCode': this.getControlValue('EmployeeCode'),
                'ServiceTypeCode': this.getControlValue('ServiceTypeCode'),
                'DateFrom': this.getControlValue('DateFrom'),
                'DateTo': this.getControlValue('DateTo')
            };
            if (this.isBranch)
                formData['BranchNumber'] = this.getControlValue('BranchNumber');
            formData[this.serviceConstants.GridMode] = '2';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', this.isBranch ? 'Sales/iCABSSNettGainBranch' : 'Sales/iCABSSNettGainBusiness', search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.hasGridData = true;
                        this.displayMessage(data);
                    } else {
                        this.hasGridData = true;
                        this.riGrid.RefreshRequired();
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                            }, 500);
                        }
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.hasGridData = true;
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = this.getURLSearchParamObject();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.SystemCharNumber, [this.sysCharConstants.SystemCharEnableCompanyCode].toString());
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.SCEnableCompanyCode = data.records[0].Required;
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }

    public onEmployeeDataReceived(data: any): void {
        this.setControlValue('EmployeeCode', data.EmployeeCode);
        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
    }

    public onViewTypeChange(): void {
        this.hasViewTypeCHnage = true;
        this.isEmployee = this.getControlValue('ViewType') === 'Negotiating Employee';
    }

    public onDataRecieved(data: any): void {
        this.setControlValue('CompanyCode', data.CompanyCode);
        this.setControlValue('CompanyDesc', data.CompanyDesc);

    }

    public onServiceTypeSelect(data: any): void {
        this.setControlValue('ServiceTypeDesc', data.ServiceTypeDesc);
        this.setControlValue('ServiceTypeCode', data.ServiceTypeCode);
    }

    public buildGrid(): void {
        let viewType: string = this.getControlValue('ViewType');
        let valueColumnsCount: number = this.pageParams.viewModeList[this.pageParams.selectedIndex].count;
        this.riGrid.Clear();
        this.riGrid.AddColumn('GroupCode', 'SalesStatistics', 'GroupCode', MntConst.eTypeText, this.isBranch ? 12 : 6);
        this.riGrid.AddColumn('Description', 'SalesStatistics', 'Description', MntConst.eTypeTextFree, 20, false, 'Description');
        this.riGrid.AddColumnScreen('Description', false);
        if ((viewType === 'Negotiating Employee') || (viewType !== 'Branch' && this.isBranch)) {
            this.riGrid.AddColumn('Branch', 'SalesStatistics', 'Branch', MntConst.eTypeCode, 3, false);
        }
        this.riGrid.AddColumn('Open', 'NettGain', 'Open', MntConst.eTypeText, 15, false);
        this.riGrid.AddColumnAlign('Open', MntConst.eAlignmentRight);
        if (valueColumnsCount) {
            for (let i = 0; i < valueColumnsCount; i++) {
                this.riGrid.AddColumn(i.toString(), 'SalesStatistics', i.toString(), MntConst.eTypeText, 15);
                this.riGrid.AddColumnAlign(i.toString(), MntConst.eAlignmentRight);
                this.coulumnMap.set(i.toString(), i + 1);
            }
        }
        this.riGrid.AddColumn('Close', 'NettGain', 'Close', MntConst.eTypeText, 15, false);
        this.riGrid.AddColumnAlign('Close', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('GroupCode2', 'SalesStatistics', 'GroupCode2', MntConst.eTypeText, this.isBranch ? 12 : 6);
        this.riGrid.Complete();
    }

    public onViewModeChange(data: any): void {
        this.pageParams.selectedIndex = data.target.selectedIndex;
    }

    public getViews(): void {
        this.isRequesting = true;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formData: Object = {
            'RequestFunction': 'GetViews',
            'Level': 'NETTGAIN',
            'BusinessCode': this.utils.getBusinessCode()
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'Sales/iCABSSNettGainBusiness', search, formData).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.isRequesting = false;
                if (this.hasError(data)) {
                    this.displayMessage(data);
                } else {
                    let name: string;
                    let value: string;
                    let count: number;
                    let viewDataStringByComma: Array<any> = data.ViewDataString.split(',');
                    for (let i = 0; i < viewDataStringByComma.length; i++) {
                        let viewDataStringByPipe: Array<any> = viewDataStringByComma[i].split('|');
                        for (let j = 0; j < viewDataStringByPipe.length; j++) {
                            switch (j) {
                                case 0: value = viewDataStringByPipe[j]; break;
                                case 1: name = viewDataStringByPipe[j]; break;
                                case 2: count = viewDataStringByPipe[j]; break;
                            }
                        }
                        this.pageParams.viewModeList.push({ name: name, value: value, count: count });
                    }
                    this.buildGrid();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.isRequesting = false;
                this.displayMessage(error);
            });
    }

    public getEmployeeName(): void {
        this.lookupUtils.getEmployeeSurname(this.getControlValue('EmployeeCode')).then((data) => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
            } else {
                this.setControlValue('EmployeeSurname', '');
            }
        });
    }


    public onRiGridRefresh(): void {
        if (this.hasViewTypeCHnage) {
            this.pageParams.gridCurrentPage = 1;
            this.hasViewTypeCHnage = false;
        }
        this.buildGrid();
        this.populateGrid();
    }

    public getCurrentPage(data: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridConfig.gridCacheRefresh = false;
            super.getCurrentPage(data);
        }
        this.pageParams.gridConfig.gridCacheRefresh = true;
    }

    public onGridBodyDoubleClick(data: any): void {
        let currentCol: string = this.riGrid.CurrentColumnName;
        switch (currentCol) {
            case 'GroupCode':
            case 'GroupCode2':
                if (this.getControlValue('ViewType') === 'Branch' && this.riGrid.Details.GetValue('GroupCode') !== 'Totals') {
                    this.navigate('Business', BIReportsRoutes.ICABSSNETTGAINBRANCH, {
                        type: 'branch',
                        GroupCode: this.riGrid.Details.GetValue('GroupCode')
                    });
                }
                break;
            default:
                if (currentCol !== 'Open' && currentCol !== 'Close' && currentCol !== 'Branch') {
                    let groupCode: string = this.riGrid.Details.GetValue('GroupCode');
                    let groupDesc: string = this.riGrid.Details.GetAttribute('GroupCode', 'title');
                    let viewType: string = this.getControlValue('ViewType');
                    let queryParams: any = {
                        GroupCode: groupCode,
                        GroupDesc: groupDesc,
                        GroupRowID: this.riGrid.Details.GetAttribute('GroupCode', 'rowID'),
                        ColumnName: (this.riGrid.headerArray[0][this.riGrid.CurrentCell].text).toString().replace('\n', ' '),
                        Column: this.coulumnMap.get(currentCol)
                    };
                    if ((viewType === 'Negotiating Employee' || viewType === 'Sales Employee')) {
                        queryParams['GroupCode'] = groupDesc;
                        queryParams['GroupDesc'] = groupCode;
                        if (queryParams['GroupRowID'] !== 'Total')
                            queryParams['BranchNumber'] = this.riGrid.Details.GetValue('Branch');
                    }
                    if (queryParams['GroupRowID'] === 'Total' && viewType === 'Service Type') {
                        queryParams['GroupCode'] = groupDesc;
                        queryParams['GroupDesc'] = groupCode;
                    }
                    if (queryParams['GroupRowID'] === 'Total' && viewType === 'Branch') {
                        if (!this.isBranch) {
                            queryParams['GroupCode'] = 'Totals';
                        } else {
                            queryParams['GroupCode'] = this.getControlValue('BranchNumber');
                            queryParams['GroupDesc'] = this.getControlValue('BranchName');
                        }
                    }
                    queryParams['GroupRowID'] = queryParams['GroupRowID'] === 'Total' ? '' : queryParams['GroupRowID'];
                    this.navigate(this.isBranch ? 'branch' : 'Business', BIReportsRoutes.ICABSSNETTGAINDETAIL, queryParams);
                }

                break;
        }
    }

}
