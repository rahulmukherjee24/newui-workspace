import { OnInit, Component, Injector, OnDestroy, ViewChild, AfterContentInit } from '@angular/core';

import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { ContractManagementModuleRoutes, BIReportsRoutes } from '@base/PageRoutes';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { InternalGridSearchSalesModuleRoutes } from '@base/PageRoutes';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSASuspendedPortfolioBranch.html'
})

export class SuspendedPortfolioBranchComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private showPremises: boolean = true;

    public commonGridFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public isBranch: boolean = false;
    public isBusiness: boolean = false;
    public pageId: string = '';
    public pageType: string;
    public controls: Array<Object> = [
        { name: 'BusinessDesc', type: MntConst.eTypeCode, disabled: true },
        { name: 'ExcludeText' },
        { name: 'FilterText', type: MntConst.eTypeText },
        { name: 'IncludeText', value: 'True' },
        { name: 'IncTermInd', type: MntConst.eTypeCheckBox },
        { name: 'Level', type: MntConst.eTypeText, value: 'All' },
        { name: 'ShowType', type: MntConst.eTypeText, value: 'All' },
        { name: 'ViewFor', type: MntConst.eTypeText, value: 'servicing' },
        { name: 'ViewBy', type: MntConst.eTypeText, value: 'Branch' },
        { name: 'ViewVADD', type: MntConst.eTypeText, value: 'All' },
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true }
    ];

    public viewVADDValues: any = [
        { text: 'All (excl. VADD)', value: 'All' },
        { text: 'Suspended VADD', value: 'VADD' }
    ];
    public viewByvalues: any = [
        { text: 'Branch', value: 'Branch' },
        { text: 'Region', value: 'Region' }
    ];
    public showTypeValues: any = [
        { text: 'All', value: 'All' },
        { text: 'Contracts', value: 'C' },
        { text: 'Jobs', value: 'J' }
    ];
    public viewForValues: any = [
        { text: 'Servicing Branch', value: 'servicing' },
        { text: 'Negotiating Branch', value: 'negotiating' }
    ];
    public levelValues: any = [
        { text: 'All', value: 'All' },
        { text: 'Contract/Job/Sales', value: 'Contract' },
        { text: 'Premises Value', value: 'Premise' },
        { text: 'Service Cover Value', value: 'Service' }
    ];
    public inputParams: any = {
        branchParams: {
            'parentMode': 'LookUp'
        }
    };

    private xhrParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: {
            'Branch': 'Application/iCABSASuspendedPortfolioBranch',
            'Business': 'Application/iCABSASuspendedPortfolioBusiness'
        }
    };

    public gridConfig: any = {
        itemsPerPage: 12,
        totalItem: 1,
        action: '2',
        riSortOrder: ''
    };

    constructor(injector: Injector) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.riExchange.getParentAttributeValue('ShowProductSales')) {
            this.pageParams.showProductSales = this.riExchange.getParentAttributeValue('ShowProductSales');
        } else {
            this.lookUpShowProductSales();
        }
        this.onWindowLoad();
    }

    ngAfterContentInit(): void {
        this.pageType = this.utils.capitalizeFirstLetter(this.riExchange.getParentHTMLValue('pageType'));
        this.pageId = PageIdentifier['ICABSASUSPENDEDPORTFOLIO' + this.pageType.toUpperCase()];
        this.browserTitle = this.pageTitle = 'Suspended Portfolio By ' + this.pageType;
        this['is' + this.pageType] = this.pageType;

        super.ngAfterContentInit();
        if (this.isReturning()) {
            this.pageParams.gridCacheRefresh = false;
            this.isRequesting = true;
            this.onRiGridRefresh();
        } else {
            if (this.isBusiness) {
                this.setControlValue('Level', 'Business');
            }
            this.pageParams.gridCacheRefresh = true;
            this.setControlValue('IncTermInd', true);
            if (this.parentMode === 'RegionSuspendSummary') {
                if (this.pageParams.showProductSales) {
                    this.showTypeValues.push({ text: 'Product Sales', value: 'P' });
                }
                this.riExchange.getParentHTMLValue('ViewVADD');
                this.riExchange.getParentHTMLValue('ViewFor');
                if (!this.riExchange.getParentHTMLValue('ViewFor')) {
                    this.onChangeControls('ViewVADD');
                }
                let fieldName: string = this.riExchange.getParentAttributeValue('FieldName');

                if (fieldName === 'All') {
                    this.setControlValue('Level', 'All');
                } else if (fieldName.indexOf('Premise') > -1) {
                    this.setControlValue('Level', 'Premise');
                } else if (fieldName.indexOf('Service') > -1) {
                    this.setControlValue('Level', 'Service');
                } else if (fieldName.indexOf('Contract') > -1) {
                    this.setControlValue('Level', 'Contract');
                }

                if (fieldName !== 'All') {
                    this.setControlValue('ShowType', fieldName.charAt(0));
                }

                this.setControlValue('IncTermInd', this.riExchange.getParentHTMLValue('IncTermInd'));
            }
            this.setControlValue('BusinessDesc', this.utils.getBusinessText(this.utils.getBusinessCode()));
            this.setControlValue('BranchNumber', this.riExchange.getParentAttributeValue('BranchNumber') || this.utils.getBranchCode());
            this.setControlValue('BranchName', this.riExchange.getParentAttributeValue('BranchName') || this.utils.getBranchTextOnly());

            if (this.parentMode) {
                this.onRiGridRefresh();
            }
        }
    }

    private onWindowLoad(): void {
        this.pageParams.gridHandle = this.pageParams.gridHandle || this.utils.randomSixDigitString();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        if (this.isBranch) {
            this.riGrid.FunctionPaging = true;
            let level: string = this.getControlValue('Level');
            /* The following AddColumn records are only for those columns that will be shown on screen. Export-Only columns are not included*/
            this.riGrid.AddColumn('ContractNumber', 'Value', 'ContractNumber', MntConst.eTypeText, 15);
            this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ContractName', 'Value', 'ContractName', MntConst.eTypeText, 15);
            if (level === 'All') {
                this.riGrid.AddColumn('GridLevel', 'Value', 'GridLevel', MntConst.eTypeTextFree, 5);
            }
            if (level !== 'Contract') {
                this.riGrid.AddColumn('PremiseNumber', 'Value', 'PremiseNumber', MntConst.eTypeText, 15);
                this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('PremiseName', 'Value', 'PremiseName', MntConst.eTypeText, 15);
                if (level === 'Service' || level === 'All') {
                    this.riGrid.AddColumn('ServiceCoverNumber', 'Value', 'ServiceCoverNumber', MntConst.eTypeText, 15);
                    this.riGrid.AddColumnAlign('ServiceCoverNumber', MntConst.eAlignmentCenter);
                    this.riGrid.AddColumn('ProductName', 'Value', 'ProductName', MntConst.eTypeText, 15, false);
                }
            }
            this.riGrid.AddColumn('SuspendReason', 'ValueSummary', 'SuspendReason', MntConst.eTypeText, 15);
            this.riGrid.AddColumn('SuspendDate', 'ValueSummary', 'SuspendDate', MntConst.eTypeDate, 15);
            this.riGrid.AddColumnAlign('SuspendDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('CommenceDate', 'ValueSummary', 'CommenceDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('CommenceDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnScreen('CommenceDate', false);
            this.riGrid.AddColumn('Value', 'ValueSummary', 'Value', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('Value', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('Status', 'ValueSummary', 'Status', MntConst.eTypeText, 15);
            this.riGrid.AddColumnAlign('Status', MntConst.eAlignmentCenter);
            this.riGrid.Complete();

            this.dateCollist = this.riGrid.getColumnIndexList(['SuspendDate']);
            // Extra Column Not Sent To Grid For Initializing
            this.dateCollist += ',9';
        }

        if (this.isBusiness) {
            if (this.getControlValue('ViewBy') === 'Region') {
                this.riGrid.AddColumn('RegionCode', 'Value', 'RegionCode', MntConst.eTypeText, 15);
                this.riGrid.AddColumnAlign('RegionCode', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('RegionDesc', 'Value', 'RegionDesc', MntConst.eTypeText, 20);
                this.riGrid.AddColumnAlign('RegionDesc', MntConst.eAlignmentCenter);
            } else {
                this.riGrid.AddColumn('BranchNumber', 'Value', 'BranchNumber', MntConst.eTypeText, 15);
                this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('BranchName', 'Value', 'BranchName', MntConst.eTypeText, 20);
                this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentCenter);
            }
            this.riGrid.AddColumn('ContractValue', 'Value', 'ContractValue', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('ContractValue', MntConst.eAlignmentRight);

            if (this.showPremises) {
                this.riGrid.AddColumn('ContractPremiseValue', 'Value', 'ContractPremiseValue', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('ContractPremiseValue', MntConst.eAlignmentRight);

                this.riGrid.AddColumn('ContractServiceValue', 'Value', 'ContractServiceValue', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('ContractServiceValue', MntConst.eAlignmentRight);

                this.riGrid.AddColumn('ContractTotalValue', 'Value', 'ContractTotalValue', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('ContractTotalValue', MntConst.eAlignmentRight);
            }

            this.riGrid.AddColumn('JobValue', 'Value', 'JobValue', MntConst.eTypeText, 15);
            this.riGrid.AddColumnAlign('JobValue', MntConst.eAlignmentRight);

            if (this.showPremises) {
                this.riGrid.AddColumn('JobPremiseValue', 'Value', 'JobPremiseValue', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('JobPremiseValue', MntConst.eAlignmentRight);

                this.riGrid.AddColumn('JobServiceValue', 'Value', 'JobServiceValue', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('JobServiceValue', MntConst.eAlignmentRight);

                this.riGrid.AddColumn('JobTotalValue', 'Value', 'JobTotalValue', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('JobTotalValue', MntConst.eAlignmentRight);
            }

            if (this.pageParams.showProductSales && this.showPremises) {
                this.riGrid.AddColumn('ProValue', 'Value', 'ProValue', MntConst.eTypeText, 15);
                this.riGrid.AddColumnAlign('ProValue', MntConst.eAlignmentRight);

                this.riGrid.AddColumn('ProPremiseValue', 'Value', 'ProPremiseValue', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('ProPremiseValue', MntConst.eAlignmentRight);

                this.riGrid.AddColumn('ProServiceValue', 'Value', 'ProServiceValue', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('ProServiceValue', MntConst.eAlignmentRight);

                this.riGrid.AddColumn('ProTotalValue', 'Value', 'ProTotalValue', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('ProTotalValue', MntConst.eAlignmentRight);
            }

            this.riGrid.AddColumn('TotalValue', 'Value', 'TotalValue', MntConst.eTypeText, 15);
            this.riGrid.AddColumnAlign('TotalValue', MntConst.eAlignmentRight);

            this.riGrid.Complete();
        }
    }

    private populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());
        let form: any = {};
        form['Level'] = this.getControlValue('Level');
        form['ViewVADD'] = this.getControlValue('ViewVADD');
        form['ViewFor'] = this.getControlValue('ViewFor');
        form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        form['IncTermInd'] = this.getControlValue('IncTermInd') ? 'True' : 'False';
        form['Function'] = '';
        if (this.isBusiness) {
            form['ViewBy'] = this.getControlValue('ViewBy');
        }
        if (this.isBranch) {
            form['ShowType'] = this.getControlValue('ShowType');
            form['BranchNumber'] = this.getControlValue('BranchNumber');
            form['IncludeText'] = this.getControlValue('IncludeText') === 'True' ? 'True' : 'False';
            form['FilterText'] = this.getControlValue('FilterText');
        }


        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        form[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        form[this.serviceConstants.PageSize] = this.gridConfig.itemsPerPage;
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;

        form['riSortOrder'] = this.riGrid.SortOrder;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation[this.pageType], gridSearch, form).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch((error) => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.buildGrid();
            this.commonGridFunction.onRefreshClick();
        }
    }

    public onGridBodyDoubleClick?(): void {
        if (this.isBranch) {
            let currContractUrlParam: any = this.riGrid.Details.GetAttribute('ContractNumber', 'AdditionalProperty');
            let routeValues: any = this.riGrid.Details.GetValue('ContractNumber').split('/');
            let navPath: any;
            if (currContractUrlParam) {
                switch (this.riGrid.CurrentColumnName) {
                    case 'ContractNumber':
                        switch (routeValues[0]) {
                            case 'C':
                                navPath = ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE;
                                break;
                            case 'J':
                                navPath = ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE;
                                break;
                            case 'P':
                                navPath = ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE;
                                break;
                        }
                        if (routeValues[1]) {
                            this.navigate('LoadByKeyFields', navPath, {
                                parentMode: 'LoadByKeyFields',
                                ContractNumber: routeValues[1],
                                ContractName: this.riGrid.Details.GetValue('ContractName')
                            });
                        }
                        break;
                    case 'PremiseNumber':
                        if (this.riGrid.Details.GetValue('PremiseNumber') && this.riGrid.Details.GetValue('PremiseNumber') !== '0') {
                            this.navigate('LoadByKeyFields', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                                parentMode: 'LoadByKeyFields',
                                ContractNumber: routeValues[1],
                                ContractName: this.riGrid.Details.GetValue('ContractName'),
                                PremiseNumber: this.riGrid.Details.GetValue('PremiseNumber'),
                                PremiseName: this.riGrid.Details.GetValue('PremiseName'),
                                contractTypeCode: routeValues[0]
                            });
                        }
                        break;
                    case 'ServiceCoverNumber':
                        let path: any = routeValues[0] === 'P' ? InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE : ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE;
                        this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute('ServiceCoverNumber', 'AdditionalProperty'));
                        if (this.riGrid.Details.GetValue('ServiceCoverNumber')) {
                            this.navigate('ServiceCoverGrid', path, {
                                parentMode: 'ServiceCoverGrid',
                                ContractNumber: routeValues[1],
                                ContractName: this.riGrid.Details.GetValue('ContractName'),
                                PremiseNumber: this.riGrid.Details.GetValue('PremiseNumber'),
                                PremiseName: this.riGrid.Details.GetValue('PremiseName'),
                                ServiceCoverNumber: this.riGrid.Details.GetValue('ServiceCoverNumber'),
                                ServiceCoverROWID: this.riGrid.Details.GetAttribute('ServiceCoverNumber', 'AdditionalProperty'),
                                currentContractType: routeValues[0]
                            });
                        }
                        break;
                }
            }
        }

        if (this.isBusiness) {
            let currentCol = this.riGrid.CurrentColumnName;
            if (this.getControlValue('ViewBy') === 'Branch') {
                this.setAttribute('BranchNumber', this.riGrid.Details.GetValue('BranchNumber'));
                this.setAttribute('BranchName', this.riGrid.Details.GetValue('BranchName'));
                switch (currentCol) {
                    case 'BranchNumber':
                        this.setAttribute('FieldName', 'All');
                        this.navigate('RegionSuspendSummary', BIReportsRoutes.ICABSASUSPENDEDPORTFOLIOBRANCH, {
                            pageType: 'Branch'
                        });
                        break;
                    case 'ContractValue':
                    case 'ContractPremiseValue':
                    case 'ContractServiceValue':
                    case 'JobValue':
                    case 'JobPremiseValue':
                    case 'JobServiceValue':
                    case 'ProValue':
                    case 'ProPremiseValue':
                    case 'ProServiceValue':
                        this.setAttribute('FieldName', this.riGrid.CurrentColumnName);
                        this.setAttribute('Level', 'Job');
                        this.navigate('RegionSuspendSummary', BIReportsRoutes.ICABSASUSPENDEDPORTFOLIOBRANCH, {
                            pageType: 'Branch'
                        });
                        break;
                }

            } else {
                if (currentCol === 'RegionCode' && this.riGrid.Details.GetAttribute(currentCol, 'drilldown')) {
                    this.displayMessage(MessageConstant.Message.PageNotCovered, CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                }
            }
        }
    }

    public onChangeControls(controlId: string): void {
        this.pageParams.gridCurrentPage = 1;
        this.gridConfig.totalRecords = 0;
        this.pageParams.gridCacheRefresh = true;
        let viewVADD: string = this.getControlValue('ViewVADD');
        switch (controlId) {
            case 'ViewVADD':
                viewVADD === 'All' ? this.setControlValue('ViewFor', 'servicing') : this.setControlValue('ViewFor', 'negotiating');
                if (this.isBusiness) {
                    if (viewVADD === 'VADD') {
                        this.showPremises = false;
                    } else {
                        this.showPremises = true;
                    }
                }
                if (this.isBranch) {
                    if (viewVADD === 'VADD') {
                        this.levelValues = [
                            { text: 'Contract/Job/Sales', value: 'Contract' }
                        ];
                    } else {
                        this.levelValues = [
                            { text: 'All', value: 'All' },
                            { text: 'Contract/Job/Sales', value: 'Contract' },
                            { text: 'Premises Value', value: 'Premise' },
                            { text: 'Service Cover Value', value: 'Service' }
                        ];
                    }
                    this.setControlValue('Level', this.levelValues[0].value);
                }
                this.riGrid.Clear();
                this.buildGrid();
                this.riGrid.RefreshRequired();
                break;
            case 'ViewFor':
            case 'IncTermInd':
            case 'Level':
                this.riGrid.Clear();
                this.riGrid.RefreshRequired();
                break;
        }
    }

    public getCurrentPage(currentPage: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridCacheRefresh = false;
            super.getCurrentPage(currentPage);
        }
    }

    private lookUpShowProductSales(): void {
        if (!this.pageParams.showProductSales) {
            let lookupIP = [
                {
                    'table': 'ContractType',
                    'query': {
                        'BusinessCode': this.businessCode(),
                        'ContractTypeCode': 'P'
                    },
                    'fields': ['ContractTypeCode']
                }
            ];
            this.LookUp.lookUpPromise(lookupIP).then((data) => {
                if (data && data[0] && data[0].length) {
                    this.pageParams.showProductSales = true;
                    this.setAttribute('ShowProductSales', true);
                    if (this.isBranch) {
                        this.showTypeValues.push({ text: 'Product Sales', value: 'P' });
                    }
                }
            });
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }
}
