import { IControls } from './../../../base/ControlsType';
import { DropdownConstants } from './../../../base/ComponentConstants';
import { Component, Injector, OnInit, AfterContentInit, OnDestroy } from '@angular/core';

import * as moment from 'moment';
import { Subscription } from 'rxjs/Subscription';

import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { StaticUtils } from '@shared/services/static.utility';

import { BranchServiceAreaSearchComponent } from '@internal/search/iCABSBBranchServiceAreaSearch';
import { CustomerTypeSearchComponent } from '@internal/search/iCABSSCustomerTypeSearch';
import { EmployeeSearchComponent } from '@internal/search/iCABSBEmployeeSearch';
import { ProductDetailSearchComponent } from '@internal/search/iCABSBProductDetailSearch.component';
import { ProductSearchGridComponent } from '@internal/search/iCABSBProductSearch';
import { SalesAreaSearchComponent } from '@internal/search/iCABSBSalesAreaSearch.component';

@Component({
    templateUrl: 'iCABSARContractReport.html'
})
export class ContractReportComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    private httpParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARContractReport'
    };
    private levelChangeSubscription: Subscription;

    protected pageId: string;
    protected controls: IControls[] = [
        //#region Header Controls
        { name: 'ReportLevel', type: MntConst.eTypeText, required: true },
        { name: 'ReportType', type: MntConst.eTypeText, value: 'premise' },
        { name: 'SortBy', type: MntConst.eTypeCode, value: 'Postcode' },
        { name: 'RepDest', type: MntConst.eTypeText, value: 'direct' },
        { name: 'TotalsOnly', type: MntConst.eTypeCheckBox },
        { name: 'OutputToCSV', type: MntConst.eTypeCheckBox },
        //#endregion
        //#region Tab 1 Controls
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true },
        { name: 'ValueFrom', type: MntConst.eTypeCurrency, required: true, value: '0.00' },
        { name: 'ValueTo', type: MntConst.eTypeCurrency, required: true, value: '9999999.99' },
        { name: 'FilterBy', type: MntConst.eTypeText, value: 'All' },
        { name: 'SeasonalInd', type: MntConst.eTypeText, value: 'IncludeSeasonal' },
        { name: 'TermDateFrom', type: MntConst.eTypeDate },
        { name: 'TermDateTo', type: MntConst.eTypeDate },
        { name: 'IncludeDeleted', type: MntConst.eTypeCheckBox },
        { name: 'IncludeContracts', type: MntConst.eTypeCheckBox, value: true },
        { name: 'IncludeJobs', type: MntConst.eTypeCheckBox, value: true },
        { name: 'IncludeProductSales', type: MntConst.eTypeCheckBox, value: true },
        { name: 'IncludeTrialProducts', type: MntConst.eTypeCheckBox },
        { name: 'IncludeFOCProducts', type: MntConst.eTypeCheckBox },
        //#endregion
        //#region Tab 2 Controls
        { name: 'SalesAreaCode', type: MntConst.eTypeCode },
        { name: 'SalesEmployeeCode', type: MntConst.eTypeText },
        { name: 'ServiceAreaCode', type: MntConst.eTypeText },
        { name: 'PostcodeList', type: MntConst.eTypeText },
        { name: 'MarketSegmentString', type: MntConst.eTypeText },
        { name: 'CustomerTypes', type: MntConst.eTypeText },
        { name: 'ServiceTypes', type: MntConst.eTypeText },
        { name: 'IncludeProducts', type: MntConst.eTypeText },
        { name: 'ExcludeProducts', type: MntConst.eTypeText },
        { name: 'IncludeProductDetail', type: MntConst.eTypeText },
        { name: 'ExcludeProductDetail', type: MntConst.eTypeText },
        //#endregion
        //#region Hidden Controls
        { name: 'SalesBranchNumber', type: MntConst.eTypeText },
        { name: 'ServiceBranchNumber', type: MntConst.eTypeText },
        { name: 'RegionCode', type: MntConst.eTypeText }
        //#endregion
    ];

    public dropdown: any = {
        marketSegment: {
            inputParams: {
                operation: 'System/iCABSSMarketSegmentSearch',
                module: 'segmentation',
                method: 'ccm/search'
            },
            displayColumns: ['MarketSegment.MarketSegmentCode', 'MarketSegment.MarketSegmentDesc']
        },
        serviceType: {
            inputParams: {
                params: {
                    parentMode: 'ContractJobReport'
                }
            }
        }
    };
    public ellipsis: any = {
        salesArea: {
            childParams: {
                'parentMode': 'ContractJobReport'
            },
            component: SalesAreaSearchComponent
        },
        salesEmployee: {
            childParams: {
                'parentMode': 'ContractJobReport'
            },
            component: EmployeeSearchComponent
        },
        serviceArea: {
            childParams: {
                parentMode: 'ContractJobReport'
            },
            component: BranchServiceAreaSearchComponent
        },
        customerType: {
            childParams: {
                parentMode: 'ContractJobReport',
                CustomerTypes: ''
            },
            component: CustomerTypeSearchComponent
        },
        includeProduct: {
            childParams: {
                parentMode: 'ContractJobReport:Include',
                IncludeProducts: ''
            },
            component: ProductSearchGridComponent
        },
        excludeProduct: {
            childParams: {
                parentMode: 'ContractJobReport:Exclude',
                ExcludeProducts: ''
            },
            component: ProductSearchGridComponent
        },
        includeProductDetail: {
            childParams: {
                parentMode: 'ContractJobReport:Include',
                currentValue: ''
            },
            component: ProductDetailSearchComponent
        },
        excludeProductDetail: {
            childParams: {
                parentMode: 'ContractJobReport:Exclude'
            },
            component: ProductDetailSearchComponent
        }
    };
    public reportMessage: string = '';

    constructor(injector: Injector) {
        super(injector);

        this.pageTitle = 'Contract/Job Report';

        this.levelChangeSubscription = this.activatedRoute.queryParams.subscribe(
            (param: Record<string, string>) => {
                if (param && param['ReportLevel']) {
                    this.reportMessage = '';
                    // Set Page Type
                    if (this.uiForm && this.uiForm.controls && this.uiForm.controls.hasOwnProperty('ReportLevel')) {
                        this.setControlValue('ReportLevel', param['ReportLevel']);
                    }
                }
            });
    }

    //#region LifeCycle Hooks
    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();

        let branch: string = this.utils.getBranchCode();
        this.ellipsis.salesArea.childParams.SalesBranchNumber = branch;
        this.ellipsis.serviceArea.childParams.ServiceBranchNumber = branch;

        if (!this.isReturning()) {
            this.riExchange.getParentHTMLValue('ReportLevel');
            // Set Dates
            let dates: Array<Date> = StaticUtils.getFirstAndLastDayOfYear();

            this.setControlValue('SalesBranchNumber', branch);
            this.setControlValue('ServiceBranchNumber', branch);
            this.setControlValue('DateFrom', dates[0]);
            this.setControlValue('DateTo', dates[1]);

            this.getRegionCode();
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();

        if (this.levelChangeSubscription) {
            this.levelChangeSubscription.unsubscribe();
        }
    }
    //#endregion

    //#region Private Methods
    public getRegionCode(): void {
        this.utils.getRegionCode().subscribe(data => {
            this.setControlValue('RegionCode', data);
        }, error => {
            this.displayMessage(error);
        });
    }

    private submitBatchProcess(): void {
        let ignore: Array<string> = ['BranchNumber', 'RepDest', 'SortByDesc'];
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '6');

        this.controls.forEach(element => {
            let control: string = element.name;
            if (ignore.indexOf(control) < 0) {
                if (element.type === MntConst.eTypeCheckBox) {
                    query.set(control, StaticUtils.convertCheckboxValueToRequestValue(this.getControlValue(control)));
                } else {
                    query.set(control, this.getControlValue(control));
                }
            }
        });

        query.set('SortByDesc', this.getControlValue('SortBy'));
        query.set('RepManDest', StaticUtils.getReportManDest(this.getControlValue('RepDest')));
        query.set('BranchNumber', this.utils.getBranchCode());

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrGet(this.httpParams.method, this.httpParams.module, this.httpParams.operation, query).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.errorMessage || data.fullError) {
                this.displayMessage(data);
            } else if (data.ReturnHTML) {
                this.reportMessage = data.ReturnHTML;
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage(error);
        });
    }
    //#endregion

    //#region Public Methods
    public onChangeFilterBy(): void {
        const shouldEnable: boolean = (this.getControlValue('FilterBy') === 'T' || this.getControlValue('FilterBy') === 'All');

        this.disableControl('TermDateFrom', !shouldEnable);
        this.disableControl('TermDateTo', !shouldEnable);

        if (!shouldEnable) {
            this.setControlValue('TermDateFrom', '');
            this.setControlValue('TermDateTo', '');
        }
    }

    public onChangeOutputToCSV(): void {
        let isOutputToCSV: boolean = this.getControlValue('OutputToCSV');
        if (isOutputToCSV) {
            this.setControlValue('RepDest', 'Email');
            this.setControlValue('TotalsOnly', false);
        }

        this.disableControl('RepDest', isOutputToCSV);
        this.disableControl('TotalsOnly', isOutputToCSV);
    }

    public onChangeIncludeOptions(option: string): void {
        let optionValue: boolean = this.getControlValue('Include' + option);
        if (!optionValue) {
            return;
        }
        switch (option) {
            case 'Contracts':
            case 'Jobs':
            case 'ProductSales':
                this.setControlValue('IncludeTrialProducts', false);
                this.setControlValue('IncludeFOCProducts', false);
                break;
            case 'TrialProducts':
                this.setControlValue('IncludeContracts', false);
                this.setControlValue('IncludeJobs', false);
                this.setControlValue('IncludeProductSales', false);
                this.setControlValue('IncludeFOCProducts', false);
                break;
            case 'FOCProducts':
                this.setControlValue('IncludeContracts', false);
                this.setControlValue('IncludeJobs', false);
                this.setControlValue('IncludeProductSales', false);
                this.setControlValue('IncludeTrialProducts', false);
                break;
        }
    }

    public submitReport(): void {
        // Check If The Form Is Valid
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }

        /**
         * Check Custom Validations For Termination Dates
         * Check Both The Dates Are Present And From Date Is Lesser Than TO Date
         */
        const termDateFrom: string = this.getControlValue('TermDateFrom');
        const termDateTo: string = this.getControlValue('TermDateTo');

        if ((!termDateFrom && termDateTo) // Check If Termination Start Date Is There But Not End Date
            || (termDateFrom && !termDateTo) // Check If Termination End Date Is There But Not Start Date
            || moment(termDateFrom, StaticUtils.c_s_DATE_FORMAT_FIXED).diff(moment(termDateTo, StaticUtils.c_s_DATE_FORMAT_FIXED)) > 0) { // Check If Termination Start Date Is Greater Than End Date
            this.displayMessage('Termination Start And End Date Must Contain Valid Dates');
            return;
        }

        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '6');
        query.set(this.serviceConstants.Function, 'ValidatePostcodeList');
        query.set('PostcodeList', '');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrGet(this.httpParams.method, this.httpParams.module, this.httpParams.operation, query).then(data => {
            if (data.ErrorMessage) {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(data.ErrorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
            } else {
                this.submitBatchProcess();
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage(error);
        });
    }

    public onMarketSegmentSelect(data: any): void {
        this.setControlValue('MarketSegmentString', data[DropdownConstants.c_s_KEY_MULTISELECTVALUE]);
    }

    public onServiceTypeSelect(data: any): void {
        this.setControlValue('ServiceTypes', data[DropdownConstants.c_s_KEY_MULTISELECTVALUE]);
    }

    public onEllipsisDataSelect(data: string, control: string): void {
        let newValue: string = '';

        switch (control) {
            case 'CustomerTypes':
                newValue = data['CustomerTypeCode'];
                this.ellipsis.customerType['childParams']['CustomerTypes'] = newValue;
                break;
            case 'IncludeProducts':
                newValue = data['IncludeProducts'];
                this.ellipsis.includeProduct['childParams']['IncludeProducts'] = newValue;
                break;
            case 'ExcludeProducts':
                newValue = data['ExcludeProducts'];
                this.ellipsis.excludeProduct['childParams']['ExcludeProducts'] = newValue;
                break;
            case 'IncludeProductDetail':
                newValue = data['IncludeProductDetail'];
                this.ellipsis.includeProductDetail['childParams']['currentValue'] = newValue;
                break;
            case 'ExcludeProductDetail':
                newValue = data['ExcludeProductDetail'];
                this.ellipsis.excludeProductDetail['childParams']['currentValue'] = newValue;
                break;
            default:
                newValue = data[control];
        }

        this.setControlValue(control, newValue);
    }

    public onChangeEllipsisControls(control: string): void {
        let newValue: string = this.getControlValue(control);
        switch (control) {
            case 'CustomerTypes':
                this.ellipsis.customerType['childParams']['CustomerTypes'] = newValue;
                break;
            case 'IncludeProducts':
                this.ellipsis.includeProduct['childParams']['IncludeProducts'] = newValue;
                break;
            case 'ExcludeProducts':
                this.ellipsis.excludeProduct['childParams']['ExcludeProducts'] = newValue;
                break;
            case 'IncludeProductDetail':
                this.ellipsis.includeProductDetail['childParams']['currentValue'] = newValue;
                break;
            case 'ExcludeProductDetail':
                this.ellipsis.excludeProductDetail['childParams']['currentValue'] = newValue;
                break;
        }
    }
    //#endregion
}
