import { Component, Injector, ViewChild, OnInit, AfterContentInit } from '@angular/core';

import { BIReportsRoutes } from '@app/base/PageRoutes';
import { BranchServiceAreaSearchComponent } from '@internal/search/iCABSBBranchServiceAreaSearch';
import { CommonDropdownComponent } from '@shared/components/common-dropdown/common-dropdown.component';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { DropdownComponent } from '@shared/components/dropdown/dropdown';
import { EmployeeSearchComponent } from '@internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SalesAreaSearchComponent } from '@internal/search/iCABSBSalesAreaSearch.component';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { ContractManagementModuleRoutes } from '@base/PageRoutes';

@Component({
    templateUrl: 'iCABSARLostBusinessAnalysisGrid.html',
    providers: [CommonLookUpUtilsService]
})
export class LostBusinessAnalysisGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {

    @ViewChild('marketSegmentSearchDropDown') marketSegmentSearchDropDown: CommonDropdownComponent;
    @ViewChild('reasonCodeDropdown') public reasonCodeDropdown: DropdownComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private httpParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: {
            main: 'ApplicationReport/iCABSARLostBusinessAnalysisBusinessGrid',
            Branch: 'ApplicationReport/iCABSARLostBusinessAnalysisGrid'
        }
    };

    public controls = [
        { name: 'BranchType', value: 'Service' },
        { name: 'BusinessCode', disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BusinessDesc', disabled: true, required: true, type: MntConst.eTypeText, nosubmit: true },
        { name: 'BranchNumber', disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BranchName', disabled: true, required: true, type: MntConst.eTypeText, nosubmit: true },
        { name: 'CompanyCode' },
        { name: 'CompanyDesc', nosubmit: true },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'DateType' },
        { name: 'GroupBy', value: 'None' },
        { name: 'LessThanMonths', required: true, type: MntConst.eTypeText },
        { name: 'LessThanYears', required: true, type: MntConst.eTypeText },
        { name: 'LostBusinessCode' },
        { name: 'LostBusinessDesc', nosubmit: true },
        { name: 'MarketSegmentCode' },
        { name: 'MarketSegmentDesc', nosubmit: true },
        { name: 'MoreThanValue', required: true, type: MntConst.eTypeDecimal2 },
        { name: 'ShowType' },
        { name: 'SalesAreaCode', type: MntConst.eTypeCode, nosubmit: true },
        { name: 'SalesAreaDesc', type: MntConst.eTypeText, disabled: true, nosubmit: true },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode, nosubmit: true },
        { name: 'BranchServiceAreaDesc', type: MntConst.eTypeText, disabled: true, nosubmit: true },
        { name: 'EmployeeCode', type: MntConst.eTypeText },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true, nosubmit: true }
    ];
    public pageId: string;
    public hasGridData: boolean = false;
    public groupByValues: Array<any> = [
        { value: 'MarketSegment', name: 'Market Segment', control: 'MarketSegmentCode' },
        { value: 'LostBusiness', name: 'Lost Business Reason', control: 'LostBusinessCode' },
        { value: 'Company', name: 'Company', control: 'CompanyCode' }
    ];
    public dropdown: any = {
        dropdownSearchReason: {
            inputParams: {
                'parentMode': 'LookUp'
            },
            active: {
                id: '',
                text: ''
            },
            isDisabled: false
        },
        companySearch: {
            inputParams: {
                parentMode: 'LookUp',
                countryCode: this.countryCode(),
                businessCode: this.businessCode()
            },
            active: { id: '', text: '' },
            isDisabled: false
        },
        marketSegment: {
            inputParams: {
                operation: 'System/iCABSSMarketSegmentSearch',
                module: 'segmentation',
                method: 'ccm/search'
            },
            displayFields: ['MarketSegment.MarketSegmentCode', 'MarketSegment.MarketSegmentDesc'],
            active: { id: '', text: '' },
            isDisabled: false
        }
    };
    public ellipsis: any = {
        employee: {
            configParams: {
                parentMode: 'LookUp'
            },
            component: EmployeeSearchComponent
        },
        salesArea: {
            configParams: {
                parentMode: 'LookUp'
            },
            component: SalesAreaSearchComponent
        },
        serviceArea: {
            configParams: {
                parentMode: 'LookUp'
            },
            component: BranchServiceAreaSearchComponent
        }
    };
    public type: string = '';
    public employee: string = '';
    public salesArea: string = '';
    public serviceArea: string = '';
    public isBranchTypeNeg: boolean = false;
    public isEmployeeEnabled: boolean = true;
    public isServiceAreaEnabled: boolean = true;
    public isSalesAreaEnabled: boolean = true;

    constructor(injector: Injector, private sysCharConstants: SysCharConstants, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.browserTitle = this.pageTitle = 'Business Lost Business Analysis';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.type = this.riExchange.getParentHTMLValue('type');
        switch (this.type) {
            case 'main':
                let maingroup: string = this.riExchange.getParentHTMLValue('group');
                this.pageId = PageIdentifier['ICABSARLOSTBUSINESSANALYSISBUSINESSGRID' + maingroup] + (maingroup ? this.parentMode : '');
                this.pageTitle = 'Business Lost Business Analysis';
                this.utils.setTitle(this.pageTitle);
                break;
            case 'Branch':
                let group: string = this.riExchange.getParentHTMLValue('group');
                this.pageId = PageIdentifier['ICABSARLOSTBUSINESSANALYSISGRID' + group] + (group ? this.parentMode : '');
                this.pageTitle = 'Branch Lost Business Analysis';
                this.utils.setTitle(this.pageTitle);
                break;
            default:
                this.pageId = PageIdentifier.ICABSARLOSTBUSINESSANALYSISBUSINESSGRIDGROUPBY;
                this.pageTitle = 'Business Lost Business Analysis';
                this.utils.setTitle(this.pageTitle);
                break;
        }

        super.ngAfterContentInit();
        if (!this.isReturning()) {
            this.pageParams.blnDrillDownMarketSegmentCode = false;
            this.pageParams.blnDrillDownLostBusinessCode = false;
            this.pageParams.blnDrillDownCompanyCode = false;
            this.pageParams.gridConfig = {
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString(),
                gridCacheRefresh: true,
                pageSize: 10
            };
            this.pageParams.gridCurrentPage = 1;
            this.loadSysChar();
            this.setControlValue('BusinessCode', this.utils.getBusinessCode());
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            this.setControlValue('DateType', 'Effective');
            this.setControlValue('ShowType', 'All');
            if (this.parentMode) {
                this.drilldownInput();
            } else {
                this.inputFilters();
            }

            if (this.type === 'Branch') {
                // tslint:disable-next-line: no-unused-expression
                this.riExchange.getParentHTMLValue('BranchNumber') || this.setControlValue('BranchNumber', this.utils.getBranchCode());
                if (this.parentMode) {
                    this.riExchange.getParentHTMLValue('LostBusinessCode');
                    this.riExchange.getParentHTMLValue('LostBusinessDesc');
                    this.riExchange.getParentHTMLValue('CompanyCode');
                    this.riExchange.getParentHTMLValue('CompanyDesc');
                    this.riExchange.getParentHTMLValue('MarketSegmentCode');
                    this.riExchange.getParentHTMLValue('MarketSegmentDesc');
                    this.setDropdownValues();
                    if (this.parentMode === 'LostBusinessAnalysis1' || this.parentMode === 'LostBusinessAnalysis') {
                        this.disableControl('BranchType', true);
                    } else {
                        this.riExchange.getParentHTMLValue('BranchNumber');
                        this.riExchange.getParentHTMLValue('BranchName');
                        this.riExchange.getParentHTMLValue('BranchNumber');
                        this.riExchange.getParentHTMLValue('BranchName');
                        this.employee = this.riExchange.getParentHTMLValue('EmployeeCode');
                        this.riExchange.getParentHTMLValue('EmployeeSurname');
                        this.salesArea = this.riExchange.getParentHTMLValue('SalesAreaCode');
                        this.riExchange.getParentHTMLValue('SalesAreaDesc');
                        this.serviceArea = this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
                        this.riExchange.getParentHTMLValue('BranchServiceAreaDesc');
                        this.riExchange.getParentHTMLValue('BranchType');

                        this.onBranchTypeChange();
                        /**
                         * @todo - Add Logic
                         */
                    }
                }
                this.commonLookup.getBranchName(this.getControlValue('BranchNumber')).then(data => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('BranchName', data[0][0].BranchName);
                    }
                }).catch(error => {
                    this.displayMessage(error);
                });

                let branch: string = this.getControlValue('BranchNumber');
                this.ellipsis.salesArea.configParams.SalesBranchNumber = branch;
                this.ellipsis.serviceArea.configParams.ServiceBranchNumber = branch;
            } else if (this.type === 'main') {
                this.setControlValue('GroupBy', 'Branch');
            }
        } else {
            this.setDropdownValues();
            this.enableDisableFilterFields();
        }

        this.enableEllipsisControls();
        this.buildGrid();
        if (this.isReturning() || this.parentMode) {
            this.populateGrid();
        }
    }

    private inputFilters(): void {
        let date: Date = new Date();
        let formDate: Date = new Date(date.getFullYear(), 0, 1);
        this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(formDate) as string);
        this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(date) as string);
        this.setControlValue('MoreThanValue', this.globalize.parseCurrencyToFixedFormat('0') as number);
        this.setControlValue('LessThanYears', '999');
        this.setControlValue('LessThanMonths', '0');
    }

    private populateGrid(): void {
        this.isRequesting = true;
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        this.controls.forEach(control => {
            if (!control.nosubmit) {
                formData[control.name] = this.getControlValue(control.name);
            }
        });
        formData['Level'] = this.type === 'main' ? 'Business' : this.type;
        if (this.type === 'Branch') {
            formData['ServiceBranchNumber'] = this.getControlValue('BranchNumber');
            formData['SalesArea'] = this.getControlValue('SalesAreaCode');
            formData['ServiceArea'] = this.getControlValue('BranchServiceAreaCode');
        }
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.httpParams.method, this.httpParams.module, this.httpParams.operation[this.type], search, formData).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.isRequesting = false;
                if (this.hasError(data)) {
                    this.hasGridData = false;
                    this.displayMessage(data);
                } else {
                    this.hasGridData = true;
                    this.riGrid.RefreshRequired();
                    this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                    if (this.isReturning()) {
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 500);
                    }
                    this.riGrid.Execute(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.hasGridData = false;
                this.isRequesting = false;
                this.displayMessage(error);
            });
    }

    private drilldownInput(): void {
        Object.keys(this.uiForm.controls).forEach(control => {
            if (control !== 'GroupBy')
                this.riExchange.getParentHTMLValue(control);
        });
        if (this.getControlValue('LostBusinessCode')) {
            this.pageParams.blnDrillDownLostBusinessCode = true;
        }
        if (this.getControlValue('CompanyCode')) {
            this.pageParams.blnDrillDownCompanyCode = true;
        }
        if (this.getControlValue('MarketSegmentCode')) {
            this.pageParams.blnDrillDownMarketSegmentCode = true;
        }
        switch (this.parentMode) {
            case 'LostBusinessAnalysis':
            case 'LostBusinessAnalysis1':
                this.enableDisableFilterFields();
                break;
            case 'MarketSegment':
                this.riExchange.getParentHTMLValue('MarketSegmentCode');
                this.riExchange.getParentHTMLValue('MarketSegmentDesc');
                this.pageParams.blnDrillDownMarketSegmentCode = true;
                break;
            case 'LostBusiness':
                this.riExchange.getParentHTMLValue('LostBusinessCode');
                this.riExchange.getParentHTMLValue('LostBusinessDesc');
                this.pageParams.blnDrillDownLostBusinessCode = true;
                break;
            case 'Company':
                this.riExchange.getParentHTMLValue('CompanyCode');
                this.riExchange.getParentHTMLValue('CompanyDesc');
                this.pageParams.blnDrillDownCompanyCode = true;
                break;
            case 'Employee':
                this.riExchange.getParentHTMLValue('EmployeeCode');
                this.riExchange.getParentHTMLValue('EmployeeSurname');
                break;
            case 'SalesArea':
                this.riExchange.getParentHTMLValue('SalesAreaCode');
                this.riExchange.getParentHTMLValue('SalesAreaDesc');
                break;
            case 'ServiceArea':
                this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
                this.riExchange.getParentHTMLValue('BranchServiceAreaDesc');
                break;
        }
        this.setDropdownValues();

        this.enableDisableFilterFields();

    }

    private enableDisableFilterFields(): void {
        let groupBy = this.getControlValue('GroupBy');
        this.dropdown.dropdownSearchReason.isDisabled = (groupBy === 'LostBusiness' || this.pageParams.blnDrillDownLostBusinessCode);
        this.dropdown.marketSegment.isDisabled = (groupBy === 'MarketSegment' || this.pageParams.blnDrillDownMarketSegmentCode);
        this.dropdown.companySearch.isDisabled = (groupBy === 'Company' || this.pageParams.blnDrillDownCompanyCode);
    }

    private loadSysChar(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = this.getURLSearchParamObject();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.SystemCharNumber, [this.sysCharConstants.SystemCharEnableCompanyCode].toString());
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.SCEnableCompanyCode = data.records[0].Required;
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        if (this.getControlValue('GroupBy') !== 'None') {
            this.riGrid.AddColumn('GroupCode', 'LostBusiness', 'GroupCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumn('GroupDesc', 'LostBusiness', 'GroupDesc', MntConst.eTypeText, 30);
            this.riGrid.AddColumn('AnnualValue', 'LostBusiness', 'AnnualValue', MntConst.eTypeCurrency, 10);

            this.riGrid.AddColumnAlign('GroupCode', MntConst.eAlignmentCenter);

            this.riGrid.AddColumnOrderable('GroupCode', true);
            this.riGrid.AddColumnOrderable('AnnualValue', true);
        } else {
            this.riGrid.AddColumn('ContractNumber', 'LostBusiness', 'ContractNumber', MntConst.eTypeCode, 9, true);
            this.riGrid.AddColumn('ContractName', 'LostBusiness', 'ContractName', MntConst.eTypeText, 30);
            this.riGrid.AddColumn('PremiseNumber', 'LostBusiness', 'PremiseNumber', MntConst.eTypeInteger, 5, true);
            this.riGrid.AddColumn('ProductCode', 'LostBusiness', 'ProductCode', MntConst.eTypeCode, 8, true);
            this.riGrid.AddColumn('CustomerTypeCode', 'LostBusiness', 'CustomerTypeCode', MntConst.eTypeCode, 12);
            this.riGrid.AddColumn('InactiveEffectDate', 'LostBusiness', 'InactiveEffectDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('ProcessedDate', 'LostBusiness', 'ProcessedDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('AnnualValue', 'LostBusiness', 'AnnualValue', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumn('Duration', 'LostBusiness', 'Duration', MntConst.eTypeText, 10);
            this.riGrid.AddColumn('ValueChangeSystemDesc', 'LostBusiness', 'ValueChangeSystemDesc', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('NegBranchNumber', 'LostBusiness', 'NegBranchNumber', MntConst.eTypeInteger, 2);
            this.riGrid.AddColumn('LostBusinessCode', 'LostBusiness', 'LostBusinessCode', MntConst.eTypeCode, 2);
            this.riGrid.AddColumn('LostBusinessDetailDesc', 'LostBusiness', 'LostBusinessDetailDesc', MntConst.eTypeText, 40);

            this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('CustomerTypeCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('InactiveEffectDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('ServiceEffectDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('ProcessedDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('NegBranchNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('LostBusinessCode', MntConst.eAlignmentCenter);

            this.riGrid.AddColumnOrderable('ContractNumber', true);
            this.riGrid.AddColumnOrderable('AnnualValue', true);

            this.dateCollist = [6, 7, 8].join(',');
        }

        this.riGrid.Complete();
    }

    private setDropdownValues(): void {
        this.dropdown.marketSegment.active = {
            id: this.getControlValue('MarketSegmentCode'),
            text: this.getControlValue('MarketSegmentCode') ? this.getControlValue('MarketSegmentCode') + ' - ' + this.getControlValue('MarketSegmentDesc') : ''
        };
        this.dropdown.dropdownSearchReason.active = {
            id: this.getControlValue('LostBusinessCode'),
            text: this.getControlValue('LostBusinessCode') ? this.getControlValue('LostBusinessCode') + ' - ' + this.getControlValue('LostBusinessDesc') : ''
        };
        this.dropdown.companySearch.active = {
            id: this.getControlValue('CompanyCode'),
            text: this.getControlValue('CompanyCode') ? this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc') : ''
        };
    }

    private enableEllipsisControls(): void {
        if (this.parentMode === 'ServiceArea' || this.parentMode === 'SalesArea' || this.parentMode === 'Employee') {
            if (this.getControlValue('BranchServiceAreaCode')) {
                this.disableControl('BranchServiceAreaCode', true);
                this.isServiceAreaEnabled = false;
            }
            if (this.getControlValue('SalesAreaCode')) {
                this.disableControl('SalesAreaCode', true);
                this.isSalesAreaEnabled = false;
            }
            if (this.getControlValue('EmployeeCode')) {
                this.disableControl('EmployeeCode', true);
                this.isEmployeeEnabled = false;
            }
        }
    }

    public resetGrid(): void {
        this.pageParams.gridConfig.totalRecords = 0;
        this.pageParams.gridCurrentPage = 1;
        this.riGrid.Clear();
        this.buildGrid();
        this.riGrid.RefreshRequired();
    }

    public onGridBodyDoubleClick(): void {
        let queryParams: any = {};
        let groupBy: string = this.getControlValue('GroupBy');
        switch (this.riGrid.CurrentColumnName) {
            case 'GroupCode':
                switch (groupBy) {
                    case 'Region':
                        this.displayMessage(MessageConstant.Message.PageNotDeveloped);
                        break;
                    case 'Branch':
                        queryParams['BranchNumber'] = this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName);
                        queryParams['BranchName'] = this.riGrid.Details.GetValue('GroupDesc');
                        queryParams['type'] = 'Branch';
                        this.navigate('LostBusinessAnalysis', BIReportsRoutes.ICABSARLOSTBUSINESSANALYSISGRID, queryParams);
                        break;
                    default:
                        let code: string = (groupBy === 'ServiceArea' ? 'Branch' : '') + groupBy + 'Code';
                        let desc: string = '';
                        let route: string = BIReportsRoutes[('ICABSARLOSTBUSINESSANALYSIS' + (this.type === 'main' ? 'BUSINESS' : '') + 'GRIDGROUPBY')] + '/' + groupBy.toLowerCase();
                        switch (groupBy) {
                            case 'ServiceArea':
                                desc = 'Branch' + groupBy + 'Desc';
                                break;
                            case 'Employee':
                                desc = 'EmployeeSurname';
                                break;
                            default:
                                desc = groupBy + 'Desc';
                                break;
                        }
                        queryParams[code] = this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName);
                        queryParams[desc] = this.riGrid.Details.GetValue('GroupDesc');
                        queryParams['type'] = this.type;
                        queryParams['group'] = 'GROUPBY';
                        this.navigate(this.getControlValue('GroupBy'), route, queryParams);
                        break;

                }
                break;
            case 'ContractNumber':
                this.navigate('LostBusinessAnalysis', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                    'CurrentContractTypeURLParameter': 'C',
                    'ContractNumber': this.riGrid.Details.GetValue('ContractNumber')
                });
                break;
            case 'PremiseNumber':
                this.navigate('LostBusinessAnalysis', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    ContractNumber: this.riGrid.Details.GetValue('ContractNumber'),
                    ContractName: this.riGrid.Details.GetValue('ContractName'),
                    PremiseNumber: this.riGrid.Details.GetValue('PremiseNumber'),
                    contractTypeCode: 'C'
                });
                break;
            case 'ProductCode':
                this.navigate('LostBusinessAnalysis', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                    ContractNumber: this.riGrid.Details.GetValue('ContractNumber'),
                    ContractName: this.riGrid.Details.GetValue('ContractName'),
                    PremiseNumber: this.riGrid.Details.GetValue('PremiseNumber'),
                    ProductCode: this.riGrid.Details.GetValue('ProductCode'),
                    ServiceCoverRowID: this.riGrid.Details.GetAttribute('ProductCode', 'rowid'),
                    currentContractType: 'C'
                });
                break;
        }
    }

    public getCurrentPage(data: any): any {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridConfig.gridCacheRefresh = false;
            super.getCurrentPage(data);
        }
    }

    public onGroupByChanges(): void {
        this.enableDisableFilterFields();
        if (this.getControlValue('GroupBy') === 'MarketSegment') {
            this.setControlValue('MarketSegmentCode', '');
            this.setControlValue('MarketSegmentDesc', '');
        }
        if (this.getControlValue('GroupBy') === 'Company') {
            this.setControlValue('CompanyCode', '');
            this.setControlValue('CompanyDesc', '');
        }
        if (this.getControlValue('GroupBy') === 'LostBusiness') {
            this.setControlValue('LostBusinessCode', '');
            this.setControlValue('LostBusinessDesc', '');
        }
        this.setDropdownValues();
        this.buildGrid();
        this.hasGridData = false;
    }

    public onRiGridRefresh(): void {
        if (this.uiForm.valid)
            this.populateGrid();
    }

    public onDropdownDataRecieved(event: any): void {
        this.setControlValue('MarketSegmentCode', event['MarketSegment.MarketSegmentCode']);
        this.setControlValue('MarketSegmentDesc', event['MarketSegment.MarketSegmentDesc']);
        this.buildGrid();
        this.hasGridData = false;
    }

    public onChangeLostBusinessCode(event: any): void {
        this.setControlValue('LostBusinessCode', event['LostBusinessLang.LostBusinessCode']);
        this.setControlValue('LostBusinessDesc', event['LostBusiness.LostBusinessSystemDesc']);
        this.resetGrid();
    }

    public onChangeCompany(event: any): void {
        this.setControlValue('CompanyCode', event.CompanyCode);
        this.setControlValue('CompanyDesc', event.CompanyDesc);
        this.resetGrid();
    }

    public onBranchTypeChange(): void {
        this.isBranchTypeNeg = this.getControlValue('BranchType') === 'Neg';
        if (this.isBranchTypeNeg) {
            this.setControlValue('SalesAreaCode', '');
            this.setControlValue('SalesAreaDesc', '');
            this.setControlValue('BranchServiceAreaCode', '');
            this.setControlValue('BranchServiceAreaDesc', '');
        }

        this.disableControl('SalesAreaCode', this.isBranchTypeNeg);
        this.disableControl('BranchServiceAreaCode', this.isBranchTypeNeg);
    }

    public onFiltersForBranchChange(field: string): void {
        switch (field) {
            case 'Employee':
                this.employee = this.getControlValue('EmployeeCode');
                if (!this.employee) {
                    this.setControlValue('EmployeeSurname', '');
                } else {
                    this.commonLookup.getEmployeeSurname(this.employee).then(data => {
                        if (data && data[0] && data[0][0]) {
                            this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
                        } else {
                            this.setControlValue('EmployeeSurname', '');
                            this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Employee');
                        }
                    }).catch(error => {
                        this.displayMessage(error);
                    });
                }
                break;
            case 'Sales Area':
                this.salesArea = this.getControlValue('SalesAreaCode');
                if (!this.salesArea) {
                    this.setControlValue('SalesAreaDesc', '');
                } else {
                    this.commonLookup.getSalesAreaDesc(this.salesArea, this.getControlValue('BranchNumber')).then(data => {
                        if (data && data[0] && data[0][0]) {
                            this.setControlValue('SalesAreaDesc', data[0][0].SalesAreaDesc);
                        } else {
                            this.setControlValue('SalesAreaDesc', '');
                            this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Sales Area');
                        }
                    }).catch(error => {
                        this.displayMessage(error);
                    });
                }
                break;
            case 'Service Area':
                this.serviceArea = this.getControlValue('BranchServiceAreaCode');
                if (!this.serviceArea) {
                    this.setControlValue('BranchServiceAreaDesc', '');
                } else {
                    this.commonLookup.getBranchServiceAreaDesc(this.serviceArea, this.getControlValue('BranchNumber')).then(data => {
                        if (data && data[0] && data[0][0]) {
                            this.setControlValue('BranchServiceAreaDesc', data[0][0].BranchServiceAreaDesc);
                        } else {
                            this.setControlValue('BranchServiceAreaDesc', '');
                            this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Service Area');
                        }
                    }).catch(error => {
                        this.displayMessage(error);
                    });
                }
                break;
        }
        this.resetGrid();
    }

    public onFiltersForBranchEllipsisSelect(data: any, field: string): void {
        switch (field) {
            case 'Employee':
                this.employee = data.EmployeeCode;
                this.setControlValue('EmployeeCode', data.EmployeeCode);
                this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                break;
            case 'Sales Area':
                this.salesArea = data.SalaeAreaCode;
                this.setControlValue('SalesAreaCode', data.SalesAreaCode);
                this.setControlValue('SalesAreaDesc', data.SalesAreaDesc);
                break;
            case 'Service Area':
                this.salesArea = data.ServiceAreaCode;
                this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
                this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
                break;
        }
        this.resetGrid();
    }
}
