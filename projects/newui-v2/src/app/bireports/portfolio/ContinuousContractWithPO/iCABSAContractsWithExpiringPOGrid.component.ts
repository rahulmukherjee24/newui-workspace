import { OnInit, AfterContentInit, Injector, Component, ViewChild } from '@angular/core';

import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { ContractManagementModuleRoutes } from '@app/base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { IExportOptions } from '@app/base/ExportConfig';

@Component({
    templateUrl: 'iCABSAContractsWithExpiringPOGrid.html'
})

export class ContractWithExpiringPOGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public controls: IControls[] = [
        { name: 'BranchName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'NegSalesEmployee', disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'NegServBranch', value: 'Neg' },
        { name: 'QueryLevel', value: 'Contract' },
        { name: 'RegionCode' },
        { name: 'SelectDateTo', required: true, type: MntConst.eTypeDate },
        { name: 'UsePOExpiryDate', value: 'Yes' }
    ];
    public pageId: string = '';
    public pageTitle: string;
    public commonGridFunction: CommonGridFunction;
    public exportConfig: IExportOptions = {};

    constructor(injector: Injector, private sysCharConstants: SysCharConstants) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSACONTRACTSWITHEXPIRINGPOGRID;
        this.browserTitle = this.pageTitle = `Continuous Contracts with PO's`;
    }
    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.pageParams.gridConfig = {
            totalRecords: 1,
            gridHandle: this.utils.randomSixDigitString(),
            pageSize: 10
        };
        this.pageParams.gridCacheRefresh = true;
        this.pageParams.gridCurrentPage = 1;
        if (!this.isReturning()) {
            switch (this.parentMode) {
                case 'Business':
                case 'Region':
                    this.setControlValue('BranchNumber', this.riExchange.getParentAttributeValue('BranchNumber'));
                    this.setControlValue('BranchName', this.riExchange.getParentAttributeValue('BranchName'));
                    this.setControlValue('SelectDateTo', this.riExchange.getParentHTMLValue('SelectDateTo'));
                    if (this.parentMode === 'Business') {
                        this.setControlValue('RegionCode', '');
                    }
                    if (this.parentMode === 'Region') {
                        this.setControlValue('RegionCode', this.riExchange.getParentAttributeValue('RegionCode'));
                    }
                    break;
                default:
                    this.setControlValue('BranchNumber', this.utils.getBranchCode());
                    this.setControlValue('BranchName', this.utils.getBranchTextOnly());
                    let date = new Date();
                    let selectToDate = new Date(date.getFullYear(), date.getMonth() + 2, 0);
                    this.setControlValue('SelectDateTo', this.globalize.parseDateToFixedFormat(selectToDate) as string);
                    break;
            }

            this.loadSysChars();
        } else {
            this.pageParams.gridCacheRefresh = false;
            this.buildGrid();
            this.populateGrid();
        }

    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = this.getURLSearchParamObject();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.SystemCharNumber, [this.sysCharConstants.SystemCharEnablePORefsAtServiceCoverLevel].toString());
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.glSCPORefsAtServiceCover = data.records[0].Required;
            this.buildGrid();
            if (this.parentMode) {
                this.populateGrid();
            }
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
                this.pageParams.glSCPORefsAtServiceCover = false;
            });
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('ContractNumber', 'ContractPOExpiry', 'ContractNumber', MntConst.eTypeCode, 8, true);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('AccountNumber', 'ContractPOExpiry', 'AccountNumber', MntConst.eTypeText, 9, true);
        this.riGrid.AddColumnAlign('AccountNumber', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContractName', 'ContractPOExpiry', 'ContractName', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('ContractName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('InvoiceAnnivDate', 'ContractPOExpiry', 'InvoiceAnnivDate', MntConst.eTypeDate, 5);
        this.riGrid.AddColumnAlign('InvoiceAnnivDate', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('InvoiceFrequencyCode', 'ContractPOExpiry', 'InvoiceFrequencyCode', MntConst.eTypeInteger, 3, true);
        this.riGrid.AddColumnAlign('InvoiceFrequencyCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('NextAPIDate', 'ContractPOExpiry', 'NextAPIDate', MntConst.eTypeDate, 5);
        this.riGrid.AddColumnAlign('NextAPIDate', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContractSalesEmployee', 'ContractPOExpiry', 'ContractSalesEmployee', MntConst.eTypeText, 6);
        this.riGrid.AddColumnAlign('ContractSalesEmployee', MntConst.eAlignmentCenter);

        if (this.getControlValue('QueryLevel') === 'Premise') {
            this.riGrid.AddColumn('PremiseNumber', 'ContractPOExpiry', 'PremiseNumber', MntConst.eTypeInteger, 5, true);
            this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('BranchNumber', 'ContractPOExpiry', 'BranchNumber', MntConst.eTypeInteger, 3, true);
            this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('PremiseCommenceDate', 'ContractPOExpiry', 'PremiseCommenceDate', MntConst.eTypeDate, 5);
            this.riGrid.AddColumnAlign('PremiseCommenceDate', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('PurchaseOrderNo', 'ContractPOExpiry', 'PurchaseOrderNo', MntConst.eTypeText, 15);
            this.riGrid.AddColumnAlign('PurchaseOrderNo', MntConst.eAlignmentLeft);
        }
        if (this.getControlValue('QueryLevel') === 'ServiceCover') {
            this.riGrid.AddColumn('PremiseNumber', 'ContractPOExpiry', 'PremiseNumber', MntConst.eTypeInteger, 5, true);
            this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('BranchNumber', 'ContractPOExpiry', 'BranchNumber', MntConst.eTypeInteger, 3, true);
            this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ServiceCommenceDate', 'ContractPOExpiry', 'ServiceCommenceDate', MntConst.eTypeDate, 5);
            this.riGrid.AddColumnAlign('ServiceCommenceDate', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('ProductCode', 'ContractPOExpiry', 'ProductCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('PurchaseOrderNo', 'ContractPOExpiry', 'PurchaseOrderNo', MntConst.eTypeText, 15);
            this.riGrid.AddColumnAlign('PurchaseOrderNo', MntConst.eAlignmentLeft);
        }
        this.riGrid.AddColumn('PurchaseOrderExpiryDate', 'ContractPOExpiry', 'PurchaseOrderExpiryDate', MntConst.eTypeDate, 5);
        this.riGrid.AddColumnAlign('PurchaseOrderExpiryDate', MntConst.eAlignmentLeft);

        if (this.getControlValue('QueryLevel') === 'Contract') {
            this.riGrid.AddColumn('ContractAnnualValue', 'ContractPOExpiry', 'ContractAnnualValue', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumnAlign('ContractAnnualValue', MntConst.eAlignmentRight);

            this.riGrid.AddColumn('NoPremises', 'ContractPOExpiry', 'NoPremises', MntConst.eTypeText, 3);
            this.riGrid.AddColumnAlign('NoPremises', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('PremiseAnnualValue', 'ContractPOExpiry', 'PremiseAnnualValue', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumnAlign('PremiseAnnualValue', MntConst.eAlignmentRight);

            if (this.pageParams.glSCPORefsAtServiceCover) {
                this.riGrid.AddColumn('NoCovers', 'ContractPOExpiry', 'NoCovers', MntConst.eTypeText, 3);
                this.riGrid.AddColumnAlign('NoCovers', MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('ServiceAnnualValue', 'ContractPOExpiry', 'ServiceAnnualValue', MntConst.eTypeCurrency, 10);
                this.riGrid.AddColumnAlign('ServiceAnnualValue', MntConst.eAlignmentRight);
            }

        }
        if (this.getControlValue('QueryLevel') === 'Premise') {
            this.riGrid.AddColumn('PremiseAnnualValue', 'ContractPOExpiry', 'PremiseAnnualValue', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumnAlign('PremiseAnnualValue', MntConst.eAlignmentRight);
            if (this.pageParams.glSCPORefsAtServiceCover) {
                this.riGrid.AddColumn('NoCovers', 'ContractPOExpiry', 'NoCovers', MntConst.eTypeText, 3);
                this.riGrid.AddColumnAlign('NoCovers', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('ServiceAnnualValue', 'ContractPOExpiry', 'ServiceAnnualValue', MntConst.eTypeCurrency, 10);
                this.riGrid.AddColumnAlign('ServiceAnnualValue', MntConst.eAlignmentRight);
            }
        }
        if (this.getControlValue('QueryLevel') === 'ServiceCover') {
            this.riGrid.AddColumn('ServiceAnnualValue', 'ContractPOExpiry', 'ServiceAnnualValue', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumnAlign('ServiceAnnualValue', MntConst.eAlignmentRight);

        }
        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('PurchaseOrderExpiryDate', true);
        this.riGrid.Complete();

        this.exportConfig = {};
        this.dateCollist = this.riGrid.getColumnIndexList([
            'InvoiceAnnivDate',
            'NextAPIDate',
            'PurchaseOrderExpiryDate'
        ]);
        if (this.dateCollist.length) {
            this.exportConfig.dateColumns = this.dateCollist;
        }
    }

    public populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');

            let formData: Object = {
                'Level': 'Branch',
                'SummaryOnly': 'true'
            };

            Object.keys(this.uiForm.controls).forEach(control => {
                formData = {
                    ...formData,
                    [control]: this.getControlValue(control)
                };
            });


            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'Application/iCABSAContractsWithExpiringPOGrid', search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.displayMessage(data);
                    } else {
                        this.riGrid.RefreshRequired();
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                            }, 500);
                        }
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }

    public onUsePOExpiryDateChange(): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SelectDateTo', this.getControlValue('UsePOExpiryDate') === 'Yes');
    }

    public cacheRefreshOnHeaderClick(): void {
        this.pageParams.gridCacheRefresh = false;
    }

    public onGridBodyDoubleClick(): void {
        let rowID: string = this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'rowID');
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractNumber':
                this.navigate('ContractPOExpiryGrid', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                    ContractRowID: rowID,
                    ContractNumber: this.riGrid.Details.GetValue('ContractNumber')
                });
                break;
            case 'AccountNumber':
                this.navigate('ContractPOExpiryGrid', ContractManagementModuleRoutes.ICABSAACCOUNTMAINTENANCE, {
                    'AccountRowID': rowID,
                    'AccountNumber': this.riGrid.Details.GetValue('AccountNumber')
                });
                break;
            case 'PremiseNumber':
                this.navigate('ContractPOExpiryGrid', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    'PremiseRowID': rowID
                });
                break;
        }
    }

    public onQueryLevelChange(): void {
        this.buildGrid();
    }
}
