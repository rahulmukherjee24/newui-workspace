export interface ContractDueToKeyValueInterface {
    key: string;
    value: string;
}

export interface ContractDueToHttpParamsInterface {
    method: string;
    module: string;
    operation: string;
}

export interface ContractDueToFormControlInterface {
    name: string;
    commonValidator?: any;
    disabled?: boolean;
    required?: boolean;
    type?: string;
    value?: any;
}

export interface ContractDueToGridConfigInterface {
    pageSize: number;
    currentPage: number;
    totalRecords: number;
    action: string;
    gridCacheRefresh: boolean;
    gridHandle: string;
}
