import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { Component, Injector, OnInit, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';

import { ContractDueToKeyValueInterface, ContractDueToHttpParamsInterface, ContractDueToFormControlInterface } from './iCABSAContractsDue.interface';
import { AppModuleRoutes, ContractManagementModuleRoutes } from '@app/base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSAContractsDueToExpireGrid.html',
    providers: [CommonLookUpUtilsService]
})
export class ContractDueToExpireGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private httpParams: ContractDueToHttpParamsInterface = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Application/iCABSAContractsDueToExpireGrid'
    };
    private cacheGridData: any;

    protected pageId: string;
    public controls: ContractDueToFormControlInterface[] = [
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'BranchNumber', type: MntConst.eTypeText, required: true },
        { name: 'DateType', type: MntConst.eTypeText, value: 'Expiry' },
        { name: 'menu', type: MntConst.eTypeText, value: 'Summary' },
        { name: 'SelectDate', type: MntConst.eTypeDate, value: '', required: true },
        { name: 'SelectDateFrom', type: MntConst.eTypeDate, value: '' },
        { name: 'TierCodeFilter', type: MntConst.eTypeText, value: 'all' },
        { name: 'ViewType', type: MntConst.eTypeText, value: 'Contracts' },
        { name: 'RegionCode', type: MntConst.eTypeCode }

    ];

    public viewTypeValues: ContractDueToKeyValueInterface[] = [
        { key: 'Contracts', value: 'Contracts' },
        { key: 'Jobs', value: 'Jobs' },
        { key: 'Both', value: 'Both' }
    ];

    public tierCodeValues: Array<Record<string, string>> = [];

    public levelValues: ContractDueToKeyValueInterface[] = [
        { key: 'Summary', value: 'Summary' },
        { key: 'Detail', value: 'Detail' }
    ];

    public dateTypeValues: ContractDueToKeyValueInterface[] = [
        { key: 'AutoRenewal', value: 'Auto Renewal' },
        { key: 'DurationPeriod', value: 'Minimum Duration' },
        { key: 'Expiry', value: 'Fixed Period' }
    ];

    public RegionCode: string;
    public storeSubscription: Subscription;
    public storeData: any;

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.pageTitle = 'Contracts Due to Expire';
        this.pageId = PageIdentifier.ICABSARCONTRACTDUETOEXPIREGRID;
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('rowid') || this.utils.getBranchCode());
        this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('branchname') || this.utils.getBranchTextOnly());
        if (this.getControlValue('BranchNumber') === '0') {
            this.setControlValue('BranchName', 'Total');
        }

        this.commonLookup.getTierCode(this.utils.getBusinessCode()).then(data => {
            this.tierCodeValues = data[0];
        });
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.setControlValue('SelectDate', new Date(new Date().getFullYear(), new Date().getMonth(), 1));
        this.setControlValue('SelectDateFrom', new Date(new Date().getFullYear(), 0, 1));
        if (this.isReturning()) {
            this.pageParams.gridConfig.gridCacheRefresh = false;
            this.restorePageState();
        } else {
            //load grid on page load
            this.pageParams.gridConfig = {
                action: '2',
                gridCacheRefresh: true,
                gridHandle: this.utils.randomSixDigitString(),
                pageSize: 10,
                totalRecords: 1
            };
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.blnSummaryOnly = true;
            this.buildGrid();
            this.parentMode = this.parentMode || this.riExchange.getParentHTMLValue('pageType');
            switch (this.parentMode) {
                case 'Business':
                    this.setControlValue('RegionCode', 'Total');
                    this.setControlValue('SelectDateFrom', this.riExchange.getParentHTMLValue('dateFrom'));
                    this.setControlValue('SelectDate', this.riExchange.getParentHTMLValue('dateTo'));
                    this.setControlValue('DateType', this.riExchange.getParentHTMLValue('dateType'));
                    this.riExchange.getParentHTMLValue('ViewType');
                    this.populateGrid();
                    break;
                case 'Region':
                    break;
                case 'Branch':
                    this.utils.getRegionCode().subscribe((data) => {
                        if (!this.riExchange.getParentHTMLValue('pageType')) {
                            this.setControlValue('DateType', this.riExchange.getParentHTMLValue('dateType'));
                            this.setControlValue('RegionCode', '');
                        } else {
                            this.setControlValue('RegionCode', data);
                        }
                    });
                    break;
            }
        }
    }

    private restorePageState(): void {
        this.setControlValue('SelectDate', this.pageParams[this.pageId].dateTo);
        this.setControlValue('SelectDateFrom', this.pageParams[this.pageId].dateFrom);
        this.buildGrid();
        this.populateGrid();
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;

        const dateTypeValue: string = this.getControlValue('DateType');
        this.riGrid.AddColumn('ContractNumber', 'ContractExpiry', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnOrderable('ContractNumber', true);

        this.riGrid.AddColumn('AccountNumber', 'ContractExpiry', 'AccountNumber', MntConst.eTypeText, 9);
        this.riGrid.AddColumnAlign('AccountNumber', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnOrderable('AccountNumber', true);

        this.riGrid.AddColumn('ContractAddress', 'ContractExpiry', 'ContractAddress', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('ContractAddress', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContractCommenceDate', 'ContractExpiry', 'ContractCommenceDate', MntConst.eTypeDate, 5);
        this.riGrid.AddColumnAlign('ContractCommenceDate', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnOrderable('ContractCommenceDate', true);

        this.riGrid.AddColumn('ContractExpiryDate', 'ContractExpiry', 'ContractExpiryDate', MntConst.eTypeDate, 5);
        this.riGrid.AddColumnAlign('ContractExpiryDate', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('ContractExpiryDate', true);

        if (dateTypeValue.toLowerCase() === 'autorenewal') {
            this.riGrid.AddColumn('AnniversaryDate', 'ContractExpiry', 'AnniversaryDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('AnniversaryDate', MntConst.eAlignmentLeft);
        }

        this.riGrid.AddColumn('CustomerTier', 'ContractExpiry', 'CustomerTier', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('CustomerTier', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnOrderable('CustomerTier', true);

        this.riGrid.AddColumn('ContractAnnualValue', 'ContractExpiry', 'ContractAnnualValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('ContractAnnualValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('ContractAnnualValue', true);
        if (!this.pageParams.blnSummaryOnly) {
            this.riGrid.AddColumn('PremiseNumber', 'ContractExpiry', 'PremiseNumber', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('ProductCode', 'ContractExpiry', 'ProductCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('ServiceVisitFrequency', 'ContractExpiry', 'ServiceVisitFrequency', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ServiceQuantity', 'ContractExpiry', 'ServiceQuantity', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ServiceAnnualValue', 'ContractExpiry', 'ServiceAnnualValue', MntConst.eTypeCurrency, 8);
            this.riGrid.AddColumnAlign('ServiceAnnualValue', MntConst.eAlignmentRight);
        }
        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let form: any = {}, gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.pageParams.gridConfig.action.toString());

        form['BranchNumber'] = this.getControlValue('BranchNumber');
        form['ContractType'] = this.getControlValue('ViewType');
        form['DateType'] = this.getControlValue('DateType');
        form['Level'] = 'Branch';
        form['RegionCode'] = this.getControlValue('RegionCode') === 'Total' ? '' : this.getControlValue('RegionCode');
        form['SelectDate'] = this.getControlValue('SelectDate');
        form['SelectDateFrom'] = this.getControlValue('SelectDateFrom');
        form['SummaryOnly'] = this.pageParams.blnSummaryOnly;
        form['TierCode'] = this.getControlValue('TierCodeFilter');

        form[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
        form[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
        form[this.serviceConstants.PageCurrent] = (this.pageParams.gridCurrentPage) ? this.pageParams.gridCurrentPage : '1';
        form[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.httpParams.method, this.httpParams.module, this.httpParams.operation, gridSearch, form)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (!data || data['errorMessage']) {
                        this.displayMessage(data);
                        return;
                    }

                    this.riGrid.RefreshRequired();
                    this.pageParams.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                    if (this.isReturning()) {
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 500);
                    }
                    this.cacheGridData = data;
                    this.riGrid.Execute(data);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.displayMessage(error);
                });
    }

    public onChangeFilterBy(e: any, val: string): void {
        switch (val) {
            case 'TierCodeFilter':
                this.setControlValue('TierCodeFilter', e.target.value);
                break;
            case 'ViewType':
                this.setControlValue('ViewType', e.target.value);
                break;
            case 'menu':
                this.setControlValue('menu', e.target.value);
                this.pageParams.blnSummaryOnly = (e.target.value === 'Summary') ? true : false;

                break;
            case 'DateType':
                this.setControlValue('DateType', e.target.value);
                break;
        }
        this.resetGrid();
        this.pageParams.gridCurrentPage = 1;
    }

    public resetGrid(): void {
        this.pageParams.gridConfig.totalRecords = 0;
        this.pageParams.gridCurrentPage = 1;
        this.riGridPagination.setPage(1);
        this.riGrid.Clear();
        this.buildGrid();
        this.riGrid.RefreshRequired();
    }

    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public getCurrentPage(event: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridConfig.gridCacheRefresh = false;
            super.getCurrentPage(event);
        }
        this.pageParams.gridConfig.gridCacheRefresh = true;
    }

    public onGridBodyDoubleClick(e: any): void {
        const cellValue: string = this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName),
            currentColumnName: string = this.riGrid.CurrentColumnName,
            queryParams: any = {
                parentMode: 'ContractExpiryGrid'
            };
        this.pageParams[this.pageId] = {};
        this.pageParams[this.pageId].dateFrom = this.getControlValue('SelectDateFrom');
        this.pageParams[this.pageId].dateTo = this.getControlValue('SelectDate');
        let currContractUrlParam: any = this.riGrid.Details.GetAttribute('ContractNumber', 'AdditionalProperty');
        switch (currentColumnName.toLowerCase()) {
            case 'contractnumber':
                queryParams.ContractNumber = cellValue;
                if (cellValue && this.riGrid.Details.GetAttribute('ContractNumber', 'rowid') !== 'Total') {
                    switch (currContractUrlParam) {
                        case 'C':
                            this.navigate('ContractExpiryGrid', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, queryParams);
                            break;
                        case 'J':
                            this.navigate('ContractExpiryGrid', ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE, queryParams);
                            break;
                    }
                }
                break;
            case 'premisenumber':
                queryParams.PremiseRowID = this.riGrid.Details.GetAttribute('PremiseNumber', 'rowid');
                if (cellValue) {
                    this.navigate('ContractExpiryGrid', AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESMAINTENANCE + ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE_SUB, queryParams);
                }
                break;
        }
    }
}
