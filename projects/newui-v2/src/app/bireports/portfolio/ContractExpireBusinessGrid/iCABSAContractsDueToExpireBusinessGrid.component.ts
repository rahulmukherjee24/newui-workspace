import { MessageConstant } from '@shared/constants/message.constant';
import { BIReportsRoutes } from '@app/base/PageRoutes';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Component, Injector, OnInit, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';

import { ContractActionTypes } from '@app/actions/contract';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSAContractsDueToExpireBusinessGrid.html'
})
export class ContractExpireBusinessGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private httpParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Application/iCABSAContractsDueToExpireBusinessGrid'
    };
    private cacheGridData: any;

    protected pageId: string;
    public controls: any = [
        { name: 'BusinessCode', type: MntConst.eTypeText, required: true },
        { name: 'BusinessDesc', type: MntConst.eTypeText },
        { name: 'DateType', type: MntConst.eTypeText, value: 'Expiry' },
        { name: 'DateFrom', type: MntConst.eTypeDate, value: '' },
        { name: 'DateTo', type: MntConst.eTypeDate, value: '', required: true },
        { name: 'ViewType', type: MntConst.eTypeText, value: 'Contracts' },
        { name: 'ViewBy', type: MntConst.eTypeText, value: 'Branch' }
    ];

    public viewFilterValues: any = [
        { key: 'Contracts', value: 'Contracts' },
        { key: 'Jobs', value: 'Jobs' },
        { key: 'Both', value: 'Both' }
    ];

    public dateFilterValues: any = [
        { key: 'Expiry', value: 'Expiry' },
        { key: 'Duration Period', value: 'Duration Period' }
    ];

    public viewByFilterValues: any = [
        { key: 'Branch', value: 'Branch' },
        { key: 'Region', value: 'Region' }
    ];

    public gridConfig: any = {
        action: '2',
        currentPage: 1,
        gridCacheRefresh: true,
        gridHandle: this.utils.randomSixDigitString(),
        pageSize: 10,
        totalRecords: 1
    };
    public storeSubscription: Subscription;
    public storeData: any;

    constructor(injector: Injector, private store: Store<any>) {
        super(injector);

        this.storeSubscription = store.select('contract').subscribe(data => {
            this.storeData = data;
        });
        this.pageTitle = 'Contracts / Jobs Due to Expire by Business';
        this.pageId = PageIdentifier.ICABSARCONTRACTEXPIREBUSINESSGRID;
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.setControlValue('BusinessCode', this.businessCode());
        this.utils.getBusinessDesc(this.utils.getBusinessCode(), this.countryCode()).subscribe((data) => {
            this.setControlValue('BusinessDesc', data.BusinessDesc);
        });
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.buildGrid();
        this.setControlValue('DateFrom', new Date(new Date().getFullYear(), 0, 1));
        this.setControlValue('DateTo', new Date(new Date().getFullYear(), new Date().getMonth(), 1));
        if (this.isReturning()) {
            this.cacheGridData = this.storeData.contract_expire_business.gridData;
            this.restoreGridState();
        }
    }

    private restoreGridState(): void {
        this.riGrid.RefreshRequired();
        this.gridConfig.currentPage = this.cacheGridData.pageData ? this.cacheGridData.pageData.pageNumber : 1;
        this.gridConfig.totalRecords = this.cacheGridData.pageData ? this.cacheGridData.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
        this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
        this.setControlValue('DateFrom', this.pageParams[this.pageId].dateFrom);
        this.setControlValue('DateTo', this.pageParams[this.pageId].dateTo);
        this.riGrid.Execute(this.cacheGridData);

        //clear store
        this.store.dispatch({
            type: ContractActionTypes.CLEAR_SET_CONTRACT_EXPIRE_BUSINESS,
            payload: ''
        });
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;

        this.riGrid.AddColumn('RowCode', 'BusinessGrid', 'RowCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('RowCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('RowCode', true);

        this.riGrid.AddColumn('RowDesc', 'BusinessGrid', 'RowDesc', MntConst.eTypeText, 30);
        this.riGrid.AddColumnOrderable('RowDesc', true);

        this.riGrid.AddColumn('NumContracts', 'BusinessGrid', 'NumContracts', MntConst.eTypeCode, 11);
        this.riGrid.AddColumnOrderable('NumContracts', true);
        this.riGrid.AddColumnAlign('NumContracts', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('AnnualValue', 'BusinessGrid', 'AnnualValue', MntConst.eTypeCurrency, 5);
        this.riGrid.AddColumnAlign('AnnualValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('AnnualValue', true);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let dateFrom: string = this.getControlValue('DateFrom');
        let dateTo: string = this.getControlValue('DateTo');
        if (!dateTo) {
            return;
        }
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());
        let form: any = {};
        form['BusinessCode'] = this.getControlValue('BusinessCode');
        form['ContractType'] = this.getControlValue('ViewType');
        form['DateType'] = this.getControlValue('DateType');
        form['Level'] = 'Business';
        form['SummaryOnly'] = 'true';
        form['SelectDateFrom'] = dateFrom;
        form['SelectDateTo'] = dateTo;
        form['ViewBy'] = this.getControlValue('ViewBy');
        form[this.serviceConstants.GridCacheRefresh] = this.gridConfig.gridCacheRefresh;
        form[this.serviceConstants.GridHandle] = this.gridConfig.gridHandle;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
        form[this.serviceConstants.PageCurrent] = (this.pageParams.gridCurrentPage) ? this.pageParams.gridCurrentPage : this.gridConfig.currentPage;
        form[this.serviceConstants.PageSize] = this.gridConfig.pageSize;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.httpParams.method, this.httpParams.module, this.httpParams.operation, gridSearch, form)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (!data || data['errorMessage']) {
                        this.displayMessage(data);
                        return;
                    }

                    this.riGrid.RefreshRequired();
                    this.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                    if (this.isReturning()) {
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 500);
                    }
                    this.cacheGridData = data;
                    this.riGrid.Execute(data);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.displayMessage(error);
                });
    }

    public resetGrid(): void {
        this.gridConfig.totalRecords = 0;
        this.pageParams.gridCurrentPage = 1;
        this.riGridPagination.setPage(1);
        this.riGrid.Clear();
        this.buildGrid();
        this.riGrid.RefreshRequired();
    }

    public onChangeFilterBy(e: any, val: string): void {
        switch (val) {
            case 'contractType':
                this.setControlValue('ViewType', e.target.value);
                break;
            case 'dateType':
                this.setControlValue('DateType', e.target.value);
                break;
            case 'viewBy':
                this.setControlValue('ViewBy', e.target.value);
                break;
        }
        this.pageParams.gridCurrentPage = 1; //IUI-23983
        this.riGrid.RefreshRequired();
        this.resetGrid();
    }

    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public getCurrentPage(event: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.gridConfig.gridCacheRefresh = false;
            super.getCurrentPage(event);
        }
    }

    public onGridBodyDoubleClick(e: any): void {
        const rowid: string = this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'rowid'),
            cellValue: string = this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName),
            branchname = this.riGrid.Details.GetValue('RowDesc'),
            currentColumnName: string = this.riGrid.CurrentColumnName,
            pageObj: any = {
                gridData: this.cacheGridData
            };
        if (currentColumnName === 'RowCode') {
            this.store.dispatch({
                type: ContractActionTypes.SET_CONTRACT_EXPIRE_BUSINESS,
                payload: pageObj
            });
            const queryParams: any = {
                BusinessCode: this.getControlValue('BusinessCode'),
                rowid: rowid === 'Total' ? 0 : rowid,
                branchname: branchname,
                dateFrom: this.getControlValue('DateFrom'),
                dateTo: this.getControlValue('DateTo'),
                dateType: this.getControlValue('DateType') === 'Expiry' ? 'Expiry' : 'DurationPeriod'
            };
            this.pageParams[this.pageId] = {};
            this.pageParams[this.pageId].dateFrom = this.getControlValue('DateFrom');
            this.pageParams[this.pageId].dateTo = this.getControlValue('DateTo');
            this.pageParams[this.pageId].queryParamsOnDrillDown = queryParams;

            switch (this.getControlValue('ViewBy')) {
                case 'Region':
                    this.displayMessage(MessageConstant.Message.PageNotDeveloped);
                    break;
                case 'Branch':
                    this.navigate('Business', BIReportsRoutes.ICABSARCONTRACTDUETOEXPIREGRID, queryParams);
                    break;
            }
        }

    }
}
