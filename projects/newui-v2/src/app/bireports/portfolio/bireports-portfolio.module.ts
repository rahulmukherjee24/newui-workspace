import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BIReportsRoutesConstant } from '@base/PageRoutes';
import { SharedModule } from '@shared/shared.module';

import { DailyTransactionGridComponent } from './iCABSARDailyTransactionsGrid.component';
import { DailyTransactionBusinessGridComponent } from './iCABSARDailyTransactionsBusinessGrid.component';
import { PortfolioReportDetailsComponent } from './iCABSARPortfolioReportDetail.component';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { SuspendedPortfolioBranchComponent } from './SuspendedPortfolio/iCABSASuspendedPortfolioBranch';
import { LostBusinessAnalysisGridComponent } from './LostBusinessAnalysis/iCABSARLostBusinessAnalysisGrid.component';
import { PortfolioReportComponent } from './PortfolioByBusiness/iCABSARPortfolioReport';
import { InternalSearchModule } from '@app/internal/search.module';
import { ContractReportComponent } from './ContractJob/iCABSARContractReport.component';
import { ContractExpireBusinessGridComponent } from './ContractExpireBusinessGrid/iCABSAContractsDueToExpireBusinessGrid.component';
import { ContractDueToExpireGridComponent } from './ContractDueToExpireGrid/iCABSAContractsDueToExpireGrid.component';
import { NettgainBusinessComponent } from './PortfolioMovement/iCABSSNettGainBusiness.component';
import { NettGainDetailComponent } from './PortfolioMovement/iCABSSNettGainDetail.component';
import { ContractsWithExpiringPOBBusinessGridComponent } from './ContinuousContractWithPO/iCABSAContractsWithExpiringPOBusinessGrid.component';
import { ContractWithExpiringPOGridComponent } from './ContinuousContractWithPO/iCABSAContractsWithExpiringPOGrid.component';
import { AccountsByCategoryComponent } from './iCABSARAccountsByCategory.component';
import { UnAllocatedUnitsComponent } from './iCABSAUnallocatedUnitsGrid.component';
@Component({
    template: `<router-outlet></router-outlet>`
})

export class PortfolioBIReportsRootComponent {

    constructor() {
    }
}

@NgModule({
    exports: [
    ],
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        InternalSearchModule,
        RouterModule.forChild([
            {
                path: '', component: PortfolioBIReportsRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSARDAILYTRANSACTIONSBUSINESSGRID, component: DailyTransactionBusinessGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARDAILYTRANSACTIONSGRID, component: DailyTransactionGridComponent },
                    { path: BIReportsRoutesConstant.ICABSASUSPENDEDPORTFOLIOBRANCH, component: SuspendedPortfolioBranchComponent },
                    { path: BIReportsRoutesConstant.ICABSASUSPENDEDPORTFOLIOBUSINESS, component: SuspendedPortfolioBranchComponent },
                    { path: BIReportsRoutesConstant.ICABSARPORTFOLIOREPORTDETAIL, component: PortfolioReportDetailsComponent },
                    { path: BIReportsRoutesConstant.ICABSARPORTFOLIOREPORTCONTRACTDETAIL, component: PortfolioReportDetailsComponent },
                    { path: BIReportsRoutesConstant.ICABSARPORTFOLIOREPORT, component: PortfolioReportComponent },
                    { path: BIReportsRoutesConstant.ICABSARPORTFOLIOREPORTREGION, component: PortfolioReportComponent },
                    { path: BIReportsRoutesConstant.ICABSARPORTFOLIOREPORTBRANCH, component: PortfolioReportComponent },
                    { path: BIReportsRoutesConstant.ICABSARCONTRACTREPORT, component: ContractReportComponent },
                    { path: BIReportsRoutesConstant.ICABSARCONTRACTEXPIREBUSINESSGRID, component: ContractExpireBusinessGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARCONTRACTDUETOEXPIREGRID, component: ContractDueToExpireGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISBUSINESSGRID, component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISBUSINESSGRIDGROUPBY, component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISGRID, component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISGRIDGROUPBY, component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISGRIDGROUPBY + '/employee', component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISGRIDGROUPBY + '/salesarea', component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISGRIDGROUPBY + '/servicearea', component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISGRIDGROUPBY + '/marketsegment', component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISGRIDGROUPBY + '/lostbusiness', component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISGRIDGROUPBY + '/company', component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISBUSINESSGRIDGROUPBY + '/lostbusiness', component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISBUSINESSGRIDGROUPBY + '/marketsegment', component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISBUSINESSGRIDGROUPBY + '/company', component: LostBusinessAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSNETTGAINBUSINESS, component: NettgainBusinessComponent },
                    { path: BIReportsRoutesConstant.ICABSSNETTGAINBRANCH, component: NettgainBusinessComponent },
                    { path: BIReportsRoutesConstant.ICABSSNETTGAINDETAIL, component: NettGainDetailComponent },
                    { path: BIReportsRoutesConstant.ICABSACONTRACTSWITHEXPIRINGPOBUSINESSGRID, component: ContractsWithExpiringPOBBusinessGridComponent },
                    { path: BIReportsRoutesConstant.ICABSACONTRACTSWITHEXPIRINGPOGRID, component: ContractWithExpiringPOGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARACCOUNTSBYCATEGORY, component: AccountsByCategoryComponent },
                    { path: BIReportsRoutesConstant.ICABSAUNALLOCATEDUNITSGRID, component: UnAllocatedUnitsComponent }
                ], data: { domain: 'BI REPORTS PORTFOLIO' }
            }

        ])
    ],
    declarations: [
        PortfolioBIReportsRootComponent,
        DailyTransactionBusinessGridComponent,
        DailyTransactionGridComponent,
        SuspendedPortfolioBranchComponent,
        PortfolioReportDetailsComponent,
        PortfolioReportComponent,
        ContractReportComponent,
        ContractExpireBusinessGridComponent,
        ContractDueToExpireGridComponent,
        LostBusinessAnalysisGridComponent,
        ContractReportComponent,
        NettgainBusinessComponent,
        NettGainDetailComponent,
        ContractsWithExpiringPOBBusinessGridComponent,
        ContractWithExpiringPOGridComponent,
        AccountsByCategoryComponent,
        UnAllocatedUnitsComponent
    ]

})

export class PortfolioBIReportsModule { }
