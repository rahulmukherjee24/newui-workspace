import { Component, OnInit, OnDestroy, Injector, ViewChild, AfterContentInit } from '@angular/core';
import { BCompanySearchComponent } from '@internal/search/iCABSBCompanySearch';
import { ContractManagementModuleRoutes, InternalGridSearchServiceModuleRoutes, BIReportsRoutes } from '@base/PageRoutes';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ServiceTypeSearchComponent } from '@app/internal/search/iCABSBServiceTypeSearch.component';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { IControls } from '@app/base/ControlsType';

@Component({
    templateUrl: 'iCABSARReleasedForInvoiceGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class ReleasedForInvoiceGridComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit, IGridHandlers {

    @ViewChild('companyDropdown') companyDropdown: BCompanySearchComponent;
    @ViewChild('errorModal') public errorModal: any;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('serviceTypeSearch') serviceTypeSearch: ServiceTypeSearchComponent;

    public commonGridFunction: CommonGridFunction;

    public alertMessage: any;
    public isDateMandatory: boolean = true;
    public isShowEmployee: boolean = true;
    public messageType: string;
    public optionReportTypeList: Array<any> = [];
    public pageId: string;
    public pageType: string = '';
    public search: QueryParams;
    public totalItems: number;
    public viewReport: Array<any> = [];

    public controls: IControls[] = [
        { name: 'BranchName', disabled: true },
        { name: 'BranchNumber', disabled: true },
        { name: 'BusinessDesc', disabled: true },
        { name: 'BusinessCode', disabled: true },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'CompanyCode', type: MntConst.eTypeCode },
        { name: 'CompanyDesc', type: MntConst.eTypeText },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'ReportType', value: 'NotReleased' },
        { name: 'ServiceTypeCode', type: MntConst.eTypeCode },
        { name: 'ServiceTypeDesc', type: MntConst.eTypeText },
        { name: 'ViewReportBy' }
    ];

    public viewReportByBranch: Array<Object> = [
        { key: 'Employee', value: 'Employee' },
        { key: 'Service Type', value: 'ServiceType' }
    ];

    public viewReportByBusiness: Array<Object> = [
        { key: 'Branch', value: 'Branch' },
        { key: 'Region', value: 'Region' }
    ];

    public viewReportByRegion: Array<Object> = [
        { key: 'Branch', value: 'Branch' },
        { key: 'Employee', value: 'ServiceArea' }
    ];


    public dropdown: Record<string, Record<string, Object>> = {
        company: {
            params: {
                businessCode: this.businessCode(),
                countryCode: this.countryCode(),
                parentMode: 'LookUp'
            },
            active: { id: '', text: '' }
        },
        serviceTypeSearch: {
            params: {
                parentMode: 'LookUp'
            },
            active: { id: '', text: '' }
        }
    };

    public ellipsisConfig: Record<string, Record<string, Object>> = {
        employee: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: EmployeeSearchComponent
        }
    };

    constructor(injector: Injector, public sysCharConstants: SysCharConstants, private commonLookupUtil: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    ngAfterContentInit(): void {
        this.pageType = this.utils.capitalizeFirstLetter(this.riExchange.getParentHTMLValue('pageType'));
        this.pageParams.currentContractType = this.riExchange.getParentHTMLValue('contractType');
        this.pageId = this.pageType ? PageIdentifier['ICABSARRELEASEDFORINVOICE' + this.pageType.toUpperCase() + 'GRID'] : PageIdentifier.ICABSARRELEASEDFORINVOICEGRID;
        let contractLabel = this.utils.getCurrentContractLabel(this.pageParams.currentContractType);
        this.pageTitle = this.pageType ? this.pageType + ' Value Of Released ' + contractLabel : 'Value Of ' + contractLabel + ' Invoices Detail';
        this.utils.setTitle(this.pageTitle);
        this.setRequiredStatus('EmployeeCode', !(this.pageType));
        this.loadSysChars();
        this.buildReportTypeOptions();
        this.viewReport = this['viewReportBy' + this.pageType] || [];
        super.ngAfterContentInit();
        this.onWindowLoad();
        this.reportTypeOnChange();
    }

    private onWindowLoad(): void {
        if (!this.isReturning()) {
            this.pageParams.gridConfig = {
                pageSize: 10,
                totalRecords: 1
            };
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.gridCurrentPage = 1;
            this.doEventsForParentMode();

        } else {
            this.pageParams.gridCacheRefresh = false;
            if (this.getControlValue('ServiceTypeCode')) {
                this.serviceTypeSearch.setValue(this.getControlValue('ServiceTypeCode'));
            }
            if (this.getControlValue('CompanyDesc')) {
                this.dropdown.company.active = {
                    id: this.getControlValue('CompanyCode'),
                    text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
                };
            }
            this.disableControl('EmployeeCode', this.pageParams.employeeDisable);
            this.setRequiredStatus('EmployeeCode', !this.pageParams.employeeDisable);
            if (this.getControlValue('EmployeeCode').toUpperCase() === 'TOTAL') {
                this.isShowEmployee = false;
            }
            this.populateGrid();
        }
    }

    public doEventsForParentMode(): void {
        switch (this.parentMode) {
            case 'Branch':
                this.riExchange.getParentHTMLValue('BusinessCode');
                if (this.pageType === 'Branch') {
                    this.setControlValue('BranchNumber', this.riExchange.getParentAttributeValue('BranchNumber'));
                    this.setControlValue('BranchName', this.riExchange.getParentAttributeValue('BranchName'));
                    this.riExchange.getParentHTMLValue('ServiceTypeCode');
                    this.riExchange.getParentHTMLValue('ServiceTypeDesc');
                } else {
                    this.riExchange.getParentHTMLValue('BranchNumber');
                    this.riExchange.getParentHTMLValue('BranchName');
                }
                this.setControlValue('ViewReportBy', this.riExchange.getParentAttributeValue('ViewReportBy'));
                this.setControlValue('EmployeeCode', this.riExchange.getParentAttributeValue('EmployeeCode'));
                this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('EmployeeSurname'));
                this.setControlValue('ReportType', this.riExchange.getParentHTMLValue('ReportType'));
                this.riExchange.getParentHTMLValue('DateFrom');
                this.riExchange.getParentHTMLValue('DateTo');
                this.isShowEmployee = !(this.getControlValue('EmployeeCode') === 'TOTAL');
                this.pageParams.employeeDisable = true;
                this.disableControl('EmployeeCode', true);
                this.setRequiredStatus('EmployeeCode', false);
                this.setCommonDropdown();
                break;
            case 'Business':
            case 'Region':
                if (this.pageType === 'Branch') {
                    this.setControlValue('ViewReportBy', 'Employee');
                    this.riExchange.getParentHTMLValue('ServiceTypeDesc');
                } else if (this.pageType === 'Region') {
                    this.setControlValue('ViewReportBy', 'Branch');
                    this.riExchange.getParentHTMLValue('RegionCode');
                } else {
                    this.setControlValue('EmployeeCode', this.riExchange.getParentAttributeValue('EmployeeCode'));
                    this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('EmployeeSurname'));
                    this.setControlValue('ViewReportBy', 'Employee');
                    this.disableControl('EmployeeCode', true);
                    this.pageParams.employeeDisable = true;
                }
                this.riExchange.getParentHTMLValue('BusinessCode');
                this.setControlValue('BranchNumber', this.riExchange.getParentAttributeValue('BranchNumber'));
                this.setControlValue('BranchName', this.utils.getBranchTextOnly(this.riExchange.getParentAttributeValue('BranchNumber')));
                this.riExchange.getParentHTMLValue('DateFrom');
                this.riExchange.getParentHTMLValue('DateTo');
                this.riExchange.getParentHTMLValue('ReportType');
                this.riExchange.getParentHTMLValue('ServiceTypeCode');
                this.setCommonDropdown();
                break;
            default:
                let branchText: Array<string> = this.utils.getBranchText().split('-');
                if (this.pageType === 'Business') {
                    this.setControlValue('BusinessCode', this.utils.getBusinessCode());
                    this.setControlValue('BusinessDesc', this.utils.getBusinessText());
                } else if (this.pageType === 'Region') {
                        this.commonLookupUtil.getRegionDesc().subscribe((data) => {
                            this.setControlValue('RegionCode', data[0][0]['RegionCode']);
                            this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
                        });
                } else {
                    this.setControlValue('BranchNumber', this.utils.getBranchCode());
                    this.setControlValue('BranchName', branchText[1]);
                }
                this.setControlValue('DateFrom', this.utils.formatDate(new Date(new Date().getFullYear(), 0, 1)));
                this.setControlValue('DateTo', this.utils.formatDate(new Date()));
                this.setControlValue('ViewReportBy', !this.pageType ? 'Employee' : this.pageType === 'Branch' ? 'Employee' : 'Branch');
                break;
        }
        if (this.parentMode && (this.parentMode.toLowerCase() === 'branch' || this.parentMode.toLowerCase() === 'region' || this.parentMode.toLowerCase() === 'business')) {
            this.onRiGridRefresh();
        }
    }

    private setCommonDropdown(): void {
        if (this.riExchange.getParentHTMLValue('CompanyDesc')) {
            this.dropdown.company.active = {
                id: this.riExchange.getParentHTMLValue('CompanyCode'),
                text: this.riExchange.getParentHTMLValue('CompanyCode') + ' - ' + this.riExchange.getParentHTMLValue('CompanyDesc')
            };
        }
        this.serviceTypeSearch.setValue(this.riExchange.getParentHTMLValue('ServiceTypeCode'));
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        let colName = ['JanValue', 'FebValue', 'MarValue', 'AprValue', 'MayValue', 'JunValue', 'JulValue', 'AugValue', 'SepValue', 'OctValue', 'NovValue', 'DecValue', 'TotalValue'];
        if (this.pageType) {
            if (this.pageType === 'Business') {
                this.riGrid.AddColumn('GroupCode', 'Released', 'GroupCode', MntConst.eTypeCode, 5, true);
                this.riGrid.AddColumnAlign('GroupCode', MntConst.eAlignmentCenter);
            }

            if (this.pageType === 'Branch') {
                if (this.getControlValue('ViewReportBy') === 'ServiceType') {
                    this.riGrid.AddColumn('ServiceTypeCode', 'Released', 'ServiceTypeCode', MntConst.eTypeCode, 6, true);
                    this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);
                } else if (this.getControlValue('ViewReportBy') === 'Employee') {
                    this.riGrid.AddColumn('EmployeeCode', 'Released', 'EmployeeCode', MntConst.eTypeCode, 6, true);
                    this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
                }
            }

            if (this.pageType === 'Region') {
                if (this.getControlValue('ViewReportBy') === 'ServiceArea') {
                    this.riGrid.AddColumn('BranchName','Released','BranchName',MntConst.eTypeText,5);
                    this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentCenter);
                }
                this.riGrid.AddColumn('BranchNumber','Released','BranchNumber',MntConst.eTypeCode,5, true);
                this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
            }

            for (let idx = 0; idx < 13; idx++) {
                this.riGrid.AddColumn(colName[idx], 'Released', colName[idx], MntConst.eTypeTextFree, 10);
                this.riGrid.AddColumnAlign(colName[idx], MntConst.eAlignmentRight);
            }

        } else {
            this.riGrid.AddColumn('ContractNumber', 'Released', 'ContractNumber', MntConst.eTypeCode, 9, true);
            this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('PremiseNumber', 'Released', 'PremiseNumber', MntConst.eTypeInteger, 5, true);
            this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('PremiseName', 'Released', 'PremiseName', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('ProductCode', 'Released', 'ProductCode', MntConst.eTypeCode, 8, true);
            this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
            if (this.pageParams.currentContractType.toUpperCase() === 'J') {
                this.riGrid.AddColumn('ServiceVisitFrequency', 'Released', 'ServiceVisitFrequency', MntConst.eTypeInteger, 3);
                this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('VisitsDone', 'Released', 'VisitsDone', MntConst.eTypeInteger, 3);
                this.riGrid.AddColumnAlign('VisitsDone', MntConst.eAlignmentCenter);
            }
            this.riGrid.AddColumn('ServiceCommenceDate', 'Released', 'ServiceCommenceDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('ServiceCommenceDate', MntConst.eAlignmentCenter);
            if (this.pageParams.currentContractType.toUpperCase() === 'P') {
                this.riGrid.AddColumn('InvoicePerDelivery', 'Released', 'InvoicePerDelivery', MntConst.eTypeImage, 1);
                this.riGrid.AddColumnAlign('InvoicePerDelivery', MntConst.eAlignmentCenter);
            }
            if (this.pageParams.currentContractType.toUpperCase() === 'J') {
                this.riGrid.AddColumn('InvoiceReleasedDate', 'Released', 'InvoiceReleasedDate', MntConst.eTypeDate, 10);
                this.riGrid.AddColumnAlign('InvoiceReleasedDate', MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('Invoiced', 'Released', 'Invoiced', MntConst.eTypeImage, 1);
                this.riGrid.AddColumnAlign('Invoiced', MntConst.eAlignmentCenter);
            }
            if (this.pageParams.currentContractType.toUpperCase() === 'P') {
                this.riGrid.AddColumn('OrderQuantity', 'Released', 'OrderQuantity', MntConst.eTypeTextFree, 10);
                this.riGrid.AddColumnAlign('OrderQuantity', MntConst.eAlignmentRight);
                this.riGrid.AddColumn('DeliveredQuantity', 'Released', 'DeliveredQuantity', MntConst.eTypeTextFree, 10);
                this.riGrid.AddColumnAlign('DeliveredQuantity', MntConst.eAlignmentRight);
            }
            this.riGrid.AddColumn('ServiceAnnualValue', 'Released', 'ServiceAnnualValue', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumnAlign('ServiceAnnualValue', MntConst.eAlignmentRight);
            if (this.pageParams.currentContractType.toUpperCase() === 'P') {
                this.riGrid.AddColumn('OSValue', 'Released', 'OSValue', MntConst.eTypeTextFree, 10);
                this.riGrid.AddColumnAlign('OSValue', MntConst.eAlignmentRight);
            }
            this.riGrid.AddColumn('BranchServiceAreaCode', 'Released', 'BranchServiceAreaCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);
            if (this.pageParams.currentContractType.toUpperCase() === 'J') {
                this.riGrid.AddColumn('SupervisorEmployeeCode', 'Released', 'SupervisorEmployeeCode', MntConst.eTypeCode, 6);
                this.riGrid.AddColumnAlign('SupervisorEmployeeCode', MntConst.eAlignmentCenter);
                this.riGrid.AddColumnOrderable('SupervisorEmployeeCode', true);
                this.riGrid.AddColumn('ServiceCoverDetail', 'Released', 'ServiceCoverDetail', MntConst.eTypeText, 20);
            }
            this.riGrid.AddColumnOrderable('ContractNumber', true);
            this.riGrid.AddColumnOrderable('ServiceCommenceDate', true);
            this.riGrid.AddColumnOrderable('BranchServiceAreaCode', true);
        }
        this.riGrid.Complete();
        this.dateCollist = this.riGrid.getColumnIndexList(['ServiceCommenceDate']);
    }

    private populateGrid(): void {
        this.buildGrid();
        this.validateScreenParameters();
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        const urlPageType = this.pageType === 'Region' ? 'Branch' : this.pageType;
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, '2');
        let form: any = {};
        form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        form[this.serviceConstants.Level] = this.pageType ? this.pageType : 'Detail';
        form['CompanyCode'] = this.getControlValue('CompanyCode');
        form['ContractTypeCode'] = this.pageParams.currentContractType;
        if (this.pageType !== 'Business') {
            form[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        }
        if (this.pageType === 'Region') {
            form[this.serviceConstants.RegionCode] = this.getControlValue('RegionCode');
            form['ReportBy'] = this.getControlValue('ViewReportBy');
        } else {
            form['ViewBy'] = this.getControlValue('ViewReportBy');
        }
        form['DateFrom'] = this.getControlValue('DateFrom');
        form['DateTo'] = this.getControlValue('DateTo');
        form['ReportType'] = this.getControlValue('ReportType');
        form['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
        if (!this.pageType) {
            form[this.serviceConstants.EmployeeCode] = this.getControlValue('EmployeeCode');
        }
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.PageSize] = '10';
        form[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        form[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
        this.ajaxSource.next(this.ajaxconstant.START);

        this.ajaxSubscription = this.httpService.makePostRequest('bi/reports', 'reports', 'ApplicationReport/iCABSARReleasedForInvoice' + urlPageType + 'Grid', gridSearch, form)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.commonGridFunction.setPageData(data);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.pageParams.hasGridData = false;
                    this.displayMessage(error);
                });
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    public buildReportTypeOptions(): void {
        this.optionReportTypeList = [];
        this.optionReportTypeList.push({ value: 'NotReleased', text: 'Not Released' });
        if (this.pageParams.currentContractType.toUpperCase() === 'J') {
            this.optionReportTypeList.push({ value: 'Released', text: 'Released' });
            this.optionReportTypeList.push({ value: 'IncompleteVisitTriggeredJobs', text: 'Incomplete Visit Triggered Jobs' });
        }
        this.optionReportTypeList.push({ value: 'NotInvoiced', text: 'Released Not Invoiced' });
        this.optionReportTypeList.push({ value: 'Invoiced', text: 'Released and Invoiced' });
    }

    public onGridBodyDoubleClick(): void {
        let contractNumber = this.riGrid.Details.GetValue('ContractNumber');
        let params: any = {};
        for (let control in this.uiForm.controls) {
            if (true)
                params[control] = this.getControlValue(control);
        }
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractNumber':
                if (!contractNumber || contractNumber === 'TOTAL')
                    return;
                this.navigate('InvoiceReleased', (this.pageParams.currentContractType.toUpperCase() === 'P' ? ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE : ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE), {
                    'currentContractTypeURLParameter': this.pageParams.currentContractType,
                    'ContractNumber': contractNumber
                });
                break;
            case 'PremiseNumber':
                if (!this.riGrid.Details.GetAttribute('PremiseNumber', 'rowid'))
                    return;
                this.navigate('InvoiceReleased', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    'ContractTypeCode': this.pageParams.currentContractType,
                    'PremiseRowID': this.riGrid.Details.GetAttribute('PremiseNumber', 'rowid'),
                    'PremiseNumber': this.riGrid.Details.GetValue('PremiseNumber'),
                    'ContractNumber': contractNumber
                });
                break;
            case 'ProductCode':
                if (!this.riGrid.Details.GetAttribute('ProductCode', 'rowid'))
                    return;
                if (this.pageParams.currentContractType.toUpperCase() === 'P') {
                    this.navigate('InvoiceReleased', InternalGridSearchServiceModuleRoutes.ICABSAPRODUCTSALESSCENTRYGRID.URL_1, {
                        'currentContractType': this.pageParams.currentContractType,
                        'PremiseRowID': this.riGrid.Details.GetAttribute('PremiseNumber', 'rowid'),
                        'PremiseNumber': this.riGrid.Details.GetValue('PremiseNumber'),
                        'ContractNumber': contractNumber
                    });

                } else {
                    this.navigate('InvoiceReleased', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                        'currentContractType': this.pageParams.currentContractType,
                        'ServiceCoverRowID': this.riGrid.Details.GetAttribute('ProductCode', 'rowid')
                    });
                }
                break;
            case 'EmployeeCode':
                params['EmployeeCode'] = this.riGrid.Details.GetAttribute('EmployeeCode', 'rowid');
                params['EmployeeSurname'] = this.riGrid.Details.GetAttribute('EmployeeCode', 'title');
                params['contractType'] = this.pageParams.currentContractType;
                this.navigate('Branch', this.pageParams.currentContractType.toUpperCase() === 'J' ? BIReportsRoutes.ICABSARRELEASEDFORJOBINVOICEGRID : BIReportsRoutes.ICABSARRELEASEDFORPRODUCTINVOICEGRID, params);
                break;
            case 'ServiceTypeCode':
                if (this.riGrid.Details.GetAttribute('ServiceTypeCode', 'rowid').toUpperCase() === 'TOTAL') {
                    return;
                }
                params['ServiceTypeCode'] = this.riGrid.Details.GetAttribute('ServiceTypeCode', 'rowid');
                params['contractType'] = this.pageParams.currentContractType;
                this.navigate('Branch', this.pageParams.currentContractType.toUpperCase() === 'J' ? BIReportsRoutes.ICABSARRELEASEDFORJOBINVOICEGRID : BIReportsRoutes.ICABSARRELEASEDFORPRODUCTINVOICEGRID, params);
                break;
            case 'GroupCode':
                if (this.riGrid.Details.GetAttribute('GroupCode', 'additionalProperty') === 'Branch') {
                    this.navigate('Business', BIReportsRoutes.ICABSARRELEASEDFORJOBINVOICEBRANCHGRID, {
                        'BranchNumber': this.riGrid.Details.GetValue('GroupCode'),
                        'contractType': 'J',
                        'pageType': 'Branch'
                    });
                } else {
                    this.navigate('Business', BIReportsRoutes.ICABSARRELEASEDFORJOBINVOICEREGIONGRID, {
                        'RegionCode': this.riGrid.Details.GetValue('GroupCode'),
                        'contractType': 'J',
                        'pageType': 'Region'
                    });
                }
                break;
            case 'BranchName':
                if (this.riGrid.Details.GetRowId('BranchName') === 'TOTAL') {
                    return;
                }
                this.setAttribute('EmployeeCode', this.riGrid.Details.GetAttribute('BranchName', 'rowid'));
                this.setAttribute('EmployeeSurname', this.riGrid.Details.GetAttribute('BranchName', 'title'));
                this.setAttribute('BranchNumber', this.riGrid.Details.GetAttribute('BranchNumber', 'rowid'));
                this.setAttribute('BranchName', this.riGrid.Details.GetAttribute('BranchNumber', 'title'));
                params['contractType'] = this.pageParams.currentContractType;
                this.navigate('Region', this.pageParams.currentContractType.toUpperCase() === 'J' ? BIReportsRoutes.ICABSARRELEASEDFORJOBINVOICEGRID : BIReportsRoutes.ICABSARRELEASEDFORPRODUCTINVOICEGRID, params);
                break;
            case 'BranchNumber':
                if (this.riGrid.Details.GetRowId('BranchNumber') === '' || this.riGrid.Details.GetRowId('BranchNumber') === 'TOTAL') {
                    return;
                }
                this.setAttribute('BranchNumber', this.riGrid.Details.GetAttribute('BranchNumber', 'rowid'));
                this.setAttribute('BranchName', this.riGrid.Details.GetAttribute('BranchNumber', 'title'));
                params['contractType'] = this.pageParams.currentContractType;
                if (this.getControlValue('ViewReportBy') === 'ServiceArea') {
                    this.setAttribute('EmployeeCode', this.riGrid.Details.GetAttribute('BranchName', 'rowid'));
                    this.setAttribute('EmployeeSurname', this.riGrid.Details.GetAttribute('BranchName', 'title'));
                    this.navigate('Region', this.pageParams.currentContractType.toUpperCase() === 'J' ? BIReportsRoutes.ICABSARRELEASEDFORJOBINVOICEGRID : BIReportsRoutes.ICABSARRELEASEDFORPRODUCTINVOICEGRID, params);
                } else {
                    params['pageType'] = 'Branch';
                    this.navigate('Region',this.pageParams.currentContractType.toUpperCase() === 'J' ? BIReportsRoutes.ICABSARRELEASEDFORJOBINVOICEBRANCHGRID : BIReportsRoutes.ICABSARRELEASEDFORPRODUCTINVOICEBRANCHGRID, params);
                }
                break;
        }
    }

    public reportTypeOnChange(): void {
        this.setRequiredStatus('DateFrom', !(this.getControlValue('ReportType') === 'NotReleased'));
        this.setRequiredStatus('DateTo', !(this.getControlValue('ReportType') === 'NotReleased'));
        this.isDateMandatory = !(this.getControlValue('ReportType') === 'NotReleased');
    }

    public validateScreenParameters(): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'DateFrom', this.getControlValue('ReportType') !== 'NotReleased');
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'DateTo', this.getControlValue('ReportType') !== 'NotReleased');
    }

    private doLookupForEmployee(employeeCodeControl: string, employeeDescControl: string): void {
        let lookupIP = [
            {
                'table': 'Employee',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'EmployeeCode': this.getControlValue(employeeCodeControl)
                },
                'fields': ['EmployeeSurname']
            }
        ];
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data && data[0].length > 0) {
                let Employee = data[0][0];
                this.setControlValue(employeeDescControl, Employee.EmployeeSurname);
            } else {
                this.setControlValue(employeeDescControl, '');
                this.setControlValue(employeeCodeControl, '');
                this.messageType = 'error';
                this.alertMessage = {
                    msg: MessageConstant.Message.RecordNotFound,
                    timestamp: (new Date()).getMilliseconds()
                };
            }
        });
    }

    public onEmployeeChange(): void {
        if (this.getControlValue('EmployeeCode')) {
            this.doLookupForEmployee('EmployeeCode', 'EmployeeSurname');
        } else {
            this.setControlValue('EmployeeSurname', '');
        }
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = new QueryParams();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.BusinessCode, this.businessCode());
        companySysChar.set(this.serviceConstants.CountryCode, this.countryCode());
        companySysChar.set(this.serviceConstants.SystemCharNumber, this.sysCharConstants.SystemCharEnableCompanyCode);
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            try {
                if (data.records) {
                    this.pageParams.vSCEnableCompanyCode = data.records[0].Required;
                }
            } catch (error) {
                let msgTxt: string = error.errorMessage;
                msgTxt += error.fullError ? ' - ' + error.fullError : '';
                this.alertMessage = {
                    msg: msgTxt,
                    timestamp: (new Date()).getMilliseconds()
                };
            }
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    public onEmployeeDataReceived(data: any): void {
        if (data) {
            this.setControlValue('EmployeeCode', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
    }

    public onServiceTypeChange(data: any): void {
        if (data) {
            this.setControlValue('ServiceTypeCode', data.ServiceTypeCode);
            this.setControlValue('ServiceTypeDesc', data.ServiceTypeDesc);
        }
    }

    public onCompanyChange(data: any): void {
        if (data) {
            this.setControlValue('CompanyCode', data.CompanyCode);
            this.setControlValue('CompanyDesc', data.CompanyDesc);
        }
    }
}
