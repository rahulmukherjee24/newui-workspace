import { Component, OnInit, Injector, AfterContentInit, ViewChild } from '@angular/core';

import { BIReportsRoutes } from './../../base/PageRoutes';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from './../../../shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { IGridHandlers } from './../../base/BaseComponentLight';
import { InvoiceCreditReasonLanguageSearchComponent } from '@app/internal/search/iCABSSInvoiceCreditReasonLanguageSearch.component';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { SysCharConstants } from './../../../shared/constants/syscharservice.constant';


@Component({
    templateUrl: 'iCABSARJobCreditBranchGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class JobCreditBranchGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('invoiceCreditReasonCode') invoiceCreditReasonCode: InvoiceCreditReasonLanguageSearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    public pageId: string;
    public commonGridFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public pageType: string;
    public parentMode: string;
    public companyInputParams: any = {
    };
    public companyDefault: Object = {
        id: '',
        text: ''
    };
    public dropdown: any = {
        invoiceCreditReasonCode: {
            isRequired: false,
            isDisabled: false,
            isTriggerValidate: false,
            params: {
                parentMode: 'LookUp-All'
            },
            isActive: {
                id: '',
                text: ''
            },
            displayFields: ['InvoiceCreditReasonLang.InvoiceCreditReasonCode', 'InvoiceCreditReasonLang.InvoiceCreditReasonDesc']
        }
    };
    public controls = [
        { name: 'BranchNumber', disabled: true, type: MntConst.eTypeCode, value: this.utils.getBranchCode() },
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText, value: this.utils.getBranchTextOnly() },
        { name: 'CompanyCode', disabled: true, type: MntConst.eTypeText },
        { name: 'CompanyDesc', disabled: true, type: MntConst.eTypeTextFree },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true },
        { name: 'InvoiceCreditReasonCode', type: MntConst.eTypeCode },
        { name: 'InvoiceCreditReasonDesc', type: MntConst.eTypeText },
        { name: 'EmployeeCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'EmployeeName', type: MntConst.eTypeText, disabled: true },
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ViewBy', value: 'Branch' }
    ];

    constructor(injector: Injector, public sysCharConstants: SysCharConstants, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.companyInputParams[this.serviceConstants.CountryCode] = this.countryCode();
        this.companyInputParams[this.serviceConstants.BusinessCode] = this.businessCode();
        this.companyInputParams['parentMode'] = 'LookUp';
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageType = this.riExchange.getParentHTMLValue('pageType');
        this.pageId = PageIdentifier['ICABSARJOBCREDIT' + this.pageType.toUpperCase() + 'GRID'];
        super.ngAfterContentInit();
        this.loadSysChars();
        this.onWindowLoad();
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let sysCharQuery: QueryParams = new QueryParams();
        let syscharList: Array<number> = [
            this.sysCharConstants.SystemCharEnableCompanyCode
        ];

        sysCharQuery.set(this.serviceConstants.Action, '0');
        sysCharQuery.set(this.serviceConstants.BusinessCode, this.businessCode());
        sysCharQuery.set(this.serviceConstants.CountryCode, this.countryCode());
        sysCharQuery.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));
        this.httpService.sysCharRequest(sysCharQuery).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            try {
                if (data.records) {
                    this.pageParams.vSCEnableCompanyCode = data.records[0].Required;
                }
            } catch (error) {
                this.displayMessage(error);
            }
        });
    }

    private onWindowLoad(): void {
        this.pageTitle = this.pageType + ' ' + 'Job Credit';
        this.browserTitle = this.pageType ? this.pageTitle + ' - ' + this.utils.getBusinessText() : 'Sales Analysis - ' + this.utils.getBusinessText();
        this.pageParams.gridHandle = this.utils.randomSixDigitString();
        this.parentMode = this.riExchange.getParentHTMLValue('parentMode');
        this.buildGrid();
        let companyCode: string = this.pageType ? this.getControlValue('CompanyCode') : this.riExchange.getParentHTMLValue('CompanyCode');
        let invoiceCreditReasonCode: string = this.pageType ? this.getControlValue('InvoiceCreditReasonCode') : this.riExchange.getParentHTMLValue('InvoiceCreditReasonCode');
        if (this.isReturning()) {
            if (companyCode) {
                this.companyDefault = {
                    id: companyCode,
                    text: companyCode + ' - ' + this.getControlValue('CompanyDesc')
                };
            }
            if (invoiceCreditReasonCode) {
                this.dropdown.invoiceCreditReasonCode.isActive = {
                    id: invoiceCreditReasonCode,
                    text: invoiceCreditReasonCode + ' - ' + this.getControlValue('InvoiceCreditReasonDesc')
                };
            }
            this.onRiGridRefresh();
        } else {
            this.pageParams.gridConfig = {
                itemsPerPage: 10,
                totalItem: 1
            };
            if (this.pageType) {
                this.commonLookup.setTradingDates(this.uiForm, 'DateFrom', 'DateTo');
                this.pageParams.gridCacheRefresh = true;
                this.pageParams.gridCurrentPage = 1;
            } else {
                this.setControlValue('CompanyCode', this.riExchange.getParentHTMLValue('CompanyCode'));
                this.setControlValue('CompanyDesc', this.riExchange.getParentHTMLValue('CompanyDesc'));
                this.setControlValue('DateFrom', this.riExchange.getParentHTMLValue('DateFrom'));
                this.setControlValue('DateTo', this.riExchange.getParentHTMLValue('DateTo'));
                this.setControlValue('InvoiceCreditReasonCode', this.riExchange.getParentHTMLValue('InvoiceCreditReasonCode'));
                this.setControlValue('InvoiceCreditReasonDesc', this.riExchange.getParentHTMLValue('InvoiceCreditReasonDesc'));
                if (this.riExchange.getParentHTMLValue('EmployeeCode')) {
                    this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
                    this.setControlValue('EmployeeName', this.riExchange.getParentHTMLValue('EmployeeName'));
                } else if (this.riExchange.getParentHTMLValue('BranchNumberSelected')) {
                    this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('BranchNumberSelected'));
                    this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('BranchNameSelected'));
                } else if (this.riExchange.getParentHTMLValue('RegionCodeSelected')) {
                    this.setControlValue('RegionCode', this.riExchange.getParentHTMLValue('RegionCodeSelected'));
                    this.setControlValue('RegionDesc', this.riExchange.getParentHTMLValue('RegionDescSelected'));
                }

                if (companyCode) {
                    this.companyDefault = {
                        id: companyCode,
                        text: companyCode + ' - ' + this.getControlValue('CompanyDesc')
                    };
                }
                if (invoiceCreditReasonCode) {
                    this.dropdown.invoiceCreditReasonCode.isActive = {
                        id: invoiceCreditReasonCode,
                        text: invoiceCreditReasonCode + ' - ' + this.getControlValue('InvoiceCreditReasonDesc')
                    };
                }
                this.disableControls([]);
                this.onRiGridRefresh();
            }
            if (this.parentMode === 'Business' || this.pageType === 'Business') {
                this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            } else if (this.parentMode === 'Region' || this.pageType === 'Region') {
                this.commonLookup.getRegionDesc().subscribe((data) => {
                    this.setControlValue('RegionCode', data[0][0]['RegionCode']);
                    this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
                });
                this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('BranchNumberSelected'));
                this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('BranchNameSelected'));
            }
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        if (this.pageType) {
            switch (this.pageType) {
                case 'Region':
                    this.riGrid.AddColumn('BranchNumber', 'Credit', 'BranchNumber', MntConst.eTypeCode, 7, true);
                    this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
                    this.riGrid.AddColumn('BranchName', 'Credit', 'BranchName', MntConst.eTypeText, 30);
                    break;
                case 'Business':
                    this.riGrid.AddColumn('GroupCode', 'Credit', 'GroupCode', MntConst.eTypeCode, 7, true);
                    this.riGrid.AddColumnAlign('GroupCode', MntConst.eAlignmentCenter);
                    this.riGrid.AddColumn('GroupDesc', 'Credit', 'GroupDesc', MntConst.eTypeText, 30);
                    break;
                case 'Branch':
                    this.riGrid.AddColumn('EmployeeCode', 'Credit', 'EmployeeCode', MntConst.eTypeCode, 7, true);
                    this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
                    this.riGrid.AddColumn('EmployeeName', 'Credit', 'EmployeeName', MntConst.eTypeText, 30);
                    break;
            }
            this.riGrid.AddColumn('CreditCount', 'Credit', 'CreditCount', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('CreditCount', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('CreditValue', 'Credit', 'CreditValue', MntConst.eTypeText, 15);
            this.riGrid.AddColumnAlign('CreditValue', MntConst.eAlignmentRight);
        } else {
            this.riGrid.AddColumn('SalesStatsProcessedDate', 'Credit', 'SalesStatsProcessedDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('SalesStatsProcessedDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ContractNumber', 'Credit', 'ContractNumber', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('PremiseNumber', 'Credit', 'PremiseNumber', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ProductCode', 'Credit', 'ProductCode', MntConst.eTypeCode, 10);
            this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('InvoiceCreditReason', 'Credit', 'InvoiceCreditReason', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('BranchNumber', 'Credit', 'BranchNumber', MntConst.eTypeInteger, 10);
            this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('Employee', 'Credit', 'Employee', MntConst.eTypeText, 10);
            this.riGrid.AddColumn('ProRataChargeValue', 'Credit', 'ProRataChargeValue', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('ProRataChargeValue', MntConst.eAlignmentRight);
            this.riGrid.AddColumnOrderable('SalesStatsProcessedDate', true);
            this.riGrid.AddColumnOrderable('ContractNumber', true);
            this.riGrid.AddColumnOrderable('InvoiceCreditReason', true);
            this.riGrid.AddColumnOrderable('BranchNumber', true);
            this.riGrid.AddColumnOrderable('Employee', true);
        }
        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            let formData: Object = {};

            formData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
            formData['CompanyCode'] = this.getControlValue('CompanyCode');
            formData['DateFrom'] = this.getControlValue('DateFrom');
            formData['DateTo'] = this.getControlValue('DateTo');
            formData['CreditReasonCode'] = this.getControlValue('InvoiceCreditReasonCode');
            if (this.pageType) {
                formData['Level'] = this.pageType;
            }
            if (this.pageType === 'Branch' || this.parentMode === 'Branch') {
                formData['BranchNumber'] = this.getControlValue('BranchNumber');
            }
            switch (this.pageType) {
                case 'Branch':
                    break;
                case 'Business':
                    formData['ViewType'] = this.getControlValue('ViewBy');
                    break;
                case 'Region':
                    formData['RegionCode'] = this.getControlValue('RegionCode');
                    break;
                default:
                    formData['level'] = 'detail';
                    formData['RowID'] = this.riExchange.getParentHTMLValue('RowID');
                    if (this.parentMode === 'Branch') {
                        formData['function'] = 'BranchDetailForEmployee';
                    } else if (this.parentMode === 'Business') {
                        formData['function'] = 'BusinessDetailFor' + this.riExchange.getParentHTMLValue('ViewBySelected');
                    } else if (this.parentMode === 'Region') {
                        formData['function'] = 'RegionDetailForBranch';
                    }
                    break;
            }

            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage || 1;
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.pageType === 'Branch' || this.pageType === 'Region' ? '' : this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = 'Descending';
            if (this.pageType)
                formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCurrentPage === 1 ? 'True' : '';

            this.ajaxSource.next(this.ajaxconstant.START);
            let operation: string;
            switch (this.pageType) {
                case 'Branch':
                case 'Business':
                case 'Region':
                    operation = 'ApplicationReport/iCABSARJobCredit';
                    break;
                default:
                    operation = 'ApplicationReport/iCABSARJobCreditDetail';
                    break;
            }
            this.httpService.xhrPost('bi/reports', 'reports', operation, search, formData).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.commonGridFunction.setPageData(data);
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.pageParams.hasGridData = false;
                    this.displayMessage(error);
                });
        }
    }

    public onDataRecieved(data: any, type: string): void {
        this.setControlValue('CompanyCode', data.CompanyCode);
        this.setControlValue('CompanyDesc', data.CompanyDesc);
        this.commonGridFunction.resetGrid();
    }

    public invoiceCreditReasonCodeSelected(data: any): void {
        if (data) {
            this.riGrid.HeaderClickedColumn = '';
            this.riGrid.DescendingSort = true;
            this.setControlValue('InvoiceCreditReasonCode', data['InvoiceCreditReasonLang.InvoiceCreditReasonCode'] || '');
            this.setControlValue('InvoiceCreditReasonDesc', data['InvoiceCreditReasonLang.InvoiceCreditReasonDesc'] || '');
            this.commonGridFunction.resetGrid();
        }
    }

    public onGridBodyDoubleClick(): void {
        let currentColumnName: string = this.riGrid.CurrentColumnName;
        if (
            (currentColumnName === 'EmployeeCode' && this.riGrid.Details.GetRowId('EmployeeCode') !== 'TOTAL')
            || (currentColumnName === 'GroupCode' && this.riGrid.Details.GetRowId('GroupCode') !== 'TOTAL')
            || (currentColumnName === 'BranchNumber' && this.riGrid.Details.GetRowId('BranchNumber') !== 'TOTAL')
        ) {
            let params: any = {
                'CompanyCode': this.getControlValue('CompanyCode'),
                'DateFrom': this.getControlValue('DateFrom'),
                'DateTo': this.getControlValue('DateTo'),
                'InvoiceCreditReasonCode': this.getControlValue('InvoiceCreditReasonCode'),
                'InvoiceCreditReasonDesc': this.getControlValue('InvoiceCreditReasonDesc')
            };
            switch (this.pageType) {
                case 'Branch':
                    params['EmployeeCode'] = this.riGrid.Details.GetValue('EmployeeCode');
                    params['EmployeeName'] = this.riGrid.Details.GetValue('EmployeeName');
                    params['RowID'] = this.riGrid.Details.GetAttribute('EmployeeCode', 'rowid');
                    break;
                case 'Business':
                    if (this.getControlValue('ViewBy') === 'Branch') {
                        params['BranchNumberSelected'] = this.riGrid.Details.GetValue('GroupCode');
                        params['BranchNameSelected'] = this.riGrid.Details.GetValue('GroupDesc');
                    } else {
                        params['RegionCodeSelected'] = this.riGrid.Details.GetValue('GroupCode');
                        params['RegionDescSelected'] = this.riGrid.Details.GetValue('GroupDesc');
                    }
                    params['RowID'] = this.riGrid.Details.GetAttribute('GroupCode', 'rowid');
                    params['ViewBySelected'] = this.getControlValue('ViewBy');
                    break;
                case 'Region':
                    params['BranchNumberSelected'] = this.riGrid.Details.GetValue('BranchNumber');
                    params['BranchNameSelected'] = this.riGrid.Details.GetValue('BranchName');
                    params['RowID'] = this.riGrid.Details.GetAttribute('BranchNumber', 'rowid');
                    break;
            }
            this.navigate(this.pageType, BIReportsRoutes.iCABSARJOBCREDITDETAIL, params);
        }
    }
}
