import { AfterContentInit, OnInit, Component, Injector, OnDestroy, ViewChild } from '@angular/core';

import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { ProductSearchGridComponent } from '@internal/search/iCABSBProductSearch';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { StaticUtils } from '@shared/services/static.utility';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSSSalesStatistics.html',
    providers: [CommonLookUpUtilsService]
})

export class SalesStatisticsComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    public pageId: string;
    public controls = [
        { name: 'BranchName', disabled: true },
        { name: 'BranchNumber', disabled: true },
        { name: 'BusinessCode', readonly: true, required: true },
        { name: 'BusinessDesc', readonly: true, disabled: true },
        { name: 'CompanyCode', type: MntConst.eTypeCode },
        { name: 'CompanyDesc', type: MntConst.eTypeText },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EmployeeCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'FilterBy', type: MntConst.eTypeText, value: 'ReportGroup' },
        { name: 'FilterCode', type: MntConst.eTypeCode },
        { name: 'FilterDesc', type: MntConst.eTypeText },
        { name: 'RegionName', disabled: true },
        { name: 'RegionNumber', disabled: true },
        { name: 'ReportGroup', type: MntConst.eTypeText },
        { name: 'ReportMode', type: MntConst.eTypeText, value: 'Processed' },
        { name: 'SalesType', type: MntConst.eTypeText, value: 'All' },
        { name: 'ViewMode', type: MntConst.eTypeText },
        { name: 'ViewType', type: MntConst.eTypeText, value: 'Branch' },
        { name: 'vMergeEmployees', type: MntConst.eTypeCheckBox }
    ];

    public bCode = this.businessCode();
    public curPageTitle: string;
    public dateCol: string;
    public hasGridData: boolean = false;
    public pageType: string;
    public reportGroup: Array<string>;
    public showEmployeeOnly: boolean = false;
    public showProductSearch: boolean = false;
    public valueColumnsCount: any = [];
    public viewModeIdx: number;
    public viewModeValues: any = [];

    public viewTypeValues: Array<any> = [
        { value: 'Branch', text: 'Branch' },
        { value: 'BranchProduct', text: 'Branch/Product' },
        { value: 'Product', text: 'Product' },
        { value: 'Region', text: 'Region' },
        { value: 'Sales Employee', text: 'Employee' }
    ];

    public viewTypeValuesBranch: Array<any> = [
        { value: 'Negotiating Employee', text: 'Negotiating Employee' },
        { value: 'Product', text: 'Product' },
        { value: 'Sales Employee', text: 'Sales Employee' }
    ];

    public viewTypeValuesEmp: Array<any> = [
        { value: 'Product', text: 'Product' }
    ];

    public viewTypeValuesRegion: Array<any> = [
        { value: 'Branch', text: 'Branch' },
        { value: 'Sales Employee', text: 'Employee' },
        { value: 'Product', text: 'Product' }
    ];

    public filterOnValues: any = [
        { text: 'Product Code', value: 'ProductCode' },
        { text: 'Report Group', value: 'ReportGroup' }
    ];

    public salesTypeValues: any = [
        { text: 'All', value: 'All' },
        { text: 'Sales Employee', value: 'SalesEmp' },
        { text: 'Service/Sales Emp', value: 'ServiceSales' }
    ];

    public reportModeValues: any = [
        { text: 'Processed', value: 'Processed' },
        { text: 'Effective', value: 'Effective' }
    ];

    public reportModeValuesEmp: any = [
        { text: 'Processed', value: 'Processed' },
        { text: 'ReportGroup', value: 'Report Group' },
        { text: 'Effective', value: 'Effective' }
    ];

    public dropdown: any = {
        company: {
            params: {
                businessCode: this.bCode,
                countryCode: this.countryCode(),
                parentMode: 'LookUp'
            },
            active: { id: '', text: '' }
        }
    };

    public ellipsis = {
        ProductSearchComponentEllipsis: {
            childparams: {
                'parentMode': 'LookUp-FilterCode'
            },
            component: ProductSearchGridComponent
        },
        employee: {
            showBranchLevel: false,
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: EmployeeSearchComponent
        }
    };

    constructor(injector: Injector, public sysCharConstants: SysCharConstants, private lookupUtils: CommonLookUpUtilsService, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.getReportGroupDD();
    }

    ngAfterContentInit(): void {
        this.pageType = StaticUtils.capitalizeFirstLetterAlphaNumberic(this.riExchange.getParentHTMLValue('pageType'));
        this.pageId = PageIdentifier.ICABSSSALESSTATISTICS + this.pageType.toUpperCase();
        this.curPageTitle = 'Sales Statistics - ' + this.pageType + ' - ';
        this.pageTitle = this.curPageTitle + 'Processed';
        this.utils.setTitle(this.pageTitle);
        super.ngAfterContentInit();
        this.loadSysChars();
        this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        if (this.isReturning()) {
            this.showProductSearch = (this.getControlValue('FilterBy') === 'ProductCode');
            this.pageTitle = this.curPageTitle + this.getControlValue('ReportMode');
            this.utils.setTitle(this.pageTitle);
            if (this.getControlValue('CompanyDesc')) {
                this.dropdown.company.active = {
                    id: this.getControlValue('CompanyCode'),
                    text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
                };
            }
            switch (this.pageType) {
                case 'Branch':
                    this.viewTypeValues = this.viewTypeValuesBranch;
                    break;
                case 'Business':
                    this.disableControl('EmployeeCode', false);
                    this.showEmployeeOnly = this.getControlValue('ViewType') === 'Branch';
                    break;
                case 'Employee':
                    this.viewTypeValues = this.viewTypeValuesEmp;
                    break;
                case 'Region':
                    this.viewTypeValues = this.viewTypeValuesRegion;
                    break;
            }
            this.buildGrid();
            this.populateGrid();
        } else {
            this.pageParams.gridConfig = {
                pageSize: 10,
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString(),
                gridCacheRefresh: true
            };
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.valueColumnsCount = [];
            this.pageParams.viewModeValues = [];

            this.doEventsForParentMode();
        }
    }

    private doEventsForParentMode(): void {
        const branchNumberByAttr: string = this.riExchange.getParentAttributeValue('Branch');
        const branchNumber: string = this.utils.getBranchCode();
        const filterBy: string = this.riExchange.getParentHTMLValue('FilterBy');
        const companyCode: string = this.riExchange.getParentHTMLValue('CompanyCode');
        this.setControlValue('FilterBy', 'ReportGroup');//IUI-24409

        switch (this.parentMode) {
            case 'employee':
                const EmpCode: string = this.riExchange.getParentAttributeValue('EmployeeCode');
                this.setControlValue('EmployeeCode', EmpCode);
                this.lookupUtils.getEmployeeSurname(EmpCode).then((data) => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
                    }
                });
                this.setControlValue('BranchNumber', branchNumberByAttr);
                this.riExchange.getParentHTMLValue('DateFrom');
                this.riExchange.getParentHTMLValue('DateTo');
                this.showProductSearch = (filterBy === 'ProductCode');
                if (this.showProductSearch) {
                    this.riExchange.getParentHTMLValue('FilterCode');
                    this.riExchange.getParentHTMLValue('FilterDesc');
                } else {
                    this.setControlValue('ReportGroup', this.riExchange.getParentHTMLValue('FilterCode'));
                }

                if (companyCode) {
                    this.dropdown.company.active = {
                        id: companyCode,
                        text: companyCode + ' - ' + this.riExchange.getParentHTMLValue('CompanyDesc')
                    };
                }
                this.viewTypeValues = this.viewTypeValuesEmp;
                this.reportModeValues = this.reportModeValuesEmp;
                this.setControlValue('ViewType', 'Product');
                this.riExchange.getParentHTMLValue('ViewMode');
                this.riExchange.getParentHTMLValue('SalesType');
                this.riExchange.getParentHTMLValue('ReportMode');
                this.onReportModeChange();
                break;
            case 'region':
                this.viewTypeValues = this.viewTypeValuesRegion;
                this.riExchange.getParentHTMLValue('DateFrom');
                this.riExchange.getParentHTMLValue('DateTo');
                this.riExchange.getParentHTMLValue('ReportMode');
                this.setControlValue('BranchNumber', branchNumberByAttr);
                this.setControlValue('BranchName', this.utils.getBranchTextOnly(branchNumberByAttr));
                this.onReportModeChange();
                break;
            case 'business':
                this.setControlValue('BranchNumber', branchNumberByAttr);
                this.setControlValue('BranchName', this.utils.getBranchTextOnly(branchNumberByAttr));
                this.riExchange.getParentHTMLValue('DateFrom');
                this.riExchange.getParentHTMLValue('DateTo');
                this.showProductSearch = (filterBy === 'ProductCode');
                if (this.showProductSearch) {
                    this.riExchange.getParentHTMLValue('FilterCode');
                    this.riExchange.getParentHTMLValue('FilterDesc');
                } else {
                    this.setControlValue('ReportGroup', this.riExchange.getParentHTMLValue('FilterCode'));
                }
                this.riExchange.getParentHTMLValue('SalesType');
                this.riExchange.getParentHTMLValue('ReportMode');
                this.riExchange.getParentHTMLValue('ViewMode');

                if (companyCode) {
                    this.dropdown.company.active = {
                        id: companyCode,
                        text: companyCode + ' - ' + this.riExchange.getParentHTMLValue('CompanyDesc')
                    };
                }

                if (this.pageType === 'Region') {
                    this.setControlValue('RegionNumber', this.riExchange.getParentHTMLValue('Region'));
                    this.commonLookup.getRegionDesc().subscribe((data) => {
                        this.setControlValue('RegionName', data[0][0]['RegionDesc']);
                    });
                    this.viewTypeValues = this.viewTypeValuesRegion;
                    this.setControlValue('ViewType', 'Branch');
                } else {
                    this.viewTypeValues = this.viewTypeValuesBranch;
                    this.setControlValue('ViewType', 'Sales Employee');
                }
                this.onReportModeChange();
                break;
            default:
                if (this.pageType === 'Branch') {
                    this.setControlValue('BranchNumber', branchNumber);
                    this.setControlValue('BranchName', this.utils.getBranchTextOnly());
                    this.viewTypeValues = this.viewTypeValuesBranch;
                    this.setControlValue('ViewType', 'Sales Employee');

                } else if (this.pageType === 'Business') {
                    this.disableControl('EmployeeCode', false);
                    this.setControlValue('BusinessCode', this.bCode);
                    this.setControlValue('BusinessDesc', this.utils.getBusinessText());
                    this.showEmployeeOnly = this.getControlValue('ViewType') === 'Branch';

                } else if (this.pageType === 'Region') {
                    this.viewTypeValues = this.viewTypeValuesRegion;
                    this.setControlValue('BranchNumber', branchNumber);
                    this.setControlValue('BranchName', this.utils.getBranchTextOnly());
                    this.commonLookup.getRegionDesc().subscribe((data) => {
                        this.setControlValue('RegionNumber', data[0][0]['RegionCode']);
                        this.setControlValue('RegionName', data[0][0]['RegionDesc']);
                    });

                } else {
                    this.setControlValue('BusinessCode', this.bCode);
                    this.setControlValue('BusinessDesc', this.utils.getBusinessText());
                }
                this.commonLookup.setTradingDates(this.uiForm, 'DateFrom', 'DateTo', this.bCode);
                break;
        }
        this.loadViews();
    }

    private getReportGroupDD(): any {
        this.lookupUtils.getReportGroup().then((data) => {
            this.reportGroup = data[0];
        });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('GroupCode', 'SalesStatistics', 'GroupCode', MntConst.eTypeText, 15);
        this.riGrid.AddColumn('Description', 'SalesStatistics', 'Description', MntConst.eTypeTextFree, 20, false, 'Description');
        this.riGrid.AddColumnScreen('Description', false);

        if (this.pageType === 'Business' && this.getControlValue('ViewType') === 'Sales Employee') {
            this.riGrid.AddColumn('BranchNumber', 'SalesStatistics', 'BranchNumber', MntConst.eTypeCode, 2);
            this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
        }

        if (this.pageType === 'Branch') {
            this.riGrid.AddColumnScreen('Description', this.getControlValue('ViewType') === 'Postcode');
        }

        if (this.pageType === 'Region' && this.getControlValue('ViewType') === 'Sales Employee') {
            this.riGrid.AddColumn('BranchNumber', 'SalesStatistics', 'BranchNumber', MntConst.eTypeCode, 2);
            this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
        }

        if (this.pageParams.valueColumnsCount[this.pageParams.viewModeIdx]) {
            for (let i = 1; i <= this.pageParams.valueColumnsCount[this.pageParams.viewModeIdx]; i++) {
                this.riGrid.AddColumn(i.toString(), 'SalesStatisitcs', i.toString(), MntConst.eTypeDecimal2, 15);
                this.riGrid.AddColumnAlign(i.toString(), MntConst.eAlignmentRight);
            }
        }
        this.riGrid.AddColumn('GroupCode2', 'SalesStatistics', 'GroupCode2', MntConst.eTypeText, 15);
        this.riGrid.Complete();

        this.dateCol = '';
        if (this.getControlValue('ViewType') === 'Sales Employee' || this.getControlValue('ViewType') === 'Negotiating Employee') {
            this.dateCol = (this.riGrid.colArray.length).toString();
        }
    }

    public populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        let formData: any = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        formData[this.serviceConstants.BusinessCode] = this.bCode;
        formData[this.serviceConstants.Level] = this.pageType;

        switch (this.pageType) {
            case 'Branch':
                formData['MergeEmployees'] = this.getControlValue('vMergeEmployees');
                formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
                break;
            case 'Employee':
                formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
                formData['EmployeeLevelParent'] = 'EmployeeLevelParent';
                formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
                break;
            case 'Business':
                if (this.showEmployeeOnly && this.getControlValue('EmployeeCode') !== '') {
                    formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
                }
                break;
            case 'Region':
                formData['MergeEmployees'] = this.getControlValue('vMergeEmployees') || false;
                formData['RegionCode'] = this.getControlValue('RegionNumber');
                break;
        }

        formData['CompanyCode'] = this.getControlValue('CompanyCode');
        formData['DateFrom'] = this.getControlValue('DateFrom');
        formData['DateTo'] = this.getControlValue('DateTo');
        formData['ViewType'] = this.getControlValue('ViewType');
        formData['FilterCode'] = this.getControlValue('FilterCode');
        formData['FilterDesc'] = this.getControlValue('FilterDesc');
        formData['ViewMode'] = this.getControlValue('ViewMode');
        formData['StatMode'] = this.riExchange.getParentHTMLValue('pageMode') === 'national' ? 'national' : 'normal';
        formData['ReportMode'] = this.getControlValue('ReportMode');
        formData['FilterBy'] = this.getControlValue('FilterBy');
        if (this.pageType !== 'Employee') {
            formData['SalesType'] = this.getControlValue('SalesType');
        }
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
        formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'Sales/iCABSSSalesStatistics' + this.pageType, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                if (this.isReturning()) {
                    setTimeout(() => {
                        this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                    }, 500);
                }
                this.riGrid.Execute(data);
            }
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.hasGridData = false;
                this.displayMessage(error);
            });
    }

    private checkDrilldownForRegion(obj: any): boolean {
        let isValid: boolean = true;
        if (['Sales Employee', 'Employee'].includes(obj.viewType) && ['GroupCode', 'GroupCode2', 'BranchNumber'].includes(obj.colName)) {
            isValid = false;
        }
        if (obj.viewType === 'Product' && ['GroupCode', 'GroupCode2'].includes(obj.colName)) {
            isValid = false;
        }
        return isValid;
    }

    public riGridBodyOnDblClick(): void {
        let params: any = {};
        const localObj: any = {
            colName: this.riGrid.CurrentColumnName,
            employeeCode: this.getControlValue('EmployeeCode'),
            gcTitle: this.riGrid.Details.GetAttribute('GroupCode', 'title'),
            groupCode: this.riGrid.Details.GetValue('GroupCode'),
            groupCode2RowId: this.riGrid.Details.GetAttribute('GroupCode2', 'rowid'),
            groupCodeRowId: this.riGrid.Details.GetAttribute('GroupCode', 'rowid'),
            pageType: this.riExchange.getParentHTMLValue('pageType'),
            viewType: this.getControlValue('ViewType'),
            branchFromGrid: this.riGrid.Details.GetValue('BranchNumber')
        };

        if (localObj.pageType.toLowerCase() === 'region' && !this.checkDrilldownForRegion(localObj)) {
            return;
        }

        if (['GroupCode', 'GroupCode2'].indexOf(localObj.colName) >= 0) {
            if ([localObj.groupCodeRowId, localObj.groupCode2RowId].includes('Total')) {
                return;
            }

            if (localObj.groupCodeRowId) {
                if (localObj.viewType === 'Branch' || localObj.viewType === 'BranchProduct' || localObj.viewType === 'Region') {
                    if (localObj.viewType === 'Branch' && this.showEmployeeOnly && localObj.employeeCode !== '') {
                        this.displayMessage('To view details, the selected employee must be removed from the ‘Show Only Employee’ field', CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                        return;
                    }
                    params[localObj.viewType] = localObj.groupCode;
                    params['pageType'] = localObj.viewType.toLowerCase();
                    this.navigate('business', localObj.viewType === 'Region' ? BIReportsRoutes.ICABSSSALESSTATISTICSREGION : BIReportsRoutes.ICABSSSALESSTATISTICSBRANCH, params);

                } else if (this.pageType === 'Branch') {
                    if (localObj.viewType === 'Negotiating Employee' || localObj.viewType === 'Sales Employee') {
                        params['Branch'] = this.getControlValue('BranchNumber');
                        params['pageType'] = 'employee';
                        params['EmployeeCode'] = localObj.gcTitle;
                        this.navigate('employee', BIReportsRoutes.ICABSSSALESSTATISTICSEMPLOYEE, params);
                    }
                }
            } else {
                //Pages : iCABSSSalesStatisticsRegion.htm
                params['RegionCode'] = this.getControlValue('RegionNumber');
                params['RegionDesc'] = this.getControlValue('RegionName');
                params['BranchNumber'] = this.getControlValue('BranchNumber');
                this.navigate(this.riExchange.getParentHTMLValue('pageType'), BIReportsRoutes.ICABSSSALESSTATISTICSDETAIL, params);
            }
        } else {
            if (localObj.groupCodeRowId && localObj.groupCodeRowId !== 'TOTAL') {
                if (localObj.viewType !== 'BranchProduct') {
                    if (this.showEmployeeOnly && localObj.employeeCode !== '') {
                        this.displayMessage('To view details, the selected employee must be removed from the ‘Show Only Employee’ field', CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                        return;
                    }
                    params['ColumnName'] = this.riGrid.headerArray[0][this.riGrid.CurrentCell].text;
                    params['Column'] = localObj.colName;
                    params['RowID'] = localObj.groupCodeRowId;
                    params['GroupCode'] = localObj.groupCode;
                    params['GroupDesc'] = localObj.gcTitle;
                    if (localObj.pageType.toLowerCase() === 'business') {
                        if (localObj.viewType === 'Sales Employee') {
                            params['Branch'] = localObj.branchFromGrid;
                        }
                    } else if (localObj.pageType.toLowerCase() === 'region') {
                        params['RegionCode'] = this.getControlValue('RegionNumber');
                        params['RegionDesc'] = this.getControlValue('RegionName');
                        if (localObj.branchFromGrid) { params['BranchNumber'] = localObj.branchFromGrid; }
                    }
                    this.navigate(this.riExchange.getParentHTMLValue('pageType'), BIReportsRoutes.ICABSSSALESSTATISTICSDETAIL, params);
                }
            }
        }
    }

    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.pageParams.gridConfig.gridCacheRefresh = false;
        super.getCurrentPage(currentPage);
    }

    public gridRefresh(): void {
        this.pageParams.gridConfig.totalRecords = 0;
        this.hasGridData = false;
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let sysCharQuery: QueryParams = new QueryParams();
        let syscharList: Array<number> = [
            this.sysCharConstants.SystemCharEnableCompanyCode
        ];
        if (this.pageType === 'Branch') {
            syscharList.push(this.sysCharConstants.SystemCharMergeBranchSales);
        }
        sysCharQuery.set(this.serviceConstants.Action, '0');
        sysCharQuery.set(this.serviceConstants.BusinessCode, this.bCode);
        sysCharQuery.set(this.serviceConstants.CountryCode, this.countryCode());
        sysCharQuery.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));
        this.httpService.sysCharRequest(sysCharQuery).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            try {
                if (data && data.records && data.records.length) {
                    this.pageParams.vSCEnableCompanyCode = data.records[0].Required;
                    if (this.pageType === 'Branch') {
                        this.pageParams.vSCMergeBranchSales = data.records[1].Required;
                    }
                }
            } catch (error) {
                this.displayMessage(error);
            }
        });
    }

    public onDateChange(): any {
        this.riGrid.RefreshRequired();
    }

    public loadViews(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set('RequestFunction', 'GetViews');
        this.ajaxSource.next(this.ajaxconstant.START);
        const operation: string = (this.pageType === 'Region') ? 'Sales/iCABSSSalesStatisticsBranch' : 'Sales/iCABSSSalesStatistics' + this.pageType; // As data is same for business/branch and region using branch api, seperate API is not created for Region
        this.httpService.xhrGet('bi/reports', 'reports', operation, search)
            .then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.hasGridData = false;
                    this.displayMessage(data);
                } else {
                    if (data.ViewDataString) {
                        data.ViewDataString.split(',').map((itm, idx) => {
                            const split: string = itm.split('|');
                            this.pageParams.viewModeValues.push({ value: split[0], text: split[1] });
                            this.pageParams.valueColumnsCount.push(split[2]);
                            if (this.parentMode) {
                                this.pageParams.viewModeIdx = this.pageParams.viewModeValues.map(e => e.value).indexOf(this.riExchange.getParentHTMLValue('ViewMode'));
                            } else {
                                if (idx === 0) {
                                    this.setControlValue('ViewMode', split[0]);
                                    this.pageParams.viewModeIdx = idx;
                                }
                            }
                        });
                    }
                    this.buildGrid();
                    if (this.parentMode) { // IUI-24434
                        this.populateGrid();
                    }
                }
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.displayMessage(error);
                });
    }

    public onReportModeChange(): void {
        this.gridRefresh();
        this.pageTitle = this.curPageTitle + this.getControlValue('ReportMode');
        this.utils.setTitle(this.pageTitle);
    }

    public onSalesTypeChange(): void {
        this.gridRefresh();
        this.riGrid.RefreshRequired();
    }

    public onCompanyChange(data: any): void {
        this.gridRefresh();
        this.riGrid.RefreshRequired();
        if (data) {
            this.setControlValue('CompanyCode', data.CompanyCode);
            this.setControlValue('CompanyDesc', data.CompanyDesc);
        }
    }

    public onFilterChange(): void {
        this.gridRefresh();
        this.showProductSearch = (this.getControlValue('FilterBy') === 'ProductCode');
        this.setControlValue('FilterCode', '');
        this.setControlValue('FilterDesc', '');

    }

    public onReportGroupChange(e: any): void {
        this.gridRefresh();
        const text: string = e.options[e.options.selectedIndex].text.split('-')[1].trim();
        this.setControlValue('FilterCode', e.value);
        this.setControlValue('FilterDesc', text);
    }

    public onViewModeChange(e: any): void {
        this.pageParams.viewModeIdx = e.options.selectedIndex;
        this.gridRefresh();
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }

    public onViewTypeChange(): void {
        if (this.pageType === 'Business') {
            this.setControlValue('EmployeeCode', '');
            this.setControlValue('EmployeeSurname', '');
            this.showEmployeeOnly = this.getControlValue('ViewType') === 'Branch';
        }
        this.gridRefresh();
    }

    public onProductSearchDataReceived(data: any): void {
        if (data) {
            this.setControlValue('FilterCode', data.ProductCode);
            this.setControlValue('FilterDesc', data.ProductDesc);
        }
    }

    public onEmployeeDataReceived(data: any): void {
        if (data && data['EmployeeCode'] && data['EmployeeSurname']) {
            this.setControlValue('EmployeeCode', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
        this.gridRefresh();
    }
}
