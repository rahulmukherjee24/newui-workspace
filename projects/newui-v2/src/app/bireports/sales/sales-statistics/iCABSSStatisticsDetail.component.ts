import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { Component, OnInit, Injector } from '@angular/core';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@base/XhrParams';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { StaticUtils } from '@shared/services/static.utility';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSSStatisticsDetail.html',
    providers: [CommonLookUpUtilsService]
})

export class StatisticsDetailComponent extends LightBaseComponent implements OnInit {

    public pageId: string = '';
    public message: string;
    public pageType: string;

    public controls: IControls[] = [
        { name: 'BusinessDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'CompanyCode', type: MntConst.eTypeText },
        { name: 'BranchNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'EmployeeCode', type: MntConst.eTypeText },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'DateFrom', type: MntConst.eTypeDate },
        { name: 'DateTo', type: MntConst.eTypeDate },
        { name: 'ReportMode', type: MntConst.eTypeTextFree, value: 'Processed' },
        { name: 'SalesType', type: MntConst.eTypeTextFree, value: 'All' },
        { name: 'RepDest', type: MntConst.eTypeTextFree, value: 'direct' }
    ];

    public dropdown: any = {
        company: {
            params: {
                businessCode: this.businessCode(),
                countryCode: this.countryCode(),
                parentMode: 'LookUp'
            },
            active: { id: '', text: '' }
        }
    };

    public ellipsis: any = {
        employeeSearch: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: EmployeeSearchComponent
        }
    };

    public xhrParams: IXHRParams = {
        operation: 'CreateBatchProcessEntry',
        module: 'create-batch',
        method: 'create/batch'
    };


    constructor(injector: Injector, public sysCharConstants: SysCharConstants, private lookupUtils: CommonLookUpUtilsService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSSTATISTICSDETAIL;
        this.pageTitle = 'Sales Detail';
        this.browserTitle = 'Sales Detail Report';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.setFieldsOnLoad();
        this.loadSysChars();
    }

    private setFieldsOnLoad(): void {
        this.setControlValue('BusinessDesc', this.utils.getBusinessText());
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchTextOnly());
        this.lookupUtils.setTradingDates(this.uiForm, 'DateFrom', 'DateTo');
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let sysCharQuery: QueryParams = new QueryParams();
        let syscharList: Array<number> = [this.sysCharConstants.SystemCharEnableCompanyCode];
        sysCharQuery.set(this.serviceConstants.Action, '0');
        sysCharQuery.set(this.serviceConstants.BusinessCode, this.businessCode());
        sysCharQuery.set(this.serviceConstants.CountryCode, this.countryCode());
        sysCharQuery.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));
        this.httpService.sysCharRequest(sysCharQuery).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            try {
                if (data && data.records && data.records.length) {
                    this.pageParams.vSCEnableCompanyCode = data.records[0].Required;
                }
            } catch (error) {
                this.displayMessage(error);
            }
        });
    }

    public onSubmit(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set(this.serviceConstants.CountryCode, this.countryCode());

        let arrParameterName: Array<string> = [], arrParameterValue: Array<string> = [];

        arrParameterName.push('Mode'); arrParameterValue.push('Branch');
        arrParameterName.push('Level'); arrParameterValue.push('Detail');
        arrParameterName.push('BusinessCode'); arrParameterValue.push(this.businessCode());
        arrParameterName.push('BranchNumber'); arrParameterValue.push(this.utils.getBranchCode());
        arrParameterName.push('EmployeeCode'); arrParameterValue.push(this.getControlValue('EmployeeCode'));
        arrParameterName.push('DateFrom'); arrParameterValue.push(this.getControlValue('DateFrom'));
        arrParameterName.push('DateTo'); arrParameterValue.push(this.getControlValue('DateTo'));
        arrParameterName.push('RepManDest'); arrParameterValue.push(StaticUtils.getReportManDest(this.getControlValue('RepDest')));
        arrParameterName.push('CompanyCode'); arrParameterValue.push(this.getControlValue('CompanyCode'));
        arrParameterName.push('ReportMode'); arrParameterValue.push(this.getControlValue('ReportMode'));
        arrParameterName.push('SalesType'); arrParameterValue.push(this.getControlValue('SalesType'));

        let formData: Object = {};
        formData['Description'] = 'Sales Detail';
        formData['ProgramName'] = 'iCABSSalesDetail.p';
        formData['StartDate'] = this.globalize.parseDateToFixedFormat(new Date()).toString();
        formData['StartTime'] = this.utils.hmsToSeconds(this.utils.Time());
        formData['Report'] = 'report';
        formData['ParameterName'] = arrParameterName.join(StaticUtils.RI_SEPARATOR_VALUE_LIST);
        formData['ParameterValue'] = arrParameterValue.join(StaticUtils.RI_SEPARATOR_VALUE_LIST);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.message = data.fullError;
                    return;
                }
            },
            (error) => {
                this.displayMessage(error);
            });
    }

    public onCompanyChange(data: Record<string, string>): void {
        if (data) {
            this.setControlValue('CompanyCode', data.CompanyCode);
            this.setControlValue('CompanyDesc', data.CompanyDesc);
        }
    }

    public onEmployeeCodeChange(): void {
        this.setControlValue('EmployeeSurname', '');
        if (!this.getControlValue('EmployeeCode')) {
            return;
        }
        this.lookupUtils.getEmployeeSurname(this.getControlValue('EmployeeCode')).then((data) => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound);
                this.setControlValue('EmployeeCode', '');
            }
        });
    }

    public onEmployeeSearchDataReturn(data: Record<string, string>): void {
        if (data) {
            this.setControlValue('EmployeeCode', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
    }
}
