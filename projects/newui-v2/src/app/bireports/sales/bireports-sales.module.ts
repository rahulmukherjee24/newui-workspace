import { JobCreditBranchGridComponent } from './iCABSARJobCreditBranchGrid.component';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BIReportsRoutesConstant } from '@base/PageRoutes';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { SharedModule } from '@shared/shared.module';

import { ReleasedForInvoiceGridComponent } from './iCABSARReleasedForInvoiceGrid.component';
import { SalesStatisticsComponent } from './sales-statistics/iCABSSSalesStatistics.component';
import { SalesStatisticsDetailComponent } from './sales-statistics/iCABSSSalesStatisticsDetail.component';
import { StatisticsDetailComponent } from './sales-statistics/iCABSSStatisticsDetail.component';
import { OriginOfBusinessBranchGridComponent } from './iCABSAROriginOfBusinessBranchGrid.component';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class SalesBIReportsRootComponent {
    constructor(viewContainerRef: ViewContainerRef) { }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchEllipsisModule,
        InternalSearchModule,
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: SalesBIReportsRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSARRELEASEDFORJOBINVOICEGRID, component: ReleasedForInvoiceGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARRELEASEDFORINVOICEBUSINESSGRID, component: ReleasedForInvoiceGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARRELEASEDFORPRODUCTINVOICEGRID, component: ReleasedForInvoiceGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARRELEASEDFORJOBINVOICEBRANCHGRID, component: ReleasedForInvoiceGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARRELEASEDFORPRODUCTINVOICEBRANCHGRID, component: ReleasedForInvoiceGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARRELEASEDFORJOBINVOICEREGIONGRID, component: ReleasedForInvoiceGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARRELEASEDFORPRODUCTINVOICEREGIONGRID, component: ReleasedForInvoiceGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSSALESSTATISTICSBUSINESS, component: SalesStatisticsComponent },
                    { path: BIReportsRoutesConstant.ICABSSSALESSTATISTICSBRANCH, component: SalesStatisticsComponent },
                    { path: BIReportsRoutesConstant.ICABSSSALESSTATISTICSREGION, component: SalesStatisticsComponent },
                    { path: BIReportsRoutesConstant.ICABSSSALESSTATISTICSDETAIL, component: SalesStatisticsDetailComponent },
                    { path: BIReportsRoutesConstant.ICABSSSALESSTATISTICSEMPLOYEE, component: SalesStatisticsComponent },
                    { path: BIReportsRoutesConstant.ICABSSSTATISTICSDETAIL, component: StatisticsDetailComponent },
                    { path: BIReportsRoutesConstant.iCABSARJOBCREDITBRANCH, component: JobCreditBranchGridComponent },
                    { path: BIReportsRoutesConstant.iCABSARJOBCREDITDETAIL, component: JobCreditBranchGridComponent },
                    { path: BIReportsRoutesConstant.iCABSARJOBCREDITBUSINESS, component: JobCreditBranchGridComponent },
                    { path: BIReportsRoutesConstant.iCABSARJOBCREDITREGION, component: JobCreditBranchGridComponent },
                    { path: BIReportsRoutesConstant.iCABSARORIGINOFBUSINESSBRANCH, component: OriginOfBusinessBranchGridComponent },
                    { path: BIReportsRoutesConstant.iCABSARORIGINOFBUSINESSEMPLOYEE, component: OriginOfBusinessBranchGridComponent },
                    { path: BIReportsRoutesConstant.iCABSARORIGINOFBUSINESSBUSINESS, component: OriginOfBusinessBranchGridComponent },
                    { path: BIReportsRoutesConstant.iCABSARORIGINOFBUSINESSDETAIL, component: OriginOfBusinessBranchGridComponent },
                    { path: BIReportsRoutesConstant.iCABSARORIGINOFBUSINESSREGION, component: OriginOfBusinessBranchGridComponent }
                ], data: { domain: 'BI REPORTS SALES' }
            }
        ])
    ],
    declarations: [
        SalesBIReportsRootComponent,
        ReleasedForInvoiceGridComponent,
        SalesStatisticsComponent,
        SalesStatisticsDetailComponent,
        StatisticsDetailComponent,
        JobCreditBranchGridComponent,
        OriginOfBusinessBranchGridComponent
    ]
})

export class SalesBIReportsModule { }
