import { IExportOptions } from './../../base/ExportConfig';
import { AccountSearchComponent } from './../../internal/search/iCABSASAccountSearch';
import { CommonGridFunction } from './../../base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { Component, OnInit, OnDestroy, Injector, ViewChild, AfterContentInit } from '@angular/core';
import { ContractManagementModuleRoutes, InternalMaintenanceServiceModuleRoutes } from './../../base/PageRoutes';
import { ContractSearchComponent } from '@app/internal/search/iCABSAContractSearch';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { GroupAccountNumberComponent } from './../../internal/search/iCABSSGroupAccountNumberSearch';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { IGridHandlers } from './../../base/BaseComponentLight';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ModalAdvService } from './../../../shared/components/modal-adv/modal-adv.service';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { PremiseSearchComponent } from '@app/internal/search/iCABSAPremiseSearch';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { SysCharConstants } from './../../../shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSBulkProofofService.html',
    providers: [CommonLookUpUtilsService]
})


export class BulkProofOfServiceComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit, IGridHandlers {
    @ViewChild('errorModal') public errorModal;
    @ViewChild('messageModal') public messageModal;
    @ViewChild('promptModal') public promptModal;
    @ViewChild('promptModalForSave') public promptModalForSave;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    private vbDisplayLevelInd: boolean;
    private fetchReportURLParams: Record<string, string> = {
        action: '6',
        operation: 'Application/iCABSBulkProofofService'
    };
    public exportConfig: IExportOptions = {};
    public hasGridData: boolean = false;
    public pageId: string;
    public pageType: string = '';
    public commonGridFunction: CommonGridFunction;
    public contractSearchComponent: any;
    public premiseSearchComponent: any;
    public gridConfig: Record<string, number> = {
        itemsPerPage: 10,
        totalItem: 1
    };
    constructor(injector: Injector,
        private commonUtils: CommonLookUpUtilsService,
        private sysCharConstants: SysCharConstants,
        private modalAdvService: ModalAdvService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSBULKPROOFOFSERVICE;
    }
    controls = [
        { name: 'GroupAccountNumber', type: MntConst.eTypeCode },
        { name: 'GroupName', type: MntConst.eTypeText, disabled: true },
        { name: 'AccountNumber', type: MntConst.eTypeCode },
        { name: 'AccountName', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true },
        { name: 'PostCode', type: MntConst.eTypeText },
        { name: 'SearchType', type: MntConst.eTypeText, value: 'PremiseVisitInd' },
        { name: 'ServiceDateFrom', type: MntConst.eTypeDate, value: new Date(new Date().getFullYear(), new Date().getMonth() - 1, 1), required: true },
        { name: 'ServiceDateTo', type: MntConst.eTypeDate, value: new Date(new Date().getFullYear(), new Date().getMonth(), 1 - 1), required: true }
    ];

    public ellipsis: Record<string, Record<string, Object>> = {
        commonProperties: {
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'Lookup-UpdateParent',
                'showAddNewDisplay': false
            },
            showHeader: true
        },
        groupAccount: {
            contentComponent: GroupAccountNumberComponent,
            showHeader: true,
            disabled: false
        },
        account: {
            contentComponent: AccountSearchComponent,
            childConfigParams: {
                'parentMode': 'Lookup-UpdateParent',
                'showAddNewDisplay': false
            },
            disabled: false
        },
        contract: {
            childConfigParams: {
                'parentMode': 'ContractSearch',
                'showAddNew': false,
                'shouldClear': true,
                'currentContractType': 'C',
                'currentContractTypeURLParameter': '<contract>'
            }
        },
        premise: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'showAddNew': false,
                'shouldClear': true
            }
        }
    };

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.onWindowLoad();
    }

    private onWindowLoad(): void {
        this.getSysChars();
        this.contractSearchComponent = ContractSearchComponent;
        this.premiseSearchComponent = PremiseSearchComponent;
        if (this.isReturning()) {
            this.pageParams.gridCacheRefresh = false;
            if (this.getControlValue('ContractNumber')) {
                this.disableControl('PremiseNumber', false);
            }
        } else {
            this.setRequiredStatus('GroupAccountNumber', true);
            this.setRequiredStatus('AccountNumber', true);
            this.setRequiredStatus('ContractNumber', true);
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.isMandatory = true;
        }
        this.onChangeControls('SearchType');
        this.onRiGridRefresh();
    }

    private getSysChars(): void {
        let syscharList: Array<number> = [
            this.sysCharConstants.SystemCharEnableVisitDetail,
            this.sysCharConstants.SystemCharEnableEntryOfVisitRefInVisitEntry,
            this.sysCharConstants.SystemCharEnableWED,
            this.sysCharConstants.SystemCharEnableServiceCoverDisplayLevel,
            this.sysCharConstants.SystemCharEnableBarcodes,
            this.sysCharConstants.SystemCharEnableNoServiceReasons,
            this.sysCharConstants.SystemCharEnableInfestations,
            this.sysCharConstants.SystemCharEnablePreps,
            this.sysCharConstants.SystemCharEnableDrivingCharges,
            this.sysCharConstants.SystemCharEnableWorkLoadIndex,
            this.sysCharConstants.SystemCharEnableServiceEntryUserCodeLog,
            this.sysCharConstants.SystemCharEnableServiceDocketNumberVisitEnty
        ];
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                let list: Array<any> = data.records;
                this.pageParams.SystemCharEnableVisitDetail = list[0].Required;
                this.pageParams.SystemCharEnableEntryOfVisitRefInVisitEntry = list[1].Required;
                this.pageParams.SystemCharEnableWED = list[2].Required;
                this.pageParams.SystemCharEnableServiceCoverDisplayLevel = list[3].Required;
                this.pageParams.SystemCharEnableBarcodes = list[4].Required;
                this.pageParams.SystemCharEnableNoServiceReasons = list[5].Required;
                this.pageParams.SystemCharEnableInfestations = list[6].Required;
                this.pageParams.SystemCharEnablePreps = list[7].Required;
                this.pageParams.SystemCharEnableDrivingCharges = list[8].Required;
                this.pageParams.SystemCharEnableWorkLoadIndex = list[9].Required;
                this.pageParams.SystemCharEnableServiceEntryUserCodeLog = list[10].Required;
                this.pageParams.SystemCharEnableServiceDocketNumberVisitEnty = list[11].Required;
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        this.commonGridFunction.onRefreshClick();
    }

    public buildGrid(): void {
        let searchType: string = this.getControlValue('SearchType');
        this.riGrid.Clear();
        this.hasGridData = false;
        this.riGrid.AddColumn('Account', 'ServiceVisitSummary', 'Account', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('Account', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('Contract', 'ServiceVisitSummary', 'Contract', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('Contract', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('PremiseNumber', 'ServiceVisitSummary', 'PremiseNumber', MntConst.eTypeText, 5, true);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        if (searchType !== 'PremiseVisitInd') {
            this.riGrid.AddColumn('ProductCode', 'ServiceVisitSummary', 'ProductCode', MntConst.eTypeCode, 4, true);
            this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentLeft);
            this.riGrid.AddColumn('ProductDesc', 'ServiceVisitSummary', 'ProductDesc', MntConst.eTypeText, 40);
            this.riGrid.AddColumnAlign('ProductDesc', MntConst.eAlignmentLeft);
            this.riGrid.AddColumn('ServiceVisitFrequency', 'ServiceVisitSummary', 'ServiceVisitFrequency', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        }

        if (searchType === 'DetailInd' || searchType === 'PremiseVisitInd') {
            this.riGrid.AddColumn('ServiceDateStart', 'ServiceVisitSummary', 'ServiceDateStart', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);
            if (searchType === 'DetailInd') {
                if (this.pageParams.SystemCharEnableVisitDetail) {
                    this.riGrid.AddColumn('ServiceDateEnd', 'ServiceVisitSummary', 'ServiceDateEnd', MntConst.eTypeDate, 10);
                    this.riGrid.AddColumnAlign('ServiceDateEnd', MntConst.eAlignmentCenter);
                }
                if (this.pageParams.SystemCharEnableEntryOfVisitRefInVisitEntry) {
                    this.riGrid.AddColumn('VisitReference', 'ServiceVisitSummary', 'VisitReference', MntConst.eTypeText, 10);
                    this.riGrid.AddColumnAlign('VisitReference', MntConst.eAlignmentLeft);
                }
                this.riGrid.AddColumn('PlannedVisitDate', 'Grid', 'PlannedVisitDate', MntConst.eTypeImage, 10);
                this.riGrid.AddColumnAlign('PlannedVisitDate', MntConst.eAlignmentCenter);
                this.riGrid.AddColumnScreen('PlannedVisitDate', false);
                this.riGrid.AddColumn('ChangeProcessDate', 'ServiceVisitSummary', 'ChangeProcessDate', MntConst.eTypeDate, 10);
                this.riGrid.AddColumnAlign('ChangeProcessDate', MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('VisitTypeCode', 'ServiceVisitSummary', 'VisitTypeCode', MntConst.eTypeCode, 2, true);
                this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('ServicedQuantity', 'ServiceVisitSummary', 'ServicedQuantity', MntConst.eTypeInteger, 5);
                this.riGrid.AddColumnAlign('ServicedQuantity', MntConst.eAlignmentCenter);

                if (this.pageParams.SystemCharEnableWED) {
                    this.riGrid.AddColumn('WEDSQty', 'ServiceVisitSummary', 'WEDSQty', MntConst.eTypeInteger, 5);
                    this.riGrid.AddColumnAlign('WEDSQty', MntConst.eAlignmentCenter);
                }

                this.riGrid.AddColumn('EmployeeSurname', 'ServiceVisitSummary', 'EmployeeSurname', MntConst.eTypeText, 20);
                this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);
                this.riGrid.AddColumn('SharedInd', 'ServiceVisitSummary', 'SharedInd', MntConst.eTypeImage, 1);
                this.riGrid.AddColumnAlign('SharedInd', MntConst.eAlignmentCenter);

                if (this.pageParams.SystemCharEnableVisitDetail) {
                    this.riGrid.AddColumn('VisitDetail', 'ServiceVisitSummary', 'VisitDetail', MntConst.eTypeImage, 1);
                    this.riGrid.AddColumnAlign('VisitDetail', MntConst.eAlignmentCenter);
                }
            }

            if (this.pageParams.SystemCharEnableServiceCoverDisplayLevel) {
                if (searchType === 'DetailInd') {
                    this.vbDisplayLevelInd = false;
                }
                if (searchType === 'DetailInd' && this.vbDisplayLevelInd) {
                    this.riGrid.AddColumn('ItemDescription', 'ServiceVisitSummary', 'ItemDescription', MntConst.eTypeText, 13);
                    this.riGrid.AddColumnAlign('ItemDescription', MntConst.eAlignmentLeft);
                    this.riGrid.AddColumn('PremiseLocationNumber', 'ServiceVisitSummary', 'PremiseLocationNumber', MntConst.eTypeText, 4);
                    this.riGrid.AddColumnAlign('PremiseLocationNumber', MntConst.eAlignmentLeft);
                    this.riGrid.AddColumn('VisitNarrativeDesc', 'ServiceVisitSummary', 'VisitNarrativeDesc', MntConst.eTypeText, 13);
                    this.riGrid.AddColumnAlign('VisitNarrativeDesc', MntConst.eAlignmentLeft);
                    this.riGrid.AddColumn('ProductComponentDesc', 'ServiceVisitSummary', 'ProductComponentDesc', MntConst.eTypeText, 13);
                    this.riGrid.AddColumnAlign('ProductComponentDesc', MntConst.eAlignmentLeft);
                    this.riGrid.AddColumn('ReplacementValue', 'ServiceVisitSummary', 'ReplacementValue', MntConst.eTypeCurrency, 10);
                    this.riGrid.AddColumnAlign('ReplacementValue', MntConst.eAlignmentRight);
                    this.riGrid.AddColumn('AdditionalChargeValue', 'ServiceVisitSummary', 'AdditionalChargeValue', MntConst.eTypeCurrency, 10);
                    this.riGrid.AddColumnAlign('AdditionalChargeValue', MntConst.eAlignmentRight);
                }
            }

            if (searchType === 'PremiseVisitInd') {
                this.riGrid.AddColumn('Employee', 'ServiceVisitSummary', 'Employee', MntConst.eTypeText, 20);
                this.riGrid.AddColumnAlign('Employee', MntConst.eAlignmentLeft);
                this.riGrid.AddColumn('PremiseContactName', 'ServiceVisitSummary', 'PremiseContactName', MntConst.eTypeText, 20);
                this.riGrid.AddColumnAlign('PremiseContactName', MntConst.eAlignmentLeft);

                if (this.pageParams.SystemCharEnableEntryOfVisitRefInVisitEntry) {
                    this.riGrid.AddColumn('VisitReference', 'ServiceVisitSummary', 'VisitReference', MntConst.eTypeText, 10);
                    this.riGrid.AddColumnAlign('VisitReference', MntConst.eAlignmentLeft);
                }


                this.riGrid.AddColumn('PremiseContactSignature', 'ServiceVisitSummary', 'PremiseContactSignature', MntConst.eTypeImage, 1);
                this.riGrid.AddColumnAlign('PremiseContactSignature', MntConst.eAlignmentLeft);

                if (this.pageParams.SystemCharEnableBarcodes) {
                    this.riGrid.AddColumn('BarcodeScanInd', 'ServiceVisitSummary', 'BarcodeScanInd', MntConst.eTypeImage, 1);
                    this.riGrid.AddColumnAlign('BarcodeScanInd', MntConst.eAlignmentCenter);
                }

                if (this.pageParams.SystemCharEnableNoServiceReasons) {
                    this.riGrid.AddColumn('NoPremiseVisitReasonCode', 'ServiceVisitSummary', 'NoPremiseVisitReasonCode', MntConst.eTypeCode, 2);
                    this.riGrid.AddColumnAlign('NoPremiseVisitReasonCode', MntConst.eAlignmentCenter);
                }

                this.riGrid.AddColumn('ProductList', 'ServiceVisitSummary', 'ProductList', MntConst.eTypeText, 20);
                this.riGrid.AddColumnAlign('ProductList', MntConst.eAlignmentLeft);
            }

            if (this.pageParams.SystemCharEnableInfestations) {
                this.riGrid.AddColumn('Infestations', 'ServiceVisitSummary', 'Infestations', MntConst.eTypeText, 15);
                this.riGrid.AddColumnAlign('Infestations', MntConst.eAlignmentLeft);
            }

            if (searchType === 'DetailInd' && this.pageParams.SystemCharEnableServiceDocketNumberVisitEnty) {
                this.riGrid.AddColumn('DeliveryNoteNo', 'ServiceVisitSummary', 'DeliveryNoteNo', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('DeliveryNoteNo', MntConst.eAlignmentRight);
            }

            if (this.pageParams.SystemCharEnableServiceCoverDisplayLevel && searchType !== 'DetailInd') {
                this.riGrid.AddColumn('ReplacementValue', 'ServiceVisitSummary', 'ReplacementValue', MntConst.eTypeCurrency, 10);
                this.riGrid.AddColumnAlign('ReplacementValue', MntConst.eAlignmentRight);
            }

            if (this.pageParams.SystemCharEnablePreps) {
                this.riGrid.AddColumn('PrepCodes', 'ServiceVisitSummary', 'PrepCodes', MntConst.eTypeText, 15);
                this.riGrid.AddColumnAlign('PrepCodes', MntConst.eAlignmentLeft);
            }

            if (this.pageParams.SystemCharEnablePreps) {
                this.riGrid.AddColumn('PrepCost', 'ServiceVisitSummary', 'PrepCost', MntConst.eTypeCurrency, 10);
                this.riGrid.AddColumnAlign('PrepCost', MntConst.eAlignmentRight);
            }
            this.riGrid.AddColumn('StandardDuration', 'ServiceVisitSummary', 'StandardDuration', MntConst.eTypeTime, 5);
            this.riGrid.AddColumnAlign('StandardDuration', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('OvertimeDuration', 'ServiceVisitSummary', 'OvertimeDuration', MntConst.eTypeTime, 5);
            this.riGrid.AddColumnAlign('OvertimeDuration', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ServiceVisitCost', 'ServiceVisitSummary', 'ServiceVisitCost', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumnAlign('ServiceVisitCost', MntConst.eAlignmentRight);

            this.riGrid.AddColumn('TotalCost', 'ServiceVisitSummary', 'TotalCost', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumnAlign('TotalCost', MntConst.eAlignmentRight);

            if (this.pageParams.SystemCharEnableDrivingCharges && searchType === 'PremiseVisitInd') {

                this.riGrid.AddColumn('DrivingChargeValue', 'ServiceVisitSummary', 'DrivingChargeValue', MntConst.eTypeCurrency, 10);
                this.riGrid.AddColumnAlign('DrivingChargeValue', MntConst.eAlignmentRight);
            }

            if (this.pageParams.SystemCharEnableWorkLoadIndex) {
                this.riGrid.AddColumn('WorkLoadIndexTotal', 'ServiceVisitSummary', 'WorkLoadIndexTotal', MntConst.eTypeDecimal2, 10);
                this.riGrid.AddColumnAlign('WorkLoadIndexTotal', MntConst.eAlignmentRight);

            }

            if (searchType === 'DetailInd') {
                this.riGrid.AddColumn('Recommendation', 'ServiceVisitRecommendation', 'Recommendation', MntConst.eTypeInteger, 5);
                this.riGrid.AddColumnAlign('Recommendation', MntConst.eAlignmentCenter);
            }

            if (searchType === 'PremiseVisitInd') {
                this.riGrid.AddColumn('PrintTreatment', 'ServiceVisitSummary', 'PrintTreatment', MntConst.eTypeImage, 1, true);
            }

        } else {
            this.riGrid.AddColumn('VisitsDue', 'ServiceVisitSummary', 'VisitsDue', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('VisitsDue', MntConst.eAlignmentRight);

            this.riGrid.AddColumn('RoutinesDone', 'ServiceVisitSummary', 'RoutinesDone', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('RoutinesDone', MntConst.eAlignmentRight);

            this.riGrid.AddColumn('OthersDone', 'ServiceVisitSummary', 'OthersDone', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('OthersDone', MntConst.eAlignmentRight);

            this.riGrid.AddColumn('CreditsDone', 'ServiceVisitSummary', 'CreditsDone', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('CreditsDone', MntConst.eAlignmentRight);
        }
        if (!this.getControlValue('PremiseNumber')) {
            this.riGrid.AddColumnOrderable('PremiseNumber', true);
        }
        if (searchType !== 'PremiseVisitInd') {
            this.riGrid.AddColumnOrderable('ServiceVisitFrequency', true);
        }

        if (searchType === 'DetailInd') {
            this.riGrid.AddColumnOrderable('ServiceDateStart', true);
            if (this.pageParams.SystemCharEnableEntryOfVisitRefInVisitEntry) {
                this.riGrid.AddColumnOrderable('VisitReference', true);
            }
            this.riGrid.AddColumnOrderable('VisitTypeCode', true);
        }

        if (searchType !== 'PremiseVisitInd') {
            this.riGrid.AddColumn('Information', 'ServiceVisitSummary', 'Information', MntConst.eTypeImage, 2, false);
            this.riGrid.AddColumnAlign('Information', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ManualVisitInd', 'ServiceVisitSummary', 'ManualVisitInd', MntConst.eTypeImage, 2, false);
        }


        if (searchType === 'DetailInd' && this.pageParams.SystemCharEnableServiceEntryUserCodeLog) {
            this.riGrid.AddColumn('ProcessesDate/Time', 'ServicePlan', 'ProcessesDate/Time', MntConst.eTypeText, 16);
            this.riGrid.AddColumnAlign('ProcessesDate/Time', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('UserCode', 'ServiceVisitSummary', 'UserCode', MntConst.eTypeCode, 20);
        }
        this.riGrid.Complete();
        this.exportConfig = {};
        this.dateCollist = this.riGrid.getColumnIndexList([
            'ServiceDateStart',
            'ServiceDateEnd',
            'PlannedVisitDate',
            'ChangeProcessDate'
        ]);
        this.dateTimeCollist = this.riGrid.getColumnIndexList([
            'ProcessesDate/Time'
        ]);
        this.durationCollist = this.riGrid.getColumnIndexList([
            'StandardDuration',
            'OvertimeDuration'
        ]);

        if (this.dateCollist.length) {
            this.exportConfig.dateColumns = this.dateCollist;
        }
        if (this.dateTimeCollist.length) {
            this.exportConfig.dateTimeColumns = this.dateTimeCollist;
        }
        if (this.durationCollist.length) {
            this.exportConfig.durationColumns = this.durationCollist;
        }
    }

    public populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            let formData: Object = {};

            formData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
            formData['GroupAccountNumber'] = this.getControlValue('GroupAccountNumber');
            formData['AccountNumber'] = this.getControlValue('AccountNumber');
            formData['ContractNumber'] = this.getControlValue('ContractNumber');
            formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
            formData['PostCode'] = this.getControlValue('PostCode');
            formData['ServiceDateFrom'] = this.getControlValue('ServiceDateFrom');
            formData['ServiceDateTo'] = this.getControlValue('ServiceDateTo');
            formData['DetailInd'] = this.getControlValue('SearchType') === 'DetailInd';
            formData['ShowFreqInd'] = 'True';
            formData['PremiseVisitInd'] = this.getControlValue('SearchType') === 'PremiseVisitInd';
            formData['EnablePreps'] = this.pageParams.SystemCharEnablePreps ? 'True' : 'False';
            formData['EnableInfestations'] = this.pageParams.SystemCharEnableInfestations ? 'True' : 'False';
            formData['EnableVisitDurations'] = 'True';
            formData['EnableVisitCostings'] = 'True';
            formData['EnableDrivingCharges'] = this.pageParams.SystemCharEnableDrivingCharges ? 'True' : 'False';
            formData['EnableWorkLoadIndex'] = this.pageParams.SystemCharEnableWorkLoadIndex ? 'True' : 'False';
            formData['EnableVisitRef'] = this.pageParams.SystemCharEnableEntryOfVisitRefInVisitEntry ? 'True' : 'False';
            formData['EnableUserCode'] = this.pageParams.SystemCharEnableServiceEntryUserCodeLog ? 'True' : 'False';
            formData['EnableWED'] = this.pageParams.SystemCharEnableWED ? 'True' : 'False';
            formData['EnableServCoverDispLevel'] = this.pageParams.SystemCharEnableServiceCoverDisplayLevel ? 'True' : 'False';
            formData['EnableServDocketNoVisitEntry'] = this.pageParams.SystemCharEnableServiceDocketNumberVisitEnty ? 'True' : 'False';
            formData['EnableBarcodes'] = this.pageParams.SystemCharEnableBarcodes ? 'True' : 'False';
            formData['EnableNoServiceReasons'] = this.pageParams.SystemCharEnableNoServiceReasons ? 'True' : 'False';
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            formData[this.serviceConstants.PageSize] = '12';
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
            formData[this.serviceConstants.GridHeaderClickedColumn] = '';
            formData[this.serviceConstants.GridSortOrder] = 'Descending';
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'Application/iCABSBulkProofofService', search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.hasGridData = false;
                        this.gridConfig.totalItem = 1;
                        this.displayMessage(data);
                    } else {
                        this.hasGridData = true;
                        this.riGrid.RefreshRequired();
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                        this.riGrid.UpdateFooter = false;
                        this.riGrid.Execute(data);
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 100);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.hasGridData = false;
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }

    public onChangeControls(type: string): void {
        this.buildGrid();
        if (!this.getControlValue('ContractNumber') && !this.getControlValue('AccountNumber') && !this.getControlValue('GroupAccountNumber')) {
            this.setRequiredStatus('GroupAccountNumber', true);
            this.setRequiredStatus('ContractNumber', true);
            this.setRequiredStatus('AccountNumber', true);
            this.pageParams.isMandatory = true;
        }
        switch (type) {
            case 'GroupAccount':
                this.setControlValue('GroupName', '');
                this.disableControl('ContractNumber', false);
                this.resetValues('GroupAccount');
                this.ellipsis.account.childConfigParams['groupAccountNumber'] = '';
                this.ellipsis.account.childConfigParams['groupName'] = '';
                this.ellipsis.contract.childConfigParams['accountNumber'] = '';
                if (this.getControlValue('GroupAccountNumber')) {
                    this.commonUtils.getGroupAccountName(this.getControlValue('GroupAccountNumber'))
                        .then(data => {
                            if (data[0][0]) {
                                this.setControlValue('GroupName', data[0][0].GroupName);
                                this.setRequiredStatus('ContractNumber', false);
                                this.setRequiredStatus('AccountNumber', false);
                                this.pageParams.isMandatory = false;
                                this.ellipsis.account.childConfigParams['groupAccountNumber'] = this.getControlValue('GroupAccountNumber');
                                this.ellipsis.account.childConfigParams['groupName'] = this.getControlValue('GroupName');
                            } else {
                                if (!this.getControlValue('ContractNumber') || !this.getControlValue('AccountNumber')) {
                                    this.pageParams.isMandatory = false;
                                    this.setRequiredStatus('ContractNumber', false);
                                    this.setRequiredStatus('AccountNumber', false);
                                }
                                if (this.getControlValue('GroupAccountNumber')) {
                                    this.displayMessage(MessageConstant.Message.RecordNotFound);
                                }
                            }
                        });
                } else {
                    if (this.getControlValue('ContractNumber') || this.getControlValue('AccountNumber')) {
                        this.pageParams.isMandatory = false;
                        this.setRequiredStatus('ContractNumber', false);
                        this.setRequiredStatus('AccountNumber', false);
                    } else {
                        this.setRequiredStatus('ContractNumber', true);
                        this.setRequiredStatus('AccountNumber', true);
                        this.setRequiredStatus('GroupAccountNumber', true);
                        this.disableControl('PremiseNumber', true);
                        this.pageParams.isMandatory = true;
                    }
                }
                break;

            case 'Account':
                this.setControlValue('AccountName', '');
                this.resetValues('Account');
                this.ellipsis.contract.childConfigParams['accountNumber'] = '';
                this.ellipsis.contract.childConfigParams['accountName'] = '';
                if (this.getControlValue('AccountNumber')) {
                    this.commonUtils.getAccountName(this.getControlValue('AccountNumber'))
                        .then(data => {
                            if (data[0][0]) {
                                this.setRequiredStatus('ContractNumber', false);
                                this.setRequiredStatus('GroupAccountNumber', false);
                                this.setControlValue('AccountName', data[0][0].AccountName);
                                this.pageParams.isMandatory = false;
                                this.ellipsis.contract.childConfigParams['accountNumber'] = this.getControlValue('AccountNumber');
                            } else {
                                if (!this.getControlValue('ContractNumber') || !this.getControlValue('GroupAccountNumber')) {
                                    this.pageParams.isMandatory = false;
                                    this.setRequiredStatus('ContractNumber', false);
                                    this.setRequiredStatus('GroupAccountNumber', false);
                                }
                                if (this.getControlValue('AccountNumber')) {
                                    this.displayMessage(MessageConstant.Message.RecordNotFound);
                                }
                            }
                        });
                } else {
                    this.disableControl('PremiseNumber', true);
                    if (this.getControlValue('ContractNumber') || this.getControlValue('GroupAccountNumber')) {
                        this.pageParams.isMandatory = false;
                        this.setRequiredStatus('ContractNumber', false);
                        this.setRequiredStatus('GroupAccountNumber', false);
                    } else {
                        this.setRequiredStatus('ContractNumber', true);
                        this.setRequiredStatus('AccountNumber', true);
                        this.setRequiredStatus('GroupAccountNumber', true);
                        this.pageParams.isMandatory = true;
                    }
                }
                break;
            case 'Contract':
                this.setControlValue('ContractName', '');
                this.resetValues('Contract');
                this.ellipsis.premise.childConfigParams['ContractNumber'] = '';
                this.ellipsis.premise.childConfigParams['ContractName'] = '';
                this.disableControl('PremiseNumber', true);
                if (this.getControlValue('ContractNumber')) {
                    this.commonUtils.getContractName(this.getControlValue('ContractNumber'), this.utils.getBusinessCode(), this.getControlValue('AccountNumber'))
                        .then(data => {
                            if (data[0][0]) {
                                this.setRequiredStatus('AccountNumber', false);
                                this.setRequiredStatus('GroupAccountNumber', false);
                                this.setControlValue('ContractName', data[0][0].ContractName);
                                this.ellipsis.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                                this.ellipsis.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                                this.pageParams.isMandatory = false;
                                this.disableControl('PremiseNumber', false);
                            } else {
                                if (!this.getControlValue('AccountNumber') || !this.getControlValue('GroupAccountNumber')) {
                                    this.pageParams.isMandatory = false;
                                    this.setRequiredStatus('AccountNumber', false);
                                    this.setRequiredStatus('GroupAccountNumber', false);
                                }
                                if (this.getControlValue('ContractNumber')) {
                                    this.displayMessage(MessageConstant.Message.RecordNotFound);
                                }
                            }
                        });
                } else {
                    if (this.getControlValue('AccountNumber') || this.getControlValue('GroupAccountNumber')) {
                        this.pageParams.isMandatory = false;
                        this.setRequiredStatus('AccountNumber', false);
                        this.setRequiredStatus('GroupAccountNumber', false);
                    } else {
                        this.setRequiredStatus('ContractNumber', true);
                        this.setRequiredStatus('AccountNumber', true);
                        this.setRequiredStatus('GroupAccountNumber', true);
                        this.pageParams.isMandatory = true;
                    }
                }
                break;
            case 'Premise':
                this.setControlValue('PremiseName', '');
                if (this.getControlValue('PremiseNumber')) {
                    this.commonUtils.getPremiseName(this.getControlValue('PremiseNumber'), this.utils.getBusinessCode(), this.getControlValue('ContractNumber'))
                        .then(data => {
                            if (data[0][0]) {
                                this.setControlValue('PremiseName', data[0][0].PremiseName);
                                this.ellipsis.product.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                                this.ellipsis.product.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                                this.ellipsis.product.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
                                this.ellipsis.product.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
                            } else {
                                this.displayMessage(MessageConstant.Message.RecordNotFound);
                            }
                        });
                }
                break;
            case 'SearchType':
                this.buildGrid();
                switch (this.getControlValue('SearchType')) {
                    case 'PremiseVisitInd':
                        this.browserTitle = this.pageTitle = 'Contract Premises Visit Summary';
                        break;
                    case 'DetailInd':
                    case 'SummaryInd':
                        this.browserTitle = this.pageTitle = 'Contract Service Visit Summary';
                        break;
                }
                break;
        }
    }

    public onEllipsisDataRecieved(data: any, type: string): void {
        this.buildGrid();
        switch (type) {
            case 'GroupAccount':
                this.setControlValue('GroupAccountNumber', data.GroupAccountNumber);
                this.setControlValue('GroupName', data.GroupName);
                this.resetValues('GroupAccount');
                this.ellipsis.account.childConfigParams['groupAccountNumber'] = this.getControlValue('GroupAccountNumber');
                this.ellipsis.account.childConfigParams['groupName'] = this.getControlValue('GroupName');
                this.ellipsis.contract.childConfigParams['accountNumber'] = '';
                this.setRequiredStatus('ContractNumber', false);
                this.setRequiredStatus('AccountNumber', false);
                this.pageParams.isMandatory = false;
                break;
            case 'Account':
                this.setControlValue('AccountNumber', data.AccountNumber);
                this.setControlValue('AccountName', data.AccountName);
                this.resetValues('Account');
                this.ellipsis.contract.childConfigParams['accountNumber'] = this.getControlValue('AccountNumber');
                this.ellipsis.contract.childConfigParams['accountName'] = this.getControlValue('AccountName');
                this.setRequiredStatus('ContractNumber', false);
                this.setRequiredStatus('GroupAccountNumber', false);
                this.pageParams.isMandatory = false;
                break;
            case 'Contract':
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('ContractName', data.ContractName);
                this.resetValues('Contract');
                this.ellipsis.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                this.ellipsis.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                this.setRequiredStatus('GroupAccountNumber', false);
                this.setRequiredStatus('AccountNumber', false);
                this.disableControl('PremiseNumber', !this.getControlValue('ContractNumber'));
                this.pageParams.isMandatory = false;
                break;
            case 'Premise':
                this.setControlValue('PremiseNumber', data.PremiseNumber);
                this.setControlValue('PremiseName', data.PremiseName);
                this.ellipsis.product.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
                this.ellipsis.product.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
                break;
        }
    }

    public onGridBodyDoubleClick(): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'PremiseNumber':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    'parentMode': 'DailyTransactions',
                    'ContractNumber': this.riGrid.Details.GetValue('Contract'),
                    'PremiseNumber': this.riGrid.Details.GetValue('PremiseNumber')
                });
                break;
            case 'ServiceDateStart':
                if (this.getControlValue('SearchType') === 'PremiseVisitInd')
                    this.navigate('Summary', InternalMaintenanceServiceModuleRoutes.ICABSSEPREMISEVISITMAINTENANCE, {
                        'parentMode': 'Summary',
                        'PremiseVisitRowID': this.riGrid.Details.GetAttribute('ServiceDateStart', 'rowid')
                    });
                break;
            case 'PrintTreatment':
                if (this.riExchange.validateForm(this.uiForm)) {
                    let search: QueryParams = this.getURLSearchParamObject();
                    search.set(this.serviceConstants.Action, '6');

                    search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
                    search.set('Function', 'Single');
                    search.set('PremiseVisitRowID', this.riGrid.Details.GetAttribute('PrintTreatment', 'rowid'));
                    this.ajaxSource.next(this.ajaxconstant.START);
                    this.httpService.fetchReportURLGET({ operation: this.fetchReportURLParams.operation, search: search })
                        .then(_data => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        }).catch(error => {
                            this.displayMessage(error);
                        });
                }
                break;
            case 'ProductCode':
                this.navigate('Summary', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                    'CurrentContractTypeURLParameter': 'C',
                    'parentMode': 'Summary',
                    'currentContractType': 'C',
                    'ServiceCoverRowID': this.riGrid.Details.GetAttribute('ProductCode', 'rowID')
                });
                break;
            case 'Information':
                let information: string = this.riGrid.Details.GetAttribute('Information', 'additionalproperty');
                this.modalAdvService.emitMessage(new ICabsModalVO(information || 'No Special Instructions'));
                break;
            case 'VisitTypeCode':
                this.navigate('Summary', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE, {
                    ProductCode: this.riGrid.Details.GetValue('ProductCode'),
                    ServiceVisitRowID: this.riGrid.Details.GetAttribute('VisitTypeCode', 'rowid'),
                    'CurrentContractTypeURLParameter': 'C',
                    'currentContractType': 'C'
                });
                break;
        }
    }

    public generateReport(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');

            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set('GroupAccountNumber', this.getControlValue('GroupAccountNumber'));
            search.set('AccountNumber', this.getControlValue('AccountNumber'));
            search.set('ContractNumber', this.getControlValue('ContractNumber'));
            search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
            search.set('PostCode', this.getControlValue('PostCode'));
            search.set('ServiceDateFrom', this.getControlValue('ServiceDateFrom'));
            search.set('ServiceDateTo', this.getControlValue('ServiceDateTo'));

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.fetchReportURLGET({ operation: this.fetchReportURLParams.operation, search: search })
                .then(_data => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                }).catch(error => {
                    this.displayMessage(error);
                });
        }
    }

    public resetValues(type: string): void {
        switch (type) {
            case 'GroupAccount':
                this.setControlValue('AccountNumber', '');
                this.setControlValue('AccountName', '');
                this.setControlValue('ContractNumber', '');
                this.setControlValue('ContractName', '');
                this.setControlValue('PremiseNumber', '');
                this.setControlValue('PremiseName', '');
                break;
            case 'Account':
                this.setControlValue('ContractNumber', '');
                this.setControlValue('ContractName', '');
                this.setControlValue('PremiseNumber', '');
                this.setControlValue('PremiseName', '');
                break;
            case 'Contract':
                this.setControlValue('PremiseNumber', '');
                this.setControlValue('PremiseName', '');
                break;
        }
    }
}
