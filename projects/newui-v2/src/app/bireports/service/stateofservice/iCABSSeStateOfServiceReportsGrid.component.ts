import { Component, OnInit, OnDestroy, Injector, AfterContentInit, ViewChild } from '@angular/core';

import { AppWebWorker } from '@shared/web-worker/app-worker';
import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { ContractSearchComponent } from '@app/internal/search/iCABSAContractSearch';
import { DropdownConstants } from '@app/base/ComponentConstants';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { RefreshWorker } from '@shared/components/grid-advanced/refresh-worker';
import { ServiceConstants } from '@shared/constants/service.constants';
import { ServiceTypeSearchComponent } from '@app/internal/search/iCABSBServiceTypeSearch.component';
import { SOSDropDownService } from './sosDropDown.service';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { WebWorkerService } from '@shared/web-worker/web-worker';
import * as moment from 'moment';

@Component({
    templateUrl: 'iCABSSeStateOfServiceReportsGrid.html',
    providers: [CommonLookUpUtilsService, SOSDropDownService]
})

export class StateOfServiceReportsGridComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('serviceType') serviceType: ServiceTypeSearchComponent;

    private dateFrom: any;
    private dateTo: any;
    private isBatchProcess: boolean;

    public blnAgedReport: boolean;
    public contractSearchComponent: any;
    public employeeSearchComponent = EmployeeSearchComponent;
    public pageId: string;
    public pageType: string;
    public serviceConstants: ServiceConstants;
    public showCompanyCode: boolean;
    public URLReturn: string;
    public controls: Array<Object> = [
        { name: 'BusinessDesc', disabled: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeTextFree },
        { name: 'CompanyCode', disabled: true, type: MntConst.eTypeTextFree },
        { name: 'CompanyDesc', disabled: true, type: MntConst.eTypeTextFree },
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'ServiceTypeCode', type: MntConst.eTypeText, disabled: true },
        { name: 'ServiceTypeDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'SupervisorEmployeeCode', type: MntConst.eTypeCode },
        { name: 'SupervisorSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'RegionCode', type: MntConst.eTypeText, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ReportGroup', type: MntConst.eTypeText },
        { name: 'ReportGroupCode', type: MntConst.eTypeText },
        { name: 'ReportGroupDesc', type: MntConst.eTypeText },
        { name: 'DateSelect', type: MntConst.eTypeText, value: 'SpecifyDate' },
        { name: 'chkDoneBy', type: MntConst.eTypeText },
        { name: 'ShowType', type: MntConst.eTypeText, value: 'Contract' },
        { name: 'ShowPortfolioType', type: MntConst.eTypeText, value: 'AllContracts' },
        { name: 'VisitFrequency', type: MntConst.eTypeInteger },
        { name: 'DateFrom', type: MntConst.eTypeDate },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true },
        { name: 'DateDone', type: MntConst.eTypeDate, required: true },
        { name: 'GroupBy', type: MntConst.eTypeText, value: 'Area' },
        { name: 'FilterType', type: MntConst.eTypeText, value: 'All' },
        { name: 'NumberOfVisits', type: MntConst.eTypeInteger },
        { name: 'ProductServiceGroupCode', type: MntConst.eTypeText },
        { name: 'ProductServiceGroupDesc', type: MntConst.eTypeText }

    ];
    public inputParams: any = {
        'parentMode': 'Lookup',
        'showAddNew': false,
        'showHeader': true,
        'showCloseButton': true
    };
    public gridConfig: any = {
        pageSize: 8,
        totalRecords: 1,
        actionGrid: '2',
        actionBatch: '6',
        riSortOrder: ''
    };
    public companyInputParams: any = {
    };
    public companyDefault: Object = {
        id: '',
        text: ''
    };
    public serviceTypeDefault: Object = {
        id: '',
        text: ''
    };
    public productGroupDefault: Object = {
        id: '',
        text: ''
    };
    public dropdown: any = {
        serviceType: {
            params: {
                'parentMode': 'LookUpMultiple'
            },
            type: 'InServiceTypeCode'
        },
        company: {
            type: 'CompanyCode'
        },
        employee: {
            type: 'SupervisorEmployeeCode',
            parentMode: 'LookUp-Supervisor'
        },
        contract: {
            type: 'ContractNumber'
        },
        productServiceGroup: {
            type: 'productServiceGroup',
            params: {
                parentMode: 'LookUp'
            }
        },
        reportGroup: {
            type: 'reportGroup'
        }
    };

    constructor(injector: Injector, private lookupUtils: CommonLookUpUtilsService, public sosService: SOSDropDownService, private syscharConstants: SysCharConstants) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESTATEOFSERVICEREPORTSGRID;
        this.companyInputParams[this.serviceConstants.CountryCode] = this.countryCode();
        this.companyInputParams[this.serviceConstants.BusinessCode] = this.businessCode();
        this.companyInputParams['parentMode'] = 'LookUp';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageType = this.riExchange.getParentHTMLValue('pageType');
        this.pageParams.currentContractType = this.riExchange.getParentHTMLValue('contractType');
        if (this.riExchange.URLParameterContains('Aged') || this.riExchange.URLParameterContains('AgedSOS')) {
            this.blnAgedReport = true;
            if (!this.isReturning()) {
                this.setControlValue('ShowType', 'Contract');
                this.setControlValue('FilterType', 'Behind');
                this.setControlValue('DateSelect', 'Rolling12Months');
            }
        } else {
            this.blnAgedReport = false;
        }

        this.getSysChars();
        this.onWindowLoad();
        this.checkTimeOut();
    }

    public checkTimeOut(): void {
        let worker: AppWebWorker = new AppWebWorker();
        let workerService: WebWorkerService;
        workerService = new WebWorkerService(RefreshWorker);
        workerService.run(worker.delegatePromise, {}, true);
        workerService.worker.onmessage = response => {
            this.onRiGridRefresh();
        };
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();

        let companyCode: string = this.getControlValue('CompanyCode');
        if (companyCode) {
            this.companyDefault = {
                id: companyCode,
                text: companyCode + ' - ' + this.getControlValue('CompanyDesc')
            };
        }
        if (this.getControlValue('ServiceTypeCode')) {
            this.serviceTypeDefault = {
                id: '',
                text: '',
                multiSelectDisplay: this.getControlValue('ServiceTypeCode')
            };
        }
        let productGroupCode: string = this.getControlValue('ProductServiceGroupCode');
        if (productGroupCode) {
            this.productGroupDefault = {
                id: productGroupCode,
                text: productGroupCode + ' - ' + this.getControlValue('ProductServiceGroupDesc')
            };
        }
        if (!this.isReturning()) {
            this.pageParams.gridCacheRefresh = true;
            let showType: any = this.getControlValue('ShowType');
            this.sosService.setDateSelect(showType, this.pageParams.enableJobsToInvoiceAfterVisit);
            if (this.getControlValue('DateSelect') === 'Rolling12Months')
                this.disableControl('DateFrom', true);
            this.pageParams.fromDateShow = true;
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            this.setControlValue('BusinessCode', this.businessCode());
            if (this.pageType === 'Branch') {
                this.setControlValue('BranchNumber', this.utils.getBranchCode());
                this.setControlValue('BranchName', this.utils.getBranchTextOnly());
            } else if (this.pageType === 'Region') {
                this.lookupUtils.getRegionDesc().subscribe(data => {
                    if (data && data[0] && data[0].length) {
                        this.setControlValue('RegionCode', data[0][0].RegionCode);
                        this.setControlValue('RegionDesc', data[0][0].RegionDesc);
                    }
                }, error => {
                    this.displayMessage(error);
                });
            }
        }
        this.onRiGridRefresh();
    }

    private getSysChars(): void {
        let syscharList: Array<number> = [
            this.syscharConstants.SystemCharStateOfServiceDefaultPeriodStart,
            this.syscharConstants.SystemCharStateOfServiceDefaultPeriodEnd,
            this.syscharConstants.SystemCharEnableDoneByDate,
            this.syscharConstants.SystemCharEnableCompanyCode,
            this.syscharConstants.SystemCharEnableJobsToInvoiceAfterVisit
        ];
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                let list: Array<any> = data.records;
                this.pageParams.stateOfServiceDefaultPeriodStart = list[0];
                this.pageParams.stateOfServiceDefaultPeriodEnd = list[1];
                this.pageParams.enableDoneByDate = list[2].Required;
                this.pageParams.enableCompanyCode = list[3].Required;
                this.pageParams.enableJobsToInvoiceAfterVisit = list[4].Required;
                if (this.getControlValue('chkDoneBy')) {
                    this.setRequiredStatus('DateDone', true);
                }
                this.setDates();
                if (this.blnAgedReport)
                    this.agedDate();
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });
    }

    private setDates(): any {
        //DateFrom
        if (this.pageParams.stateOfServiceDefaultPeriodStart.Required) {
            switch (this.pageParams.stateOfServiceDefaultPeriodStart.Text) {
                case 'Today':
                    this.dateFrom = this.globalize.parseDateToFixedFormat(this.utils.Today());
                    break;
                case 'Week':
                    this.dateFrom = this.globalize.parseDateToFixedFormat(moment().startOf('isoWeek').toDate());
                    break;
                case 'Month':
                    this.dateFrom = this.globalize.parseDateToFixedFormat(moment().startOf('month').toDate());
                    break;
                case 'Quarter':
                    this.dateFrom = this.globalize.parseDateToFixedFormat(moment().startOf('quarter').toDate());
                    break;
                case 'Year':
                    this.dateFrom = new Date(new Date().getFullYear(), 0, 1);
                    break;
            }
        } else {
            this.dateFrom = new Date(new Date().getFullYear(), 0, 1);
        }
        this.setControlValue('DateFrom', this.dateFrom);

        //DateTo
        if (this.pageParams.stateOfServiceDefaultPeriodEnd.Required) {
            switch (this.pageParams.stateOfServiceDefaultPeriodEnd.Text) {
                case 'Today':
                    this.dateTo = this.globalize.parseDateToFixedFormat(new Date());
                    break;
                case 'Week':
                    this.dateTo = this.globalize.parseDateToFixedFormat(moment().startOf('week').toDate());
                    break;
                case 'Month':
                    this.dateTo = this.globalize.parseDateToFixedFormat(moment().endOf('month').toDate());
                    break;
                case 'Quarter':
                    this.dateTo = this.globalize.parseDateToFixedFormat(moment().endOf('quarter').toDate());
                    break;
            }
        } else {
            this.dateTo = new Date(new Date().getFullYear(), 0, 1);
        }
        this.setControlValue('DateTo', this.dateTo);
    }

    private agedDate(): void {
        let toDate = this.blnAgedReport ? this.globalize.parseDateToFixedFormat(new Date()) : this.dateTo;
        let fromDate = this.blnAgedReport ? this.globalize.parseDateToFixedFormat(new Date(new Date().getFullYear(), 0, 1)) : this.dateFrom;

        let toDateRoll: Date = new Date(toDate);
        toDateRoll.setDate(toDateRoll.getDate() - 1);
        let rollingDt = this.blnAgedReport ? this.globalize.parseDateToFixedFormat(toDateRoll) : this.dateTo;

        switch (this.getControlValue('DateSelect')) {
            case 'SpecifyDate':
                this.pageParams.fromDateShow = true;
                this.setControlValue('DateFrom', fromDate);
                this.setControlValue('DateTo', toDate);
                this.disableControl('DateFrom', false);
                break;
            case 'AnniversaryDate':
                this.pageParams.fromDateShow = false;
                this.setControlValue('DateTo', toDate);
                this.disableControl('DateFrom', false);
                break;
            case 'Rolling12Months':
                this.pageParams.fromDateShow = true;
                this.setControlValue('DateTo', rollingDt);
                this.setControlValue('DateFrom', this.utils.rollBackTwelveMonths(rollingDt));
                this.disableControl('DateFrom', true);
                break;
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        this.riGrid.AddColumn('ReportNumber', 'StateOfService', 'ReportNumber', MntConst.eTypeInteger, 15, true, 'Report Number');
        this.riGrid.AddColumnAlign('ReportNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('GeneratedDate', 'StateOfService', 'GeneratedDate', MntConst.eTypeDate, 10, false, 'Report Generated On');
        this.riGrid.AddColumnAlign('ReportNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('GeneratedTime', 'StateOfService', 'GeneratedTime', MntConst.eTypeTime, 10, false, 'Report Generated At');
        this.riGrid.AddColumnAlign('GeneratedTime', MntConst.eAlignmentCenter);
        if (this.pageType === 'Branch') {
            this.riGrid.AddColumn('SupervisorEmployeeCode', 'StateOfService', 'SupervisorEmployeeCode', MntConst.eTypeCode, 6, false, 'Supervisor Employee');
            this.riGrid.AddColumnAlign('SupervisorEmployeeCode', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('CompanyCode', 'StateOfService', 'CompanyCode', MntConst.eTypeCode, 2, false, 'Company Code');
        this.riGrid.AddColumnAlign('CompanyCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceTypeCode', 'StateOfService', 'ServiceTypeCode', MntConst.eTypeCode, 6, false, 'Service Type Code');
        this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('VisitFrequency', 'StateOfService', 'VisitFrequency', MntConst.eTypeCode, 3, false, 'Visit Frequency');
        this.riGrid.AddColumnAlign('VisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ProductServiceGroupCode', 'StateOfService', 'ProductServiceGroupCode', MntConst.eTypeCode, 6, false, 'Product Group Code');
        this.riGrid.AddColumnAlign('ProductServiceGroupCode', MntConst.eAlignmentCenter);
        if (this.getControlValue('ShowType') === 'Contract') {
            this.riGrid.AddColumn('DateFrom', 'StateOfService', 'DateFrom', MntConst.eTypeDate, 10, false, 'From Date');
            this.riGrid.AddColumnAlign('DateFrom', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('DateTo', 'StateOfService', 'DateTo', MntConst.eTypeDate, 10, false, 'To Date');
            this.riGrid.AddColumnAlign('DateTo', MntConst.eAlignmentCenter);
        } else {
            this.riGrid.AddColumn('ExcludeFrom', 'StateOfService', 'ExcludeFrom', MntConst.eTypeDate, 10, false, 'Exclude From Date');
            this.riGrid.AddColumnAlign('ExcludeFrom', MntConst.eAlignmentCenter);
        }
        if (!this.blnAgedReport) {
            if (this.pageParams.enableDoneByDate) {
                this.riGrid.AddColumn('DateDone', 'StateOfService', 'DateDone', MntConst.eTypeDate, 10, false, 'Done By Date');
                this.riGrid.AddColumnAlign('DateDone', MntConst.eAlignmentCenter);
            }
            this.riGrid.AddColumn('FilterType', 'StateOfService', 'FilterType', MntConst.eTypeText, 15, false, 'Filter By');
            this.riGrid.AddColumnAlign('FilterType', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('GroupBy', 'StateOfService', 'GroupBy', MntConst.eTypeText, 15, false, 'Group By');
            this.riGrid.AddColumnAlign('GroupBy', MntConst.eAlignmentCenter);
            if (this.getControlValue('ShowType') === 'Contract') {
                this.riGrid.AddColumn('NumberOfVisits', 'StateOfService', 'NumberOfVisits', MntConst.eTypeInteger, 15, false, 'By Number of Visits');
                this.riGrid.AddColumnAlign('NumberOfVisits', MntConst.eAlignmentCenter);
            }

        }
        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm) && this.getControlValue('DateTo')) {
            let gridSearch: QueryParams = this.getURLSearchParamObject();
            gridSearch.set(this.serviceConstants.Action, this.isBatchProcess ? this.gridConfig.actionBatch.toString() : this.gridConfig.actionGrid.toString());
            let form: any = {};
            if (this.blnAgedReport) {
                this.setControlValue('DateDone', this.getControlValue('DateTo'));
            }
            form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
            form['CompanyCode'] = this.getControlValue('CompanyCode');
            form['DateSelect'] = this.getControlValue('DateSelect');
            form['DateFrom'] = this.getControlValue('DateFrom');
            form['DateTo'] = this.getControlValue('DateTo');
            form['DateDone'] = this.getControlValue('DateDone');
            form['ShowType'] = this.getControlValue('ShowType');
            form['Level'] = this.riExchange.getParentHTMLValue('pageType');
            form['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
            form['NumberOfVisits'] = this.getControlValue('NumberOfVisits');
            form['VisitFrequency'] = this.getControlValue('VisitFrequency');
            form['ProductServiceGroupCode'] = this.getControlValue('ProductServiceGroupCode');
            form['ShowPortfolioType'] = this.getControlValue('ShowPortfolioType');
            form[this.serviceConstants.GridMode] = '0';
            form[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            form[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
            form[this.serviceConstants.PageSize] = this.gridConfig.pageSize;
            form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1;
            form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            form['riSortOrder'] = this.riGrid.SortOrder;
            form['FilterType'] = this.getControlValue('FilterType');
            form['GroupBy'] = this.getControlValue('GroupBy');
            form['ContractNumber'] = this.getControlValue('ContractNumber');
            form['ReportGroupCode'] = this.getControlValue('ReportGroupCode');
            form['BatchBuildType'] = this.getControlValue('GroupBy') === 'Town' ? 'GroupByTown' : 'Standard';
            form['SubmitBatchProcess'] = this.utils.convertCheckboxValueToRequestValue(this.isBatchProcess);
            form['AgedPortfolio'] = this.utils.convertCheckboxValueToRequestValue(this.blnAgedReport);
            if (this.pageType === 'Branch') {
                form['BranchNumber'] = this.getControlValue('BranchNumber');
                form['SupervisorEmployeeCode'] = this.getControlValue('SupervisorEmployeeCode');
            } else if (this.pageType === 'Region') {
                form['RegionCode'] = this.getControlValue('RegionCode');
            }
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.isBatchProcess ? 'maintenance' : 'bi/reports', 'reports', 'Service/iCABSSeStateOfServiceReportsGrid', gridSearch, form).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.displayMessage(data);
                } else {
                    if (!this.isBatchProcess) {
                        this.riGrid.RefreshRequired();
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                        this.riGrid.Execute(data);
                        setTimeout(() => {
                            this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                        }, 100);
                    }
                }
            }).catch((error) => {
                this.displayMessage(error);
            });
        }
    }

    public onRiGridRefresh(): void {
        this.isBatchProcess = false;
        this.riGrid.RefreshRequired();
        this.buildGrid();
        this.populateGrid();
    }

    public onWindowLoad(): void {
        if (this.pageType === 'Business') {
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
        }
        this.contractSearchComponent = ContractSearchComponent;
        this.sosService.getReportGroupData();
        this.setControlValue('NumberOfVisits', 1);
        this.browserTitle = this.pageTitle = this.pageType + ' State Of Service' + (this.blnAgedReport ? ' Aged Portfolio' : '') + ' Reports';
    }

    public resetValues(): void {
        this.pageParams.gridCurrentPage = 1;
        this.gridConfig.totalRecords = 1;
        this.riGrid.Clear();
        this.riGrid.RefreshRequired();
    }

    //Batch process submission
    public onGenerateReport(): void {
        if (this.riExchange.validateForm(this.uiForm) && this.getControlValue('DateTo')) {
            this.isBatchProcess = true;
            let gridSearch: QueryParams = this.getURLSearchParamObject();
            gridSearch.set(this.serviceConstants.Action, this.isBatchProcess ? this.gridConfig.actionBatch.toString() : this.gridConfig.actionGrid.toString());
            if (this.blnAgedReport) {
                this.setControlValue('DateDone', this.getControlValue('DateTo'));
            }

            gridSearch.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            gridSearch.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
            gridSearch.set('CompanyCode', this.getControlValue('CompanyCode'));
            gridSearch.set('DateSelect', this.getControlValue('DateSelect'));
            gridSearch.set('DateFrom', this.getControlValue('DateFrom'));
            gridSearch.set('DateTo', this.getControlValue('DateTo'));
            gridSearch.set('DateDone', this.getControlValue('DateDone'));
            gridSearch.set('ShowType', this.getControlValue('ShowType'));
            if (this.getControlValue('VisitFrequency')) {
                gridSearch.set('VisitFrequency', this.getControlValue('VisitFrequency'));
            }
            if (this.getControlValue('ServiceTypeCode')) {
                gridSearch.set('ServiceTypeCode', this.getControlValue('ServiceTypeCode'));
            }
            if (this.getControlValue('FilterType') !== 'All' && this.getControlValue('FilterType') !== 'NoVisits' && this.getControlValue('FilterType') !== 'NotReleased' && this.getControlValue('NumberOfVisits')) {
                gridSearch.set('NumberOfVisits', this.getControlValue('NumberOfVisits'));
            }
            if (this.getControlValue('ProductServiceGroupCode')) {
                gridSearch.set('ProductServiceGroupCode', this.getControlValue('ProductServiceGroupCode'));
            }
            if (this.getControlValue('ShowPortfolioType')) {
                gridSearch.set('ShowPortfolioType', this.getControlValue('ShowPortfolioType'));
            }
            gridSearch.set('Function', 'SubmitBatchProcess');
            gridSearch.set('FilterType', this.getControlValue('FilterType'));
            gridSearch.set('GroupBy', this.getControlValue('GroupBy'));
            gridSearch.set('ContractNumber', this.getControlValue('ContractNumber'));
            gridSearch.set('ReportGroupCode', this.getControlValue('ReportGroupCode'));
            gridSearch.set('BatchBuildType', this.getControlValue('GroupBy') === 'Town' ? 'GroupByTown' : 'Standard');
            gridSearch.set('SubmitBatchProcess', 'yes');
            gridSearch.set('AgedPortfolio', 'no');
            if (this.pageType === 'Branch') {
                gridSearch.set('BranchNumber', this.getControlValue('BranchNumber'));
                if (this.getControlValue('SupervisorEmployeeCode'))
                    gridSearch.set('SupervisorEmployeeCode', this.getControlValue('SupervisorEmployeeCode'));
            } else if (this.pageType === 'Region') {
                gridSearch.set('RegionCode', this.getControlValue('RegionCode'));
            }
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest('bi/reports', 'reports', 'Service/iCABSSeStateOfServiceReportsGrid', gridSearch)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (this.hasError(data)) {
                            this.displayMessage(data);
                        } else {
                            this.URLReturn = data.BatchProcessInformation;
                        }
                    },
                    (error) => {
                        this.displayMessage(error);
                    });
        }
    }

    public onDataRecieved(data: any, type: any): void {
        if (data) {
            switch (type) {
                case this.dropdown.serviceType.type:
                    this.setControlValue('ServiceTypeCode', data[DropdownConstants.c_s_KEY_MULTISELECTVALUE]);
                    break;
                case this.dropdown.company.type:
                    this.setControlValue('CompanyCode', data.CompanyCode);
                    this.setControlValue('CompanyDesc', data.CompanyDesc);
                    this.resetValues();
                    break;
                case this.dropdown.employee.type:
                    this.setControlValue('SupervisorEmployeeCode', data.SupervisorEmployeeCode);
                    this.setControlValue('SupervisorSurname', data.SupervisorSurname);
                    this.resetValues();
                    break;
                case this.dropdown.productServiceGroup.type:
                    this.setControlValue('ProductServiceGroupCode', data.ProductServiceGroupCode);
                    this.setControlValue('ProductServiceGroupDesc', data.ProductServiceGroupDesc);
                    break;
                case this.dropdown.contract.type:
                    this.setControlValue('ContractNumber', data.ContractNumber);
                    this.setControlValue('ContractName', data.ContractName);
                    break;
            }
        }

    }

    public onControlChange(controlId: any): void {
        this.resetValues();
        switch (controlId) {
            case 'ContractNumber':
                this.setControlValue('ContractName', '');
                this.lookupUtils.getContractName(this.getControlValue('ContractNumber')).then((data) => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('ContractName', data[0][0].ContractName);
                    }
                });
                break;
            case 'SupervisorEmployeeCode':
                this.setControlValue('SupervisorSurname', '');
                this.lookupUtils.getEmployeeSurname(this.getControlValue('SupervisorEmployeeCode')).then((data) => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('SupervisorSurname', data[0][0].EmployeeSurname);
                    }
                });
                break;
            case 'chkDoneBy':
                if (!this.getControlValue('chkDoneBy')) {
                    this.setControlValue('DoneBy', '');
                }
                break;
            case 'showTypeValues':
                let showType: any = this.getControlValue('ShowType');
                this.sosService.setDateSelect(showType, this.pageParams.enableJobsToInvoiceAfterVisit);
                this.setControlValue('DateSelect', this.sosService.dateSelectValues[0].value);
                this.setControlValue('FilterType', this.sosService.filterValues[0].value);
                this.disableControl('DateFrom', false);
                this.pageParams.fromDateShow = showType !== 'Job';
                break;
            case 'DateSelect':
                this.setControlValue('DateDone', this.getControlValue('chkDoneBy') ? this.dateTo : '');
                this.agedDate();
                break;
            case 'DateTo':
                this.setControlValue('DateDone', this.getControlValue('DateTo'));
                if (this.getControlValue('DateSelect') === 'Rolling12Months') {
                    this.setControlValue('DateFrom', this.utils.rollBackTwelveMonths(this.getControlValue('DateTo')));
                }
                break;
        }
    }

    public onGridBodyDoubleClick(): void {
        let params: any = {
            'ROWID': this.riGrid.Details.GetAttribute('ReportNumber', 'rowid'),
            'ReportNumber': this.riGrid.Details.GetValue('ReportNumber'),
            'RawRepGroupBy': this.riGrid.Details.GetAttribute('GroupBy', 'additionalproperty'),
            'RepGroupBy': this.riGrid.Details.GetValue('GroupBy'),
            'type': this.pageType
        };

        switch (this.pageType) {
            case 'Business':
                if (this.blnAgedReport) {
                    this.navigate('StateOfService', BIReportsRoutes.ICABSSEBATCHSOSAGEDPROFILEBUSINESSGRID, params);
                } else
                    this.navigate('StateOfService', BIReportsRoutes.ICABSSEBATCHSTATEOFSERVICEBUSINESSGRID, params);
                break;
            case 'Branch':
                if (this.blnAgedReport) {
                    this.navigate('StateOfService', BIReportsRoutes.ICABSSEBATCHSOSAGEDPROFILEBRANCHGRID, params);
                } else {
                    this.navigate('StateOfService', BIReportsRoutes.ICABSSEBATCHSTATEOFSERVICEBRANCHGRID, params);
                }
                break;
            case 'Region':
                if (this.blnAgedReport) {
                    this.displayMessage(MessageConstant.Message.PageNotCovered, CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                } else {
                    this.navigate('StateOfService', BIReportsRoutes.ICABSSEBATCHSTATEOFSERVICEREGIONGRID, params);
                }
                break;
        }

    }

    public getCurrentPage(currentPage: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridCacheRefresh = false;
            super.getCurrentPage(currentPage);
        }
    }
}
