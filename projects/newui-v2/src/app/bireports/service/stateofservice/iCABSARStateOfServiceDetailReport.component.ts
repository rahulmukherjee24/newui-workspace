import { Component, Injector, OnInit } from '@angular/core';

import * as moment from 'moment';
import { BranchServiceAreaSearchComponent } from '@app/internal/search/iCABSBBranchServiceAreaSearch';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { DropdownConstants } from '@app/base/ComponentConstants';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSARStateOfServiceDetailReport.html',
    providers: [CommonLookUpUtilsService]
})

export class StateOfServiceDetailReportComponent extends LightBaseComponent implements OnInit {
    public pageId: string;
    public pageTitle: string;
    public controls: IControls[] = [
        { name: 'BranchName', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaCode', disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'BusinessCode', disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BusinessDesc', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'DateDone', required: false, type: MntConst.eTypeDate },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateSelect', value: 'SpecifyDate' },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EmployeeSurname', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'FilterType', value: 'All' },
        { name: 'NumberOfVisits', type: MntConst.eTypeInteger, value: 1 },
        { name: 'ProductServiceGroupCode' },
        { name: 'RepDest', value: 'direct' },
        { name: 'SeasonalSelect', value: 'All' },
        { name: 'ServiceTypeCode' },
        { name: 'ShowType', value: 'Contract' },
        { name: 'SortBy', value: 'ServiceArea' },
        { name: 'SupervisorEmployeeCode', disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'SupervisorSurname', disabled: true, required: false, type: MntConst.eTypeText }
    ];
    public seasonalSelectArray: Array<Object> = [];
    public dateSelectArray: Array<Object> = [];
    public filterTypeArray: Array<Object> = [];
    public batchSubmittedText: string = '';
    public ellipsis: Record<string, Record<string, Object>> = {
        supervisoreEmployeeEllipsis: {
            childparams: {
                'parentMode': 'LookUp-Supervisor'
            },
            component: EmployeeSearchComponent
        },
        serviceArea: {
            childparams: {
                parentMode: 'LookUp-Emp'
            },
            component: BranchServiceAreaSearchComponent
        }
    };

    public dropdown: Record<string, Record<string, Object>> = {
        serviceTypeSearch: {
            params: {
                parentMode: 'LookUpMultiple'
            }
        },
        productServiceGroupSearch: {
            params: {
                parentMode: 'LookUpMultiple',
                ProductServiceGroupCode: ''
            }
        }
    };
    public showNumOfVisits: boolean = false;

    private dateFrom: any;
    private dateTo: any;

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService, private sysCharConstants: SysCharConstants) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARSTATEOFSERVICEDETAILREPORT;
        this.browserTitle = this.pageTitle = 'State Of Service Detail';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.setControlValue('BusinessCode', this.utils.getBusinessCode());
        this.setControlValue('BusinessDesc', this.utils.getBusinessText());
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchTextOnly());
        this.loadSysChars();
        this.dropdownBuildOptions();
    }

    public onChnage(event: any, value: string): void {
        switch (value) {
            case 'SupervisorEmployeeCode':
                this.setControlValue('SupervisorEmployeeCode', event.SupervisorEmployeeCode);
                this.setControlValue('SupervisorSurname', event.SupervisorSurname);
                break;
            case 'BranchServiceAreaCode':
                this.setControlValue('BranchServiceAreaCode', event.BranchServiceAreaCode);
                this.setControlValue('EmployeeSurname', event.EmployeeSurname);
                break;
        }
    }

    public onInputChnage(event: any): void {
        switch (event.target.id) {
            case 'SupervisorEmployeeCode':
                this.getPromiseCall('getEmployeeSurname', 'SupervisorSurname', 'EmployeeSurname', event);
                break;
            case 'BranchServiceAreaCode':
                this.getPromiseCall('getBranchServiceAreaDesc', 'EmployeeSurname', 'BranchServiceAreaDesc', event);
                break;
        }
    }

    private getPromiseCall(funcNeme: string, displayfeild: string, queryField: string, event: any): void {
        this.commonLookup[funcNeme](this.getControlValue(event.target.id)).then(data => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue(displayfeild, data[0][0][queryField]);
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    private loadSysChars(): void {
        let syscharList: Array<number> = [
            this.sysCharConstants.SystemCharStateOfServiceDefaultPeriodStart,
            this.sysCharConstants.SystemCharStateOfServiceDefaultPeriodEnd,
            this.sysCharConstants.SystemCharEnableDoneByDate
        ];
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = this.getURLSearchParamObject();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data && data.records && data.records.length) {
                let list: Array<any> = data.records;
                this.pageParams.stateOfServiceDefaultPeriodStart = list[0];
                this.pageParams.stateOfServiceDefaultPeriodEnd = list[1];
                this.pageParams.enableDoneByDate = list[2].Required;
                this.setDates();
            }


        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }

    private setDates(): any {
        //DateFrom
        if (this.pageParams.stateOfServiceDefaultPeriodStart.Required) {
            switch (this.pageParams.stateOfServiceDefaultPeriodStart.Text) {
                case 'Today':
                    this.dateFrom = this.globalize.parseDateToFixedFormat(this.utils.Today());
                    break;
                case 'Week':
                    this.dateFrom = this.globalize.parseDateToFixedFormat(moment().startOf('isoWeek').toDate());
                    break;
                case 'Month':
                    this.dateFrom = this.globalize.parseDateToFixedFormat(moment().startOf('month').toDate());
                    break;
                case 'Quarter':
                    this.dateFrom = this.globalize.parseDateToFixedFormat(moment().startOf('quarter').toDate());
                    break;
                case 'Year':
                    this.dateFrom = new Date(new Date().getFullYear(), 0, 1);
                    break;
            }
        } else {
            this.dateFrom = new Date(new Date().getFullYear(), 0, 1);
        }
        this.setControlValue('DateFrom', this.dateFrom);

        //DateTo
        if (this.pageParams.stateOfServiceDefaultPeriodEnd.Required) {
            switch (this.pageParams.stateOfServiceDefaultPeriodEnd.Text) {
                case 'Today':
                    this.dateTo = this.globalize.parseDateToFixedFormat(new Date());
                    break;
                case 'Week':
                    this.dateTo = this.globalize.parseDateToFixedFormat(moment().startOf('week').toDate());
                    break;
                case 'Month':
                    this.dateTo = this.globalize.parseDateToFixedFormat(moment().endOf('month').toDate());
                    break;
                case 'Quarter':
                    this.dateTo = this.globalize.parseDateToFixedFormat(moment().endOf('quarter').toDate());
                    break;
            }
        } else {
            this.dateTo = new Date(new Date().getFullYear(), 0, 1);
        }
        this.setControlValue('DateTo', this.dateTo);
        if (this.pageParams.enableDoneByDate) {
            this.setControlValue('DateDone', this.dateTo);
        }
    }

    public onDropDownSelect(event: any): void {
        let key = Object.keys(event)[0];
        if (event.hasOwnProperty('multiSelectValue')) {
            this.setControlValue(key, event[DropdownConstants.c_s_KEY_MULTISELECTVALUE]);
        } else {
            this.setControlValue(key, event[key]);
        }
    }

    private dropdownBuildOptions(): void {
        this.dateSelectArray = [];
        this.filterTypeArray = [];
        switch (this.getControlValue('ShowType')) {
            case 'Contract':
                this.seasonalSelectArray.push({ name: 'Seasonal', value: 'Seasonal' });
                this.seasonalSelectArray.push({ name: 'NotSeasonal', value: 'NotSeasonal' });
                this.dateSelectArray.push({ name: 'Use Anniversary Date', value: 'AnniversaryDate' });
                this.dateSelectArray.push({ name: 'Rolling 12 Months', value: 'Rolling12Months' });
                this.filterTypeArray.push({ name: 'Ahead', value: 'Ahead' });
                this.filterTypeArray.push({ name: 'Behind', value: 'Behind' });
                this.filterTypeArray.push({ name: 'No Visits', value: 'NoVisits' });
                break;
            case 'Job':
                this.seasonalSelectArray = [];
                this.dateSelectArray.push({ name: 'Exclude From Date', value: 'ExcludeFromDate' });
                this.filterTypeArray.push({ name: 'Not Completed', value: 'NotCompleted' });
                this.filterTypeArray.push({ name: 'No Visits', value: 'NoVisits' });
                this.filterTypeArray.push({ name: 'Not Released', value: 'NotReleased' });
                break;
        }
    }

    public onDropDownChnage(event: any): void {
        switch (event.target.id) {
            case 'ShowType':
                this.dropdownBuildOptions();
                break;
            case 'DateSelect':
                if (this.pageParams.enableDoneByDate) {
                    this.setControlValue('DateDone', this.dateTo);
                }
                if (this.getControlValue('DateSelect') === 'Rolling12Months') {
                    let toDateRoll: Date = new Date(this.dateTo);
                    let rollingDt = this.globalize.parseDateToFixedFormat(toDateRoll);
                    this.setControlValue('DateTo', this.dateTo);
                    this.setControlValue('DateFrom', this.utils.rollBackTwelveMonths(rollingDt));
                    this.disableControl('DateFrom', true);
                    this.disableControl('DateTo', true);
                } else {
                    this.setControlValue('DateFrom', this.dateFrom);
                    this.setControlValue('DateTo', this.dateTo);
                    this.disableControl('DateFrom', false);
                    this.disableControl('DateTo', false);
                }
                break;
            case 'FilterType':
                const isRequired: boolean = ['Ahead', 'Behind'].includes(this.getControlValue('FilterType')); // IUI-24403
                this.showNumOfVisits = isRequired;
                this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'NumberOfVisits', isRequired);
                isRequired ? this.setControlValue('NumberOfVisits', '1') : this.setControlValue('NumberOfVisits', '');
                break;
        }
    }

    public submitReportGeneration(): void {
        if (this.uiForm.valid) {
            let exArr = ['BranchName', 'BusinessDesc', 'EmployeeSurname', 'SupervisorSurname'];
            const dt: Date = new Date();
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');

            let formData = {
            };
            let strName = `Mode${this.serviceConstants.PipeDelimeter}Level`;
            let strValue = `Business${this.serviceConstants.PipeDelimeter}Detail`;
            Object.keys(this.uiForm.controls).forEach(control => {
                if (exArr.indexOf(control) === -1) {
                    strName = `${strName}${this.serviceConstants.PipeDelimeter}${control}`;
                    strValue = `${strValue}${this.serviceConstants.PipeDelimeter}${this.getControlValue(control)}`;
                }
            });
            let value = this.getControlValue('RepDest') === 'direct' ? `batch|ReportID` : `email|User`;
            strName = `${strName}${this.serviceConstants.PipeDelimeter}RepManDest${this.serviceConstants.PipeDelimeter}`;
            strValue = `${strValue}${this.serviceConstants.PipeDelimeter}${value}${this.serviceConstants.PipeDelimeter}`;
            formData['Description'] = 'State Of Service Detail';
            formData['Report'] = 'report';
            formData['StartDate'] = this.globalize.parseDateToFixedFormat(dt);
            formData['StartTime'] = this.globalize.parseTimeToFixedFormat(moment(dt).format('HH:mm'));
            formData['ProgramName'] = 'iCABSStateOfServiceGeneration.p';
            formData['ParameterName'] = strName;
            formData['ParameterValue'] = strValue;

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'ApplicationReport/iCABSARStateOfServiceDetailReport', search, formData).then(
                (data) => {
                    this.isRequesting = false;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (this.hasError(data)) {
                        this.batchSubmittedText = data.fullError;
                        return;
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }
}
