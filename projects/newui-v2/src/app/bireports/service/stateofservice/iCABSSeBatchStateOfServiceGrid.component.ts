import { Component, Injector, OnInit, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';

import { BIReportsRoutes } from '@base/PageRoutes';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IGridHandlers } from '@base/BaseComponentLight';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SOSDropDownService } from './sosDropDown.service';
import { StaticUtils } from '@shared/services/static.utility';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSSeBatchStateOfServiceGrid.html',
    providers: [
        CommonLookUpUtilsService,
        SOSDropDownService
    ]
})

export class BatchStateOfServiceGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    /**
     * Kept Here Since Handled By SysChars
     */
    private reportViewOptionsDetails: any = {
        '1': { value: 'Values', text: 'Values' },
        '2': { value: 'Exchanges', text: 'Exchanges' },
        '3': { value: 'Times', text: 'Times' }
    };
    private xhrParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: {
            'Branch': 'Service/iCABSSeBatchStateOfServiceBranchGrid',
            'Business': 'Service/iCABSSeBatchStateOfServiceBusinessGrid',
            'Region': 'Service/iCABSSeBatchStateOfServiceRegionGrid'
        }
    };

    protected pageId: string;
    protected controls: any[] = [
        { name: 'BusinessCode', type: MntConst.eTypeCode },
        { name: 'BusinessDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'chkDoneBy', disabled: true, type: MntConst.eTypeCheckBox },
        { name: 'CompanyCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'CompanyDesc', disabled: true, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'DateDone', disabled: true, type: MntConst.eTypeDate },
        { name: 'DateFrom', disabled: true, type: MntConst.eTypeDate },
        { name: 'DateSelect', disabled: true, type: MntConst.eTypeText },
        { name: 'DateTo', disabled: true, type: MntConst.eTypeDate },
        { name: 'FilterType', disabled: true, type: MntConst.eTypeText },
        { name: 'NumberOfVisits', disabled: true, type: MntConst.eTypeInteger },
        { name: 'ProductServiceGroupCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ReportGroupCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ReportGroupDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ReportView', type: MntConst.eTypeText },
        { name: 'SeasonalSelect', value: 'All', type: MntConst.eTypeText },
        { name: 'ServiceTypeCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ShowPortfolioType', disabled: true, type: MntConst.eTypeText },
        { name: 'ShowType', disabled: true, type: MntConst.eTypeText },
        { name: 'ViewBy', value: 'Branch', type: MntConst.eTypeText },
        { name: 'VisitFrequency', disabled: true, type: MntConst.eTypeInteger },
        { name: 'SupervisorEmployeeCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'SupervisorSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'GroupBy', disabled: true, type: MntConst.eTypeText },
        { name: 'ReportNumber', type: MntConst.eTypeText },
        { name: 'ROWID', type: MntConst.eTypeText },
        { name: 'RawGroupBy', type: MntConst.eTypeText },
        { name: 'RegionCode', disabled: true, type: MntConst.eTypeText },
        { name: 'RegionDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'SelectLevel', type: MntConst.eTypeText, value: 'Branch' }
    ];

    public dateSelect: string = '';
    public filterType: string = '';
    public gridConfig: any = {
        totalItem: 0,
        itemsPerPage: 10
    };
    public pageType: string = '';

    constructor(injector: Injector, private syscharConstants: SysCharConstants, public dropdowns: SOSDropDownService) {
        super(injector);
        this.browserTitle = this.pageTitle = 'Business State Of Service';
    }

    // #region LifeCycle Hooks
    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageType = StaticUtils.capitalizeFirstLetterAlphaNumberic(this.riExchange.getParentHTMLValue('type'));

        this.pageId = PageIdentifier['ICABSSEBATCHSTATEOFSERVICE' + this.pageType.toUpperCase() + 'GRID'];
        this.pageTitle = this.pageType + ' State Of Service';
        this.utils.setTitle(this.pageTitle);

        super.ngAfterContentInit();

        if (!this.dropdowns.reportGroup.length) {
            this.dropdowns.getReportGroupData();
        }
        if (!this.isReturning()) {
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.reportViewOptions = [];
            this.pageParams.seasonalOptions = [];
            this.getSysChars();
        } else {
            this.pageParams.gridCacheRefresh = false;
            this.dropdowns.setSeasonalOptions(this.getControlValue('ShowType'));
            this.buildGrid();
            this.populateGrid();
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    // #endregion

    // #region Private Methods
    private getSysChars(): void {
        let syscharList: Array<number> = [
            this.syscharConstants.SystemCharEnableInstallsRemovals,
            this.syscharConstants.SystemCharEnableDoneByDate,
            this.syscharConstants.SystemCharEnableCompanyCode,
            this.syscharConstants.SystemCharStateOfServiceReportView
        ];
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                let list: Array<any> = data.records;
                this.pageParams.enableInstallsRemovals = list[0].Required;
                this.pageParams.enableDoneByDate = list[1].Required;
                this.pageParams.enableCompanyCode = list[2].Required;
                this.pageParams.stateOfServiceReportView = list[3].Required ? list[3].Text : '1,2,3';

                if (!list[3].Required) {
                    if (!this.pageParams.enableInstallsRemovals) {
                        this.pageParams.stateOfServiceReportView = '1';
                        this.setControlValue('ReportView', this.reportViewOptionsDetails['1'].value);
                    } else {
                        this.pageParams.stateOfServiceReportView = '1,2';
                    }
                }
                this.buildReportViewOptions();
                this.setControlValues();
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });
    }

    private setControlValues(): void {
        this.setControlValue('BusinessCode', this.businessCode());
        this.setControlValue('BusinessDesc', this.utils.getBusinessText());
        this.controls.forEach(control => {
            /**
             * Need To Update Control Only If It Does Not Have Values
             * Disabld ts-rule Purposefully
             */
            // tslint:disable-next-line: no-unused-expression
            this.getControlValue(control.name) || this.riExchange.getParentHTMLValue(control.name);
        });

        // Taken Into Variables To Save Service Calls
        this.filterType = this.getControlValue('FilterType');
        this.dateSelect = this.getControlValue('DateSelect');
        this.dropdowns.setSeasonalOptions(this.getControlValue('ShowType'));

        switch (this.pageType) {
            case 'Business':
            case 'Region':
                this.buildGrid();
                this.populateGrid();
                break;
            case 'Branch':
                if (!this.riExchange.getParentHTMLValue('SeasonalSelect')) {
                    this.setControlValue('SeasonalSelect', 'All');
                }
                this.getGroupBy();
                break;
        }
    }

    private buildReportViewOptions(): void {
        let list: Array<string> = this.pageParams.stateOfServiceReportView.split(',');

        list.forEach(item => {
            this.pageParams.reportViewOptions.push(this.reportViewOptionsDetails[item]);
        });
        if (this.pageType === 'Business' || !this.riExchange.getParentHTMLValue('ReportView')) {
            this.setControlValue('ReportView', this.pageParams.reportViewOptions[0].value);
        }
    }

    // #region Build Grid Methods
    private buildGrid(): void {
        let viewBy: string = this.getControlValue('ViewBy');
        let rawGroupBy: string = this.getControlValue('RawGroupBy');
        this.riGrid.Clear();

        switch (this.pageType) {
            case 'Business':
                switch (viewBy) {
                    case 'Region':
                        this.riGrid.AddColumn('RegionCode', 'StateOfService', 'RegionCode', MntConst.eTypeCode, 6, true);
                        this.riGrid.AddColumn('RegionDesc', 'StateOfService', 'RegionDesc', MntConst.eTypeText, 14);

                        this.riGrid.AddColumnAlign('RegionCode', MntConst.eAlignmentCenter);
                        this.riGrid.AddColumnAlign('RegionDesc', MntConst.eAlignmentLeft);
                        break;
                    case 'Branch':
                        this.riGrid.AddColumn('BranchNumber', 'StateOfService', 'BranchNumber', MntConst.eTypeText, 6, true);
                        this.riGrid.AddColumn('BranchName', 'StateOfService', 'BranchName', MntConst.eTypeText, 14);

                        this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
                        this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentLeft);
                        break;
                }
                break;
            case 'Branch':
                if (rawGroupBy === 'Town') {
                    this.riGrid.AddColumn('GroupBy', 'StateOfService', 'GroupBy', MntConst.eTypeText, 15);
                }
                this.riGrid.AddColumn('BranchServiceAreaCode', 'StateOfService', 'BranchServiceAreaCode', MntConst.eTypeCode, 10, true);
                this.riGrid.AddColumn('BranchServiceAreaDesc', 'StateOfService', 'BranchServiceAreaDesc', MntConst.eTypeText, 14);
                this.riGrid.AddColumn('EmployeeSurname', 'StateOfService', 'EmployeeSurname', MntConst.eTypeText, 15);

                this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);
                this.riGrid.AddColumnAlign('BranchServiceAreaDesc', MntConst.eAlignmentLeft);
                this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);
                break;
            case 'Region':
                if (this.getControlValue('SelectLevel') === 'ServiceArea') {
                    this.riGrid.AddColumn('BranchServiceAreaCode', 'StateOfService', 'BranchServiceAreaCode', MntConst.eTypeCode, 10, true);
                    this.riGrid.AddColumn('BranchServiceAreaDesc', 'StateOfService', 'BranchServiceAreaDesc', MntConst.eTypeText, 14);
                    this.riGrid.AddColumn('EmployeeSurname', 'StateOfService', 'EmployeeSurname', MntConst.eTypeText, 15);
                    this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);
                    this.riGrid.AddColumnAlign('BranchServiceAreaDesc', MntConst.eAlignmentLeft);
                    this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);
                    this.riGrid.AddColumnOrderable('BranchServiceAreaCode', true);
                } else {
                    this.riGrid.AddColumn('BranchNumber', 'StateOfService', 'BranchNumber', MntConst.eTypeText, 6, true);
                    this.riGrid.AddColumn('BranchName', 'StateOfService', 'BranchName', MntConst.eTypeText, 14);
                    this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
                    this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentLeft);
                    this.riGrid.AddColumnOrderable('BranchNumber', true);
                }
                break;
        }

        /**
         * This Will Only Be Called From Here If Page Is Not Returning
         * Used Three Different Methods For Better Maintenance Purpose
         */
        this['buildGrid' + this.getControlValue('ReportView')]();

        this.pageParams.headerClickedColumn = 'BranchNumber';
        switch (this.pageType) {
            case 'Business':
                if (viewBy === 'Region') {
                    this.riGrid.AddColumnOrderable('RegionCode', true);
                    this.pageParams.headerClickedColumn = 'RegionCode';
                } else {
                    this.riGrid.AddColumnOrderable('BranchNumber', true);
                    this.pageParams.headerClickedColumn = 'BranchNumber';
                }
                break;
            case 'Branch':
                this.riGrid.AddColumnOrderable('BranchServiceAreaCode', true);
                this.pageParams.headerClickedColumn = 'BranchServiceAreaCode';
                if (rawGroupBy === 'Town') {
                    this.riGrid.AddColumnOrderable('GroupBy', true);
                    this.pageParams.headerClickedColumn = 'GroupBy';
                }
                break;
        }


        this.riGrid.Complete();
    }

    /**
     * Different Build Grid Methods For Different Report View
     * Might Show As Ununsed Because Of How It Is Called
     */
    private buildGridValues(): void {
        let showType: string = this.getControlValue('ShowType');

        this.riGrid.AddColumn('VisitsDueNumber', 'StateOfService', 'VisitsDueNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDueValue', 'StateOfService', 'VisitsDueValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumn('VisitsAheadNumber', 'StateOfService', 'VisitsAheadNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsAheadValue', 'StateOfService', 'VisitsAheadValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumn('VisitsBehindNumber', 'StateOfService', 'VisitsBehindNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsBehindValue', 'StateOfService', 'VisitsBehindValue', MntConst.eTypeCurrency, 10);

        if (showType === 'Contract') {
            this.riGrid.AddColumn('SuspendedVisitsNumber', 'StateOfService', 'SuspendedVisitsNumber', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('SuspendedVisitsValue', 'StateOfService', 'SuspendedVisitsValue', MntConst.eTypeCurrency, 10);
        }

        this.riGrid.AddColumn('VisitsDoneNumber', 'StateOfService', 'VisitsDoneNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDoneValue', 'StateOfService', 'VisitsDoneValue', MntConst.eTypeCurrency, 10);

        if (showType === 'Contract') {
            this.riGrid.AddColumn('SuspendedVisitsCreditNumber', 'StateOfService', 'SuspendedVisitsCreditNumber', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('SuspendedVisitsCreditValue', 'StateOfService', 'SuspendedVisitsCreditValue', MntConst.eTypeCurrency, 10);
        }

        this.riGrid.AddColumn('PercentageBehindNumber', 'StateOfService', 'PercentageBehindNumber', MntConst.eTypeDecimal2, 5);
        this.riGrid.AddColumn('PercentageBehindValue', 'StateOfService', 'PercentageBehindValue', MntConst.eTypeDecimal2, 5);
        this.riGrid.AddColumn('CreditVisitsNumber', 'StateOfService', 'CreditVisitsNumber', MntConst.eTypeInteger, 5);

        this.riGrid.AddColumnAlign('VisitsDueNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsDueValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsAheadNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsAheadValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsBehindNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsBehindValue', MntConst.eAlignmentRight);

        if (showType === 'Contract') {
            this.riGrid.AddColumnAlign('SuspendedVisitsNumber', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('SuspendedVisitsValue', MntConst.eAlignmentRight);
        }

        this.riGrid.AddColumnAlign('VisitsDoneNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsDoneValue', MntConst.eAlignmentRight);

        if (showType === 'Contract') {
            this.riGrid.AddColumnAlign('SuspendedVisitsCreditNumber', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('SuspendedVisitsCreditValue', MntConst.eAlignmentRight);
        }

        this.riGrid.AddColumnAlign('PercentageBehindNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('PercentageBehindValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('CreditVisitsNumber', MntConst.eAlignmentRight);
    }

    private buildGridExchanges(): void {
        this.riGrid.AddColumn('VisitsDueNumber', 'StateOfService', 'VisitsDueNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDoneNumber', 'StateOfService', 'VisitsDoneNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDifference', 'StateOfService', 'VisitsDifference', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('ExchangesDue', 'StateOfService', 'ExchangesDue', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ExchangesAheadNumber', 'StateOfService', 'ExchangesAheadNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ExchangesAheadValue', 'StateOfService', 'ExchangesAheadValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumn('ExchangesBehindNumber', 'StateOfService', 'ExchangesAheadNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ExchangesBehindValue', 'StateOfService', 'ExchangesAheadValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumn('SuspendedExchangesNumber', 'StateOfService', 'SuspendedExchangesNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('SuspendedExchangesValue', 'StateOfService', 'SuspendedExchangesValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumn('ExchangesDone', 'StateOfService', 'ExchangesDone', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ExchangesMissedNumber', 'StateOfService', 'ExchangesMissedNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('SuspendedExchangesCreditNumber', 'StateOfService', 'SuspendedExchangesCreditNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('SuspendedExchangesCreditValue', 'StateOfService', 'SuspendedExchangesCreditValue', MntConst.eTypeCurrency, 10);

        this.riGrid.AddColumnAlign('VisitsDueNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsDoneNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsDifference', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('ExchangesMissedNumber', MntConst.eAlignmentRight);
    }

    private buildGridTimes(): void {
        this.riGrid.AddColumn('VisitsDueNumber', 'StateOfService', 'VisitsDueNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDoneNumber', 'StateOfService', 'VisitsDoneNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDifference', 'StateOfService', 'VisitsDifference', MntConst.eTypeInteger, 5);

        this.riGrid.AddColumn('VisitTimesDue', 'StateOfService', 'VisitTimesDue', MntConst.eTypeTextFree, 5);
        this.riGrid.AddColumn('VisitTimesDone', 'StateOfService', 'VisitTimesDone', MntConst.eTypeTextFree, 5);
        this.riGrid.AddColumn('VisitTimesDifference', 'StateOfService', 'VisitTimesDifference', MntConst.eTypeTextFree, 5);

        this.riGrid.AddColumnAlign('VisitsDueNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsDoneNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsDifference', MntConst.eAlignmentRight);
    }
    // #endregion

    private populateGrid(): void {
        let gridQuery: QueryParams = this.getURLSearchParamObject();

        gridQuery.set(this.serviceConstants.Action, '2');

        gridQuery.set(this.serviceConstants.Level, this.pageType);
        gridQuery.set('ReportNumber', this.getControlValue('ReportNumber'));
        gridQuery.set('ROWID', this.getControlValue('ROWID'));
        if (this.pageType === 'Branch') {
            let branch: string = this.getControlValue('BranchNumber');
            if (!branch) {
                this.pageParams.headerClickedColumn = '';
            }
            gridQuery.set(this.serviceConstants.BranchNumber, this.getControlValue('BranchNumber'));
        } else if (this.pageType !== 'Region') {
            gridQuery.set('ViewBy', this.getControlValue('ViewBy'));
        }
        gridQuery.set('CompanyCode', this.getControlValue('CompanyCode'));
        gridQuery.set('ShowType', this.getControlValue('ShowType'));
        gridQuery.set('ShowPortfolioType', this.getControlValue('ShowPortfolioType'));
        gridQuery.set('ContractNumber', this.getControlValue('ContractNumber'));
        gridQuery.set('SeasonalSelect', this.getControlValue('SeasonalSelect'));
        gridQuery.set('ReportGroupCode', this.getControlValue('ReportGroupCode'));
        gridQuery.set('ReportView', this.getControlValue('ReportView'));
        if (this.pageType === 'Region') {
            gridQuery.set('RegionCode', this.getControlValue('RegionCode'));
            gridQuery.set('SelectType', this.getControlValue('SelectLevel'));
        }

        gridQuery.set(this.serviceConstants.GridMode, '0');
        gridQuery.set(this.serviceConstants.GridHandle, this.pageParams.gridHandle);
        gridQuery.set(this.serviceConstants.GridCacheRefresh, this.pageParams.gridCacheRefresh);
        gridQuery.set(this.serviceConstants.PageSize, 10);
        gridQuery.set(this.serviceConstants.PageCurrent, this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1);
        gridQuery.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn || this.pageParams.headerClickedColumn);
        gridQuery.set('riSortOrder', this.riGrid.SortOrder);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrGet(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation[this.pageType], gridQuery).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                }, 100);
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    private getGroupBy(): void {
        let query: QueryParams = this.getURLSearchParamObject();

        query.set(this.serviceConstants.Action, '6');
        query.set(this.serviceConstants.Function, 'GetGroupBy');
        query.set('Rowid', this.getControlValue('ROWID'));

        this.httpService.xhrGet(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation[this.pageType], query).then(data => {
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                this.setControlValue('GroupBy', data['GroupBy']);
                this.setControlValue('RawGroupBy', data['RawGroupBy']);
                this.buildGrid();
                this.populateGrid();
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }
    // #endregion

    // #region Public Methods
    public onRefreshClick(): void {
        this.pageParams.gridCacheRefresh = true;
        this.onRiGridRefresh();
    }

    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridCacheRefresh = false;
            super.getCurrentPage(currentPage);
        }
    }

    public onGridBodyDoubleClick(_event?: any): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'BranchNumber':
                let branch: string = this.riGrid.Details.GetRowId('BranchNumber');
                let name: string = this.riGrid.Details.GetValue('BranchName');
                if (branch === 'TOTAL') {
                    branch = '';
                    name = '';
                }
                this.navigate('StateOfServiceBusiness', BIReportsRoutes.ICABSSEBATCHSTATEOFSERVICEBRANCHGRID, {
                    type: 'Branch',
                    BranchNumber: branch,
                    BranchName: name
                });
                break;
            case 'BranchServiceAreaCode':
            case 'GroupBy':
                if (this.riGrid.CurrentColumnName === 'GroupBy' && this.riGrid.Details.GetRowId('GroupBy') !== 'TOTAL') {
                    return;
                }

                let serviceArea: string = this.riGrid.Details.GetAttribute('BranchServiceAreaCode', 'rowid');
                let params: any = {};
                params['BranchServiceAreaCode'] = serviceArea === 'TOTAL' ? '' : serviceArea;
                params['BranchNumber'] = this.riGrid.Details.GetAttribute('BranchServiceAreaCode', 'additionalproperty') || '';
                params['BranchName'] = this.riGrid.Details.GetAttribute('BranchServiceAreaDesc', 'additionalproperty') || '';
                params['EmployeeSurname'] = this.riGrid.Details.GetValue('EmployeeSurname');
                params['ViewTotal'] = serviceArea === 'TOTAL' || !serviceArea ? 'yes' : 'no';
                this.navigate('BatchStateOfServiceBranch', BIReportsRoutes.ICABSSESTATEOFSERVICEGRID, params);
                break;
            /**
             * Place Holder For Other Cases:
             * For Business:
             *   - Region
             * Needs To Be Modified As The Pages Are Developed
             */
        }
    }

    public onHeaderClick(): void {
        this.pageParams.gridCacheRefresh = false;
        this.onRiGridRefresh();
    }

    public resetGrid(): void {
        this.gridConfig.totalItem = 0;
        this.pageParams.gridCurrentPage = 1;
        this.riGrid.Clear();
        this.buildGrid();
        this.riGrid.RefreshRequired();
    }
    // #endregion
}
