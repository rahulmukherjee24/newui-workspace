import { Component, OnInit, Injector, ViewChild, AfterContentInit } from '@angular/core';

import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from './../../base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { BranchServiceAreaSearchComponent } from '@app/internal/search/iCABSBBranchServiceAreaSearch';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { ProductServiceGroupSearchComponent } from '@app/internal/search/iCABSBProductServiceGroupSearch.component';
import { MessageConstant } from '@shared/constants/message.constant';
import { InternalGridSearchServiceModuleRoutes } from '@app/base/PageRoutes';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { DropdownConstants } from '@app/base/ComponentConstants';

@Component({
    templateUrl: 'iCABSSeStaticVisitGridYearBranch.html'
})

export class StaticVisitGridYearBranchComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('productservice') ddProductServiceGroupSearch: ProductServiceGroupSearchComponent;
    public pageId: string = '';
    public controls: IControls[] = [
        { name: 'BranchNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'ViewTypeFilter', value: 'Premise' },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractTypeFilter', type: MntConst.eTypeCode },
        { name: 'PostalBrickString', type: MntConst.eTypeText },
        { name: 'PremiseTown', type: MntConst.eTypeText },
        { name: 'ProductServiceGroupString', type: MntConst.eTypeText },
        { name: 'VisitFrequencyFilter', type: MntConst.eTypeText },
        { name: 'SelectedYear', type: MntConst.eTypeText },
        { name: 'TotalNettValue', disabled: true, type: MntConst.eTypeCurrency },
        { name: 'TotalTime', disabled: true, type: MntConst.eTypeText },
        { name: 'TotalWED', disabled: true, type: MntConst.eTypeDecimal1 },
        { name: 'TotalUnits', disabled: true, type: MntConst.eTypeInteger },
        { name: 'TotalPremises', disabled: true, type: MntConst.eTypeInteger }
    ];

    public productServiceGroupDropdownConfig: Record<string, any> = {
        params: {
            parentMode: 'LookUp-String',
            ProductServiceGroupString: '',
            SearchValue3: '',
            ProductServiceGroupCode: ''
        },
        active: { id: '', text: '' },
        isDisabled: false,
        isRequired: false,
        isMultiSelect: true
    };

    public commonGridFunction: CommonGridFunction;
    public selectedYearOptions: Array<any> = [];

    public serviceAreaEllipsis: Record<string, any> = {
        childConfigParams: {
            'parentMode': 'LookUp-Emp'
        },
        isDisabled: false,
        component: BranchServiceAreaSearchComponent
    };

    public legendArr: Array<any> = [];
    public productSearchGroupDesc: string;
    public showLegend: boolean = false;
    public showProductService: boolean = false;
    public showDropdown: boolean;
    public filter: Record<string, boolean> = {
        isPostcodeDisplayed: false,
        isTownDisplayed: false
    };

    constructor(injector: Injector, public sysCharConstants: SysCharConstants) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSSESTATICVISITGRIDYEARBRANCH;
        this.browserTitle = this.pageTitle = 'Branch Static Visits';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.loadSystemCharacters();
        this.pageParams.hasGridData = false;
        this.pageParams.gridConfig = {
            totalItem: 1
        };
        this.pageParams.isMandatory = true;
        this.showProductService = false;
        this.showDropdown = true;
    }

    ngAfterContentInit(): void {
        this.buildYearOptions();
        super.ngAfterContentInit();
        this.windowOnLoad();
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchTextOnly());
    }

    private windowOnLoad(): void {
        if (this.isReturning()) {
            if (this.parentMode === 'SummaryWorkload') {
                this.showProductService = true;
                this.showDropdown = false;
                this.disableControl('BranchServiceAreaCode', true);
                this.disableControl('ProductServiceGroupString', true);
                this.disableControl('VisitFrequencyFilter', true);
                this.serviceAreaEllipsis.isDisabled = true;
            } else if (this.parentMode === 'BranchServiceArea') {
                this.showProductService = false;
                this.showDropdown = true;
                this.disableControl('BranchServiceAreaCode', true);
                this.serviceAreaEllipsis.isDisabled = true;
                this.productServiceGroupDropdownConfig.active = {
                    id: '',
                    text: '',
                    [DropdownConstants.c_s_KEY_MULTISELECTDISPLAY]: this.getControlValue('ProductServiceGroupString')
                };
            } else {
                if (this.pageParams['ProductServiceGroupSearch']) {
                    this.productServiceGroupDropdownConfig.active = {
                        id: '',
                        text: '',
                        [DropdownConstants.c_s_KEY_MULTISELECTDISPLAY]: this.getControlValue('ProductServiceGroupString')
                    };
                }
            }
            this.onRiGridRefresh();
        } else {
            switch (this.parentMode) {
                case 'BranchServiceArea':
                    this.showProductService = false;
                    this.showDropdown = true;
                    this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
                    this.riExchange.getParentHTMLValue('EmployeeSurname');
                    this.disableControl('BranchServiceAreaCode', true);
                    this.serviceAreaEllipsis.isDisabled = true;
                    if (this.riExchange.getParentHTMLValue('EmployeeSurname') === '') {
                        this.branchServiceAreaCodeOnchange();
                    }
                    this.buildGrid();
                    this.riGridBeforeExecute();
                    break;
                case 'SummaryWorkload':
                    this.showProductService = true;
                    this.showDropdown = false;
                    this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
                    this.riExchange.getParentHTMLValue('EmployeeSurname');
                    this.riExchange.getParentHTMLValue('ViewTypeFilter');
                    this.riExchange.getParentHTMLValue('ContractTypeFilter');
                    this.riExchange.getParentHTMLValue('ProductServiceGroupString');
                    this.riExchange.getParentHTMLValue('VisitFrequencyFilter');
                    this.riExchange.getParentHTMLValue('SelectedYear');
                    this.disableControl('BranchServiceAreaCode', true);
                    this.disableControl('ProductServiceGroupString', true);
                    this.disableControl('VisitFrequencyFilter', true);
                    this.serviceAreaEllipsis.isDisabled = true;
                    this.buildGrid();
                    this.riGridBeforeExecute();
                    break;
            }
        }
    }

    private loadSystemCharacters(): void {
        const syscharList: Array<number> = [
            this.sysCharConstants.SystemCharEnableAUPostcodeValidation,
            this.sysCharConstants.SystemCharEnableWED,
            this.sysCharConstants.SystemCharEnableInstallsRemovals

        ];
        const query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                if (data.records[0] && data.records.length > 0) {
                    this.pageParams.vEnableAUPostcodeValidation = data.records[0].Required;
                    this.filter.isPostcodeDisplayed = data.records[0].Required;
                    this.filter.isTownDisplayed = data.records[0].Required;
                }
                if (data.records[1] && data.records.length > 0) {
                    this.pageParams.vEnableWED = data.records[1].Required;
                }
                if (data.records[2] && data.records.length > 0) {
                    this.pageParams.vEnableInstallsRemovals = data.records[2].Required;
                    this.setControlValue('ViewTypeFilter', data.records[2].Required ? 'Unit' : 'Premise');
                }
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });

    }

    private buildYearOptions(): void {
        this.selectedYearOptions = [];
        const date: Date = new Date();
        for (let i = 1; i <= 7; i++) {
            const val: number = date.getFullYear() - 5 + i;
            this.selectedYearOptions.push({ value: val, text: val });
        }
        this.setControlValue('SelectedYear', date.getFullYear());
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('Month', 'StaticVisit', 'Month', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('Month', MntConst.eAlignmentCenter);

        for (let iLoop = 1; iLoop <= 31; iLoop++) {
            this.riGrid.AddColumn(iLoop.toString(), 'StaticVisit', iLoop.toString(), MntConst.eTypeText, 1);
            if (this.getControlValue('ViewTypeFilter') === 'Time' || this.getControlValue('ViewTypeFilter') === 'Value') {
                this.riGrid.AddColumnAlign(iLoop.toString(), MntConst.eAlignmentRight);
            } else {
                this.riGrid.AddColumnAlign(iLoop.toString(), MntConst.eAlignmentCenter);
            }
        }

        if (this.getControlValue('ViewTypeFilter') !== 'Time' && this.getControlValue('ViewTypeFilter') !== 'Value') {
            this.riGrid.AddColumn('Total', 'StaticVisit', 'Total', MntConst.eTypeText, 20);
            this.riGrid.AddColumnAlign('Total', MntConst.eAlignmentCenter);
        }
        if (this.getControlValue('ViewTypeFilter') === 'Unit') {
            this.riGrid.AddColumn('YTDTotal', 'StaticVisit', 'YTDTotal', MntConst.eTypeInteger, 20);
            this.riGrid.AddColumnAlign('YTDTotal', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('TotalMonthTimeValue', 'StaticVisit', 'TotalMonthTimeValue', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('TotalMonthTimeValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('TotalMonthNettValue', 'StaticVisit', 'TotalMonthNettValue', MntConst.eTypeCurrency, 20);
        this.riGrid.AddColumnAlign('TotalMonthNettValue', MntConst.eAlignmentRight);

        if (this.getControlValue('ViewTypeFilter') === 'Value') {
            this.riGrid.AddColumn('YTDNetValue', 'StaticVisit', 'YTDNetValue', MntConst.eTypeCurrency, 20);
            this.riGrid.AddColumnAlign('YTDNetValue', MntConst.eAlignmentCenter);
        }

        this.riGrid.Complete();
    }

    private riGridBeforeExecute(): any {
        const formData: Record<string, string | boolean | number> = {};
        const search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');

        formData['Level'] = 'Branch';
        formData['Year'] = this.getControlValue('SelectedYear');
        formData['BusinessCode'] = this.utils.getBusinessCode();
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['ViewTypeFilter'] = this.getControlValue('ViewTypeFilter');
        formData['ProductServiceGroupString'] = this.getControlValue('ProductServiceGroupString') || '';
        formData['Postcode'] = this.getControlValue('PostalBrickString');
        formData['Town'] = this.getControlValue('PremiseTown');
        formData['ContractType'] = this.getControlValue('ContractTypeFilter');
        formData['VisitFrequency'] = this.getControlValue('VisitFrequencyFilter');
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        formData[this.serviceConstants.GridCacheRefresh] = true;
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('service-planning/maintenance', 'plan-visits', 'Application/iCABSAStaticVisitGridYear', search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.commonGridFunction.setPageData(data);
            this.showLegend = true;
            this.getLegendHtml();
        }).catch(error => {
            this.pageParams.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        if (this.getControlValue('BranchServiceAreaCode') && this.getControlValue('PostalBrickString')) {
            this.displayMessage('You can choose by either Service Area or Service Postcode not both', CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
            this.uiForm.controls['BranchServiceAreaCode'].setErrors({ 'incorrect': true });
            this.uiForm.controls['PostalBrickString'].setErrors({ 'incorrect': true });
            this.riExchange.validateForm(this.uiForm);
        } else if (this.getControlValue('BranchServiceAreaCode') && this.getControlValue('PremiseTown')) {
            this.displayMessage('You can choose by either Service Area or Service Suburb not both', CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
            this.uiForm.controls['BranchServiceAreaCode'].setErrors({ 'incorrect': true });
            this.uiForm.controls['PremiseTown'].setErrors({ 'incorrect': true });
            this.riExchange.validateForm(this.uiForm);
        } else if (this.getControlValue('PostalBrickString') && this.getControlValue('PremiseTown')) {
            this.displayMessage('You can choose by either Service Postcode or Service Suburb not both', CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
            this.uiForm.controls['PostalBrickString'].setErrors({ 'incorrect': true });
            this.uiForm.controls['PremiseTown'].setErrors({ 'incorrect': true });
            this.riExchange.validateForm(this.uiForm);
        } else {
            this.uiForm.controls['BranchServiceAreaCode'].setErrors(null);
            this.uiForm.controls['PostalBrickString'].setErrors(null);
            this.uiForm.controls['PremiseTown'].setErrors(null);
            this.buildGrid();
            this.riGridBeforeExecute();
        }
    }

    public clearGrid(): void {
        this.commonGridFunction.resetGrid();
        this.showLegend = false;
    }

    private getGridDay(): number {
        const val: any = parseInt(this.riGrid.CurrentColumnName, 0);
        return this.utils.round(val, 0);
    }

    public riGridBodyOnDblClick(event: any): void {
        let CurrentContractTypeURLParameter;
        switch (this.getControlValue('ContractTypeFilter')) {
            case '':
                CurrentContractTypeURLParameter = '';
                break;
            case 'C':
                CurrentContractTypeURLParameter = '<contract>';
                break;
            case 'J':
                CurrentContractTypeURLParameter = '<job>';
                break;
            case 'P':
                CurrentContractTypeURLParameter = '<product>';
                break;
        }
        const currentColumn = this.riGrid.CurrentColumnName;

        const additionData = this.riGrid.Details.GetAttribute(currentColumn, 'additionalproperty').split(',');
        const arrSelection = this.riGrid.Details.GetAttribute('Month', 'RowID').split(' ');
        const vbMonth = arrSelection[0];
        const vbYear = arrSelection[1];

        switch (additionData[0]) {
            case 'Day':
                if (this.parentMode === 'SummaryWorkload') {
                    if (this.riExchange.URLParameterContains('reroute')) {
                        this.setAttribute('BusinessCodeSelectedServiceArea', this.getControlValue('BranchServiceAreaCode'));
                        this.setAttribute('EmployeeSurname', this.getControlValue('EmployeeSurname'));
                        this.setAttribute('BusinessCodeSelectedDate', this.utils.formatDate(new Date(this.getControlValue('SelectedYear'), vbMonth - 1, this.getGridDay())));
                        this.navigate('SummaryWorkload', InternalGridSearchServiceModuleRoutes.ICABSSESUMMARYWORKLOADREZONEMOVEGRID.URL_2,
                            { 'reroute': true, 'productSearchGroupDesc': this.productSearchGroupDesc });
                    }
                    else {
                        this.navigate('SummaryWorkload', InternalGridSearchServiceModuleRoutes.ICABSSESUMMARYWORKLOADREZONEMOVEGRID.URL_1,
                            { 'productSearchGroupDesc': this.productSearchGroupDesc });
                    }
                }
                else {
                    this.navigate('Year', InternalGridSearchServiceModuleRoutes.ICABSSESTATICVISITGRIDDAYBRANCH,
                        {
                            'DateType': 'Day',
                            'SelectedDate': this.utils.formatDate(new Date(this.getControlValue('SelectedYear'), vbMonth - 1, this.getGridDay())),
                            'ContractTypeFilter': this.getControlValue('ContractTypeFilter'),
                            'Postcode': this.getControlValue('PostalBrickString'),
                            'Town': this.getControlValue('PremiseTown'),
                            'VisitFrequency': this.getControlValue('VisitFrequencyFilter'),
                            'CurrentContractTypeURLParameter': CurrentContractTypeURLParameter
                        }
                    );
                }
                break;

            case 'Month':
            case 'YTDTotal':
            case 'NetValue':
            case 'YTDNetValue':
                this.navigate('Year', InternalGridSearchServiceModuleRoutes.ICABSSESTATICVISITGRIDDAYBRANCH,
                    {
                        'DateType': 'Month',
                        'SelectedMonth': vbMonth,
                        'SelectedYear': vbYear,
                        'ContractTypeFilter': this.getControlValue('ContractTypeFilter'),
                        'Postcode': this.getControlValue('PostalBrickString'),
                        'Town': this.getControlValue('PremiseTown'),
                        'VisitFrequency': this.getControlValue('VisitFrequencyFilter'),
                        'CurrentContractTypeURLParameter': CurrentContractTypeURLParameter
                    }
                );
                break;
            case 'None':
                this.displayMessage('No data to show', CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                break;

        }
    }

    public branchServiceAreaCodeOnchange(): void {
        this.setControlValue('EmployeeSurname', '');
        const search: QueryParams = this.getURLSearchParamObject(), postData: Object = {};
        postData[this.serviceConstants.Function] = 'SetEmployeeSurname';
        search.set(this.serviceConstants.Action, '6');
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest('service-planning/grid', 'structure', 'Service/iCABSSeServiceAreaDetailGrid', search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.displayMessage(MessageConstant.Message.RecordNotFound);
                    this.setControlValue('BranchServiceAreaCode', '');
                    this.setControlValue('EmployeeSurname', '');
                } else {
                    this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                }
                if (this.parentMode !== 'BranchServiceArea') {
                    this.clearGrid();
                }
            },
            (error) => {
                this.displayMessage(error);
            }
        );
    }

    public riGridAfterExecute(): void {
        let totalString: Array<any> = [];
        this.setControlValue('TotalWED', '');
        this.setControlValue('TotalUnits', '');
        this.setControlValue('TotalTime', '');
        this.setControlValue('TotalNettValue', '');

        if (this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.children.length > 0) {
            let addValue: string = this.riGrid.HTMLGridBody.children[1].children[0].children[0].getAttribute('AdditionalProperty');
            if (addValue) {
                totalString = addValue.split('|');
                this.setControlValue('TotalPremises', totalString[0]);
                this.setControlValue('TotalUnits', totalString[1]);
                this.setControlValue('TotalTime', totalString[2]);
                this.setControlValue('TotalNettValue', totalString[3]);
                if (this.pageParams.vEnableWED) {
                    this.setControlValue('TotalWED', totalString[4]);
                }
                this.riExchange.riInputElement.isError(this.uiForm, 'TotalPremises');
                this.riExchange.riInputElement.isError(this.uiForm, 'TotalUnits');
                this.riExchange.riInputElement.isError(this.uiForm, 'TotalNettValue');
                if (this.pageParams.vEnableWED) {
                    this.riExchange.riInputElement.isError(this.uiForm, 'TotalWED');
                }
            }
        }
    }

    public onDataRcv(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
        this.clearGrid();
    }

    public onProductServiceGroupSearch(data: any): void {
        this.setControlValue('ProductServiceGroupString', data[DropdownConstants.c_s_KEY_MULTISELECTVALUE]);
        this.setControlValue('ProductServiceGroupDesc', data.ProductServiceGroupDesc);
        this.pageParams['ProductServiceGroupSearch'] = data;
        this.clearGrid();
    }

    private getLegendHtml(): any {
        const postData = {};
        const search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        postData[this.serviceConstants.Function] = 'GetLegend';
        postData['BusinessCode'] = this.utils.getBusinessCode();

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest('service-planning/grid', 'areas', 'Service/iCABSSESummaryWorkloadGridMonthBranch', search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.displayMessage(data.fullError);
                }
                else {
                    this.zone.run(() => {
                        const res: any = data['HTMLLegend'];
                        this.legendArr = [];
                        if (res) {
                            let legendArr = res.split('bgcolor=');
                            for (let i = 0; i < legendArr.length; i++) {
                                let strData = legendArr[i];
                                if (strData.indexOf('"#') >= 0) {
                                    this.legendArr.push({
                                        color: strData.substr(strData.indexOf('"#') + 1, 7),
                                        label: strData.substr(strData.indexOf('<td>') + 4, strData.substr(strData.indexOf('<td>') + 4).indexOf('&'))
                                    });
                                }
                            }
                        }
                    });
                }
            },
            (error) => {
                this.displayMessage(error);
            });
    }
}
