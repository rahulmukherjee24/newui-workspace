
import { Component, OnInit, OnDestroy, Injector, ViewChild, AfterContentInit } from '@angular/core';

import { BranchSearchComponent } from '@internal/search/iCABSBBranchSearch';
import { BranchServiceAreaSearchComponent } from '@internal/search/iCABSBBranchServiceAreaSearch';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { ContractManagementModuleRoutes, InternalMaintenanceServiceModuleRoutes } from '@base/PageRoutes';
import { ContractSearchComponent } from '@app/internal/search/iCABSAContractSearch';
import { DropdownConstants } from './../../base/ComponentConstants';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { PremiseSearchComponent } from '@app/internal/search/iCABSAPremiseSearch';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ServiceCoverSearchComponent } from '@app/internal/search/iCABSAServiceCoverSearch';
import { ServiceTypeSearchComponent } from '@app/internal/search/iCABSBServiceTypeSearch.component';
import { VisitTypeSearchComponent } from '@app/internal/search/iCABSBVisitTypeSearch.component';

@Component({
    templateUrl: 'iCABSTechnicianServiceVisitGrid.html',
    providers: [CommonLookUpUtilsService]
})


export class TechnicianServiceVisitGridComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('serviceTypeSearch') serviceTypeSearch: ServiceTypeSearchComponent;
    @ViewChild('visitTypeSearch') visitTypeSearch: VisitTypeSearchComponent;

    public controls = [
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'BranchNumber', type: MntConst.eTypeInteger },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractNumber', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'NumOfVisits', type: MntConst.eTypeInteger },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceTypeCode', type: MntConst.eTypeCode },
        { name: 'ServiceTypeDesc', type: MntConst.eTypeText },
        { name: 'VisitTypeCode', type: MntConst.eTypeCode },
        { name: 'VisitTypeDesc', type: MntConst.eTypeText }
    ];

    private headerParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Service/iCABSTechnicianServiceVisitGrid'
    };

    public alertMessage: any;
    public hasGridData: boolean = false;
    public messageType: string;
    public pageId: string;
    public currentBranchCode: string = this.utils.getBranchCode();

    //Dropdown Config
    public dropdown: any = {
        branchParams: {
            'parentMode': 'LookUp-UserAuthorityBranch'
        },
        serviceTypeSearch: {
            params: {
                parentMode: 'LookUp'
            },
            active: { id: '', text: '' }
        },
        visitTypeSearch: {
            params: {
                parentMode: 'LookUp-String'
            },
            active: { id: '', text: '' }
        }
    };

    //Ellipsis Config
    public ellipsis = {
        serviceAreaEllipsis: {
            childparams: {
                parentMode: 'LookUp-ServiceEmp',
                'BranchNumberServiceBranchNumber': this.currentBranchCode,
                'ServiceBranchNumber': this.currentBranchCode,
                'BranchName': this.utils.getBranchTextOnly(this.currentBranchCode)
            },
            component: BranchServiceAreaSearchComponent
        },
        employee: {
            childConfigParams: {
                'parentMode': 'LookUp-Branch-Occupation',
                'OccupationCode': '06'
            },
            component: EmployeeSearchComponent
        },
        contractSearch: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: ContractSearchComponent
        },
        premiseSearch: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'ContractNumber': '',
                'ContractName': ''
            },
            component: PremiseSearchComponent
        },
        productSearch: {
            childConfigParams: {
                'ContractNumber': '',
                'ContractName': '',
                'PremiseName': '',
                'PremiseNumber': '',
                'parentMode': 'LookUp-Freq'
            },
            component: ServiceCoverSearchComponent
        }
    };

    constructor(injector: Injector, private lookupUtils: CommonLookUpUtilsService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSTECHNICIANSERVICEVISITGRID;
        this.browserTitle = this.pageTitle = 'Technician Service Visits';
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.riGrid.DefaultBorderColor = 'ADD8E6';
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.buildGrid();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (!this.isReturning()) {

            this.pageParams.gridConfig = {
                pageSize: 10,
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString(),
                gridCacheRefresh: true
            };
            this.pageParams.gridCurrentPage = 1;

            let date: Date = new Date();
            this.setControlValue('DateFrom', this.utils.formatDate(new Date(date.getFullYear(), date.getMonth(), 1)));
            this.setControlValue('DateTo', this.utils.formatDate(date));

            this.branchSearchDropDown.active = {
                id: this.currentBranchCode, text: this.utils.getBranchText()
            };
            this.setControlValue('BranchNumber', this.currentBranchCode);
            this.setControlValue('BranchName', this.utils.getBranchTextOnly(this.currentBranchCode));
        }
        else {
            if (this.getControlValue('BranchNumber')) {
                let branchNumber = this.getControlValue('BranchNumber');
                let branchName = this.getControlValue('BranchName');
                this.branchSearchDropDown.active = {
                    id: branchNumber, text: branchNumber + ' - ' + branchName
                };
            }
            if (this.getControlValue('VisitTypeCode')) {
                let visitTypeCode = this.getControlValue('VisitTypeCode');
                this.dropdown.visitTypeSearch.active = {
                    id: '', text: '', multiSelectDisplay: visitTypeCode
                };
            }

            if (this.getControlValue('ServiceTypeCode')) {
                this.serviceTypeSearch.setValue(this.getControlValue('ServiceTypeCode'));
            }
            this.pageParams.gridConfig.gridCacheRefresh = false;
            this.populateGrid();
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('NegBranchNumber', 'TechnicianServiceVisit', 'NegBranchNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumn('VisitDate', 'TechnicianServiceVisit', 'VisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('DueDate', 'TechnicianServiceVisit', 'DueDate', MntConst.eTypeDate, 16);
        this.riGrid.AddColumn('Duration', 'TechnicianServiceVisit', 'Duration', MntConst.eTypeTime, 6);
        this.riGrid.AddColumn('VisitTypeCode', 'TechnicianServiceVisit', 'VisitTypeCode', MntConst.eTypeCode, 4);
        this.riGrid.AddColumn('EmployeeCode', 'TechnicianServiceVisit', 'EmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('EmployeeSurname', 'TechnicianServiceVisit', 'EmployeeSurname', MntConst.eTypeText, 15);
        this.riGrid.AddColumn('ContractNumber', 'TechnicianServiceVisit', 'ContractNumber', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('PremiseNumber', 'TechnicianServiceVisit', 'PremiseNumber', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('PremiseName', 'TechnicianServiceVisit', 'PremiseName', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('PremiseAddress', 'TechnicianServiceVisit', 'PremiseAddress', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('PostCode', 'TechnicianServiceVisit', 'PostCode', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('ServicFreq', 'TechnicianServiceVisit', 'ServicFreq', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumn('ProductCode', 'TechnicianServiceVisit', 'ProductCode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumn('ServiceQuantity', 'TechnicianServiceVisit', 'ServiceQuantity', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumn('ContractTypeCode', 'TechnicianServiceVisit', 'ContractTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumn('ActualPlannedTime', 'TechnicianServiceVisit', 'ActualPlannedTime', MntConst.eTypeText, 6);
        this.riGrid.AddColumn('StaticVisitDate', 'TechnicianServiceVisit', 'StaticVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('VisitsDueAndCompleted', 'TechnicianServiceVisit', 'VisitsDueAndCompleted', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('NegBranchNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Duration', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('PremiseAddress', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('PostCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServicFreq', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ContractTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ActualPlannedTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('StaticVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitsDueAndCompleted', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnOrderable('NegBranchNumber', true);
        this.riGrid.AddColumnOrderable('VisitDate', true);
        this.riGrid.AddColumnOrderable('VisitTypeCode', true);
        this.riGrid.AddColumnOrderable('EmployeeCode', true);
        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('PremiseName', true);
        this.riGrid.AddColumnOrderable('PostCode', true);
        this.riGrid.AddColumnOrderable('ProductCode', true);
        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        let formData: any = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        formData['DateFrom'] = this.getControlValue('DateFrom');
        formData['DateTo'] = this.getControlValue('DateTo');
        formData[this.serviceConstants.EmployeeCode] = this.getControlValue('EmployeeCode');
        formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        formData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        formData[this.serviceConstants.ProductCode] = this.getControlValue('ProductCode');
        formData['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
        formData['VisitTypeCodeString'] = this.getControlValue('VisitTypeCode');
        formData['NumOfVisits'] = this.getControlValue('NumOfVisits');
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
        formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                if (this.isReturning()) {
                    setTimeout(() => {
                        this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                    }, 500);
                }
                this.riGrid.Execute(data);
            }
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.hasGridData = false;
                this.displayMessage(error);
            });
    }

    public onRiGridRefresh(func?: any): void {
        if (func)
            func();
        this.populateGrid();
    }

    public setCacheRefresh(): void {
        this.pageParams.gridConfig.gridCacheRefresh = true;
    }

    public getCurrentPage(currentPage: any): void {
        this.pageParams.gridConfig.gridCacheRefresh = false;
        super.getCurrentPage(currentPage);
    }

    public riGridBodyOnDblClick(): void {
        let contractType = this.riGrid.Details.GetValue('ContractTypeCode');
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractNumber':
                let contractRowID = this.riGrid.Details.GetAttribute('ContractNumber', 'AdditionalProperty');
                let navPath = '';
                switch (contractType) {
                    case 'J':
                        navPath = ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE;
                        break;
                    case 'P':
                        navPath = ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE;
                        break;
                    default:
                        navPath = ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE;
                        break;
                }
                this.navigate('TechServiceVisit', navPath, { 'ContractRowID': contractRowID });
                break;
            case 'VisitTypeCode':
                this.navigate('ServiceStatsAdjust', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE, {
                    'ServiceCoverRowID': this.riGrid.Details.GetAttribute('ProductCode', 'AdditionalProperty'),
                    'ServiceVisitRowID': this.riGrid.Details.GetAttribute('VisitTypeCode', 'AdditionalProperty')
                });
                break;
            case 'PremiseNumber':
                this.navigate('TechServiceVisit', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    'PremiseRowID': this.riGrid.Details.GetAttribute('PremiseNumber', 'AdditionalProperty'),
                    'ContractTypeCode': contractType
                });
                break;
            case 'ProductCode':
                switch (contractType) {
                    case 'J':
                        navPath = ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCEJOB;
                        break;
                    case 'C':
                        navPath = ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT;
                        break;
                    default:
                        navPath = ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE;
                        break;
                }
                this.navigate('TechServiceVisit', navPath, {
                    'currentContractType': contractType,
                    'ServiceCoverRowID': this.riGrid.Details.GetAttribute('ProductCode', 'AdditionalProperty')
                });
                break;
        }
    }

    public onBranchDataRecived(data: any): void {
        if (data && data.BranchNumber) {
            this.currentBranchCode = data.BranchNumber;
            this.setControlValue('BranchNumber', this.currentBranchCode);
            this.setControlValue('BranchName', data.BranchName);
            this.ellipsis.serviceAreaEllipsis.childparams.BranchNumberServiceBranchNumber = data.BranchNumber;
            this.ellipsis.serviceAreaEllipsis.childparams.ServiceBranchNumber = this.currentBranchCode;
            this.ellipsis.serviceAreaEllipsis.childparams.BranchName = data.BranchName;
            this.ellipsis.employee.childConfigParams.parentMode = this.currentBranchCode ? 'LookUp-Branch-Occupation' : 'LookUp-Occupation ';
        } else {
            this.setControlValue('BranchNumber', '');
            this.setControlValue('BranchName', '');
        }
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.riGrid.RefreshRequired();
    }

    public onDataReceivedServiceSearch(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('EmployeeCode', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
        this.riGrid.RefreshRequired();
    }

    public onChangeBranchServiceAreaCode(): void {
        this.pageParams.gridConfig.gridCacheRefresh = true;
        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.setControlValue('EmployeeCode', '');
            this.setControlValue('EmployeeSurname', '');
            return;
        }
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '0');
        let postParams: any = {};
        postParams.PostDesc = 'ServiceAreaCode';
        postParams.BusinessCode = this.utils.getBusinessCode();
        postParams.BranchNumber = this.getControlValue('BranchNumber');
        postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, postSearchParams, postParams)
            .then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (this.hasError(data)) {
                        this.setControlValue('EmployeeCode', '');
                        this.setControlValue('EmployeeSurname', '');
                        this.setControlValue('BranchServiceAreaCode', '');
                        this.displayMessage(data);
                    } else {
                        this.setControlValue('EmployeeCode', data.EmployeeCode || '');
                        this.setControlValue('EmployeeSurname', data.EmployeeSurname || '');
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.displayMessage(error);
                });
        this.riGrid.RefreshRequired();
    }

    public onContractSearchDataReturn(data: any): void {
        if (data) {
            this.setControlValue('ContractNumber', data.ContractNumber);
            this.setControlValue('ContractName', data.ContractName);
            this.ellipsis.premiseSearch.childConfigParams.ContractNumber = data.ContractNumber;
            this.ellipsis.premiseSearch.childConfigParams.ContractName = data.ContractName;
            this.ellipsis.productSearch.childConfigParams.ContractName = data.ContractName;
            this.ellipsis.productSearch.childConfigParams.ContractNumber = data.ContractNumber;
        }
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.riGrid.RefreshRequired();
    }

    public onContractNumberChange(): void {
        this.pageParams.gridConfig.gridCacheRefresh = true;
        let contractNumber = this.getControlValue('ContractNumber');
        if (!contractNumber) {
            this.setControlValue('ContractNumber', '');
            this.setControlValue('ContractName', '');
            return;
        }

        let lookupIP_details = [{
            'table': 'Contract',
            'query': { 'ContractNumber': contractNumber, 'BusinessCode': this.businessCode },
            'fields': ['ContractName']
        }];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupIP_details).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.setControlValue('ContractName', '');
                this.ellipsis.premiseSearch.childConfigParams.ContractName = '';
                this.ellipsis.premiseSearch.childConfigParams.ContractNumber = '';
                this.displayMessage(data);
            } else {
                if (data && data.length > 0 && data[0].length > 0) {
                    let contract: any = data[0][0];
                    if (contract) {
                        this.setControlValue('ContractName', contract.ContractName);
                        this.ellipsis.premiseSearch.childConfigParams.ContractName = contract.ContractName;
                        this.ellipsis.premiseSearch.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
                        this.ellipsis.productSearch.childConfigParams.ContractName = contract.ContractName;
                        this.ellipsis.productSearch.childConfigParams.ContractNumber = contractNumber;
                    }
                } else {
                    this.setControlValue('ContractNumber', '');
                }
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage(error);
        });
        this.riGrid.RefreshRequired();
    }

    public onPremiseSearchDataReturn(data: any): void {
        if (data) {
            this.setControlValue('PremiseNumber', data.PremiseNumber);
            this.setControlValue('PremiseName', data.PremiseName);
            this.ellipsis.productSearch.childConfigParams.PremiseName = data.PremiseName;
            this.ellipsis.productSearch.childConfigParams.PremiseNumber = data.PremiseNumber;
        }
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.riGrid.RefreshRequired();
    }

    public onPremiseNumberChange(): void {
        this.pageParams.gridConfig.gridCacheRefresh = true;
        let premiseNumber: string = this.getControlValue('PremiseNumber');
        let contractNumber: string = this.getControlValue('ContractNumber');

        if (!contractNumber || !premiseNumber) {
            this.setControlValue('PremiseName', '');
            this.setControlValue('PremiseNumber', '');
            return;
        }

        let lookupQuery: any = [{
            'table': 'Premise',
            'query': { 'PremiseNumber': premiseNumber, 'ContractNumber': contractNumber },
            'fields': ['PremiseName']
        }];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupQuery).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.setControlValue('PremiseNumber', '');
                this.setControlValue('PremiseName', '');
                this.displayMessage(data);
            } else {
                if (data && data.length > 0 && data[0].length > 0) {
                    let premise: any = data[0][0];
                    this.setControlValue('PremiseName', premise.PremiseName || '');
                    this.ellipsis.productSearch.childConfigParams.PremiseName = premise.PremiseName || '';
                    this.ellipsis.productSearch.childConfigParams.PremiseNumber = premiseNumber || '';
                }
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.setControlValue('PremiseNumber', '');
            this.setControlValue('PremiseName', '');
            this.displayMessage(error);
        });
        this.riGrid.RefreshRequired();
    }

    public onProductSearchReceive(data: Object): void {
        if (data) {
            this.setControlValue('ProductCode', data['ProductCode']);
            this.setControlValue('ProductDesc', data['ProductDesc']);
        }
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.riGrid.RefreshRequired();
    }

    public onProductCodeChange(): void {
        this.pageParams.gridConfig.gridCacheRefresh = true;
        if (!this.getControlValue('PremiseNumber') || !this.getControlValue('ContractNumber') || !this.getControlValue('ProductCode')) {
            this.setControlValue('ProductDesc', '');
            this.setControlValue('ProductCode', '');
            return;
        }

        let lookupQuery: any = [{
            'table': 'Product',
            'query': { 'BusinessCode': this.businessCode(), 'ProductCode': this.getControlValue('ProductCode') },
            'fields': ['ProductDesc']
        }];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(lookupQuery).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.setControlValue('ProductCode', '');
                this.displayMessage(data);
            } else {
                if (data && data.length > 0 && data[0].length > 0) {
                    let product: any = data[0][0];
                    if (product) {
                        if (product.ProductDesc) {
                            this.setControlValue('ProductDesc', product.ProductDesc);
                        }
                    }
                } else {
                    this.setControlValue('ProductCode', '');
                }
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage(error);
        });
        this.riGrid.RefreshRequired();
    }

    public onEmployeeDataReceived(data: any): void {
        if (data) {
            this.setControlValue('EmployeeCode', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.riGrid.RefreshRequired();
    }

    public onEmployeeChange(): void {
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.setControlValue('EmployeeSurname', '');
        this.lookupUtils.getEmployeeSurname(this.getControlValue('EmployeeCode')).then((data) => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
            } else {
                this.setControlValue('EmployeeCode', '');
            }
        });
        this.riGrid.RefreshRequired();
    }

    public onVisitTypeReceived(data: any): void {
        if (data) {
            this.setControlValue('VisitTypeCode', data[DropdownConstants.c_s_KEY_MULTISELECTVALUE]);
        }
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.riGrid.RefreshRequired();
    }

    public onServiceTypeChange(data: any): void {
        if (data) {
            this.setControlValue('ServiceTypeCode', data.ServiceTypeCode);
            this.setControlValue('ServiceTypeDesc', data.ServiceTypeDesc);
        }
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.riGrid.RefreshRequired();
    }

    public onDateSelect(): void {
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.riGrid.RefreshRequired();
    }
}
