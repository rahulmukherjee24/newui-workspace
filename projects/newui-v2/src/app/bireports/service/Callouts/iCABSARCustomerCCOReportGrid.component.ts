import { Component, Injector, OnInit, ViewChild, AfterContentInit } from '@angular/core';

import * as moment from 'moment';

import { LightBaseComponent } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { IControls } from '@app/base/ControlsType';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { IXHRParams } from '@base/XhrParams';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { ContractManagementModuleRoutes } from '@base/PageRoutes';
import { MessageConstant } from '@shared/constants/message.constant';

import { AccountSearchComponent } from '@app/internal/search/iCABSASAccountSearch';
import { ContractSearchComponent } from '@app/internal/search/iCABSAContractSearch';
import { PremiseSearchComponent } from '@app/internal/search/iCABSAPremiseSearch';

@Component({
    templateUrl: 'iCABSARCustomerCCOReportGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class CustomerCCOReportGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    //#region Variable Declarations
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected pageId: string;
    protected controls: IControls[] = [
        { name: 'AccountNumber', type: MntConst.eTypeCode },
        { name: 'AccountName', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeCode },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true }
    ];

    public readonly c_s_ACCOUNT: string = 'Account';
    public readonly c_s_CONTRACT: string = 'Contract';
    public readonly c_s_PREMISE: string = 'Premise';

    public ellipsis: Record<string, Record<string, string | Object>> = {
        accountSearch: {
            component: AccountSearchComponent,
            config: {
                parentMode: 'LookUp',
                showAddNewDisplay: false
            }
        },
        contractSearch: {
            component: ContractSearchComponent,
            config: {
                parentMode: 'LookUp'
            }
        },
        premiseSearch: {
            component: PremiseSearchComponent,
            config: {
                parentMode: 'LookUp'
            }
        }
    };

    private readonly c_o_XHRPARAMS: IXHRParams = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARCustomerCCOReportGrid'
    };
    //#endregion

    constructor(injector: Injector,
        private commonLookup: CommonLookUpUtilsService) {
        super(injector);

        this.pageTitle = this.browserTitle = 'Customer CCO Report';
    }

    //#region LifeCycle Hooks
    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();

        this.buildGrid();

        if (!this.isReturning()) {
            const date: string = this.globalize.parseDateToFixedFormat(new Date()).toString();
            this.setControlValue('DateFrom', date);
            this.setControlValue('DateTo', date);

            this.pageParams.gridCurrentPage = 1;
            this.pageParams.totalRecords = 1;
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
        } else {
            this.populateGrid();
        }
    }
    //#endregion

    //#region Private Methods
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('grdContractNumber', 'CustomerCCOReport', 'grdContractNumber', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumn('grdPremiseNumber', 'CustomerCCOReport', 'grdPremiseNumber', MntConst.eTypeInteger, 4, true);
        this.riGrid.AddColumn('ProductCode', 'CustomerCCOReport', 'ProductCode', MntConst.eTypeCode, 6, true);
        this.riGrid.AddColumn('ServiceVisitFrequency', 'CustomerCCOReport', 'ServiceVisitFrequency', MntConst.eTypeInteger, 4, false);
        this.riGrid.AddColumn('NoOfFreeCallouts', 'CustomerCCOReport', 'NoOfFreeCallouts', MntConst.eTypeInteger, 4, false);
        this.riGrid.AddColumn('VisitType', 'CustomerCCOReport', 'VisitType', MntConst.eTypeText, 4, true);
        this.riGrid.AddColumn('VisitDate', 'CustomerCCOReport', 'VisitDate', MntConst.eTypeDate, 10, true);
        this.riGrid.AddColumn('ActualVisitDate', 'CustomerCCOReport', 'ActualVisitDate', MntConst.eTypeDate, 10, true);
        this.riGrid.AddColumn('VisitValue', 'CustomerCCOReport', 'VisitValue', MntConst.eTypeDecimal2, 10, true);

        this.riGrid.AddColumnAlign('grdContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('grdPremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitType', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('grdContractNumber', true);
        this.riGrid.AddColumnOrderable('grdPremiseNumber', true);
        this.riGrid.AddColumnOrderable('ProductCode', true);
        this.riGrid.AddColumnOrderable('VisitDate', true);

        this.riGrid.Complete();
    }

    private updateElllipsisConfig(type: string): void {
        if (type === this.c_s_ACCOUNT) {
            this.ellipsis.contractSearch.config['accountNumber'] = this.getControlValue('AccountNumber');
        } else if (type === this.c_s_CONTRACT) {
            this.ellipsis.premiseSearch.config['AccountNumber'] = this.getControlValue('AccountNumber');
            this.ellipsis.premiseSearch.config['AccountName'] = this.getControlValue('AccountName');
            this.ellipsis.premiseSearch.config['ContractNumber'] = this.getControlValue('ContractNumber');
            this.ellipsis.premiseSearch.config['ContractName'] = this.getControlValue('ContractName');
        }
    }

    private removeFilterNameValue(type: string): void {
        this.setControlValue(type + 'Name', '');
    }

    private populateDates(): void {
        const query: QueryParams = this.getURLSearchParamObject();
        const formData: Record<string, string> = {};

        query.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'PopulateDateFrom';
        formData['AccountNumber'] = this.getControlValue('AccountNumber');
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData['PremiseNumber'] = this.getControlValue('PremiseNumber');

        this.httpService.xhrPost(this.c_o_XHRPARAMS.method,
            this.c_o_XHRPARAMS.module,
            this.c_o_XHRPARAMS.operation,
            query,
            formData).then(data => {
                if (!this.hasError(data) && data.DateFrom) {
                    this.setControlValue('DateFrom', data.DateFrom);
                }
            }).catch(error => {
                this.displayMessage(error);
            });
    }

    private validateForm(): boolean {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return false;
        }

        const fromDate: string = this.getControlValue('DateFrom');
        const toDate: string = this.getControlValue('DateTo');

        if (moment(fromDate).diff(moment(toDate)) > 0) {
            this.displayMessage(MessageConstant.PageSpecificMessage.CCOReport.dateError);
            return false;
        }

        if (!this.getControlValue('AccountNumber')
            && !this.getControlValue('ContractNumber')
            && !this.getControlValue('PremiseNumber')) {
            this.displayMessage(MessageConstant.PageSpecificMessage.CCOReport.filterError);
            return false;
        }

        return true;
    }

    private populateGrid(): void {
        if (!this.validateForm()) {
            return;
        }

        const query: QueryParams = this.getURLSearchParamObject();
        const formData: Record<string, string | number | boolean> = {};

        query.set(this.serviceConstants.Action, '2');

        formData['AccountNumber'] = this.getControlValue('AccountNumber');
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
        formData['DateFrom'] = this.getControlValue('DateFrom');
        formData['DateTo'] = this.getControlValue('DateTo');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.GridPageSize] = 10;
        formData[this.serviceConstants.GridPageCurrent] = this.pageParams.gridCurrentPage || 1;
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn || '';
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder || 'Descending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.c_o_XHRPARAMS.method,
            this.c_o_XHRPARAMS.module,
            this.c_o_XHRPARAMS.operation,
            query,
            formData).then(data => {
                if (!this.hasError(data)) {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.riGrid.RefreshRequired();
                    this.pageParams.gridCacheRefresh = false;
                    this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.pageParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    if (this.isReturning()) {
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 500);
                    }
                    this.riGrid.Execute(data);
                } else {
                    this.displayMessage(data);
                }
            }).catch(error => {
                this.displayMessage(error);
            });
    }
    //#endregion

    //#region Public Methods
    /**
     * @todo - Check Premise Function
     */
    public onFillterNumberChange(type: string): void {
        const filter: string = this.getControlValue(type + 'Number');

        if (filter) {
            switch (type) {
                case this.c_s_ACCOUNT:
                    this.commonLookup.getAccountName(filter).then(data => {
                        if (data && data[0] && data[0].length) {
                            this.setControlValue('AccountName', data[0][0]['AccountName']);
                        } else {
                            this.removeFilterNameValue(type);
                        }
                    }).catch(error => {
                        this.displayMessage(error);
                    });
                    break;
                case this.c_s_CONTRACT:
                    this.commonLookup.getContractName(filter).then(data => {
                        if (data && data[0] && data[0].length) {
                            this.setControlValue('ContractName', data[0][0]['ContractName']);
                        } else {
                            this.removeFilterNameValue(type);
                        }
                    }).catch(error => {
                        this.displayMessage(error);
                    });
                    break;
                case this.c_s_PREMISE:
                    this.commonLookup.getPremiseName(filter, null, this.getControlValue('ContractNumber')).then(data => {
                        if (data && data[0] && data[0].length) {
                            this.setControlValue('PremiseName', data[0][0]['PremiseName']);
                        } else {
                            this.removeFilterNameValue(type);
                        }
                    }).catch(error => {
                        this.displayMessage(error);
                    });
                    break;
            }
            this.populateDates();
        } else {
            this.removeFilterNameValue(type);
        }

        this.updateElllipsisConfig(type);
    }

    public onEllipsisDataReturn(data: Record<string, string>, type: string): void {
        const keys: Record<string, Array<string>> = {
            'Account': ['AccountNumber', 'AccountName'],
            'Contract': ['ContractNumber', 'ContractName'],
            'Premise': ['PremiseNumber', 'PremiseName']
        };

        if (!data) {
            return;
        }

        keys[type].forEach(key => {
            this.setControlValue(key, data[key]);
        });

        this.updateElllipsisConfig(type);

        this.populateDates();
    }

    public onRefreshClick(): void {
        this.pageParams.gridCacheRefresh = true;
        this.populateGrid();
    }

    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.pageParams.gridCacheRefresh = false;
        super.getCurrentPage(currentPage);
    }

    public onGridHeaderClick(): void {
        this.populateGrid();
    }

    public onGridBodyDoubleClick(): void {
        let currentContractType: string = this.riGrid.Details.GetAttribute('ServiceVisitFrequency', 'additionalproperty');
        let rowid: string = this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'additionalproperty');
        switch (this.riGrid.CurrentColumnName) {
            case 'grdContractNumber':
                this.navigate('CustomerCCOReport', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                    currentContractType: currentContractType,
                    ContractRowID: rowid
                });
                break;
            case 'grdPremiseNumber':
                this.navigate('CustomerCCOReport', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    CurrentContractType: currentContractType,
                    ContractTypeCode: currentContractType,
                    PremiseRowID: rowid
                });
                break;
            case 'ProductCode':
                this.navigate('CustomerCCOReport', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT, {
                    currentContractType: currentContractType,
                    contractTypeCode: currentContractType,
                    ServiceCoverRowID: rowid
                });
                break;
        }
    }
    //#endregion
}
