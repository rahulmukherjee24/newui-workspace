import { Component, Injector, OnInit, AfterContentInit, ViewChild } from '@angular/core';

import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { BIReportsRoutes, ContractManagementModuleRoutes } from '@app/base/PageRoutes';

@Component({
    templateUrl: 'iCABSARAdditionalVisitReportGrid.html'
})

export class AdditionalVisitReportGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    public pageId: string;
    public controls: IControls[] = [
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeCode, disabled: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true }
    ];
    public pageTitle: string;
    public commonGridFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    private type: string;
    constructor(injector: Injector) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.browserTitle = this.pageTitle = 'Additional Visit (Chargeable Callouts) Report';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.type = this.riExchange.getParentHTMLValue('type');
        this.pageId = this.type ? PageIdentifier.ICABSARADDITIONALVISITREPORTDETAILSGRID : PageIdentifier.ICABSARADDITIONALVISITREPORTGRID;
        this.pageTitle = `Additional Visit (Chargeable Callouts) ${this.type || ''} Report`;
        this.utils.setTitle(this.pageTitle);
        super.ngAfterContentInit();
        this.buildGrid();
        if (!this.isReturning()) {
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.totalRecords = 1;
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.setControlValue('BusinessCode', this.utils.getBusinessCode());
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            this.riExchange.getParentHTMLValue('BranchNumber');
            this.setControlValue('BranchName', this.utils.getBranchTextOnly(this.getControlValue('BranchNumber')));
            if (this.type) {
                this.riExchange.getParentHTMLValue('DateFrom');
                this.riExchange.getParentHTMLValue('DateTo');
                this.populateGrid();
            } else {
                let date: Date = new Date();
                let fromDate: Date = new Date(date.getFullYear(), 0, 1);
                let toDate: Date = new Date(date.getFullYear(), 11, 31);
                this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(fromDate) as string);
                this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(toDate) as string);
            }
        } else {
            this.pageParams.gridCacheRefresh = false;
            this.populateGrid();
        }
        if (this.type) {
            this.disableControl('DateFrom', true);
            this.disableControl('DateTo', true);
        }

    }

    private buildGrid(): void {
        this.riGrid.Clear();
        if (!this.type)
            this.riGrid.AddColumn('BranchNumber', 'AdditionalVisitReport', 'BranchNumber', MntConst.eTypeInteger, 6, true);
        else
            this.riGrid.AddColumn('ContractPremise', 'AdditionalVisitReport', 'ContractPremise', MntConst.eTypeText, 5, true);
        this.riGrid.AddColumn('FreeOutstanding', 'AdditionalVisitReport', 'FreeOutstanding', MntConst.eTypeInteger, 4, true);
        this.riGrid.AddColumn('FreeDone', 'AdditionalVisitReport', 'FreeDone', MntConst.eTypeInteger, 4, true);
        this.riGrid.AddColumn('AddnlJobsDone', 'AdditionalVisitReport', 'AddnlJobsDone', MntConst.eTypeInteger, 4, true);
        this.riGrid.AddColumn('TotalValueOfJobs', 'AdditionalVisitReport', 'TotalValueOfJobs', MntConst.eTypeInteger, 4, true);

        this.riGrid.AddColumnAlign(!this.type ? 'BranchNumber' : 'ContractPremise', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable(!this.type ? 'BranchNumber' : 'ContractPremise', true);

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');

            let formData: Object = {
                'Level': this.type ? 'Detail' : 'Summary',
                'BusinessCode': this.getControlValue('BusinessCode'),
                'DateFrom': this.getControlValue('DateFrom'),
                'DateTo': this.getControlValue('DateTo'),
                'BranchNumber': this.getControlValue('BranchNumber') === 'Total' ? '0' : this.getControlValue('BranchNumber'),
                [this.serviceConstants.GridMode]: '0',
                [this.serviceConstants.GridHandle]: this.pageParams.gridHandle,
                [this.serviceConstants.PageSize]: '10',
                [this.serviceConstants.PageCurrent]: this.pageParams.gridCurrentPage.toString(),
                [this.serviceConstants.GridCacheRefresh]: this.pageParams.gridCacheRefresh,
                [this.serviceConstants.GridHeaderClickedColumn]: this.riGrid.HeaderClickedColumn,
                [this.serviceConstants.GridSortOrder]: this.riGrid.SortOrder
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', this.type ? 'ApplicationReport/iCABSARAdditionalVisitReportDetailsGrid' : 'ApplicationReport/iCABSARAdditionalVisitReportGrid', search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    this.hasGridData = true;
                    if (this.hasError(data)) {
                        this.displayMessage(data);
                        return;
                    }
                    this.riGrid.RefreshRequired();
                    this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.pageParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    if (this.isReturning()) {
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 500);
                    }
                    this.riGrid.Execute(data);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.hasGridData = true;
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }

    public onGridBodyDoubleClick(): void {
        if (this.riGrid.CurrentColumnName === 'BranchNumber') {
            this.navigate('', BIReportsRoutes.ICABSARADDITIONALVISITREPORTDETAILSGRID, {
                [this.riGrid.CurrentColumnName]: this.riGrid.Details.GetValue('BranchNumber'),
                type: 'Branch'
            });
        }
        if (this.riGrid.CurrentColumnName === 'ContractPremise') {
            this.navigate('AdditionalVisit', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                'PremiseRowID': this.riGrid.Details.GetAttribute('ContractPremise', 'AdditionalProperty')
            });
        }
    }
}
