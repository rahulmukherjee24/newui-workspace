import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { Component, OnInit, Injector, ViewChild, AfterContentInit } from '@angular/core';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSARServiceAndInvoiceSuspendRegion.html',
    providers: [CommonLookUpUtilsService]
})

export class ServiceAndInvoiceSuspendRegionComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public commonGridFunction: CommonGridFunction;
    public pageId: string = '';
    public hasGridData: boolean = false;

    public controls: IControls[] = [
        { name: 'BusinessCode', required: true, type: MntConst.eTypeCode, disabled: true },
        { name: 'BusinessDesc', required: true, type: MntConst.eTypeText, disabled: true },
        { name: 'RegionCode', required: true, type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', required: true, type: MntConst.eTypeText, disabled: true },
        { name: 'SuspendReasonCode', type: MntConst.eTypeCode },
        { name: 'SuspendReasonDesc', type: MntConst.eTypeTextFree }
    ];

    public gridConfig: Record<string, number> = {
        totalItem: 1,
        itemsPerPage: 10
    };

    public dropdown: Record<string, Record<string, Object>> = {
        suspendReasonSearch: {
            params: {
                operation: 'Business/iCABSBSuspendReasonSearch',
                module: 'suspension',
                method: 'contract-management/search'
            },
            displayFields: ['SuspendReasonCode', 'SuspendReasonDesc'],
            active: { id: '', text: '' }
        }
    };

    public xhrParams: any = {
        operation: 'ApplicationReport/iCABSARServiceAndInvoiceSuspendRegion',
        module: 'reports',
        method: 'bi/reports'
    };

    constructor(injector: Injector, private lookupUtils: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSARSERVICEANDINVOICESUSPENDREGION;
        this.browserTitle = this.pageTitle = 'Service and Invoice Suspend';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.buildGrid();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.pageParams.gridHandle = this.utils.randomSixDigitString();
        this.pageParams.gridCurrentPage = 1;
        this.pageParams.gridCacheRefresh = true;

        if (this.isReturning()) {
            if (this.getControlValue('SuspendReasonCode')) {
                this.dropdown.suspendReasonSearch['active'] = {
                    id: this.getControlValue('SuspendReasonCode'),
                    text: this.getControlValue('SuspendReasonCode') + ' - ' + this.getControlValue('SuspendReasonDesc')
                };
            }
            this.onRiGridRefresh();
        }

        if (this.parentMode === 'Region') {
            let suspendReasonCode = this.riExchange.getParentHTMLValue('SuspendReasonCode');
            if (suspendReasonCode) {
                this.dropdown.suspendReasonSearch['active'] = {
                    id: suspendReasonCode,
                    text: suspendReasonCode + ' - ' + this.riExchange.getParentHTMLValue('SuspendReasonDesc')
                };
            }
            this.setControlValue('BusinessCode', this.utils.getBusinessCode());
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            if (this.riExchange.getParentAttributeValue('BranchNumber') > '9') {
                this.setControlValue('RegionCode', this.riExchange.getParentAttributeValue('BranchNumber'));
            } else {
                this.setControlValue('RegionCode', '0' + this.riExchange.getParentAttributeValue('BranchNumber'));
            }
            this.setControlValue('RegionDesc', this.riExchange.getParentAttributeValue('BranchName'));
            this.populateGrid();
        } else {
            this.setControlValue('BusinessCode', this.utils.getBusinessCode());
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            this.lookupUtils.getRegionDesc().subscribe((data) => {
                this.setControlValue('RegionCode', data[0][0]['RegionCode']);
                this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
            });
        }

        if (this.parentMode) {
            this.onRiGridRefresh();
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('BranchNumber', 'ContractExpiry', 'BranchNumber', MntConst.eTypeInteger, 6);
        this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('BranchName', 'ContractExpiry', 'BranchName', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('NumberOfServiceCovers', 'ContractExpiry', 'NumberOfServiceCovers', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('NumberOfServiceCovers', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ValueOfServiceCovers', 'ContractExpiry', 'ValueOfServiceCovers', MntConst.eTypeDecimal2, 10);
        this.riGrid.AddColumnAlign('ValueOfServiceCovers', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        gridQuery.set(this.serviceConstants.CountryCode, this.countryCode());
        gridQuery.set(this.serviceConstants.Action, '2');

        let formData: Object = {};
        formData[this.serviceConstants.Level] = 'Branch';
        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        formData['RegionCode'] = this.getControlValue('RegionCode');
        formData['ReportLevel'] = 'RegionLevel';
        formData['SuspendReasonFilter'] = this.getControlValue('SuspendReasonCode') || 'all';
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });

    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGridFunction.onRefreshClick();
        }
    }

    public onSuspendReasonDataRecieved(data: any): void {
        if (data) {
            this.setControlValue('SuspendReasonCode', data.SuspendReasonCode);
            this.setControlValue('SuspendReasonDesc', data.SuspendReasonDesc);
        }
    }

    public onGridBodyDoubleClick?(): void {
        if (this.riGrid.CurrentColumnName === 'BranchNumber') {
            let rowId: string = this.riGrid.Details.GetAttribute('BranchNumber', 'rowid');
            if (rowId !== 'Total') {
                this.setAttribute('SuspendReasonCode', this.getControlValue('SuspendReasonCode'));
                this.setAttribute('SuspendReasonDesc', this.getControlValue('SuspendReasonDesc'));
                this.setAttribute('BranchName', this.riGrid.Details.GetValue('BranchName'));
                this.setAttribute('BranchNumber', this.riGrid.Details.GetValue('BranchNumber'));
                this.navigate('Region', BIReportsRoutes.ICABSARSERVICEANDINVOICESUSPENDBRANCH, {
                    pageType: 'branch'
                });
            }
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }
}
