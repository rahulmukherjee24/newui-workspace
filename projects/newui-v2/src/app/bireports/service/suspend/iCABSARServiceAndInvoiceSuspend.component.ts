import { AfterContentInit, Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { ContractManagementModuleRoutes, BIReportsRoutes } from '@app/base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSARServiceAndInvoiceSuspend.html'
})

export class ServiceAndInvoiceSuspendComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected pageId: string;

    public commonGridFunction: CommonGridFunction;
    public currentContractType: string;
    public hasGridData: boolean = false;
    public isBranch: boolean;
    public isBusiness: boolean;
    public pageType: string;

    private xhrParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: {
            'Branch': 'ApplicationReport/iCABSARServiceandInvoiceSuspendGrid',
            'Business': 'ApplicationReport/iCABSARServiceAndInvoiceSuspendBusiness'
        }
    };

    public controls: IControls[] = [
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true, required: true },
        { name: 'BranchNumber', type: MntConst.eTypeCode, disabled: true, required: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode, disabled: true, required: true },
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true, required: true },
        { name: 'SuspendReasonCode', type: MntConst.eTypeCode },
        { name: 'SuspendReasonDesc', type: MntConst.eTypeTextFree },
        { name: 'ViewBy', required: true }
    ];

    public dropdown: Record<string, Record<string, Object>> = {
        suspendReasonSearch: {
            params: {
                operation: 'Business/iCABSBSuspendReasonSearch',
                module: 'suspension',
                method: 'contract-management/search'
            },
            displayFields: ['SuspendReasonCode', 'SuspendReasonDesc'],
            active: { id: '', text: '' }
        }
    };

    public gridConfig: Record<string, number> = {
        totalItem: 1,
        itemsPerPage: 10
    };

    constructor(private injector: Injector) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageType = this.utils.capitalizeFirstLetter(this.riExchange.getParentHTMLValue('pageType'));
        this.pageId = PageIdentifier['ICABSARSERVICEANDINVOICESUSPEND' + this.pageType.toUpperCase()];
        this.browserTitle = this.pageTitle = 'Service and Invoice Suspend';
        this['is' + this.pageType] = this.pageType;

        super.ngAfterContentInit();
        this.currentContractType = this.riExchange.getCurrentContractType();

        this.buildGrid();

        if (this.isReturning()) {
            this.setSuspendReason();
            this.onRiGridRefresh();
        } else {
            this.setControlValue('BusinessCode', this.utils.getBusinessCode());
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            if (this.isBusiness) {
                this.setControlValue('ViewBy', 'Branch');
            }

            if (this.isBranch) {
                if (this.parentMode === 'Business' || this.parentMode === 'Region') {
                    this.setControlValue('BranchNumber', this.riExchange.getParentAttributeValue('BranchNumber'));
                    this.setControlValue('BranchName', this.riExchange.getParentAttributeValue('BranchName'));
                    this.setControlValue('SuspendReasonCode', this.riExchange.getParentAttributeValue('SuspendReasonCode'));
                    this.setControlValue('SuspendReasonDesc', this.riExchange.getParentAttributeValue('SuspendReasonDesc'));
                    this.setSuspendReason();
                } else {
                    this.setControlValue('BranchNumber', this.utils.getBranchCode());
                    this.setControlValue('BranchName', this.utils.getBranchTextOnly());
                }
            }
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;
        }

        if (this.parentMode) {
            this.onRiGridRefresh();
        }

    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public buildGrid(): void {
        this.riGrid.Clear();

        if (this.isBranch) {
            this.riGrid.AddColumn('ContractNumber', 'ContractExpiry', 'ContractNumber', MntConst.eTypeCode, 8);
            this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('PremiseNumber', 'ContractExpiry', 'PremiseNumber', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('ProductCode', 'ContractExpiry', 'ProductCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ServiceVisitFrequency', 'ContractExpiry', 'ServiceVisitFrequency', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ServiceQuantity', 'ContractExpiry', 'ServiceQuantity', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ServiceSuspendQuantity', 'ContractExpiry', 'ServiceSuspendQuantity', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('ServiceSuspendQuantity', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('SuspendReason', 'ContractExpiry', 'SuspendReason', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('SuspendReason', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('ServiceAnnualValue', 'ContractExpiry', 'ServiceAnnualValue', MntConst.eTypeText, 8);
            this.riGrid.AddColumnAlign('ServiceAnnualValue', MntConst.eAlignmentRight);

            this.riGrid.AddColumn('SuspendStartDate', 'ContractExpiry', 'SuspendStartDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('SuspendStartDate', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('SuspendEndDate', 'ContractExpiry', 'SuspendEndDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('SuspendEndDate', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('SuspendHistory', 'ContractExpiry', 'SuspendHistory', MntConst.eTypeImage, 1, true);
            this.riGrid.AddColumnAlign('SuspendHistory', MntConst.eAlignmentCenter);


            this.riGrid.AddColumnOrderable('ContractNumber', true);
            this.riGrid.AddColumnOrderable('SuspendStartDate', true);
            this.riGrid.AddColumnOrderable('SuspendEndDate', true);
            this.riGrid.AddColumnOrderable('ServiceAnnualValue', true);
        }

        if (this.isBusiness) {
            this.riGrid.AddColumn('BranchNumber', 'ContractExpiry', 'BranchNumber', MntConst.eTypeInteger, 6);
            this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentRight);

            this.riGrid.AddColumn('BranchName', 'ContractExpiry', 'BranchName', MntConst.eTypeText, 40);
            this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('NumberOfServiceCovers', 'ContractExpiry', 'NumberOfServiceCovers', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('NumberOfServiceCovers', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ValueOfServiceCovers', 'ContractExpiry', 'ValueOfServiceCovers', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumnAlign('ValueOfServiceCovers', MntConst.eAlignmentCenter);
        }

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.Level] = 'Branch';
        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        if (this.isBranch) {
            formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        }
        if (this.isBusiness) {
            formData['ViewBy'] = this.getControlValue('ViewBy');
        }
        formData['ReportLevel'] = this.pageType + 'Level';
        formData['SuspendReasonFilter'] = this.getControlValue('SuspendReasonCode') === '' ? 'all' : this.getControlValue('SuspendReasonCode');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation[this.pageType], gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });

    }

    public onSuspendReasonDataRecieved(data: any): void {
        if (data) {
            this.setControlValue('SuspendReasonCode', data.SuspendReasonCode);
            this.setControlValue('SuspendReasonDesc', data.SuspendReasonDesc);
        }
    }

    private setSuspendReason(): void {
        if (this.getControlValue('SuspendReasonCode')) {
            this.dropdown.suspendReasonSearch['active'] = {
                id: this.getControlValue('SuspendReasonCode'),
                text: this.getControlValue('SuspendReasonCode') + ' - ' + this.getControlValue('SuspendReasonDesc')
            };
        }
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGridFunction.onRefreshClick();
        }
    }

    public onGridBodyDoubleClick?(): void {
        let currentColumn = this.riGrid.CurrentColumnName;
        if (this.riGrid.Details.GetAttribute(currentColumn, 'rowid') !== 'Total') {
            switch (currentColumn) {
                case 'BranchNumber':
                    if (this.getControlValue('ViewBy') === 'Branch') {
                        this.setAttribute('SuspendReasonCode', this.getControlValue('SuspendReasonCode'));
                        this.setAttribute('SuspendReasonDesc', this.getControlValue('SuspendReasonDesc'));
                        this.setAttribute('BranchName', this.riGrid.Details.GetValue('BranchName'));
                        this.setAttribute('BranchNumber', this.riGrid.Details.GetValue('BranchNumber'));
                        this.navigate('Business', BIReportsRoutes.ICABSARSERVICEANDINVOICESUSPENDBRANCH, {
                            pageType: this.getControlValue('ViewBy')
                        });
                    } else {
                        this.setAttribute('SuspendReasonCode', this.getControlValue('SuspendReasonCode'));
                        this.setAttribute('SuspendReasonDesc', this.getControlValue('SuspendReasonDesc'));
                        this.setAttribute('BranchName', this.riGrid.Details.GetValue('BranchName'));
                        this.setAttribute('BranchNumber', this.riGrid.Details.GetValue('BranchNumber'));
                        this.navigate('Region', BIReportsRoutes.ICABSARSERVICEANDINVOICESUSPENDREGION, {
                            pageType: this.getControlValue('ViewBy')
                        });
                    }
                    break;
                case 'ProductCode':
                    if (this.riGrid.Details.GetAttribute(currentColumn, 'rowid')) {
                        this.navigate('SuspendServiceandInvoice', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT, {
                            CurrentContractType: 'C',
                            ServiceCoverRowID: this.riGrid.Details.GetAttribute(currentColumn, 'rowid')
                        });
                    }
                    break;
                case 'PremiseNumber':
                    if (this.riGrid.Details.GetAttribute(currentColumn, 'rowid')) {
                        this.navigate('SuspendServiceandInvoice', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                            CurrentContractType: 'C',
                            PremiseRowID: this.riGrid.Details.GetAttribute(currentColumn, 'rowid')
                        });
                    }
                    break;
                case 'ContractNumber':
                    if (this.riGrid.Details.GetAttribute(currentColumn, 'rowid')) {
                        this.navigate('SuspendServiceandInvoice', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                            currentContractType: 'C',
                            ContractRowID: this.riGrid.Details.GetAttribute(currentColumn, 'rowid')
                        });
                    }
                    break;
                case 'SuspendHistory':
                    if (this.riGrid.Details.GetAttribute(currentColumn, 'rowid')) {
                        this.setAttribute('SuspendStartDate', this.riGrid.Details.GetValue('SuspendStartDate'));
                        this.setAttribute('SuspendEndDate', this.riGrid.Details.GetValue('SuspendEndDate'));
                        this.setAttribute('PremiseNumber', this.riGrid.Details.GetValue('PremiseNumber'));
                        this.setAttribute('ProductCode', this.riGrid.Details.GetValue('ProductCode'));
                        this.setAttribute('ContractNumber', this.riGrid.Details.GetValue('ContractNumber'));
                        this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute(currentColumn, 'rowid'));
                        this.navigate('SuspendServiceandInvoice', BIReportsRoutes.ICABSARSERVICESUSPENDHISTORY);
                    }
                    break;
            }
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }
}
