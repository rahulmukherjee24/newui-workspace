import { BulkProofOfServiceComponent } from './iCABSBulkProofofService.component';
import { Component, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { BIReportsRoutesConstant } from '@base/PageRoutes';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { InternalSearchModule } from '@internal/search.module';
import { SharedModule } from '@shared/shared.module';

import { BatchSOSAgedProfileGridComponent } from './stateofservice/iCABSSeBatchSOSAgedProfileGrid.component';
import { BatchStateOfServiceGridComponent } from './stateofservice/iCABSSeBatchStateOfServiceGrid.component';
import { ServiceDailyProductivityGridComponent } from './productivity/DailyProductivityEmployeeDetail/iCABSSeServiceDailyProductivityGrid';
import { StateOfServiceGridComponent } from './stateofservice/iCABSSeStateOfServiceGrid.component';
import { StateOfServiceReportsGridComponent } from './stateofservice/iCABSSeStateOfServiceReportsGrid.component';
import { TechnicianServiceVisitGridComponent } from './iCABSTechnicianServiceVisitGrid.component';
import { ProductivityMultiPESGridComponent } from './productivity/ProductivityPES/iCABSSeProductivityMultiPESGrid.component';
import { ProductivityPESGridComponent } from './productivity/ProductivityPES/iCABSSeProductivityPESGrid.component';
import { ProductivityPESDetailComponent } from './productivity/ProductivityPES/iCABSSeProductivityPESDetail.component';
import { ProductivityHCGridComponent } from './productivity/ProductivityHC/iCABSSeProductivityHCGrid.component';
import { ProductivityServiceVisitsPESGridComponent } from './productivity/ProductivityPES/iCABSSeProductivityServiceVisitsPESGrid.component';
import { StateOfServiceDetailReportComponent } from './stateofservice/iCABSARStateOfServiceDetailReport.component';
import { AdditionalVisitReportGridComponent } from './iCABSARAdditionalVisitReportGrid.component';
import { StateOfServiceNetAccountGridComponent } from './stateofservice/iCABSSeStateOfServiceNatAccountGrid.component';
import { CustomerCCOReportGridComponent } from './Callouts/iCABSARCustomerCCOReportGrid.component';
import { BatchSOSAgedArrearsBusinessGridComponent } from './stateofservice/iCABSSeBatchSOSAgedArrearsBusinessGrid.component';
import { AgedArrearsReportsGridComponent } from './stateofservice/iCABSSeAgedArrearsReportsGrid.component';
import { InfestationGridComponent } from './infestation/iCABSSeInfestationGrid.component';
import { ServiceAndInvoiceSuspendComponent } from './suspend/iCABSARServiceAndInvoiceSuspend.component';
import { ServiceSuspendHistoryComponent } from './suspend/iCABSARServiceSuspendHistory.component';
import { ServiceAndInvoiceSuspendRegionComponent } from './suspend/iCABSARServiceAndInvoiceSuspendRegion.component';
import { DailyProductivityComponent } from './productivity/DailyProductivity/iCABSSeDailyProductivity';
import { OutstandingGridComponent } from './outstanding/iCABSSeOutstandingGrid.component';
import { StockRequirementsComponent } from './Stock/iCABSARIWSStockRequirements.component';
import { InfestationServiceCoverGridComponent } from './infestation/iCABSSeInfestationServiceCoverGrid.component';
import { VisitTypeGridComponent } from './servicevisit/iCABSSeVisitTypeGrid.component';
import { StaticVisitGridYearBranchComponent } from './iCABSSeStaticVisitGridYearBranch.component';
import { ComponentOrderingListGridComponent } from './iCABSSeComponentOrderingListGrid.component';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class ProductivityBIReportsRootComponent {
    constructor() {

    }
}

@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        InternalSearchModule,
        RouterModule.forChild([
            {
                path: '', component: ProductivityBIReportsRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSSESERVICEDAILYPRODUCTIVITYGRID, component: ServiceDailyProductivityGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSESERVICEDAILYPRODUCTIVITYGRIDFR, component: ServiceDailyProductivityGridComponent },
                    { path: BIReportsRoutesConstant.ICABSTECHNICIANSERVICEVISITGRID, component: TechnicianServiceVisitGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEBATCHSTATEOFSERVICEBUSINESSGRID, component: BatchStateOfServiceGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEBATCHSTATEOFSERVICEBRANCHGRID, component: BatchStateOfServiceGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEBATCHSTATEOFSERVICEREGIONGRID, component: BatchStateOfServiceGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTATEOFSERVICEGRID, component: StateOfServiceGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTATEOFSERVICEREPORTSGRIDBYBUSINESS, component: StateOfServiceReportsGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTATEOFSERVICEREPORTSGRIDBYREGION, component: StateOfServiceReportsGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTATEOFSERVICEREPORTSGRIDBYBRANCH, component: StateOfServiceReportsGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTATEOFSERVICEREPORTSGRIDAGEDBYBRANCH, component: StateOfServiceReportsGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTATEOFSERVICEREPORTSGRIDAGEDBYBUSINESS, component: StateOfServiceReportsGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYMULTIPESGRIDBYBUSINESS, component: ProductivityMultiPESGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYMULTIPESGRIDBYBRANCH, component: ProductivityMultiPESGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYMULTIPESGRIDBYREGION, component: ProductivityMultiPESGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYBUSINESSPESGRID, component: ProductivityPESGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYBRANCHPESGRID, component: ProductivityPESGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYREGIONPESGRID, component: ProductivityPESGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYPESDETAIL, component: ProductivityPESDetailComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYBUSINESSGRID, component: ProductivityHCGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYREGIONGRID, component: ProductivityHCGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYBRANCHGRID, component: ProductivityHCGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYGRID, component: ProductivityHCGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEPRODUCTIVITYSERVICEVISITSPESGRID, component: ProductivityServiceVisitsPESGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARSTATEOFSERVICEDETAILREPORT, component: StateOfServiceDetailReportComponent },
                    { path: BIReportsRoutesConstant.ICABSSEAGEDARREARSREPORTGRIDBYBUSINESS, component: AgedArrearsReportsGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEAGEDARREARSREPORTGRIDBYBRANCH, component: AgedArrearsReportsGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEBATCHSOSAGEDARREARSBUSINESSGRID, component: BatchSOSAgedArrearsBusinessGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEBATCHSOSAGEDARREARSBRANCHGRID, component: BatchSOSAgedArrearsBusinessGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEBATCHSOSAGEDARREARSDETAILGRID, component: BatchSOSAgedArrearsBusinessGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEBATCHSOSAGEDPROFILEBRANCHGRID, component: BatchSOSAgedProfileGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEBATCHSOSAGEDPROFILEBUSINESSGRID, component: BatchSOSAgedProfileGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARADDITIONALVISITREPORTGRID, component: AdditionalVisitReportGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARADDITIONALVISITREPORTDETAILSGRID, component: AdditionalVisitReportGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTATEOFSERVICENATACCOUNTGRID, component: StateOfServiceNetAccountGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARCUSTOMERCCOREPORTGRID, component: CustomerCCOReportGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEINFESTATIONGRID, component: InfestationGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARSERVICEANDINVOICESUSPENDBUSINESS, component: ServiceAndInvoiceSuspendComponent },
                    { path: BIReportsRoutesConstant.ICABSARSERVICEANDINVOICESUSPENDBRANCH, component: ServiceAndInvoiceSuspendComponent },
                    { path: BIReportsRoutesConstant.ICABSARSERVICESUSPENDHISTORY, component: ServiceSuspendHistoryComponent },
                    { path: BIReportsRoutesConstant.ICABSARSERVICEANDINVOICESUSPENDREGION, component: ServiceAndInvoiceSuspendRegionComponent },
                    { path: BIReportsRoutesConstant.ICABSSEDAILYPRODUCTIVITYBUSINESSGRID, component: DailyProductivityComponent },
                    { path: BIReportsRoutesConstant.ICABSSEDAILYPRODUCTIVITYBRANCHGRID, component: DailyProductivityComponent },
                    { path: BIReportsRoutesConstant.ICABSSEDAILYPRODUCTIVITYSUMMARYGRID, component: DailyProductivityComponent },
                    { path: BIReportsRoutesConstant.ICABSBULKPROOFOFSERVICE, component: BulkProofOfServiceComponent },
                    { path: BIReportsRoutesConstant.ICABSSEOUTSTANDINGINSTALLATIONSGRID, component: OutstandingGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEOUTSTANDINGBUSINESSINSTALLATIONSGRID, component: OutstandingGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEOUTSTANDINGREMOVALSGRID, component: OutstandingGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEOUTSTANDINGBUSINESSREMOVALSGRID, component: OutstandingGridComponent },
                    { path: BIReportsRoutesConstant.ICABSARIWSSTOCKREQUIREMTS, component: StockRequirementsComponent },
                    { path: BIReportsRoutesConstant.ICABSSEINFESTATIONSERVICECOVERGRID, component: InfestationServiceCoverGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSEVISITTYPEGRID, component: VisitTypeGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSESTATICVISITGRIDYEARBRANCH, component: StaticVisitGridYearBranchComponent },
                    { path: BIReportsRoutesConstant.ICABSSECOMPONENTORDERINGLISTGRID, component: ComponentOrderingListGridComponent },
                    { path: BIReportsRoutesConstant.ICABSSECOMPONENTREPLACEMENTDETAILGRID, component: ComponentOrderingListGridComponent }
                ], data: { domain: 'BI REPORTS SERVICE' }
            }

        ])
    ],
    declarations: [
        ProductivityBIReportsRootComponent,
        ServiceDailyProductivityGridComponent,
        TechnicianServiceVisitGridComponent,
        BatchStateOfServiceGridComponent,
        StateOfServiceGridComponent,
        StateOfServiceReportsGridComponent,
        ProductivityMultiPESGridComponent,
        ProductivityPESGridComponent,
        ProductivityPESDetailComponent,
        ProductivityHCGridComponent,
        ProductivityServiceVisitsPESGridComponent,
        StateOfServiceDetailReportComponent,
        AgedArrearsReportsGridComponent,
        BatchSOSAgedArrearsBusinessGridComponent,
        BatchSOSAgedProfileGridComponent,
        BatchSOSAgedArrearsBusinessGridComponent,
        AdditionalVisitReportGridComponent,
        StateOfServiceNetAccountGridComponent,
        CustomerCCOReportGridComponent,
        InfestationGridComponent,
        ServiceAndInvoiceSuspendComponent,
        ServiceSuspendHistoryComponent,
        ServiceAndInvoiceSuspendRegionComponent,
        DailyProductivityComponent,
        BulkProofOfServiceComponent,
        OutstandingGridComponent,
        StockRequirementsComponent,
        InfestationServiceCoverGridComponent,
        VisitTypeGridComponent,
        StaticVisitGridYearBranchComponent,
        ComponentOrderingListGridComponent
    ]
})

export class ProductivityBIReportsModule { }
