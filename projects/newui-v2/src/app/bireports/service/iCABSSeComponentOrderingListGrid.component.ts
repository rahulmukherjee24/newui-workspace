
import { Component, OnInit, OnDestroy, Injector, ViewChild, AfterContentInit, HostListener } from '@angular/core';

import { BIReportsRoutes } from '@base/PageRoutes';
import { CommonGridFunction } from '@base/CommonGridFunction';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';

@Component({
    templateUrl: 'iCABSSeComponentOrderingListGrid.html'
})


export class ComponentOrderingListGridComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;


    private xhrParams: Record<string, IXHRParams> = {
        branch: {
            method: 'bi/reports',
            module: 'reports',
            operation: 'Service/iCABSSeComponentOrderingListGrid'
        },
        detail: {
            method: 'bi/reports',
            module: 'reports',
            operation: 'Service/iCABSSeComponentReplacementDetailGrid'
        }

    };

    public pageId: string = '';
    public pageType: string = '';
    public vEndofWeekDate: Date;
    public commonGridFunction: CommonGridFunction;
    public isBranch: boolean = false;

    protected controls: IControls[] = [
        { name: 'BranchNumber', value: this.utils.getBranchCode(), type: MntConst.eTypeInteger, disabled: true },
        { name: 'BranchName', value: this.utils.getBranchTextOnly(), type: MntConst.eTypeText, disabled: true },
        { name: 'From', required: true, type: MntConst.eTypeDate,disabled: true },
        { name: 'To', required: true, type: MntConst.eTypeDate, disabled: true }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    ngAfterContentInit(): void {
        this.pageType = this.riExchange.getParentHTMLValue('reportLevel') || 'Detail';
        this.isBranch = this.pageType === 'Branch';
        this.pageId = PageIdentifier['ICABSSECOMPONENTORDERING' + this.pageType.toUpperCase()];
        super.ngAfterContentInit();

        if (this.isReturning()) {
            this.riGrid.HeaderClickedColumn = this.pageParams.headerGirdColumn || '';
            this.riGrid.DescendingSort = this.pageParams.GridSortOrder === 'Descending';
        } else {
            this.pageParams = {
                hasGridData: false,
                gridCacheRefresh: true,
                gridCurrentPage: 1,
                riGridSortOrder: 'Descending',
                GridHandle: this.utils.randomSixDigitString(),
                gridConfig: {
                    itemsPerPage: 20,
                    totalItem: 1
                }
            };
            if (this.isBranch) {
                this.getDay();
            } else {
                this.riExchange.getParentHTMLValue('From');
                this.riExchange.getParentHTMLValue('To');
            }
        }
        this.onWindowLoad();
    }

    private onWindowLoad(): void {
        this.browserTitle = this.pageTitle = 'Branch Component Replacement Ordering ' + (this.isBranch ? 'List' : 'Details');
        this.buildGrid();
        this.commonGridFunction.onRiGridRefresh();
    }

    private buildGrid(): void {

        this.riGrid.Clear();
        if (this.pageType === 'Branch') {
            this.riGrid.AddColumn('ReportNumber', 'ComponentReplacemetn', 'ReportNumber', MntConst.eTypeInteger, 15, true, 'Report Number');
            this.riGrid.AddColumn('GeneratedDate', 'ComponentReplacement', 'GeneratedDate', MntConst.eTypeDate, 10, false, 'Report Generated On');
            this.riGrid.AddColumn('GeneratedTime', 'ComponentReplacement', 'GeneratedTime', MntConst.eTypeTime, 10, false, 'Report Generated At');
            this.riGrid.AddColumn('DateFrom', 'ComponentReplacement', 'DateFrom', MntConst.eTypeDate, 10, false, 'Date From');
            this.riGrid.AddColumn('DateTo', 'ComponentReplacement', 'DateTo', MntConst.eTypeDate, 10, false, 'Date To');
            this.riGrid.AddColumn('NumberOfComponents', 'ComponentReplacement', 'NumberOfComponents', MntConst.eTypeInteger, 15, false, 'Number of Components');
            this.riGrid.AddColumnAlign('ReportNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('GeneratedDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('GeneratedTime', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('DateFrom', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('DateTo', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('NumberOfComponents', MntConst.eAlignmentCenter);

        } else {
            this.riGrid.AddColumn('ContractNumber', 'ComponentReplacement', 'ContractNumber', MntConst.eTypeCode, 10);
            this.riGrid.AddColumn('PremiseNumber', 'ComponentReplacement', 'PremiseNumber', MntConst.eTypeInteger, 7);
            this.riGrid.AddColumn('PremiseName', 'ComponentReplacement', 'PremiseName', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('VisitFrequency', 'ComponentReplacement', 'VisitFrequency', MntConst.eTypeInteger, 7);
            this.riGrid.AddColumn('ProductCode', 'ComponentReplacement', 'ProductCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumn('ComponentReplacementCode', 'ComponentReplacement', 'ComponentReplacementCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumn('ComponentReplacementAlternateCode', 'ComponentReplacement', 'ComponentReplacementAlternateCode', MntConst.eTypeCode, 20);
            this.riGrid.AddColumn('ComponentReplacementDesc', 'ComponentReplacement', 'ComponentReplacementDesc', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('ComponentReplacementQty', 'ComponentReplacement', 'ComponentReplacementQty', MntConst.eTypeInteger, 7);
            this.riGrid.AddColumn('BranchServiceAreaCode', 'ComponentReplacement', 'BranchServiceAreaCode', MntConst.eTypeCode, 8);
        }

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        const formData: any = {};
        const search: QueryParams = this.getURLSearchParamObject();
        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        formData['Level'] = 'Branch';
        if (this.isBranch) {
            search.set(this.serviceConstants.Action, '2');
            formData['DateFrom'] = this.getControlValue('From');
            formData['DateTo'] = this.getControlValue('To');
            formData['SubmitBatchProcess'] = 'No';
        } else {
            search.set(this.serviceConstants.Action, '2');
            formData['RowID'] = this.riExchange.getParentHTMLValue('RowID');
            formData['ReportNumber'] = this.riExchange.getParentHTMLValue('ReportNumber');
        }

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.GridHandle;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.xhrParams[this.pageType.toLowerCase()].method, this.xhrParams[this.pageType.toLowerCase()].module, this.xhrParams[this.pageType.toLowerCase()].operation, search, formData).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.commonGridFunction.setPageData(data, this.pageParams.gridConfig);
            }).catch(error => {
                this.pageParams.gridConfig.hasGridData = false;
                this.displayMessage(error);
            });
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.buildGrid();
            this.commonGridFunction.onRefreshClick();
        }
    }

    public onRiGridBodyDoubleClick(event: any): void {
        if (this.riGrid.CurrentColumnName === 'ReportNumber') {
            this.navigate('Branch', BIReportsRoutes.ICABSSECOMPONENTREPLACEMENTDETAILGRID,
                {
                    'ReportNumber': this.riGrid.Details.GetValue('ReportNumber'),
                    'RowID': this.riGrid.Details.GetAttribute('ReportNumber', 'rowID')
                });
        }

    }

    public generateReport(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        const form: any = {};
        const search: QueryParams = this.getURLSearchParamObject();
        if (this.pageType === 'Branch') {
            search.set(this.serviceConstants.Action, '6');
            form['SubmitBatchProcess'] = 'yes';
            form['BusinessCode'] = this.utils.getBusinessCode();
            form['DateFrom'] = this.getControlValue('From');
            form['DateTo'] = this.getControlValue('To');
            form['BranchNumber'] = this.utils.getBranchCode();
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams[this.pageType.toLowerCase()].method, this.xhrParams[this.pageType.toLowerCase()].module, this.xhrParams[this.pageType.toLowerCase()].operation, search, form).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage(data.BatchProcessInformation, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
        }).catch(error => {
            this.pageParams.gridConfig.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public getDay(): void {
        const today = new Date();
        this.vEndofWeekDate = this.utils.addDays(today, ((7 - today.getDay())));
        let tempDate = this.vEndofWeekDate.toString();
        tempDate = (this.globalize.parseDateStringToDate(tempDate)).toString();
        this.vEndofWeekDate = new Date(tempDate);
        const tempDtFrom: Date = this.utils.addDays(this.vEndofWeekDate, 1);
        this.setControlValue('From', tempDtFrom);
        const tempDtTo = this.utils.addDays(tempDtFrom, 6);
        this.setControlValue('To', tempDtTo);
    }
    @HostListener('document:keydown', ['$event']) document_onkeydown(ev: any): void {
        if (!this.isBranch) {
            return;
        }
        let dtFrom = this.getControlValue('From');
        let dtTo = this.getControlValue('To');
        const keyCode = ev.keyCode;
        const isShiftKey = ev.shiftKey;

        if (keyCode === 106 || (isShiftKey && keyCode === 56)) {

            const datetFrom: Date = this.utils.addDays(this.vEndofWeekDate, 1);
            const dateTo: Date = this.utils.addDays(this.vEndofWeekDate, 7);
            this.setControlValue('From', datetFrom);
            this.setControlValue('To', dateTo);

        } else if (keyCode === 107 || (isShiftKey && keyCode === 187)) {

            dtFrom = this.utils.addDays(dtFrom, 7);
            dtTo = this.utils.addDays(dtTo, 7);
            this.setControlValue('From', dtFrom);
            this.setControlValue('To', dtTo);

        } else if (keyCode === 109 || (isShiftKey && keyCode === 189)) {

            dtFrom = this.utils.addDays(dtFrom, -7);
            dtTo = this.utils.addDays(dtTo, -7);
            this.setControlValue('From', dtFrom);
            this.setControlValue('To', dtTo);

        }
    }


}
