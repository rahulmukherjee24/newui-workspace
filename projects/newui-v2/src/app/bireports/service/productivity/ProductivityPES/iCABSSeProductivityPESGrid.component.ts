import { Component, OnInit, AfterContentInit, Injector, ViewChild } from '@angular/core';

import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { ProductivityDropDownService } from './productivityDropDown.service';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSSeProductivityPESGrid.html',
    providers: [ProductivityDropDownService]
})

export class ProductivityPESGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    constructor(injector: Injector, public prodService: ProductivityDropDownService, private syscharConstants: SysCharConstants) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPRODUCTIVITYPESGRID;
    }

    private actualShowType: string;
    private reportNumber: string; //this may be required in future developements
    private rowid: string;

    public controls: Array<Object> = [
        { name: 'BusinessDesc', type: MntConst.eTypeCode, disabled: true },
        { name: 'ShowType', type: MntConst.eTypeText, disabled: true },
        { name: 'GroupBy', type: MntConst.eTypeText },
        { name: 'DateFrom', type: MntConst.eTypeText, disabled: true },
        { name: 'DateTo', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'SupervisorEmployeeCode', type: MntConst.eTypeText, disabled: true },
        { name: 'SupervisorSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true }
    ];
    public pageId: string;
    public pageType: string;
    public gridConfig: any = {
        pageSize: 10,
        totalRecords: 1,
        actionGrid: '2',
        riSortOrder: ''
    };

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.pageType = this.riExchange.getParentHTMLValue('parentMode');
        if (this.pageType === 'Branch') {
            this.pageId = PageIdentifier.ICABSSEPRODUCTIVITYPESGRIDBRANCH;
        } else if (this.pageType === 'Region') {
            this.pageId = PageIdentifier.ICABSSEPRODUCTIVITYPESGRIDREGION;
        }
        super.ngAfterContentInit();
        this.onWindowLoad();
    }

    private onWindowLoad(): void {
        this.getSysChars();
        this.browserTitle = this.pageTitle = this.pageType + ' Service Productivity';
        this.setControlValue('ShowType', this.riExchange.getParentHTMLValue('ShowType'));
        this.setControlValue('DateFrom', this.riExchange.getParentHTMLValue('DateFrom'));
        this.setControlValue('DateTo', this.riExchange.getParentHTMLValue('DateTo'));
        this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('BranchName') || this.utils.getBranchTextOnly(this.getControlValue('BranchNumber').trim()));
        switch (this.pageType) {
            case 'Business':
                this.setControlValue('BusinessDesc', this.utils.getBusinessText());
                this.prodService.view = [
                    { value: 'Branch', text: 'Branch' },
                    { value: 'Region', text: 'Region' },
                    { value: 'Employee', text: 'Employee' }
                ];
                this.setControlValue('GroupBy', 'Branch');
                break;
            case 'Branch':
                this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('BranchNumber'));
                this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('BranchName') || this.utils.getBranchTextOnly(this.getControlValue('BranchNumber').trim()));
                this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
                this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
                this.setControlValue('SupervisorEmployeeCode', this.riExchange.getParentHTMLValue('SupervisorEmployeeCode'));
                this.setControlValue('SupervisorSurname', this.riExchange.getParentHTMLValue('SupervisorSurname'));
                this.setControlValue('GroupBy', this.riExchange.getParentHTMLValue('GroupBy'));
                this.disableControl('GroupBy', true);
                break;
            case 'Region':
                if (!this.isReturning()) {
                    this.riExchange.getParentHTMLValue('GroupBy');
                }
                this.prodService.view = [
                    { value: 'Branch', text: 'Branch' },
                    { value: 'Employee', text: 'Employee' }
                ];
                this.setControlValue('RegionCode', this.riExchange.getParentHTMLValue('RegionCode'));
                this.setControlValue('RegionDesc', this.riExchange.getParentHTMLValue('RegionDesc'));
                break;
        }
        this.actualShowType = this.riExchange.getParentHTMLValue('ShowType');
        this.reportNumber = this.riExchange.getParentHTMLValue('ReportNumber');
        this.rowid = this.riExchange.getParentHTMLValue('RowID');
        this.onRiGridRefresh();
    }

    private getSysChars(): void {
        let syscharList: Array<number> = [
            this.syscharConstants.SystemCharAllowProductiveTimeAdjustment,
            this.syscharConstants.SystemCharEnableInitialCharge
        ];
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                let list: Array<any> = data.records;
                this.pageParams.allowProductiveTimeAdjustment = list[0].Required;
                this.pageParams.enableInitialCharge = list[1].Required;
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        this.riGrid.AddColumn('GroupCode', 'Productivity', 'GroupCode', MntConst.eTypeCode, 6, true);
        this.riGrid.AddColumnAlign('GroupCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('GroupDesc', 'Productivity', 'GroupDesc', MntConst.eTypeText, 10);
        if (this.actualShowType !== 'T') {
            this.riGrid.AddColumn('AvailableHours', 'Productivity', 'AvailableHours', MntConst.eTypeDecimal2, 6);
            this.riGrid.AddColumn('ProductiveHours', 'Productivity', 'ProductiveHours', MntConst.eTypeDecimal2, 6);

            if (this.pageParams.allowProductiveTimeAdjustment) {
                this.riGrid.AddColumn('AdjProductiveHours', 'Productivity', 'AdjProductiveHours', MntConst.eTypeDecimal2, 6);
                this.riGrid.AddColumn('TotalProductiveHours', 'Productivity', 'TotalProductiveHours', MntConst.eTypeDecimal2, 6);
            }
            this.riGrid.AddColumn('AverageProductiveHours', 'Productivity', 'AverageProductiveHours', MntConst.eTypeDecimal2, 6);
            this.riGrid.AddColumn('ProductivePercentage', 'Productivity', 'ProductivePercentage', MntConst.eTypeDecimal2, 6);

            this.riGrid.AddColumn('OvertimeHours', 'Productivity', 'OvertimeHours', MntConst.eTypeDecimal2, 6);
            this.riGrid.AddColumn('TotalHours', 'Productivity', 'TotalHours', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumn('ContractVisitValue', 'Productivity', 'ContractVisitValue', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumn('JobVisitValue', 'Productivity', 'JobVisitValue', MntConst.eTypeDecimal2, 10);
            if (this.pageParams.enableInitialCharge) {
                this.riGrid.AddColumn('InitialChargeValue', 'Productivity', 'InitialChargeValue', MntConst.eTypeDecimal2, 10);
            }
            this.riGrid.AddColumn('AdditionalProductivity', 'Productivity', 'AdditionalProductivity', MntConst.eTypeDecimal2, 6);
            this.riGrid.AddColumn('TotalProductivity', 'Productivity', 'TotalProductivity', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumn('ProductivityPerHour', 'Productivity', 'ProductivityPerHour', MntConst.eTypeDecimal2, 5);
        } else {
            this.riGrid.AddColumn('ContractRoutines', 'Productivity', 'ContractRoutines', MntConst.eTypeInteger, 6);
            this.riGrid.AddColumn('ContractOthers', 'Productivity', 'ContractOthers', MntConst.eTypeInteger, 6);

            this.riGrid.AddColumn('JobRoutines', 'Productivity', 'JobRoutines', MntConst.eTypeInteger, 6);
            this.riGrid.AddColumn('JobOthers', 'Productivity', 'JobOthers', MntConst.eTypeInteger, 6);

            this.riGrid.AddColumn('TotalRoutines', 'Productivity', 'TotalRoutines', MntConst.eTypeInteger, 6);
        }
        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (this.getControlValue('DateFrom') && this.getControlValue('DateTo')) {
            let gridSearch: QueryParams = this.getURLSearchParamObject();
            gridSearch.set(this.serviceConstants.Action, this.gridConfig.actionGrid.toString());
            let form: any = {};
            form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
            form['Level'] = this.pageType;
            form['ReportNumber'] = ''; //this.reportNumber;
            form['RowID'] = this.rowid;
            form['AddInAdditionalProductivity'] = 'YES';
            form['ShowType'] = this.actualShowType;
            form['GroupBy'] = this.getControlValue('GroupBy');
            if (this.pageType === 'Branch') {
                form['BranchNumber'] = this.getControlValue('BranchNumber');
                form['ContractNumber'] = this.getControlValue('ContractNumber');
                form['DateFrom'] = this.getControlValue('DateFrom');
                form['DateTo'] = this.getControlValue('DateTo');
                form['SupervisorEmployeeCode'] = this.getControlValue('SupervisorEmployeeCode');
            } else if (this.pageType === 'Region') {
                form['Function'] = 'Productivity';
                form['LanguageCode'] = this.riExchange.LanguageCode();
                form['RegionCode'] = this.getControlValue('RegionCode');
            }
            form[this.serviceConstants.GridMode] = '0';
            form[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            form[this.serviceConstants.PageSize] = this.gridConfig.pageSize;
            form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1;
            form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            form['riSortOrder'] = 'Descending';
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'Service/iCABSSeProductivity' + this.pageType + 'PESGrid', gridSearch, form).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.displayMessage(data);
                } else {
                    this.riGrid.RefreshRequired();
                    this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                    this.riGrid.Execute(data);
                    setTimeout(() => {
                        this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                    }, 100);
                }
            }).catch((error) => {
                this.displayMessage(error);
            });
        }
    }

    public onGridBodyDoubleClick(): void {
        if (this.riGrid.CurrentCell === 0 && this.riGrid.Details.GetAttribute('GroupCode', 'rowid') !== 'TOTAL') {
            let params: any = {
                'ShowType': this.getControlValue('ShowType'),
                'DateFrom': this.getControlValue('DateFrom'),
                'DateTo': this.getControlValue('DateTo'),
                'ReportNumber': '',
                'RowID': this.riExchange.getParentHTMLValue('RowID'),
                'BranchNumber': this.riGrid.Details.GetAttribute('GroupCode', 'rowid'),
                'pageType': 'Branch'
            };
            switch (this.getControlValue('GroupBy')) {
                case 'Branch':
                    switch (this.pageType) {
                        case 'Business':
                        case 'Region':
                            params['GroupBy'] = 'Employee';
                            this.navigate(params.pageType, BIReportsRoutes.ICABSSEPRODUCTIVITYBRANCHPESGRID, params);
                            break;
                    }
                    break;
                case 'Region':
                    this.displayMessage(MessageConstant.Message.PageNotCovered, CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                    break;
                case 'Employee':
                    params['BranchNumber'] = this.pageType === 'Region' ? this.riGrid.Details.GetAttribute('GroupCode', 'additionalproperty').split('|')[1] : this.getControlValue('BranchNumber');
                    params['BranchName'] = this.utils.getBranchTextOnly(this.getControlValue('BranchNumber').trim());
                    params['EmployeeCode'] = this.riGrid.Details.GetValue('GroupCode');
                    params['DateFrom'] = this.getControlValue('DateFrom');
                    params['DateTo'] = this.getControlValue('DateTo');
                    params['GroupBy'] = this.getControlValue('GroupBy');
                    this.navigate('', BIReportsRoutes.ICABSSEPRODUCTIVITYPESDETAIL, params);
                    break;
            }
        }
    }

    public onRiGridRefresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
        this.populateGrid();
    }

    public resetValues(): void {
        this.pageParams.gridCurrentPage = 1;
        this.gridConfig.totalRecords = 1;
        this.riGrid.Clear();
        this.riGrid.RefreshRequired();
    }
}
