import { Component, OnInit, Injector, AfterContentInit, ViewChild } from '@angular/core';

import { BIReportsRoutes } from '@base/PageRoutes';
import { CommonLookUpUtilsService } from './../../../../../shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { ProductivityDropDownService } from './productivityDropDown.service';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSSeProductivityPESDetail.html',
    providers: [CommonLookUpUtilsService, ProductivityDropDownService]
})

export class ProductivityPESDetailComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService, public prodService: ProductivityDropDownService, private syscharConstants: SysCharConstants) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPRODUCTIVITYPESGRID;
        this.browserTitle = this.pageTitle = 'Service Productivity Detail';
    }

    private actualShowType: string;
    public gridConfig: any = {
        pageSize: 10,
        totalRecords: 1,
        actionGrid: '2',
        riSortOrder: ''
    };
    public controls: any = [
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'EmployeeCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'ShowType', type: MntConst.eTypeText, disabled: true },
        { name: 'DateFrom', type: MntConst.eTypeDate, disabled: true },
        { name: 'DateTo', type: MntConst.eTypeDate, disabled: true },
        { name: 'GroupBy', type: MntConst.eTypeText, disabled: true }
    ];
    public pageId: string;
    public pageType: string;

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.pageType = this.riExchange.getParentHTMLValue('pageType');
        this.pageId = PageIdentifier.ICABSSEPRODUCTIVITYPESDETAIL;
        super.ngAfterContentInit();
        this.pageParams.gridCacheRefresh = true;
        this.onWindowLoad();
    }

    private onWindowLoad(): void {
        this.getSysChars();
        this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('BranchNumber'));
        this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('BranchName'));
        this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
        this.commonLookup.getEmployeeSurname(this.riExchange.getParentHTMLValue('EmployeeCode')).then((data) => {
            if (data[0][0])
                this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
        });
        this.setControlValue('ShowType', this.riExchange.getParentHTMLValue('ShowType'));
        this.setControlValue('GroupBy', this.riExchange.getParentHTMLValue('GroupBy'));
        this.setControlValue('DateFrom', this.riExchange.getParentHTMLValue('DateFrom'));
        this.setControlValue('DateTo', this.riExchange.getParentHTMLValue('DateTo'));
        this.actualShowType = this.riExchange.getParentHTMLValue('ShowType');
        this.pageParams.gridHandle = this.pageParams.gridHandle || this.utils.randomSixDigitString();
        this.onRiGridRefresh();
    }

    private getSysChars(): void {
        let syscharList: Array<number> = [
            this.syscharConstants.SystemCharAllowProductiveTimeAdjustment,
            this.syscharConstants.SystemCharEnableInitialCharge
        ];
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                let list: Array<any> = data.records;
                this.pageParams.allowProductiveTimeAdjustment = list[0].Required;
                this.pageParams.enableInitialCharge = list[1].Required;
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        this.riGrid.AddColumn('ServiceDateStart', 'Productivity', 'ServiceDateStart', MntConst.eTypeDate, 10, true);
        this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);
        if (this.actualShowType !== 'T') {
            this.riGrid.AddColumn('AvailableHours', 'Productivity', 'AvailableHours', MntConst.eTypeDecimal2, 6);
            this.riGrid.AddColumn('ProductiveHours', 'Productivity', 'ProductiveHours', MntConst.eTypeDecimal2, 6);

            if (this.pageParams.allowProductiveTimeAdjustment) {
                this.riGrid.AddColumn('AdjProductiveHours', 'Productivity', 'AdjProductiveHours', MntConst.eTypeDecimal2, 6);
                this.riGrid.AddColumn('TotalProductiveHours', 'Productivity', 'TotalProductiveHours', MntConst.eTypeDecimal2, 6);
            }
            this.riGrid.AddColumn('AverageProductiveHours', 'Productivity', 'AverageProductiveHours', MntConst.eTypeDecimal2, 6);
            this.riGrid.AddColumn('ProductivePercentage', 'Productivity', 'ProductivePercentage', MntConst.eTypeDecimal2, 6);

            this.riGrid.AddColumn('OvertimeHours', 'Productivity', 'OvertimeHours', MntConst.eTypeDecimal2, 6);
            this.riGrid.AddColumn('TotalHours', 'Productivity', 'TotalHours', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumn('ContractVisitValue', 'Productivity', 'ContractVisitValue', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumn('JobVisitValue', 'Productivity', 'JobVisitValue', MntConst.eTypeDecimal2, 10);
            if (this.pageParams.enableInitialCharge) {
                this.riGrid.AddColumn('InitialChargeValue', 'Productivity', 'InitialChargeValue', MntConst.eTypeDecimal2, 10);
            }
            this.riGrid.AddColumn('AdditionalProductivity', 'Productivity', 'AdditionalProductivity', MntConst.eTypeDecimal2, 6);
            this.riGrid.AddColumn('TotalProductivity', 'Productivity', 'TotalProductivity', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumn('ProductivityPerHour', 'Productivity', 'ProductivityPerHour', MntConst.eTypeDecimal2, 5);
        } else {
            this.riGrid.AddColumn('ContractRoutines', 'Productivity', 'ContractRoutines', MntConst.eTypeInteger, 6);
            this.riGrid.AddColumn('ContractOthers', 'Productivity', 'ContractOthers', MntConst.eTypeInteger, 6);

            this.riGrid.AddColumn('JobRoutines', 'Productivity', 'JobRoutines', MntConst.eTypeInteger, 6);
            this.riGrid.AddColumn('JobOthers', 'Productivity', 'JobOthers', MntConst.eTypeInteger, 6);

            this.riGrid.AddColumn('TotalRoutines', 'Productivity', 'TotalRoutines', MntConst.eTypeInteger, 6);
        }
        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.actionGrid.toString());
        let form: any = {};
        form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        form['Function'] = 'Productivity';
        form['Level'] = 'BranchDetail';
        form['LanguageCode'] = this.riExchange.LanguageCode();
        form['BranchNumber'] = this.riExchange.getParentHTMLValue('BranchNumber');
        form['DateFrom'] = this.riExchange.getParentHTMLValue('DateFrom');
        form['DateTo'] = this.riExchange.getParentHTMLValue('DateTo');
        form['EmployeeCode'] = this.riExchange.getParentHTMLValue('EmployeeCode');
        form['ShowType'] = this.actualShowType;
        form['GroupBy'] = this.riExchange.getParentHTMLValue('GroupBy');
        if (this.riExchange.getParentHTMLValue('BusEmployee')) {
            form['BusEmployee'] = 'Employee';
        }
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        form[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        form[this.serviceConstants.PageSize] = this.gridConfig.pageSize;
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form['riSortOrder'] = 'Descending';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'Service/iCABSSeProductivityPESGrid', gridSearch, form).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                }, 100);
            }
        }).catch((error) => {
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
        this.populateGrid();
    }

    public onGridBodyDoubleClick(): void {
        let params: any = {
            'BranchNumber': this.getControlValue('BranchNumber'),
            'BranchName': this.getControlValue('BranchName'),
            'ShowType': this.getControlValue('ShowType'),
            'GroupBy': this.getControlValue('GroupBy'),
            'Date': this.riGrid.Details.GetValue('ServiceDateStart'),
            'EmployeeCode': this.getControlValue('EmployeeCode'),
            'EmployeeSurname': this.getControlValue('EmployeeSurname')
        };
        this.navigate('', BIReportsRoutes.ICABSSEPRODUCTIVITYSERVICEVISITSPESGRID, params);
    }

    public getCurrentPage(currentPage: any): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridCacheRefresh = false;
            super.getCurrentPage(currentPage);
        }
    }
}
