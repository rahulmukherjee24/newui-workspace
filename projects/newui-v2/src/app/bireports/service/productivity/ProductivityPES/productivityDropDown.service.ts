
import { Injectable } from '@angular/core';

@Injectable()
export class ProductivityDropDownService {
    constructor() { }

    public show: any = [
        { value: 'All', text: 'All' },
        { value: 'C', text: 'Contracts' },
        { value: 'J', text: 'Jobs' },
        { value: 'T', text: 'Count' }
    ];

    public view: any = [
        { value: 'Employee', text: 'Employee' },
        { value: 'Supervisor', text: 'Supervisor' }
    ];

    public groupbyBusiness: any = [
        { value: 'Branch', text: 'Branch' },
        { value: 'Region', text: 'Region' },
        { value: 'Product', text: 'Product' }
    ];

    public groupbyBranch: any = [
        { value: 'BranchServiceArea', text: 'Service Area' },
        { value: 'Product', text: 'Product' }
    ];

    public groupbyRegion: any = [
        { value: 'Branch', text: 'Branch' },
        { value: 'Product', text: 'Product' }
    ];

    public viewbyBranchDetail: any = [
        { value: 'Product', text: 'Product' },
        { value: 'Date', text: 'Date' }
    ];

    public showType: any = [
        { value: 'All', text: 'All' },
        { value: 'Contract', text: 'Contracts' },
        { value: 'Job', text: 'Jobs' },
        { value: 'ProductSale', text: 'Product Sales' }
    ];
}
