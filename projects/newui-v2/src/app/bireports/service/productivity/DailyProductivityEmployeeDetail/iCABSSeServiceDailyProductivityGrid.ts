import { AfterContentInit, Component, OnInit, Injector, ViewChild } from '@angular/core';

import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { ContractManagementModuleRoutes, InternalGridSearchSalesModuleRoutes } from '@base/PageRoutes';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

import { IExportOptions } from '@base/ExportConfig';
import { CommonGridFunction } from '@app/base/CommonGridFunction';

@Component({
    templateUrl: 'iCABSSeServiceDailyProductivityGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class ServiceDailyProductivityGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public alertMessage: any;

    public employeeSearchComponent = EmployeeSearchComponent;
    public pageId: string;
    public messageType: string;

    public ellipsisConfig: any = {
        employeeSearchParams: {
            parentMode: 'LookUp-Service'
        }
    };

    public controls: Array<Object> = [
        { name: 'EmployeeCode', type: MntConst.eTypeText },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        {
            name: 'DateFrom', type: MntConst.eTypeDate,
            value: this.globalize.parseDateToFixedFormat(new Date(new Date().getFullYear(), new Date().getMonth(), 1)),
            required: true
        },
        { name: 'DateTo', type: MntConst.eTypeDate, value: this.globalize.parseDateToFixedFormat(new Date()), required: true },
        { name: 'TotalWorkTime', type: MntConst.eTypeTime, disabled: true },
        { name: 'TotalProductiveTime', type: MntConst.eTypeTime, disabled: true },
        { name: 'TotalTravelTime', type: MntConst.eTypeTime, disabled: true },
        { name: 'TotalProductivity', type: MntConst.eTypeDecimal2, disabled: true },
        { name: 'TotalWorkedTime', type: MntConst.eTypeTime, disabled: true },
        { name: 'ProductivityPerHour', disabled: true, type: MntConst.eTypeDecimal2 },
        { name: 'PageSize', type: MntConst.eTypeInteger, value: 25, required: true },
        { name: 'DayStartTime', type: MntConst.eTypeTime, disabled: true },
        { name: 'DayEndTime', type: MntConst.eTypeTime, disabled: true },
        { name: 'TotalDrivingTime', type: MntConst.eTypeTime, disabled: true },
        { name: 'DayStartMileage', type: MntConst.eTypeInteger, disabled: true },
        { name: 'DayEndMileage', type: MntConst.eTypeInteger, disabled: true },
        { name: 'TotalMileage', type: MntConst.eTypeInteger, disabled: true },
        { name: 'AverageSpeed', type: MntConst.eTypeDecimal1, disabled: true },
        { name: 'TotalUnexplainedTime', type: MntConst.eTypeTime, disabled: true },
        { name: 'TotalRevenue', type: MntConst.eTypeDecimal2, disabled: true },
        { name: 'RevenuePerHourOnSite', type: MntConst.eTypeDecimal2, disabled: true },
        { name: 'RevenuePerHour', type: MntConst.eTypeDecimal2, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeCode, disabled: true, required: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode, disabled: true, required: true }
    ];
    public exportConfig: IExportOptions = {};
    public enablePerHour: boolean = true;
    public commonGridFunction: CommonGridFunction;

    constructor(injector: Injector, private lookupUtils: CommonLookUpUtilsService) {
        super(injector);
        this.browserTitle = this.pageTitle = 'Daily Productivity';
        this.commonGridFunction = new CommonGridFunction(this);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.enablePerHour = this.riExchange.getParentHTMLValue('enablePerHour') ? true : false;
        this.pageId = this.enablePerHour ? PageIdentifier.ICABSSESERVICEDAILYPRODUCTIVITYGRIDFR : PageIdentifier.ICABSSESERVICEDAILYPRODUCTIVITYGRID;
        super.ngAfterContentInit();
        this.onWindowLoad();
    }

    //#region private methods

    private onWindowLoad(): void {
        if (this.isReturning()) {
            this.riGrid.RefreshRequired();
            this.buildGrid();
            this.commonGridFunction.onRiGridRefresh();
        } else {
            this.pageParams.gridConfig = {
                totalItem: 1,
                gridHandle: this.utils.randomSixDigitString(),
                gridCacheRefresh: true,
                action: '2',
                riSortOrder: '',
                itemsPerPage: 25
            };
            this.pageParams.gridCurrentPage = 1;
            if (this.parentMode && this.parentMode.toLowerCase() === 'summary') {
                this.riExchange.getParentHTMLValue('BusinessCode');
                this.riExchange.getParentHTMLValue('BranchNumber');
                this.setControlValue('EmployeeCode', this.riExchange.getParentAttributeValue('GroupCode'));
                this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('GroupDesc'));
                this.setControlValue('DateFrom', this.riExchange.getParentAttributeValue('DateFrom'));
                this.setControlValue('DateTo', this.riExchange.getParentAttributeValue('DateTo'));
                this.buildGrid();
                this.populateGrid();
            } else {
                this.setControlValue('BusinessCode', this.utils.getBusinessCode());
                this.setControlValue('BranchNumber', this.utils.getBranchCode());
                this.buildGrid();
            }
        }
        if (this.parentMode && this.parentMode.toLowerCase() === 'summary') {
            this.disableControl('EmployeeCode', true);
            if (!this.enablePerHour) {
                this.disableControl('DateFrom', true);
                this.disableControl('DateTo', true);
            }
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        /* The following AddColumn records are only for those columns that will be shown on screen. Export-Only columns are not included*/
        if (this.getControlValue('DateFrom') && this.getControlValue('DateTo')) {
            if (this.enablePerHour) {
                this.riGrid.AddColumn('EmployeeCode', 'Activity', 'EmployeeCode', MntConst.eTypeCode, 6);
                this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);

                if (this.getControlValue('DateFrom') !== this.getControlValue('DateTo')) {
                    this.riGrid.AddColumn('ServiceDateStart', 'Activity', 'ServiceDateStart', MntConst.eTypeDate, 10);
                    this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);
                }

                this.riGrid.AddColumn('KeyedStartTime', 'Activity', 'KeyedStartTime', MntConst.eTypeTime, 6);
                this.riGrid.AddColumnAlign('KeyedStartTime', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('KeyedEndTime', 'Activity', 'KeyedEndTime', MntConst.eTypeTime, 6);
                this.riGrid.AddColumnAlign('KeyedEndTime', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('ActivityDesc', 'Activity', 'ActivityDesc', MntConst.eTypeText, 15);

                this.riGrid.AddColumn('ContractNumber', 'Activity', 'ContractNumber', MntConst.eTypeCode, 8);
                this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('PremiseNumber', 'Activity', 'PremiseNumber', MntConst.eTypeInteger, 5);
                this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('PremiseName', 'Activity', 'PremiseName', MntConst.eTypeText, 15);

                this.riGrid.AddColumn('VisitTypeCode', 'Activity', 'VisitTypeCode', MntConst.eTypeCode, 2);
                this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('ProductCode', 'Activity', 'ProductCode', MntConst.eTypeCode, 10);
                this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('Duration', 'Activity', 'Duration', MntConst.eTypeText, 6);
                this.riGrid.AddColumnAlign('Duration', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('TravelTime', 'Activity', 'TravelTime', MntConst.eTypeText, 6);
                this.riGrid.AddColumnAlign('TravelTime', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('Productivity', 'Activity', 'Productivity', MntConst.eTypeDecimal2, 10);

                this.riGrid.AddColumn('Mileage', 'Activity', 'Mileage', MntConst.eTypeInteger, 5);
                this.riGrid.Complete();

                this.exportConfig = {};
                this.dateCollist = this.riGrid.getColumnIndexList([
                    'ServiceDateStart'
                ]);
                this.durationCollist = this.riGrid.getColumnIndexList([
                    'KeyedStartTime',
                    'KeyedEndTime',
                    'Duration',
                    'TravelTime'
                ]);
                if (this.dateCollist.length) {
                    this.exportConfig.dateColumns = this.dateCollist;
                }
                if (this.durationCollist.length) {
                    this.exportConfig.durationColumns = this.durationCollist;
                }
            } else {
                if (!this.getControlValue('EmployeeCode')) {
                    this.riGrid.AddColumn('EmployeeCode', 'Activity', 'EmployeeCode', MntConst.eTypeCode, 6);
                    this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
                }

                if (this.getControlValue('DateFrom') !== this.getControlValue('DateTo')) {
                    this.riGrid.AddColumn('ServiceDateStart', 'Activity', 'ServiceDateStart', MntConst.eTypeDate, 10);
                    this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);
                }

                this.riGrid.AddColumn('KeyedStartTime', 'Activity', 'KeyedStartTime', MntConst.eTypeTime, 6);
                this.riGrid.AddColumnAlign('KeyedStartTime', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('KeyedEndTime', 'Activity', 'KeyedEndTime', MntConst.eTypeTime, 6);
                this.riGrid.AddColumnAlign('KeyedEndTime', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('ActivityDesc', 'Activity', 'ActivityDesc', MntConst.eTypeText, 15);

                this.riGrid.AddColumn('ContractNumber', 'Activity', 'ContractNumber', MntConst.eTypeCode, 8);
                this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('PremiseNumber', 'Activity', 'PremiseNumber', MntConst.eTypeInteger, 5);
                this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('PremiseName', 'Activity', 'PremiseName', MntConst.eTypeText, 15);

                this.riGrid.AddColumn('VisitTypeCode', 'Activity', 'VisitTypeCode', MntConst.eTypeCode, 2);
                this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('ProductCode', 'Activity', 'ProductCode', MntConst.eTypeCode, 10);
                this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('ServicedQuantity', 'Activity', 'ServicedQuantity', MntConst.eTypeInteger, 3);
                this.riGrid.AddColumnAlign('ServicedQuantity', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('Duration', 'Activity', 'Duration', MntConst.eTypeText, 6);
                this.riGrid.AddColumnAlign('Duration', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('ServicingValue', 'Activity', 'ServicingValue', MntConst.eTypeDecimal2, 10);

                this.riGrid.AddColumn('ProductValue', 'Activity', 'ProductValue', MntConst.eTypeDecimal2, 10);

                this.riGrid.AddColumn('DrivingCost', 'Activity', 'DrivingCost', MntConst.eTypeDecimal2, 10);

                this.riGrid.AddColumn('Mileage', 'Activity', 'Mileage', MntConst.eTypeInteger, 5);

                this.riGrid.Complete();
            }
        }
    }

    private populateGrid(): void {
        if (this.getControlValue('DateFrom') && this.getControlValue('DateTo') && this.getControlValue('PageSize')) {
            const operation = this.enablePerHour ? 'Service/iCABSServiceDailyProductivityGridFR' : 'Service/iCABSSeServiceDailyProductivityGrid';
            let gridSearch: QueryParams = this.getURLSearchParamObject();
            gridSearch.set(this.serviceConstants.Action, this.pageParams.gridConfig.action);

            let form: any = {};
            form['Level'] = 'Detail';
            form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
            form['EmployeeCode'] = this.getControlValue('EmployeeCode');
            form['DateFrom'] = this.getControlValue('DateFrom');
            form['DateTo'] = this.getControlValue('DateTo');
            form['BranchNumber'] = this.riExchange.URLParameterContains('Employee') ? 0 : this.getControlValue('BranchNumber');
            form[this.serviceConstants.GridMode] = '0';
            form[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
            form[this.serviceConstants.GridCacheRefresh] = true;
            form[this.serviceConstants.PageSize] = this.getControlValue('PageSize');
            form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
            form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            form['riSortOrder'] = this.riGrid.SortOrder;
            if (!this.enablePerHour) {
                form[this.serviceConstants.MethodType] = 'grid';
            }
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', operation, gridSearch, form).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.commonGridFunction.setPageData(data, this.pageParams.gridConfig);
                if (!this.hasError(data)) {
                    setTimeout(() => {
                        this.afterExecute(data);
                    }, 100);
                }
            }).catch((error) => {
                this.displayMessage(error);
            });
        }
    }

    private afterExecute(data: any): void {
        //the variable totalsInfoRow calculation is : number of rows * total number of columns, followed by a split on basis of delimiter
        let totalsInfoRows: any = data.body.cells[(this.riGrid.getAllRowsInGrid().length - 1) * (data.header.cells.length)].additionalData.split('|');
        if (this.enablePerHour) {
            this.setControlValue('TotalWorkTime', totalsInfoRows[0]);
            this.setControlValue('TotalProductiveTime', totalsInfoRows[1]);
            this.setControlValue('TotalTravelTime', totalsInfoRows[2]);
            this.setControlValue('TotalProductivity', totalsInfoRows[3]);
            this.setControlValue('TotalWorkedTime', totalsInfoRows[4]);
            this.setControlValue('ProductivityPerHour', parseFloat(totalsInfoRows[5]).toFixed(2));
        } else {
            this.setControlValue('DayStartTime', totalsInfoRows[0]);
            this.setControlValue('DayEndTime', totalsInfoRows[1]);
            this.setControlValue('TotalWorkedTime', totalsInfoRows[2]);
            this.setControlValue('TotalDrivingTime', totalsInfoRows[3]);
            this.setControlValue('TotalProductiveTime', totalsInfoRows[4]);
            this.setControlValue('TotalUnexplainedTime', totalsInfoRows[5]);
            this.setControlValue('DayStartMileage', totalsInfoRows[6]);
            this.setControlValue('DayEndMileage', totalsInfoRows[7]);
            this.setControlValue('TotalMileage', totalsInfoRows[8]);
            this.setControlValue('AverageSpeed', parseFloat(totalsInfoRows[9]).toFixed(1));
            this.setControlValue('TotalRevenue', parseFloat(totalsInfoRows[10]).toFixed(2));
            this.setControlValue('RevenuePerHourOnSite', parseFloat(totalsInfoRows[11]).toFixed(2));
            this.setControlValue('RevenuePerHour', parseFloat(totalsInfoRows[12]).toFixed(2));
        }
    }

    //#endregion

    //#region public Methods

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    public onGridBodyDoubleClick(): void {
        let currContractUrlParam: any = this.riGrid.Details.GetAttribute('KeyedEndTime', 'AdditionalProperty');
        if (currContractUrlParam) {
            let params: any = {
                'ContractNumber': this.riGrid.Details.GetValue('ContractNumber'),
                'CurrentContractTypeURLParameter': currContractUrlParam,
                'currentContractType': currContractUrlParam
            };
            switch (this.riGrid.CurrentColumnName) {
                case 'ContractNumber':
                    if (currContractUrlParam.toUpperCase() === 'C') {
                        this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, params);
                    } else if (currContractUrlParam.toUpperCase() === 'J') {
                        this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE, params);
                    } else if (currContractUrlParam.toUpperCase() === 'P') {
                        this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE, params);
                    }
                    break;
                case 'PremiseNumber':
                    params['PremiseNumber'] = this.riGrid.Details.GetValue('PremiseNumber');
                    params['PremiseRowID'] = this.riGrid.Details.GetAttribute('PremiseNumber', 'AdditionalProperty');
                    params['ContractTypeCode'] = currContractUrlParam;
                    this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, params);
                    break;
                case 'ProductCode':
                    params['PremiseNumber'] = this.riGrid.Details.GetValue('PremiseNumber');
                    params['ProductCode'] = this.riGrid.Details.GetValue('ProductCode');
                    params['ServiceCoverRowID'] = this.riGrid.Details.GetAttribute('ProductCode', 'AdditionalProperty');
                    if (currContractUrlParam.toUpperCase() === 'P') {
                        this.navigate('ServicePlanning', InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE, params);
                    } else {
                        this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, params);
                    }
                    break;
            }
        }
    }

    public resetValues(): void {
        const controls = [
            'TotalWorkTime', 'TotalProductiveTime', 'TotalTravelTime', 'TotalProductivity',
            'TotalWorkedTime', 'ProductivityPerHour', 'DayStartTime', 'DayEndTime',
            'TotalDrivingTime', 'DayStartMileage', 'DayEndMileage', 'TotalMileage',
            'AverageSpeed', 'TotalUnexplainedTime', 'TotalRevenue', 'RevenuePerHourOnSite',
            'RevenuePerHour'
        ];
        controls.forEach((control) => {
            this.setControlValue(control, '');
        });
        this.commonGridFunction.resetGrid();
    }

    public onEmployeeSelect(employeeCode: any): void {
        if (employeeCode) {
            this.setControlValue('EmployeeCode', employeeCode['EmployeeCode']);
            this.setControlValue('EmployeeSurname', employeeCode['EmployeeSurname']);
        }
        this.resetValues();
    }

    public onChangeEmployeeCode(): void {
        this.setControlValue('EmployeeSurname', '');
        this.lookupUtils.getEmployeeSurname(this.getControlValue('EmployeeCode')).then((data) => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
            }
        });
        this.resetValues();
    }
    //#endregion
}
