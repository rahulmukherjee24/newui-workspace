import { Component, Injector, OnInit, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';

import { CommonGridFunction } from '@base/CommonGridFunction';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { LookUpData } from '@shared/services/lookup';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { ModalAdvService } from '@shared/components/modal-adv/modal-adv.service';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { StaticUtils } from '@shared/services/static.utility';
import { ExportConfig } from '@app/base/ExportConfig';
import { HttpConstants } from '@app/base/HttpConstants';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';

import * as moment from 'moment';

import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { ISelectedProperty } from '../selection-screens/iCABSCMTicketAnalysisSelect.component';
import { SelectedValueHandler } from './../selection-screens/iCABSCMTicketAnalysisSelect.component';
import { FilterCleanUpHandlerService } from './../selection-screens/FilterCleanUpHandlerService';

enum EXHRParams {
    c_s_METHOD = 'bi/reports',
    c_s_MODULE = 'reports',
    c_s_OPERATION = 'ContactManagement/iCABSCMTicketAnalysisGeneration',
    c_s_FILEPATH = 'FilePath',
    c_s_CONTACTTYPE = 'ContactType',
    c_s_CONTACTTYPEDETAIL = 'ContactTypeDetail',
    c_s_DATEFROM = 'DateFrom',
    c_s_DATETO = 'DateTo',
    c_s_REPORTTYPETICKET = 'ReportTypeTicket',
    c_s_REPORTTYPEROOTCAUSE = 'ReportTypeRootCause',
    c_s_SUBMITBATCHPROCESS = 'SubmitBatchProcess',
    c_s_REPORTNAME = 'ReportName',
    c_s_EXTRACRITERIA = 'ExtraCriteria',
    c_s_REPORTNUMBER = 'ReportNumber',
    c_s_EXPORTTYPE = 'ExportType'
}

@Component({
    templateUrl: 'iCABSCMTicketAnalysisGeneration.html'
})
export class TicketAnalysisGenerationComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private readonly c_s_REPORT_TYPE_TICKET: string = 'ReportTypeTicket';

    protected pageId: string;
    protected controls: IControls[] = [
        { name: 'ReportType', value: 'ReportTypeTicket' },
        { name: 'ContactType', value: 'all' },
        { name: 'ContactTypeDetail', value: 'all' },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate },
        { name: 'ExportThreshold', type: MntConst.eTypeInteger, disabled: true },
        { name: 'ReportName' },
        { name: 'ExtraCriteria' }
    ];

    public readonly gridConfig: Record<string, number> = {
        pageSize: 10,
        totalRecords: 1
    };
    public commonGrid: CommonGridFunction;
    public reportMessage: string = '';
    public selectedMainTab: number = 0;
    public selectedSubTab: number = 0;
    public additionalFilters: SelectedValueHandler = new SelectedValueHandler();
    public readonly c_s_WARNING: string = MessageConstant.Message.BatchFinishWaitWarning;

    constructor(injector: Injector,
        private cleanup: FilterCleanUpHandlerService,
        private modalService: ModalAdvService) {
        super(injector);

        this.pageId = PageIdentifier.ICABSCMTICKETANALYSISGENERATION;
        this.browserTitle = this.pageTitle = 'Ticket Analysis Report';

        this.commonGrid = new CommonGridFunction(this);
    }

    // #region LifeCycle Hooks
    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();

        this.uiForm.controls['ExportThreshold'].setValidators(Validators.max(10000));
        this.buildGrid();

        if (!this.isReturning()) {
            this.pageParams.contactTypes = [];
            this.pageParams.contactTypeDetails = [{
                value: 'all',
                text: 'All'
            }];
            this.pageParams.gridCurrentPage = 1;
            this.fetchDataForForms();
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    // #endregion

    // #region Private Methods
    private fetchDataForForms(): void {
        let query: Array<LookUpData> = [{
            table: 'ContactType',
            fields: ['ContactTypeCode', 'ContactTypeSystemDesc'],
            query: {}
        }, {
            table: 'riRegistry',
            fields: ['RegValue'],
            query: {
                'RegSection': 'CCM Ticket Analysis',
                'RegKey': this.businessCode() + '_ExportThreshold'
            }
        }, {
            'table': 'Business',
            'query': {
                'BusinessCode': this.businessCode()
            },
            'fields': ['SalesTradingYear', 'SalesTradingMonth']
        }, {
            'table': 'ContactTypeLang',
            'query': {
                'LanguageCode': this.riExchange.LanguageCode(),
                'BusinessCode': this.businessCode()
            },
            'fields': ['ContactTypeCode', 'ContactTypeDesc']
        }];

        this.LookUp.lookUpPromise(query, 100).then(data => {
            if (!data) {
                this.displayMessage(MessageConstant.Message.RecordNotFound);
                return;
            }

            /**
             * @todo Remove This To Common Lookup
             */
            if (data[0] && data[0].length) {
                if (data[3] && data[3].length) {
                    this.pageParams.contactTypes = [];
                    let contactTypeDataset: any = data[0];
                    let contactTypeLangDataset: any = data[3];
                    if (data[0].length > 0) {
                        contactTypeDataset.forEach(idx => {
                            let filterData = contactTypeLangDataset.find(detailObj => (detailObj.ContactTypeCode === idx.ContactTypeCode));
                            if (filterData) {
                                this.pageParams.contactTypes.push({
                                    text: filterData.ContactTypeDesc ? filterData.ContactTypeDesc : idx.ContactTypeDesc,
                                    value: filterData.ContactTypeCode ? filterData.ContactTypeCode : idx.ContactTypeCode
                                });
                            }
                        });
                        this.utils.sortByKey(this.pageParams.contactTypes, 'text');
                    }
                }
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - ContactType');
            }

            let threshold: number = 2000;
            if (data[1] && data[1].length) {
                threshold = data[1][0].RegValue;
            }
            this.setControlValue('ExportThreshold', threshold);

            if (data[2] && data[2].length) {
                this.setDates(data[2][0]);
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Sales Trading');
            }

            this.populateGrid();
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    private setDates(trading: Record<string, number>): void {
        let year: number = trading.SalesTradingYear;
        let month: number = trading.SalesTradingMonth;

        if (month === 1) {
            let date: Array<Date> = StaticUtils.getFirstAndLastDayOfYear();

            date[0].setFullYear(date[0].getFullYear() - 1);
            date[1].setFullYear(date[1].getFullYear() - 1);

            this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(date[0]));
            this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(date[1]));
        } else {
            let date: Date = new Date(year, month - 2, 1);

            this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(date));

            date = moment(date).endOf('month').toDate();

            this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(date));
        }

    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('ReportNumber', 'CMTicketAnalysis', 'ReportNumber', MntConst.eTypeText, 8, true, 'Report Number');
        this.riGrid.AddColumnAlign('ReportNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ReportNumber', true);

        this.riGrid.AddColumn('ReportName', 'CMTicketAnalysis', 'ReportName', MntConst.eTypeText, 50, false, 'Report Name');
        this.riGrid.AddColumnAlign('ReportName', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ReportName', true);

        this.riGrid.AddColumn('GeneratedDate', 'CMTicketAnalysis', 'GeneratedDate', MntConst.eTypeDate, 10, false, 'Report Generated On');
        this.riGrid.AddColumnAlign('GeneratedDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('GeneratedDate', true);

        this.riGrid.AddColumn('GeneratedTime', 'CMTicketAnalysis', 'GeneratedTime', MntConst.eTypeText, 10, false, 'Report Generated At');
        this.riGrid.AddColumnAlign('GeneratedTime', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContactType', 'CMTicketAnalysis', 'ContactType', MntConst.eTypeText, 30, false, 'Contact Type');
        this.riGrid.AddColumnAlign('ContactType', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ContactType', true);

        this.riGrid.AddColumn('ContactTypeDetail', 'CMTicketAnalysis', 'ContactTypeDetail', MntConst.eTypeText, 30, false, 'Contact Type Detail');
        this.riGrid.AddColumnAlign('ContactTypeDetail', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('DateFrom', 'CMTicketAnalysis', 'DateFrom', MntConst.eTypeDate, 10, false, 'Date Range From');
        this.riGrid.AddColumnAlign('DateFrom', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('DateFrom', true);

        this.riGrid.AddColumn('DateTo', 'CMTicketAnalysis', 'DateTo', MntConst.eTypeDate, 10, false, 'Date Range To');
        this.riGrid.AddColumnAlign('DateTo', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ExtraCriteria', 'CMTicketAnalysis', 'ExtraCriteria', MntConst.eTypeTextFree, 30, false, 'Extra Criteria');
        this.riGrid.AddColumnAlign('ExtraCriteria', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Lines', 'CMTicketAnalysis', 'Lines', MntConst.eTypeText, 7, false, 'Lines');
        this.riGrid.AddColumnAlign('Lines', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Regenerated', 'CMTicketAnalysis', 'Regenerated', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('Regenerated', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let search: QueryParams = new QueryParams();
        let formData: Record<string, string | boolean | number> = {};
        let isTypeTicket: boolean = (this.c_s_REPORT_TYPE_TICKET === this.getControlValue('ReportType'));

        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }

        search.set(this.serviceConstants.CountryCode, this.countryCode());
        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.Level] = ''; // Required For API
        formData[EXHRParams.c_s_FILEPATH] = 'CMTicketAnalysisExtracts';
        formData[EXHRParams.c_s_CONTACTTYPE] = this.getControlValue(EXHRParams.c_s_CONTACTTYPE);
        formData[EXHRParams.c_s_CONTACTTYPEDETAIL] = this.getControlValue(EXHRParams.c_s_CONTACTTYPEDETAIL);
        formData[EXHRParams.c_s_DATEFROM] = this.getControlValue(EXHRParams.c_s_DATEFROM);
        formData[EXHRParams.c_s_DATETO] = this.getControlValue(EXHRParams.c_s_DATETO);
        formData[EXHRParams.c_s_REPORTTYPETICKET] = isTypeTicket;
        formData[EXHRParams.c_s_REPORTTYPEROOTCAUSE] = !isTypeTicket;
        formData[EXHRParams.c_s_SUBMITBATCHPROCESS] = 'No';

        formData[this.serviceConstants.GridMode] = 0;
        formData[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        formData[this.serviceConstants.GridCacheRefresh] = true;
        formData[this.serviceConstants.GridPageSize] = this.gridConfig.pageSize;
        formData[this.serviceConstants.GridPageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData[this.serviceConstants.GridSortOrder] = 'Descending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(EXHRParams.c_s_METHOD, EXHRParams.c_s_MODULE, EXHRParams.c_s_OPERATION, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                }, 100);
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }
    // #endregion

    // #region Public Methods
    public onContactTypeChange(): void {
        let search: QueryParams = new QueryParams();
        let contactType: string = this.getControlValue('ContactType');

        this.pageParams.contactTypeDetails = [];
        this.setControlValue('ContactTypeDetail', 'all');

        if (contactType === 'all') {
            return;
        }

        search.set(this.serviceConstants.CountryCode, this.countryCode());
        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set(this.serviceConstants.Action, '6');

        search.set(this.serviceConstants.Function, 'GetContactTypeDetailCodes');
        search.set('ContactType', this.getControlValue('ContactType'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrGet(EXHRParams.c_s_METHOD, EXHRParams.c_s_MODULE, EXHRParams.c_s_OPERATION, search).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                let codes: Array<string> = data.ContactTypeDetailCodes.split(StaticUtils.RI_SEPARATOR_VALUE_NEWLINE);
                let descs: Array<string> = data.ContactTypeDetailDescs.split(StaticUtils.RI_SEPARATOR_VALUE_NEWLINE);
                let options: Array<Record<string, string>> = [];

                codes.forEach((item, index) => {
                    let option: Record<string, string> = {
                        value: item,
                        text: descs[index]
                    };

                    options.push(option);
                });
                this.pageParams.contactTypeDetails = options;
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    public onGenerateReport(): void {
        let search: QueryParams = new QueryParams();
        let formData: Record<string, string | boolean | number> = {};
        let isTypeTicket: boolean = (this.c_s_REPORT_TYPE_TICKET === this.getControlValue('ReportType'));

        search.set(this.serviceConstants.CountryCode, this.countryCode());
        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.Function, 'SubmitBatchProcess');

        formData[EXHRParams.c_s_SUBMITBATCHPROCESS] = 'yes';
        formData[EXHRParams.c_s_REPORTNAME] = this.getControlValue(EXHRParams.c_s_REPORTNAME);
        formData[EXHRParams.c_s_DATEFROM] = this.getControlValue(EXHRParams.c_s_DATEFROM);
        formData[EXHRParams.c_s_DATETO] = this.getControlValue(EXHRParams.c_s_DATETO);
        formData[EXHRParams.c_s_REPORTTYPETICKET] = StaticUtils.convertCheckboxValueToRequestValue(isTypeTicket);
        formData[EXHRParams.c_s_REPORTTYPEROOTCAUSE] = StaticUtils.convertCheckboxValueToRequestValue(!isTypeTicket);
        formData[EXHRParams.c_s_CONTACTTYPE] = this.getControlValue(EXHRParams.c_s_CONTACTTYPE);
        formData[EXHRParams.c_s_CONTACTTYPEDETAIL] = this.getControlValue(EXHRParams.c_s_CONTACTTYPEDETAIL);
        formData[EXHRParams.c_s_FILEPATH] = 'CMTicketAnalysisExtracts';
        formData[EXHRParams.c_s_EXTRACRITERIA] = this.additionalFilters.list;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(EXHRParams.c_s_METHOD, EXHRParams.c_s_MODULE, EXHRParams.c_s_OPERATION, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                this.reportMessage = data.BatchProcessInformation || '';
                this.selectedMainTab = 0;
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    public onRefreshClick(): void {
        this.commonGrid.onRefreshClick();
    }

    public onHeaderClick(): void {
        this.onRefreshClick();
    }

    public onGridBodyDoubleClick(): void {
        if (this.riGrid.CurrentColumnName === 'Regenerated') {
            let msg: string = MessageConstant.PageSpecificMessage.regenerateConfirm.msg;
            msg += this.riGrid.Details.GetValue('ReportNumber');
            msg += ' ';
            msg += this.riGrid.Details.GetValue('ReportName');
            msg += '. ';
            msg += MessageConstant.PageSpecificMessage.regenerateConfirm.dates;
            msg += '. ';
            msg += MessageConstant.PageSpecificMessage.regenerateConfirm.confirm;
            this.modalService.emitPrompt(new ICabsModalVO(msg, null, this.onGenerateReport.bind(this)));
        } else if (this.riGrid.CurrentColumnName === 'ReportNumber') {
            let search: QueryParams = new QueryParams();
            let gridRequest: Record<string, any> = {};
            let formData: Record<string, string | boolean | number> = {};
            let isTypeTicket: boolean = (this.c_s_REPORT_TYPE_TICKET === this.getControlValue('ReportType'));

            search.set(this.serviceConstants.CountryCode, this.countryCode());
            search.set(this.serviceConstants.BusinessCode, this.businessCode());
            search.set(this.serviceConstants.Action, '5');

            formData[EXHRParams.c_s_REPORTNUMBER] = this.riGrid.Details.GetValue('ReportNumber');
            formData[EXHRParams.c_s_REPORTTYPETICKET] = StaticUtils.convertCheckboxValueToRequestValue(isTypeTicket);
            formData[EXHRParams.c_s_REPORTTYPEROOTCAUSE] = StaticUtils.convertCheckboxValueToRequestValue(!isTypeTicket);
            formData[EXHRParams.c_s_EXPORTTYPE] = ExportConfig.c_s_TYPE_CSV;

            gridRequest[HttpConstants.c_s_API_METHOD] = EXHRParams.c_s_METHOD;
            gridRequest['module'] = EXHRParams.c_s_MODULE;
            gridRequest['operation'] = EXHRParams.c_s_OPERATION;
            gridRequest['http_method'] = 'POST';
            gridRequest['search'] = search;
            gridRequest['formData'] = formData;

            this.ajaxSource.next(this.ajaxconstant.START);
            this.displayMessage(MessageConstant.Message.ExportMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
            this.httpService.makeExportRequest(gridRequest, StaticUtils.getTitle()).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data && !data.hasOwnProperty('SpreadsheetURL')) {
                    this.displayMessage(MessageConstant.Message.GeneralError);
                }
            }).catch(error => {
                this.displayMessage(error);
            });
        }
    }

    public onAdditionalFilterSelect(data: ISelectedProperty): void {
        this.additionalFilters.value = data;
    }

    public clearAdditionalFilters(): void {
        this.cleanup.emitCleaner();
        this.selectedSubTab = 0;
        this.additionalFilters.clear();
    }
    // #endregion
}
