import { Subject } from 'rxjs/Rx';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class FilterCleanUpHandlerService {
    private cleanup: Subject<any> = new Subject<any>();

    emitCleaner(): void {
        this.cleanup.next({
            cleanup: true
        });
    }

    getObservableSource(): Observable<any> {
        return this.cleanup.asObservable();
    }
}
