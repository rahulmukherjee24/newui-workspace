import { Component, OnInit, Injector, AfterContentInit, ViewChild, OnDestroy } from '@angular/core';

import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { MessageConstant } from '@shared/constants/message.constant';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { LookUpData } from '@shared/services/lookup';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { StaticUtils } from '@shared/services/static.utility';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { CustomerTypeSearchComponent } from '@app/internal/search/iCABSSCustomerTypeSearch';
import { EllipsisComponent } from '@shared/components/ellipsis/ellipsis';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { IControls } from '@app/base/ControlsType';
import { Subscription } from 'rxjs';
import { OrderBy } from '@shared/pipes/orderBy';
import { BIReportsRoutes } from '@app/base/PageRoutes';

@Component({
  templateUrl: './iCABSCMRootCauseAnalysisGrid.html',
  providers: [CommonLookUpUtilsService]
})

export class RootCauseAnalysisGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
  @ViewChild('riGrid') riGrid: GridAdvancedComponent;
  @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
  @ViewChild('customerTypeSearchEllipsis') public customerTypeSearchEllipsis: EllipsisComponent;

  public controls: IControls[] = [
    { name: 'BusinessCode', required: true, type: MntConst.eTypeCode },
    { name: 'BusinessDesc', disabled: true, required: true, type: MntConst.eTypeText },
    { name: 'BranchNumber', disabled: true, type: MntConst.eTypeInteger },
    { name: 'BranchName', disabled: true },
    { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
    { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
    { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
    { name: 'DateTo', required: true, type: MntConst.eTypeDate },
    { name: 'ContactTypeCode', value: 'all', type: MntConst.eTypeCode },
    { name: 'ContactTypeDetailCode', value: 'all', type: MntConst.eTypeCode },
    { name: 'CustomerTypeCode', type: MntConst.eTypeCode },
    { name: 'CustomerTypeDesc', type: MntConst.eTypeText, disabled: true },
    { name: 'RootCauseCode', value: 'all', type: MntConst.eTypeCode }
  ];

  public pageId: string;
  public pageType: string;
  public reportMessage: string = '';
  public ttRootCause = [];
  public commonGridFunction: CommonGridFunction;

  public ellipsis: any = {
    customerTypeCodeEllipsis: {
      showCloseButton: true,
      showHeader: true,
      disabled: false,
      childparams: {
        'parentMode': 'LookUp'
      },
      component: CustomerTypeSearchComponent
    }
  };
  private regionCodeSubscription: Subscription;
  private orderBy: OrderBy;

  constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService) {
    super(injector);
    this.commonGridFunction = new CommonGridFunction(this);
    this.orderBy = injector.get(OrderBy);
  }

  public ngOnInit(): void {
    super.ngOnInit();
    this.pageParams.gridConfig = {
      totalItem: 1,
      itemsPerPage: 10,
      gridHandle: this.utils.randomSixDigitString()
    };
    this.pageParams.gridCurrentPage = 1;
    this.pageParams.gridCacheRefresh = true;
  }

  public ngAfterContentInit(): void {
    this.pageType = StaticUtils.capitalizeFirstLetterAlphaNumberic(this.riExchange.getParentHTMLValue('pageType'));
    this.pageId = PageIdentifier.ICABSCMROOTCAUSEANALYSISGRID;
    this.pageTitle = this.pageType + ' - Customer Complaint Reports';
    this.utils.setTitle(this.pageTitle);
    super.ngAfterContentInit();
    this.windowOnload();
    this.pageParams.hasGridData = false;
    this.pageParams.contactTypes = [];
    this.pageParams.contactTypeDetails = [{
      value: 'all',
      text: 'All'
    }];
    this.lookUpForTicketDetail();
    this.lookUpForRootCause();
  }

  public ngOnDestroy(): void {
    if (this.regionCodeSubscription) {
      this.regionCodeSubscription.unsubscribe();
    }
  }

  private windowOnload(): void {
    if (this.isReturning()) {
      const ContactTypeDetailCode = this.getControlValue('ContactTypeDetailCode');
      this.onContactTypeChange();
      this.setControlValue('ContactTypeDetailCode', ContactTypeDetailCode);
      this.buildGrid();
      this.populateGrid();
    }
    else {
      switch (this.pageType) {
        case 'Business':
          this.setControlValue('BusinessCode', this.utils.getBusinessCode());
          this.setControlValue('BusinessDesc', this.utils.getBusinessText());
          break;
        case 'Region':
          this.regionCodeSubscription = this.commonLookup.getRegionDesc().subscribe(data => {
            if (data && data[0] && data[0][0]) {
              this.setControlValue('RegionCode', data[0][0]['RegionCode']);
              this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
            } else {
              this.displayMessage(MessageConstant.Message.RecordNotFound, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
            }
          }, error => {
            this.displayMessage(error);
          });
          break;
        case 'Branch':
          this.setControlValue('BranchNumber', this.utils.getBranchCode());
          this.setControlValue('BranchName', this.utils.getBranchTextOnly());
          break;
        default:
      }
      const date = new Date();
      this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(new Date(date.getFullYear(), date.getMonth(), 1)));
      this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(new Date(date.getFullYear(), date.getMonth() + 1, 0)));
      this.buildGrid();
      this.populateGrid();
    }
  }

  private lookUpForTicketDetail(): void {
    const query: Array<LookUpData> = [{
      'table': 'ContactTypeLang',
      'query': {
        'LanguageCode': this.riExchange.LanguageCode(),
        'BusinessCode': this.businessCode()
      },
      'fields': ['ContactTypeCode', 'ContactTypeDesc']
    }];

    this.LookUp.lookUpPromise(query, 100).then(data => {
      if (data && data.length) {
        data[0].forEach(element => {
          this.pageParams.contactTypes.push({ text: element['ContactTypeDesc'], value: element['ContactTypeCode'] });
        });
        this.utils.sortByKey(this.pageParams.contactTypes, 'text');
      }
      else {
        this.displayMessage(MessageConstant.Message.RecordNotFound);
      }
    }).catch(error => {
      this.displayMessage(error);
    });
  }

  private lookUpForRootCause(): void {
    const gLanguageCode = this.riExchange.LanguageCode();
    const data = [
      {
        'table': 'RootCause',
        'query': { 'BusinessCode': this.utils.getBusinessCode() },
        'fields': ['BusinessCode', 'RootCauseCode', 'RootCauseSystemDescription']
      },
      {
        'table': 'RootCauseLang',
        'query': { 'BusinessCode': this.utils.getBusinessCode(), 'LanguageCode': gLanguageCode },
        'fields': ['BusinessCode', 'RootCauseCode', 'RootCauseDescription', 'LanguageCode']
      }

    ];

    this.lookUpRecord(data, 500).subscribe(
      (e) => {
        if (e['results'] && e['results'].length > 0) {

          if (e['results'] && e['results'][0].length > 0) {
            const RootCause: any[] = e['results'][0];
            let RootCauseLang: any[] = [];

            if (e['results'].length > 1 && e['results'][1].length > 0) {
              RootCauseLang = e['results'][1];
            }

            RootCause.forEach(item => {
              const filterData = RootCauseLang.find(langObj => ((langObj.BusinessCode === item.BusinessCode)
                && (langObj.RootCauseCode === item.RootCauseCode)));
              this.ttRootCause.push({
                'ttRootCauseCode': item.RootCauseCode,
                'ttRootCauseSystemDescription': (filterData) ? filterData.RootCauseDescription : item.RootCauseSystemDescription
              });
            });
            if (this.ttRootCause.length > 0) {
              this.ttRootCause = this.orderBy.transform(this.ttRootCause, 'ttRootCauseSystemDescription');
            }
          }
        }
      });
  }

  private populateGrid(): void {
    if (!this.riExchange.validateForm(this.uiForm))
      return;
    const formData: Record<string, string | boolean | number> = {};
    const search: QueryParams = this.getURLSearchParamObject();
    search.set(this.serviceConstants.Action, '2');
    switch (this.pageType) {
      case 'Business':
        formData['BusinessCode'] = this.utils.getBusinessCode();
        formData['Level'] = '1';
        break;
      case 'Region':
        formData['RegionCode'] = this.getControlValue('RegionCode');
        formData['Level'] = '2';
        break;
      case 'Branch':
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['Level'] = '3';
        break;
      default:
    }

    formData['BatchBuildType'] = this.pageType;
    formData['DateFrom'] = this.getControlValue('DateFrom');
    formData['DateTo'] = this.getControlValue('DateTo');
    formData[this.serviceConstants.GridMode] = '0';
    formData[this.serviceConstants.PageSize] = '10';
    formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
    if (this.pageParams.gridCacheRefresh) {
      formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
    }
    formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
    formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
    formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

    this.ajaxSource.next(this.ajaxconstant.START);
    this.httpService.xhrPost('bi/reports', 'reports', 'ContactManagement/iCABSCMRootCauseAnalysisGrid', search, formData).then(data => {
      this.ajaxSource.next(this.ajaxconstant.COMPLETE);
      this.commonGridFunction.setPageData(data, this.pageParams.gridConfig);
    }).catch(error => {
      this.ajaxSource.next(this.ajaxconstant.COMPLETE);
      this.pageParams.hasGridData = false;
      this.displayMessage(error);
    });
  }

  public onContactTypeChange(): void {
    const search: QueryParams = new QueryParams();
    const contactType: string = this.getControlValue('ContactTypeCode');

    this.pageParams.contactTypeDetails = [];
    this.setControlValue('ContactTypeDetailCode', 'all');

    if (contactType === 'all') {
      return;
    }

    search.set(this.serviceConstants.CountryCode, this.countryCode());
    search.set(this.serviceConstants.BusinessCode, this.businessCode());
    search.set(this.serviceConstants.Action, '6');

    search.set(this.serviceConstants.Function, 'GetContactTypeDetailCodes');
    search.set('ContactType', contactType);

    this.ajaxSource.next(this.ajaxconstant.START);
    this.httpService.xhrGet('bi/reports', 'reports', 'ContactManagement/iCABSCMTicketAnalysisGeneration', search).then(data => {
      this.ajaxSource.next(this.ajaxconstant.COMPLETE);
      if (this.hasError(data)) {
        this.displayMessage(data);
      } else {
        const codes: Array<string> = data.ContactTypeDetailCodes.split(StaticUtils.RI_SEPARATOR_VALUE_NEWLINE);
        const descs: Array<string> = data.ContactTypeDetailDescs.split(StaticUtils.RI_SEPARATOR_VALUE_NEWLINE);
        this.pageParams.contactTypeDetails = codes.map((code, index) => {
          return {
            value: code,
            text: descs[index]
          };
        });
      }
    }).catch(error => {
      this.displayMessage(error);
    });
  }

  public lookUpRecord(data: any, maxresults: any): any {
    const queryLookUp = new QueryParams();
    queryLookUp.set(this.serviceConstants.Action, '0');
    queryLookUp.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
    queryLookUp.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
    if (maxresults) {
      queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
    }
    return this.httpService.lookUpRequest(queryLookUp, data);
  }

  public buildGrid(): void {
    this.riGrid.Clear();
    this.riGrid.AddColumn('ReportNumber', 'RootCause', 'ReportNumber', MntConst.eTypeInteger, 7, true, 'Report Number');
    this.riGrid.AddColumnAlign('ReportNumber', MntConst.eAlignmentCenter);
    switch (this.pageType) {
      case 'Region':
        this.riGrid.AddColumn('RegionCode', 'RootCause', 'RegionCode', MntConst.eTypeCode, 8, false, 'Region Code');
        this.riGrid.AddColumnAlign('RegionCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('RegionDesc', 'RootCause', 'RegionDesc', MntConst.eTypeText, 15, false, 'Region Code');
        this.riGrid.AddColumnAlign('RegionDesc', MntConst.eAlignmentCenter);
        break;
      case 'Branch':
        this.riGrid.AddColumn('BranchNumber', 'RootCause', 'BranchNumber', MntConst.eTypeCode, 3, false, 'Branch Number');
        this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchName', 'RootCause', 'BranchName', MntConst.eTypeText, 15, false, 'Branch Name');
        this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentCenter);
        break;
    }
    this.riGrid.AddColumn('CreatedDate', 'RootCause', 'CreatedDate', MntConst.eTypeText, 16, false, 'Report Created On');
    this.riGrid.AddColumnAlign('CreatedDate', MntConst.eAlignmentCenter);
    this.riGrid.AddColumn('DateFrom', 'RootCause', 'DateFrom', MntConst.eTypeDate, 10, false, 'From Date');
    this.riGrid.AddColumnAlign('DateFrom', MntConst.eAlignmentCenter);
    this.riGrid.AddColumn('DateTo', 'RootCause', 'DateTo', MntConst.eTypeDate, 10, false, 'To Date');
    this.riGrid.AddColumnAlign('DateTo', MntConst.eAlignmentCenter);
    this.riGrid.AddColumn('ContactType', 'RootCause', 'ContactType', MntConst.eTypeText, 15, false, 'Contact Type');
    this.riGrid.AddColumnAlign('ContactType', MntConst.eAlignmentCenter);
    this.riGrid.AddColumn('ContactTypeDetail', 'RootCause', 'ContactTypeDetail', MntConst.eTypeText, 15, false, 'Detail');
    this.riGrid.AddColumnAlign('ContactTypeDetail', MntConst.eAlignmentCenter);
    this.riGrid.AddColumn('CustomerType', 'RootCause', 'CustomerType', MntConst.eTypeText, 15, false, 'Customer Type');
    this.riGrid.AddColumnAlign('CustomerType', MntConst.eAlignmentCenter);
    this.riGrid.AddColumn('RootCause', 'RootCause', 'RootCause', MntConst.eTypeText, 15, false, 'Root Cause');
    this.riGrid.AddColumnAlign('RootCause', MntConst.eAlignmentCenter);

    this.riGrid.Complete();
  }

  public onRiGridRefresh(): void {
    this.pageParams.gridCacheRefresh = true;
    this.buildGrid();
    this.populateGrid();
  }

  public onHeaderClick(): void {
    this.onRiGridRefresh();
  }

  public submitReportGeneration(): void {
    if (!this.riExchange.validateForm(this.uiForm))
      return;
    const search: QueryParams = new QueryParams();
    const formData: Record<string, string | boolean | number> = {};

    search.set(this.serviceConstants.CountryCode, this.countryCode());
    search.set(this.serviceConstants.BusinessCode, this.businessCode());
    search.set(this.serviceConstants.Action, '6');
    search.set(this.serviceConstants.Function, 'SubmitBatchProcess');

    switch (this.pageType) {
      case 'Business':
        formData['Level'] = '1';
        break;
      case 'Region':
        formData['RegionCode'] = this.getControlValue('RegionCode');
        formData['Level'] = '2';
        break;
      case 'Branch':
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['Level'] = '3';
        break;
      default:
    }
    formData['BusinessCode'] = this.businessCode();
    formData['BatchBuildType'] = this.pageType;
    formData['DateFrom'] = this.getControlValue('DateFrom');
    formData['DateTo'] = this.getControlValue('DateTo');
    formData['ContactTypeCode'] = this.getControlValue('ContactTypeCode');
    formData['ContactTypeDetailCode'] = this.getControlValue('ContactTypeDetailCode');
    formData['CustomerTypeCode'] = this.getControlValue('CustomerTypeCode') || 'all';
    formData['RootCauseCode'] = this.getControlValue('RootCauseCode');
    formData['SubmitBatchProcess'] = 'yes';
    formData['methodtype'] = 'maintenance';

    this.ajaxSource.next(this.ajaxconstant.START);
    this.httpService.xhrPost('bi/reports', 'reports', 'ContactManagement/iCABSCMRootCauseAnalysisGrid', search, formData).then(data => {
      this.ajaxSource.next(this.ajaxconstant.COMPLETE);
      if (data) {
        this.reportMessage = data.BatchProcessInformation;
        return;
      }
    }).catch(error => {
      this.displayMessage(error);
    });
  }

  public riGridBodyOnDblClick(): void {
    const currentColumn = this.riGrid.CurrentColumnName;
    const params: any = {
      'pagetype': this.pageType,
      'DateFrom': this.getControlValue('DateFrom'),
      'DateTo': this.getControlValue('DateTo'),
      'ContactType': this.riGrid.Details.GetValue('ContactType'),
      'ContactTypeDetail': this.riGrid.Details.GetValue('ContactTypeDetail'),
      'CustomerType': this.riGrid.Details.GetValue('CustomerType'),
      'RootCause': this.riGrid.Details.GetValue('RootCause'),
      'ReportNumber': this.riGrid.Details.GetValue('ReportNumber'),
      'RowID': this.riGrid.Details.GetAttribute(currentColumn, 'rowid')
    };
    if (this.riGrid.CurrentColumnName === 'ReportNumber') {
      this.navigate(this.pageType, BIReportsRoutes.ICABSCMROOTCAUSEANALYSISDETAILGRID, params);
    }
  }

  public onCustomerTypeDataReceived(data: any): void {
    if (data) {
      this.setControlValue('CustomerTypeCode', data.CustomerTypeCode);
      this.setControlValue('CustomerTypeDesc', data.CustomerTypeDesc);
      this.uiForm.controls['CustomerTypeCode'].markAsDirty();
    }
  }
}
