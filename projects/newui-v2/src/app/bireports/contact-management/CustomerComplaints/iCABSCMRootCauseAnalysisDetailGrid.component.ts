import { Component, OnInit, Injector, AfterContentInit, ViewChild, OnDestroy } from '@angular/core';

import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { MessageConstant } from '@shared/constants/message.constant';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { StaticUtils } from '@shared/services/static.utility';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { IControls } from '@app/base/ControlsType';
import { Subscription } from 'rxjs';
import { BIReportsRoutes, InternalMaintenanceApplicationModuleRoutes } from '@app/base/PageRoutes';
import { IXHRParams } from '@app/base/XhrParams';

@Component({
    templateUrl: './iCABSCMRootCauseAnalysisDetailGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class RootCauseAnalysisDetailGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public controls: IControls[] = [
        { name: 'BusinessCode', required: true, type: MntConst.eTypeCode },
        { name: 'BusinessDesc', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'BranchName', disabled: true },
        { name: 'ViewBy', value: 'Branch' },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'DateFrom', type: MntConst.eTypeDate, disabled: true },
        { name: 'DateTo', type: MntConst.eTypeDate, disabled: true },
        { name: 'ContactType', type: MntConst.eTypeText, disabled: true },
        { name: 'ContactTypeDetail', type: MntConst.eTypeText, disabled: true },
        { name: 'CustomerType', type: MntConst.eTypeText, disabled: true },
        { name: 'RootCause', type: MntConst.eTypeText, disabled: true },
        { name: 'ReportNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'ContactDetailCode', type: MntConst.eTypeCode }
    ];

    public xhrParams: IXHRParams = {
        operation: 'ContactManagement/iCABSCMRootCauseAnalysisDetailGrid',
        module: 'reports',
        method: 'bi/reports'
    };
    public pageId: string;
    public pageType: string;
    public commonGridFunction: CommonGridFunction;
    public pageMode: string;

    private regionCodeSubscription: Subscription;
    private reportRowId: string | number | boolean;
    private numHeaderCount: number;
    public showRegion: boolean = false;
    public showBranch: boolean = false;

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    public ngOnInit(): void {
        super.ngOnInit();
        if (!this.isReturning()) {
            this.pageParams.gridConfig = {
                totalItem: 1,
                itemsPerPage: 10
            };
            this.pageParams.gridCurrentPage = 1;
        }
        this.reportRowId = this.riExchange.getParentAttributeValue('RowID');
        this.pageMode = this.riExchange.getParentHTMLValue('parentMode');
    }

    public ngAfterContentInit(): void {
        this.pageType = StaticUtils.capitalizeFirstLetterAlphaNumberic(this.riExchange.getParentHTMLValue('pagetype'));
        const viewBy = (this.riExchange.getParentAttributeValue('ViewBy')) ? this.riExchange.getParentAttributeValue('ViewBy').toUpperCase() : '';
        this.pageId = PageIdentifier['ICABSCMROOTCAUSEANALYSIS' + this.pageType.toUpperCase() + 'GRID' + viewBy];
        this.pageTitle = 'Customer Complaint Detail - ' + this.pageType;
        this.utils.setTitle(this.pageTitle);
        super.ngAfterContentInit();
        if (this.pageType !== 'Branch' || this.pageMode === 'LookUp' || this.pageMode === 'LookUpRegion') {
            this.setInitialValues();
        } else {
            this.windowOnload();
        }

        this.pageParams.hasGridData = false;
    }

    public ngOnDestroy(): void {
        if (this.regionCodeSubscription) {
            this.regionCodeSubscription.unsubscribe();
        }
    }

    private getPagelevel(): string {
        switch (this.pageType) {
            case 'Business':
                return '1';
            case 'Region':
                return '2';
            case 'Branch':
                return '3';
        }
    }

    public setInitialValues(viewTypeChange: boolean = false): void {
        const formData: Record<string, string | boolean | number> = {};
        const search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.Function, 'GetInitialValues');
        formData['Level'] = this.getPagelevel();
        formData['HeaderRowid'] = this.reportRowId;
        formData['RegionCode'] = this.riExchange.getParentAttributeValue('RegionCode') || this.getControlValue('RegionCode');
        if (this.pageMode === 'LookUpRegion') {
            formData['RegionCode'] = this.riExchange.getParentHTMLValue('RegionCode');
        }
        formData['ViewBy'] = this.getControlValue('ViewBy');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.numHeaderCount = data.NumColumns;
            if (viewTypeChange) {
                if (this.pageParams.gridCurrentPage === 1) {
                    this.onRiGridRefresh();
                } else {
                    this.buildGrid();
                    this.pageParams.gridCurrentPage = 1;
                }
            } else {
                this.windowOnload();
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.hasGridData = false;
            this.displayMessage(error);
        });

    }

    private windowOnload(): void {
        if (this.isReturning()) {
            this.setFields();
            this.buildGrid();
            this.populateGrid();
        } else {
            this.riExchange.getParentHTMLValue('ContactType');
            this.riExchange.getParentHTMLValue('ContactTypeDetail');
            this.riExchange.getParentHTMLValue('CustomerType');
            this.riExchange.getParentHTMLValue('RootCause');
            this.riExchange.getParentHTMLValue('ReportNumber');
            this.riExchange.getParentHTMLValue('DateFrom');
            this.riExchange.getParentHTMLValue('DateTo');

            this.setFields();
            this.buildGrid();
            this.populateGrid();
        }

    }

    private setFields(): void {
        switch (this.pageType) {
            case 'Business':
                this.setControlValue('BusinessCode', this.utils.getBusinessCode());
                this.setControlValue('BusinessDesc', this.utils.getBusinessText());
                break;
            case 'Region':
                this.regionCodeSubscription = this.commonLookup.getRegionDesc().subscribe(data => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('RegionCode', data[0][0]['RegionCode']);
                        this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
                    } else {
                        this.displayMessage(MessageConstant.Message.RecordNotFound, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    }
                }, error => {
                    this.displayMessage(error);
                });
                break;
            case 'Branch':
                if (this.pageMode === 'LookUpRegion') {
                    this.showRegion = true;
                    this.showBranch = false;
                    this.riExchange.getParentHTMLValue('RegionCode');
                    this.riExchange.getParentHTMLValue('RegionDesc');
                } else {
                    this.showBranch = true;
                    this.riExchange.getParentHTMLValue('BranchNumber');
                    this.setControlValue('BranchName', this.utils.getBranchTextOnly());
                    if (this.getControlValue('BranchNumber') === '') {
                        this.setControlValue('BranchNumber', 'Totals');
                        this.setControlValue('BranchName', '');
                    }
                }
                break;
            default:
        }
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        const formData: Record<string, string | boolean | number> = {};
        const search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        switch (this.pageType) {
            case 'Business':
                formData['Level'] = '1';
                break;
            case 'Region':
                formData['RegionCode'] = this.getControlValue('RegionCode');
                formData['Level'] = '2';
                break;
            case 'Branch':
                formData['BranchNumber'] = this.getControlValue('BranchNumber');
                formData['Level'] = '3';
                break;
            default:
        }
        formData['BusinessCode'] = this.utils.getBusinessCode();
        formData['ReportNumber'] = this.getControlValue('ReportNumber');
        formData['BranchNumber'] = this.pageMode === 'LookUp' ? this.getControlValue('BranchNumber') : this.utils.getBranchCode();
        if (!this.getControlValue('BranchNumber')) {
            formData['BranchNumber'] = '';
        }
        formData['RegionCode'] = this.riExchange.getParentAttributeValue('RegionCode') || this.getControlValue('RegionCode');
        formData['DateFrom'] = this.getControlValue('DateFrom');
        formData['DateTo'] = this.getControlValue('DateTo');
        formData['ContactType'] = this.getControlValue('ContactType');
        formData['ContactTypeDetail'] = this.getControlValue('ContactTypeDetail');
        formData['ContactDetailCode'] = this.riExchange.getParentAttributeValue('ContactDetailCode') || this.getControlValue('ContactDetailCode');
        formData['CustomerType'] = this.getControlValue('CustomerType');
        formData['RootCause'] = this.getControlValue('RootCause');
        formData['HeaderRowid'] = this.reportRowId;
        formData['ViewBy'] = this.pageMode === 'LookUp' ? 'Branch' : this.getControlValue('ViewBy');
        formData['Rowid'] = '';
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
        formData[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        formData[this.serviceConstants.GridCacheRefresh] = true;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.commonGridFunction.setPageData(data, this.pageParams.gridConfig);
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.hasGridData = false;
            this.displayMessage(error);
        });
    }
    public buildGrid(): void {
        this.riGrid.Clear();

        if (this.pageType !== 'Branch' || this.pageMode === 'LookUpRegion') {
            this.riGrid.AddColumn('ComplaintType', 'ComplaintTypeGrid', 'ComplaintType', MntConst.eTypeText, 30, false, 'Complaint Type');
            this.riGrid.AddColumnAlign('ComplaintType', MntConst.eAlignmentLeft);

            for (let i = 0; i < this.numHeaderCount; i++) {
                this.riGrid.AddColumn('Column' + i, 'RootCauseGrid', 'Column' + i, MntConst.eTypeText, 2, false, '', false, true);
                this.riGrid.AddColumnAlign('Column' + i, MntConst.eAlignmentLeft);
            }

        } else {
            this.riGrid.PageSize = 10;
            this.riGrid.AddColumn('ContactNumber', 'RootCauseGrid2', 'ContactNumber', MntConst.eTypeText, 8, true, 'Contact Number');
            this.riGrid.AddColumnAlign('ContactNumber', MntConst.eAlignmentLeft);
            this.riGrid.AddColumn('CallLog', 'RootCauseGrid2', 'CallLog', MntConst.eTypeText, 8, true, 'Call Log');
            this.riGrid.AddColumnAlign('CallLog', MntConst.eAlignmentLeft);
            this.riGrid.AddColumn('ContactTypeDetail', 'RootCauseGrid2', 'ContactTypeDetail', MntConst.eTypeText, 20, true, 'Contact Type Detail');
            this.riGrid.AddColumnAlign('ContactTypeDetail', MntConst.eAlignmentLeft);
            this.riGrid.AddColumn('AccountNumber', 'RootCauseGrid2', 'AccountNumber', MntConst.eTypeText, 9, true, 'Account Number');
            this.riGrid.AddColumnAlign('AccountNumber', MntConst.eAlignmentLeft);
            this.riGrid.AddColumn('AccountName', 'RootCauseGrid2', 'Account Name', MntConst.eTypeText, 20, true, 'Contact Name');
            this.riGrid.AddColumnAlign('AccountName', MntConst.eAlignmentLeft);
        }
        this.riGrid.Complete();
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.populateGrid();
    }


    public riGridBodyOnDblClick(): void {
        let currentColumn = this.riGrid.CurrentColumnName;
        const additionData = this.riGrid.Details.GetAttribute(currentColumn, 'additionalproperty').split(',');
        if (additionData[0] !== 'Grand Total' || additionData[1] === 'Grand Total') {
            switch (this.pageType) {
                case 'Business':
                    if (this.getControlValue('ViewBy') === 'Region') {
                        this.setAttribute('RowID', this.reportRowId);
                        this.setAttribute('RegionCode', additionData[0] === 'Grand Total' ? '' : additionData[0]);
                        this.setAttribute('ContactDetailCode', additionData[1]);
                        this.navigate('LookUp', BIReportsRoutes.ICABSCMROOTCAUSEANALYSISBUSINESSGRID,
                            {
                                'ContactType': this.getControlValue('ContactType'),
                                'ContactTypeDetail': this.getControlValue('ContactTypeDetail'),
                                'CustomerType': this.getControlValue('CustomerType'),
                                'RootCause': this.getControlValue('RootCause'),
                                'ReportNumber': this.getControlValue('ReportNumber'),
                                'ViewBy': this.getControlValue('ViewBy'),
                                'pagetype': 'Business'
                            }
                        );
                    } else {
                        this.setAttribute('RowID', this.reportRowId);
                        this.setAttribute('RegionCode', '');
                        this.setControlValue('BranchNumber', additionData[0] === 'Grand Total' ? '' : additionData[0]);
                        this.setControlValue('ContactDetailCode', additionData[1]);
                        this.navigate('LookUp', BIReportsRoutes.ICABSCMROOTCAUSEANALYSISBRANCHGRID,
                            {
                                'pagetype': 'Branch'
                            }
                        );
                    }
                    break;
                case 'Region':
                    this.setAttribute('RowID', this.reportRowId);
                    this.setAttribute('RegionCode', additionData[0] === 'Grand Total' ? '' : additionData[0]);
                    this.setAttribute('BranchNumber', '');
                    this.setAttribute('ContactDetailCode', additionData[1]);
                    this.navigate('LookUpRegion', BIReportsRoutes.ICABSCMROOTCAUSEANALYSISBRANCHGRIDREGION,
                        {
                            'pagetype': 'Branch'
                        }
                    );
                    break;
                case 'Branch':
                    if (this.riExchange.getParentMode() === 'LookUpRegion') {
                        this.setAttribute('RowID', this.reportRowId);
                        this.setAttribute('RegionCode', '');
                        this.setControlValue('BranchNumber', additionData[0] === 'Grand Total' ? '' : additionData[0]);
                        this.setAttribute('ContactDetailCode', additionData[1]);
                        this.navigate('LookUp', BIReportsRoutes.ICABSCMROOTCAUSEANALYSISBRANCHGRID,
                            {
                                'pagetype': 'Branch',
                                'ViewBy': this.getControlValue('ViewBy')
                            }
                        );
                    } else {
                        this.setAttribute('RowID', this.reportRowId);
                        this.navigate('SalesOrderCMSearch', InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_1,
                            {
                                'PassContactROWID': additionData[0],
                                'pagetype': 'Branch'
                            }
                        );
                    }
            }
        }

    }
}
