
import { HttpClientModule } from '@angular/common/http';
import { Component, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BIReportsRoutesConstant } from '@base/PageRoutes';
import { SharedModule } from '@shared/shared.module';
import { SearchEllipsisDropdownModule } from '@app/internal/search-ellipsis-dropdown.module';

import { TicketAnalysisSelectComponent } from './selection-screens/iCABSCMTicketAnalysisSelect.component';
import { TicketAnalysisGenerationComponent } from './TicketAnalysis/iCABSCMTicketAnalysisGeneration.component';
import { RootCauseAnalysisGridComponent } from './CustomerComplaints/iCABSCMRootCauseAnalysisGrid.component';
import { FilterCleanUpHandlerService } from './selection-screens/FilterCleanUpHandlerService';
import { RootCauseAnalysisDetailGridComponent } from './CustomerComplaints/iCABSCMRootCauseAnalysisDetailGrid.component';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class ContactManagementBIReportsRootComponent {
    constructor() {
    }
}

@NgModule({
    exports: [
    ],
    imports: [
        HttpClientModule,
        SharedModule,
        SearchEllipsisDropdownModule,
        RouterModule.forChild([
            {
                path: '', component: ContactManagementBIReportsRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSCMTICKETANALYSISGENERATION, component: TicketAnalysisGenerationComponent },
                    { path: BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISGRIDBUSINESS, component: RootCauseAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISGRIDREGION, component: RootCauseAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISGRIDBRANCH, component: RootCauseAnalysisGridComponent },
                    { path: BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISDETAILGRID, component: RootCauseAnalysisDetailGridComponent },
                    { path: BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISBUSINESSGRID, component: RootCauseAnalysisDetailGridComponent },
                    { path: BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISREGIONGRID, component: RootCauseAnalysisDetailGridComponent },
                    { path: BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISBRANCHGRID, component: RootCauseAnalysisDetailGridComponent },
                    { path: BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISBRANCHGRIDREGION, component: RootCauseAnalysisDetailGridComponent }

                ], data: { domain: 'BI REPORTS CONTACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        ContactManagementBIReportsRootComponent,
        TicketAnalysisSelectComponent,
        TicketAnalysisGenerationComponent,
        RootCauseAnalysisGridComponent,
        RootCauseAnalysisDetailGridComponent
    ],
    providers: [
        FilterCleanUpHandlerService
    ]

})

export class ContactManagementBIReportsModule { }
