import { AfterContentInit, Component, OnInit, Injector } from '@angular/core';

import { EmployeeSearchComponent } from '@internal/search/iCABSBEmployeeSearch';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';

import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { StaticUtils } from '@shared/services/static.utility';

@Component({
    templateUrl: 'iCABSARTechnicianWorkSummaryReport.html'
})
export class TechnicianWorkSummaryReportComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    public controls = [
        { name: 'BranchName', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', readonly: true, disabled: true, required: true, type: MntConst.eTypeInteger },
        { name: 'BusinessCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeInteger },
        { name: 'BusinessDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'EmployeeCode', required: false, type: MntConst.eTypeText },
        { name: 'EmployeeSurname', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'RepDest', required: true, value: 'direct' },
        { name: 'SupervisorEmployeeCode', required: false, type: MntConst.eTypeText },
        { name: 'SupervisorSurname', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'VisitDateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'VisitDateTo', required: true, type: MntConst.eTypeDate }
    ];

    public pageId: string;
    public pageTitle: string;

    public alertMessage: any;
    public ellipsis: any = {
        superVisorEmployeeEllipsis: {
            childparams: {
                'parentMode': 'LookUp-Supervisor'
            },
            component: EmployeeSearchComponent
        },
        employeeEllipsis: {
            childparams: {
                'parentMode': 'LookUp'
            },
            component: EmployeeSearchComponent
        }
    };
    public messageType: string = CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR;
    public reportMessage: string;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARTECHNICIANWORKSUMMARYREPORT;
        this.browserTitle = this.pageTitle = 'Technician Work Summary';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchTextOnly());
        this.setControlValue('BusinessCode', this.utils.getBusinessCode());
        this.setControlValue('BusinessDesc', this.utils.getBusinessText());
        let date: Date = new Date();
        let formDate: Date = new Date(date.getFullYear(), date.getMonth(), 1);
        this.setControlValue('VisitDateFrom', this.globalize.parseDateToFixedFormat(formDate) as string);
        this.setControlValue('VisitDateTo', this.globalize.parseDateToFixedFormat(date) as string);
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
    }

    private doLookupForEmployee(employeeCodeControl: string, employeeDescControl: string): void {
        let lookupIP = [
            {
                'table': 'Employee',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'EmployeeCode': this.getControlValue(employeeCodeControl)
                },
                'fields': ['EmployeeSurname']
            }
        ];
        this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data[0] && data[0].length) {
                this.setControlValue(employeeDescControl, data[0][0].EmployeeSurname);
            } else {
                this.setControlValue(employeeDescControl, '');
            }
        });
    }

    private getReportManDest(): string {
        return this.getControlValue('RepDest') === 'direct' ? 'batch|ReportID' : 'email|User';
    }

    public onSuperVisorEmployeeChnage(): void {
        this.doLookupForEmployee('SupervisorEmployeeCode', 'SupervisorSurname');
    }

    public onEmployeeChnage(): void {
        this.doLookupForEmployee('EmployeeCode', 'EmployeeSurname');
    }

    public onSuperVisorEmployeeDataReceived(data: any): void {
        this.setControlValue('SupervisorEmployeeCode', data.SupervisorEmployeeCode);
        this.setControlValue('SupervisorSurname', data.SupervisorSurname);
    }

    public onEmployeeDataReceived(data: any): void {
        this.setControlValue('EmployeeCode', data.EmployeeCode);
        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
    }

    public onReportGeneration(data: any): void {
        let date: Date = new Date();
        this.reportMessage = '';
        if (this.uiForm.valid) {
            let queryParams: QueryParams = this.getURLSearchParamObject();
            queryParams.set(this.serviceConstants.Action, '0');
            let formData: Object = {
                'Description': 'Technician Work Summary',
                'ProgramName': 'iCABSTechWorkSummaryGeneration.p',
                'StartDate': this.globalize.parseDateToFixedFormat(date) as string,
                'StartTime': date.getHours() * 60 * 60 + date.getMinutes() * 60 + date.getSeconds(),
                'Report': 'report',
                'ParameterName': 'Mode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'BusinessCode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'BranchNumber' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'SupervisorEmployeeCode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'EmployeeCode' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'VisitDateFrom' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'VisitDateTo' + StaticUtils.RI_SEPARATOR_VALUE_LIST + 'RepManDest',
                'ParameterValue': 'Business' + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.utils.getBusinessCode() + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.utils.getBranchCode() + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('SupervisorEmployeeCode') + StaticUtils.RI_SEPARATOR_VALUE_LIST
                    + this.getControlValue('EmployeeCode') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('VisitDateFrom') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getControlValue('VisitDateTo') + StaticUtils.RI_SEPARATOR_VALUE_LIST + this.getReportManDest()

            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest('bi/reports', 'reports', 'ApplicationReport/iCABSARTechnicianWorkSummaryReport', queryParams, formData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.reportMessage = data.fullError;
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    let msgTxt: string = error.errorMessage;
                    msgTxt += error.fullError ? ' - ' + error.fullError : '';
                    this.alertMessage = {
                        msg: msgTxt,
                        timestamp: (new Date()).getMilliseconds()
                    };
                });
        }
    }
}
