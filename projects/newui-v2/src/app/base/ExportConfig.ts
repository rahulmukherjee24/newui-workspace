import { SessionStorage } from 'ngx-webstorage';

export interface IExportOptions {
    dateColumns?: string;
    dateTimeColumns?: string;
    dateCell?: string;
    datePattern?: string;
    durationColumns?: string;
}

export class ExportConfig {
    public static readonly c_s_STORAGE_KEY: string = 'last-grid';
    public static readonly c_s_OPTIONS_KEY: string = 'options';
    public static readonly c_s_DATE_FORMAT_1: string = 'yyyy/MM/dd - HH:mm';
    public static readonly c_s_DATE_FORMAT_2: string = 'yyyy/MM/dd HH:mm';
    public static readonly c_s_TYPE_CSV: string = 'CSV';

    @SessionStorage(ExportConfig.c_s_STORAGE_KEY, {})
    private sessionStorage: Record<string, any>;

    constructor() {
    }

    public static setExportConfig(config?: IExportOptions): void {
        let exportConfig: ExportConfig = new ExportConfig();

        exportConfig.store(config);
    }

    public store(config?: IExportOptions): void {
        this.sessionStorage[ExportConfig.c_s_OPTIONS_KEY] = config;

        /**
         * This Line Is Redundant And Added For A Known Bug
         * ** Serialization doesn't work for objects **
         */
        this.sessionStorage = this.sessionStorage;
    }
}
