export class HttpConstants {
    public static readonly c_s_API_METHOD: string = 'method';
    public static readonly c_s_API_MODULE: string = 'module';
    public static readonly c_s_API_OPERATION: string = 'operation';
    public static readonly c_s_API_SEARCH: string = 'search';
    public static readonly c_s_API_FORMDATA: string = 'formData';
    public static readonly c_s_HTTP_METHOD: string = 'http_method';
    public static readonly c_s_HTTP_GET: string = 'GET';
    public static readonly c_s_HTTP_POST: string = 'POST';
}
