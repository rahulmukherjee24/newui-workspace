export interface IControls {
    name: string;
    value?: string | number | boolean | Date;
    type?: string;
    required?: boolean;
    disabled?: boolean;
}
