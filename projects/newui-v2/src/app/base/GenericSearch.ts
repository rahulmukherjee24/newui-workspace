/**
 * @description
 *  - Generic Search Component And Ancilliaries
 *  - Use It To Fetch Data From Any Table
 *  - Follow The Metthod Descriptions For Details
 *  - This Will Be Translated To The Following:
 *      SELECT * FROM <Passed Table Name> WHERE <PASSED CONDITION 1> && <PASSED CONDITION 2> .... && <PASSED CONDITION n>
 *  - Design Is Based On: https://rentokilinitial.atlassian.net/wiki/spaces/IUI/pages/96534743/Search+Requests
 */
import { Injector } from '@angular/core';

import { HttpService } from './../../shared/services/http-service';
import { QueryParams } from './../../shared/services/http-params-wrapper';
import { ServiceConstants } from './../../shared/constants/service.constants';
import { Utils } from './../../shared/services/utility';

//#region Interfaces
/**
 * @interface IGenericSearchQuery
 * @description General Query Structure
 * @property {string} table Required. Table Name
 * @property {IGenericSearchFieldCondition} conditions Optional. Array Of IGenericSearchFieldCondition
 */
export interface IGenericSearchQuery {
    table: string;
    conditions?: Array<IGenericSearchFieldCondition>;
}

/**
 * @interface IGenericSearchFieldCondition
 * @description General Field Condition Structure
 * @property {string} field Field Name To Validate
 * @property {string} value Value Of The Field To Match
 * @property {operator} Optional. Value From EGenericSearchOpertorStrings. Leave Blank For "Equal To" Check
 */
export interface IGenericSearchFieldCondition {
    field: string;
    value: string;
    operator?: EGenericSearchOpertorStrings;
}
//#endregion

//#region Enums For Constants
/**
 * @enum EGenericSearchConfigConstants
 * @description Generic Search Config Constants
 */
export enum EGenericSearchConfigConstants {
    c_s_TABLE_NAME = 'table',
    c_s_FIELD_NAME = 'field',
    c_s_FIELD_VALUE = 'value',
    c_s_OPERATOR = 'operator',
    c_s_CONDITION_PARAM_NAME = 'search.op.'
}

/**
 * @enum EGenericSearchOpertorStrings
 * @description Generic Search Query Condition Operators
 */
export enum EGenericSearchOpertorStrings {
    LESS_THAN = 'LT',
    LESS_THAN_EQUAL = 'LE',
    GREATER_THAN = 'GT',
    GREATER_THAN_EQUAL = 'GE',
    NOT_EQUAL = 'NE',
    BEGINS_WITH = 'BEGINS',
    CONTAINS = 'CONTAINS',
    NONE = ''
}

/**
 * @enum EGenericSearchXHRParams
 * @description Generic Search API Params
 */
export enum EGenericSearchXHRParams {
    method = 'generic/search',
    module = 'report',
    operation = 'GenericSearch',
    action = 2,
    pageSize = 10
}
//#endregion

//#region Class
/**
 * @class GenericSearch
 * @classdesc Generic Search Object TO Search Or Add/Delete Fields From Query Conditions
 */
export class GenericSearch {
    private query: IGenericSearchQuery;
    private search: QueryParams = new QueryParams();

    private constants: ServiceConstants = new ServiceConstants();
    private http: HttpService;
    private utils: Utils;

    private maxRecords: number = 0;

    /**
     * @constructor
     * @classdesc Generic Search Object TO Search Or Add/Delete Fields From Query Conditions
     * @param {Injector} injector Injector For Services
     */
    constructor(private injector: Injector) {
        this.utils = this.injector.get(Utils);
        this.http = this.injector.get(HttpService);
    }

    //#region Getters And Setters
    /**
     * @member update
     * @description Updates The Condition On The Field; Adds If Does Not Exists
     */
    public set update(condition: IGenericSearchFieldCondition) {
        let conditions: Array<IGenericSearchFieldCondition> = this.query.conditions || [];
        let updateIdx: number = -1;
        for (let idx = 0; idx < conditions.length; idx++) {
            let field: string = conditions[idx][EGenericSearchConfigConstants.c_s_FIELD_NAME];

            if (field === condition[EGenericSearchConfigConstants.c_s_FIELD_NAME]) {
                updateIdx = idx;
                break;
            }
        }

        if (updateIdx === -1) {
            updateIdx = conditions.length;
            conditions[updateIdx] = {} as IGenericSearchFieldCondition;
        }

        conditions[updateIdx][EGenericSearchConfigConstants.c_s_FIELD_NAME]
            = condition[EGenericSearchConfigConstants.c_s_FIELD_NAME];
        conditions[updateIdx][EGenericSearchConfigConstants.c_s_FIELD_VALUE]
            = condition[EGenericSearchConfigConstants.c_s_FIELD_VALUE];
        conditions[updateIdx][EGenericSearchConfigConstants.c_s_OPERATOR]
            = condition[EGenericSearchConfigConstants.c_s_OPERATOR] || EGenericSearchOpertorStrings.NONE;

        this.buildQuery();
    }

    /**
     * @member remove
     * @description Removes The Condition On The Field
     */
    public set remove(field: string) {
        let conditions: Array<IGenericSearchFieldCondition> = this.query.conditions || [];
        let deleteIdx: number = -1;
        for (let idx = 0; idx < conditions.length; idx++) {
            let fieldName: string = conditions[idx][EGenericSearchConfigConstants.c_s_FIELD_NAME];

            if (fieldName === field) {
                deleteIdx = idx;
                break;
            }
        }

        if (deleteIdx === -1) {
            return;
        } else {
            conditions.splice(deleteIdx, 1);
        }

        this.buildQuery();
    }

    /**
     * @member pageSize
     * @description Sets The Number Of Revords Per Page
     */
    public set pageSize(size: number) {
        this.maxRecords = size;
        if (this.search) {
            this.search.set(this.constants.PageSize, this.maxRecords);
        }
    }
    //#endregion

    //#region Private Methods
    private buildQuery(): void {
        this.search = new QueryParams();

        this.addTableQuery();

        this.search.set(this.constants.CountryCode, this.utils.getCountryCode());
        this.search.set(this.constants.BusinessCode, this.utils.getBusinessCode());
        this.search.set(this.constants.Action, EGenericSearchXHRParams.action);
        if (this.maxRecords) {
            this.search.set(this.constants.PageSize, this.maxRecords);
        }
    }

    private addTableQuery(): void {
        this.search.set(EGenericSearchConfigConstants.c_s_TABLE_NAME,
            this.query[EGenericSearchConfigConstants.c_s_TABLE_NAME]);

        if (!this.query.conditions || !this.query.conditions.length) {
            return;
        }

        this.query.conditions.forEach(item => {
            this.search.set(item[EGenericSearchConfigConstants.c_s_FIELD_NAME],
                item[EGenericSearchConfigConstants.c_s_FIELD_VALUE]);

            if (item[EGenericSearchConfigConstants.c_s_OPERATOR]) {
                this.search.set(EGenericSearchConfigConstants.c_s_CONDITION_PARAM_NAME + item[EGenericSearchConfigConstants.c_s_FIELD_NAME],
                    item[EGenericSearchConfigConstants.c_s_OPERATOR]);
            }
        });
    }
    //#endregion

    //#region Public Methods
    /**
     * @method init
     * @description Initializes The Query
     * @param {IGenericSearchQuery} query Query Structure
     */
    public init(query: IGenericSearchQuery): void {
        this.query = query;

        this.buildQuery();
    }

    /**
     * @method fetch
     * @description Executes The Query And Returns Result As Promise
     * @returns Promise<any>
     */
    public fetch(): Promise<any> {
        return this.http.xhrGet(EGenericSearchXHRParams.method,
            EGenericSearchXHRParams.module,
            EGenericSearchXHRParams.operation,
            this.search);
    }
    //#endregion
}
//#endregion
