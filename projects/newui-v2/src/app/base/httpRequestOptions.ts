/***
 * @todo: Resolve dependency with AuthService (Create a abstract class)
 */

import { AuthService } from '../../shared/services/auth.service';
import { environment } from '../../environments/environment';
import { HttpParams, HttpParameterCodec, HttpHeaders } from '@angular/common/http';
import { Injector } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ServiceConstants } from '../../shared/constants/service.constants';

class PayloadEncoder implements HttpParameterCodec {
    encodeKey(key: string): string {
        return encodeURIComponent(key);
    }

    encodeValue(value: string): string {
        return encodeURIComponent(value);
    }

    decodeKey(key: string): string {
        return decodeURIComponent(key);
    }

    decodeValue(value: string): string {
        return decodeURIComponent(value);
    }
}

//#region Interfaces

export interface IParameterType {
    params?: HttpParams;
    headers: HttpHeaders;
}

export interface IHttpParts {
    prefix: string;
    suffix: string;
}

//#endregion

/**
 * @enum HttpRequestConstants
 * @description Values for different segments of url
 */
export class HttpServiceUrl {
    public static readonly LINE_OF_BUSINESS: IHttpParts = {
        prefix: 'lineofbusiness-service-dot',
        suffix: '.appspot.com/service/lineofbusiness/1.0/api/'
    };
    public static readonly TERRITORY_MANAGEMENT: IHttpParts = {
        prefix: 'territory-service-dot',
        suffix: '.appspot.com/service/territory-management/1.0/api/vehiclecomponents/'
    };

    public static readonly DEPOT_MAINTENANCE: IHttpParts = {
        prefix: 'territory-service-dot',
        suffix: '.appspot.com/service/territory-management/1.0/api/depots/'
    };
    public static readonly ICABS_PROXY: IHttpParts = {
        prefix: '',
        suffix: '.appspot.com/api/nextgenui/1.0/api/x/'
    };

    public static readonly IMPORT: IHttpParts = {
        prefix: 'import-service-dot',
        suffix: '.appspot.com/service/imports/1.0/api/'
    };

    public static readonly PRODUCT_SERVICE_GROUP: IHttpParts = {
        prefix: 'productservicegroup-service-dot',
        suffix: '.appspot.com/service/productservicegroup/1.0/api/'
    };
}

export interface IWebWorkerData {
    url: string;
    method: string;
    token: string;
    useClientId: boolean;
    search?: QueryParams;
    postHeaders?: IWebWorkerHeaders;
    formData?: string | Record<string, string>;
}

export interface IWebWorkerHeaders {
    module?: string;
    operation?: string;
}

export interface IBuildWorkerOptions {
    shouldIgnoreSecrets?: boolean;
    shouldSerializeBody?: boolean;
}

export class HttpRequestOptions {
    private endpoint: string;
    private pathParams = [];
    public search: QueryParams;
    public bodyParams = {};
    public httpMethod = '';
    public header = {};
    private service: IHttpParts;
    private authService: AuthService;
    private options: IParameterType;
    private formData: HttpParams;
    private ignoreSecretKey: boolean = false;

    // getGcpEnvironment will return prod if region falls in this array.
    private prodRegionList: Array<string> = ['australia', 'hongkong', 'indonesia'];

    @LocalStorage('SETUP_INFO', {})
    private localStore: Record<string, any>;

    constructor(private injector: Injector, private serviceParam: any) {
        this.service = serviceParam;
        this.authService = injector.get(AuthService);
        this.search = new QueryParams();
    }

    //#region Private methods for url value creation

    private getEnvironment(currentEnv: string, region?: string): string {
        switch (currentEnv.toLowerCase()) {
            case 'dev':
                return 'dev';
            case 'devonqa':
            case 'qa':
                return 'test';
            case 'devonsg':
            case 'staging':
                return 'staging';
            case 'devontraining':
            case 'training':
                return 'staging2';
            case 'prod':
                return region && this.prodRegionList.indexOf(region.toLowerCase()) >= 0 ? 'prod' : 'production';
            default:
                return 'dev';
        }
    }

    private getRegion(chosenRegion: string): string {
        switch (chosenRegion.toLowerCase()) {
            case 'asia':
                return 'asia';
            case 'india':
            case 'asiawest':
                return 'india';
            case 'india2':
                return 'india2';
            case 'pacific':
            case 'australia':
                return 'australia';
            case 'hongkong':
                return 'hongkong';
            case 'indonesia':
                return 'indonesia';
            case 'north america':
                return 'us';
            default:
                return 'europe';
        }
    }
    //#endregion

    public get Url(): string {
        const prefix: string = this.service.prefix ? this.service.prefix + '-' : '';
        const regionCode: string = this.localStore.regionCode.code;

        return 'https://'
            + prefix
            + 'icabsui-api-'
            + this.getRegion(regionCode)
            + '-'
            + this.getEnvironment(environment['NODE_ENV'], regionCode)
            + this.service.suffix
            + (this.endpoint ? this.endpoint + '/' : '')
            + this.pathParams.join('/');
    }

    public static create(injector: Injector, service: any): HttpRequestOptions {
        return new HttpRequestOptions(injector, service);
    }

    public addPathParam(value: string): HttpRequestOptions {
        this.pathParams.push(value);
        return this;
    }

    public addEndpoint(value: string): HttpRequestOptions {
        this.endpoint = value;
        return this;
    }

    public addQueryParams(searchData: QueryParams): HttpRequestOptions {
        this.search = searchData;
        return this;
    }

    public addQueryParam(name: any, value: any): HttpRequestOptions {
        this.search.set(name, value);
        return this;
    }

    public addBodyParams(formData: Record<string, string | boolean | number>): HttpRequestOptions {
        this.bodyParams = formData;
        return this;
    }

    public addBodyParam(name: string, value: any): HttpRequestOptions {
        this.bodyParams[name] = value;
        return this;
    }

    public addMethodType(methodType: string): HttpRequestOptions {
        this.httpMethod = methodType;
        return this;
    }

    public addService(service: IHttpParts): HttpRequestOptions {
        this.service = service;
        return this;
    }

    public addPageHeader(name: string, value: any): HttpRequestOptions {
        this.header[name] = value;
        return this;
    }

    public addIgnoreSecretKey(status: boolean): HttpRequestOptions {
        this.ignoreSecretKey = status;
        return this;
    }

    public addIgnoreList(key: string): HttpRequestOptions {
        delete this.bodyParams[key];
        return this;
    }

    public get FormData(): HttpParams {
        this.formData = new HttpParams({
            encoder: new PayloadEncoder(),
            fromObject: this.bodyParams
        });
        return this.formData;
    }

    public get Options(): IParameterType {
        const constants = new ServiceConstants();
        const token: string = this.authService.getToken();
        if (!token) {
            return;
        }
        if (!this.ignoreSecretKey) {
            this.header['client_id'] = environment['MULE_CLIENT_ID'] ? environment['MULE_CLIENT_ID'] : '';
            this.header['client_secret'] = environment['MULE_CLIENT_SECRET'] ? environment['MULE_CLIENT_SECRET'] : '';
        }
        this.header[constants.Authorization] = 'Bearer ' + this.authService.getToken();
        if (this.httpMethod === 'POST' || this.httpMethod === 'PUT') {
            this.header[constants.ContentType] = 'application/x-www-form-urlencoded';
        }
        this.options = {
            ...this.options,
            headers: new HttpHeaders(this.header)
        };

        if (this.search && this.search.keys().length) {
            this.search.set(constants.email, this.authService.getSavedEmail());
            this.options = {
                ...this.options,
                params: new HttpParams({
                    encoder: new PayloadEncoder(),
                    fromObject: this.search.getMap()
                })
            };
        }

        return this.options;
    }

    public buildWorkerData(options?: IBuildWorkerOptions): IWebWorkerData {

        const workerData: IWebWorkerData = {
            url: this.Url,
            method: this.httpMethod,
            token: this.authService.getToken(),
            useClientId: options && options.shouldIgnoreSecrets
        };

        if (this.header) {
            workerData.postHeaders = this.header;
        }
        if (this.bodyParams) {
            if (options && options.shouldSerializeBody) {
                let qString: string = '';
                Object.keys(this.bodyParams).forEach(key => {
                    if (qString) {
                        qString += '&';
                    }
                    qString += encodeURIComponent(key) + '=' + encodeURIComponent(this.bodyParams[key]);
                });
                workerData.formData = qString;

            } else {
                workerData.formData = this.bodyParams;
            }
        }
        if (this.search && this.search.keys().length) {
            workerData.search = this.search;
        }

        return workerData;
    }
}
