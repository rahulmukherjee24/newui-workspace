export interface IFormSchema {
    name: string;
    programURL: string;
    configPath: string;
}

export interface IForm {
    controls: IControl [];
    title: string ;
    service: IServiceType;
    instructions: ITypeInstruction[];
}

export interface IControl {
    type: string;
    name: string;
    label: string;
    required: boolean;
    disable: boolean;
    hidden?: boolean;
    default: string;
    list?: IList;
    maxLength: number;
    colSize: number;
    description?: string;
    value?: string | Date ;
    isPrimary?: boolean;
}

export interface IList {
    value:	IStaticOptions[];
    lookUp?: ILookUpOptions;
}

export interface IStaticOptions {
    text: string;
    value: string | number | boolean;
}

export interface ILookUpOptions {
    table: string;
    query: Record < string, string > ;
    fields: string[];
}

export interface IDate {
    value: string;
    delta: string;
}

export interface IServiceType {
    operation: string;
    module: string;
    action: number;
    extraParameters?: string[];
}

export interface ITypeInstruction {
    heading?: string ;
    values: ITypeSubInstruction[];
}

export interface ITypeSubInstruction {
    main: string;
    sub?: string[];
}


