import { Renderer2, Injector } from '@angular/core';

import { ICabsModalVO } from '../../shared/components/modal-adv/modal-adv-vo';
import { LightBaseComponent } from './BaseComponentLight';
import { MessageConstant } from '../../shared/constants/message.constant';
import { MntConst } from '../../shared/services/riMaintenancehelper';
import { ModalAdvService } from '../../shared/components/modal-adv/modal-adv.service';

export class CommonMaintenanceFunction {

    private modalAdvService: ModalAdvService;
    private renderer: Renderer2;

    constructor(private maintenanceComponent: LightBaseComponent, private injector: Injector) {
        if (!this.modalAdvService) {
            this.modalAdvService = this.injector.get(ModalAdvService);
        }
        if (!this.renderer) {
            this.renderer = this.injector.get(Renderer2);
        }
    }

    /**
     * @method onAddClick
     * @param hasTab - Pass true if the maintenance page has tab
     * @param tabToBeFocused - Id of the tab to be focused
     */
    public onAddClick(hasTab: boolean = false, tabToBeFocused?: string, ignoreArray: string[] = []): void {
        this.setPageMode(MntConst.eModeAdd, null, ignoreArray);
        if (hasTab) {
            this.focusToTab(tabToBeFocused);
            setTimeout(() => {
                this.clearErrorTabs();
            }, 200);
        }
    }

    /**
     * @method onSaveClick - Called when user clicks on the Save Button
     * @param confirmCallback - Confirmation call back function
     * @param cancelCallback - Cancellation call back function
     * @param hasTab - Whether the maintenance page has tab
     * @param tabList - Tabs of Maintenance page
     */
    public onSaveClick(confirmCallback: any, cancelCallback?: any, hasTab: boolean = false, tabList?: any): void {
        if (hasTab && !(this.maintenanceComponent['riExchange'].validateForm(this.maintenanceComponent['uiForm']))) {
            setTimeout(() => {
                this.selectErrorTab(tabList);
            }, 100);
        }

        if (this.maintenanceComponent['riExchange'].validateForm(this.maintenanceComponent['uiForm'])) {
            if (hasTab) {
                setTimeout(() => {
                    this.clearErrorTabs();
                }, 200);
            }
            this.modalAdvService.emitPrompt(
                new ICabsModalVO(
                    MessageConstant.Message.ConfirmRecord, null,
                    confirmCallback, cancelCallback ? cancelCallback : () => { this.onPromptCancelClick(); }
                )
            );
        }
    }

    /**
     * @method onCancelClick - Called when user clicks on the Cancel Button
     * @param tempdata - Data to be set to the Form
     * @param callbackFunc - Callback function to be called during cancellation of add mode
     * @param hasTab - Whether the maintenance page has tab
     * @param tabToBeFocused - Id of the tab to be focused
     */
    public onCancelClick(tempdata: Record<string, string>, callbackFunc: any, hasTab: boolean = false, tabToBeFocused?: string): void {
        if (hasTab) {
            setTimeout(() => {
                this.clearErrorTabs();
            }, 200);
        }
        if (this.maintenanceComponent['pageParams']['pageMode'] === MntConst.eModeAdd) {
            if (hasTab) {
                this.focusToTab(tabToBeFocused);
            }
            if (callbackFunc) {
                callbackFunc();
            }
        } else {
            this.maintenanceComponent.setFormValue(tempdata);
        }
    }

    /**
     * @method onDeleteClick - Called when user click on the Delete button
     * @param confirmCallback - Confirmation call back function
     * @param cancelCallback - Cancellation call back function
     */
    public onDeleteClick(confirmCallback: any, cancelCallback?: any): void {
        this.maintenanceComponent['pageParams']['pageMode'] = MntConst.eModeDelete;
        this.modalAdvService.emitPrompt(
            new ICabsModalVO(
                MessageConstant.Message.DeleteRecord, null,
                confirmCallback, cancelCallback ? cancelCallback : () => { this.onPromptCancelClick(); }
            )
        );
    }

    /**
     * @method onPromptCancelClick - Called when user click on the cancel button of Prompt Modal
     */
    private onPromptCancelClick(): void {
        if (this.maintenanceComponent['pageParams']['pageMode'] === MntConst.eModeDelete) {
            this.maintenanceComponent['pageParams']['pageMode'] = MntConst.eModeUpdate;
        }
    }
    /**
     * @method setPageMode - Used to set page mode for maintenance page
     * @param mode - Page mode to be set
     * @param data - Data to be set to form
     */
    public setPageMode(mode: string, data?: any, ignoreArray: string[] = []): void {
        data ? this.maintenanceComponent.setFormValue(data) : this.maintenanceComponent.clearControls(ignoreArray, true);
        switch (mode) {
            case MntConst.eModeAdd:
                this.maintenanceComponent['pageParams']['pageMode'] = MntConst.eModeAdd;
                break;
            case MntConst.eModeSelect:
                this.maintenanceComponent['pageParams']['pageMode'] = MntConst.eModeSelect;
                break;
            case MntConst.eModeUpdate:
                this.maintenanceComponent['pageParams']['pageMode'] = MntConst.eModeUpdate;
                break;
        }
    }

    /**
     * @method focusToTab - Used to set focus to a tab by ID
     * @param tabId - Id of the tab to be focused
     */
    public focusToTab(tabId: string): void {
        let element = document.querySelector('.nav-tabs li#' + tabId + ' a');
        if (element) {
            this.renderer.selectRootElement(element, true).click();
        }
    }

    /**
     * @method setFocusToTabElement - Used to set focus to a tab
     */
    public setFocusToTabElement(): void {
        setTimeout(() => {
            let elemList = document.querySelectorAll('.screen-body .nav-tabs li a');
            let currentSelectedIndex = Array.prototype.indexOf.call(
                elemList, document.querySelector('.screen-body .nav-tabs li a.active')
            );
            let nextTab = currentSelectedIndex + 1;
            let tabItemList = document.querySelectorAll(
                '.screen-body .tab-content .tab-pane:nth-child(' + nextTab +
                ') .ui-select-toggle, .screen-body .tab-content .tab-pane:nth-child(' + nextTab +
                ') input:not([disabled]), .screen-body .tab-content .tab-pane:nth-child(' + nextTab +
                ') select:not([disabled])'
            );

            for (let l = 0; l < tabItemList.length; l++) {
                let el = tabItemList[l];
                if (el) {
                    const type: string = el.getAttribute('type');
                    if (
                        (type === undefined) ||
                        (type === null) ||
                        (type && type.toLowerCase() !== 'hidden' && type.toLowerCase() !== 'button')
                    ) {
                        setTimeout(() => {
                            el['focus']();
                        }, 90);
                        break;
                    }
                }
            }
        }, 100);
    }

    /**
     * @method selectErrorTab - Used to select the error tab
     * @param tabList - Tabs of maintenance page
     */
    private selectErrorTab(tabList: any): void {
        let i = 0;
        let isFocusedToTabCalled = false;
        let elem = document.querySelector('.nav-tabs').children;
        for (let tab in tabList) {
            if (tab) {
                let element = document.querySelectorAll(
                    `div#${tabList[tab]['id']} input.ng-invalid.ng-touched,
                    div#${tabList[tab]['id']} textarea.ng-invalid.ng-touched,
                    div#${tabList[tab]['id']} select.ng-invalid.ng-touched`
                );

                if (element && element.length > 0) {
                    if (!isFocusedToTabCalled) {
                        this.focusToTab(tabList[tab]['id']);
                        isFocusedToTabCalled = true;
                    }
                    if (elem[i]) {
                        if (!this.maintenanceComponent['utils'].hasClass(elem[i], 'error')) {
                            this.maintenanceComponent['utils'].addClass(elem[i], 'error');
                        }
                    }
                }
                else {
                    if (elem[i]) {
                        if (this.maintenanceComponent['utils'].hasClass(elem[i], 'error')) {
                            this.maintenanceComponent['utils'].removeClass(elem[i], 'error');
                        }
                    }
                }
                i++;
            }
        }
    }

    /**
     * @method onSaveFocus - User to move focus to next tab if next tab present, else focuses the save button
     * @param event - Keyboard event
     */
    public onSaveFocus(event: any): void {
        let nextTab: number = 0;
        let code = (event.keyCode ? event.keyCode : event.which);
        let elemList = document.querySelectorAll('.screen-body .nav-tabs li a');
        let currentSelectedIndex = Array.prototype.indexOf.call(elemList,
            document.querySelector('.screen-body .nav-tabs li a.active'));
        if (code === 9 && currentSelectedIndex < (elemList.length - 1)) {
            nextTab = currentSelectedIndex + 1;
            if (elemList[nextTab])
                elemList[nextTab]['click']();
        }
    }

    /**
     * @method clearErrorTabs - User to clear the error from tab
     */
    public clearErrorTabs(): void {
        let elem = document.querySelector('.nav-tabs').children;
        for (let i = 0; i < elem.length; i++) {
            if (this.maintenanceComponent['utils'].hasClass(elem[i], 'error')) {
                this.maintenanceComponent['utils'].removeClass(elem[i], 'error');
            }
        }
    }
}
