export class DropdownConstants {
    public static readonly c_s_KEY_MULTISELECTVALUE: string = 'multiSelectValue';
    public static readonly c_s_KEY_MULTISELECTDISPLAY: string = 'multiSelectDisplay';
}
