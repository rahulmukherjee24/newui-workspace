import { OnInit, ViewChild, OnDestroy, AfterContentInit, NgZone } from '@angular/core';

import { AjaxConstant } from './../../shared/constants/AjaxConstants';
import { AjaxObservableConstant } from './../../shared/constants/ajax-observable.constant';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GlobalizeService } from './../../shared/services/globalize.service';
import { GlobalNotificationsService } from '@shared/services/global.notifications.service';
import { HttpService } from './../../shared/services/http-service';
import { Injector } from '@angular/core';
import { LocaleTranslationService } from './../../shared/services/translation.service';
import { LookUp } from './../../shared/services/lookup';
import { MntConst } from './../../shared/services/riMaintenancehelper';
import { NavData } from './../../shared/services/navigationData';
import { Observable } from 'rxjs';
import { QueryParams } from '../../shared/services/http-params-wrapper';
import { RiExchange } from './../../shared/services/riExchange';
import { RouteAwayGlobals } from '../../shared/services/route-away-global.service';
import { RouteCallback } from '../base/Callback';
import { Router, ActivatedRoute } from '@angular/router';
import { ServiceConstants } from './../../shared/constants/service.constants';
import { Subscription } from 'rxjs';
import { Utils } from './../../shared/services/utility';

export abstract class LightBaseComponent implements OnInit, OnDestroy, AfterContentInit, RouteCallback {
    // Subscription variable
    protected ajaxSource = new BehaviorSubject<any>(0);
    protected ajaxSource$;
    protected isReturningFlag: boolean = false;

    //Inject classes
    protected localeTranslateService: LocaleTranslationService;
    protected utils: Utils;
    protected httpService: HttpService;
    protected serviceConstants: ServiceConstants;
    protected router: Router;
    protected activatedRoute: ActivatedRoute;
    protected formBuilder: FormBuilder;
    protected riExchange: RiExchange;
    protected LookUp: LookUp;
    protected globalize: GlobalizeService;
    protected activatedRouteSubscription: Subscription;
    private notification: GlobalNotificationsService;
    public zone: NgZone;

    //Parametrs to be saved
    public pageParams: any = {};
    protected attributes: any = {};
    protected formData: any = {};

    //Variables to be used all classes
    protected pageTitle: string = '';
    protected abstract pageId: string = '';
    protected uiForm: FormGroup;
    protected abstract controls = [];
    protected parentMode = '';
    protected browserTitle: string = '';

    protected navUrl: string = '';
    protected isRequesting: boolean = false;

    //canDeactivateRoute service
    protected routeAwayGlobals: RouteAwayGlobals;

    //Regional Settings control types
    protected controlDataTypes = {};
    protected ajaxSubscription: Subscription;
    protected ajaxconstant: AjaxObservableConstant;

    //Grid Column List Parameters
    public dateCollist: string = '';
    public durationCollist: string = '';
    public dateTimeCollist: string = '';

    constructor(injector: Injector) {
        this.injectServices(injector);
    }

    public ngOnInit(): void {
        this.ajaxSource$ = this.ajaxSource.asObservable();
        this.localeTranslateService.setUpTranslation();
        this.uiForm = this.formBuilder.group({});
        if (this.pageParams === undefined) {
            this.pageParams = {};
        }
        this.pageParams['vBusinessCode'] = this.utils.getBusinessCode();
        this.pageParams['vCountryCode'] = this.utils.getCountryCode();
        this.pageParams['gUserCode'] = this.utils.getUserCode();
        this.isReturningFlag = false;
        this.riExchange.setRouteCallback(this);

        this.activatedRouteSubscription = this.activatedRoute.queryParams.subscribe(
            (param: any) => {
                if (param) {
                    this.riExchange.setRouterParams(param);
                }
            });

        this.ajaxSubscription = this.ajaxSource$.subscribe(event => {
            if (event !== 0) {
                switch (event) {
                    case AjaxConstant.START:
                        this.isRequesting = true;
                        break;
                    case AjaxConstant.COMPLETE:
                        this.isRequesting = false;
                        break;
                }
            }
        });

        this.setUp();
        this.riExchange.renderForm(this.uiForm, this.controls);
        if (this.browserTitle) {
            this.utils.setTitle(this.browserTitle);
        }
        this.setCommonAttributes();
    }

    public ngAfterContentInit(): void {
        this.handleBackStack(this.pageId);
    }

    public ngOnDestroy(): void {
        //Release memory
        this.httpService = null;
        this.router = null;
        this.utils = null;

        //Unsubscribe
        this.ajaxSource = null;
        if (this.activatedRouteSubscription) {
            this.activatedRouteSubscription.unsubscribe();
        }
        this.routeAwayGlobals.resetRouteAwayFlags();
    }

    private injectServices(injector: Injector): void {
        this.localeTranslateService = injector.get(LocaleTranslationService);
        this.activatedRoute = injector.get(ActivatedRoute);
        this.utils = injector.get(Utils);
        this.httpService = injector.get(HttpService);
        this.serviceConstants = injector.get(ServiceConstants);
        this.router = injector.get(Router);
        this.formBuilder = injector.get(FormBuilder);
        this.riExchange = injector.get(RiExchange);
        this.LookUp = injector.get(LookUp);
        this.routeAwayGlobals = injector.get(RouteAwayGlobals);
        this.globalize = injector.get(GlobalizeService);
        this.ajaxconstant = injector.get(AjaxObservableConstant);
        this.notification = injector.get(GlobalNotificationsService);
        this.zone = injector.get(NgZone);
    }

    private setUp(): void {
        this.parentMode = this.riExchange.getParentMode();
        if (this.pageParams) {
            this.pageParams['currentContractType'] = this.riExchange.getCurrentContractType();
            this.pageParams['currentContractTypeLabel'] = this.riExchange.getCurrentContractTypeLabel();
        }
    }


    public isReturning(): boolean {
        return this.isReturningFlag;
    }

    /**
     * Return QueryParams with business code and country code
     */

    public getURLSearchParamObject(): QueryParams {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        return search;
    }

    public businessCode(): string {
        return this.utils.getBusinessCode();
    }

    public countryCode(): string {
        return this.utils.getCountryCode();
    }

    /************ Wrappers For riExchange.riInputElement - Start ************/
    /**
     * Wrapper to riExchange.riInputElement.GetValue; Don't need to pass the form name
     * @method getControlValue
     * @param control - Control name to get value
     * @param doTypeCheck - Converts value if checkbox type
     * @return string
     */
    public getControlValue(control: string, doTypeCheck?: boolean): any {
        let type: string = this.controlDataTypes[control] || '';
        let value: any;

        value = this.riExchange.riInputElement.GetValue(this.uiForm, control, type);

        if (doTypeCheck) {
            if (type === MntConst.eTypeCheckBox) {
                value = this.utils.convertResponseValueToCheckboxInput(value);
            }
        }
        return value;
    }
    /**
     * Wrapper to riExchange.riInputElement.SetValue; Don't need to pass the form name
     * @method setControlValue
     * @param control - Control name to get value
     * @param value - Value to set
     * @return void
     */
    public setControlValue(control: string, value: any): void {
        let type: string = this.controlDataTypes[control] || '';
        if (type === MntConst.eTypeCheckBox) {
            try {
                value = this.utils.convertResponseValueToCheckboxInput(value);
            } catch (excp) {
                console.log(control, excp);
            }
        }
        this.riExchange.riInputElement.SetValue(this.uiForm, control, value, type);
    }

    /**
     * Wrapper to riExchange.riInputElement.SetRequiredStatus; Don't need to pass the form name
     * @method setRequiredStatus
     * @param control - Control name to get value
     * @param status - Boolean Value to set
     * @return void
     */
    public setRequiredStatus(control: string, status: boolean): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, control, status);
        this.riExchange.updateCtrl(this.controls, control, 'required', status);
    }
    /**
     * Wrapper to riExchange.riInputElement.Enable/this.riExchange.riInputElement.Disable; Don't need to pass the form name
     * @method disableControl
     * @param control - Control name to get value
     * @param disable - Flag to enable/disable
     * @return void
     */
    public disableControl(control: string, disable: boolean): void {
        if (disable) {
            this.riExchange.riInputElement.Disable(this.uiForm, control);
            return;
        }
        this.riExchange.riInputElement.Enable(this.uiForm, control);
    }
    /**
     * disable controls in the form except the controls in ignore Array
     * @method disableControls
     * @param ignore - Array of controls not to be disabled
     * @return void
     */
    public disableControls(ignore: Array<string>): void {
        for (let control in this.uiForm.controls) {
            if (!control || ignore.indexOf(control) >= 0) {
                continue;
            } else {
                this.riExchange.riInputElement.Disable(this.uiForm, control);
            }
        }
    }

    /**
     * enable controls in the form except the controls in ignore Array
     * @method enableControls
     * @param ignore
     * @return void
     */

    public enableControls(ignore: Array<string>): void {
        for (let control in this.uiForm.controls) {
            if (!control || ignore.indexOf(control) >= 0) {
                continue;
            } else {
                this.riExchange.riInputElement.Enable(this.uiForm, control);
            }
        }
    }

    /**
     * Wrapper to riExchange.riInputElement.SetMarkedAsTouched; Don't need to pass the form name
     * @method markControlAsTouched
     * @param control - Control name to get value
     * @return void
     */
    public markControlAsTouched(control: string): void {
        this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, control, true);
        return;
    }
    public markControlAsUnTouched(control: string): void {
        this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, control, false);
        return;
    }

    /**
     * Wrapper to riExchange.riInputElement.SetMarkedAsTouched; Don't need to pass the form name
     * @method markControlAsDirty
     * @param control - Control name to get value
     * @return void
     */
    public markControlAsDirty(control: string): void {
        this.uiForm.controls[control].markAsDirty();
        return;
    }

    /**
     * Wrapper to riExchange.riInputElement.isCorrect; Don't need to pass the form name
     * Checks if form control has any validation errors
     * @method hasNoError
     * @param control - Control name to check error
     * @return Boolean
     */
    public hasNoError(control: string): boolean {
        return this.riExchange.riInputElement.isCorrect(this.uiForm, control);
    }

    /**
     * @method isCOntrolChecked
     * @description Check if control is checked
     * @param control Control name
     * @return boolean
     */
    public isControlChecked(control: string): boolean {
        return this.riExchange.riInputElement.checked(this.uiForm, control);
    }

    /************ Wrappers For riExchange.riInputElement - End ************/

    /**
     * Clears controls in the form except the controls in ignore Array
     * @method clearControls
     * @param ignore - Array of controls not to be cleared
     * @return void
     */
    public clearControls(ignore: Array<string>, markPristine?: boolean): void {
        for (let control in this.uiForm.controls) {
            if (!control || ignore.indexOf(control) >= 0) {
                continue;
            }
            this.setControlValue(control, '');
            this.uiForm.controls[control].markAsUntouched();
            if (markPristine) {
                this.uiForm.controls[control].markAsPristine();
            }
        }
    }

    /**
     * Set form value
     * @method setFormValue
     * @param data - Data to be set to form controls
     * @return void
     */
    public setFormValue(data: any): void {
        if (data) {
            for (const control in this.uiForm.controls) {
                if (control && data.hasOwnProperty(control)) {
                    this.setControlValue(control, data[control]);
                }
            }
            this.formPristine();
        }
    }

    public handleBookmark(field: string, data: string): void {
        // Function used as part of interface
    }

    /**
     * Check back stack and handle stack data
     */
    public handleBackStack(pageId: string): any {
        let data: NavData = this.riExchange.getLastStackData();
        if (data && pageId === data.getPageId()) { //Back stack has data for the page
            this.isReturningFlag = true;
            this.riExchange.popNavigationData();
            this.pageParams = data.getPageData();
            this.formData = data.getFormData();
            this.attributes = data.getPageAttributes();
            if (data.getControls()) {
                this.controls = data.getControls();
            }
            if (this.pageParams) {
                this.parentMode = this.pageParams.parentMode;
            }
            this.populateUIFromFormData();
        } else {
            return {};
        }
    }

    public populateUIFromFormData(): void {
        for (let i = 0; i < this.controls.length; i++) {
            if (this.formData[this.controls[i].name]) {
                this.riExchange.riInputElement.SetValue(this.uiForm,
                    this.controls[i].name, this.formData[this.controls[i].name]);
            }
        }
    }


    public setAttribute(field: string, value: any): void {
        this.attributes[field] = value;
    }

    public getAttribute(field: string): any {
        return this.attributes[field];
    }

    public nullValidate(field: any): any {
        let nullValidator: any = new RegExp('[^null |$]');
        if (!nullValidator.test(field.value)) {
            return { 'invalidValue': true };
        }
        return null;
    }

    /**
     * Method to navigate to a new page
     */
    public navigate(exchangeMode: string, path: string, queryParams?: any): void {
        let data = new NavData();
        let urlExtraParams: any = queryParams || {};
        this.pageParams.parentMode = this.parentMode;
        data.setPageData(this.pageParams);
        data.setExchangeMode(exchangeMode);
        data.setBackLabel(this.pageTitle);
        data.setPageId(this.pageId);
        data.setFormData(this.uiForm.getRawValue());
        data.setPageAttributes(this.attributes);
        data.setBackRoute(this.navUrl);
        data.setControls(this.createControlObjectFromForm());
        this.riExchange.pushInNavigationData(data);
        if (queryParams) {
            queryParams.parentMode = exchangeMode;
        } else {
            urlExtraParams.parentMode = exchangeMode;
        }
        this.riExchange.clearRouterParams();
        this.router.navigate([path], {
            queryParams: urlExtraParams
        });
    }

    public createControlObjectFromForm(): any[] {
        let tempControl: any[] = [];
        for (let key in this.uiForm.controls) {
            if (key) {
                let type = this.riExchange.getCtrlType(this.controls, key);
                let _validator: any = this.uiForm.controls[key].validator && this.uiForm.controls[key].validator(this.uiForm.controls[key]);
                let required: boolean = _validator && _validator.required;
                let obj = {
                    name: key,
                    readonly: this.uiForm.controls[key]['readonly'],
                    disabled: this.uiForm.controls[key]['disabled'],
                    required: required,
                    type: type,
                    value: this.uiForm.controls[key]['value']
                };
                tempControl.push(obj);
            }
        }
        return tempControl;
    }

    @ViewChild('routeAwayComponent') public routeAwayComponent;
    public canDeactivate(): Observable<boolean> {
        this.routeAwayGlobals.setSaveEnabledFlag(this.uiForm.dirty);
        if (this.routeAwayComponent) {
            return this.routeAwayComponent.canDeactivate();
        }
    }

    public formPristine(): void {
        this.uiForm.markAsPristine();
    }

    public setCommonAttributes(field: string = ''): void {
        if (field) {
            if (this.uiForm.controls[field])
                this.uiForm.controls[field].setValidators([this.utils.commonValidate]);
        } else {
            for (let c of this.controls) {
                if (c['commonValidator'] && c['commonValidator'] === true) {
                    this.uiForm.controls[c.name].setValidators([this.utils.commonValidate]);
                }
                if (c['commonValidator'] && c['commonValidator'] === true && c['required']) {
                    this.uiForm.controls[c.name].setValidators([this.utils.commonValidate, this.nullValidate]);
                }
                if (c['type'] && c['type'] !== '') {
                    this.controlDataTypes[c['name']] = c['type'];
                }
            }
        }

    }

    /*
    check for dirty controls in the form except the controls in ignore Array
    @method checkDirtyControls
    @param ignore - Array of controls not to be checked
    @return void
    */
    public checkDirtyControls(ignore: Array<string>): boolean {
        for (let control in this.uiForm.controls) {
            if (ignore.indexOf(control) < 0) {
                if (this.uiForm.controls[control].dirty)
                    return true;
            }
        }
    }

    public getCurrentPage(currentPage: any, doNotRefreshGrid?: boolean): void {
        if (!this.isReturning() || this.pageParams.gridCurrentPage !== currentPage.value) {
            this.pageParams.gridCurrentPage = currentPage.value;
            if (!doNotRefreshGrid) {
                this['onRiGridRefresh']();
            }
        }
    }

    protected displayMessage(msg: string | any, type?: string): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.notification.displayMessage(msg, type);
    }

    public hasError(data?: any): boolean {
        if (!data) {
            return true;
        } else if (data.hasOwnProperty('errorMessage') || data.hasOwnProperty('fullError')) {
            return true;
        }
    }
}

export interface IGridHandlers {
    onRiGridRefresh(): void;
    onGridBodyDoubleClick?(event?: any): void;
    onHeaderClick?(event?: any): void;
}
