import { FormGroup } from '@angular/forms';
import { NavData } from '../../shared/services/navigationData';
import { RiExchange } from '../../shared/services/riExchange';

export interface StateRetentionParams {
    populateForm?: boolean;
    uiFormGroupName?: string;
    pageDataPropName?: string;
    storeDataPropName?: string;
    pageAttributesPropName?: string;
}

export class StateRetentionFunction {

    constructor(private component: any, private riExchange: RiExchange, private stateRetentionParams: StateRetentionParams) {
        if (!stateRetentionParams.pageDataPropName && component.hasOwnProperty('pageParams')) {
            stateRetentionParams.pageDataPropName = 'pageParams';
        }
        if (typeof stateRetentionParams.populateForm !== 'boolean') {
            stateRetentionParams.populateForm = true;
        }
    }

    public isReturing(): boolean {
        const data: NavData = this.riExchange.getLastStackData();
        return data && this.component.pageId === data.getPageId();
    }

    public getLastStackData(): NavData {
        return this.riExchange.getLastStackData();
    }

    public getFormData(): Object {
        return this.riExchange.getLastStackData().getFormData();
    }

    public getPageData(): Object {
        return this.riExchange.getLastStackData().getPageData();
    }

    public getStoreData(): Object {
        return this.riExchange.getLastStackData().getStoreData();
    }

    public getPageAttributes(): Object {
        return this.riExchange.getLastStackData().getPageAttributes();
    }

    public popNavigationData(): void {
        this.riExchange.popNavigationData();
    }

    public applyStateRetention(): void {
        const data: NavData = this.riExchange.getLastStackData();
        if (this.stateRetentionParams.pageDataPropName) {
            this.component[this.stateRetentionParams.pageDataPropName] = data.pageData;
        }
        if (this.stateRetentionParams.storeDataPropName) {
            this.component[this.stateRetentionParams.storeDataPropName] = data.storeData;
        }
        if (this.stateRetentionParams.pageAttributesPropName) {
            this.component[this.stateRetentionParams.pageAttributesPropName] = data.pageAttributes;
        }
        this.populateUIFromFormData(data.formData);
        this.popNavigationData(); // clear pagestack
    }

    public populateUIFromFormData(formData: Object): void {
        if (this.stateRetentionParams.populateForm && this.stateRetentionParams.uiFormGroupName) {
            const uiFormGroup = this.component[this.stateRetentionParams.uiFormGroupName] as FormGroup;
            uiFormGroup.setValue(formData);
        }
    }

    public setDataForStateRetention(): void {
        let navData = new NavData();
        navData.setBackLabel(this.component.pageTitle || this.getPageTitle());
        navData.setPageId(this.component.pageId);
        if (this.stateRetentionParams.uiFormGroupName) {
            const uiFormGroup = this.component[this.stateRetentionParams.uiFormGroupName] as FormGroup;
            navData.setFormData(uiFormGroup.getRawValue());
        }
        if (this.stateRetentionParams.pageDataPropName) {
            navData.setPageData(this.component[this.stateRetentionParams.pageDataPropName]);
        }
        if (this.stateRetentionParams.storeDataPropName) {
            navData.setStoreData(this.component[this.stateRetentionParams.storeDataPropName]);
        }
        if (this.stateRetentionParams.pageAttributesPropName) {
            navData.setPageAttributes(this.component[this.stateRetentionParams.pageAttributesPropName]);
        }
        // check if already exist, if so then delete earlier one and save new one
        if (this.isReturing()) {
            this.popNavigationData();
        }
        this.riExchange.pushInNavigationData(navData);
    }

    private getPageTitle(): string {
        const titleElement = document.querySelector('.page-title h1')! as HTMLElement;
        return titleElement.textContent;
    }
}
