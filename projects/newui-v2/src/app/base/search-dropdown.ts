export class SearchDropdown {
    public responseData: any;
    public displayFields: any;
    public active: any;
    public isActive: any;
    public preValue: string;

    public setValue(code: string): void {
        this.preValue = code;
        if (!this.responseData) {
            return;
        }
        for (let i = 0; i < this.responseData.length; i++) {
            let element: any = this.responseData[i];
            if (element[this.displayFields[0]] === code) {
                this.active = this.isActive = {
                    id: code,
                    text: code + ' - ' + element[this.displayFields[1]]
                };
                this.preValue = '';
                break;
            }
        }
    }
}
