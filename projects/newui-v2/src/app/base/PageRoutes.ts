export class AppModuleRoutes {
    public static readonly ACCOUNTADMIN = 'accountadmin/';
    public static readonly ACCOUNTMAINTENANCE = 'accountmaintenance/';
    public static readonly AREAS = 'areas/';
    public static readonly BILLTOCASH = '/billtocash/';
    public static readonly CCM = '/ccm/';
    public static readonly CONTRACTADMIN = 'contractadmin/';
    public static readonly CONTRACTMANAGEMENT = '/contractmanagement/';
    public static readonly CUSTOMERINFO = 'customerinfo/';
    public static readonly EXTRANETSORCONNECT = '/extranetsorconnect/';
    public static readonly GENERAL = 'general/';
    public static readonly GOOGLE_MAP_PAGES = 'googlemap/';
    public static readonly GRID = '/grid/';
    public static readonly GRID_APPLICATION = 'grid/application/';
    public static readonly GRID_MENUSERVICE = 'grid/service/nav/';
    public static readonly GRID_SALES = 'grid/sales/';
    public static readonly GRID_SERVICE = 'grid/service/';
    public static readonly GRID_SPLITAPPLICATION = 'grid/application/nav/';
    public static readonly GRID_SPLITSALES = 'grid/sales/nav/';
    public static readonly GRID_SPLITSERVICE = 'grid/service/drilldown/';
    public static readonly GROUPACCOUNT = 'groupaccount/';
    public static readonly INTERNAL_MAINTENANCE = 'maintenance/';
    public static readonly ITFUNCTIONS = '/itfunctions/';
    public static readonly LOGIN = '/application/login';
    public static readonly SETUP = '/application/setup';
    public static readonly MAINTENANCE = 'maintenance/';
    public static readonly MAINTENANCE_APPLICATION = 'application/';
    public static readonly MAINTENANCE_SALES = 'sales/';
    public static readonly MAINTENANCE_SERVICE = 'service/';
    public static readonly PEOPLE = '/people/';
    public static readonly POSTLOGIN = '/postlogin';
    public static readonly PREMISESADMIN = 'premisesadmin/';
    public static readonly PREMISESMAINTENANCE = 'premisesmaintenance/';
    public static readonly PRODUCTADMIN = 'productadmin/';
    public static readonly PRODUCTSALE = 'productsale/';
    public static readonly PROSPECTTOCONTRACT = '/prospecttocontract/';
    public static readonly RENEGOTIATIONS = '/renegotiations/';
    public static readonly REPORTS = 'reports/';
    public static readonly RETENTION = 'retention/';
    public static readonly SERVICECOVERADMIN = 'servicecoveradmin/';
    public static readonly SERVICECOVERMAINTENANCE = 'servicecovermaintenance/';
    public static readonly SERVICEDELIVERY = '/servicedelivery/';
    public static readonly SERVICEPLANNING = '/serviceplanning/';
    public static readonly BIREPORTS_PDA_LEAD = '/pda-lead/';
    public static readonly BIREPORTS_SALES = '/bireports/sales/';
    public static readonly BIREPORTS_PORTFOLIO = '/bireports/portfolio/';
    public static readonly BIREPORTS_PDA = 'bireports/pda';
    public static readonly BIREPORTS_INVOICE = 'bireports/invoice/';
    public static readonly BIREPORTS_SERVICE = 'bireports/service/';
    public static readonly CCMMOBILE = '/ccm-mobile/';
    public static readonly BIREPORTS_TURNOVER = '/bireports/turnover/';
    public static readonly BIREPORTS_CREDITANALYSIS = '/bireports/creditanalysis/';
    public static readonly BIREPORTS_ACTUALVSCONTRACTUAL = '/bireports/actualvscontractual/';
    public static readonly BIREPORTS_CLIENTRETENTION = '/bireports/clientretention/';
    public static readonly BIREPORTS_CONTACTMANAGEMENT = '/bireports/cm/';
    public static readonly VEHICLE_MANAGEMENT = '/vehiclemanagement/';
    public static readonly BIREPORTS_API = '/api/';
    public static readonly BIREPORTS_STOCKUSAGE = '/bireports/stockusage/';
    public static readonly RIBATCH = 'riBatch/';
}

export class BillToCashModuleRoutes {
    public static readonly ICABSAINVOICEGROUPMAINTENANCE = 'maintenance/invoicegroup/search';
    public static readonly ICABSASERVICECOVERACCEPTGRID = 'servicecover/acceptGrid';
    public static readonly ICABSARVARIANCEFROMPRICECOSTGRID = 'variancefromprice';
    public static readonly ICABSARGENERATENEXTINVOICERUNFORECAST = 'application/invoiceRunForecastComponent';
    public static readonly ICABSAINVOICEDETAILSMAINTENANCE = 'maintenance/invoiceDetailsMaintainance';
    public static readonly ICABSACONTRACTINVOICEGRID = AppModuleRoutes.BILLTOCASH + 'contract/invoice';
    public static readonly ICABSSESERVICEVISITRELEASEGRID = 'shared/visitreleasegrid';
}

export class ContractManagementModuleRoutes {
    public static readonly ICABSAPREMISESELECTMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.RETENTION + 'deletePremise/premiseSelectMaintenance';
    public static readonly ICABSAMULTIPREMISEPURCHASEORDERAMEND = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESADMIN + 'contract/multipremise/purchaseorderamend';
    public static readonly ICABSASERVICEVISITDETAILSUMMARY = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.SERVICECOVERADMIN + 'serviceVisitDetail/serviceVisitDetailSummaryGrid';
    public static readonly ICABSASERVICECOVERFREQUENCYMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.SERVICECOVERADMIN + 'maintenance/ServiceCoverFrequencyMaintenance';
    public static readonly ICABSAACCOUNTMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.ACCOUNTMAINTENANCE + 'account/maintenance';
    public static readonly ICABSAACCOUNTASSIGN = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.ACCOUNTADMIN + 'account/assign/search';
    public static readonly ICABSAACCOUNTMERGE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.ACCOUNTADMIN + 'account/merge/search';
    public static readonly ICABSAACCOUNTMOVE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.ACCOUNTADMIN + 'account/move/search';
    public static readonly ICABSACONTRACTINVOICEDETAILGRID = AppModuleRoutes.CONTRACTMANAGEMENT + 'contractInvoice/ContractInvoiceDetailGrid';
    public static readonly ICABSAPREMISEMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESMAINTENANCE + 'maintenance/premise';
    public static readonly ICABSACONTRACTMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + 'maintenance/contract';
    public static readonly ICABSAJOBMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + 'maintenance/job';
    public static readonly ICABSAPREMISESERVICESUSPENDMAINTENANCE = 'servicesuspendmaintenance';
    public static readonly ICABSAPNOLSITEREFERENCEMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.SERVICECOVERADMIN + 'application/pnolSiteRef';
    public static readonly ICABSAPRODUCTSALEMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + 'maintenance/product';
    public static readonly ICABSASERVICECOVERYTDMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.SERVICECOVERADMIN + 'servicecover/YTDMaintenance';
    public static readonly ICABSRENEWALEXTRACTGENERATION = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.REPORTS + 'sales/renewalextractgeneration';
    public static readonly ICABSSEPREMISECONTACTCHANGEGRID = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESADMIN + 'PDAReturns/premiseContactChange';
    public static readonly ICABSASERVICECOVERTRIALPERIODRELEASEGRID = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.SERVICECOVERADMIN + 'servicecover/TialPeriodRelease';
    public static readonly ICABSBPOSTCODEMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.AREAS + 'branch/postalcode/maintenance';
    public static readonly ICABSBPOSTCODESGRID = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.AREAS + 'branchgeography/postcodesgrid';
    public static readonly ICABSBSALESAREAGRID = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.AREAS + 'salesAreaGrid';
    public static readonly ICABSSGROUPACCOUNTMOVE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.GROUPACCOUNT + 'account/groupaccountmove';
    public static readonly ICABSSEDATACHANGEGRID = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.CUSTOMERINFO + 'customerdataupdate/datachangegrid';
    public static readonly ICABSAPOSTCODEMOVEBRANCH = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESADMIN + 'premise/postcodemovebranch';
    public static readonly ICABSSSALESSTATSADJUSTMENTGRID = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.REPORTS + 'salesMaintenance/renegAdjustments';
    public static readonly ICABSASERVICECOVERSELECTMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.RETENTION + 'clientRetention/serviceCoverSelectMaintenance';
    public static readonly ICABSACONTRACTSELECTMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.RETENTION + 'maintenance/ContractSelectMaintenance';
    public static readonly ICABSSGROUPACCOUNTMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.GROUPACCOUNT + 'account/groupaccountmaintenance';
    public static readonly ICABSAPRODUCTCODEUPGRADE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.SERVICECOVERADMIN + 'contractservicecover/productupgrade';
    public static readonly ICABSASERVICECOVERINVOICEONFIRSTVISITMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.SERVICECOVERADMIN + 'maintenance/serviceCoverInvoiceMaintenceOnFirstVisit';
    public static readonly ICABSASERVICECOVERMAINTENANCE = AppModuleRoutes.SERVICECOVERMAINTENANCE + 'maintenance/servicecover';
    public static readonly ICABSASERVICECOVERMAINTENANCECONTRACT = AppModuleRoutes.SERVICECOVERMAINTENANCE + 'maintenance/servicecover/contract';
    public static readonly ICABSASERVICECOVERMAINTENANCEJOB = AppModuleRoutes.SERVICECOVERMAINTENANCE + 'maintenance/servicecover/job';
    public static readonly ICABSASERVICECOVERMAINTENANCEREDUCE = AppModuleRoutes.SERVICECOVERMAINTENANCE + 'maintenance/servicecover/reduce';
    public static readonly ICABSAINACTIVESERVICECOVERINFOMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.RETENTION + 'maintenance/inactiveservicecover';

    public static readonly ICABSAMULTIPREMISESPECIAL = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESADMIN + 'application/multiPremisesSpecial';
    public static readonly ICABSANEGBRANCHMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.CONTRACTADMIN + 'negbranchmaintenance';
    public static readonly ICABSAPERCENTAGEPRICECHANGE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.SERVICECOVERADMIN + 'percentagepricechange/perchanges/:type';
    public static readonly ICABSACONTRACTANNIVERSARYCHANGE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.CONTRACTADMIN + 'contractAnniversaryChange';
    public static readonly ICABSARBRANCHCONTRACTREPORT = 'contractForBranchReport';
    public static readonly ICABSAMASSPRICECHANGEGRID = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.SERVICECOVERADMIN + 'application/massPriceChangeGrid';
    public static readonly ICABSBPRODUCTCOVERMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PRODUCTADMIN + 'business/service/productcover';
    public static readonly ICABSAINACTIVEPREMISEINFOMAINTENANCE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.RETENTION + 'inactive/premiseinfo';
    public static readonly ICABSAINACTIVEPREMISEINFOMAINTENANCE_CONTRACT_CANCEL = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.RETENTION + 'inactive/premiseinfo/contract/cancel';
    public static readonly ICABSAINACTIVEPREMISEINFOMAINTENANCE_JOB_CANCEL = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.RETENTION + 'inactive/premiseinfo/job/cancel';
    public static readonly ICABSAINACTIVEPREMISEINFOMAINTENANCE_PRODUCT_CANCEL = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.RETENTION + 'inactive/premiseinfo/product/cancel';
    public static readonly ICABSAINACTIVEPREMISEINFOMAINTENANCE_REINSTATE = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.RETENTION + 'inactive/premiseinfo/reinstate';

    public static readonly ICABSAPREMISESELECTMAINTENANCE_SUB = 'deletePremise/premiseSelectMaintenance';
    public static readonly ICABSAMULTIPREMISEPURCHASEORDERAMEND_SUB = 'contract/multipremise/purchaseorderamend';
    public static readonly ICABSASERVICEVISITDETAILSUMMARY_SUB = 'serviceVisitDetail/serviceVisitDetailSummaryGrid';
    public static readonly ICABSASERVICECOVERFREQUENCYMAINTENANCE_SUB = 'maintenance/ServiceCoverFrequencyMaintenance';
    public static readonly ICABSAACCOUNTMAINTENANCE_SUB = 'account/maintenance';
    public static readonly ICABSAACCOUNTASSIGN_SUB = 'account/assign/search';
    public static readonly ICABSAACCOUNTMERGE_SUB = 'account/merge/search';
    public static readonly ICABSAACCOUNTMOVE_SUB = 'account/move/search';
    public static readonly ICABSACONTRACTINVOICEDETAILGRID_SUB = 'contractInvoice/ContractInvoiceDetailGrid';
    public static readonly ICABSAPREMISEMAINTENANCE_SUB = 'maintenance/premise';
    public static readonly ICABSACONTRACTMAINTENANCE_SUB = 'maintenance/contract';
    public static readonly ICABSAJOBMAINTENANCE_SUB = 'maintenance/job';
    public static readonly ICABSAPNOLSITEREFERENCEMAINTENANCE_SUB = 'application/pnolSiteRef';
    public static readonly ICABSAPRODUCTSALEMAINTENANCE_SUB = 'maintenance/product';
    public static readonly ICABSASERVICECOVERYTDMAINTENANCE_SUB = 'servicecover/YTDMaintenance';
    public static readonly ICABSRENEWALEXTRACTGENERATION_SUB = 'sales/renewalextractgeneration';
    public static readonly ICABSCUSTOMERLETTERBYTYPE = 'customerletters/bytype';
    public static readonly ICABSSEPREMISECONTACTCHANGEGRID_SUB = 'PDAReturns/premiseContactChange';
    public static readonly ICABSASERVICECOVERTRIALPERIODRELEASEGRID_SUB = 'servicecover/TialPeriodRelease';
    public static readonly ICABSBPOSTCODEMAINTENANCE_SUB = 'branch/postalcode/maintenance';
    public static readonly ICABSBPOSTCODESGRID_SUB = 'branchgeography/postcodesgrid';
    public static readonly ICABSBSALESAREAGRID_SUB = 'salesAreaGrid';
    public static readonly ICABSSGROUPACCOUNTMOVE_SUB = 'account/groupaccountmove';
    public static readonly ICABSSEDATACHANGEGRID_SUB = 'customerdataupdate/datachangegrid';
    public static readonly ICABSAPOSTCODEMOVEBRANCH_SUB = 'premise/postcodemovebranch';
    public static readonly ICABSSSALESSTATSADJUSTMENTGRID_SUB = 'salesMaintenance/renegAdjustments';
    public static readonly ICABSASERVICECOVERSELECTMAINTENANCE_SUB = 'clientRetention/serviceCoverSelectMaintenance';
    public static readonly ICABSACONTRACTSELECTMAINTENANCE_SUB = 'maintenance/ContractSelectMaintenance';
    public static readonly ICABSSGROUPACCOUNTMAINTENANCE_SUB = 'account/groupaccountmaintenance';
    public static readonly ICABSAPRODUCTCODEUPGRADE_SUB = 'contractservicecover/productupgrade';
    public static readonly ICABSASERVICECOVERINVOICEONFIRSTVISITMAINTENANCE_SUB = 'maintenance/serviceCoverInvoiceMaintenceOnFirstVisit';
    public static readonly ICABSASERVICECOVERMAINTENANCE_SUB = 'maintenance/servicecover';
    public static readonly ICABSAMULTIPREMISESPECIAL_SUB = 'application/multiPremisesSpecial';
    public static readonly ICABSANEGBRANCHMAINTENANCE_SUB = 'negbranchmaintenance';
    public static readonly ICABSAPERCENTAGEPRICECHANGECONTRACT_SUB = 'percentagepricechange/perchanges/Contract';
    public static readonly ICABSAPERCENTAGEPRICECHANGEPREMISE_SUB = 'percentagepricechange/perchanges/Premise';
    public static readonly ICABSAPERCENTAGEPRICECHANGEJOB_SUB = 'percentagepricechange/perchanges/ServiceCover';
    public static readonly ICABSACONTRACTANNIVERSARYCHANGE_SUB = 'contractAnniversaryChange';
    public static readonly ICABSAMASSPRICECHANGEGRID_SUB = 'application/massPriceChangeGrid';
    public static readonly ICABSASERVICECOVERMAINTENANCECONTRACT_SUB = 'maintenance/servicecover/contract';
    public static readonly ICABSASERVICECOVERMAINTENANCEJOB_SUB = 'maintenance/servicecover/job';
    public static readonly ICABSASERVICECOVERMAINTENANCEREDUCE_SUB = 'maintenance/servicecover/reduce';
    public static readonly ICABSBPRODUCTCOVERMAINTENANCE_SUB = 'business/service/productcover';
    public static readonly ICABSCMGENERALSEARCHGRID_SUB = 'generalsearchgrid';
    public static readonly ICABSBWASTECONSIGN_SUB = 'business/waste/grid';
    public static readonly ICABSBWASTECONSIGNMENT = 'contractmanagement/wasteconsign/';
    public static readonly ICABSAINACTIVESERVICECOVERINFOMAINTENANCE_SUB = {
        DEFAULT: 'maintenance/inactiveservicecover',
        CANCEL: 'maintenance/inactiveservicecover/cancel',
        REINSTATE: 'maintenance/inactiveservicecover/reinstate',
        DELETE: 'maintenance/inactiveservicecover/delete',
        CANCEL_JOB: 'maintenance/inactiveservicecover/canceljob',
        CANCEL_PRODUCT: 'maintenance/inactiveservicecover/cancelproduct'
    };

    public static readonly ICABSAINACTIVECONTRACTINFOMAINTENANCE_SUB = 'inactive/contractinfo';
    public static readonly ICABSAINACTIVECONTRACTINFOMAINTENANCECANCEL_SUB = 'inactive/contractinfo/cancel';
    public static readonly ICABSAINACTIVECONTRACTINFOMAINTENANREINSTATEL_SUB = 'inactive/contractinfo/reinstate';
    public static readonly ICABSBPRODUCTEXPENSEMAINTENANCE = 'product/expense/maintenance';
    public static readonly ICABSASERVICECOVERUNSUSPENDGRID = 'serviceProcesses/deliveryConfirmation';
    public static readonly ICABSAINACTIVEPREMISEINFOMAINTENANCE_SUB = 'inactive/premiseinfo';
    public static readonly ICABSAINACTIVEPREMISEINFOMAINTENANCE_CONTRACT_CANCEL_SUB = 'inactive/premiseinfo/contract/cancel';
    public static readonly ICABSAINACTIVEPREMISEINFOMAINTENANCE_JOB_CANCEL_SUB = 'inactive/premiseinfo/job/cancel';
    public static readonly ICABSAINACTIVEPREMISEINFOMAINTENANCE_PRODUCT_CANCEL_SUB = 'inactive/premiseinfo/product/cancel';
    public static readonly ICABSAINACTIVEPREMISEINFOMAINTENANCE_REINSTATE_SUB = 'inactive/premiseinfo/reinstate';
    public static readonly ICABSBSALESAREAPOSTCODEREZONEGRID_SUB = 'rezone/salesarea/postcode';
    public static readonly ICABSBSALESAREAREZONEGRID_SUB = 'rezone/salesarea/grid';
    public static readonly ICABSBWASTECONSIGNMENTNOTERANGETYPEMAINTENANCE_SUB = 'note/range/type';

    public static readonly ICABSBSALESAREAPOSTCODEREZONEGRID = AppModuleRoutes.GENERAL + ContractManagementModuleRoutes.ICABSBSALESAREAPOSTCODEREZONEGRID_SUB;
    public static readonly ICABSBSALESAREAREZONEGRID = AppModuleRoutes.GENERAL + ContractManagementModuleRoutes.ICABSBSALESAREAREZONEGRID;
    public static readonly ICABSBWASTECONSIGNMENTNOTERANGETYPEMAINTENANCE = '/contractmanagement/wasteconsign/' + ContractManagementModuleRoutes.ICABSBWASTECONSIGNMENTNOTERANGETYPEMAINTENANCE_SUB;
    public static readonly ICABSBCONTRACTSALESEMPLOYEEREASSIGNGRID = 'business/contractSalesEmployeeReassignGrid';
    public static readonly ICABSARCUSTOMERLETTERSFORTYPEGRID_SUB = 'customerletters/fortype';
    public static readonly ICABSARCUSTOMERLETTERSFORTYPEGRID = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.REPORTS + ContractManagementModuleRoutes.ICABSARCUSTOMERLETTERSFORTYPEGRID_SUB;
    public static readonly ICABSBWASTECONSIGNNOTEHISTORYGRID = 'note/history/grid';
    public static readonly ICABSBWASTECONSIGNNOTEHISTORYGRIDFULL = ContractManagementModuleRoutes.ICABSBWASTECONSIGNMENT + ContractManagementModuleRoutes.ICABSBWASTECONSIGNNOTEHISTORYGRID;
    public static readonly ICABSLINEOFBUSINESSMAINTENANCE_SUB = 'lineofbusiness';
}

export class CCMModuleRoutes {
    public static readonly ICABSCMSMSMESSAGESGRID = 'service/smsmessages';
    public static readonly ICABSCONTACTMEDIUMGRID = 'service/contactmedium';
    public static readonly ICABSCMCONTACTACTIONMAINTENANCE = 'service/contactActionMaintenance';
    public static readonly ICABSCMCALLANALYSISGRID = 'contactmanagement/callAnalysisGrid';
    public static readonly SENDBULKSMSBUSINESS = 'sendbulksms/business';
    public static readonly SENDBULKSMSBRANCH = 'sendbulksms/branch';
    public static readonly SENDBULKSMSACCOUNT = 'sendbulksms/account';
    public static readonly ICABSCMCONTACTREDIRECTION = 'contactmanagement/scmContactRedirection';
    public static readonly ICABSCMCONTACTREDIRECTIONMAINTENANCE = 'contactmanagement/scmContactRedirectionMaintenance';
    public static readonly ICABSCMCALLCENTREASSIGNGRID = 'customerContact/callCentreAssignGrid';
    public static readonly ICABSCMWORKORDERREVIEWGRID = 'workOrderReviewGrid';
    public static readonly ICABSATELESALESORDERGRID = 'customerContact/telesalesordergrid';
    public static readonly ICABSCMCALLCENTREGRIDNEWCONTACT = 'contactManagement/CallCentreGridNewContact';
    public static readonly ICABSCMCALLCENTREGRID = 'callcentersearch';
    public static readonly ICABSCMCALLCENTREREVIEWGRID = 'centreReview';
    public static readonly ICABSCMCUSTOMERCONTACTCALLOUTGRID = 'customercontact/callout/grid';
    public static readonly ICABSSNOTIFICATIONGROUPMAINTENANCE = 'notification/group/maintenance';
    public static readonly ICABSSCONTACTTYPEMAINTENANCE = 'system/contact/contacttype';
    public static readonly ICABSSNOTIFICATIONTEMPLATEMAINTENANCE = 'system/maintenance/notificationtemplate';
    public static readonly ICABSSNOTIFICATIONTEMPLATEGRID = 'system/grid/notificationtemplate';
}

export class ExtranetsOrConnectModuleRoutes {
}

export class InternalGridSearchModuleRoutesConstant {
    public static readonly ICABSSESUMMARYWORKLOADGRIDMONTHBRANCH = {
        URL_1: 'summary/workload',
        URL_2: 'summary/workload/reroute'
    };
    public static readonly ICABSBSERVICEAREAREZONEGRID = 'servicearea/rezone';
    public static readonly ICABSARRETURNEDPAPERWORKTYPEGRID = 'returnPaperWorkTypeGrid';
    public static readonly ICABSBPREPARATIONMIXGRID = 'prep/preparationmixgrid';
    public static readonly ICABSSESERVICEPLANNINGDETAILGRIDHG = 'service/servicePlanningDetailGridHg';
    public static readonly ICABSSEWASTECONSIGNMENTNOTEGRID = 'wasteConsignmentNotegrid';
}

export class InternalGridSearchModuleRoutes {
    public static readonly ICABSBSERVICEAREAREZONEGRID = AppModuleRoutes.GRID + InternalGridSearchModuleRoutesConstant.ICABSBSERVICEAREAREZONEGRID;
    public static readonly ICABSSESUMMARYWORKLOADGRIDMONTHBRANCH = {
        URL_1: InternalGridSearchModuleRoutesConstant.ICABSSESUMMARYWORKLOADGRIDMONTHBRANCH.URL_1,
        URL_2: InternalGridSearchModuleRoutesConstant.ICABSSESUMMARYWORKLOADGRIDMONTHBRANCH.URL_2
    };
    public static readonly ICABSARRETURNEDPAPERWORKTYPEGRID = InternalGridSearchModuleRoutesConstant.ICABSARRETURNEDPAPERWORKTYPEGRID;
    public static readonly ICABSSESERVICEPLANNINGDETAILGRIDHG = AppModuleRoutes.GRID + InternalGridSearchModuleRoutesConstant.ICABSSESERVICEPLANNINGDETAILGRIDHG;
    public static readonly ICABSBPREPARATIONMIXGRID = AppModuleRoutes.GRID + InternalGridSearchModuleRoutesConstant.ICABSBPREPARATIONMIXGRID;
    public static readonly ICABSSEWASTECONSIGNMENTNOTEGRID = AppModuleRoutes.GRID + InternalGridSearchModuleRoutesConstant.ICABSSEWASTECONSIGNMENTNOTEGRID;
}

export class InternalGridSearchSalesModuleRoutesConstant {
    public static readonly ICABSCMPROSPECTCONVGRID = 'prospectconvgrid';
    public static readonly ICABSSPIPELINESERVICECOVERGRID = 'PipelineServiceCoverGridComponent';
    public static readonly ICABSSPIPELINEPREMISEGRID = 'PipelinePremiseGridComponent';
    public static readonly ICABSSSOQUOTEGRID = 'ssoQuoteGrid';
    public static readonly ICABSSDLCONTRACTAPPROVALGRID = 'contractapprovalgrid';
    public static readonly ICABSSAPPROVALBUSINESS = 'approvalbusiness';
    public static readonly ICABSSSALESSTATISTICSSERVICEVALUEDETAILADJUSTGRID = 'statisticsServiceValueDetailAdjustGrid';
    public static readonly ICABSSSOSERVICECOVERGRID = 'SOServiceCoverGrid';
    public static readonly ICABSSPIPELINEQUOTEGRID = 'PipelineQuoteGrid';
    public static readonly ICABSARENVAGENCYQUARTERLYRETURN = 'envagencyquarterlyreturn';
    public static readonly ICABSALINKEDPRODUCTSGRID = 'LinkedProductsGrid';
    public static readonly ICABSSSOPREMISEGRID = 'SOPremiseGrid';
    public static readonly ICABSSDLHISTORYGRID = 'DLHistoryGridComponent';
    public static readonly ICABSAEMPTYPREMISELOCATIONSEARCHGRID = 'EmptyPremiseLocationSearchGrid';
    public static readonly ICABSASTATICVISITGRIDDAY = 'service/staticVisitGridDay';
    public static readonly ICABSASTATICVISITGRIDYEAR = 'service/StaticVisitGridYear';
    public static readonly ICABSAPRORATACHARGESUMMARY = 'proRatacharge/summary';
    public static readonly ICABSACONTRACTHISTORYGRID = 'contract/history';
    public static readonly ICABSAPLANVISITGRIDYEAR = 'contract/planVisitGridYear';
    public static readonly ICABSAPLANVISITGRIDDAY = 'contract/planVisitGridDay';
    public static readonly ICABSAPRODUCTSALESSCDETAILMAINTENANCE = 'productSalesSCDetailsMaintenance';
    public static readonly ICABSAPREMISESERVICESUMMARY = 'premiseServiceSummary';
    public static readonly ICABSBSALESAREAPOSTCODEGRID = 'SalesAreaPostCode';
    public static readonly ICABSARELATEDVISITGRID = 'relatedVisitGrid';
    public static readonly ICABSPREMISEACCESSTIMESGRID = 'PremiseAccessTimesGrid';
    public static readonly ICABSSCONTACTTYPEDETAILESCALATEGRID = 'contactTypeDetailEscalateGrid';
    public static readonly ICABSSCONTACTTYPEDETAILPROPERTIESGRID = 'contactTypeDetailPropertiesGrid';
    public static readonly ICABSACONTRACTINVOICETURNOVERGRID = 'contactinvoice/turnover';
    public static readonly ICABSBUSERAUTHORITYBRANCHGRID = 'Userauthoritybranch';
    public static readonly ICABSAPRORATACHARGEBRANCHGRID = 'prorata/charge/branch';
    public static readonly ICABSBINVOICERANGEUPDATEGRID = 'invoice/range/update/grid';
    public static readonly ICABSSESERVICEPLANNINGSUMMARYGRID = 'service/planning/summary/grid';
    public static readonly ICABSSESERVICEPATTERNGRID = 'servicepattern/grid';
    public static readonly ICABSSEPREMISEACTIVITYGRID = 'premiseactivity/grid';
    public static readonly ICABSSESERVICEAREASEQUENCEGRID = 'servicearea/sequence';
    public static readonly ICABSSESERVICEPLANGRID = 'serviceplan/grid';
    public static readonly ICABSSESERVICEPLANNINGEMPLOYEETIMEGRIDHG = 'service/planning/employeetime/grid/hg';
    public static readonly ICABSSSALESSTATISTICSSERVICEVALUEGRID = {
        CONTRACT: 'statistics/servicevalue/contract',
        JOB: 'statistics/servicevalue/job',
        PRODUCT: 'statistics/servicevalue/product'
    };
    public static readonly ICABSSESERVICEPLANSUMMARYDETAILGRID = 'serviceplan/summary/detail';
}

export class InternalGridSearchSalesModuleRoutes {
    public static readonly ICABSCMPROSPECTCONVGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSCMPROSPECTCONVGRID;
    public static readonly ICABSSPIPELINESERVICECOVERGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSPIPELINESERVICECOVERGRID;
    public static readonly ICABSSPIPELINEPREMISEGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSPIPELINEPREMISEGRID;
    public static readonly ICABSSSOQUOTEGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSSOQUOTEGRID;
    public static readonly ICABSSDLCONTRACTAPPROVALGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSDLCONTRACTAPPROVALGRID;
    public static readonly ICABSSAPPROVALBUSINESS = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSAPPROVALBUSINESS;
    public static readonly ICABSSSALESSTATISTICSSERVICEVALUEDETAILADJUSTGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSSALESSTATISTICSSERVICEVALUEDETAILADJUSTGRID;
    public static readonly ICABSSSOSERVICECOVERGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSSOSERVICECOVERGRID;
    public static readonly ICABSSPIPELINEQUOTEGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSPIPELINEQUOTEGRID;
    public static readonly ICABSARENVAGENCYQUARTERLYRETURN = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSARENVAGENCYQUARTERLYRETURN;
    public static readonly ICABSALINKEDPRODUCTSGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSALINKEDPRODUCTSGRID;
    public static readonly ICABSSSOPREMISEGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSSOPREMISEGRID;
    public static readonly ICABSSDLHISTORYGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSDLHISTORYGRID;
    public static readonly ICABSAEMPTYPREMISELOCATIONSEARCHGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSAEMPTYPREMISELOCATIONSEARCHGRID;
    public static readonly ICABSASTATICVISITGRIDDAY = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSASTATICVISITGRIDDAY;
    public static readonly ICABSASTATICVISITGRIDYEAR = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSASTATICVISITGRIDYEAR;
    public static readonly ICABSAPRORATACHARGESUMMARY = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSAPRORATACHARGESUMMARY;
    public static readonly ICABSACONTRACTHISTORYGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSACONTRACTHISTORYGRID;
    public static readonly ICABSAPLANVISITGRIDYEAR = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSAPLANVISITGRIDYEAR;
    public static readonly ICABSAPLANVISITGRIDDAY = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSAPLANVISITGRIDDAY;
    public static readonly ICABSAPRODUCTSALESSCDETAILMAINTENANCE = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSAPRODUCTSALESSCDETAILMAINTENANCE;
    public static readonly ICABSAPREMISESERVICESUMMARY = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSAPREMISESERVICESUMMARY;
    public static readonly ICABSBSALESAREAPOSTCODEGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSBSALESAREAPOSTCODEGRID;
    public static readonly ICABSARELATEDVISITGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSARELATEDVISITGRID;
    public static readonly ICABSPREMISEACCESSTIMESGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSPREMISEACCESSTIMESGRID;
    public static readonly ICABSSCONTACTTYPEDETAILESCALATEGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSCONTACTTYPEDETAILESCALATEGRID;
    public static readonly ICABSSCONTACTTYPEDETAILPROPERTIESGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSCONTACTTYPEDETAILPROPERTIESGRID;
    public static readonly ICABSACONTRACTINVOICETURNOVERGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSACONTRACTINVOICETURNOVERGRID;
    public static readonly ICABSBUSERAUTHORITYBRANCHGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSBUSERAUTHORITYBRANCHGRID;
    public static readonly ICABSAPRORATACHARGEBRANCHGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSAPRORATACHARGEBRANCHGRID;
    public static readonly ICABSBINVOICERANGEUPDATEGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSBINVOICERANGEUPDATEGRID;
    public static readonly ICABSSESERVICEPLANNINGSUMMARYGRID = AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEPLANNINGSUMMARYGRID;
    public static readonly ICABSSESERVICEPATTERNGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEPATTERNGRID;
    public static readonly ICABSSEPREMISEACTIVITYGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSEPREMISEACTIVITYGRID;
    public static readonly ICABSSESERVICEAREASEQUENCEGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEAREASEQUENCEGRID;
    public static readonly ICABSSESERVICEPLANGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEPLANGRID;
    public static readonly ICABSSESERVICEPLANNINGEMPLOYEETIMEGRIDHG = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEPLANNINGEMPLOYEETIMEGRIDHG;
    public static readonly ICABSSSALESSTATISTICSSERVICEVALUEGRID = {
        CONTRACT: AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSSALESSTATISTICSSERVICEVALUEGRID.CONTRACT,
        JOB: AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSSALESSTATISTICSSERVICEVALUEGRID.JOB,
        PRODUCT: AppModuleRoutes.GRID_SPLITSALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSSALESSTATISTICSSERVICEVALUEGRID.PRODUCT
    };
    public static readonly ICABSSESERVICEPLANSUMMARYDETAILGRID = AppModuleRoutes.GRID_SALES + InternalGridSearchSalesModuleRoutesConstant.ICABSSESERVICEPLANSUMMARYDETAILGRID;
}

export class GoogleMapPagesModuleRoutesConstant {
    public static readonly ICABSATECHNICIANVISITDIARYGRID = 'technician/visit/diary';
    public static readonly ICABSAROUTINGSEARCH = 'application/routingsearch';
    public static readonly ICABSAMULTITECHVISITDIARYGRID = 'technician/visit/diary/multi';
}

export class GoogleMapPagesModuleRoutes {
    public static readonly ICABSATECHNICIANVISITDIARYGRID = AppModuleRoutes.GOOGLE_MAP_PAGES + GoogleMapPagesModuleRoutesConstant.ICABSATECHNICIANVISITDIARYGRID;
    public static readonly ICABSAROUTINGSEARCH = AppModuleRoutes.GOOGLE_MAP_PAGES + GoogleMapPagesModuleRoutesConstant.ICABSAROUTINGSEARCH;
    public static readonly ICABSAMULTITECHVISITDIARYGRID = AppModuleRoutes.GOOGLE_MAP_PAGES + GoogleMapPagesModuleRoutesConstant.ICABSAMULTITECHVISITDIARYGRID;
}
export class InternalGridSearchApplicationModuleRoutesConstant {
    public static readonly ICABSAAPPLYAPICONTRACTGRID = 'applyapigrid';
    public static readonly ICABSASERVICECOVERSEASONGRID = 'servicecover/seasongrid';
    public static readonly ICABSAPNOLLEVELHISTORY = 'PNOLLevelHistory';
    public static readonly ICABSASERVICECOVERWASTEGRID = 'servicecover/wastegrid';
    public static readonly ICABSAINVOICEHEADERGRID = 'invoiceheadergridcomponent';
    public static readonly ICABSSASERVICECOVERDISPLAYGRID = 'serviceCoverDisplayGrid';
    public static readonly ICABSCMCALLCENTRESEARCHEMPLOYEEGRID = 'cmCallCentreSearchEmployeeGrid';
    public static readonly ICABSCMCALLCENTREGRIDNOTEPAD = 'callCentreGridNotepad';
    public static readonly ICABSATELESALESORDERLINEGRID = 'teleSalesOrderLineGrid';
    public static readonly ICABSACREDITAPPROVALGRID = 'creditApprovalGrid';
    public static readonly ICABSCMCALLANALYSISTICKETGRID = 'CallAnalysisTicketGridComponent';
    public static readonly ICABSALINKEDPREMISESUMMARYGRID = 'linkedPremiseSummaryGrid';
    public static readonly ICABSAINVOICEGROUPADDRESSAMENDMENTGRID = 'addressAmendment';
    public static readonly ICABSCLOSEDTEMPLATEDATEGRID = 'closedTemplateDateGrid';
    public static readonly ICABSCMCALLCENTERGRIDEMPLOYEEVIEW = 'callcentergridemployee';
    public static readonly ICABSACALENDARTEMPLATEBRANCHACCESSGRID = 'calendarTemplateBranchAccessGrid';
    public static readonly ICABSCMCUSTOMERCONTACTDETAILGRID = 'customer/contactdetail';
    public static readonly ICABSCMCALLCENTREGRIDCALLLOGDETAILVIEW = 'callcentregridcallLogdetailview';
    public static readonly ICABSBPREMISEPOSTCODEREZONEGRID = 'premisePostcodeRezoneGrid';
    public static readonly ICABSCMHCALEADGRID = 'leadgrid';
    public static readonly ICABSASERVICECOVERCALENDARDATEGRID = 'servicecover/calendardate';
    public static readonly ICABSASERVICECOVERCLOSEDDATEGRID = 'servicecovercloseddategrid';
    public static readonly ICABSAEMPLOYEEGRID = 'employeegrid';
    public static readonly ICABSBLISTGROUPSEARCH = 'ListGroupSearch';
    public static readonly ICABSCMACCOUNTREVIEWGRID = 'reportContacts/accountReviewGrid';
    public static readonly ICABSSEPLANVISITGRID = 'visitmaintenance/planvisitGrid';
    public static readonly ICABSSCONTACTTYPEDETAILASSIGNEEGRID = 'contactyypedetailassigneegrid';
    public static readonly ICABSATECHNICIANVISITDIARYGRID = 'technician/visit/diary';
    public static readonly ICABSAINVOICEBYACCOUNTGRID = 'invoice/account';
    public static readonly ICABSCMCUSTOMERCONTACTNOTIFICATIONSGRID = 'customercontact/notification';
    public static readonly ICABSSEDESPATCHGRID = 'despatchgrid';
    public static readonly ICABSASERVICECOVERUPDATEABLEGRID = 'serviceCoverUpdateableGrid';
    public static readonly ICABSSEPREMISEPDAICABSACTIVITYGRID = 'pdaactivitygrid';
    public static readonly ICABSSNOTIFICATIONGROUPGRID = 'notificationgroupgrid';
    public static readonly ICABSASERVICEVISITDISPLAYGRID = 'servicevisitdisplaygrid';
}

export class InternalGridSearchApplicationModuleRoutes {
    public static readonly ICABSAAPPLYAPICONTRACTGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSAAPPLYAPICONTRACTGRID;
    public static readonly ICABSASERVICECOVERSEASONGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICECOVERSEASONGRID;
    public static readonly ICABSAPNOLLEVELHISTORY = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSAPNOLLEVELHISTORY;
    public static readonly ICABSASERVICECOVERWASTEGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICECOVERWASTEGRID;
    public static readonly ICABSAINVOICEHEADERGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSAINVOICEHEADERGRID;
    public static readonly ICABSSASERVICECOVERDISPLAYGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSSASERVICECOVERDISPLAYGRID;
    public static readonly ICABSCMCALLCENTRESEARCHEMPLOYEEGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCALLCENTRESEARCHEMPLOYEEGRID;
    public static readonly ICABSCMCALLCENTREGRIDNOTEPAD = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCALLCENTREGRIDNOTEPAD;
    public static readonly ICABSATELESALESORDERLINEGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSATELESALESORDERLINEGRID;
    public static readonly ICABSACREDITAPPROVALGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSACREDITAPPROVALGRID;
    public static readonly ICABSCMCALLANALYSISTICKETGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCALLANALYSISTICKETGRID;
    public static readonly ICABSALINKEDPREMISESUMMARYGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSALINKEDPREMISESUMMARYGRID;
    public static readonly ICABSAINVOICEGROUPADDRESSAMENDMENTGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSAINVOICEGROUPADDRESSAMENDMENTGRID;
    public static readonly ICABSCLOSEDTEMPLATEDATEGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSCLOSEDTEMPLATEDATEGRID;
    public static readonly ICABSCMCALLCENTERGRIDEMPLOYEEVIEW = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCALLCENTERGRIDEMPLOYEEVIEW;
    public static readonly ICABSACALENDARTEMPLATEBRANCHACCESSGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSACALENDARTEMPLATEBRANCHACCESSGRID;
    public static readonly ICABSCMCUSTOMERCONTACTDETAILGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCUSTOMERCONTACTDETAILGRID;
    public static readonly ICABSCMCALLCENTREGRIDCALLLOGDETAILVIEW = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCALLCENTREGRIDCALLLOGDETAILVIEW;
    public static readonly ICABSBPREMISEPOSTCODEREZONEGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSBPREMISEPOSTCODEREZONEGRID;
    public static readonly ICABSCMHCALEADGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSCMHCALEADGRID;
    public static readonly ICABSASERVICECOVERCALENDARDATEGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICECOVERCALENDARDATEGRID;
    public static readonly ICABSASERVICECOVERCLOSEDDATEGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICECOVERCLOSEDDATEGRID;
    public static readonly ICABSAEMPLOYEEGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSAEMPLOYEEGRID;
    public static readonly ICABSBLISTGROUPSEARCH = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSBLISTGROUPSEARCH;
    public static readonly ICABSCMACCOUNTREVIEWGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSCMACCOUNTREVIEWGRID;
    public static readonly ICABSSEPLANVISITGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSSEPLANVISITGRID;
    public static readonly ICABSSCONTACTTYPEDETAILASSIGNEEGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSSCONTACTTYPEDETAILASSIGNEEGRID;
    public static readonly ICABSCMCUSTOMERCONTACTNOTIFICATIONSGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCUSTOMERCONTACTNOTIFICATIONSGRID;
    public static readonly ICABSSEPREMISEPDAICABSACTIVITYGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSSEPREMISEPDAICABSACTIVITYGRID;
    public static readonly ICABSAINVOICEBYACCOUNTGRID = AppModuleRoutes.GRID_SPLITAPPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSAINVOICEBYACCOUNTGRID;
    public static readonly ICABSSEDESPATCHGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSSEDESPATCHGRID;
    public static readonly ICABSASERVICECOVERUPDATEABLEGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICECOVERUPDATEABLEGRID;
    public static readonly ICABSSNOTIFICATIONGROUPGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSSNOTIFICATIONGROUPGRID;
    public static readonly ICABSASERVICEVISITDISPLAYGRID = AppModuleRoutes.GRID_APPLICATION + InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICEVISITDISPLAYGRID;
}

export class InternalGridSearchServiceModuleRoutesConstant {
    public static readonly ICABSSESERVICEPLANNINGLISTENTRY = 'internal/maintenance/serviceplanninglistentry';
    public static readonly ICABSSEHCANEWLOCATIONGRID = 'hca/newlocation';
    public static readonly ICABSSSOPROSPECTGRID = 'prospectgrid';
    public static readonly ICABSACLOSEDTEMPLATEBRANCHACCESSGRID = 'ClosedTemplateBranchAccessGrid';
    public static readonly ICABSSECUSTOMERSIGNATUREDETAIL = 'customerSignatureDetail';
    public static readonly ICABSANNIVERSARYGENERATE = 'AnniversaryGenerate';
    public static readonly ICABSADIARYYEARGRID = 'DairyYearGridGridComponent';
    public static readonly ICABSASERVICECOVERDISPLAYENTRY = 'servicedisplayentry';
    public static readonly ICABSASERVICECOVERDETAILLOCATIONENTRYGRID = 'ServiceCoverDetailLocationEntryGridComponent';
    public static readonly ICABSAPLANVISITTABULAR = 'premise/planVisit';
    public static readonly ICABSBBRANCHHOLIDAYGRID = 'business/branchholiday';

    public static readonly ICABSACALENDARTEMPLATEDATEGRID = 'business/calendartemplategrid';
    public static readonly ICABSASEASONALTEMPLATEDETAILGRID = 'seasonaltemplatedetailgrid';
    public static readonly ICABSSINFESTATIONTOLERANCEGRID = 'contractmanagement/account/infestationToleranceGrid';
    public static readonly ICABSCMCUSTOMERCONTACTHISTORYGRID = 'contactmanagement/customercontactHistorygrid';
    public static readonly ICABSSESERVICEVISITENTRYGRID = 'visitentry';
    public static readonly ICABSACONTRACTSERVICESUMMARY = 'contractmanagement/account/contractservicesummary';
    public static readonly ICABSSEDEBRIEFSUMMARYGRID = 'debrief/summary';
    public static readonly ICABSASERVICEVALUEGRID = 'contractmanagement/account/serviceValue';
    public static readonly ICABSSEDEBRIEFOUTSTANDINGGRID = 'debrief/outstandinggrid';
    public static readonly ICABSSEFOLLOWUPGRID = 'followupcalls';
    public static readonly ICABSSEPLANVISITGRIDDAYBRANCH = 'planvisit/daybranch';
    public static readonly ICABSSESTATICVISITGRIDDAYBRANCH = 'staticvisit/daybranch';
    public static readonly ICABSSEHCASPECIALINSTRUCTIONSGRID = 'hca/specialinstructions';
    public static readonly ICABSAACCOUNTADDRESSCHANGEHISTORYGRID = 'contractmanagement/account/addressChangeHistory';
    public static readonly ICABSASERVICEVISITSUMMARY = 'contractmanagement/maintenance/contract/visitsummary';
    public static readonly ICABSCMWORKORDERGRID = 'contactmanagement/scmworkordergrid';

    public static readonly ICABSSCONTACTTYPEDETAILASSIGNEEGRID = 'contactManagement/contactTypeDetail/ContactTypeDetailAssigneeGrid';
    public static readonly ICABSCMGENERALSEARCHINFOGRID = 'contactmanagement/generalsearchInfo';
    public static readonly ICABSACONTRACTPRODUCTSUMMARY = 'contract/product/summary';

    public static readonly ICABSSVISITTOLERANCEGRID = 'visittolerancegrid';
    public static readonly ICABSARECOMMENDATIONGRID = {
        URL_1: 'recommendation',
        URL_2: 'contractmanagement/account/recommendationGrid'
    };
    public static readonly ICABSAPRODUCTSALESSCENTRYGRID = {
        URL_1: 'productSalesSCEntryGrid',
        URL_2: 'contractmanagement/maintenance/productSalesSCEntryGrid'
    };
    public static readonly ICABSACUSTOMERINFORMATIONSUMMARY = {
        URL_1: 'contract/customerinformation',
        URL_2: 'contractmanagement/maintenance/contract/customerinformation'
    };
    public static readonly ICABSAPREMISESEARCHGRID = {
        URL_1: 'premise/search',
        URL_2: 'contractmanagement/maintenance/contract/premise'
    };
    public static readonly ICABSSESERVICEPLANNINGGRID = 'planning/grid';
    public static readonly ICABSSESERVICEPLANNEDDATESGRID = 'planned/dates/grid';
    public static readonly ICABSSESERVICEWORKLISTGRID = 'worklist/grid';
    public static readonly ICABSSESERVICEACTIVITYUPDATEGRID = 'visit/activity';
    public static readonly ICABSSESERVICEWORKLISTDATEGRID = 'service/worklist/dategrid';
    public static readonly ICABSSESUMMARYWORKLOADREZONEMOVEGRID = {
        URL_1: 'service/worklist/rezone/movegrid',
        URL_2: 'service/worklist/rezone/movegrid/reroute',
        URL_3: 'service/worklist/rezone/destmovegrid'
    };
    public static readonly ICABSSEDEBRIEFACTIONSGRID = 'debrief/action';
    public static readonly ICABSSESERVICEAREADETAILGRID = 'serviceplanning/serviceareadetail';
    public static readonly ICABSSESERVICEVISITENTITLEMENTENTRYGRID = 'visitentitlemententry';
    public static readonly ICABSASERVICEVISITPLANNINGGRID = 'visit/planning/grid';
    public static readonly ICABSSECUSTOMERSIGNATURESUMMARY = 'customerSignatureSummary/business';
    public static readonly ICABSSESERVICEAREAREZONEREJECTIONSGRID = 'area/rezone/rejectiongrid';
    public static readonly ICABSSECUSTOMERSIGNATURESUMMARYBRANCH = 'customerSignatureSummary/branch';
    public static readonly ICABSSECUSTOMERSIGNATURESUMMARYREGION = 'customerSignatureSummary/region';
    public static readonly ICABSARCUSTOMERQUARTERLYRETURNSPRINT = 'customerquarterlyreturns';
    public static readonly ICABSSESERVICEVISITRELEASEDETAILGRID = 'visit/releasedetailgrid';
    public static readonly ICABSSESERVICEPLANNINGBRANCHSUMMARY = 'planning/branchsummary';
    public static readonly ICABSSESERVICEPLANNINGSTOCKREPORT = 'planning/stockreport';
    public static readonly ICABSABRANCHSESERVICEPLANSUMMARYRESERVESTOCK = 'planning/grid/reservestock';
    public static readonly ICABSSTEAMUSERGRID = 'team/user/grid';
    public static readonly ICABSSESERVICEVISITANNIVDATEMAINTENANCE = 'visit/annidate/maintenance';
    public static readonly ICABSSEVISITDATEDISCREPANCYGRID = 'serviceplanning/visitdatemismatch';
}

export class InternalGridSearchServiceModuleRoutes {
    public static readonly ICABSSESERVICEPLANNINGLISTENTRY = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEPLANNINGLISTENTRY;
    public static readonly ICABSSEHCANEWLOCATIONGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSEHCANEWLOCATIONGRID;
    public static readonly ICABSSSOPROSPECTGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSSOPROSPECTGRID;
    public static readonly ICABSACLOSEDTEMPLATEBRANCHACCESSGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSACLOSEDTEMPLATEBRANCHACCESSGRID;
    public static readonly ICABSSECUSTOMERSIGNATUREDETAIL = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSECUSTOMERSIGNATUREDETAIL;
    public static readonly ICABSANNIVERSARYGENERATE = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSANNIVERSARYGENERATE;
    public static readonly ICABSADIARYYEARGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSADIARYYEARGRID;
    public static readonly ICABSASERVICECOVERDISPLAYENTRY = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSASERVICECOVERDISPLAYENTRY;
    public static readonly ICABSSESERVICEVISITENTRYGRID = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEVISITENTRYGRID;
    public static readonly ICABSASERVICECOVERDETAILLOCATIONENTRYGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSASERVICECOVERDETAILLOCATIONENTRYGRID;
    public static readonly ICABSAPLANVISITTABULAR = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSAPLANVISITTABULAR;
    public static readonly ICABSSEFOLLOWUPGRID = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSEFOLLOWUPGRID;
    public static readonly ICABSSEDEBRIEFOUTSTANDINGGRID = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSEDEBRIEFOUTSTANDINGGRID;
    public static readonly ICABSBBRANCHHOLIDAYGRID = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSBBRANCHHOLIDAYGRID;
    public static readonly ICABSACALENDARTEMPLATEDATEGRID = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSACALENDARTEMPLATEDATEGRID;
    public static readonly ICABSASEASONALTEMPLATEDETAILGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSASEASONALTEMPLATEDETAILGRID;
    public static readonly ICABSSINFESTATIONTOLERANCEGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSINFESTATIONTOLERANCEGRID;
    public static readonly ICABSSEHCASPECIALINSTRUCTIONSGRID = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSEHCASPECIALINSTRUCTIONSGRID;
    public static readonly ICABSCMCUSTOMERCONTACTHISTORYGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSCMCUSTOMERCONTACTHISTORYGRID;
    public static readonly ICABSACONTRACTSERVICESUMMARY = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSACONTRACTSERVICESUMMARY;
    public static readonly ICABSSEDEBRIEFSUMMARYGRID = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSEDEBRIEFSUMMARYGRID;
    public static readonly ICABSASERVICEVALUEGRID = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSASERVICEVALUEGRID;
    public static readonly ICABSSEPLANVISITGRIDDAYBRANCH = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSEPLANVISITGRIDDAYBRANCH;
    public static readonly ICABSSESTATICVISITGRIDDAYBRANCH = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESTATICVISITGRIDDAYBRANCH;
    public static readonly ICABSAACCOUNTADDRESSCHANGEHISTORYGRID = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSAACCOUNTADDRESSCHANGEHISTORYGRID;
    public static readonly ICABSASERVICEVISITSUMMARY = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSASERVICEVISITSUMMARY;
    public static readonly ICABSCMWORKORDERGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSCMWORKORDERGRID;

    public static readonly ICABSSVISITTOLERANCEGRID = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSVISITTOLERANCEGRID;
    public static readonly ICABSARECOMMENDATIONGRID = {
        URL_1: AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSARECOMMENDATIONGRID.URL_1,
        URL_2: AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSARECOMMENDATIONGRID.URL_2
    };
    public static readonly ICABSAPRODUCTSALESSCENTRYGRID = {
        URL_1: AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSAPRODUCTSALESSCENTRYGRID.URL_1,
        URL_2: AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSAPRODUCTSALESSCENTRYGRID.URL_2
    };
    public static readonly ICABSACUSTOMERINFORMATIONSUMMARY = {
        URL_1: AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSACUSTOMERINFORMATIONSUMMARY.URL_1,
        URL_2: AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSACUSTOMERINFORMATIONSUMMARY.URL_2
    };
    public static readonly ICABSAPREMISESEARCHGRID = {
        URL_1: AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSAPREMISESEARCHGRID.URL_1,
        URL_2: AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSAPREMISESEARCHGRID.URL_2
    };
    public static readonly ICABSCMGENERALSEARCHINFOGRID = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSCMGENERALSEARCHINFOGRID;
    public static readonly ICABSACONTRACTPRODUCTSUMMARY = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSACONTRACTPRODUCTSUMMARY;
    public static readonly ICABSSESERVICEPLANNINGGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEPLANNINGGRID;
    public static readonly ICABSSESERVICEPLANNEDDATESGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEPLANNEDDATESGRID;
    public static readonly ICABSSESERVICEWORKLISTGRID = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEWORKLISTGRID;
    public static readonly ICABSSESERVICEACTIVITYUPDATEGRID = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEACTIVITYUPDATEGRID;
    public static readonly ICABSSESERVICEWORKLISTDATEGRID = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEWORKLISTDATEGRID;
    public static readonly ICABSSESUMMARYWORKLOADREZONEMOVEGRID = {
        URL_1: AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESUMMARYWORKLOADREZONEMOVEGRID.URL_1,
        URL_2: AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESUMMARYWORKLOADREZONEMOVEGRID.URL_2,
        URL_3: AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESUMMARYWORKLOADREZONEMOVEGRID.URL_3
    };
    public static readonly ICABSSESERVICEPLANNINGBRANCHSUMMARY = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEPLANNINGBRANCHSUMMARY;
    public static readonly ICABSSESERVICEPLANNINGSTOCKREPORT = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEPLANNINGSTOCKREPORT;
    public static readonly ICABSABRANCHSESERVICEPLANSUMMARYRESERVESTOCK = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSABRANCHSESERVICEPLANSUMMARYRESERVESTOCK;
    public static readonly ICABSSEDEBRIEFACTIONSGRID = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSEDEBRIEFACTIONSGRID;
    public static readonly ICABSSESERVICEAREADETAILGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEAREADETAILGRID;
    public static readonly ICABSSESERVICEVISITENTITLEMENTENTRYGRID = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEVISITENTITLEMENTENTRYGRID;
    public static readonly ICABSASERVICEVISITPLANNINGGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSASERVICEVISITPLANNINGGRID;
    public static readonly ICABSSECUSTOMERSIGNATURESUMMARY = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSECUSTOMERSIGNATURESUMMARY;
    public static readonly ICABSSECUSTOMERSIGNATURESUMMARYBRANCH = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSECUSTOMERSIGNATURESUMMARYBRANCH;
    public static readonly ICABSSESERVICEAREAREZONEREJECTIONSGRID = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEAREAREZONEREJECTIONSGRID;
    public static readonly ICABSSECUSTOMERSIGNATURESUMMARYREGION = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSECUSTOMERSIGNATURESUMMARYREGION;
    public static readonly ICABSARCUSTOMERQUARTERLYRETURNSPRINT = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSARCUSTOMERQUARTERLYRETURNSPRINT;
    public static readonly ICABSSESERVICEVISITRELEASEDETAILGRID = AppModuleRoutes.GRID_MENUSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEVISITRELEASEDETAILGRID;
    public static readonly ICABSSTEAMUSERGRID = AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSTEAMUSERGRID;
    public static readonly ICABSSESERVICEVISITANNIVDATEMAINTENANCE = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEVISITANNIVDATEMAINTENANCE;
    public static readonly ICABSSEVISITDATEDISCREPANCYGRID = AppModuleRoutes.GRID_SPLITSERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSEVISITDATEDISCREPANCYGRID;
}

export class InternalMaintenanceModuleRoutesConstant {
    public static readonly ICABSBWASTECONSIGNMENTNOTERANGEMAINTENANCE = 'wasteconsingnmentnoterange';
    public static readonly ICABSCMPROSPECTMAINTENANCE = 'cm/prospect';
    public static readonly ICABSBBRANCHPCODESERVICEGRPENTRYMAINTENANCE = 'branch/pcode/service/grpentry';
    public static readonly ICABSBRETURNEDPAPERWORKMAINTENANCE = 'returned/paperwork';
    public static readonly ICABSBBRANCHSERVICEAREAGROUPDETAILMAINTENANCE = 'branch/serviceareagroup';
    public static readonly ICABSBPREPARATIONMIXMAINTENANCE = 'preparationmix';
    public static readonly ICABSSNOTIFICATIONTEMPLATEDETAILMAINTENANCE = 'templatedetailmaintenance';
    public static readonly ICABSBDETECTORMAINTENANCE = 'detector';
    public static readonly ICABSSNOTIFICATIONGROUPEMPLOYEEMAINTENANCE = 'groupEmployeeMaintenance';
    public static readonly ICABSBINFESTATIONLEVELMAINTENANCE = 'infestation/level';
    public static readonly ICABSSEMANUALWASTECONSIGNMENT = {
        URL1: 'wasteconsignment/manual',
        URL2: 'wasteconsignment/void'
    };
    public static readonly ICABSSEUNRETURNEDCONSIGNMENTNOTESGRID = 'unreturnedconsignment/notes';
    public static readonly ICABSSEUNRETURNEDCONSIGNMENTNOTESDETAILGRID = 'maintenance/unreturnedconsignment/notes/detail';
    public static readonly ICABSBBRANCHPRODSERVICEGRPENTRYMAINTENANCE = 'maintenance/branchprodservice/grpentry';
    public static readonly ICABSASERVICEVISITPLANNINGMAINTENANCE = 'service/visitPlanning';
    public static readonly ICABSAENTITLEMENTMAINTENENCE = 'entitlement';
}

export class InternalMaintenanceModuleRoutes {// Parent Path - /maintenance
    public static readonly ICABSBPRODUCTSERVICEGROUPMAINTENANCE = 'maintenance/productserviceGroupmaintenance';
    public static readonly ICABSBSYSTEMBUSINESSVISITTYPEMAINTENANCE = 'systembusiness/visit/type';
    public static readonly ICABSBWASTECONSIGNMENTNOTERANGEMAINTENANCE = AppModuleRoutes.MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSBWASTECONSIGNMENTNOTERANGEMAINTENANCE;
    public static readonly ICABSCMPROSPECTMAINTENANCE = AppModuleRoutes.MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSCMPROSPECTMAINTENANCE;
    public static readonly ICABSBBRANCHPCODESERVICEGRPENTRYMAINTENANCE = AppModuleRoutes.INTERNAL_MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSBBRANCHPCODESERVICEGRPENTRYMAINTENANCE;
    public static readonly ICABSBRETURNEDPAPERWORKMAINTENANCE = AppModuleRoutes.INTERNAL_MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSBRETURNEDPAPERWORKMAINTENANCE;
    public static readonly ICABSBBRANCHSERVICEAREAGROUPDETAILMAINTENANCE = AppModuleRoutes.INTERNAL_MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSBBRANCHSERVICEAREAGROUPDETAILMAINTENANCE;
    public static readonly ICABSBPREPARATIONMIXMAINTENANCE = AppModuleRoutes.INTERNAL_MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSBPREPARATIONMIXMAINTENANCE;
    public static readonly ICABSSNOTIFICATIONTEMPLATEDETAILMAINTENANCE = AppModuleRoutes.MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSSNOTIFICATIONTEMPLATEDETAILMAINTENANCE;
    public static readonly ICABSBDETECTORMAINTENANCE = AppModuleRoutes.MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSBDETECTORMAINTENANCE;
    public static readonly ICABSSNOTIFICATIONGROUPEMPLOYEEMAINTENANCE = AppModuleRoutes.MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSSNOTIFICATIONGROUPEMPLOYEEMAINTENANCE;
    public static readonly ICABSBINFESTATIONLEVELMAINTENANCE = AppModuleRoutes.MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSBINFESTATIONLEVELMAINTENANCE;
    public static readonly ICABSSEUNRETURNEDCONSIGNMENTNOTESGRID = AppModuleRoutes.MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSSEUNRETURNEDCONSIGNMENTNOTESGRID;
    public static readonly ICABSSEUNRETURNEDCONSIGNMENTNOTESDETAILGRID = AppModuleRoutes.MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSSEUNRETURNEDCONSIGNMENTNOTESDETAILGRID;
    public static readonly ICABSBBRANCHPRODSERVICEGRPENTRYMAINTENANCE = AppModuleRoutes.MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSBBRANCHPRODSERVICEGRPENTRYMAINTENANCE;
    public static readonly ICABSASERVICEVISITPLANNINGMAINTENANCE = AppModuleRoutes.MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSASERVICEVISITPLANNINGMAINTENANCE;
    public static readonly ICABSAENTITLEMENTMAINTENENCE = AppModuleRoutes.MAINTENANCE + InternalMaintenanceModuleRoutesConstant.ICABSAENTITLEMENTMAINTENENCE;
}

export class InternalMaintenanceApplicationModuleRoutesConstant {
    public static readonly ICABSAINVOICEHEADERADDRESSDETAILS = 'invoice/addressdetails';
    public static readonly ICABSACONTRACTRENEWALMAINTENANCE = 'contract/renewal';
    public static readonly ICABSASERVICECOVERCOMPONENTENTRY = 'ServiceCoverComponentEntry';
    public static readonly RIMBATCHPROCESSMONITORMAINTENANCE = 'BatchProcessMonitorMaintenance';
    public static readonly ICABSASERVICECOVERDETAILGROUPMAINTENANCE = 'serviceCoverDetailGroupMaintenance';
    public static readonly ICABSAINVOICEREPRINTMAINTENANCE = 'invoiceReprintMaintenance';
    public static readonly ICABSAINVOICEGROUPPAYMENTMAINTENANCE = 'grouppaymentmaintenance';
    public static readonly ICABSAACCOUNTBANKDETAILSMAINTENANCE = 'AccountBankDetailsMaintenance';
    public static readonly ICABSAINVOICEPRINTMAINTENANCE = 'invoice/print/maintenance';
    public static readonly ICABSASERVICECOVERSEASONMAINTENANCE = 'servicecover/season';
    public static readonly ICABSASERVICECOVERDETAILMAINTENANCE = 'ServiceCoverDetailMaintenance';
    public static readonly ICABSACALENDARTEMPLATEMAINTENANCE = 'calendarTemplateMaintenance';
    public static readonly ICABSASEASONALTEMPLATEMAINTENANCE = 'seasonal/templatemaintenance';
    public static readonly ICABSAACCOUNTOWNERMAINTENANCE = 'accountowner';
    public static readonly ICABSAPLANVISITMAINTENANCE = 'planvisit';
    public static readonly ICABSACONTRACTHISTORYDETAIL = 'contractHistoryDetail';
    public static readonly ICABSALOSTBUSINESSREQUESTMAINTENANCE = 'lostbusinessrequestmaintenance';
    public static readonly ICABSAPREMISELOCATIONMAINTENANCE = 'premiseLocationMaintenance';
    public static readonly ICABSACUSTOMERINFORMATIONACCOUNTMAINTENANCE = 'CustomerInformationAccountMaintenance';
    public static readonly ICABSATRIALPERIODRELEASEMAINTENANCE = 'trialperiodrelease';
    public static readonly ICABSACUSTOMERINFORMATIONMAINTENANCE = 'customerinformation';
    public static readonly ICABSASERVICECOVERPRICECHANGEMAINTENANCE = 'serviceCoverPriceChange';
    public static readonly ICABSASERVICECOVERDISPLAYMASSVALUES = 'serviceCoverDisplayMassValues';
    public static readonly ICABSASERVICECOVERLOCATIONMAINTENANCE = 'servicecoverlocationmaintenance';
    public static readonly ICABSASERVICECOVERCOMMENCEDATEMAINTENANCE = 'serviceCoverCommencedate';
    public static readonly ICABSASERVICECOVERCOMMENCEDATEMAINTENANCEEX = 'servicecover/commencedate';
    public static readonly ICABSASERVICECOVERLOCATIONMOVEMAINTENANCE = 'ServiceCoverLocationMoveMaintenance';
    public static readonly ICABSBPRODUCTMAINTENANCE = 'product/maintenance';
    public static readonly ICABSCMCALLCENTRECREATEFIXEDPRICEJOB = 'callcentre/createfixedpricejob';
    public static readonly ICABSALOSTBUSINESSCONTACTMAINTENANCE = 'lostbusinesscontactmaintenance';
    public static readonly ICABSASERVICECOVERSEASONALDATESMAINTENANCE = 'servicecover/seasonaldatesmaintenance';
    public static readonly ICABSAVISITCANCELLATIONMAINTENANCE = 'visitcancellation';
    public static readonly ICABSSCONTACTTYPEDETAILMAINTENANCE = 'maintenance/contacttype/detail';
    public static readonly ICABSBLOSTBUSINESSDETAILMAINTENANCE = 'business/lost/detail/maintenance';
    public static readonly ICABSBPRODUCTDETAILMAINTENANCE = 'maintenance/product/detail';
    public static readonly ICABSASERVICECOVERSUSPENDMAINTENANCE = {
        CONTRACT: 'servicecover/suspend/contract',
        JOB: 'servicecover/suspend/job'
    };
    public static readonly ICABSAPREMISESUSPENDMAINTENANCE = {
        CONTRACT: 'premise/suspend/contract',
        JOB: 'premise/suspend/job'
    };
    public static readonly ICABSBVALIDLINKEDPRODUCTSMAINTENANCE = 'maintenance/validlinkedproduct';
    public static readonly ICABSAVISITAPPOINTMENTMAINTENANCE = 'visit/appointment';
    public static readonly ICABSBPRODUCTSERVICEGROUPMAINTENANCE = 'maintenance/productservice/group';
    public static readonly ICABSSCUSTOMERTYPEMAINTENANCE = 'maintenance/customertype';
    public static readonly ICABSASERVICECOVERSERVICESUSPENDMAINTENANCE = 'maintenance/servicecover/service/suspend';
    public static readonly ICABSSCMCUSTOMERCONTACTMAINTENANCE = {
        URL_1: 'contactmanagement/customerContactMaintenance',
        URL_2: 'contactmanagement/customerContactMaintenance/related'
    };
    public static readonly ICABSSESERVICEPLANNINGMAINTENANCE = 'maintenance/servicePlanningMaintenance';
    public static readonly ICABSBPREPARATIONMAINTENANCE = 'application/preparationmaintenance';
}

export class InternalMaintenanceApplicationModuleRoutes {
    public static readonly ICABSAINVOICEHEADERADDRESSDETAILS = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAINVOICEHEADERADDRESSDETAILS;
    public static readonly ICABSACONTRACTRENEWALMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSACONTRACTRENEWALMAINTENANCE;
    public static readonly ICABSASERVICECOVERCOMPONENTENTRY = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERCOMPONENTENTRY;
    public static readonly RIMBATCHPROCESSMONITORMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.RIMBATCHPROCESSMONITORMAINTENANCE;
    public static readonly ICABSASERVICECOVERDETAILGROUPMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERDETAILGROUPMAINTENANCE;
    public static readonly ICABSAINVOICEREPRINTMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAINVOICEREPRINTMAINTENANCE;
    public static readonly ICABSAINVOICEGROUPPAYMENTMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAINVOICEGROUPPAYMENTMAINTENANCE;
    public static readonly ICABSAACCOUNTBANKDETAILSMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAACCOUNTBANKDETAILSMAINTENANCE;
    public static readonly ICABSAINVOICEPRINTMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAINVOICEPRINTMAINTENANCE;
    public static readonly ICABSASERVICECOVERSEASONMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERSEASONMAINTENANCE;
    public static readonly ICABSASERVICECOVERDETAILMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERDETAILMAINTENANCE;
    public static readonly ICABSACALENDARTEMPLATEMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSACALENDARTEMPLATEMAINTENANCE;
    public static readonly ICABSALOSTBUSINESSREQUESTMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSALOSTBUSINESSREQUESTMAINTENANCE;
    public static readonly ICABSASEASONALTEMPLATEMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASEASONALTEMPLATEMAINTENANCE;
    public static readonly ICABSAACCOUNTOWNERMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAACCOUNTOWNERMAINTENANCE;
    public static readonly ICABSAPLANVISITMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAPLANVISITMAINTENANCE;
    public static readonly ICABSACONTRACTHISTORYDETAIL = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSACONTRACTHISTORYDETAIL;
    public static readonly ICABSAPREMISELOCATIONMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAPREMISELOCATIONMAINTENANCE;
    public static readonly ICABSACUSTOMERINFORMATIONACCOUNTMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSACUSTOMERINFORMATIONACCOUNTMAINTENANCE;
    public static readonly ICABSATRIALPERIODRELEASEMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSATRIALPERIODRELEASEMAINTENANCE;
    public static readonly ICABSACUSTOMERINFORMATIONMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSACUSTOMERINFORMATIONMAINTENANCE;
    public static readonly ICABSASERVICECOVERPRICECHANGEMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERPRICECHANGEMAINTENANCE;
    public static readonly ICABSASERVICECOVERDISPLAYMASSVALUES = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERDISPLAYMASSVALUES;
    public static readonly ICABSASERVICECOVERLOCATIONMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERLOCATIONMAINTENANCE;
    public static readonly ICABSASERVICECOVERCOMMENCEDATEMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERCOMMENCEDATEMAINTENANCE;
    public static readonly ICABSAVISITCANCELLATIONMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAVISITCANCELLATIONMAINTENANCE;
    public static readonly ICABSASERVICECOVERCOMMENCEDATEMAINTENANCEEX = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERCOMMENCEDATEMAINTENANCEEX;
    public static readonly ICABSASERVICECOVERLOCATIONMOVEMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERLOCATIONMOVEMAINTENANCE;
    public static readonly ICABSBPRODUCTMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSBPRODUCTMAINTENANCE;
    public static readonly ICABSSCONTACTTYPEDETAILMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSSCONTACTTYPEDETAILMAINTENANCE;
    public static readonly ICABSALOSTBUSINESSCONTACTMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSALOSTBUSINESSCONTACTMAINTENANCE;
    public static readonly ICABSASERVICECOVERSEASONALDATESMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERSEASONALDATESMAINTENANCE;
    public static readonly ICABSBLOSTBUSINESSDETAILMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSBLOSTBUSINESSDETAILMAINTENANCE;
    public static readonly ICABSBPRODUCTDETAILMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSBPRODUCTDETAILMAINTENANCE;
    public static readonly ICABSASERVICECOVERSUSPENDMAINTENANCE = {
        CONTRACT: AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERSUSPENDMAINTENANCE.CONTRACT,
        JOB: AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSASERVICECOVERSUSPENDMAINTENANCE.JOB
    };
    public static readonly ICABSAPREMISESUSPENDMAINTENANCE = {
        CONTRACT: AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAPREMISESUSPENDMAINTENANCE.CONTRACT,
        JOB: AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAPREMISESUSPENDMAINTENANCE.JOB
    };
    public static readonly ICABSBVALIDLINKEDPRODUCTSMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSBVALIDLINKEDPRODUCTSMAINTENANCE;
    public static readonly ICABSAVISITAPPOINTMENTMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSAVISITAPPOINTMENTMAINTENANCE;
    public static readonly ICABSBPRODUCTSERVICEGROUPMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSBPRODUCTSERVICEGROUPMAINTENANCE;
    public static readonly ICABSSCUSTOMERTYPEMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSSCUSTOMERTYPEMAINTENANCE;
    public static readonly ICABSCMCALLCENTRECREATEFIXEDPRICEJOB = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSCMCALLCENTRECREATEFIXEDPRICEJOB;
    public static readonly ICABSSCMCUSTOMERCONTACTMAINTENANCE = {
        URL_1: AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_1,
        URL_2: AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_2
    };
    public static readonly ICABSSESERVICEPLANNINGMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSSESERVICEPLANNINGMAINTENANCE;
    public static readonly ICABSBPREPARATIONMAINTENANCE = AppModuleRoutes.MAINTENANCE_APPLICATION + InternalMaintenanceApplicationModuleRoutesConstant.ICABSBPREPARATIONMAINTENANCE;
}

export class InternalMaintenanceSalesModuleRoutesConstant {
    public static readonly ICABSAPRORATACHARGEMAINTENANCE = 'invoice/ProRataChargeMaintenance';
    public static readonly ICABSAINVOICENARRATIVEMAINTENANCE = 'contract/invoicenarrative';
    public static readonly ICABSACUSTOMERINFOMAINTENANCE = 'customerInfoMaintenance';
    public static readonly ICABSACONTRACTCOMMENCEDATEMAINTENANCE = 'commencedate';
    public static readonly ICABSACONTRACTCOMMENCEDATEMAINTENANCEEX = 'commencedateex';
    public static readonly ICABSRENEWALGENERATE = 'renewalLetterGenerate';
    public static readonly ICABSASERVICECOVERCALENDARDATESMAINTENANCE = 'ServiceCoverCalendarDatesMaintenance';
    public static readonly ICABSAINVOICECHARGEMAINTENANCE = 'invoiceChargeMaintenance';
    public static readonly ICABSAINVOICEPRINTLINEMAINTENANCE = 'InvoicePrintLine';
    public static readonly ICABSAINVOICEGROUPPREMISEMAINTENANCE = 'invoicepremisegroup/search';
    public static readonly ICABSBINVOICERUNDATEMAINTENANCE = 'invoiceRunDateMaintenance';
    public static readonly ICABSSPROSPECTSTATUSCHANGE = 'prospectstatuschange';
    public static readonly ICABSBINVOICERANGEMAINTENANCE = 'rangemaintenance';
    public static readonly ICABSBINVOICERUNDATECONFIRM = 'rundateconfirm';
    public static readonly ICABSSDLSERVICECOVERMAINTENANCE = 'dlservicecover';
    public static readonly ICABSSSOQUOTESTATUSMAINTENANCE = 'quotestatusmaintenance';
    public static readonly ICABSSDLPREMISEMAINTENANCE = 'sdlpremisemaintenance';
    public static readonly ICABSSDLCONTRACTMAINTENANCE = 'sSdlContractMaintenance';
    public static readonly ICABSSVISITTOLERANCEMAINTENANCE = 'visitTolerance';
    public static readonly ICABSSSOQUOTESUBMITMAINTENANCE = 'quoteSubmitMaintenance';
    public static readonly ICABSBEXPENSECODEMAINTENANCE = 'expensecode';
    public static readonly RIMBATCHPROGRAMSCHEDULEMAINTENANCE = 'batchProgramSchedule';
    public static readonly ICABSASERVICECOVERCOMPONENTREPLACEMENT = 'maintenance/servicecoverreplacement';
    public static readonly ICABSCMTELESALESENTRYCONFIRMORDER = 'ccm/telesalesentryconfirmorder';
    public static readonly ICABSSCONTACTTYPEDETAILLANGMAINTENANCE = 'contact/type/detial/langmaintenance';
    public static readonly ICABSBLOSTBUSINESSMAINTENANCE = 'lostbusinessmaintenance';
    public static readonly ICABSSCONTACTTYPEDETAILPCEXCMAINT = 'system/contacttypedetailpcexcmaint';
    public static readonly ICABSSCONTACTTYPEDETAILPROPERTIESMAINT = 'maintenance/contacttypedetailpropertiesmaintenance';
    public static readonly ICABSBSALESAREAMAINTENANCE = 'maintenance/salesarea';
    public static readonly ICABSACONTRACTSUSPENDMAINTENANCE = {
        CONTRACT: 'contract/suspend',
        JOB: 'job/suspend'
    };
    public static readonly ICABSCMPROSPECTCONVERSIONMAINTENANCE = 'prospect/conversion';
    public static readonly ICABMASSSERVICESUSPENDMAINTENANCE = 'contract/suspendbymass';
    public static readonly ICABSAMASSSERVICECOVERSUSPENDMAINTENANCE = 'contract/suspendbymass/servicecover';
}

export class InternalMaintenanceSalesModuleRoutes {
    public static readonly ICABSAPRORATACHARGEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSAPRORATACHARGEMAINTENANCE;
    public static readonly ICABSAINVOICENARRATIVEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSAINVOICENARRATIVEMAINTENANCE;
    public static readonly ICABSACUSTOMERINFOMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSACUSTOMERINFOMAINTENANCE;
    public static readonly ICABSACONTRACTCOMMENCEDATEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSACONTRACTCOMMENCEDATEMAINTENANCE;
    public static readonly ICABSACONTRACTCOMMENCEDATEMAINTENANCEEX = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSACONTRACTCOMMENCEDATEMAINTENANCEEX;
    public static readonly ICABSRENEWALGENERATE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSRENEWALGENERATE;
    public static readonly ICABSASERVICECOVERCALENDARDATESMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSASERVICECOVERCALENDARDATESMAINTENANCE;
    public static readonly ICABSAINVOICECHARGEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSAINVOICECHARGEMAINTENANCE;
    public static readonly ICABSAINVOICEPRINTLINEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSAINVOICEPRINTLINEMAINTENANCE;
    public static readonly ICABSAINVOICEGROUPPREMISEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSAINVOICEGROUPPREMISEMAINTENANCE;
    public static readonly ICABSBINVOICERUNDATEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSBINVOICERUNDATEMAINTENANCE;
    public static readonly ICABSSPROSPECTSTATUSCHANGE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSSPROSPECTSTATUSCHANGE;
    public static readonly ICABSBINVOICERANGEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSBINVOICERANGEMAINTENANCE;
    public static readonly ICABSBINVOICERUNDATECONFIRM = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSBINVOICERUNDATECONFIRM;
    public static readonly ICABSSDLSERVICECOVERMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSSDLSERVICECOVERMAINTENANCE;
    public static readonly ICABSSSOQUOTESTATUSMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSSSOQUOTESTATUSMAINTENANCE;
    public static readonly ICABSSDLPREMISEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSSDLPREMISEMAINTENANCE;
    public static readonly ICABSSDLCONTRACTMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSSDLCONTRACTMAINTENANCE;
    public static readonly ICABSSVISITTOLERANCEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSSVISITTOLERANCEMAINTENANCE;
    public static readonly ICABSSSOQUOTESUBMITMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSSSOQUOTESUBMITMAINTENANCE;
    public static readonly ICABSBEXPENSECODEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSBEXPENSECODEMAINTENANCE;
    public static readonly RIMBATCHPROGRAMSCHEDULEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.RIMBATCHPROGRAMSCHEDULEMAINTENANCE;
    public static readonly ICABSASERVICECOVERCOMPONENTREPLACEMENT = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSASERVICECOVERCOMPONENTREPLACEMENT;
    public static readonly ICABSCMTELESALESENTRYCONFIRMORDER = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSCMTELESALESENTRYCONFIRMORDER;
    public static readonly ICABSSCONTACTTYPEDETAILLANGMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSSCONTACTTYPEDETAILLANGMAINTENANCE;
    public static readonly ICABSBLOSTBUSINESSMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSBLOSTBUSINESSMAINTENANCE;
    public static readonly ICABSSCONTACTTYPEDETAILPCEXCMAINT = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSSCONTACTTYPEDETAILPCEXCMAINT;
    public static readonly ICABSSCONTACTTYPEDETAILPROPERTIESMAINT = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSSCONTACTTYPEDETAILPROPERTIESMAINT;
    public static readonly ICABSBSALESAREAMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSBSALESAREAMAINTENANCE;
    public static readonly ICABSACONTRACTSUSPENDMAINTENANCE = {
        CONTRACT: AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSACONTRACTSUSPENDMAINTENANCE.CONTRACT,
        JOB: AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSACONTRACTSUSPENDMAINTENANCE.JOB
    };
    public static readonly ICABSCMPROSPECTCONVERSIONMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSCMPROSPECTCONVERSIONMAINTENANCE;
    public static readonly ICABMASSSERVICESUSPENDMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABMASSSERVICESUSPENDMAINTENANCE;
    public static readonly ICABSAMASSSERVICECOVERSUSPENDMAINTENANCE = AppModuleRoutes.MAINTENANCE_SALES + InternalMaintenanceSalesModuleRoutesConstant.ICABSAMASSSERVICECOVERSUSPENDMAINTENANCE;
}

export class InternalMaintenanceServiceModuleRoutesConstant {
    public static readonly ICABSBBRANCHHOLIDAYMAINTENANCE = 'business/branchholidaymaintenance';
    public static readonly ICABSTEAMMAINTENANCE = 'teamMaintenance';
    public static readonly ICABSALINKEDPRODUCTSMAINTENANCE = 'linkedproduct';
    public static readonly ICABSSESERVICEPLANNINGEMPLOYEETIMEMAINTENANCEHG = 'serviceplanning/employeetimemaintenancehg';
    public static readonly ICABSSEDATACHANGEMAINTENANCE = 'datachangemaintenance';
    public static readonly ICABSSEPDAICABSINFESTATIONMAINTENANCE = 'pda/infestationmaintenance';
    public static readonly ICABSSEINFESTATIONMAINTENANCE = 'infestationmaintenance';
    public static readonly ICABSSESERVICEPLANCANCEL = 'serviceplancancel';
    public static readonly ICABSSESERVICEAREASEQUENCEMAINTENANCE = 'servicearea/sequencemaintenance';
    public static readonly ICABSSESERVICEVISITMAINTENANCE = 'visit/maintenance';
    public static readonly ICABSSEPREPUSEDMAINTENANCE = 'prepusedmaintenance';
    public static readonly ICABSADIARYENTRY = 'diaryentry';
    public static readonly ICABSCMNATAXJOBSERVICEDETAILGROUPMAINTENANCE = 'maintenance/nataxjobservicedetailgroupmaintenance';
    public static readonly ICABSBAPIRATEMAINTENANCE = 'apiRateMaintenance';
    public static readonly ICABSCMNATAXJOBSERVICECOVERMAINTENANCE = 'jobServiceCoverMaintenance';
    public static readonly ICABSCMCUSTOMERCONTACTEMPLOYEEVIEW = 'customercontactemployeeview';
    public static readonly ICABSCMEMPLOYEEVIEWBRANCHDETAILS = 'employee/branchdetails';
    public static readonly ICABSCMCAMPAIGNENTRY = 'campaign/entry';
    public static readonly ICABSCMPROSPECTMAINTENANCE = 'prospectmaintenance';
    public static readonly ICABSCMTELESALESENTRY = 'telesalesEntry';
    public static readonly ICABSSECREDITSERVICEVISITGROUPMAINTENANCE = 'credit/visitgroupmaintenance';
    public static readonly ICABSCMCONTACTPERSONMAINTENANCE = 'ContactPersonMaintenance';
    public static readonly ICABSSEPREMISEVISITMAINTENANCE = 'premisevisitmaintenance';
    public static readonly ICABSCMCALLOUTMAINTENANCE = 'calloutmaintenance';
    public static readonly ICABSCMSMSREDIRECT = 'SMSRedirect';
    public static readonly ICABSSESERVICEVALUEMAINTENANCE = 'servicevalue';
    public static readonly ICABSCMCUSTOMERCONTACTROOTCAUSEENTRY = 'customercontactroot';
    public static readonly ICABSCMCUSTOMERCONTACTMAINTENANCECOPY = 'CustomerContactMaintenanceCopy';
    public static readonly ICABSSESERVICEPLANDELIVERYNOTEGENERATION = 'business/ServicePlanDeliveryNoteGeneration';
    public static readonly ICABSWORKLISTCONFIRMSUBMIT = 'worklistconfirmsubmit';
    public static readonly ICABSSEDEBRIEFMAINTENANCE = 'debrief/maintain';
    public static readonly ICABSSESERVICEPLANDESCMAINTENANCE = 'plandescmaintenance';
    public static readonly ICABSSEPLANVISITMAINTENANCE = 'planvisitmaintenance';
    public static readonly ICABSSEPLANVISITMAINTENANCE2 = 'planvisitmaintenance2';
    public static readonly ICABSSEPRODUCTIVITYADJUSTMENTMAINTENANCE = 'productivity/adjust/maintenance';
    public static readonly ICABSSEPDAICABSACTIVITYMAINTENANCE = 'maintenance/pdaactivity';
    public static readonly ICABSSESERVICEPLANNINGDETAILHG = 'serviceplanning/serviceplanningdetailhg';
    public static readonly ICABSCMWORKORDERMAINTENANCE = 'workordermaintenance';
    public static readonly ICABSCMTELESALESENTRYORDERLINEMAINTENANCE = 'telesalesEntry/orderline';
    public static readonly ICABSSESERVICEVISITRECOMMENDATIONMAINTENANCE = 'servicevisit/recommendation';
    public static readonly ICABSBBRANCHSERVICEAREAMAINTENANCE = 'maintenance/branchservicearea';
    public static readonly ICABSBVISITTYPEMAINTENANCE = 'maintenance/visit/type';
    public static readonly ICABSSTEAMUSERMAINTENANCE = 'team/user/maintenance';
    public static readonly ICABSBBRANCHSERVICEAREAEMPLOYEEMAINTENANCE = 'servicearea/employee';
}

export class InternalMaintenanceServiceModuleRoutes {
    public static readonly ICABSBBRANCHHOLIDAYMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSBBRANCHHOLIDAYMAINTENANCE;
    public static readonly ICABSTEAMMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSTEAMMAINTENANCE;
    public static readonly ICABSALINKEDPRODUCTSMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSALINKEDPRODUCTSMAINTENANCE;
    public static readonly ICABSSESERVICEPLANNINGEMPLOYEETIMEMAINTENANCEHG = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSESERVICEPLANNINGEMPLOYEETIMEMAINTENANCEHG;
    public static readonly ICABSSEDATACHANGEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSEDATACHANGEMAINTENANCE;
    public static readonly ICABSSEPDAICABSINFESTATIONMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSEPDAICABSINFESTATIONMAINTENANCE;
    public static readonly ICABSSEINFESTATIONMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSEINFESTATIONMAINTENANCE;
    public static readonly ICABSSESERVICEPLANCANCEL = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSESERVICEPLANCANCEL;
    public static readonly ICABSSESERVICEAREASEQUENCEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSESERVICEAREASEQUENCEMAINTENANCE;
    public static readonly ICABSSESERVICEVISITMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSESERVICEVISITMAINTENANCE;
    public static readonly ICABSSEPREPUSEDMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSEPREPUSEDMAINTENANCE;
    public static readonly ICABSADIARYENTRY = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSADIARYENTRY;
    public static readonly ICABSCMNATAXJOBSERVICEDETAILGROUPMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMNATAXJOBSERVICEDETAILGROUPMAINTENANCE;
    public static readonly ICABSSEPREMISEVISITMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSEPREMISEVISITMAINTENANCE;
    public static readonly ICABSBAPIRATEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSBAPIRATEMAINTENANCE;
    public static readonly ICABSCMNATAXJOBSERVICECOVERMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMNATAXJOBSERVICECOVERMAINTENANCE;
    public static readonly ICABSCMCUSTOMERCONTACTEMPLOYEEVIEW = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMCUSTOMERCONTACTEMPLOYEEVIEW;
    public static readonly ICABSCMEMPLOYEEVIEWBRANCHDETAILS = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMEMPLOYEEVIEWBRANCHDETAILS;
    public static readonly ICABSCMPROSPECTMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMPROSPECTMAINTENANCE;
    public static readonly ICABSCMTELESALESENTRY = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMTELESALESENTRY;
    public static readonly ICABSSECREDITSERVICEVISITGROUPMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSECREDITSERVICEVISITGROUPMAINTENANCE;
    public static readonly ICABSCMCAMPAIGNENTRY = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMCAMPAIGNENTRY;
    public static readonly ICABSCMCONTACTPERSONMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMCONTACTPERSONMAINTENANCE;
    public static readonly ICABSCMCALLOUTMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMCALLOUTMAINTENANCE;
    public static readonly ICABSCMSMSREDIRECT = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMSMSREDIRECT;
    public static readonly ICABSCMCUSTOMERCONTACTROOTCAUSEENTRY = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMCUSTOMERCONTACTROOTCAUSEENTRY;
    public static readonly ICABSCMCUSTOMERCONTACTMAINTENANCECOPY = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMCUSTOMERCONTACTMAINTENANCECOPY;
    public static readonly ICABSSESERVICEPLANDELIVERYNOTEGENERATION = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSESERVICEPLANDELIVERYNOTEGENERATION;
    public static readonly ICABSWORKLISTCONFIRMSUBMIT = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSWORKLISTCONFIRMSUBMIT;
    public static readonly ICABSSEDEBRIEFMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSEDEBRIEFMAINTENANCE;
    public static readonly ICABSSESERVICEPLANDESCMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSESERVICEPLANDESCMAINTENANCE;
    public static readonly ICABSSEPLANVISITMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSEPLANVISITMAINTENANCE;
    public static readonly ICABSSEPLANVISITMAINTENANCE2 = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSEPLANVISITMAINTENANCE2;
    public static readonly ICABSSEPDAICABSACTIVITYMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSEPDAICABSACTIVITYMAINTENANCE;
    public static readonly ICABSSEPRODUCTIVITYADJUSTMENTMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSEPRODUCTIVITYADJUSTMENTMAINTENANCE;
    public static readonly ICABSSESERVICEPLANNINGDETAILHG = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSESERVICEPLANNINGDETAILHG;
    public static readonly ICABSCMWORKORDERMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMWORKORDERMAINTENANCE;
    public static readonly ICABSCMTELESALESENTRYORDERLINEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSCMTELESALESENTRYORDERLINEMAINTENANCE;
    public static readonly ICABSSESERVICEVISITRECOMMENDATIONMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSESERVICEVISITRECOMMENDATIONMAINTENANCE;
    public static readonly ICABSBBRANCHSERVICEAREAMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSBBRANCHSERVICEAREAMAINTENANCE;
    public static readonly ICABSBVISITTYPEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSBVISITTYPEMAINTENANCE;
    public static readonly ICABSSTEAMUSERMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSTEAMUSERMAINTENANCE;
    public static readonly ICABSBBRANCHSERVICEAREAEMPLOYEEMAINTENANCE = AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSBBRANCHSERVICEAREAEMPLOYEEMAINTENANCE;
}

export class InternalSearchModuleRoutes {
    public static readonly ICABSACMNATAXJOBSERVICECOVERSEARCH = 'application/mnAtAxJobServiceCover';
    public static readonly ICABSSHISTORYTYPELANGUAGESEARCH = 'application/HistoryTypeLanguageSearch';
    public static readonly ICABSBCLOSEDTEMPLATESEARCH = 'application/closedtemplatesearch';
    public static readonly ICABSBPRODUCTSEARCH = 'application/productSearchGrid';
    public static readonly ICABSCMPROSPECTSEARCH = 'application/prospectsSearch';
    public static readonly ICABSSESERVICEPLANNINGINFO = 'service/serviceplanninginfo';
    public static readonly ICABSBSEASONALTEMPLATESEARCH = 'application/seasonal/templatesearch';
    public static readonly ICABSASERVICESUMMARYDETAIL = 'application/serviceSummaryDetail';
    public static readonly ICABSSEPLANVISITSEARCH = 'application/planvisitsearch';
    public static readonly ICABSSESERVICEPLANSEARCH = 'application/serviceplansearch';
    public static readonly ICABSBPESTNETONLINELEVELSEARCH = 'application/pnollevelsearch';
    public static readonly ICABSBINFESTATIONLEVELSEARCH = 'application/infestationlevelsearch';
    public static readonly ICABSMARKTSELECTSEARCH = 'application/marktselectsearch';
    public static readonly ICABSALOSTBUSINESSREQUESTSEARCH = 'application/lostBusinessRequestSearch';
    public static readonly ICABSBRANCHSERVICEAREASEARCH = 'application/branchservicearea';
    public static readonly ICABSBPRODUCTDETAILSEARCH = 'application/productdetailsearch';
    public static readonly ICABSBWASTETRANSFERTYPESEARCH = 'application/wasteTransfertypesearch';
    public static readonly ICABSALOSTBUSINESSCONTACTSEARCH = 'application/lostBusinessContactSearch';
    public static readonly ICABSCMPROSPECTSTATUSSEARCH = 'application/prospectStatusSearch';
    public static readonly ICABSBLOSTBUSINESSDETAILSEARCH = 'application/lostbusinessdetailsearch';
    public static readonly ICABSBPREPARATIONSEARCH = 'application/preparationSearch';
    public static readonly ICABSSTAXCODESEARCH = 'application/taxcodeSearch';
    public static readonly ICABSCMCALLCENTREREVIEWGRIDMULTI = 'application/callcentrereviewgridmulti';
    public static readonly ICABSSLOSTBUSINESSCONTACTOUTCOMELANGUAGESEARCH = 'application/LostBusinessContactOutcomeLanguageSearch';
    public static readonly ICABSSCONTACTMEDIUMLANGUAGESEARCH = 'application/contactmediumsearch';
    public static readonly ICABSBPRODUCTCOVERSEARCH = 'application/productcoversearch';
    public static readonly ICABSBINVOICERUNDATESELECTPRINT2 = 'application/invoicerundateselect/print2';
    public static readonly ICABSSSYSTEMINVOICECREDITREASONSEARCH = 'application/invoice/creditreason';
    public static readonly ICABSSSYSTEMINVOICEFORMATLANGUAGESEARCH = 'application/invoice/invoiceformatlanguage';
    public static readonly ICABSSSICSEARCH = 'application/invoice/sicsearch';
    public static readonly RIMGLANGUAGESEARCH = 'application/riMGLanguageSearch';
    public static readonly ICABSSEINFESTATIONSEARCH = 'application/service/infestationsearch';
    public static readonly ICABSBCUSTOMERCATEGORYSEARCH = 'application/customer/category';
    public static readonly ICABSAROUTINGSEARCH = 'application/routingsearch';
    public static readonly ICABSSESERVICEVISITSEARCH = 'application/servicevisit';
    public static readonly ICABSSEPREPUSEDSEARCH = 'application/prepusedsearch';
    public static readonly ICABSSEDEBRIEFSEARCH = 'application/debriefsearch';
    public static readonly ICABSSSYSTEMPDAACTIVITYTYPELANGUAGESEARCH = 'application/pdaactivitytype/languagesearch';
    public static readonly ICABSBBRANCHPCODESERVICEGRPENTRYSEARCH = 'application/branchpcode/servicegrpentrysearch';
    public static readonly ICABSSEPDAICABSPREPUSEDSEARCH = 'application/pdaicabsprepusedsearch';
    public static readonly ICABSSEPDAICABSPREPUSED = 'application/pdaicabsprepused';
    public static readonly ICABSSEPDAICABSINFESTATIONSEARCH = 'application/infestation/search';
    public static readonly ICABSBSYSTEMBUSINESSVISITTYPESEARCH = 'application/system/business/visittype';
    public static readonly RIMUSERSEARCH = 'search/user';
    public static readonly ICABSBWASTECONSIGNMENTNOTERANGETYPESEARCH = 'application/wasteConsignmentNoteRange';
    public static readonly ICABSBVISITACTIONSEARCH = 'search/visitaction';
    public static readonly RIMUSERTYPESEARCH = 'riM/usertype';
}

export class ITFunctionsModuleRoutesConstant {
    public static readonly RIMUSERINFORMATIONMAINTENANCE = 'maintenance/user';
    public static readonly ICABSBBUSINESSREGISTRYGRID = 'businessRegistryGrid';
    public static readonly RIMUSERTYPEMAINTENANCE = 'maintenance/usertype';
    public static readonly RIADDUSERALLSERVERS = 'used/addtoservers';
    public static readonly RIBATCHTRANSLATIONS = 'translations/batch';
    public static readonly RIIMPORT = 'importing';
    public static readonly RIUSERINFORMATIONDETAIL = 'userInformationDetail';
}

export class ITFunctionsModuleRoutes {
    public static readonly ICABSBBRANCHMAINTENANCE = 'maintenance/branch';
    public static readonly RIMGTRANSLATIONMAINTENANCE = 'maintenance/rimgtranslation';
    public static readonly RIMUSERTYPEMENUACCESSMAINTENANCE = 'maintenance/usertype/menuaccess';
    public static readonly ICABSBCOMPANYMAINTENANCE = 'maintenance/company';
    public static readonly RIMUSERINFORMATIONMAINTENANCE = AppModuleRoutes.ITFUNCTIONS + ITFunctionsModuleRoutesConstant.RIMUSERINFORMATIONMAINTENANCE;
    public static readonly ICABSBBUSINESSREGISTRYGRID = AppModuleRoutes.ITFUNCTIONS + ITFunctionsModuleRoutesConstant.ICABSBBUSINESSREGISTRYGRID;
    public static readonly RIMUSERTYPEMAINTENANCE = AppModuleRoutes.ITFUNCTIONS + ITFunctionsModuleRoutesConstant.RIMUSERTYPEMAINTENANCE;
    public static readonly RIMREPORTVIEWERSEARCH = 'riMReportViewerSearch';
    public static readonly RIADDUSERALLSERVERS = AppModuleRoutes.ITFUNCTIONS + ITFunctionsModuleRoutesConstant.RIADDUSERALLSERVERS;
    public static readonly RIIMPORT = AppModuleRoutes.ITFUNCTIONS + ITFunctionsModuleRoutesConstant.RIIMPORT;
    public static readonly RIUSERINFORMATIONDETAIL = AppModuleRoutes.ITFUNCTIONS + ITFunctionsModuleRoutesConstant.RIUSERINFORMATIONDETAIL;
}

export class PeopleModuleRoutes {
    public static readonly ICABSAREMPLOYEEEXPORTGRIDBUSINESS = 'employeeExport/business';
    public static readonly ICABSAREMPLOYEEEXPORTGRIDREGION = 'employeeExport/region';
    public static readonly ICABSAREMPLOYEEEXPORTGRIDBRANCH = 'employeeExport/branch';
    public static readonly ICABSBPREPCHARGERATEMAINTENANCE = 'prepcharge/ratemaintenance';
    public static readonly ICABSBEMPLOYEEMAINTENANCE = AppModuleRoutes.PEOPLE + 'business/employeemaintenance';
}

export class ProspectToContractModuleRoutes {
    public static readonly ICABSCMPROSPECTBULKIMPORT = 'application/CMProspectBulkImport';
    public static readonly ICABSCMPROSPECTENTRYGRID = 'contactmanagement/prospectEntryGrid';
    public static readonly ICABSBBUSINESSORIGINMAINTENANCE = 'prospect/business/businessOrigin';
    public static readonly ICABSCMPROSPECTMAINTENANCE = 'maintenance/prospectmaintenance';
    public static readonly ICABSCMDIARYMAINTENANCE = 'maintenance/diary';
    public static readonly ICABSCMDIARYDAYMAINTENANCE = 'maintenance/diarydaymaintaianance';
    public static readonly ICABSCMPROSPECTGRID = 'prospectgrid';
    public static readonly ICABSBLOSTBUSINESSDETAILGRID = 'lostbusinessdetailgrid';
    public static readonly ICABSCMPIPELINEPROSPECTMAINTENANCE = 'maintenance/prospect';
    public static readonly ICABSCMPROSPECTENTRYMAINTENANCE = {
        NATAXJOB: 'maintenance/prospectentry/nataxjob',
        CONFIRM: 'maintenance/prospectentry/confirm'
    };
}

export class ServiceDeliveryModuleRoutes {
    public static readonly ICABSSESERVICEDOCKETDATAENTRY = 'service/visit/maintenance/docketentry';
    public static readonly ICABSSEPESVISITGRID = 'pdareturns/SePESVisitGrid';
    public static readonly ICABSWORKLISTCONFIRM = 'service/worklist';
    public static readonly ICABSAPRODUCTSALESDELIVERIESDUEGRID = 'reportsplanning/productsalesdeliverydue';
    public static readonly ICABSSEHCARISKASSESSMENTGRID = 'riskassessmentgrid';
    public static readonly ICABSARPRENOTIFICATIONREPORT = 'reports/prenotificationreport';
    public static readonly ICABSARRETURNEDPAPERWORKGRID = 'lettersandlabels/returnedpaperworkgrid';
    public static readonly ICABSSERVICECALLTYPEGRIDBUSINESS = 'businessservicecalltype';
    public static readonly ICABSSERVICECALLTYPEGRIDREGION = 'regionservicecalltype';
    public static readonly ICABSSERVICECALLTYPEGRIDBRANCH = 'branchservicecalltype';
    public static readonly ICABSSERVICECALLTYPEGRIDSERVICEAREA = 'serviceareaservicecalltype';
    public static readonly ICABSARENVAGENCYBUSINESSWASTE = 'wasteconsignment/envagencybusinesswaste';
    public static readonly ICABSBSERVICEAREAALLOCATIONGRID = 'servicearea/allocation';
    public static readonly iCABSARENVAGENCYEXCEPTIONS = 'wasteconsignment/envagencyexceptions';
    public static readonly ICABSBBRANCHSERVICEAREAGRID = 'branch/serviceArea';
    public static readonly ICABSBVALIDLINKEDPRODUCTSGRID = 'products/valid/linked';
    public static readonly ICABSBPRODUCTLANGUAGEMAINTENANCE = 'maintenance/productlanguage';
    public static readonly ICABSARDAILYPRENOTIFICATIONREPORT = 'dailyprenotificationreport';
    public static readonly ICABSSEDEBRIEFEMPLOYEEGRID = 'debrief/employee';
    public static readonly ICABSARVANLOADINGREPORT = 'report/vanloading';
    public static readonly ICABSSEDEBRIEFBRANCHGRID = 'debriefbranchgrid';
    public static readonly ICABSSEBLANKCONSIGNMENTNOTEPRINT = 'blankconsignmentnoteprint';
    public static readonly ICABSSEGROUPSERVICEVISITENTRYGRID = 'groupvisit/serviceentrygrid';
    public static readonly ICABSSEPDAWORKLISTENTRYGRID = 'service/pda/worklistentry';
    public static readonly ICABSSESERVICEVISITREJECTIONSGRID = 'visit/rejections';
    public static readonly ICABSSESERVICEVISITREJECTIONSGRID_ALL = 'visit/rejections/Allbranches';
    public static readonly ICABSARENVAGENCYQUARTERLYRETURN = 'envagencyquarterly/envagencyquarterlyreturn';
    public static readonly ICABSARWASTETRANSFERNOTESPRINT = 'lettersandlabels/wastetransfernotes';
    public static readonly ICABSSEWASTECONSIGNMENTNOTEGENERATE = 'wasteconsignmentnote';
    public static readonly ICABSBVISITACTIONMAINTENANCE = 'business/visitaction';
    public static readonly ICABSSEWASTECONSIGNMENTNOTEGENERATEDETAIL = 'wasteconsignmentnotegeneratedetail';
    public static readonly ICABSBBRANCHSERVICEAREAEMPLOYEEGRID = 'servicearea/employee/grid';
}

export class ServicePlanningModuleRoutesConstant {
    public static readonly ICABSSEAREAREALLOCATIONGRID = 'area/reallocation';
}

export class ServicePlanningModuleRoutes {
    public static readonly ICABSSESERVICEMOVEGRID = 'ServiceMoveGridSearch';
    public static readonly ICBSSACALENDERHISTORYGRID = 'application/calenderHistoryGrid';
    public static readonly ICABSCLOSEDTEMPLATEMAINTENANCE = 'application/closedtemplatemaintenance';
    public static readonly ICABSACLOSEDSERVICEGRID = 'Templates/HolidayClosedTemplateUse';
    public static readonly ICABSSESERVICEPLANNINGGRIDHG = 'serviceplanninggridhg';
    public static readonly ICABSACALENDARSERVICEGRID = 'Templates/CalendarTemplateUse';
    public static readonly ICABSASERVICECOVERCALENDARDATESMAINTENANCEGRID = 'calendarandSeasons/serviceCoverCalendarDateMaintenanceGrid';
    public static readonly ICABSSESERVICEPLANNINGSPLITSERVICEMAINTENANCEHG = 'split/service/maintenanceHG';
    public static readonly ICABSSERVICEPLANNINGCALENDAR = 'calendarandSeasons/serviceplanningcalendar';
    public static readonly ICABSSECLEARDOWNPLANVISITS = 'visitmaintenance/cleardownplanvisit';
    public static readonly ICABSSEPLANNINGDIARY = 'planning/diary';
    public static readonly ICABSSESERVICEAREARESEQUENCING = 'servicearea/resequence';
    public static readonly ICABSSEAREAREALLOCATIONGRID = AppModuleRoutes.SERVICEPLANNING + 'area/reallocation';
    public static readonly ICABSSESERVICEAREAPOSTCODESEQUENCEGRID = 'servicearea/postcode/sequence';
    public static readonly ICABSSESERVICEPLANNINGEXPORTENTRY = 'exportentry';
}
export class RenegotiationRoutesConstants {
    public static readonly ICABSARENEGSGRID = 'grid';
    public static readonly ICABSARENEGSADDUPDATEROUTE = 'grid/addupdate';
    public static readonly ICABSARENEGSPREMISESMAINTENANCEROUTE = 'grid/premisesmaintenance';
    public static readonly ICABSARENEGSPREMISESGRIDROUTE = 'grid/premisesgrid';
    public static readonly ICABSARENEGSCONTRACTDETAILSROUTE = 'grid/contactdetails';
    public static readonly ICABSARENEGSACCOUNTDETAILSROUTE = 'grid/accountdetails';
    public static readonly ICABSARENEGSSERVICECOVERGRIDROUTE = 'grid/servicecovergrid';
}
export class RenegotiationRoutes {
    public static readonly ICABSARENEGSGRID = RenegotiationRoutesConstants.ICABSARENEGSGRID;
    public static readonly ICABSARENEGSADDUPDATE = RenegotiationRoutesConstants.ICABSARENEGSADDUPDATEROUTE;
    public static readonly ICABSARENEGSACCOUNTDETAILS = RenegotiationRoutesConstants.ICABSARENEGSACCOUNTDETAILSROUTE;
    public static readonly ICABSARENEGSSERVICECOVERGRID = RenegotiationRoutesConstants.ICABSARENEGSSERVICECOVERGRIDROUTE;

    public static readonly ICABSARENEGSGRIDFULLPATH = AppModuleRoutes.RENEGOTIATIONS + RenegotiationRoutesConstants.ICABSARENEGSGRID;
    public static readonly ICABSARENEGSADDUPDATEFULLPATH = AppModuleRoutes.RENEGOTIATIONS + RenegotiationRoutesConstants.ICABSARENEGSADDUPDATEROUTE;
    public static readonly ICABSARENEGSPREMISESMAINTENANCE = RenegotiationRoutesConstants.ICABSARENEGSPREMISESMAINTENANCEROUTE;
    public static readonly ICABSARENEGSPREMISESMAINTENANCENEW = RenegotiationRoutesConstants.ICABSARENEGSPREMISESMAINTENANCEROUTE;
    public static readonly ICABSARENEGSPREMISESGRID = RenegotiationRoutesConstants.ICABSARENEGSPREMISESGRIDROUTE;
    public static readonly ICABSARENEGSPREMISESMAINTENANCEPATH = AppModuleRoutes.RENEGOTIATIONS + RenegotiationRoutesConstants.ICABSARENEGSPREMISESMAINTENANCEROUTE;
    public static readonly ICABSARENEGSPREMISESGRIDPATH = AppModuleRoutes.RENEGOTIATIONS + RenegotiationRoutesConstants.ICABSARENEGSPREMISESGRIDROUTE;
    public static readonly ICABSAPREMISESMAINTAINANCEPATH = AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESMAINTENANCE + 'maintenance/premise';
    public static readonly ICABSARENEGSCONTRACTDETAILS = RenegotiationRoutesConstants.ICABSARENEGSCONTRACTDETAILSROUTE;
    public static readonly ICABSARENEGSCONTRACTDETAILSFULLPATH = AppModuleRoutes.RENEGOTIATIONS + RenegotiationRoutesConstants.ICABSARENEGSCONTRACTDETAILSROUTE;
    public static readonly ICABSARENEGSACCOUNTDETAILSFULLPATH = AppModuleRoutes.RENEGOTIATIONS + RenegotiationRoutesConstants.ICABSARENEGSACCOUNTDETAILSROUTE;
    public static readonly ICABSARENEGSSERVICECOVERGRIDFULLPATH = AppModuleRoutes.RENEGOTIATIONS + RenegotiationRoutesConstants.ICABSARENEGSSERVICECOVERGRIDROUTE;
    public static readonly ICABSARENEGSPREMISESMAINTENANCENEWFULLPATH = AppModuleRoutes.RENEGOTIATIONS + RenegotiationRoutesConstants.ICABSARENEGSPREMISESMAINTENANCEROUTE;
}
export class BIReportsRoutesConstant {
    // PDA Module
    public static readonly ICABSARTECHNICIANWORKSUMMARYREPORT = 'technicianwork/summary';

    // PDA Lead Module
    public static readonly ICABSCMLEADSSUBMITTEDBRANCH = 'leads/submittedbranch';
    public static readonly ICABSCMLEADSSUBMITTEDBUSINESS = 'leads/submittedbusiness';
    public static readonly ICABSCMLEADSSUBMITTEDREGION = 'leads/submittedregion';
    public static readonly ICABSCMLEADSSUBMITTEDDETAIL = 'leads/submitteddetails';

    // Sales Module
    public static readonly ICABSARRELEASEDFORJOBINVOICEGRID = 'releasedinvoice/job';
    public static readonly ICABSARRELEASEDFORPRODUCTINVOICEGRID = 'releasedinvoice/product';
    public static readonly ICABSARRELEASEDFORJOBINVOICEBRANCHGRID = 'releasedinvoicebranch/job';
    public static readonly ICABSARRELEASEDFORJOBINVOICEREGIONGRID = 'releasedinvoiceregion/job';
    public static readonly ICABSARRELEASEDFORINVOICEBUSINESSGRID = 'releasedinvoicebusiness/job';
    public static readonly ICABSARRELEASEDFORPRODUCTINVOICEBRANCHGRID = 'releasedinvoicebranch/product';
    public static readonly ICABSARRELEASEDFORPRODUCTINVOICEREGIONGRID = 'releasedinvoiceregion/product';
    public static readonly ICABSSSALESSTATISTICSBUSINESS = 'statistics/business';
    public static readonly ICABSSSALESSTATISTICSBRANCH = 'statistics/branch';
    public static readonly ICABSSSALESSTATISTICSREGION = 'statistics/region';
    public static readonly ICABSSSALESSTATISTICSEMPLOYEE = 'statistics/employee';
    public static readonly ICABSSSTATISTICSDETAIL = 'statistics/sales/detail';
    public static readonly iCABSARJOBCREDITBRANCH = 'jobcredit/branch';
    public static readonly iCABSARJOBCREDITDETAIL = 'jobcredit/detail';
    public static readonly iCABSARJOBCREDITBUSINESS = 'jobcredit/business';
    public static readonly iCABSARJOBCREDITREGION = 'jobcredit/region';
    public static readonly iCABSARORIGINOFBUSINESSBRANCH = 'originofbusiness/branch';
    public static readonly iCABSARORIGINOFBUSINESSEMPLOYEE = 'originofbusiness/employee';
    public static readonly iCABSARORIGINOFBUSINESSBUSINESS = 'originofbusiness/business';
    public static readonly iCABSARORIGINOFBUSINESSDETAIL = 'originofbusiness/detail';
    public static readonly iCABSARORIGINOFBUSINESSREGION = 'originofbusiness/region';

    // Portfolio Module
    public static readonly ICABSARDAILYTRANSACTIONSGRID = 'dailytransactiongrid';
    public static readonly ICABSARDAILYTRANSACTIONSBUSINESSGRID = 'dailytransactiongrid/by/business';
    public static readonly ICABSASUSPENDEDPORTFOLIOBRANCH = 'suspended';
    public static readonly ICABSASUSPENDEDPORTFOLIOBUSINESS = 'suspended/by/business';
    public static readonly ICABSARPORTFOLIOREPORT = 'business';
    public static readonly ICABSARPORTFOLIOREPORTBRANCH = 'branch';
    public static readonly ICABSARPORTFOLIOREPORTREGION = 'region';
    public static readonly ICABSARPORTFOLIOREPORTDETAIL = 'portfolioreport/details';
    public static readonly ICABSARPORTFOLIOREPORTCONTRACTDETAIL = 'portfolioreport/contract/details';
    public static readonly ICABSARLOSTBUSINESSANALYSISBUSINESSGRID = 'lostbusiness/analysisgrid/business';
    public static readonly ICABSARLOSTBUSINESSANALYSISBUSINESSGRIDGROUPBY = 'lostbusiness/analysisgrid/business/groupby';
    public static readonly ICABSARLOSTBUSINESSANALYSISGRID = 'lostbusiness/analysisgrid/branch';
    public static readonly ICABSARLOSTBUSINESSANALYSISGRIDGROUPBY = 'lostbusiness/analysisgrid/branch/groupby';
    public static readonly ICABSARCONTRACTREPORT = 'general/contract';
    public static readonly ICABSARCONTRACTEXPIREBUSINESSGRID = 'general/contractexpirebusinessgrid';
    public static readonly ICABSARCONTRACTDUETOEXPIREGRID = 'general/contractduetoexpiregrid';
    public static readonly ICABSSNETTGAINBUSINESS = 'movement/nettgain/business';
    public static readonly ICABSSNETTGAINBRANCH = 'movement/nettgain/branch';
    public static readonly ICABSSNETTGAINDETAIL = 'movement/nettgain/details';
    public static readonly ICABSACONTRACTSWITHEXPIRINGPOBUSINESSGRID = 'contracts/withexpiring/pob';
    public static readonly ICABSACONTRACTSWITHEXPIRINGPOGRID = 'contracts/withexpiring/pogrid';
    public static readonly ICABSARACCOUNTSBYCATEGORY = 'accounts/category';
    public static readonly ICABSAUNALLOCATEDUNITSGRID = 'unallocatedunitsgrid';

    // Invoice Module
    public static readonly NEXTINVOICERUNFORECASTDETAIL = 'nextinvoicerunforecastdetail';
    public static readonly ICABSARINVOICEANALYSISGRID = 'analysis';
    public static readonly ICABSARINVOICEANALYSISDETAILGRID = 'analysis/detail';
    public static readonly ICABSARINVOICEANALYSISEMPLOYEEGRID = 'analysis/employee';
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTSTANDINGBRANCH = 'lostrequest/business/outstandingbranch';
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTSTANDINGBUSINESS = 'lostrequest/business/outstandingbusiness';
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTSTANDINGREGION = 'lostrequest/business/outstandingregion';
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTSTANDINGEMPLOYEE = 'lostrequest/business/outstandingemployee';
    public static readonly ICABSARNEXTINVOICERUNFORECASTBUSINESSGRID = 'forecastgrid';
    public static readonly RIBATCHCOMPONENT = 'ribatch';

    // Service Module
    public static readonly ICABSSESERVICEDAILYPRODUCTIVITYGRID = 'productivity/daily';
    public static readonly ICABSSESERVICEDAILYPRODUCTIVITYGRIDFR = 'productivity/daily/fr';
    public static readonly ICABSSEDAILYPRODUCTIVITYBUSINESSGRID = 'productivity/daily/business';
    public static readonly ICABSSEDAILYPRODUCTIVITYBRANCHGRID = 'productivity/daily/branch';
    public static readonly ICABSSEDAILYPRODUCTIVITYSUMMARYGRID = 'productivity/daily/summary';
    public static readonly ICABSTECHNICIANSERVICEVISITGRID = 'technician/servicevisit';
    public static readonly ICABSSESTATEOFSERVICEREPORTSGRIDBYBUSINESS = 'stateofservice/by/business';
    public static readonly ICABSSESTATEOFSERVICEREPORTSGRIDBYREGION = 'stateofservice/by/region';
    public static readonly ICABSSESTATEOFSERVICEREPORTSGRIDBYBRANCH = 'stateofservice/by/branch';
    public static readonly ICABSSESTATEOFSERVICEREPORTSGRIDAGEDBYBRANCH = 'sosagedprofile/by/branch';
    public static readonly ICABSSESTATEOFSERVICEREPORTSGRIDAGEDBYBUSINESS = 'sosagedprofile/by/business';
    public static readonly ICABSSEBATCHSOSAGEDPROFILEBRANCHGRID = 'batchsosagedprofile/by/branch';
    public static readonly ICABSSEBATCHSOSAGEDPROFILEBUSINESSGRID = 'batchsosagedprofile/by/business';
    public static readonly ICABSSEBATCHSTATEOFSERVICEBUSINESSGRID = 'stateofservice/batch/business';
    public static readonly ICABSSEBATCHSTATEOFSERVICEBRANCHGRID = 'stateofservice/batch/branch';
    public static readonly ICABSSEBATCHSTATEOFSERVICEREGIONGRID = 'stateofservice/batch/region';
    public static readonly ICABSSESTATEOFSERVICEGRID = 'stateofservice/grid';
    public static readonly ICABSSEPRODUCTIVITYMULTIPESGRIDBYBUSINESS = 'productivity/multi/pes/business';
    public static readonly ICABSSEPRODUCTIVITYMULTIPESGRIDBYREGION = 'productivity/multi/pes/region';
    public static readonly ICABSSEPRODUCTIVITYMULTIPESGRIDBYBRANCH = 'productivity/multi/pes/branch';
    public static readonly ICABSSEPRODUCTIVITYBUSINESSPESGRID = 'productivity/business';
    public static readonly ICABSSSALESSTATISTICSDETAIL = 'statistics/details';
    public static readonly ICABSSEPRODUCTIVITYBRANCHPESGRID = 'productivity/branch';
    public static readonly ICABSSEPRODUCTIVITYPESDETAIL = 'productivity/detail';
    public static readonly ICABSSEPRODUCTIVITYBUSINESSGRID = 'productivity/hc/business';
    public static readonly ICABSSEPRODUCTIVITYREGIONGRID = 'productivity/hc/region';
    public static readonly ICABSSEPRODUCTIVITYBRANCHGRID = 'productivity/hc/branch';
    public static readonly ICABSSEPRODUCTIVITYGRID = 'productivity/hc/details';
    public static readonly ICABSSEPRODUCTIVITYSERVICEVISITSPESGRID = 'productivity/servicevisits';
    public static readonly ICABSSEPRODUCTIVITYREGIONPESGRID = 'productivity/region';
    public static readonly ICABSARSTATEOFSERVICEDETAILREPORT = 'stateofservice/detailreport';
    public static readonly ICABSSEAGEDARREARSREPORTGRIDBYBUSINESS = 'stateofservice/agedarrears/business';
    public static readonly ICABSSEAGEDARREARSREPORTGRIDBYBRANCH = 'stateofservice/agedarrears/branch';
    public static readonly ICABSSEBATCHSOSAGEDARREARSBUSINESSGRID = 'stateofservice/batchsos/agedarrears/business';
    public static readonly ICABSSEBATCHSOSAGEDARREARSBRANCHGRID = 'stateofservice/batchsos/agedarrears/branch';
    public static readonly ICABSSEBATCHSOSAGEDARREARSDETAILGRID = 'stateofservice/batchsos/agedarrears/detail';
    public static readonly ICABSARADDITIONALVISITREPORTGRID = 'additionalvisit/report/grid';
    public static readonly ICABSARADDITIONALVISITREPORTDETAILSGRID = 'additionalvisit/report/detail/grid';
    public static readonly ICABSSESTATEOFSERVICENATACCOUNTGRID = 'stateofservice/netaccount/grid';
    public static readonly ICABSARCUSTOMERCCOREPORTGRID = 'customer/cco';
    public static readonly ICABSSEINFESTATIONGRID = 'infestations/by/branch';
    public static readonly ICABSARSERVICEANDINVOICESUSPENDBUSINESS = 'suspended/by/business';
    public static readonly ICABSARSERVICEANDINVOICESUSPENDBRANCH = 'suspended/by/branch';
    public static readonly ICABSARSERVICESUSPENDHISTORY = 'suspended/servicehistory';
    public static readonly ICABSARSERVICEANDINVOICESUSPENDREGION = 'suspended/by/region';
    public static readonly ICABSBULKPROOFOFSERVICE = 'proof';
    public static readonly ICABSSEOUTSTANDINGINSTALLATIONSGRID = 'oustanding/installations/branch';
    public static readonly ICABSSEOUTSTANDINGBUSINESSINSTALLATIONSGRID = 'oustanding/installations/business';
    public static readonly ICABSSEOUTSTANDINGREMOVALSGRID = 'oustanding/removals/branch';
    public static readonly ICABSSEOUTSTANDINGBUSINESSREMOVALSGRID = 'oustanding/removals/business';
    public static readonly ICABSARIWSSTOCKREQUIREMTS = 'stock/requirements';
    public static readonly ICABSSEINFESTATIONSERVICECOVERGRID = 'infestations/by/all';
    public static readonly ICABSSEVISITTYPEGRID = 'servicevisit/by/types';
    public static readonly ICABSSESTATICVISITGRIDYEARBRANCH = 'staticvisit/by/branch';
    public static readonly ICABSSECOMPONENTORDERINGLISTGRID = 'componentordering/by/branch';
    public static readonly ICABSSECOMPONENTREPLACEMENTDETAILGRID = 'componentordering/detail';

    // Turnover Module
    public static readonly ICABSARTURNOVERBUSINESS = 'business';
    public static readonly ICABSARTURNOVERBUSINESSMARKET = 'business/market';
    public static readonly ICABSARTURNOVERBUSINESSSERVICE = 'business/service';
    public static readonly ICABSARTURNOVERBRANCH = 'branch';
    public static readonly ICABSARTURNOVERBRANCHMARKET = 'branch/market';
    public static readonly ICABSARTURNOVERBRANCHSERVICE = 'branch/service';
    public static readonly ICABSARBRANCHTURNOVERDETAILGRID = 'branch/detail';
    public static readonly ICABSADEFERREDTURNOVERBUSINESSGRID = 'deferred/turnover/business';
    public static readonly ICABSADEFERREDTURNOVERBRANCHGRID = 'deferred/turnover/branch';
    public static readonly ICABSADEFERREDTURNOVERDETAILGRID = 'deferred/turnover/detail';

    // StockUsage Module
    public static readonly ICABSSESTOCKUSAGEESTIMATESBRANCH = 'branch';
    public static readonly ICABSSESTOCKUSAGEESTIMATESDETAIL = 'detail';
    public static readonly ICABSSESTOCKUSAGEESTIMATESBUSINESS = 'business';
    public static readonly ICABSSESTOCKUSAGEESTIMATESREGION = 'region';
    public static readonly ICABSSESTOCKUSAGEESTIMATESPREMISES = 'premises';
    public static readonly ICABSSESTOCKUSAGEESTIMATESBRANCHDETAIL = 'branchDetail';

    // Credit Analysis
    public static readonly ICABSARCREDITREASONANALYSIS = 'reasonanalysis';
    public static readonly ICABSARCREDITREASONANALYSISBUSINESS = 'reasonbusiness';
    public static readonly ICABSARCREDITREASONANALYSISREGION = 'reasonregion';
    public static readonly ICABSARCREDITREASONANALYSISDETAIL = 'reasonanalysis/detail';
    public static readonly ICABSARCREDITREASONANALYSISDETAILDETAIL = 'reasonanalysis/details';

    //ActaualVsContractual Module
    public static readonly ICABSARACTUALVSCONTRACTUALREPORT = 'acual/vscontractual';

    //Client Retention Module
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTCOMEBUSINESS = 'requestsoutcome/by/business';
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTCOMEBRANCH = 'requestsoutcome/by/branch';
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTCOMEREGION = 'requestsoutcome/by/region';
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTCOMEDETAIL = 'requestsoutcome/detail';

    //ContactManagement Module
    public static readonly ICABSCMTICKETANALYSISGENERATION = 'ticket/analysis';
    public static readonly ICABSCMROOTCAUSEANALYSISGRIDBUSINESS = 'rootcause/analysis/business';
    public static readonly ICABSCMROOTCAUSEANALYSISGRIDREGION = 'rootcause/analysis/region';
    public static readonly ICABSCMROOTCAUSEANALYSISGRIDBRANCH = 'rootcause/analysis/branch';
    public static readonly ICABSCMROOTCAUSEANALYSISDETAILGRID = 'rootcause/analysis/detail';
    public static readonly ICABSCMROOTCAUSEANALYSISBUSINESSGRID = 'rootcause/analysis/detail/business';
    public static readonly ICABSCMROOTCAUSEANALYSISREGIONGRID = 'rootcause/analysis/detail/region';
    public static readonly ICABSCMROOTCAUSEANALYSISBRANCHGRID = 'rootcause/analysis/detail/branch';
    public static readonly ICABSCMROOTCAUSEANALYSISBRANCHGRIDREGION = 'rootcause/analysis/detail/branch_region';

    //API Module
    public static readonly iCABSACONTACTSAPIEXEMPTGRID = 'exempt';
    public static readonly ICABSARTAXCODEREPORT = 'taxcodereport';
    public static readonly ICABSARTAXCODESUMMARY = 'taxcodesummary';
    public static readonly ICABSARTAXCODESUMMARYDETAIL = 'taxcodesummary/detail';
}
export class BIReportsRoutes {
    // PDA Module
    public static readonly ICABSARTECHNICIANWORKSUMMARYREPORT = AppModuleRoutes.BIREPORTS_PDA + BIReportsRoutesConstant.ICABSARTECHNICIANWORKSUMMARYREPORT;
    public static readonly DAILYTRANSACTIONGRID = AppModuleRoutes.BIREPORTS_PDA + BIReportsRoutesConstant.ICABSARTECHNICIANWORKSUMMARYREPORT;

    // PDA Lead Module
    public static readonly ICABSCMLEADSSUBMITTEDBRANCH = AppModuleRoutes.BIREPORTS_PDA_LEAD + BIReportsRoutesConstant.ICABSCMLEADSSUBMITTEDBRANCH;
    public static readonly ICABSCMLEADSSUBMITTEDBUSINESS = AppModuleRoutes.BIREPORTS_PDA_LEAD + BIReportsRoutesConstant.ICABSCMLEADSSUBMITTEDBUSINESS;
    public static readonly ICABSCMLEADSSUBMITTEDREGION = AppModuleRoutes.BIREPORTS_PDA_LEAD + BIReportsRoutesConstant.ICABSCMLEADSSUBMITTEDREGION;
    public static readonly ICABSCMLEADSSUBMITTEDDETAIL = AppModuleRoutes.BIREPORTS_PDA_LEAD + BIReportsRoutesConstant.ICABSCMLEADSSUBMITTEDDETAIL;

    // Sales Module
    public static readonly ICABSARRELEASEDFORJOBINVOICEGRID = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSARRELEASEDFORJOBINVOICEGRID;
    public static readonly ICABSARRELEASEDFORPRODUCTINVOICEGRID = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSARRELEASEDFORPRODUCTINVOICEGRID;
    public static readonly ICABSARRELEASEDFORJOBINVOICEBRANCHGRID = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSARRELEASEDFORJOBINVOICEBRANCHGRID;
    public static readonly ICABSARRELEASEDFORJOBINVOICEREGIONGRID = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSARRELEASEDFORJOBINVOICEREGIONGRID;
    public static readonly ICABSARRELEASEDFORINVOICEBUSINESSGRID = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSARRELEASEDFORINVOICEBUSINESSGRID;
    public static readonly ICABSARRELEASEDFORPRODUCTINVOICEBRANCHGRID = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSARRELEASEDFORPRODUCTINVOICEBRANCHGRID;
    public static readonly ICABSARRELEASEDFORPRODUCTINVOICEREGIONGRID = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSARRELEASEDFORPRODUCTINVOICEREGIONGRID;
    public static readonly ICABSSSALESSTATISTICSBUSINESS = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSSSALESSTATISTICSBUSINESS;
    public static readonly ICABSSSALESSTATISTICSBRANCH = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSSSALESSTATISTICSBRANCH;
    public static readonly ICABSSSALESSTATISTICSREGION = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSSSALESSTATISTICSREGION;
    public static readonly ICABSSSALESSTATISTICSEMPLOYEE = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSSSALESSTATISTICSEMPLOYEE;
    public static readonly ICABSSSTATISTICSDETAIL = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSSSTATISTICSDETAIL;
    public static readonly iCABSARJOBCREDITBRANCH = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.iCABSARJOBCREDITBRANCH;
    public static readonly iCABSARJOBCREDITDETAIL = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.iCABSARJOBCREDITDETAIL;
    public static readonly iCABSARJOBCREDITBUSINESS = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.iCABSARJOBCREDITBUSINESS;
    public static readonly iCABSARJOBCREDITREGION = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.iCABSARJOBCREDITREGION;
    public static readonly iCABSARORIGINOFBUSINESSBRANCH = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.iCABSARORIGINOFBUSINESSBRANCH;
    public static readonly iCABSARORIGINOFBUSINESSEMPLOYEE = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.iCABSARORIGINOFBUSINESSEMPLOYEE;
    public static readonly iCABSARORIGINOFBUSINESSBUSINESS = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.iCABSARORIGINOFBUSINESSBUSINESS;
    public static readonly iCABSARORIGINOFBUSINESSDETAIL = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.iCABSARORIGINOFBUSINESSDETAIL;
    public static readonly iCABSARORIGINOFBUSINESSREGION = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.iCABSARORIGINOFBUSINESSREGION;
    // Portfolio Module
    public static readonly ICABSARDAILYTRANSACTIONSGRID = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARDAILYTRANSACTIONSGRID;
    public static readonly ICABSARDAILYTRANSACTIONSBUSINESSGRID = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARDAILYTRANSACTIONSBUSINESSGRID;
    public static readonly ICABSASUSPENDEDPORTFOLIOBRANCH = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSASUSPENDEDPORTFOLIOBRANCH;
    public static readonly ICABSASUSPENDEDPORTFOLIOBUSINESS = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSASUSPENDEDPORTFOLIOBUSINESS;
    public static readonly ICABSARPORTFOLIOREPORT = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARPORTFOLIOREPORT;
    public static readonly ICABSARPORTFOLIOREPORTBRANCH = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARPORTFOLIOREPORTBRANCH;
    public static readonly ICABSARPORTFOLIOREPORTREGION = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARPORTFOLIOREPORTREGION;
    public static readonly ICABSARPORTFOLIOREPORTDETAIL = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARPORTFOLIOREPORTDETAIL;
    public static readonly ICABSARPORTFOLIOREPORTCONTRACTDETAIL = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARPORTFOLIOREPORTCONTRACTDETAIL;
    public static readonly ICABSARLOSTBUSINESSANALYSISBUSINESSGRID = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISBUSINESSGRID;
    public static readonly ICABSARLOSTBUSINESSANALYSISBUSINESSGRIDGROUPBY = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISBUSINESSGRIDGROUPBY;
    public static readonly ICABSARLOSTBUSINESSANALYSISGRID = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISGRID;
    public static readonly ICABSARLOSTBUSINESSANALYSISGRIDGROUPBY = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARLOSTBUSINESSANALYSISGRIDGROUPBY;
    public static readonly ICABSARCONTRACTREPORT = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARCONTRACTREPORT;
    public static readonly ICABSARCONTRACTEXPIREBUSINESSGRID = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARCONTRACTEXPIREBUSINESSGRID;
    public static readonly ICABSARCONTRACTDUETOEXPIREGRID = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARCONTRACTDUETOEXPIREGRID;
    public static readonly ICABSSNETTGAINBUSINESS = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSSNETTGAINBUSINESS;
    public static readonly ICABSSNETTGAINBRANCH = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSSNETTGAINBRANCH;
    public static readonly ICABSSNETTGAINDETAIL = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSSNETTGAINDETAIL;
    public static readonly ICABSACONTRACTSWITHEXPIRINGPOBUSINESSGRID = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSACONTRACTSWITHEXPIRINGPOBUSINESSGRID;
    public static readonly ICABSACONTRACTSWITHEXPIRINGPOGRID = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSACONTRACTSWITHEXPIRINGPOGRID;
    public static readonly ICABSARACCOUNTSBYCATEGORY = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSARACCOUNTSBYCATEGORY;
    public static readonly ICABSAUNALLOCATEDUNITSGRID = AppModuleRoutes.BIREPORTS_PORTFOLIO + BIReportsRoutesConstant.ICABSAUNALLOCATEDUNITSGRID;

    // Invoice Module
    public static readonly NEXTINVOICERUNFORECASTDETAIL = AppModuleRoutes.BIREPORTS_INVOICE + BIReportsRoutesConstant.NEXTINVOICERUNFORECASTDETAIL;
    public static readonly ICABSARINVOICEANALYSISGRID = AppModuleRoutes.BIREPORTS_INVOICE + BIReportsRoutesConstant.ICABSARINVOICEANALYSISGRID;
    public static readonly ICABSARINVOICEANALYSISDETAILGRID = AppModuleRoutes.BIREPORTS_INVOICE + BIReportsRoutesConstant.ICABSARINVOICEANALYSISDETAILGRID;
    public static readonly ICABSARINVOICEANALYSISEMPLOYEEGRID = AppModuleRoutes.BIREPORTS_INVOICE + BIReportsRoutesConstant.ICABSARINVOICEANALYSISEMPLOYEEGRID;
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTSTANDINGBRANCH = AppModuleRoutes.BIREPORTS_INVOICE + BIReportsRoutesConstant.ICABSARLOSTBUSINESSREQUESTSOUTSTANDINGBRANCH;
    public static readonly ICABSARNEXTINVOICERUNFORECASTBUSINESSGRID = AppModuleRoutes.BIREPORTS_INVOICE + BIReportsRoutesConstant.ICABSARNEXTINVOICERUNFORECASTBUSINESSGRID;
    public static readonly RIBATCHCOMPONENT = AppModuleRoutes.BIREPORTS_INVOICE + BIReportsRoutesConstant.RIBATCHCOMPONENT;

    // Service Module
    public static readonly ICABSSESERVICEDAILYPRODUCTIVITYGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSESERVICEDAILYPRODUCTIVITYGRID;
    public static readonly ICABSSESERVICEDAILYPRODUCTIVITYGRIDFR = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSESERVICEDAILYPRODUCTIVITYGRIDFR;
    public static readonly ICABSSEDAILYPRODUCTIVITYBUSINESSGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEDAILYPRODUCTIVITYBUSINESSGRID;
    public static readonly ICABSSEDAILYPRODUCTIVITYBRANCHGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEDAILYPRODUCTIVITYBRANCHGRID;
    public static readonly ICABSSEDAILYPRODUCTIVITYSUMMARYGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEDAILYPRODUCTIVITYSUMMARYGRID;
    public static readonly ICABSTECHNICIANSERVICEVISITGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSTECHNICIANSERVICEVISITGRID;
    public static readonly ICABSSESTATEOFSERVICEREPORTSGRIDBYBUSINESS = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSESTATEOFSERVICEREPORTSGRIDBYBUSINESS;
    public static readonly ICABSSESTATEOFSERVICEREPORTSGRIDBYREGION = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSESTATEOFSERVICEREPORTSGRIDBYREGION;
    public static readonly ICABSSESTATEOFSERVICEREPORTSGRIDBYBRANCH = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSESTATEOFSERVICEREPORTSGRIDBYBRANCH;
    public static readonly ICABSSESTATEOFSERVICEREPORTSGRIDAGEDBYBRANCH = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSESTATEOFSERVICEREPORTSGRIDAGEDBYBRANCH;
    public static readonly ICABSSEBATCHSOSAGEDPROFILEBRANCHGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEBATCHSOSAGEDPROFILEBRANCHGRID;
    public static readonly ICABSSEBATCHSOSAGEDPROFILEBUSINESSGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEBATCHSOSAGEDPROFILEBUSINESSGRID;
    public static readonly ICABSSEBATCHSTATEOFSERVICEBUSINESSGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEBATCHSTATEOFSERVICEBUSINESSGRID;
    public static readonly ICABSSEBATCHSTATEOFSERVICEBRANCHGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEBATCHSTATEOFSERVICEBRANCHGRID;
    public static readonly ICABSSEBATCHSTATEOFSERVICEREGIONGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEBATCHSTATEOFSERVICEREGIONGRID;
    public static readonly ICABSSESTATEOFSERVICEGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSESTATEOFSERVICEGRID;
    public static readonly ICABSSEPRODUCTIVITYMULTIPESGRIDBYBUSINESS = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYMULTIPESGRIDBYBUSINESS;
    public static readonly ICABSSEPRODUCTIVITYMULTIPESGRIDBYREGION = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYMULTIPESGRIDBYREGION;
    public static readonly ICABSSEPRODUCTIVITYMULTIPESGRIDBYBRANCH = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYMULTIPESGRIDBYBRANCH;
    public static readonly ICABSSEPRODUCTIVITYBUSINESSPESGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYBUSINESSPESGRID;
    public static readonly ICABSSSALESSTATISTICSDETAIL = AppModuleRoutes.BIREPORTS_SALES + BIReportsRoutesConstant.ICABSSSALESSTATISTICSDETAIL;
    public static readonly ICABSSEPRODUCTIVITYBRANCHPESGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYBRANCHPESGRID;
    public static readonly ICABSSEPRODUCTIVITYPESDETAIL = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYPESDETAIL;
    public static readonly ICABSSEPRODUCTIVITYBUSINESSGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYBUSINESSGRID;
    public static readonly ICABSSEPRODUCTIVITYREGIONGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYREGIONGRID;
    public static readonly ICABSSEPRODUCTIVITYBRANCHGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYBRANCHGRID;
    public static readonly ICABSSEPRODUCTIVITYGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYGRID;
    public static readonly ICABSSEPRODUCTIVITYREGIONPESGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYREGIONPESGRID;
    public static readonly ICABSSEPRODUCTIVITYSERVICEVISITSPESGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEPRODUCTIVITYSERVICEVISITSPESGRID;
    public static readonly ICABSARSTATEOFSERVICEDETAILREPORT = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSARSTATEOFSERVICEDETAILREPORT;
    public static readonly ICABSSEAGEDARREARSREPORTGRIDBUSINESS = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEAGEDARREARSREPORTGRIDBYBUSINESS;
    public static readonly ICABSSEAGEDARREARSREPORTGRIDBRANCH = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEAGEDARREARSREPORTGRIDBYBRANCH;
    public static readonly ICABSSEBATCHSOSAGEDARREARSBUSINESSGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEBATCHSOSAGEDARREARSBUSINESSGRID;
    public static readonly ICABSSEBATCHSOSAGEDARREARSBRANCHGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEBATCHSOSAGEDARREARSBRANCHGRID;
    public static readonly ICABSSEBATCHSOSAGEDARREARSDETAILGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEBATCHSOSAGEDARREARSDETAILGRID;
    public static readonly ICABSARADDITIONALVISITREPORTGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSARADDITIONALVISITREPORTGRID;
    public static readonly ICABSARADDITIONALVISITREPORTDETAILSGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSARADDITIONALVISITREPORTDETAILSGRID;
    public static readonly ICABSSESTATEOFSERVICENATACCOUNTGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSESTATEOFSERVICENATACCOUNTGRID;
    public static readonly ICABSARCUSTOMERCCOREPORTGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSARCUSTOMERCCOREPORTGRID;
    public static readonly ICABSSEINFESTATIONGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEINFESTATIONGRID;
    public static readonly ICABSARSERVICEANDINVOICESUSPENDBUSINESS = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSARSERVICEANDINVOICESUSPENDBUSINESS;
    public static readonly ICABSARSERVICEANDINVOICESUSPENDBRANCH = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSARSERVICEANDINVOICESUSPENDBRANCH;
    public static readonly ICABSARSERVICESUSPENDHISTORY = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSARSERVICESUSPENDHISTORY;
    public static readonly ICABSARSERVICEANDINVOICESUSPENDREGION = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSARSERVICEANDINVOICESUSPENDREGION;
    public static readonly ICABSBULKPROOFOFSERVICE = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSBULKPROOFOFSERVICE;
    public static readonly ICABSSEOUTSTANDINGINSTALLATIONSGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEOUTSTANDINGINSTALLATIONSGRID;
    public static readonly ICABSSEOUTSTANDINGBUSINESSINSTALLATIONSGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEOUTSTANDINGBUSINESSINSTALLATIONSGRID;
    public static readonly ICABSSEOUTSTANDINGREMOVALSGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEOUTSTANDINGREMOVALSGRID;
    public static readonly ICABSSEOUTSTANDINGBUSINESSREMOVALSGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEOUTSTANDINGBUSINESSREMOVALSGRID;
    public static readonly ICABSARIWSSTOCKREQUIREMTS = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSARIWSSTOCKREQUIREMTS;
    public static readonly ICABSSEINFESTATIONSERVICECOVERGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEINFESTATIONSERVICECOVERGRID;
    public static readonly ICABSSEVISITTYPEGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSEVISITTYPEGRID;
    public static readonly ICABSSESTATICVISITGRIDYEARBRANCH = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSESTATICVISITGRIDYEARBRANCH;
    public static readonly ICABSSECOMPONENTORDERINGLISTGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSECOMPONENTORDERINGLISTGRID;
    public static readonly ICABSSECOMPONENTREPLACEMENTDETAILGRID = AppModuleRoutes.BIREPORTS_SERVICE + BIReportsRoutesConstant.ICABSSECOMPONENTREPLACEMENTDETAILGRID;

    // Turnover Module
    public static readonly ICABSARTURNOVERBUSINESS = AppModuleRoutes.BIREPORTS_TURNOVER + BIReportsRoutesConstant.ICABSARTURNOVERBUSINESS;
    public static readonly ICABSARTURNOVERBUSINESSMARKET = AppModuleRoutes.BIREPORTS_TURNOVER + BIReportsRoutesConstant.ICABSARTURNOVERBUSINESSMARKET;
    public static readonly ICABSARTURNOVERBUSINESSSERVICE = AppModuleRoutes.BIREPORTS_TURNOVER + BIReportsRoutesConstant.ICABSARTURNOVERBUSINESSSERVICE;
    public static readonly ICABSARTURNOVERBRANCH = AppModuleRoutes.BIREPORTS_TURNOVER + BIReportsRoutesConstant.ICABSARTURNOVERBRANCH;
    public static readonly ICABSARTURNOVERBRANCHMARKET = AppModuleRoutes.BIREPORTS_TURNOVER + BIReportsRoutesConstant.ICABSARTURNOVERBRANCHMARKET;
    public static readonly ICABSARTURNOVERBRANCHSERVICE = AppModuleRoutes.BIREPORTS_TURNOVER + BIReportsRoutesConstant.ICABSARTURNOVERBRANCHSERVICE;
    public static readonly ICABSARBRANCHTURNOVERDETAILGRID = AppModuleRoutes.BIREPORTS_TURNOVER + BIReportsRoutesConstant.ICABSARBRANCHTURNOVERDETAILGRID;
    public static readonly ICABSADEFERREDTURNOVERBUSINESSGRID = AppModuleRoutes.BIREPORTS_TURNOVER + BIReportsRoutesConstant.ICABSADEFERREDTURNOVERBUSINESSGRID;
    public static readonly ICABSADEFERREDTURNOVERBRANCHGRID = AppModuleRoutes.BIREPORTS_TURNOVER + BIReportsRoutesConstant.ICABSADEFERREDTURNOVERBRANCHGRID;
    public static readonly ICABSADEFERREDTURNOVERDETAILGRID = AppModuleRoutes.BIREPORTS_TURNOVER + BIReportsRoutesConstant.ICABSADEFERREDTURNOVERDETAILGRID;

    //StockUsage Module
    public static readonly ICABSSESTOCKUSAGEESTIMATESBRANCH = AppModuleRoutes.BIREPORTS_STOCKUSAGE + BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESBRANCH;
    public static readonly ICABSSESTOCKUSAGEESTIMATESDETAIL = AppModuleRoutes.BIREPORTS_STOCKUSAGE + BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESDETAIL;
    public static readonly ICABSSESTOCKUSAGEESTIMATESBUSINESS = AppModuleRoutes.BIREPORTS_STOCKUSAGE + BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESBUSINESS;
    public static readonly ICABSSESTOCKUSAGEESTIMATESREGION = AppModuleRoutes.BIREPORTS_STOCKUSAGE + BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESREGION;
    public static readonly ICABSSESTOCKUSAGEESTIMATESPREMISES = AppModuleRoutes.BIREPORTS_STOCKUSAGE + BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESPREMISES;
    public static readonly ICABSSESTOCKUSAGEESTIMATESBRANCHDETAIL = AppModuleRoutes.BIREPORTS_STOCKUSAGE + BIReportsRoutesConstant.ICABSSESTOCKUSAGEESTIMATESBRANCHDETAIL;

    // Credit Analysis
    public static readonly ICABSARCREDITREASONANALYSIS = AppModuleRoutes.BIREPORTS_CREDITANALYSIS + BIReportsRoutesConstant.ICABSARCREDITREASONANALYSIS;
    public static readonly ICABSARCREDITREASONANALYSISBUSINESS = AppModuleRoutes.BIREPORTS_CREDITANALYSIS + BIReportsRoutesConstant.ICABSARCREDITREASONANALYSISBUSINESS;
    public static readonly ICABSARCREDITREASONANALYSISREGION = AppModuleRoutes.BIREPORTS_CREDITANALYSIS + BIReportsRoutesConstant.ICABSARCREDITREASONANALYSISREGION;
    public static readonly ICABSARCREDITREASONANALYSISDETAIL = AppModuleRoutes.BIREPORTS_CREDITANALYSIS + BIReportsRoutesConstant.ICABSARCREDITREASONANALYSISDETAIL;

    //ActualVsContractual Module
    public static readonly ICABSARACTUALVSCONTRACTUALREPORT = AppModuleRoutes.BIREPORTS_ACTUALVSCONTRACTUAL + BIReportsRoutesConstant.ICABSARACTUALVSCONTRACTUALREPORT;

    //Client Retention Module
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTCOMEBUSINESS = AppModuleRoutes.BIREPORTS_CLIENTRETENTION + BIReportsRoutesConstant.ICABSARLOSTBUSINESSREQUESTSOUTCOMEBUSINESS;
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTCOMEBRANCH = AppModuleRoutes.BIREPORTS_CLIENTRETENTION + BIReportsRoutesConstant.ICABSARLOSTBUSINESSREQUESTSOUTCOMEBRANCH;
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTCOMEREGION = AppModuleRoutes.BIREPORTS_CLIENTRETENTION + BIReportsRoutesConstant.ICABSARLOSTBUSINESSREQUESTSOUTCOMEREGION;
    public static readonly ICABSARLOSTBUSINESSREQUESTSOUTCOMEDETAIL = AppModuleRoutes.BIREPORTS_CLIENTRETENTION + BIReportsRoutesConstant.ICABSARLOSTBUSINESSREQUESTSOUTCOMEDETAIL;

    //ContactManagement Module
    public static readonly ICABSCMTICKETANALYSISGENERATION = AppModuleRoutes.BIREPORTS_CONTACTMANAGEMENT + BIReportsRoutesConstant.ICABSCMTICKETANALYSISGENERATION;
    public static readonly ICABSARCREDITREASONANALYSISDETAILDETAIL = AppModuleRoutes.BIREPORTS_CREDITANALYSIS + BIReportsRoutesConstant.ICABSARCREDITREASONANALYSISDETAILDETAIL;
    public static readonly ICABSCMROOTCAUSEANALYSISGRIDBUSINESS = AppModuleRoutes.BIREPORTS_CONTACTMANAGEMENT + BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISGRIDBUSINESS;
    public static readonly ICABSCMROOTCAUSEANALYSISGRIDREGION = AppModuleRoutes.BIREPORTS_CONTACTMANAGEMENT + BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISGRIDREGION;
    public static readonly ICABSCMROOTCAUSEANALYSISGRIDBRANCH = AppModuleRoutes.BIREPORTS_CONTACTMANAGEMENT + BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISGRIDBRANCH;
    public static readonly ICABSCMROOTCAUSEANALYSISDETAILGRID = AppModuleRoutes.BIREPORTS_CONTACTMANAGEMENT + BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISDETAILGRID;
    public static readonly ICABSCMROOTCAUSEANALYSISBUSINESSGRID = AppModuleRoutes.BIREPORTS_CONTACTMANAGEMENT + BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISBUSINESSGRID;
    public static readonly ICABSCMROOTCAUSEANALYSISREGIONGRID = AppModuleRoutes.BIREPORTS_CONTACTMANAGEMENT + BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISREGIONGRID;
    public static readonly ICABSCMROOTCAUSEANALYSISBRANCHGRID = AppModuleRoutes.BIREPORTS_CONTACTMANAGEMENT + BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISBRANCHGRID;
    public static readonly ICABSCMROOTCAUSEANALYSISBRANCHGRIDREGION = AppModuleRoutes.BIREPORTS_CONTACTMANAGEMENT + BIReportsRoutesConstant.ICABSCMROOTCAUSEANALYSISBRANCHGRIDREGION;



    //APi Module
    public static readonly iCABSACONTACTSAPIEXEMPTGRID = AppModuleRoutes.BIREPORTS_API + BIReportsRoutesConstant.iCABSACONTACTSAPIEXEMPTGRID;
    public static readonly ICABSARTAXCODEREPORT = AppModuleRoutes.BIREPORTS_INVOICE + BIReportsRoutesConstant.ICABSARTAXCODEREPORT;
    public static readonly ICABSARTAXCODESUMMARY = AppModuleRoutes.BIREPORTS_INVOICE + BIReportsRoutesConstant.ICABSARTAXCODESUMMARY;
    public static readonly ICABSARTAXCODESUMMARYDETAIL = AppModuleRoutes.BIREPORTS_INVOICE + BIReportsRoutesConstant.ICABSARTAXCODESUMMARYDETAIL;
}

export class CCMMobileModuleConstants {
    public static readonly ICABSARCALLCENTREREVIEW = 'centrereview';
}

export class CCMMobileModuleRoutes {
    public static readonly ICABSARCALLCENTREREVIEW = AppModuleRoutes.CCMMOBILE + CCMMobileModuleConstants.ICABSARCALLCENTREREVIEW;
}

export class VehicleModuleConstants {
    public static readonly ICABSBVEHICLECOMPONENTSMAINTENANCE = 'vehiclecomponentsmaintenance';
    public static readonly ICABSBVEHICLETYPEMAINTENANCE = 'vehicletype';
    public static readonly ICABSBVEHICLEMAINTENANCE = 'vehiclemaintenance';
    public static readonly ICABSBDEPOTMAINTENANCE = 'depotmaintenance';
    public static readonly ICABSBVEHICLEMANUFACTURERMODELMAINTENANCE = 'vehiclemanufacturermodelmaintenance';
    public static readonly ICABSBVEHICLECLASSMAINTENANCE = 'vehicleclass';
}

export class RiBatchModuleConstants {
    public static readonly ICABSRIBATCHBRANCHFUNCTION = 'branchfunction';
    public static readonly ICABSRIBATCHFINANCIALACCOUNTS = 'financialaccounts';
    public static readonly ICABSRIBATCHSTOCKCONTROL = 'stockcontrol';
    public static readonly ICABSRIBATCHDISPLAYANDLOCATIONS = 'displaysandlocations';
}

export class VehicleModuleRoutes {
    public static readonly ICABSBVEHICLECOMPONENTSMAINTENANCE = AppModuleRoutes.VEHICLE_MANAGEMENT + VehicleModuleConstants.ICABSBVEHICLECOMPONENTSMAINTENANCE;
    public static readonly ICABSBVEHICLETYPEMAINTENANCE = AppModuleRoutes.VEHICLE_MANAGEMENT + VehicleModuleConstants.ICABSBVEHICLETYPEMAINTENANCE;
    public static readonly ICABSBVEHICLEMAINTENANCE = AppModuleRoutes.VEHICLE_MANAGEMENT + VehicleModuleConstants.ICABSBVEHICLEMAINTENANCE;
    public static readonly ICABSBDEPOTMAINTENANCE = AppModuleRoutes.VEHICLE_MANAGEMENT + VehicleModuleConstants.ICABSBDEPOTMAINTENANCE;
    public static readonly ICABSBVEHICLEMANUFACTURERMODELMAINTENANCE = AppModuleRoutes.VEHICLE_MANAGEMENT + VehicleModuleConstants.ICABSBVEHICLEMANUFACTURERMODELMAINTENANCE;
    public static readonly ICABSBVEHICLECLASSMAINTENANCE = AppModuleRoutes.VEHICLE_MANAGEMENT + VehicleModuleConstants.ICABSBVEHICLECLASSMAINTENANCE;
}

export class RiBatchModuleRoutes {
    public static readonly ICABSRIBATCHBRANCHFUNCTION = RiBatchModuleConstants.ICABSRIBATCHBRANCHFUNCTION;
    public static readonly ICABSRIBATCHFINANCIALACCOUNTS = RiBatchModuleConstants.ICABSRIBATCHFINANCIALACCOUNTS;
    public static readonly ICABSRIBATCHSTOCKCONTROL = RiBatchModuleConstants.ICABSRIBATCHSTOCKCONTROL;
    public static readonly ICABSRIBATCHDISPLAYANDLOCATIONS = RiBatchModuleConstants.ICABSRIBATCHDISPLAYANDLOCATIONS;
}
