import { QueryParams } from '@shared/services/http-params-wrapper';
import { LightBaseComponent, IGridHandlers } from './BaseComponentLight';
import { IXHRParams } from './XhrParams';

export class CommonGridFunction implements IGridHandlers {

    private lightComponent: LightBaseComponent;
    constructor(private newClass: any) {
        this.lightComponent = (newClass) as LightBaseComponent;
    }

    public onRefreshClick(callbackFun?: any): void {
        this.lightComponent.pageParams.gridCacheRefresh = true;
        this.onRiGridRefresh(callbackFun);
    }

    public onRiGridRefresh(callbackFun?: any): void {
        if (callbackFun)
            callbackFun();
        this.newClass['populateGrid']();
    }

    public getCurrentPage(currentPage: any): void {
        if (!this.newClass['riGrid']['RefreshRequiredStatus']()) {
            this.lightComponent.pageParams.gridCacheRefresh = false;
            this.lightComponent.getCurrentPage(currentPage, true);
            this.onRiGridRefresh();
        }
    }

    public setPageData(data: any, config: Record<string, any> = null): void {
        this.lightComponent.pageParams.gridConfig = config || {
            itemsPerPage: 10,
            totalItem: 1
        };
        if (this.lightComponent.hasError(data)) {
            this.lightComponent.pageParams.hasGridData = false;
            this.lightComponent.pageParams.gridConfig.totalItem = 1;
            this.newClass.displayMessage(data);
        } else {
            this.lightComponent.pageParams.hasGridData = true;
            this.newClass.riGrid.RefreshRequired();
            this.lightComponent.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
            this.lightComponent.pageParams.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.lightComponent.pageParams.gridConfig.itemsPerPage : 1;
            this.newClass.riGrid.Execute(data);
            setTimeout(() => {
                this.newClass.riGridPagination.setPage(this.lightComponent.pageParams.gridCurrentPage);
            }, 100);
        }
    }

    public resetGrid(): void {
        this.lightComponent.pageParams.gridConfig.totalItem = 0;
        this.lightComponent.pageParams.gridCurrentPage = 1;
        this.lightComponent.pageParams.hasGridData = false;
        this.newClass['riGrid']['Clear']();
        this.newClass['riGrid']['RefreshRequired']();
    }

    public fetchGridData(xhrParams: IXHRParams, search: QueryParams, formData: any): void {
        this.lightComponent['ajaxSource'].next(this.lightComponent['ajaxconstant'].START);

        this.lightComponent['httpService'].xhrPost(xhrParams.method, xhrParams.module, xhrParams.operation, search, formData).then(data => {
            this.lightComponent['ajaxSource'].next(this.lightComponent['ajaxconstant'].COMPLETE);
            this.setPageData(data, this.lightComponent.pageParams.gridConfig);
        }).catch(error => {
            this.lightComponent['ajaxSource'].next(this.lightComponent['ajaxconstant'].COMPLETE);
            this.lightComponent.pageParams.hasGridData = false;
            this.lightComponent['displayMessage'](error);
        });
    }
}
