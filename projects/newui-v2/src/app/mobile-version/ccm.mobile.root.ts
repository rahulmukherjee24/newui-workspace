import { Component, OnInit, OnDestroy, ViewChild, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '@shared/services/auth.service';
import { RiExchange } from '@shared/services/riExchange';

@Component({
    template: `
        <router-outlet></router-outlet>
        <icabs-modal #errorModal="child" [(showHeader)]="showErrorHeader" [config]="{backdrop: 'static'}"></icabs-modal>
    `,
    styleUrls: ['./styles/small-device.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CCMRootMobileComponent implements OnInit, OnDestroy {
    @ViewChild('errorModal') public errorModal;

    public authJson: any;
    public componentInteractionSubscription: Subscription;
    public displayNavBar: boolean;
    public errorSubscription: Subscription;
    public muleJson: any;
    public routerSubscription: Subscription;
    public showErrorHeader: boolean = true;

    constructor(private riExchange: RiExchange, private authService: AuthService, private ls: LocalStorageService) {
    }

    ngOnInit(): void {
        this.displayNavBar = true;
        if (!this.authService.oauthResponse) {
            this.authJson = this.ls.retrieve('OAUTH');
        } else {
            this.authJson = this.authService.oauthResponse;
        }
    }

    ngOnDestroy(): void {
        if (this.componentInteractionSubscription) { this.componentInteractionSubscription.unsubscribe(); }
        if (this.errorSubscription) { this.errorSubscription.unsubscribe(); }
        if (this.routerSubscription) { this.routerSubscription.unsubscribe(); }
        this.riExchange.releaseReference(this);
    }
}
