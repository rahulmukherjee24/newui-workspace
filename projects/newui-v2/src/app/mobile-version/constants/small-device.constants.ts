export class SmallDeviceConstants {
    public static readonly PageMsgList: any = {
        'allLoaded': 'All Records Shown....',
        'error': 'Error while loading, Please try again...',
        'noRecord': ' No Record Available...',
        'onLoading': 'Loading, Please wait...'
    };
    public static readonly ccmPageTitle: string = 'My Contact Centre Review';
}
