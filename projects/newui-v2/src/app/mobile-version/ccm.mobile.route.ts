import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CCMMobileModuleConstants } from '@base/PageRoutes';
import { CCMRootMobileComponent } from './ccm.mobile.root';
import { CentreReviewGridMobileComponent } from './ccm/iCABSCMCallCenterReviewGridMobile.component';

const routes: Routes = [
    {
        path: '', component: CCMRootMobileComponent, children: [
            { path: CCMMobileModuleConstants.ICABSARCALLCENTREREVIEW, component: CentreReviewGridMobileComponent }
        ], data: { domain: 'CUSTOMER CONTACT MANAGEMENT - MOBILE' }
    }
];

export const CCMRouteDefinitionsMobile: ModuleWithProviders = RouterModule.forChild(routes);
