import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit, Injector, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

import { AuthService } from '@shared/services/auth.service';
import { GlobalizeService } from '@shared/services/globalize.service';
import { GridToSlideComponent } from '@shared/components/gridToSlide/gridToSlide.component';
import { HttpService } from '@shared/services/http-service';
import { LookUp } from '@shared/services/lookup';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ServiceConstants } from '@shared/constants/service.constants';
import { SmallDeviceConstants } from '../constants/small-device.constants';
import { Utils } from '@shared/services/utility';

@Component({
    templateUrl: 'iCABSCMCallCentreReviewGridMobile.html',
    encapsulation: ViewEncapsulation.None
})
export class CentreReviewGridMobileComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('gridToSlide') gridToSlide: GridToSlideComponent;

    private Date: Date = new Date();
    private httpSubscription: any;
    private lastPageNumber: any = 1;
    private pageMsgObj: any;
    private screen_body: any;
    private serviceConstants: ServiceConstants;
    private xhrParams: object = {
        method: 'ccm/maintenance',
        module: 'call-centre',
        operation: 'ContactManagement/iCABSCMCallCentreReviewGrid'
    };

    public businessCode: string;
    public currentPage: any = 0;
    public columnsToHide: string[] = ['log', '(i)', 'employee', 'actionby', 'act'];
    public countryCode: string;
    public customAlertMsg: any;
    public messageType: string;
    public DateFilter: any;
    public DateFilterList: any = [{ text: 'Action By Date', value: 'actionby' }, { text: 'Created date', value: 'created' }];
    public fromDate: any;
    public fromDateDisplay: string;
    public isRequesting: boolean = false;
    public logNumber: string;
    public ticketNumber: string;
    public query: QueryParams = new QueryParams();
    public pageMsg: string;
    public pageTitle: string;
    public showFilters: boolean = false;
    public slides: Array<any> = [];
    public toDate: any;
    public toDateDisplay: string;
    public userCode: string;
    public userName: string;

    constructor(
        private authService: AuthService,
        private globalizeService: GlobalizeService,
        private httpService: HttpService,
        private injector: Injector,
        private localStorage: LocalStorageService,
        private LookUp: LookUp,
        private router: Router,
        private utils: Utils
    ) {
        this.serviceConstants = injector.get(ServiceConstants);
        this.pageMsgObj = SmallDeviceConstants.PageMsgList;
        this.pageTitle = SmallDeviceConstants.ccmPageTitle;
        this.DateFilter = this.DateFilterList[0].value;
        this.businessCode = this.utils.getBusinessCode();
        this.userCode = this.utils.getUserCode();
        this.countryCode = this.utils.getCountryCode();
        this.userName = this.authService.displayName || this.localStorage.retrieve('DISPLAYNAME');
    }

    ngOnInit(): void {
        this.pageMsg = this.pageMsgObj['onLoading'];
        this.doLookup();
    }

    ngOnDestroy(): void {
        if (this.httpSubscription) { this.httpSubscription.unsubscribe(); }
    }

    private doLookup(): any {
        const businessDropdown: string = this.utils.getBusinessCode();

        const lookupForDates = [{
            'table': 'riRegistry',
            'query': { 'RegSection': 'Contact Centre Review', 'RegKey': businessDropdown + '_FromDate (NumberOfMonthsInThePast)' },
            'fields': ['RegValue']
        },
        {
            'table': 'riRegistry',
            'query': { 'RegSection': 'Contact Centre Review', 'RegKey': businessDropdown + '_ToDate (NumberOfMonthsInTheFuture)' },
            'fields': ['RegValue']
        }
        ];

        this.LookUp.lookUpRecord(lookupForDates).subscribe((data) => {
            const dataFromDate = data[0] || undefined;
            const dataToDate = data[1] || undefined;

            if (!(dataFromDate && dataFromDate.length && dataToDate && dataToDate.length)) {
                this.pageMsg = this.pageMsgObj['error'];
                this.customAlertMsg = {
                    msg: 'Error while loading....',
                    timestamp: (new Date()).getMilliseconds()
                };
                return;
            }

            let vMonthsAgo: any, vMonthsAhead: any;
            const newDate = new Date();

            vMonthsAgo = dataFromDate[0].RegValue ? (dataFromDate[0].RegValue * 1) : 3;
            this.fromDate = new Date(new Date().setMonth(new Date().getMonth() - vMonthsAgo));
            this.fromDate = this.globalizeService.parseDateToFixedFormat(this.fromDate);

            vMonthsAhead = dataToDate[0].RegValue ? (dataToDate[0].RegValue * 1) : 1;
            this.toDate = newDate;
            const monthEnd: number = this.toDate.getMonth() + vMonthsAhead;
            this.toDate.setMonth(monthEnd);
            this.toDate = this.globalizeService.parseDateToFixedFormat(this.toDate);
            if (this.fromDate && this.toDate) { this.onRefreshData(); }
        },
            (error) => {
                this.pageMsg = this.pageMsgObj['error'];
                this.customAlertMsg = {
                    msg: error.fullError,
                    timestamp: (new Date()).getMilliseconds()
                };
            });

    }

    private scroll = (e: any): void => {
        if (this.isRequesting) { return; }
        const maxScroll: any = e.srcElement.scrollHeight - e.srcElement.clientHeight;
        const currentScroll: any = e.srcElement.scrollTop;
        const pCent: any = (currentScroll / maxScroll) * 100;
        if ((Math.ceil(pCent) > 85)) { this.onRefreshData(); }
    }

    public ngAfterContentInit(): void {
        this.screen_body = document.querySelector('.screen-body');
        this.screen_body.addEventListener('scroll', this.scroll, true);
    }

    public updateDatePickerValue(e: any, valueFor?: string): void {
        (valueFor && valueFor === 'from') ? this.fromDate = e.value : this.toDate = e.value;
    }

    public onActionChange(e: any): void {
        this.DateFilter = e.srcElement.value;
    }

    public onRefreshData(val?: string): void {
        if (val && val === 'fromfilter') { this.currentPage = 0; this.slides = []; this.lastPageNumber = 1; }
        if (this.currentPage >= this.lastPageNumber) { this.pageMsg = this.pageMsgObj['allLoaded']; return; }
        this.currentPage++;
        this.pageMsg = this.pageMsgObj['onLoading'];

        this.query.set(this.serviceConstants.BusinessCode, this.businessCode);
        this.query.set(this.serviceConstants.CountryCode, this.countryCode);
        this.query.set('action', '2');
        this.query.set('BranchNumber', this.utils.getBranchCode());
        this.query.set('CallLogID', this.logNumber || '');
        this.query.set('CustomerContactNumber', this.ticketNumber);
        this.query.set('DateFilter', this.DateFilter);
        this.query.set('DateFrom', this.fromDate);
        this.query.set('DateTo', this.toDate);
        this.query.set('Filter', 'allOpen');
        this.query.set('FilterBranchNumber', this.utils.getBranchCode());
        this.query.set('FilterDisputed', 'all');
        this.query.set('FilterLevel', 'currentactioner');
        this.query.set('FilterPassed', 'all');
        this.query.set('FilterValue', 'all');
        this.query.set('gridSortOrder', 'Ascending');
        this.query.set('MyContactType', 'all');
        this.query.set('PageCurrent', this.currentPage);
        this.query.set('PageSize', '3');
        this.query.set('ReassignSelect', 'all');
        this.query.set('riGridHandle', this.utils.gridHandle);

        this.isRequesting = true;
        this.httpSubscription = this.httpService.makeGetRequest(this.xhrParams['method'], this.xhrParams['module'], this.xhrParams['operation'], this.query)
            .subscribe(
                (data) => {
                    this.isRequesting = false;
                    if (!data || data.hasError) {
                        this.lastPageNumber = 0;
                        this.pageMsg = this.pageMsgObj['error'];
                        this.slides = [];
                        return;
                    }

                    if (data.body.cells && !data.body.cells.length) {
                        this.lastPageNumber = 0;
                        this.pageMsg = this.pageMsgObj['noRecord'];
                        this.slides = [];
                        this.gridToSlide.loadSlides(data);
                        return;
                    }
                    (val && val === 'fromfilter') ? this.gridToSlide.loadSlides(data, true) : this.gridToSlide.loadSlides(data);
                    this.pageMsg = '';
                    this.lastPageNumber = data.pageData.lastPageNumber;

                },
                (error) => {
                    this.isRequesting = false;
                    this.lastPageNumber = 0;
                    this.pageMsg = this.pageMsgObj['error'];
                    this.slides = [];
                    this.customAlertMsg = {
                        msg: error.fullError,
                        timestamp: (new Date()).getMilliseconds()
                    };
                });
    }

    public navigateToTicketMaintenance(event: any): void {
        this.router.navigate(['/application/contactmanagement/customerContactMaintenance'], { queryParams: { parentMode: 'CallCentreReview', CustomerContactROWID: event.slide['rowId'] } });
    }

    public signOut(e: any): void {
        this.authService.signOut();
        setTimeout(() => {
            window.location.reload();
        }, 500);
    }

}
