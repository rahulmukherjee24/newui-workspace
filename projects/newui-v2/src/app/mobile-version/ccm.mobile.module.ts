
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { CCMRootMobileComponent } from './ccm.mobile.root';
import { CCMRouteDefinitionsMobile } from './ccm.mobile.route';
import { SharedModule } from '@shared/shared.module';
import { SmallDeviceConstants } from './constants/small-device.constants';
import { SmallDeviceUtils } from './small-device-utils';

import { CentreReviewGridMobileComponent } from './ccm/iCABSCMCallCenterReviewGridMobile.component';
// import { MobileHeaderComponent } from './shared/components/mobile-header/mobile-header.component';

@NgModule({
    imports: [
        CCMRouteDefinitionsMobile,
        HttpClientModule,
        SharedModule
    ],
    providers: [
        SmallDeviceConstants,
        SmallDeviceUtils
    ],
    declarations: [
        CCMRootMobileComponent,
        CentreReviewGridMobileComponent
        // MobileHeaderComponent
    ],
    entryComponents: []
})

export class CCMMobileModule { }
