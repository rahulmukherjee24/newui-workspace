
import { Component, OnInit, Injector, ViewChild, OnDestroy, EventEmitter, AfterContentInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { BaseComponent } from '@base/BaseComponent';
import { ContactMediumLanguageSearchComponent } from '@internal/search/iCABSSContactMediumLanguageSearch.component';
import { EllipsisComponent } from '@shared/components/ellipsis/ellipsis';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from '@shared/constants/message.constant';
import { PageIdentifier } from '@base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { ScreenNotReadyComponent } from '@shared/components/screenNotReady';
import { EllipsisGenericComponent, GenericEllipsisEvent } from '@shared/components/ellipsis-generic/ellipsis-generic';
import { IXHRParams } from '@app/base/XhrParams';

@Component({
    templateUrl: 'iCABSBBusinessOriginMaintenance.html'
})

export class BusinessOriginMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('businessOriginCodeEllipsis') public businessOriginCodeEllipsis: EllipsisGenericComponent;
    @ViewChild('businessSourceCodeEllipsis') public businessSourceCodeEllipsis: EllipsisComponent;
    @ViewChild('contactMediumCodeEllipsis') public contactMediumCodeEllipsis: EllipsisComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    public controls: Array<any> = [
        { name: 'ActiveInd', type: MntConst.eTypeCheckBox },
        { name: 'BusinessCode' },
        { name: 'BusinessOriginCode', required: true, type: MntConst.eTypeCode, commonValidator: true },
        { name: 'BusinessOriginSystemDesc', required: true, type: MntConst.eTypeText, commonValidator: true },
        { name: 'BusinessSourceCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'BusinessSourceDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ContactMediumCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ContactMediumDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'DetailRequiredInd', type: MntConst.eTypeCheckBox },
        { name: 'LeadInd', type: MntConst.eTypeCheckBox },
        { name: 'NewBusinessInd', type: MntConst.eTypeCheckBox },
        { name: 'ROWID' },
        { name: 'TransferCodeInd', type: MntConst.eTypeCheckBox },
        { name: 'ValidForContract', type: MntConst.eTypeCheckBox },
        { name: 'ValidForJob', type: MntConst.eTypeCheckBox },
        { name: 'ValidForProductSale', type: MntConst.eTypeCheckBox },
        { name: 'ValidForReneg', type: MntConst.eTypeCheckBox }
    ];
    public ellipsis: any = {
        businessOriginCode: {
            autoOpen: false,
            ellipsisTitle: 'Business Origin Search',
            configParams: {
                table: '',
                shouldShowAdd: true,
                parentMode: 'Search',
                extraParams: {
                    'LanguageCode': this.riExchange.LanguageCode(),
                    'search.sortby': 'BusinessOriginCode'
                }
            },
            tableColumn: [
                { 'title': 'Code', 'name': 'BusinessOriginCode', type: MntConst.eTypeCode, size: 2 },
                { 'title': 'Description', 'name': 'BusinessOriginSystemDesc', type: MntConst.eTypeText, required: 'Required', size: 40 }
            ],
            disable: false,
            httpConfig: {
                operation: 'Business/iCABSBBusinessOriginSearch',
                module: 'segmentation',
                method: 'prospect-to-contract/search'
            }
        },
        businessSourceCode: {
            autoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: ScreenNotReadyComponent, // Component of iCABSBBusinessSourceLanguageSearch
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        },
        contactMediumCode: {
            autoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            contentComponent: ContactMediumLanguageSearchComponent,
            showHeader: true,
            disabled: false
        }
    };
    public httpSubscription: Subscription;
    public lookUpSubscription: Subscription;
    public queryParams: IXHRParams = {
        module: 'segmentation',
        method: 'prospect-to-contract/admin',
        operation: 'Business/iCABSBBusinessOriginMaintenance'
    };
    public pageId: string = '';
    public riMaintenanceCurrentMode: string = '';

    /* ========== Set Focus On =============== */
    public setFocusOnBusinessOriginCode = new EventEmitter<boolean>();
    public setFocusOnBusinessOriginSystemDesc = new EventEmitter<boolean>();

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBBUSINESSORIGINMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Business Origin Maintenance';
    }
    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            this.populateUIFromFormData();
        } else {
            this.windowOnload();
        }
    }

    ngAfterContentInit(): void {
        if (!this.getControlValue('BusinessOriginCode') && !this.isReturning()) this.businessOriginCodeEllipsis.openModal();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.lookUpSubscription) {
            this.lookUpSubscription.unsubscribe();
        }
        if (this.httpSubscription) {
            this.httpSubscription.unsubscribe();
        }
        this.routeAwayGlobals.resetRouteAwayFlags();
    }

    private windowOnload(): void {
        this.setControlValue('BusinessCode', this.businessCode());
        this.setFocusOnBusinessOriginCode.emit(true);

        this.riMaintenanceSelectMode();
        switch (this.riExchange.getParentMode()) {
            case 'BusinessOriginAdd':
                this.riMaintenanceAddMode();
                this.disableControl('BusinessOriginCode', false);
                break;
            case 'BusinessOriginUpdate':
                this.riMaintenanceUpdateMode();
                this.setControlValue('ROWID', this.riExchange.getParentAttributeValue('RowID') || '');
                this.fetchRecord();
                break;
        }
        if (this.getControlValue('BusinessOriginCode')) {
            this.fetchRecord();
        }
        this.riMaintenanceExecMode(this.getFormMode());
    }

    private fetchRecord(): void {
        this.riMaintenanceAddTableCommit();
        this.disableControl('BusinessSourceDesc', true);
        this.disableControl('ContactMediumDesc', true);
    }

    private setDefaultFormData(): void {
        this.pageParams.BusinessOriginCode = this.getControlValue('BusinessOriginCode');
    }

    private resetControl(): void {
        this.riExchange.resetCtrl(this.controls);
        this.riExchange.renderForm(this.uiForm, this.controls);
        this.markAsPrestine();
    }

    private markAsPrestine(): void {
        this.controls.forEach((i) => {
            this.uiForm.controls[i.name].markAsPristine();
            this.uiForm.controls[i.name].markAsUntouched();
        });
    }

    /* ------- riMiantenance Logic ------ */
    private riMaintenanceExecMode(mode: string): void {
        switch (mode) {
            case 'select':
                this.pageParams.isEnableSelect = true;
                this.pageParams.isEnableUpdate = false;
                this.disableControls(['BusinessOriginCode']);
                this.disableControl('BusinessOriginCode', false);
                this.ellipsis.businessSourceCode.disabled = true;
                this.ellipsis.contactMediumCode.disabled = true;
                this.markAsPrestine();
                break;
            case 'add':
                this.pageParams.isEnableSelect = false;
                this.pageParams.isEnableUpdate = false;
                this.resetControl();
                this.enableControls(['BusinessSourceDesc', 'ContactMediumDesc']);
                this.ellipsis.businessSourceCode.disabled = false;
                this.ellipsis.contactMediumCode.disabled = false;
                this.setDefaultFormData();
                this.setFocusOnBusinessOriginCode.emit(true);
                break;
            case 'update':
                this.pageParams.isEnableSelect = false;
                this.pageParams.isEnableUpdate = true;
                this.enableControls(['BusinessSourceDesc', 'ContactMediumDesc']);
                this.disableControl('BusinessOriginCode', true);
                this.ellipsis.businessSourceCode.disabled = false;
                this.ellipsis.contactMediumCode.disabled = false;
                this.markAsPrestine();
                break;
            case 'delete':
                this.pageParams.isEnableSelect = false;
                this.pageParams.isEnableUpdate = true;
                this.enableControls(['BusinessSourceDesc', 'ContactMediumDesc']);
                this.disableControl('BusinessOriginCode', true);
                this.ellipsis.businessSourceCode.disabled = false;
                this.ellipsis.contactMediumCode.disabled = false;
                break;
        }
    }

    private riMaintenanceExecContinue(mode: string): void {
        switch (mode) {
            case 'eModeSaveAdd':
                this.confirm();
                break;
            case 'eModeSaveUpdate':
                this.confirm();
                break;
            case 'eModeDelete':
                this.confirmDelete();
                break;
            case 'add':
                this.riMaintenanceAfterSave();
                break;
            case 'update':
                this.riMaintenanceAfterSave();
                break;
            case 'delete':
                this.riMaintenanceAfterDelete();
                break;
        }
    }

    private riMaintenanceSelectMode(): void {
        this.setFormMode(this.c_s_MODE_SELECT);
    }

    private riMaintenanceUpdateMode(): void {
        this.setFormMode(this.c_s_MODE_UPDATE);
    }

    private riMaintenanceAddMode(): void {
        this.setFormMode(this.c_s_MODE_ADD);
    }

    private riMaintenanceDeleteMode(): void {
        this.setFormMode(this.c_s_MODE_DELETE);
    }
    private riMaintenanceAddTableCommit(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        search.set('LanguageCode', this.riExchange.LanguageCode());
        search.set('BusinessOriginCode', this.getControlValue('BusinessOriginCode'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
            .subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        this.riMaintenanceSelectMode();
                        this.riMaintenanceExecMode(this.getFormMode());
                    } else {
                        this.setControlValue('ROWID', e.ttBusinessOrigin);
                        this.setControlValue('BusinessOriginCode', e.BusinessOriginCode);
                        this.pageParams.BusinessOriginCode = e.BusinessOriginCode;
                        this.setControlValue('BusinessSourceCode', e.BusinessSourceCode);
                        this.setControlValue('ContactMediumCode', e.ContactMediumCode);
                        this.setControlValue('BusinessOriginSystemDesc', e.BusinessOriginSystemDesc);
                        this.setControlValue('NewBusinessInd', e.NewBusinessInd);
                        this.setControlValue('ValidForReneg', e.ValidForReneg);
                        this.setControlValue('ValidForContract', e.ValidForContract);
                        this.setControlValue('ValidForJob', e.ValidForJob);
                        this.setControlValue('ValidForProductSale', e.ValidForProductSale);
                        this.setControlValue('LeadInd', e.LeadInd);
                        this.setControlValue('ActiveInd', e.ActiveInd);
                        this.setControlValue('DetailRequiredInd', e.DetailRequiredInd);
                        this.setControlValue('ContactMediumCode', e.ContactMediumCode);
                        this.setControlValue('TransferCodeInd', e.TransferCodeInd);
                        if (e.BusinessOriginCode) {
                            this.riMaintenanceUpdateMode();
                            this.riMaintenanceAddVirtualTableCommit();
                        } else {
                            this.riMaintenanceSelectMode();
                        }
                        this.riMaintenanceExecMode(this.getFormMode());
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.riMaintenanceSelectMode();
                    this.riMaintenanceExecMode(this.getFormMode());
                });
    }

    private riMaintenanceAddVirtualTableCommit(): void {
        this.businessSourceCodeOnkeydown();
        this.contactMediumCodeOnkeydown();
    }

    private riMaintenanceAfterSave(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.pageParams.actionAfterSave);

        let postDataAdd: Object = {};
        postDataAdd['BusinessOriginCode'] = this.getControlValue('BusinessOriginCode');
        postDataAdd['BusinessOriginSystemDesc'] = this.getControlValue('BusinessOriginSystemDesc');
        postDataAdd['BusinessSourceCode'] = this.getControlValue('BusinessSourceCode');
        postDataAdd['ContactMediumCode'] = this.getControlValue('ContactMediumCode');
        postDataAdd['NewBusinessInd'] = this.getControlValue('NewBusinessInd');
        postDataAdd['ValidForReneg'] = this.getControlValue('ValidForReneg');
        postDataAdd['ValidForContract'] = this.getControlValue('ValidForContract');
        postDataAdd['ValidForJob'] = this.getControlValue('ValidForJob');
        postDataAdd['ValidForProductSale'] = this.getControlValue('ValidForProductSale');
        postDataAdd['LeadInd'] = this.getControlValue('LeadInd');
        postDataAdd['ActiveInd'] = this.getControlValue('ActiveInd');
        postDataAdd['DetailRequiredInd'] = this.getControlValue('DetailRequiredInd');
        postDataAdd['TransferCodeInd'] = this.getControlValue('TransferCodeInd');
        if (this.getFormMode() === this.c_s_MODE_UPDATE) {
            postDataAdd['ROWID'] = this.getControlValue('ROWID');
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postDataAdd)
            .subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    } else {
                        this.markAsPrestine();
                        this.setControlValue('ROWID', e.ttBusinessOrigin);
                        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully);
                        modalVO.closeCallback = this.riMaintenanceAfterSaveAdd.bind(this);
                        this.modalAdvService.emitMessage(modalVO);
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private riMaintenanceAfterSaveAdd(): void {
        this.setDefaultFormData();
        this.riMaintenanceUpdateMode();
        this.riMaintenanceExecMode(this.getFormMode());
    }

    private riMaintenanceAfterDeleteSuccess(): void {
        this.resetControl();
        this.riMaintenanceSelectMode();
        this.riMaintenanceExecMode(this.getFormMode());
        this.ngAfterContentInit();
    }

    private riMaintenanceAfterAbandonAdd(): void {
        if (this.parentMode === 'BusinessOriginAdd' || this.parentMode === 'BusinessOriginUpdate') {
            this.riMaintenance.RequestWindowClose = true;
        }
    }

    private riMaintenanceAfterDelete(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.pageParams.actionAfterDelete);

        let formdata: Object = {};
        formdata['ROWID'] = this.getControlValue('ROWID');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formdata)
            .subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        this.fetchRecord();
                    } else {
                        this.markAsPrestine();
                        this.resetControl();
                        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordDeleted);
                        modalVO.closeCallback = this.riMaintenanceAfterDeleteSuccess.bind(this);
                        this.modalAdvService.emitMessage(modalVO);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.fetchRecord();
                });
    }

    public businessOriginCodeOnkeydown(e: any): void {
        if (e.target.value && (this.getFormMode() !== this.c_s_MODE_ADD)) {
            this.fetchRecord();
        }
        this.setFocusOnBusinessOriginSystemDesc.emit(true);
    }

    public businessSourceCodeOnSelect(data: any): void {
        if (data['BusinessOriginCode']) {
            this.setControlValue('BusinessOriginCode', data['BusinessOriginCode'] || '');
            this.uiForm.markAsDirty();
            if (this.getFormMode() !== this.c_s_MODE_ADD) {
                this.fetchRecord();
                this.riMaintenanceUpdateMode();
                this.riMaintenanceExecMode(this.getFormMode());
            }
        }
        if (data[GenericEllipsisEvent.add]) {
            this.riMaintenanceAddMode();
            this.riMaintenanceExecMode(this.getFormMode());
        }
    }

    public contactMediumCodeOnSelect(data: any): void {
        if (data.ContactMediumCode) {
            this.setControlValue('ContactMediumCode', data.ContactMediumCode);
            this.uiForm.markAsDirty();
        }
        if (data.ContactMediumDesc) {
            this.setControlValue('ContactMediumDesc', data.ContactMediumDesc);
        }
    }

    public businessSourceCodeOnkeydown(): void {
        let lookupIP = [
            {
                'table': 'BusinessSourceLang',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'BusinessSourceCode': this.getControlValue('BusinessSourceCode') ? this.getControlValue('BusinessSourceCode') : ''
                },
                'fields': ['BusinessSourceDesc']
            }
        ];

        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            let BusinessSourceLang: any = data[0][0];
            if (BusinessSourceLang) {
                this.setControlValue('BusinessSourceDesc', BusinessSourceLang.BusinessSourceDesc ? BusinessSourceLang.BusinessSourceDesc : '');
            } else {
                this.setControlValue('BusinessSourceCode', '');
                this.setControlValue('BusinessSourceDesc', '');
            }
        });
    }

    public contactMediumCodeOnkeydown(): void {
        let lookupIP = [
            {
                'table': 'ContactMediumLang',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ContactMediumCode': this.getControlValue('ContactMediumCode') ? this.getControlValue('ContactMediumCode') : ''
                },
                'fields': ['ContactMediumDesc']
            }
        ];

        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            let ContactMediumLang = data[0][0];
            if (ContactMediumLang) {
                this.setControlValue('ContactMediumDesc', ContactMediumLang.ContactMediumDesc ? ContactMediumLang.ContactMediumDesc : '');
            } else {
                this.setControlValue('ContactMediumCode', '');
                this.setControlValue('ContactMediumDesc', '');
            }
        });
    }

    /* ===================== SAVE/Cancel/Delete ================== */
    public save(): void {
        this.riExchange.validateForm(this.uiForm);
        if (this.uiForm.valid) {
            switch (this.getFormMode()) {
                case 'add':
                    this.setFocusOnBusinessOriginCode.emit(true);
                    this.pageParams.actionAfterSave = 1;
                    this.riMaintenanceExecContinue(MntConst.eModeSaveAdd);
                    break;
                case 'update':
                    this.pageParams.actionAfterSave = 2;
                    this.riMaintenanceExecContinue(MntConst.eModeSaveUpdate);
                    break;
            }
        }
    }

    public delete(): void {
        this.riMaintenanceDeleteMode();
        this.riExchange.validateForm(this.uiForm);
        if (this.uiForm.valid) {
            this.pageParams.actionAfterDelete = 3;
            this.riMaintenanceExecContinue(MntConst.eModeDelete);
        }
    }

    public confirm(): any {
        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.confirmed.bind(this));
        this.modalAdvService.emitPrompt(modalVO);
    }

    public confirmDelete(): any {
        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.confirmed.bind(this));
        this.modalAdvService.emitPrompt(modalVO);
    }
    public confirmed(obj: any): any {
        this.riMaintenanceExecContinue(this.getFormMode());
    }
    public cancel(): void {
        this.resetControl();
        this.setControlValue('BusinessOriginCode', this.pageParams.BusinessOriginCode);
        if (this.getFormMode() !== this.c_s_MODE_ADD) {
            this.fetchRecord();
        } else {
            this.riMaintenanceSelectMode();
            this.ngAfterContentInit();
        }
        this.riMaintenanceExecMode(this.getFormMode());
    }

}
