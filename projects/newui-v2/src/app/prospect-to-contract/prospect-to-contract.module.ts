import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { BusinessOriginMaintenanceComponent } from './TableMaintenanceBusiness/BusinessOrigin/iCABSBBusinessOriginMaintenance.component';
import { CMProspectBulkImportComponent } from './ProspectAndDiaryMaintenance/iCABSCMProspectBulkImport.component';
import { DiaryDayMaintenanceComponent } from './ProspectAndDiaryMaintenance/DiaryDay/iCABSCMDiaryDayMaintenance.component';
import { DiaryMaintenanceComponent } from './ProspectAndDiaryMaintenance/iCABSCMDiaryMaintenance.component';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { LostBusinessDetailGridComponent } from './TableMaintenanceBusiness/BusinessOrigin/iCABSBLostBusinessDetailGrid.component';
import { MaintenanceTypeAccountComponent } from './ProspectAndDiaryMaintenance/tabs/maintenanceTypeAccount';
import { MaintenanceTypeGeneralComponent } from './ProspectAndDiaryMaintenance/tabs/maintenanceTypeGeneral';
import { MaintenanceTypePremiseComponent } from './ProspectAndDiaryMaintenance/tabs/maintenanceTypePremise';
import { PipelineGridComponent } from './SalesOrderProcessing/iCABSSPipelineGrid.component';
import { ProspectEntryGridComponent } from './ProspectAndDiaryMaintenance/iCABSCMProspectEntryGrid';
import { ProspectEntryMaintenanceComponent } from './ProspectAndDiaryMaintenance/NatAccJob/iCABSCMProspectEntryMaintenance';
import { ProspectGridComponent } from './ProspectAndDiaryMaintenance/iCABSCMProspectGrid';
import { ProspectMaintenanceComponent } from './ProspectAndDiaryMaintenance/iCABSCMPipelineProspectMaintenance';
import { ProspectToContractRootComponent } from './prospect-to-contract.component';
import { ProspectToContractRouteDefinitions } from './prospect-to-contract.route';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { SharedModule } from '@shared/shared.module';

@NgModule({
    exports: [
        CMProspectBulkImportComponent,
        DiaryMaintenanceComponent
    ],
    imports: [
        HttpClientModule,
        InternalSearchEllipsisModule,
        InternalSearchModule,
        ProspectToContractRouteDefinitions,
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        SharedModule
    ],
    declarations: [
        BusinessOriginMaintenanceComponent,
        CMProspectBulkImportComponent,
        DiaryDayMaintenanceComponent,
        DiaryMaintenanceComponent,
        LostBusinessDetailGridComponent,
        MaintenanceTypeAccountComponent,
        MaintenanceTypeGeneralComponent,
        MaintenanceTypePremiseComponent,
        PipelineGridComponent,
        ProspectEntryGridComponent,
        ProspectEntryMaintenanceComponent,
        ProspectGridComponent,
        ProspectMaintenanceComponent,
        ProspectToContractRootComponent
    ],
    entryComponents: [
        BusinessOriginMaintenanceComponent,
        CMProspectBulkImportComponent,
        DiaryMaintenanceComponent,
        LostBusinessDetailGridComponent,
        MaintenanceTypeAccountComponent,
        MaintenanceTypeGeneralComponent,
        MaintenanceTypePremiseComponent,
        PipelineGridComponent,
        ProspectEntryGridComponent,
        ProspectGridComponent,
        ProspectMaintenanceComponent,
        ProspectToContractRootComponent
    ]

})

export class ProspectToContractModule { }
