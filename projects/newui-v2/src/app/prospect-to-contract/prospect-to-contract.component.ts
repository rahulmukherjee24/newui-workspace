import { Component, ViewContainerRef, OnInit, AfterContentInit, OnDestroy, ViewChild, NgZone } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '@shared/services/auth.service';
import { ComponentInteractionService } from '@shared/services/component-interaction.service';
import { RiExchange } from '@shared/services/riExchange';

@Component({
    template: `<router-outlet></router-outlet>
<icabs-modal #errorModal="child" [(showHeader)]="showErrorHeader" [config]="{backdrop: 'static'}"></icabs-modal>`
})

export class ProspectToContractRootComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('errorModal') public errorModal;

    public authJson: any;
    public componentInteractionSubscription: Subscription;
    public displayNavBar: boolean;
    public errorSubscription: Subscription;
    public muleJson: any;
    public routerSubscription: Subscription;
    public showErrorHeader: boolean = true;

    constructor(viewContainerRef: ViewContainerRef, private riExchange: RiExchange, private _authService: AuthService, private _router: Router, private _ls: LocalStorageService, private _componentInteractionService: ComponentInteractionService, private _zone: NgZone) {
    }

    ngOnInit(): void {
        this.displayNavBar = true;
        if (!this._authService.oauthResponse) {
            this.authJson = this._ls.retrieve('OAUTH');
        } else {
            this.authJson = this._authService.oauthResponse;
        }
    }

    ngAfterContentInit(): void {
        // statement
    }

    ngOnDestroy(): void {
        if (this.componentInteractionSubscription) {
            this.componentInteractionSubscription.unsubscribe();
        }
        if (this.errorSubscription) {
            this.errorSubscription.unsubscribe();
        }
        if (this.routerSubscription) {
            this.routerSubscription.unsubscribe();
        }
        this.riExchange.releaseReference(this);
    }
}
