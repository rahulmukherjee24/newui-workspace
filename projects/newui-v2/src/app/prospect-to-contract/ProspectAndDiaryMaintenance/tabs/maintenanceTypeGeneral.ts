import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';

import { ActionTypes } from './../../../actions/prospect';
import { CustomerTypeSearchComponent } from '@app/internal/search/iCABSSCustomerTypeSearch';
import { ErrorService } from '@shared/services/error.service';
import { GlobalizeService } from '@shared/services/globalize.service';
import { HttpService } from '@shared/services/http-service';
import { LocaleTranslationService } from '@shared/services/translation.service';
import { PostCodeSearchComponent } from '@app/internal/search/iCABSBPostcodeSearch.component';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { RiExchange } from '@shared/services/riExchange';
import { ServiceConstants } from '@shared/constants/service.constants';
import { SICSearchComponent } from '@app/internal/search/iCABSSSICSearch.component';
import { Utils } from '@shared/services/utility';

@Component({
    selector: 'icabs-maintenance-type-a',
    templateUrl: 'maintenanceTabGeneral.html'
})

export class MaintenanceTypeGeneralComponent implements OnInit, OnDestroy {

    private allFormControls: Array<any> = [];

    public autoOpen: boolean = false;
    public cmdCancelDisable: boolean = true;
    public CurrDate: Date = new Date();
    public CustomerTypeSearchComponent = CustomerTypeSearchComponent;
    public dateDisable: any = {
        estimatedClosedDate: false
    };
    public disableInitial: Array<string> = [
        'CustomerTypeDesc', 'SICDescription', 'SICDescription', 'EstimatedTotalQuotes', 'EstimatedTotalValue', 'ExpectedTotalValue',
        'ExpectedValue', 'ExpectedJobValue', 'ExpectedProductSaleValue', 'DaysOld', 'ProspectStatusDesc'
    ];
    public fieldDisable: any = {
        'CustomerTypeCode': false,
        'CustomerTypeDesc': true,
        'SICCode': false,
        'SICDescription': true,
        'Narrative': false,
        'EstimatedContractQuotes': false,
        'EstimatedProductSaleQuotes': false,
        'EstimatedJobQuotes': false,
        'EstimatedTotalQuotes': true,
        'EstimatedContractValue': false,
        'EstimatedJobValue': false,
        'EstimatedProductSaleValue': false,
        'EstimatedTotalValue': true,
        'ExpectedTotalValue': true,
        'ExpectedValue': true,
        'ExpectedJobValue': true,
        'ExpectedProductSaleValue': true,
        'EstimatedClosedDate': false,
        'DaysOld': true,
        'ProspectStatusCode': false,
        'ProspectStatusDesc': true,
        'Probability': false,
        'ConvertedToNumber': false,
        'chkAuthorise': false,
        'chkReject': false,
        'chkBranch': false
    };
    public fieldRequired: any = {
        'CustomerTypeCode': false,
        'CustomerTypeDesc': false,
        'SICCode': false,
        'SICDescription': false,
        'Narrative': false,
        'EstimatedContractQuotes': false,
        'EstimatedProductSaleQuotes': false,
        'EstimatedJobQuotes': false,
        'EstimatedTotalQuotes': false,
        'EstimatedContractValue': false,
        'EstimatedJobValue': false,
        'EstimatedProductSaleValue': false,
        'EstimatedTotalValue': false,
        'ExpectedTotalValue': false,
        'ExpectedValue': false,
        'ExpectedJobValue': false,
        'ExpectedProductSaleValue': false,
        'EstimatedClosedDate': false,
        'DaysOld': false,
        'ProspectStatusCode': false,
        'ProspectStatusDesc': false,
        'Probability': false,
        'ConvertedToNumber': false,
        'chkAuthorise': false,
        'chkReject': false,
        'chkBranch': false
    };
    public fieldVisibility: any = {
        'isHiddenCustomerTypeCode': false,
        'isHiddenCustomerTypeDesc': false,
        'isHiddenSICCode': false,
        'isHiddenSICDescription': false,
        'isHiddenNarrative': false,
        'isHiddenEstimatedContractQuotes': false,
        'isHiddenEstimatedProductSaleQuotes': false,
        'isHiddenEstimatedJobQuotes': false,
        'isHiddenEstimatedTotalQuotes': false,
        'isHiddenEstimatedContractValue': false,
        'isHiddenEstimatedJobValue': false,
        'isHiddenEstimatedProductSaleValue': false,
        'isHiddenExpectedTotalValue': false,
        'isHiddenExpectedValue': false,
        'isHiddenEstimatedClosedDate': false,
        'isHiddenDaysOld': false,
        'isHiddenProspectStatusCode': true,
        'isHiddenProspectStatusDesc': false,
        'isHiddenProbability': false,
        'isHiddenConvertedToNumber': false,
        'isHiddenchkAuthorise': false,
        'isHiddenchkReject': false,
        'isHiddenchkBranch': false,
        'isHiddenproductHeading': false

    };
    public inputParams: any = { 'parentMode': 'Contract-Search', 'ContractTypeCode': '', 'countryCode': '', 'businessCode': '' };
    public inputParamsCustomerTypeSearch: any = { 'parentMode': 'LookUp-PremiseIncSIC', 'CWIExcludeCustomerTypes': '' };
    public inputSICSearch: any = { 'parentMode': 'LookUp-SICLang' };
    public isDisabledCustomerTypeSearch: boolean = false;
    public maintenanceGeneralFormGroup: FormGroup;
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public parentQueryParams: any;
    public queryContract: QueryParams = new QueryParams();
    public queryLookUp: QueryParams = new QueryParams();
    public queryParam: QueryParams = new QueryParams();
    public queryParamsProspect: any = {
        action: '0',
        operation: 'ContactManagement/iCABSCMPipelineProspectMaintenance',
        module: 'prospect',
        method: 'prospect-to-contract/maintenance',
        contentType: 'application/x-www-form-urlencoded'
    };
    public querySubscription: Subscription;
    public searchModalRoute: string = '';
    public showCloseButton: boolean = true;
    public showHeader: boolean = true;
    public sICSearchComponent: any = SICSearchComponent;
    public storeSubscription: Subscription;
    public systemParametersFromParent: any = { ttBusiness: [{}], systemChars: {} };
    public translateSubscription: Subscription;
    public zipSearchComponent = PostCodeSearchComponent;

    constructor(
        private errorService: ErrorService,
        private fb: FormBuilder,
        private globalize: GlobalizeService,
        private httpService: HttpService,
        private riExchange: RiExchange,
        private route: ActivatedRoute,
        private router: Router,
        private serviceConstants: ServiceConstants,
        private store: Store<any>,
        private translateService: LocaleTranslationService,
        private utils: Utils,
        private zone: NgZone
    ) {
        this.maintenanceGeneralFormGroup = this.fb.group({
            CustomerTypeCode: [{ value: '', disabled: false }],
            CustomerTypeDesc: [{ value: '', disabled: false }],
            SICCode: [{ value: '', disabled: false }],
            SICDescription: [{ value: '', disabled: false }],
            Narrative: [{ value: '', disabled: false }],
            EstimatedContractQuotes: [{ value: '0', disabled: false }],
            EstimatedJobQuotes: [{ value: '0', disabled: false }],
            EstimatedProductSaleQuotes: [{ value: '0', disabled: false }],
            EstimatedTotalQuotes: [{ value: '0', disabled: false }],
            EstimatedContractValue: [{ value: '0', disabled: false }],
            EstimatedJobValue: [{ value: '0', disabled: false }],
            EstimatedProductSaleValue: [{ value: '0', disabled: false }],
            EstimatedTotalValue: [{ value: '0', disabled: false }],
            ExpectedValue: [{ value: '0', disabled: false }],
            ExpectedJobValue: [{ value: '0', disabled: false }],
            ExpectedProductSaleValue: [{ value: '0', disabled: false }],
            ExpectedTotalValue: [{ value: '0', disabled: false }],
            EstimatedClosedDate: [{ value: '', disabled: false }],
            DaysOld: [{ value: '', disabled: false }],
            ProspectStatusCode: [{ value: '', disabled: false }],
            ProspectStatusDesc: [{ value: '', disabled: false }],
            Probability: [{ value: '', disabled: false }],
            ConvertedToNumber: [{ value: '', disabled: false }],
            chkAuthorise: [{ value: '', disabled: false }],
            chkReject: [{ value: '', disabled: false }],
            chkBranch: [{ value: '', disabled: false }],
            cancelClick: [{ value: '', disabled: false }]
        });
        this.storeSubscription = store.select('prospect').subscribe(data => {
            if (data['action']) {
                if (data['action'].toString() === ActionTypes.SAVE_SYSTEM_PARAMETER) {
                    this.systemParametersFromParent['systemChars'] = data['data']['systemChars'];
                    this.setUI();
                } else if (data['action'].toString() === ActionTypes.EXCHANGE_METHOD) {
                    for (let m of data['data']) {
                        if (this[m]) {
                            this[m]();
                        }
                    }

                } else if (data['action'].toString() === ActionTypes.FORM_CONTROLS) {
                    let formKey: string = Object.keys(data['data'])[0];
                    let formKeyInList: Array<any> = this.allFormControls.filter(element => {
                        return element.hasOwnProperty(formKey);
                    });
                    if (!(formKeyInList && formKeyInList.length)) {
                        this.allFormControls.push(data['data']);
                    }
                } else if (data['action'].toString() === ActionTypes.UPDATE_FORMS) {
                    this.updateStoreControl(ActionTypes.FORM_CONTROLS);
                }
            }

        });
    }

    ngOnInit(): void {
        this.translateService.setUpTranslation();
        this.updateStoreControl(ActionTypes.FORM_CONTROLS);
        this.disableAllGeneral();
    }

    ngOnDestroy(): void {
        this.storeSubscription.unsubscribe();
        if (this.querySubscription)
            this.querySubscription.unsubscribe();
        if (this.translateSubscription)
            this.translateSubscription.unsubscribe();
    }

    private updateStoreControl(action: string): void {
        this.store.dispatch({
            type: ActionTypes[action],
            payload: { formGeneral: this.maintenanceGeneralFormGroup }
        });
    }

    /**
     * Total quote calculation
     */

    private calculateTotalQuotes(): void {
        let estimatedContractQuotes: any, estimatedJobQuotes: any, estimatedProductSaleQuotes: any;
        estimatedContractQuotes = (this.maintenanceGeneralFormGroup.controls['EstimatedContractQuotes'].value) ? this.globalize.parseIntegerToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedContractQuotes'].value) as any : 0;
        estimatedJobQuotes = (this.maintenanceGeneralFormGroup.controls['EstimatedJobQuotes'].value) ? this.globalize.parseIntegerToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedJobQuotes'].value) as any : 0;
        estimatedProductSaleQuotes = (this.maintenanceGeneralFormGroup.controls['EstimatedProductSaleQuotes'].value) ? this.globalize.parseIntegerToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedProductSaleQuotes'].value) as any : 0;
        let total: any = estimatedContractQuotes + estimatedJobQuotes + estimatedProductSaleQuotes;
        this.maintenanceGeneralFormGroup.controls['EstimatedTotalQuotes'].setValue(total);
    }
    /**
     * Total quote value calculation
     */

    private calculateTotalValues(): void {
        let estimatedContractValue: any, estimatedJobValue: any, estimatedProductSaleValue: any;
        estimatedContractValue = (this.maintenanceGeneralFormGroup.controls['EstimatedContractValue'].value) ? this.globalize.parseDecimalToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedContractValue'].value, 2) as any : 0;
        estimatedJobValue = (this.maintenanceGeneralFormGroup.controls['EstimatedJobValue'].value) ? this.globalize.parseDecimalToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedJobValue'].value, 2) as any : 0;
        estimatedProductSaleValue = (this.maintenanceGeneralFormGroup.controls['EstimatedProductSaleValue'].value) ? this.globalize.parseDecimalToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedProductSaleValue'].value, 2) as any : 0;
        let total: any = estimatedContractValue + estimatedJobValue + estimatedProductSaleValue;
        this.maintenanceGeneralFormGroup.controls['EstimatedTotalValue'].setValue(total);
    }

    /***Set fields properties at the time of page load
    */
    public setUI(): void {
        if (this.systemParametersFromParent.systemChars.customBusinessObject.Update === false) {
            this.fieldVisibility.isHiddenProspectStatusCode = true;
            this.maintenanceGeneralFormGroup.controls['EstimatedClosedDate'].setValue(this.systemParametersFromParent.systemChars.DefaultClosedDate);
            this.maintenanceGeneralFormGroup.controls['Probability'].setValue(this.systemParametersFromParent.systemChars.DefaultProbability);
            this.maintenanceGeneralFormGroup.controls['DaysOld'].setValue('0');
            this.maintenanceGeneralFormGroup.controls['EstimatedContractQuotes'].setValue('0');
            this.maintenanceGeneralFormGroup.controls['EstimatedJobQuotes'].setValue('0');
            this.maintenanceGeneralFormGroup.controls['EstimatedProductSaleQuotes'].setValue('0');
            this.maintenanceGeneralFormGroup.controls['EstimatedTotalQuotes'].setValue('0');
            this.maintenanceGeneralFormGroup.controls['EstimatedContractValue'].setValue('0');
            this.maintenanceGeneralFormGroup.controls['EstimatedJobValue'].setValue('0');
            this.maintenanceGeneralFormGroup.controls['EstimatedProductSaleValue'].setValue('0');
            this.maintenanceGeneralFormGroup.controls['EstimatedTotalValue'].setValue('0');
            this.maintenanceGeneralFormGroup.controls['ExpectedValue'].setValue('0');
            this.maintenanceGeneralFormGroup.controls['ExpectedJobValue'].setValue('0');
            this.maintenanceGeneralFormGroup.controls['ExpectedProductSaleValue'].setValue('0');
            this.estimatedContractQuotesOnChange();
            this.estimatedJobQuotesOnChange();
            this.estimatedProductSaleQuotesOnChange();
            this.estimatedContractValueOnChange();
            this.estimatedJobValueOnChange();
            this.estimatedProductSaleValueOnChange();
        }
        if (this.systemParametersFromParent.systemChars.vSICCodeEnable) {
            this.inputParamsCustomerTypeSearch.parentMode = 'LookUp-PremiseIncSIC';
        } else {
            this.inputParamsCustomerTypeSearch.parentMode = 'LookUp';
        }
        if (this.systemParametersFromParent.systemChars.DefaultClosedDate) {
            let dateArr = this.systemParametersFromParent.systemChars.DefaultClosedDate.split('/');
            this.CurrDate = new Date(dateArr[0], dateArr[1] - 1, dateArr[2]);
        }

        if (this.systemParametersFromParent.systemChars.vSICCodeEnable) {
            this.fieldVisibility.isHiddenSICCode = false;
            this.fieldRequired.SICCode = true;
        } else {
            this.fieldVisibility.isHiddenSICCode = true;
            this.fieldRequired.SICCode = false;
        }
        if (this.systemParametersFromParent.systemChars.vSCCustomerTypeMandatory) {
            this.fieldRequired.CustomerTypeCode = true;
        } else {
            this.fieldRequired.CustomerTypeCode = false;
        }
        this.fieldRequired['Narrative'] = true;
        if (this.systemParametersFromParent.systemChars.currentURL.match(new RegExp('Prospect', 'i'))) {
            this.fieldRequired.ProspectStatusCode = false;
            this.fieldDisable.ProspectStatusCode = true;
            this.fieldVisibility.isHiddenConvertedToNumber = true;
            this.fieldVisibility.isHiddenchkAuthorise = true;

        } else if (this.systemParametersFromParent.systemChars.currentURL.match(new RegExp('NatAxJob', 'i'))) {
            this.fieldDisable.ProspectStatusCode = false;
            this.fieldRequired.ProspectStatusCode = true;
            this.fieldVisibility.isHiddenConvertedToNumber = false;
            this.fieldVisibility.isHiddenchkAuthorise = true;
        } else {
            this.fieldVisibility.isHiddenConvertedToNumber = false;
            this.fieldVisibility.isHiddenchkAuthorise = false;
        }
        this.fieldDisable.ConvertedToNumber = true;

        if (this.systemParametersFromParent.systemChars.currentURL.match(new RegExp('Confirm', 'i'))) {
            this.fieldRequired.chkAuthorise = true;
            this.fieldRequired.chkReject = true;
            this.fieldRequired.chkBranch = true;
        }

        if (this.systemParametersFromParent.systemCharsvProductSaleInUse) {
            this.fieldVisibility.isHiddenExpectedProductSaleValue = false;
            this.fieldVisibility.productHeading = false;
            this.fieldVisibility.isHiddenEstimatedProductSaleQuotes = false;
            this.fieldVisibility.isHiddenEstimatedProductSaleValue = false;
        }

        let parentMode = this.riExchange.ParentMode(this.systemParametersFromParent.systemChars.routeParams);
        if (parentMode === 'ContactManagement' || parentMode === 'WorkOrderMaintenance' || parentMode === 'ContactRelatedTicket'
            || parentMode === 'SalesOrder' || parentMode === 'CallCentreSearch' || parentMode === 'PipelineGridNew' || parentMode === 'CallCentreSearchNew'
            || parentMode === 'CallCentreSearchNewExisting' || parentMode === 'LostBusinessRequest') {
            if (this.systemParametersFromParent.systemChars.customBusinessObject.Update === true)
                this.fieldVisibility.isHiddenProspectStatusCode = false;
        }
        if (parentMode === 'CallCentreSearchNew' || parentMode === 'CallCentreSearchNewExisting') {
            this.maintenanceGeneralFormGroup.controls['Narrative'].setValue(this.riExchange.GetParentHTMLInputValue(this.systemParametersFromParent.systemChars.routeParams, 'CallNotepad'));
        }
        if (this.systemParametersFromParent.systemChars.vSCDisableQuoteDetsOnAddProspect && !parentMode) {
            this.fieldVisibility.isHiddenEstimatedContractQuotes = true;
            this.fieldVisibility.isHiddenEstimatedContractValue = true;
            this.fieldVisibility.isHiddenExpectedValue = true;
            this.fieldVisibility.isHiddenEstimatedClosedDate = true;
        } else {
            this.fieldVisibility.isHiddenEstimatedContractQuotes = false;
            this.fieldVisibility.isHiddenEstimatedContractValue = false;
            this.fieldVisibility.isHiddenExpectedValue = false;
            this.fieldVisibility.isHiddenEstimatedClosedDate = false;
        }
        this.updateValidators();
        this.updateDisable();
    }

    /*** Update validation rules
    */

    public updateValidators(): void {
        for (let f in this.fieldRequired) {
            if (this.fieldRequired.hasOwnProperty(f)) {
                if (this.maintenanceGeneralFormGroup.controls[f]) {
                    if (this.fieldRequired[f]) {
                        if (f === 'EstimatedContractValue' || f === 'EstimatedJobValue' || f === 'EstimatedProductSaleValue') {
                            this.maintenanceGeneralFormGroup.controls[f].setValidators([this.minValidateDecimal.bind(this), this.utils.commonValidate]);
                        } else if (f === 'EstimatedContractQuotes' || f === 'EstimatedJobQuotes' || f === 'EstimatedProductSaleQuotes') {
                            this.maintenanceGeneralFormGroup.controls[f].setValidators([this.minValidate.bind(this), this.utils.commonValidate]);
                        } else if (!(f === 'chkAuthorise' || f === 'chkReject' || f === 'chkBranch')) {
                            this.maintenanceGeneralFormGroup.controls[f].setValidators([Validators.required, this.utils.commonValidate]);
                        }
                    } else {
                        this.maintenanceGeneralFormGroup.controls[f].clearValidators();
                        this.maintenanceGeneralFormGroup.controls[f].setValidators([this.utils.commonValidate]);
                        if (f === 'EstimatedContractValue' || f === 'EstimatedJobValue' || f === 'EstimatedProductSaleValue') {
                            this.maintenanceGeneralFormGroup.controls[f].setValidators([this.minWithZeroValidateDecimal.bind(this), this.utils.commonValidate]);
                        } else if (f === 'EstimatedContractQuotes' || f === 'EstimatedJobQuotes' || f === 'EstimatedProductSaleQuotes') {
                            this.maintenanceGeneralFormGroup.controls[f].setValidators([this.minWithZeroValidate.bind(this), this.utils.commonValidate]);
                        }
                    }
                    this.maintenanceGeneralFormGroup.controls[f].updateValueAndValidity();
                }
            }
        }

    }

    /*** Update validation rules
    */

    public updateDisable(): void {
        for (let f in this.fieldDisable) {
            if (this.fieldDisable.hasOwnProperty(f)) {
                if (this.fieldDisable[f] && this.maintenanceGeneralFormGroup.controls[f]) {
                    this.maintenanceGeneralFormGroup.controls[f].disable();
                }
                else {
                    this.maintenanceGeneralFormGroup.controls[f].enable();
                }
                this.maintenanceGeneralFormGroup.controls[f].updateValueAndValidity();
            }
        }
    }

    /**
     * HTML inputs values changes on change of CustomerTypeCode
     */
    public customerTypeCodeOnchange(customerTypeCode: string): void {
        if (customerTypeCode) {
            this.queryParam.set(this.serviceConstants.Action, '0');
            this.queryParam.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            this.queryParam.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
            this.queryParam.set('CustomerTypeCode', customerTypeCode);
            this.queryParam.set('SICCodeEnable', this.systemParametersFromParent.systemChars.vSICCodeEnable);
            this.queryParam.set('PostDesc', 'CustomerType2');

            this.httpService.makeGetRequest(this.queryParamsProspect.method, this.queryParamsProspect.module, this.queryParamsProspect.operation, this.queryParam).subscribe(
                (data) => {
                    try {
                        if (data.CustomerTypeDesc)
                            this.maintenanceGeneralFormGroup.controls['CustomerTypeDesc'].setValue(data.CustomerTypeDesc);
                        else
                            this.maintenanceGeneralFormGroup.controls['CustomerTypeDesc'].setValue('');
                        if (data.SICCode)
                            this.maintenanceGeneralFormGroup.controls['SICCode'].setValue(data.SICCode);
                        else
                            this.maintenanceGeneralFormGroup.controls['SICCode'].setValue('');
                        if (data.SICDescription)
                            this.maintenanceGeneralFormGroup.controls['SICDescription'].setValue(data.SICDescription);
                        else
                            this.maintenanceGeneralFormGroup.controls['SICDescription'].setValue('');
                        this.sICCodeOnchange(this.maintenanceGeneralFormGroup.controls['SICCode'].value);
                    } catch (error) {
                        this.errorService.emitError(error);
                    }

                },
                (error) => {
                    this.errorService.emitError(error);
                }
            );
        } else {
            this.maintenanceGeneralFormGroup.controls['CustomerTypeDesc'].setValue('');
        }
    }

    /**
     * Receive CustomerTypeCode details from ellipsis
     */
    public onCustomerTypeSearch(data: any): void {
        if (this.systemParametersFromParent.systemChars.vSICCodeEnable) {
            this.maintenanceGeneralFormGroup.controls['CustomerTypeCode'].setValue(data.CustomerTypeCode);
            this.maintenanceGeneralFormGroup.controls['CustomerTypeDesc'].setValue(data.CustomerTypeDesc);
            this.maintenanceGeneralFormGroup.controls['SICCode'].setValue(data.SICCode);
            this.sICCodeOnchange(data.SICCode);
        } else {
            this.maintenanceGeneralFormGroup.controls['CustomerTypeCode'].setValue(data.CustomerTypeCode);
            this.maintenanceGeneralFormGroup.controls['CustomerTypeDesc'].setValue(data.CustomerTypeDesc);
        }
        this.maintenanceGeneralFormGroup.controls['CustomerTypeCode'].markAsDirty();
    }

    public onProspectStatusDataReceived(data: any): void {
        this.maintenanceGeneralFormGroup.controls['ProspectStatusCode'].setValue(data.ProspectStatusCode);
        this.maintenanceGeneralFormGroup.controls['ProspectStatusDesc'].setValue(data.ProspectStatusDesc);
    }
    /*** HTML inputs values changes on change of sICCode
   */
    public sICCodeOnchange(sICCode: string): void {
        if (sICCode) {
            this.queryParam.set(this.serviceConstants.Action, '0');
            this.queryParam.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            this.queryParam.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
            this.queryParam.set('SICCode', sICCode);
            this.queryParam.set('PostDesc', 'SIC');

            this.httpService.makeGetRequest(this.queryParamsProspect.method, this.queryParamsProspect.module, this.queryParamsProspect.operation, this.queryParam).subscribe(
                (data) => {
                    try {
                        this.maintenanceGeneralFormGroup.controls['SICDescription'].setValue(data.SICDesc);
                    } catch (error) {
                        this.errorService.emitError(error);
                    }

                },
                (error) => {
                    this.errorService.emitError(error);
                }
            );
        } else {
            this.maintenanceGeneralFormGroup.controls['SICDescription'].setValue('');
        }
    }


    /**
     * Method to call onchange EstimatedContractQuotes value
     */
    public estimatedContractQuotesOnChange(): void {
        this.fieldRequired.EstimatedContractValue = false;
        if (this.maintenanceGeneralFormGroup.controls['EstimatedContractQuotes'].value && this.globalize.parseIntegerToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedContractQuotes'].value) > 0) {
            this.fieldRequired.EstimatedContractValue = true;
        } else {
            this.fieldRequired.EstimatedContractValue = false;
        }
        this.updateValidators();
        this.calculateTotalQuotes();
    }

    /**
     * Method to call onchange estimatedJobQuotes value
     */
    public estimatedJobQuotesOnChange(): void {
        this.fieldRequired.EstimatedJobValue = false;
        if (this.maintenanceGeneralFormGroup.controls['EstimatedJobQuotes'].value && this.globalize.parseIntegerToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedJobQuotes'].value) > 0) {
            this.fieldRequired.EstimatedJobValue = true;
        } else {
            this.fieldRequired.EstimatedJobValue = false;
        }
        this.updateValidators();
        this.calculateTotalQuotes();
    }

    /**
     * Method to call onchange estimatedProductSale value
     */
    public estimatedProductSaleQuotesOnChange(): void {
        this.fieldRequired.EstimatedProductSaleValue = false;
        if (this.maintenanceGeneralFormGroup.controls['EstimatedProductSaleQuotes'].value && this.globalize.parseIntegerToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedProductSaleQuotes'].value) > 0) {
            this.fieldRequired.EstimatedProductSaleValue = true;
        } else {
            this.fieldRequired.EstimatedProductSaleValue = false;
        }
        this.updateValidators();
        this.calculateTotalQuotes();
    }

    /**
     * Method to call onchange estimatedContractValue value
     */
    public estimatedContractValueOnChange(): void {
        this.fieldRequired.EstimatedContractQuotes = false;
        if (this.maintenanceGeneralFormGroup.controls['EstimatedContractValue'].value && this.globalize.parseDecimalToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedContractValue'].value, 2) > 0) {
            this.fieldRequired.EstimatedContractQuotes = true;
        } else {
            this.fieldRequired.EstimatedContractQuotes = false;
        }
        this.updateValidators();
        this.calculateTotalValues();
    }

    /**
     * Method to call onchange estimatedJobValue value
     */
    public estimatedJobValueOnChange(): void {
        this.fieldRequired.EstimatedJobQuotes = false;
        if (this.maintenanceGeneralFormGroup.controls['EstimatedJobValue'].value && this.globalize.parseDecimalToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedJobValue'].value, 2) > 0) {
            this.fieldRequired.EstimatedJobQuotes = true;
        } else {
            this.fieldRequired.EstimatedJobQuotes = false;
        }
        this.calculateTotalValues();
        this.updateValidators();
    }
    /**
     * Method to call onchange estimatedProductSaleValue value
     */
    public estimatedProductSaleValueOnChange(): void {
        this.fieldRequired.EstimatedProductSaleQuotes = false;
        if (this.maintenanceGeneralFormGroup.controls['EstimatedProductSaleValue'].value && this.globalize.parseDecimalToFixedFormat(this.maintenanceGeneralFormGroup.controls['EstimatedProductSaleValue'].value, 2) > 0) {
            this.fieldRequired.EstimatedProductSaleQuotes = true;
        } else {
            this.fieldRequired.EstimatedProductSaleQuotes = false;
        }
        this.updateValidators();
        this.calculateTotalValues();
    }

    /**
     * Set prospect status code
     */
    public setStatuClick(): void {
        this.maintenanceGeneralFormGroup.controls['cancelClick'].setValue('y');
        this.systemParametersFromParent.systemChars['saveElement'].click();
    }

    /**
     * Get Prospect status
     */
    public getStatusDesc(): void {
        let data = [{
            'table': 'ProspectStatusLang',
            'query': { 'ProspectStatusCode': this.maintenanceGeneralFormGroup.controls['ProspectStatusCode'].value },
            'fields': ['ProspectStatusDesc']
        }];
        this.lookUpRecord(JSON.parse(JSON.stringify(data)), 100).subscribe((e) => {
            if (e.results[0][0])
                this.maintenanceGeneralFormGroup.controls['ProspectStatusDesc'].setValue(e.results[0][0].ProspectStatusDesc);
        });
    }

    public disableCancelPoor(): void {
        this.cmdCancelDisable = true;
    }

    /**
     * Generic method to call look up service
     */

    public lookUpRecord(data: any, maxresults: any): any {
        this.queryLookUp.set(this.serviceConstants.Action, '0');
        this.queryLookUp.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        this.queryLookUp.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (maxresults) {
            this.queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        }
        return this.httpService.lookUpRequest(this.queryLookUp, data);
    }


    /**
     * change date
     */

    public effectiveDateSelectedValue(event: any): void {
        if (!this.systemParametersFromParent.systemChars.updateGeneral)
            this.maintenanceGeneralFormGroup.controls['EstimatedClosedDate'].setValue(event.value);
    }

    /**
     * Custom validator to validate greater than 0
     */
    public minValidateDecimal(field: any): any {
        if (this.globalize.parseDecimal2ToFixedFormat(field.value) <= 0 || field.value === '') {
            return { 'invalidValue': true };
        }
        return null;
    }

    public minValidate(field: any): any {
        if (this.globalize.parseDecimal2ToFixedFormat(field.value) <= 0 || field.value === '') {
            return { 'invalidValue': true };
        }
        return null;
    }

    public minWithZeroValidate(field: any): any {
        if (this.globalize.parseIntegerToFixedFormat(field.value) < 0) {
            return { 'invalidValue': true };
        }
        return null;
    }
    public minWithZeroValidateDecimal(field: any): any {
        if (this.globalize.parseDecimal2ToFixedFormat(field.value) < 0) {
            return { 'invalidValue': true };
        }
        return null;
    }
    /**
     * Call onchane methods in update mode
     */

    public updateGeneralData(): void {
        this.fieldVisibility.isHiddenProspectStatusCode = false;
        this.fieldRequired.ProspectStatusCode = true;
        setTimeout(() => {
            let dateArr: Array<number> = this.maintenanceGeneralFormGroup.controls['EstimatedClosedDate'].value.split('/');
            this.CurrDate = new Date(dateArr[0], dateArr[1] - 1, dateArr[2]);
            this.systemParametersFromParent.systemChars.updateGeneral = false;
        }, 0);
        if (this.maintenanceGeneralFormGroup.controls['ProspectStatusCode'].value === '01' && this.systemParametersFromParent.systemChars.customBusinessObject.Enable === true)
            this.cmdCancelDisable = false;
        else
            this.cmdCancelDisable = true;
        this.estimatedContractQuotesOnChange();
        this.estimatedJobQuotesOnChange();
        this.estimatedProductSaleQuotesOnChange();
        this.estimatedContractValueOnChange();
        this.estimatedJobValueOnChange();
        this.estimatedProductSaleValueOnChange();
        this.updateValidators();
    }


    public disableAllGeneral(): void {
        for (let c in this.maintenanceGeneralFormGroup.controls) {
            if (this.maintenanceGeneralFormGroup.controls.hasOwnProperty(c)) {
                this.maintenanceGeneralFormGroup.controls[c].disable();
            }
        }
        for (let k in this.fieldDisable) {
            if (this.fieldDisable.hasOwnProperty(k)) {
                this.fieldDisable[k] = true;
            }
        }
        this.updateDisable();
        for (let p in this.dateDisable) {
            if (this.dateDisable.hasOwnProperty(p)) {
                this.dateDisable[p] = true;
            }
        }
        this.isDisabledCustomerTypeSearch = true;
    }
    public enableAllGeneral(): void {
        for (let c in this.maintenanceGeneralFormGroup.controls) {
            if (this.maintenanceGeneralFormGroup.controls.hasOwnProperty(c)) {
                this.maintenanceGeneralFormGroup.controls[c].enable();
            }
        }
        for (let k in this.fieldDisable) {
            if (this.fieldDisable.hasOwnProperty(k) && this.disableInitial.indexOf(k) < 0) {
                this.fieldDisable[k] = false;
            }
        }
        this.updateDisable();
        for (let p in this.dateDisable) {
            if (this.dateDisable.hasOwnProperty(p)) {
                this.dateDisable[p] = false;
            }
        }
        this.isDisabledCustomerTypeSearch = false;
        this.setUI();
    }
    public resetGrneralData(): void {
        this.setUI();
    }

    public onSICCodeSearch(data: any): void {
        this.maintenanceGeneralFormGroup.controls['SICCode'].setValue(data.SICCode);
        this.sICCodeOnchange(this.maintenanceGeneralFormGroup.controls['SICCode'].value);
        this.maintenanceGeneralFormGroup.controls['SICCode'].markAsDirty();
    }

}
