import { Component, OnInit, AfterContentInit, OnDestroy, Injector, ViewChild, ElementRef, Renderer } from '@angular/core';

import { AccountPremiseSearchComponent } from '@internal/grid-search/iCABSAAccountPremiseSearchGrid';
import { AccountSearchComponent } from '@internal/search/iCABSASAccountSearch';
import { AUPostcodeSearchComponent } from '@internal/grid-search/iCABSAAUPostcodeSearch';
import { BaseComponent } from '@base/BaseComponent';
import { ContactMediumLanguageSearchComponent } from '@internal/search/iCABSSContactMediumLanguageSearch.component';
import { ContractSearchComponent } from '@internal/search/iCABSAContractSearch';
import { CustomerTypeSearchComponent } from '@internal/search/iCABSSCustomerTypeSearch';
import { EmployeeSearchComponent } from '@internal/search/iCABSBEmployeeSearch';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceServiceModuleRoutes, InternalSearchModuleRoutes, ContractManagementModuleRoutes } from '@base/PageRoutes';
import { InvoiceGroupSearchComponent } from '@internal/search/iCABSAInvoiceGroupSearch.component';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst, RiTab } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PostCodeSearchComponent } from '@internal/search/iCABSBPostcodeSearch.component';
import { PremiseSearchComponent } from '@internal/search/iCABSAPremiseSearch';
import { ProspectSearchComponent } from '@internal/search/iCABSCMProspectSearch.component';
//import { ProspectStatusSearchComponent } from '@internal/search/iCABSCMProspectStatusSearch.component';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { RouteAwayComponent } from '@shared/components/route-away/route-away';

@Component({
    templateUrl: 'iCABSCMProspectEntryMaintenance.html'
})

export class ProspectEntryMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('AccountNumber') public account: ElementRef;
    @ViewChild('ContractNumber') public contract: ElementRef;
    @ViewChild('NavTabs') public navTabs;
    @ViewChild('PostCodeEllipsis') public postCodeEllipsis: any;
    @ViewChild('PremiseModal') public premiseModal: any;
    @ViewChild('PremiseNumber') public premise: ElementRef;
    @ViewChild('PremisePostCodeEllipsis') public premisePostCodeEllipsis: any;
    @ViewChild('PremiseSearchModal') public premiseSearchModal: any;
    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;
    @ViewChild('Save') public save;
    @ViewChild('TabContent') public tabContent;

    private currentTab: number = 0;
    private headerParams: any = {
        method: 'prospect-to-contract/maintenance',
        module: 'prospect',
        operation: 'ContactManagement/iCABSCMProspectEntryMaintenance'
    };
    private isRecordSelected: boolean = false;
    private pageFormData: Object = {};
    private tabLength: number = 3;

    public controls: Array<Object> = [
        //Main
        { name: 'AccountNumber', required: true, type: MntConst.eTypeCode },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger },
        { name: 'ProspectNumber', type: MntConst.eTypeAutoNumber },
        //Tab 1
        { name: 'AddressLine1', required: true },
        { name: 'AddressLine2' },
        { name: 'AddressLine3' },
        { name: 'AddressLine4', required: true },
        { name: 'AddressLine5' },
        { name: 'AssignToEmployeeCode' },
        { name: 'AssignToEmployeeName', disabled: true },
        { name: 'BusinessOriginCode', type: MntConst.eTypeCode },
        { name: 'BusinessOriginDesc', disabled: true },
        { name: 'cmdGetAddress' },
        { name: 'ContactEmail', type: MntConst.eTypeEMail },
        { name: 'ContactFax', type: MntConst.eTypeText },
        { name: 'ContactMediumCode', required: true, type: MntConst.eTypeCode },
        { name: 'ContactMediumDesc', disabled: true },
        { name: 'ContactMobile', type: MntConst.eTypeText },
        { name: 'ContactName', required: true },
        { name: 'ContactPosition', required: true, type: MntConst.eTypeText },
        { name: 'ContactTelephone', required: true, type: MntConst.eTypeText },
        { name: 'Name', required: true },
        { name: 'PDALeadEmployeeCode', type: MntConst.eTypeCode },
        { name: 'PDALeadEmployeeSurname', disabled: true },
        { name: 'PDALeadInd' },
        { name: 'Postcode', required: true, type: MntConst.eTypeCode },
        { name: 'SMSMessage' },

        //Tab 2 - Premise
        { name: 'AnnualValue', disabled: true, type: MntConst.eTypeCurrency },
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, required: true, type: MntConst.eTypeInteger },
        { name: 'ClientReference' },
        { name: 'cmdGetPremiseAddress' },
        { name: 'CommenceDate', required: true, type: MntConst.eTypeDate },
        { name: 'ContractExpiryDate', required: true, type: MntConst.eTypeDate },
        { name: 'CustomerTypeCode', required: true, type: MntConst.eTypeCode },
        { name: 'CustomerTypeDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'InvoiceGroupDesc', disabled: true },
        { name: 'InvoiceGroupNumber', required: true, type: MntConst.eTypeInteger },
        { name: 'NegotiatingSalesEmployeeCode', required: true, type: MntConst.eTypeCode },
        { name: 'NegotiatingSalesEmployeeSurname', disabled: true },
        { name: 'PaymentDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'PaymentTypeCode', required: true, type: MntConst.eTypeCode },
        { name: 'PremiseAddressLine1', required: true },
        { name: 'PremiseAddressLine2' },
        { name: 'PremiseAddressLine3' },
        { name: 'PremiseAddressLine4', required: true },
        { name: 'PremiseAddressLine5' },
        { name: 'PremiseContactName', required: true },
        { name: 'PremiseContactPosition', required: true, type: MntConst.eTypeText },
        { name: 'PremiseContactTelephone', required: true, type: MntConst.eTypeText },
        { name: 'PremiseName', required: true },
        { name: 'PremisePostcode', required: true, type: MntConst.eTypeCode },
        { name: 'PurchaseOrderNumber' },
        { name: 'ServicingSalesEmployeeCode', required: true, type: MntConst.eTypeCode },
        { name: 'ServicingSalesEmployeeSurname', disabled: true, type: MntConst.eTypeText },

        //Tab 3
        { name: 'chkAuthorise' },
        { name: 'chkBranch' },
        { name: 'chkReject' },
        { name: 'ConvertedToNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'Narrative', required: true },
        { name: 'ProspectStatusCode', required: true, type: MntConst.eTypeCode },
        { name: 'ProspectStatusDesc', disabled: true },

        //hidden
        { name: 'CreateNewInvoiceGroupInd' },
        { name: 'ErrorMessageDesc', type: MntConst.eTypeText },
        { name: 'LocalSystemInd' },
        { name: 'LoggedInBranchNumber', type: MntConst.eTypeInteger },
        { name: 'NegBranchNumber', type: MntConst.eTypeInteger },
        { name: 'OptionMenu' },
        { name: 'ProspectROWID' },
        { name: 'ProspectTypeDesc', type: MntConst.eTypeText },
        { name: 'SalesBranchNumber', type: MntConst.eTypeInteger },
        { name: 'ServiceBranchNumber', type: MntConst.eTypeInteger }
    ];
    public dropdownConfig = {
        businessOrigin: {
            isDisabled: true,
            isRequired: false,
            inputParams: {
            },
            triggerValidate: false,
            active: {
                id: '',
                text: ''
            }
        }
    };
    public ellipsisConfig: any = {
        prospect: {
            childConfigParams: {
                'parentMode': 'Search',
                'vAddURLParam': this.pageParams.addURLParam,
                'showAddNew': true
            },
            component: ProspectSearchComponent
        },
        account: {
            childConfigParams: {
                'parentMode': 'LookUp-NatAx',
                'showCountryCode': false,
                'showBusinessCode': false,
                'showAddNewDisplay': false
            },
            component: AccountSearchComponent
        },
        accountPremise: {
            childConfigParams: {
                'parentMode': 'Prospect',
                'showAddNew': false
            },
            component: AccountPremiseSearchComponent
        },
        contract: {
            childConfigParams: {
                'parentMode': 'Prospect',
                'showCountry': false,
                'showBusiness': false,
                'showAddNew': false
            },
            component: ContractSearchComponent
        },
        premise: {
            childConfigParams: {
                'parentMode': 'Prospect',
                'showAddNew': false
            },
            component: PremiseSearchComponent
        },
        contactMediumLang: {
            childConfigParams: {
                'parentMode': 'ContactManagement',
                'showAddNew': false
            },
            component: ContactMediumLanguageSearchComponent
        },
        prospectStatusSearchConfig: {
            autoOpen: false,
            ellipsisTitle: 'Prospect Status Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'ContactManagement'
            },
            httpConfig: {
                operation: 'ContactManagement/iCABSCMProspectStatusSearch',
                module: 'prospect',
                method: 'prospect-to-contract/search'
            },
            tableColumn: [
                { title: 'Code', name: 'ProspectStatusCode', type: MntConst.eTypeCode },
                { title: 'Description', name: 'ProspectStatusDesc', type: MntConst.eTypeText },
                { title: 'Converted', name: 'ProspectConvertedInd', type: MntConst.eTypeCheckBox },
                { title: 'User Selectable', name: 'UserSelectableInd', type: MntConst.eTypeCheckBox }
            ],
            disable: false
        },
        invoiceGroup: {
            childConfigParams: {
                'parentMode': 'ContactManagement',
                'isEllipsis': true,
                'showAddNew': false
            },
            component: InvoiceGroupSearchComponent
        },
        customerType: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'showAddNew': false
            },
            component: CustomerTypeSearchComponent
        },
        paymentTypeSearchConfig: {
            ellipsisTitle: 'Payment Type Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp',
                rowmetadata: [
                    { name: 'ReferenceRequired', type: 'img' },
                    { name: 'MandateRequired', type: 'img' },
                    { name: 'InvoiceSuspendInd', type: 'img' },
                    { name: 'DefaultInd', type: 'img' }
                ],
                extraParams: {
                    'search.sortby': 'PaymentTypeCode'
                }
            },
            httpConfig: {
                operation: 'Business/iCABSBPaymentTypeSearch',
                module: 'payment',
                method: 'bill-to-cash/search'
            },
            tableColumn: [
                { title: 'Code', name: 'PaymentTypeCode' },
                { title: 'Description', name: 'PaymentDesc' },
                { title: 'Reference Required', name: 'ReferenceRequired' },
                { title: 'Mandate Required', name: 'MandateRequired' },
                { title: 'Suspend Invoice', name: 'InvoiceSuspendInd' },
                { title: 'Default', name: 'DefaultInd' }
            ],
            disable: false
        },
        negSalesEmployee: {
            childConfigParams: {
                'parentMode': 'LookUp-NegSales',
                'showAddNew': false
            },
            component: EmployeeSearchComponent
        },
        assignToEmployee: {
            childConfigParams: {
                'parentMode': 'LookUp-ProspectAssignTo',
                'showAddNew': false
            },
            component: EmployeeSearchComponent
        },
        serviceSalesEmployee: {
            childConfigParams: {
                'parentMode': 'LookUp-ServiceSales',
                'showAddNew': false
            },
            component: EmployeeSearchComponent
        },
        PDALeadEmployee: {
            childConfigParams: {
                'parentMode': 'LookUp-PDALead',
                'showAddNew': false
            },
            component: EmployeeSearchComponent
        },
        postCode: {
            childConfigParams: {
                'parentMode': 'Prospect',
                'showAddNew': false
            },
            component: PostCodeSearchComponent
        },
        premisePostCode: {
            childConfigParams: {
                'parentMode': 'Premise',
                'showAddNew': false
            },
            component: AUPostcodeSearchComponent
        }
    };
    public isAddEnabled: boolean = false;
    public isAnnualValueVisible: boolean = false;
    public isAssignToEmployeeVisible: boolean = false;
    public isBtnGetAddressDisabled: boolean = true;
    public isBtnGetAddressVisible: boolean = true;
    public isBtnGetPremiseAddressDisabled: boolean = true;
    public isBtnGetPremiseAddressVisible: boolean = true;
    public isConfirmButtonsVisible: boolean = false;
    public isContractExpiryDateRequired: boolean = false;
    public isConvertedToNumberVisible: boolean = false;
    public isOptionDisabled: boolean = true;
    public isPDALeadVisible: boolean = false;
    public isProspectStatusRequired: boolean = false;
    public isProspectStatusVisible: boolean = false;
    public isSaveDisabled: boolean = true;
    public isSMSMessageVisible: boolean = false;
    public menuArray: Array<any> = [{ value: '', text: 'Options' }];
    public pageId: string = '';
    public showHeader: boolean = true;
    public showCloseButton: boolean = true;
    public tab: any = {
        tab1: { id: 'grdMain', name: 'Main', visible: true, active: true },
        tab2: { id: 'grdPremise', name: 'Premise', visible: true, active: false },
        tab3: { id: 'grdNotes', name: 'Notes', visible: true, active: false }
    };
    public riTab: RiTab;

    constructor(injector: Injector, public renderer: Renderer) {
        super(injector);
        this.pageId = PageIdentifier.ICABSCMPROSPECTENTRYMAINTENANCE;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        if (this.isReturning()) {
            this.pageParams.currentMode = MntConst.eModeUpdate;
            this.ellipsisConfig.prospect.childConfigParams.vAddURLParam = this.pageParams.addURLParam;
            this.isRecordSelected = true;
            this.windowOnLoad();
            if (this.getControlValue('BusinessOriginCode')) {
                this.dropdownConfig.businessOrigin.active = {
                    id: this.getControlValue('BusinessOriginCode'),
                    text: this.getControlValue('BusinessOriginCode') + ' - ' + this.getControlValue('BusinessOriginDesc')
                };
            }
            this.ellipsisConfig.contract.childConfigParams.accountNumber = this.getControlValue('AccountNumber');
            this.ellipsisConfig.invoiceGroup.childConfigParams.AccountNumber = this.getControlValue('AccountNumber');
            this.ellipsisConfig.premise.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
            this.storeData();
            if (this.pageParams.isNatAxJob) this.afterFetch();
            this.disableControl('OptionMenu', false);
        } else {
            this.pageParams.currentMode = MntConst.eModeNormal;
            this.pageParams.isNatAxJob = (this.router.url.indexOf('nataxjob') > -1);
            this.pageParams.addURLParam = this.pageParams.isNatAxJob ? '<NatAxJob>' : '<Confirm>';

            if (this.pageParams.isNatAxJob) {
                this.ellipsisConfig.prospect.childConfigParams.vAddURLParam = this.pageParams.addURLParam;
                this.ellipsisConfig.prospect.autoOpen = true;
            }
            this.loadSysCharValues();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private loadSysCharValues(): void {
        let sysCharNumbers = [
            this.sysCharConstants.SystemCharEnableAddressLine3,
            this.sysCharConstants.SystemCharEnableHopewiserPAF,
            this.sysCharConstants.SystemCharEnableDatabasePAF,
            this.sysCharConstants.SystemCharAddressLine4Required,
            this.sysCharConstants.SystemCharAddressLine5Required,
            this.sysCharConstants.SystemCharPostCodeRequired,
            this.sysCharConstants.SystemCharPostCodeMustExistinPAF,
            this.sysCharConstants.SystemCharRunPAFSearchOnFirstAddressLine,
            this.sysCharConstants.SystemCharMaximumAddressLength,
            this.sysCharConstants.SystemCharDisableFirstCapitalOnAddressContact
        ];

        let sysCharIP: Object = {
            operation: 'iCABSCMProspectEntryMaintenance',
            action: 0,
            businessCode: this.utils.getBusinessCode(),
            countryCode: this.utils.getCountryCode(),
            SysCharList: sysCharNumbers.toString()
        };

        this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            this.pageParams.isSCEnableAddressLine3 = data['records'][0].Required;
            this.pageParams.isSCEnableHopewiserPAF = data['records'][1].Required;
            this.pageParams.isSCEnableDatabasePAF = data['records'][2].Required;
            this.pageParams.isSCAddressLine4Required = data['records'][3].Required;
            this.pageParams.isSCAddressLine5Required = data['records'][4].Required;
            this.pageParams.isSCAddressLine5Logical = data['records'][4].Logical;
            this.pageParams.isSCPostCodeRequired = data['records'][5].Required;
            this.pageParams.isSCPostCodeMustExistInPAF = data['records'][6].Required;
            this.pageParams.isSCRunPAFSearchOnFirstAddressLine = data['records'][7].Required;
            this.pageParams.isSCEnableMaxAddress = data['records'][8].Required;
            this.pageParams.maximumAddressLength = data['records'][8].Integer;
            this.pageParams.isSCCapitalFirstLtr = data['records'][9].Required;

            this.LookUp.GetRegistrySetting('Sales Order Processing', 'Central/Local').then((data: any) => {
                let isLocalSystemInd: boolean = (data[0][0].RegValue === 'Local') ? true : false;
                this.setControlValue('LocalSystemInd', isLocalSystemInd);
                this.windowOnLoad();
            });
        });
    }

    private windowOnLoad(): void {
        if (!this.pageParams.isSCEnableHopewiserPAF && !this.pageParams.isSCEnableDatabasePAF) {
            this.isBtnGetAddressVisible = false;
            this.isBtnGetPremiseAddressVisible = false;
        }

        this.riTab = new RiTab(this.tab, this.utils);
        this.tab = this.riTab.tabObject;
        this.riTab.TabAdd('Main');
        this.riTab.TabAdd('Premise');
        this.riTab.TabAdd('Notes');

        this.buildMenu();

        this.isBtnGetAddressDisabled = true;
        this.isBtnGetPremiseAddressDisabled = true;
        this.disableControl('PDALeadInd', true);
        if (this.pageParams.isSCCapitalFirstLtr) this.capitalizeControls();
        this.setRequiredStatus('AddressLine5', this.pageParams.isSCAddressLine5Logical);
        this.setRequiredStatus('PremiseAddressLine5', this.pageParams.isSCAddressLine5Logical);
        this.enableDisableControls();

        if (this.pageParams.isNatAxJob) {
            this.pageTitle = 'Key Account Job Entry';
            this.setControlValue('ProspectTypeDesc', 'NatAxJob');
            this.disableControl('ProspectNumber', false);
            this.setDefaultView();
            if (this.pageParams.currentMode === MntConst.eModeNormal) {
                this.resetForm();
            }
        } else {
            this.pageTitle = 'Key Account Job Confirmation';
            this.setControlValue('ProspectTypeDesc', 'Confirm');
            this.isAddEnabled = false;
            if (this.parentMode === 'ConfirmNatAx') {
                //Fetch prospect
                this.isAddEnabled = true;
                this.riExchange.getParentAttributeValue('ProspectNumber');
                this.setControlValue('ProspectROWID', this.riExchange.getParentAttributeValue('ProspectNumberRowID'));
                this.setControlValue('ProspectNumber', this.riExchange.getParentAttributeValue('ProspectNumber'));
                this.fetchRecord();
            }
        }

        this.utils.setTitle(this.pageTitle);
        this.isConfirmButtonsVisible = !this.pageParams.isNatAxJob;
        this.setControlValue('LoggedInBranchNumber', this.utils.getBranchCode());
        this.requireFields();
    }

    private buildMenu(): void {
        this.addMenuOption('NatAxJobServiceCover', 'Service Cover');

        if (!this.pageParams.isNatAxJob) {
            this.addMenuOption('GoToJob', 'Job');
        }
    }

    private resetForm(): void {
        this.uiForm.reset();
        this.dropdownConfig.businessOrigin.active = {
            id: '',
            text: ''
        };
        this.setControlValue('ProspectTypeDesc', 'NatAxJob');
        this.setControlValue('LoggedInBranchNumber', this.utils.getBranchCode());
    }

    private addMenuOption(value: string, text: string): void {
        let option: Object = {
            value: value,
            text: text
        };

        this.menuArray.push(option);
    }

    private capitalizeControls(): void {
        let controlArray: Array<any> = ['Name', 'AddressLine1', 'AddressLine2', 'AddressLine3', 'AddressLine4', 'AddressLine5', 'ContactName',
            'PremiseName', 'PremiseAddressLine1', 'PremiseAddressLine2', 'PremiseAddressLine3', 'PremiseAddressLine4', 'PremiseAddressLine5', 'PremiseContactName'];

        for (let control in this.controls) {
            if (control) {
                let controlName = this.controls[control]['name'];
                if (controlArray.indexOf(controlName) > -1) {
                    this.controls[control]['type'] = MntConst.eTypeText;
                }
            }
        }
    }

    private enableDisableControls(): void {
        if (this.pageParams.currentMode === MntConst.eModeAdd || this.pageParams.currentMode === MntConst.eModeUpdate) {
            this.isSaveDisabled = false;
            this.isBtnGetAddressDisabled = false;
            this.isBtnGetPremiseAddressDisabled = false;
            for (let control in this.controls) {
                if (control) {
                    this.disableControl(this.controls[control]['name'], this.controls[control]['disabled']);
                }
            }
            this.disableControl('ProspectNumber', true);
            if (this.pageParams.currentMode === MntConst.eModeAdd) {
                this.disableControl('ContractNumber', true);
                this.disableControl('PremiseNumber', true);
                this.disableControl('OptionMenu', true);
            } else {
                this.disableControl('OptionMenu', false);
            }
            this.dropdownConfig.businessOrigin.isDisabled = false;
        }
        else {
            this.disableControl('ProspectNumber', false);
            this.disableControls(['ProspectNumber']);
            this.isSaveDisabled = true;
            this.isBtnGetAddressDisabled = true;
            this.isBtnGetPremiseAddressDisabled = true;
            this.dropdownConfig.businessOrigin.isDisabled = true;
        }

    }

    private requireFields(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');

        let bodyParams: any = {};
        bodyParams[this.serviceConstants.Function] = 'RequireExpiryDate';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.pageParams.isContractExpiryDateRequired = (data.RequireExpiryDate.trim() === 'yes') ? true : false;
                this.setRequiredStatus('ContractExpiryDate', this.pageParams.isContractExpiryDateRequired);
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private fetchRecord(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set('ProspectNumber', this.getControlValue('ProspectNumber'));
        searchParams.set(this.serviceConstants.Action, '0');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.isRecordSelected = true;
                for (let key in data) {
                    if (key === 'AddressLIne3') {
                        this.setControlValue('AddressLine3', data['AddressLIne3']);
                    } else {
                        this.setControlValue(key, data[key]);
                    }
                }
                if (this.pageParams.isNatAxJob) {
                    this.afterFetch();
                } else {
                    this.pageParams.currentMode = MntConst.eModeUpdate;
                    this.enableDisableControls();
                    this.lookupDescriptions();
                    if (!this.getControlValue('ConvertedToNumber')) {
                        this.isConvertedToNumberVisible = false;
                        this.isConfirmButtonsVisible = true;
                    } else {
                        this.isConvertedToNumberVisible = true;
                        this.isConfirmButtonsVisible = false;
                    }
                }

                //Set Ellipsis Value
                this.ellipsisConfig.contract.childConfigParams.accountNumber = data.AccountNumber;
                this.ellipsisConfig.invoiceGroup.childConfigParams.AccountNumber = data.AccountNumber;
                this.ellipsisConfig.premise.childConfigParams.ContractNumber = data.ContractNumber;
                this.ellipsisConfig.premise.childConfigParams.AccountNumber = data.AccountNumber;
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private lookupDescriptions(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let lookup_details = [{
            'table': 'ProspectStatusLang',
            'query': {
                'LanguageCode': this.riExchange.LanguageCode(),
                'ProspectStatusCode': this.getControlValue('ProspectStatusCode')
            },
            'fields': ['ProspectStatusDesc']
        }, {
            'table': 'BusinessOriginLang',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'LanguageCode': this.riExchange.LanguageCode(),
                'BusinessOriginCode': this.getControlValue('BusinessOriginCode')
            },
            'fields': ['BusinessOriginCode', 'BusinessOriginDesc']
        }, {
            'table': 'ContactMediumLang',
            'query': {
                'LanguageCode': this.riExchange.LanguageCode(),
                'ContactMediumCode': this.getControlValue('ContactMediumCode')
            },
            'fields': ['ContactMediumDesc']
        }, {
            'table': 'PaymentType',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'PaymentTypeCode': this.getControlValue('PaymentTypeCode')
            },
            'fields': ['PaymentDesc']
        }, {
            'table': 'CustomerTypeLanguage',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'LanguageCode': this.riExchange.LanguageCode(),
                'CustomerTypeCode': this.getControlValue('CustomerTypeCode')
            },
            'fields': ['CustomerTypeDesc']
        }, {
            'table': 'Branch',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'BranchNumber': this.getControlValue('BranchNumber')
            },
            'fields': ['BranchName']
        }, {
            'table': 'InvoiceGroup',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'AccountNumber': this.getControlValue('AccountNumber'),
                'InvoiceGroupNumber': this.getControlValue('InvoiceGroupNumber')
            },
            'fields': ['InvoiceGroupDesc']
        }, {
            'table': 'Employee',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'EmployeeCode': this.getControlValue('NegotiatingSalesEmployeeCode')
            },
            'fields': ['EmployeeSurname']
        }, {
            'table': 'Employee',
            'query': {
                'BusinessCode': this.utils.getBusinessCode(),
                'EmployeeCode': this.getControlValue('ServicingSalesEmployeeCode')
            },
            'fields': ['EmployeeSurname']
        }];

        this.LookUp.lookUpRecord(lookup_details).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.length) {
                    let prospecteData = data[0][0];
                    let businessOriginData = data[1][0];
                    let contactMediumData = data[2][0];
                    let paymentTypeData = data[3][0];
                    let customerTypeData = data[4][0];
                    let branchData = data[5][0];
                    let invoiceGroupData = data[6][0];
                    let negSalesEmpData = data[7][0];
                    let servicingSalesData = data[8][0];

                    this.dropdownConfig.businessOrigin.active = {
                        id: '',
                        text: ''
                    };

                    if (prospecteData) {
                        this.setControlValue('ProspectStatusDesc', prospecteData.ProspectStatusDesc);
                    }
                    if (businessOriginData) {
                        this.dropdownConfig.businessOrigin.active = {
                            id: businessOriginData.BusinessOriginCode,
                            text: businessOriginData.BusinessOriginCode + ' - ' + businessOriginData.BusinessOriginDesc
                        };
                        this.setControlValue('BusinessOriginDesc', businessOriginData.BusinessOriginDesc);
                    }
                    if (contactMediumData) {
                        this.setControlValue('ContactMediumDesc', contactMediumData.ContactMediumDesc);
                    }
                    if (paymentTypeData) {
                        this.setControlValue('PaymentDesc', paymentTypeData.PaymentDesc);
                    }
                    if (customerTypeData) {
                        this.setControlValue('CustomerTypeDesc', customerTypeData.CustomerTypeDesc);
                    }
                    if (branchData) {
                        this.setControlValue('BranchName', branchData.BranchName);
                    }
                    if (invoiceGroupData) {
                        this.setControlValue('InvoiceGroupDesc', invoiceGroupData.InvoiceGroupDesc);
                    }
                    if (negSalesEmpData) {
                        this.setControlValue('NegotiatingSalesEmployeeSurname', negSalesEmpData.EmployeeSurname);
                    }
                    if (servicingSalesData) {
                        this.setControlValue('ServicingSalesEmployeeSurname', servicingSalesData.EmployeeSurname);
                    }
                    if (this.pageParams.currentMode === MntConst.eModeUpdate) {
                        this.storeData();
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private afterFetch(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');

        let bodyParams: any = {
            'ProspectNumber': this.getControlValue('ProspectNumber'),
            'LoggedInBranchNumber': this.getControlValue('LoggedInBranchNumber')
        };
        bodyParams[this.serviceConstants.Function] = 'CheckValidProspect,GetDefaultValues';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);

                if (this.pageParams.isNatAxJob) {
                    if (!this.getControlValue('ConvertedToNumber')) {
                        this.isConvertedToNumberVisible = false;
                        this.isConfirmButtonsVisible = false;
                    } else {
                        this.isConvertedToNumberVisible = true;
                        this.isConfirmButtonsVisible = false;
                    }
                }
                else {
                    if (!this.getControlValue('ConvertedToNumber')) {
                        this.isConvertedToNumberVisible = false;
                        this.isConfirmButtonsVisible = true;
                    } else {
                        this.isConvertedToNumberVisible = true;
                        this.isConfirmButtonsVisible = false;
                    }
                }
                this.lookupDescriptions();
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }

                this.pageParams.currentMode = MntConst.eModeUpdate;
                this.setControlValue('AnnualValue', data.AnnualValue);
                this.setControlValue('NegBranchNumber', data.NegBranchNumber);
                this.setControlValue('ServiceBranchNumber', data.ServiceBranchNumber);

                this.ellipsisConfig.negSalesEmployee.childConfigParams.negativeBranchNumber = data.NegBranchNumber;
                this.enableDisableControls();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private afterSaveUpdate(data: any): void {
        if (data.ErrorMessageDesc) {
            let modalObj: ICabsModalVO = new ICabsModalVO(data.ErrorMessageDesc);
            modalObj.closeCallback = this.closePage.bind(this);
            this.modalAdvService.emitMessage(modalObj);
        } else {
            this.closePage();
        }
    }

    private closePage(): void {
        if (this.getControlValue('ConvertedToNumber')) {
            this.isConvertedToNumberVisible = true;
            this.isConfirmButtonsVisible = false;

            if (!this.pageParams.isNatAxJob) {
                this.location.back();
            } else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
            }
        }
        else if (!this.pageParams.isNatAxJob) {
            this.isConvertedToNumberVisible = false;
            this.isConfirmButtonsVisible = true;
            this.location.back();
        }
        else {
            this.isConvertedToNumberVisible = false;
            this.isConfirmButtonsVisible = false;
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
        }
    }

    private beforeSaveAdd(): void {
        //QRS check contract is valid for this account
        if (this.getControlValue('ContractNumber') && this.getControlValue('AccountNumber')) {
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');

            let bodyParams: any = {};
            bodyParams[this.serviceConstants.Function] = 'CheckContractForAccount';
            bodyParams[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
            bodyParams[this.serviceConstants.AccountNumber] = this.getControlValue('AccountNumber');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }
                    //Display error message
                    if (data.ErrorMessageDesc) {
                        this.modalAdvService.emitMessage(new ICabsModalVO(data.ErrorMessageDesc));
                        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'ContractNumber', true);
                        this.contract.nativeElement.focus();
                    } else {
                        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveRecord.bind(this)));
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        } else {
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveRecord.bind(this)));
        }
    }

    private afterSaveAdd(): void {
        this.isProspectStatusRequired = true;
        this.setRequiredStatus('ProspectStatusCode', true); // Hidden field on Add
        this.navigate('Prospect', InternalMaintenanceServiceModuleRoutes.ICABSCMNATAXJOBSERVICECOVERMAINTENANCE);
        this.disableControl('PDALeadInd', true);
    }

    private saveRecord(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        let bodyParams: any = {};

        if (this.pageParams.currentMode === MntConst.eModeAdd) {
            searchParams.set(this.serviceConstants.Action, '1');
            bodyParams[this.serviceConstants.Function] = 'CheckContractForAccount';
        } else {
            searchParams.set(this.serviceConstants.Action, '2');
            bodyParams[this.serviceConstants.Function] = 'CheckValidProspect,GetDefaultValues';
        }

        let checkBoxList: Array<any> = ['chkAuthorise', 'chkReject', 'chkBranch', 'CreateNewInvoiceGroupInd'];
        let saveControls: Array<any> = [
            'ProspectROWID', 'ProspectNumber', 'Name', 'AddressLine1', 'AddressLine2', 'AddressLine3', 'AddressLine4', 'AddressLine5', 'Postcode',
            'ContactName', 'ContactPosition', 'ContactTelephone', 'ContactMobile', 'ContactFax', 'ContactEmail', 'BusinessOriginCode', 'ContactMediumCode',
            'PDALeadEmployeeCode', 'AccountNumber', 'ContractNumber', 'PremiseNumber', 'CommenceDate', 'ContractExpiryDate', 'PaymentTypeCode', 'CustomerTypeCode',
            'BranchNumber', 'InvoiceGroupNumber', 'NegotiatingSalesEmployeeCode', 'ServicingSalesEmployeeCode', 'PurchaseOrderNumber', 'ClientReference',
            'PremiseName', 'PremiseAddressLine1', 'PremiseAddressLine2', 'PremiseAddressLine3', 'PremiseAddressLine4', 'PremiseAddressLine5', 'PremisePostcode',
            'PremiseContactName', 'PremiseContactPosition', 'PremiseContactTelephone', 'Narrative', 'ProspectStatusCode', 'ConvertedToNumber', 'NegBranchNumber',
            'ServiceBranchNumber', 'SalesBranchNumber', 'LoggedInBranchNumber', 'ProspectTypeDesc', 'CreateNewInvoiceGroupInd', 'AnnualValue',
            'chkAuthorise', 'chkReject', 'chkBranch'];

        //These control are not used when in add mode
        let skipControls = ['ProspectROWID', 'chkAuthorise', 'chkReject', 'chkBranch'];

        for (let i = 0; i < saveControls.length; i++) {
            if ((skipControls.indexOf(saveControls[i]) > -1) && this.pageParams.currentMode === MntConst.eModeAdd) continue;

            if (checkBoxList.indexOf(saveControls[i]) > -1) {
                bodyParams[saveControls[i]] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue(saveControls[i]));
            } else {
                bodyParams[saveControls[i]] = this.getControlValue(saveControls[i]);
            }
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage && data.fullError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.formPristine();
                if (this.pageParams.currentMode === MntConst.eModeAdd) {
                    this.pageParams.currentMode = MntConst.eModeUpdate;
                    this.setControlValue('ProspectNumber', data.ProspectNumber);
                    this.setControlValue('ProspectROWID', data.Prospect);
                    this.afterSaveAdd();
                } else {
                    this.afterSaveUpdate(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private afterSave(): void {
        this.isBtnGetAddressDisabled = true;
        this.isBtnGetPremiseAddressDisabled = true;
    }

    private afterCancel(): void {
        this.isBtnGetAddressDisabled = true;
        this.isBtnGetPremiseAddressDisabled = true;
        this.isProspectStatusRequired = true;
        this.setRequiredStatus('ProspectStatusCode', true); // Hidden field on Add
        this.disableControl('PDALeadInd', true);
        if (this.getControlValue('PDALeadEmployeeCode') === '') {
            this.setControlValue('PDALeadInd', false);
        }
    }

    private accountNumberOnChange(): void {
        this.disableControl('ContractNumber', true);
        this.disableControl('PremiseNumber', true);
        if (!this.getControlValue('AccountNumber')) {
            this.setControlValue('ContractNumber', '');
            this.setControlValue('PremiseNumber', '');
            this.ellipsisConfig.contract.childConfigParams.accountNumber = '';
            this.ellipsisConfig.invoiceGroup.childConfigParams.AccountNumber = '';
        } else {
            this.ellipsisConfig.contract.childConfigParams.accountNumber = this.getControlValue('AccountNumber');
            this.ellipsisConfig.invoiceGroup.childConfigParams.AccountNumber = this.getControlValue('AccountNumber');
            this.ellipsisConfig.premise.childConfigParams.AccountNumber = this.getControlValue('AccountNumber');
            this.ellipsisConfig.premise.component = AccountPremiseSearchComponent;

            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');

            let bodyParams: any = {};
            bodyParams[this.serviceConstants.Function] = 'GetAccountDetails';
            bodyParams[this.serviceConstants.AccountNumber] = this.getControlValue('AccountNumber');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.setControlValue('ContractNumber', '');
                        this.setControlValue('PremiseNumber', '');
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }

                    this.disableControl('ContractNumber', false);
                    this.disableControl('PremiseNumber', false);
                    this.ellipsisConfig.contract.childConfigParams.accountNumber = this.getControlValue('AccountNumber');
                    for (let key in data) {
                        if (key) {
                            this.setControlValue(key, data[key]);
                        }
                    }

                    let isChecked = data.CreateNewInvoiceGroupInd === 'yes' ? true : false;
                    if (isChecked) {
                        this.setControlValue('InvoiceGroupNumber', '');
                    }
                    this.disableControl('InvoiceGroupNumber', isChecked);
                    this.setRequiredStatus('InvoiceGroupNumber', !isChecked);

                    //Display error message
                    if (data.ErrorMessageDesc) {
                        this.modalAdvService.emitMessage(new ICabsModalVO(data.ErrorMessageDesc));
                        this.account.nativeElement.focus();
                    }
                    else {
                        if (this.getControlValue('AddressLine1') && !this.getControlValue('PremiseNumber')) {
                            //Call to select Premise to be serviced
                            this.ellipsisConfig.accountPremise.childConfigParams.AccountNumber = this.getControlValue('AccountNumber');
                            this.premiseSearch();
                        }
                    }
                    this.ellipsisConfig.negSalesEmployee.childConfigParams.negativeBranchNumber = data.NegBranchNumber;
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    private premiseSearch(): void {
        if (!this.getControlValue('ContractNumber')) {
            setTimeout(() => {
                this.premiseSearchModal.openModal();
            }, 100);
        } else {
            this.premiseModal.openModal();
        }
    }

    private contractPremiseOnChange(): void {
        if (!this.getControlValue('ContractNumber')) {
            this.setControlValue('PremiseNumber', '');
            this.ellipsisConfig.premise.childConfigParams.ContractNumber = '';
            this.ellipsisConfig.premise.component = AccountPremiseSearchComponent;
        } else {
            this.ellipsisConfig.premise.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
            this.ellipsisConfig.premise.component = PremiseSearchComponent;
        }

        if (this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber')) {
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');

            let bodyParams: any = {};
            bodyParams[this.serviceConstants.Function] = 'GetPremiseDetails';
            bodyParams[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
            bodyParams[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
            bodyParams['BranchNumber'] = this.getControlValue('BranchNumber');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        setTimeout(() => {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        }, 1000);
                        return;
                    }

                    if (data.ErrorMessageDesc) {
                        this.modalAdvService.emitMessage(new ICabsModalVO(data.ErrorMessageDesc));
                        this.premise.nativeElement.focus();
                    }

                    for (let key in data) {
                        if (key) {
                            this.setControlValue(key, data[key]);
                        }
                    }

                    let isChecked = data.CreateNewInvoiceGroupInd === 'yes' ? true : false;
                    if (isChecked) {
                        this.setControlValue('InvoiceGroupNumber', '');
                        this.setControlValue('InvoiceGroupDesc', '');
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    public premisePostCodeOnChange(): void {
        if (this.getControlValue('PremisePostcode')) {
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');

            let bodyParams: any = {
                PremisePostcode: this.getControlValue('PremisePostcode'),
                PremiseAddressLine4: this.getControlValue('PremiseAddressLine4'),
                PremiseAddressLine5: this.getControlValue('PremiseAddressLine5')
            };
            bodyParams[this.serviceConstants.Function] = 'GetSalesDetails';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }
                    this.setControlValue('ServicingSalesEmployeeCode', data.ServicingSalesEmployeeCode);
                    this.setControlValue('ServicingSalesEmployeeSurname', data.ServicingSalesEmployeeSurname);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    private onSMSMessageChange(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');

        let bodyParams: any = {};
        bodyParams[this.serviceConstants.Function] = 'CleanSMSMessage';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.setControlValue('SMSMessage', data.SMSMessage);
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private cmdGetAddressOnClick(): void {
        if (this.pageParams.isSCEnableHopewiserPAF && !this.pageParams.isSCEnableDatabasePAF) {
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.PageNotDeveloped + ' riMPAFSearch', null));
        }
        else if (this.pageParams.isSCEnableDatabasePAF) {
            this.ellipsisConfig.postCode.childConfigParams.AddressLine4 = this.getControlValue('AddressLine4');
            this.ellipsisConfig.postCode.childConfigParams.AddressLine5 = this.getControlValue('AddressLine5');
            this.ellipsisConfig.postCode.childConfigParams.PostCode = this.getControlValue('Postcode');
            this.postCodeEllipsis.openModal();
        }
    }

    private cmdGetPremiseAddressOnClick(): void {
        if (this.pageParams.isSCEnableHopewiserPAF && !this.pageParams.isSCEnableDatabasePAF) {
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.PageNotDeveloped + ' riMPAFSearch', null));
        }
        if (this.pageParams.isSCEnableDatabasePAF) {
            this.ellipsisConfig.premisePostCode.childConfigParams.town = this.getControlValue('PremiseAddressLine4');
            this.ellipsisConfig.premisePostCode.childConfigParams.state = this.getControlValue('PremiseAddressLine5');
            this.ellipsisConfig.premisePostCode.childConfigParams.postCode = this.getControlValue('PremisePostcode');
            this.premisePostCodeEllipsis.openModal();
        }
    }

    public getExpiryDate(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');

        let bodyParams: any = {};
        bodyParams[this.serviceConstants.Function] = 'GetExpiryDate';
        bodyParams['CommenceDate'] = this.getControlValue('CommenceDate');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.setControlValue('ContractExpiryDate', data.ContractExpiryDate);
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private setDefaultView(): void {
        this.dropdownConfig.businessOrigin.active = {
            id: '',
            text: ''
        };
        this.isAddEnabled = false;
        this.isProspectStatusVisible = true;
        this.isAnnualValueVisible = true;
        this.isConvertedToNumberVisible = true;
        this.ellipsisConfig.invoiceGroup.childConfigParams.AccountNumber = '';
        this.ellipsisConfig.contract.childConfigParams.accountNumber = '';
        this.ellipsisConfig.premise.childConfigParams.ContractNumber = '';
    }

    private storeData(): void {
        for (let control in this.controls) {
            if (control) {
                let controlName: string = this.controls[control]['name'];
                this.pageFormData[controlName] = this.getControlValue(controlName);
            }
        }
    }

    private restoreData(): void {
        for (let control in this.controls) {
            if (control) {
                let controlName: string = this.controls[control]['name'];
                this.setControlValue(controlName, this.pageFormData[controlName]);
            }
        }

        this.dropdownConfig.businessOrigin.active = {
            id: this.getControlValue('BusinessOriginCode') ? this.getControlValue('BusinessOriginCode') : '',
            text: this.getControlValue('BusinessOriginCode') ? this.getControlValue('BusinessOriginCode') + ' - ' + this.getControlValue('BusinessOriginDesc') : ''
        };
    }

    /************************************************************************************************
     * Public Methods
     ************************************************************************************************/

    /**
     * Called When in addMode
     */
    public beforeAdd(): void {
        this.isAnnualValueVisible = false;
        this.isProspectStatusVisible = false;
        this.isProspectStatusRequired = false;
        this.utils.makeTabsNormal();
        this.setRequiredStatus('ProspectStatusCode', false); // Hidden field on Add

        this.isBtnGetAddressDisabled = false;
        this.isBtnGetPremiseAddressDisabled = false;

        this.account.nativeElement.focus();
        this.riTab.TabFocus(1);
        let branchCode: string = this.utils.getBranchCode();
        let branchName: string = this.utils.getBranchText(branchCode).split(' - ')[1];
        this.setControlValue('ServiceBranchNumber', branchCode);
        this.setControlValue('BranchNumber', branchCode);
        this.setControlValue('BranchName', branchName);
        this.setControlValue('OptionMenu', '');
        this.disableControl('OptionMenu', true);
    }

    public onCancelClick(): void {
        if (this.pageParams.currentMode === MntConst.eModeAdd) {
            this.resetForm();
            this.setDefaultView();
            this.pageParams.currentMode = MntConst.eModeNormal;
            this.enableDisableControls();
            this.ellipsisConfig.prospect.autoOpen = true;
        }
        else if (this.pageParams.currentMode === MntConst.eModeUpdate) {
            this.restoreData();
            this.disableControl('ContractNumber', false);
            this.disableControl('PremiseNumber', false);
        }
        this.formPristine();
        this.utils.makeTabsNormal();
        this.setControlValue('OptionMenu', '');
    }

    /**
     * Fetches all ellipsis data
     */
    public fetchEllipsisData(data: any, type: string): void {
        switch (type) {
            case 'prospect':
                if (data.AddMode) {
                    this.resetForm();
                    this.isRecordSelected = false;
                    this.isAddEnabled = true;
                    this.pageParams.currentMode = MntConst.eModeAdd;
                    this.enableDisableControls();
                    this.beforeAdd();
                } else {
                    this.setControlValue('ProspectNumber', data.ProspectNumber);
                    this.fetchRecord();
                }
                break;
            case 'account':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'AccountNumber');
                this.setControlValue('AccountNumber', data.AccountNumber);
                this.accountNumberOnChange();
                this.ellipsisConfig.contract.childConfigParams.accountNumber = data.AccountNumber;
                break;
            case 'accountPremise':
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('PremiseNumber', data.PremiseNumber);
                this.contractPremiseOnChange();
                this.ellipsisConfig.premise.childConfigParams.ContractNumber = data.ContractNumber;
                break;
            case 'contract':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ContractNumber');
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.ellipsisConfig.premise.childConfigParams.ContractNumber = data.ContractNumber;
                this.ellipsisConfig.premise.component = PremiseSearchComponent;
                break;
            case 'premise':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'PremiseNumber');
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('PremiseNumber', data.PremiseNumber);
                this.contractPremiseOnChange();
                break;
            case 'postCode':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'Postcode');
                this.setControlValue('AddressLine4', data.AddressLine4);
                this.setControlValue('AddressLine5', data.AddressLine5);
                this.setControlValue('Postcode', data.Postcode);
                this.contractPremiseOnChange();
                break;
            case 'premisePostCode':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'PremisePostcode');
                this.setControlValue('PremiseAddressLine4', data.PremiseAddressLine4);
                this.setControlValue('PremiseAddressLine5', data.PremiseAddressLine5);
                this.setControlValue('PremisePostcode', data.PremisePostcode);
                this.contractPremiseOnChange();
                break;
            case 'contactMediumLang':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ContactMediumCode');
                this.setControlValue('ContactMediumCode', data.ContactMediumCode);
                this.setControlValue('ContactMediumDesc', data.ContactMediumDesc);
                break;
            case 'assignToEmployee':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'AssignToEmployeeCode');
                this.setControlValue('AssignToEmployeeCode', data.EmployeeCode);
                this.setControlValue('AssignToEmployeeName', data.EmployeeSurname);
                break;
            case 'PDALeadEmployee':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'PDALeadEmployeeCode');
                this.setControlValue('PDALeadEmployeeCode', data.EmployeeCode);
                this.setControlValue('PDALeadEmployeeSurname', data.EmployeeSurname);
                break;
            case 'paymentType':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'PaymentTypeCode');
                this.setControlValue('PaymentTypeCode', data.PaymentTypeCode);
                this.setControlValue('PaymentDesc', data.PaymentDesc);
                break;
            case 'customerType':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'CustomerTypeCode');
                this.setControlValue('CustomerTypeCode', data.CustomerTypeCode);
                this.setControlValue('CustomerTypeDesc', data.CustomerTypeDesc);
                break;
            case 'invoiceGroup':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'InvoiceGroupNumber');
                this.setControlValue('InvoiceGroupNumber', data.InvoiceGroupNumber);
                this.setControlValue('InvoiceGroupDesc', data.InvoiceGroupDesc);
                break;
            case 'negSalesEmployee':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'NegotiatingSalesEmployeeCode');
                this.setControlValue('NegotiatingSalesEmployeeCode', data.NegotiatingSalesEmployeeCode);
                this.setControlValue('NegotiatingSalesEmployeeSurname', data.NegotiatingSalesEmployeeSurname);
                break;
            case 'serviceSalesEmployee':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ServicingSalesEmployeeCode');
                this.setControlValue('ServicingSalesEmployeeCode', data.ContractOwner);
                this.setControlValue('ServicingSalesEmployeeSurname', data.ContractOwnerSurname);
                break;
            case 'prospectStatus':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ProspectStatusCode');
                this.setControlValue('ProspectStatusCode', data.ProspectStatusCode);
                this.setControlValue('ProspectStatusDesc', data.ProspectStatusDesc);
                break;
        }
    }

    public onBusinessOriginReceived(data: any): void {
        this.uiForm.markAsDirty();
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'BusinessOriginCode');
        this.dropdownConfig.businessOrigin.active.id = data['BusinessOriginLang.BusinessOriginCode'];
        this.setControlValue('BusinessOriginCode', data['BusinessOriginLang.BusinessOriginCode']);
        this.setControlValue('BusinessOriginDesc', data['BusinessOriginLang.BusinessOriginDesc']);
    }

    /**
     * Fires when input field value changes
     */
    public onChangeControl(event: any): void {
        let control = event.target.id;
        switch (control) {
            case 'ProspectNumber': this.fetchRecord();
                break;
            case 'AccountNumber': this.accountNumberOnChange();
                break;
            case 'ContractNumber': this.contractPremiseOnChange();
                break;
            case 'PremiseNumber': this.contractPremiseOnChange();
                break;
            case 'SMSMessage': this.onSMSMessageChange();
                break;
        }
    }

    public addressLine4OnBlur(): void {
        if (this.getControlValue('AddressLine4')) this.setControlValue('AddressLine4', this.getControlValue('AddressLine4').trim());
        if (this.pageParams.isSCAddressLine4Required && !this.getControlValue('AddressLine4')) {
            this.cmdGetAddressOnClick();
        }
    }

    public addressLine5OnBlur(): void {
        if (this.getControlValue('AddressLine5')) this.setControlValue('AddressLine5', this.getControlValue('AddressLine5').trim());
        if (this.pageParams.isSCAddressLine5Required && !this.getControlValue('AddressLine5')) {
            this.cmdGetAddressOnClick();
        }
    }

    public postcodeOnBlur(): void {
        if (this.getControlValue('Postcode')) this.setControlValue('Postcode', this.getControlValue('Postcode').trim());
        if (this.pageParams.isSCPostCodeRequired && !this.getControlValue('Postcode')) {
            this.cmdGetAddressOnClick();
        }
    }

    public premiseAddressLine4OnBlur(): void {
        if (this.getControlValue('PremiseAddressLine4')) this.setControlValue('PremiseAddressLine4', this.getControlValue('PremiseAddressLine4').trim());
        if (this.pageParams.isSCAddressLine4Required && !this.getControlValue('PremiseAddressLine4')) {
            this.cmdGetPremiseAddressOnClick();
        }
    }

    public premiseAddressLine5OnBlur(): void {
        if (this.getControlValue('PremiseAddressLine5')) this.setControlValue('PremiseAddressLine5', this.getControlValue('PremiseAddressLine5').trim());
        if (this.pageParams.isSCAddressLine5Required && !this.getControlValue('PremiseAddressLine5')) {
            this.cmdGetPremiseAddressOnClick();
        }
    }

    public premisePostcodeOnBlur(): void {
        if (this.getControlValue('PremisePostcode')) this.setControlValue('PremisePostcode', this.getControlValue('PremisePostcode').trim());
        if (this.pageParams.isSCPostCodeRequired && !this.getControlValue('PremisePostcode')) {
            this.cmdGetPremiseAddressOnClick();
        }
    }

    public confirmSave(): void {
        this.dropdownConfig.businessOrigin.triggerValidate = true;
        this.utils.highlightTabs();
        if (!this.riExchange.validateForm(this.uiForm)) return;

        this.lookupDescriptions();
        this.riTab.TabFocus(1);
        if (this.pageParams.currentMode === MntConst.eModeAdd) {
            this.beforeSaveAdd();
        } else {
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveRecord.bind(this)));
        }
    }

    public onCheckboxSelect(event: any): void {
        this.setControlValue('chkAuthorise', false);
        this.setControlValue('chkReject', false);
        this.setControlValue('chkBranch', false);
        this.setControlValue(event.target.id, true);
    }

    public onOptionChange(data: string): void {
        if (this.isRecordSelected) {
            switch (data) {
                case 'NatAxJobServiceCover':
                    this.setControlValue('OptionMenu', '');
                    this.navigate('Prospect', InternalSearchModuleRoutes.ICABSACMNATAXJOBSERVICECOVERSEARCH);
                    break;
                case 'GoToJob':
                    this.setControlValue('OptionMenu', '');
                    if (this.getControlValue('ConvertedToNumber')) {
                        this.navigate('Prospect', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE,
                            {
                                ContractNumber: this.getControlValue('ConvertedToNumber'),
                                currentContractType: 'J'
                            });
                    }
                    break;
            }
            this.riExchange.riInputElement.MarkAsPristine(this.uiForm, 'OptionMenu');
        }
        else {
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.NoRecordSelected));
        }
    }

    public focusSave(obj: any): void {
        this.riTab.focusNextTab(obj);
    }
}
