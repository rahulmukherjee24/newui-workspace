import { OnInit, Injector, Component, ViewChild } from '@angular/core';

import { CommonMaintenanceFunction } from '@app/base/CommonMaintenanceFunction';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import {
    EllipsisGenericComponent,
    IGenericEllipsisControl,
    GenericEllipsisEvent
} from '@shared/components/ellipsis-generic/ellipsis-generic';
import { HTTP_METHOD, RestService } from '@shared/services/rest-service';
import { HttpRequestOptions, HttpServiceUrl } from '@app/base/httpRequestOptions';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst, RiTab } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { RouteAwayComponent } from '@shared/components/route-away/route-away';

@Component({
    templateUrl: 'iCABSBDepotMaintenance.html'
})

export class DepotMaintenanceComponent extends LightBaseComponent implements OnInit {

    @ViewChild('depotSearchEllipsis') depotSearchEllipsis: EllipsisGenericComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;

    public controls: IControls[] = [
        { name: 'DepotNumber', type: MntConst.eTypeInteger, required: true },
        { name: 'DepotName', type: MntConst.eTypeText, required: true },
        { name: 'DepotAddressLine1', type: MntConst.eTypeText, required: true },
        { name: 'DepotAddressLine2', type: MntConst.eTypeText },
        { name: 'DepotAddressLine3', type: MntConst.eTypeText },
        { name: 'DepotAddressLine4', type: MntConst.eTypeText, required: true },
        { name: 'DepotAddressLine5', type: MntConst.eTypeText },
        { name: 'DepotPostCode', type: MntConst.eTypeText, required: true },
        { name: 'WasteManagementLicenseNumber', type: MntConst.eTypeTextFree, required: true },
        { name: 'DepotContactTelephone', type: MntConst.eTypeText, required: true },
        { name: 'DepotContactEmail', type: MntConst.eTypeText },
        { name: 'DepotContactFax', type: MntConst.eTypeText },
        { name: 'DepotContactName', type: MntConst.eTypeText, required: true },
        { name: 'DepotContactPosition', type: MntConst.eTypeText, required: true },
        { name: 'ConsignorAddressText', type: MntConst.eTypeText },
        { name: 'WasteChemicalPercentageText1', type: MntConst.eTypeText },
        { name: 'WasteChemicalPercentageText2', type: MntConst.eTypeText },
        { name: 'WasteChemicalText1', type: MntConst.eTypeText },
        { name: 'WasteChemicalText2', type: MntConst.eTypeText },
        { name: 'WasteColourText', type: MntConst.eTypeText },
        { name: 'WasteDescriptionText', type: MntConst.eTypeText },
        { name: 'WasteEWCCodesText1', type: MntConst.eTypeText },
        { name: 'WasteEWCCodesText2', type: MntConst.eTypeText },
        { name: 'WasteEWCCodesText3', type: MntConst.eTypeText },
        { name: 'WasteHazardList', type: MntConst.eTypeText },
        { name: 'WasteManagementOperationText', type: MntConst.eTypeText },
        { name: 'WasteProcessDescription', type: MntConst.eTypeText },
        { name: 'WasteOperationCode', type: MntConst.eTypeText },
        { name: 'GpsXCoordinate', type: MntConst.eTypeText },
        { name: 'GpsYCoordinate', type: MntConst.eTypeText },
        { name: 'NumberOfBays', type: MntConst.eTypeText },
        { name: 'ShadowTime', type: MntConst.eTypeTime },
        { name: 'DepotOpeningTime', type: MntConst.eTypeTime, disabled: true },
        { name: 'DepotClosingTime', type: MntConst.eTypeTime, disabled: true }
    ];

    public pageId: string;
    public depotSearchConfig: IGenericEllipsisControl = {
        autoOpen: true,
        ellipsisTitle: 'Depot Search',
        configParams: {
            table: 'Depot',
            shouldShowAdd: true
        },
        tableColumn: [
            { 'title': 'Number', 'name': 'DepotNumber' },
            { 'title': 'Name', 'name': 'DepotName' }
        ],
        disable: false
    };
    public MntConst = MntConst;
    public tab: any;
    public riTab: RiTab;
    private tabList: any;
    private translatedMessageList = {
        'Depot_Details': 'Depot Details',
        'Waste_Information': 'Waste Information'
    };

    public isDisabled: boolean = true;
    private tempDepotData: Record<string, string>;
    private headerParams: IXHRParams = {
        operation: 'Business/iCABSBDepotMaintenance',
        module: 'service',
        method: 'service-delivery/admin'
    };
    public commonMaintenanceFunction: CommonMaintenanceFunction;
    public requestOptions: HttpRequestOptions;

    constructor(
        private injector: Injector,
        private restService: RestService
    ) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBDEPOTMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Depot Maintenance';
        this.commonMaintenanceFunction = new CommonMaintenanceFunction(this, injector);
        this.pageParams = {
            pageMode: MntConst.eModeSelect
        };
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.zone.run(() => {
            this.tabDraw();
        });
        this.disableControls(['DepotNumber']);
    }

    public onBtnClick(action: string): void {
        switch (action) {
            case 'Save':
                this.commonMaintenanceFunction.onSaveClick(
                    () => { this.onBtnClick('PromptConfirm'); }, null, true, this.tabList
                );
                break;
            case 'Cancel':
                this.commonMaintenanceFunction.onCancelClick(this.tempDepotData, () => { this.enableSelectMode(); }, true, this.tabList['tab1']['id']);
                break;
            case 'Delete':
                this.commonMaintenanceFunction.onDeleteClick(
                    () => { this.onBtnClick('PromptConfirm'); }
                );
                break;
            case 'PromptConfirm':
                let formData = {};
                if (this.pageParams.pageMode === MntConst.eModeDelete) {
                    this.commonServiceCall(3, formData, this.deleteDepotRecord.bind(this));
                } else {
                    for (const control in this.uiForm.controls) {
                        if (control) {
                            formData[control] = this.getControlValue(control);
                        }
                    }
                    this.commonServiceCall(this.pageParams.pageMode === MntConst.eModeAdd ? 1 : 2, formData, this.saveDepotRecord.bind(this));
                }
                break;
        }
    }

    public onDepotNumberChange(): void {
        const depotNumber: string = this.getControlValue('DepotNumber');
        if (depotNumber) {
            this.getSearchData(depotNumber, this.fetchDepotRecord.bind(this));
        } else {
            this.enableSelectMode(false);
        }
    }

    private fetchDepotRecord(error: any, successData: any): void {
        if (successData) {
            this.enableEditMode(successData);
        }
    }

    private saveDepotRecord(error: any, successData: any): void {
        if (successData) {
            this.commonMaintenanceFunction.focusToTab(this.tabList['tab1']['id']);
            this.enableEditMode(successData);
            this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
        }
    }

    private deleteDepotRecord(error: any, successData: any): void {
        if (error) {
            this.pageParams.pageMode = MntConst.eModeUpdate;
        } else if (successData) {
            this.commonMaintenanceFunction.focusToTab(this.tabList['tab1']['id']);
            this.displayMessage(MessageConstant.Message.RecordDeletedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
            this.enableSelectMode();
        }
    }

    private commonServiceCall(action: number | string, formData: any, callback: (error: any, success: any) => void): void {
        this.isRequesting = true;
        this.requestOptions = HttpRequestOptions
            .create(this.injector, HttpServiceUrl.DEPOT_MAINTENANCE)
            .addPathParam(this.utils.getCountryCode())
            .addPathParam(this.utils.getBusinessCode())
            .addPathParam(this.getControlValue('DepotNumber'));
        switch (action) {
            case 1:
                this.requestOptions.addMethodType(HTTP_METHOD.METHOD_TYPE_POST);
                break;
            case 2:
                this.requestOptions.addMethodType(HTTP_METHOD.METHOD_TYPE_PUT);
                break;
            case 3:
                this.requestOptions.addMethodType(HTTP_METHOD.METHOD_TYPE_DELETE);
                break;
        }
        this.requestOptions.addBodyParams(formData);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.restService.executeRequest(this.requestOptions)
            .then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.formPristine();
                if (this.hasError(data)) {
                    callback('Success with error', null);
                    this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    return;
                }
                callback(null, data);
            }).catch((error) => {
                callback(error, null);
                this.displayMessage(error);
            });
    }

    private getSearchData(depotNumber: string, callback: (error: any, success: any) => void): void {
        this.isRequesting = true;
        this.requestOptions = HttpRequestOptions
            .create(this.injector, HttpServiceUrl.DEPOT_MAINTENANCE)
            .addPathParam(this.utils.getCountryCode())
            .addPathParam(this.utils.getBusinessCode())
            .addPathParam(depotNumber)
            .addMethodType(HTTP_METHOD.METHOD_TYPE_GET);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.restService.executeRequest(this.requestOptions)
            .then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.formPristine();
                if (this.hasError(data)) {
                    callback('Success with error', null);
                    this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    return;
                }
                callback(null, data);
            }).catch((error) => {
                callback(error, null);
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }

    public onEllipsisDataRecived(data: any): void {
        if (data) {
            if (data[GenericEllipsisEvent.add]) {
                this.enableControls([]);
                this.isDisabled = false;
                this.depotSearchConfig.disable = true;
                this.depotSearchConfig.autoOpen = false;
                this.commonMaintenanceFunction.onAddClick(true, this.tabList['tab1']['id']);
            } else {
                this.getSearchData(data['DepotNumber'], this.fetchDepotRecord.bind(this));
            }
        }
    }

    private enableSelectMode(openModal: boolean = true): void {
        this.isDisabled = true;
        this.tempDepotData = null;
        this.depotSearchConfig.disable = false;
        this.disableControls(['DepotNumber']);
        this.disableControl('DepotNumber', false);
        this.commonMaintenanceFunction.setPageMode(MntConst.eModeSelect);
        if (openModal) {
            this.depotSearchEllipsis.openModal();
        }
    }

    private enableEditMode(data: any): void {
        this.isDisabled = false;
        this.tempDepotData = data;
        this.depotSearchConfig.disable = false;
        this.enableControls(['DepotNumber']);
        this.disableControl('DepotNumber', true);
        this.commonMaintenanceFunction.setPageMode(MntConst.eModeUpdate, data);
    }

    private tabDraw(): void {
        this.tabList = {
            tab1: { id: 'grdDepotAddress', name: this.translatedMessageList['Depot_Details'], visible: true, active: true },
            tab2: { id: 'grdWasteInfo', name: this.translatedMessageList['Waste_Information'], visible: true, active: false }
        };
        this.riTab = new RiTab(this.tabList, this.utils);
        this.tab = this.riTab.tabObject;
    }
}
