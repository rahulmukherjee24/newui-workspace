import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { RouterModule } from '@angular/router';

import { VehicleModuleConstants } from '@app/base/PageRoutes';
import { SharedModule } from '@shared/shared.module';
import { SearchEllipsisDropdownModule } from '@app/internal/search-ellipsis-dropdown.module';

import { VehicleTypeMaintenanceComponent } from './vehicle-type/iCABSBVehicleTypeMaintenance.component';
import { VehicleComponentsMaintenanceComponent } from './vehicle-component-maintenance/iCABSBVehicleComponentsMaintenance.component';
import { VehicleMaintenanceComponent } from './vehicle-maintenance/iCABSBVehicleMaintenance.component';
import { DepotMaintenanceComponent } from './depot-maintenance/iCABSBDepotMaintenance.component';
import { VehicleManufacturerModelMaintenanceComponent } from './vehicle-manufacturer-model/iCABSBVehicleManufacturerModelMaintenance.component';
import { VehicleClassMaintenanceComponent } from './vehicle-class/iCABSBVehicleClassMaintenance.component';
import { RouteAwayGuardService } from '@shared/services/route-away-guard.service';
import { InternalSearchModule } from '@internal/search.module';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class VehicleManagementRootComponent {
    constructor(viewContainerRef: ViewContainerRef) { }
}

@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        SearchEllipsisDropdownModule,
        RouterModule.forChild([
            {
                path: '', component: VehicleManagementRootComponent, children: [
                    { path: VehicleModuleConstants.ICABSBVEHICLETYPEMAINTENANCE, component: VehicleTypeMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: VehicleModuleConstants.ICABSBVEHICLECOMPONENTSMAINTENANCE, component: VehicleComponentsMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: VehicleModuleConstants.ICABSBVEHICLEMAINTENANCE, component: VehicleMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: VehicleModuleConstants.ICABSBDEPOTMAINTENANCE, component: DepotMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: VehicleModuleConstants.ICABSBVEHICLEMANUFACTURERMODELMAINTENANCE, component: VehicleManufacturerModelMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: VehicleModuleConstants.ICABSBVEHICLECLASSMAINTENANCE, component: VehicleClassMaintenanceComponent, canDeactivate: [RouteAwayGuardService] }
                ], data: { domain: 'VEHICLE MANAGEMENT' }
            }
        ])
    ],
    declarations: [
        VehicleManagementRootComponent,
        VehicleTypeMaintenanceComponent,
        VehicleComponentsMaintenanceComponent,
        VehicleMaintenanceComponent,
        DepotMaintenanceComponent,
        VehicleManufacturerModelMaintenanceComponent,
        VehicleClassMaintenanceComponent
    ]
})

export class VehicleModule { }
