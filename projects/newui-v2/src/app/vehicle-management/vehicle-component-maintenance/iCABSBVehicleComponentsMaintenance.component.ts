import { OnInit, Injector, Component, ViewChild, AfterContentInit } from '@angular/core';

import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { GenericSearch, IGenericSearchQuery } from '@app/base/GenericSearch';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { ProductSearchGridComponent } from '@app/internal/search/iCABSBProductSearch';
import { RouteAwayComponent } from '@shared/components/route-away/route-away';
import { ProductServiceGroupSearchComponent } from '@app/internal/search/iCABSBProductServiceGroupSearch.component';
import { HttpRequestOptions, HttpServiceUrl } from '@app/base/httpRequestOptions';
import { RestService, HTTP_METHOD } from '@shared/services/rest-service';
import { DropdownConstants } from '@app/base/ComponentConstants';

@Component({
    templateUrl: 'iCABSBVehicleComponentsMaintenance.html'
})

export class VehicleComponentsMaintenanceComponent extends LightBaseComponent implements OnInit, AfterContentInit {

    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;
    @ViewChild('promptModal') public promptModal;
    @ViewChild('productservice') productservice: ProductServiceGroupSearchComponent;

    public controls: IControls[] = [
        { name: 'CompartmentID' },
        { name: 'LoadSpaceCapacity', type: MntConst.eTypeDecimal2, required: true },
        { name: 'Height', type: MntConst.eTypeDecimal2, required: true },
        { name: 'Width', type: MntConst.eTypeDecimal2, required: true },
        { name: 'Length', type: MntConst.eTypeDecimal2, required: true },
        { name: 'LoadWeightCapacity', type: MntConst.eTypeDecimal2, required: true },
        { name: 'ProductCodes' },
        { name: 'ProductServiceGroupCode' },
        { name: 'ProductServiceGroupDesc', type: MntConst.eTypeText }
    ];

    public pageId: string;
    public isDisabled: boolean = true;
    public pageMode = MntConst.eModeUpdate;
    public promptConfirmContent: string;
    public requestOptions: HttpRequestOptions;

    public productCodeEllipsis: Record<string, any> = {
        childparams: {
            parentMode: 'ProductCodesSearch',
            ProductCodes: ''
        },
        component: ProductSearchGridComponent
    };

    public productGroupServiceSearchParams: any = {
        params: {
            parentMode: 'LookUp'
        },
        active: { id: '', text: '' }
    };

    public vehicleRecord: any = {};
    public genericSearch: GenericSearch;
    public vehicleList: Array<any>;

    public genericSearchQuery: IGenericSearchQuery = {
        table: 'VehicleComponents'
    };

    constructor(private injector: Injector, private restService: RestService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBVEHICLECOMPONENTSMAINTENANCE;
        this.genericSearch = new GenericSearch(injector);
        this.browserTitle = this.pageTitle = 'Vehicle Components Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.genericSearch.init(this.genericSearchQuery);
        this.getVehicleList();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.disableControls(['CompartmentID']);
    }

    private getVehicleList(): void {
        this.genericSearch.fetch().then((data) => {
            this.vehicleList = data.records;
        });
    }

    public onAddClick(): void {
        this.setRequiredStatus('CompartmentID', true);
        this.setControlValue('ProductServiceGroupCode', '');
        this.productGroupServiceSearchParams.active = {
            id: '', text: '', multiSelectDisplay: ''
        };
        this.isDisabled = false;
        this.uiForm.reset();
        this.uiForm.enable();
        this.pageMode = MntConst.eModeAdd;
        this.onProductCodeChange();
    }

    public onCompartmentIDSelection(): void {
        this.isRequesting = true;
        this.isDisabled = false;
        this.uiForm.enable();
        this.requestOptions = HttpRequestOptions
            .create(this.injector, HttpServiceUrl.TERRITORY_MANAGEMENT)
            .addPathParam(this.utils.getCountryCode())
            .addPathParam(this.utils.getBusinessCode())
            .addPathParam(this.getControlValue('CompartmentID'))
            .addMethodType(HTTP_METHOD.METHOD_TYPE_GET);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.restService.executeRequest(this.requestOptions)
            .then((data) => {
                this.isRequesting = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    return;
                }
                this.setFormControlValues(data);
                this.vehicleRecord = data;
                this.formPristine();
            }).catch((error) => {
                this.handleError(error);
            });
    }

    public onSaveClick(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.promptModal.show();
            this.promptConfirmContent = MessageConstant.Message.ConfirmRecord;
        }
    }

    public onCancelClick(): void {
        if (this.pageMode === MntConst.eModeAdd) {
            this.enablePageEditMode();
            this.setRequiredStatus('CompartmentID', false);
            this.setControlValue('ProductServiceGroupCode', '');
            this.productGroupServiceSearchParams.active = {
                id: '', text: '', multiSelectDisplay: ''
            };
        } else {
            const selectedCompartmentID = this.getControlValue('CompartmentID');
            const selectedVehicle = this.vehicleList.find(({ CompartmentID }) => CompartmentID === selectedCompartmentID);
            selectedVehicle.ProductServiceGroups = this.getControlValue('ProductServiceGroupCode');
            if (this.vehicleRecord) {
                this.setFormControlValues(this.vehicleRecord);
            }
        }
    }

    public onDeleteClick(): void {
        this.pageMode = MntConst.eModeDelete;
        this.promptModal.show();
        this.promptConfirmContent = MessageConstant.Message.DeleteRecord;
    }

    public onConfirmation(obj: any): void {
        if (this.pageMode === MntConst.eModeDelete) {
            this.deleteVehicleRecord();
        } else {
            this.saveVehicleRecord();
        }
    }

    public onCancellation(): void {
        if (this.pageMode === MntConst.eModeDelete) {
            this.pageMode = MntConst.eModeUpdate;
        }
    }

    private saveVehicleRecord(): void {
        this.isRequesting = true;
        let postObj: any = {
            table: 'VehicleComponents',
            methodtype: 'maintenance',
            CompartmentID: this.getControlValue('CompartmentID'),
            LoadSpaceCapacity: this.getControlValue('LoadSpaceCapacity'),
            Height: this.getControlValue('Height'),
            Width: this.getControlValue('Width'),
            Length: this.getControlValue('Length'),
            LoadWeightCapacity: this.getControlValue('LoadWeightCapacity'),
            ProductCodes: this.getControlValue('ProductCodes'),
            ProductServiceGroups: this.getControlValue('ProductServiceGroupCode')
        };
        this.requestOptions = HttpRequestOptions
            .create(this.injector, HttpServiceUrl.TERRITORY_MANAGEMENT)
            .addPathParam(this.utils.getCountryCode())
            .addPathParam(this.utils.getBusinessCode())
            .addPathParam(this.getControlValue('CompartmentID'))
            .addBodyParams(postObj)
            .addMethodType(this.pageMode === MntConst.eModeAdd ? HTTP_METHOD.METHOD_TYPE_POST : HTTP_METHOD.METHOD_TYPE_PUT);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.restService.executeRequest(this.requestOptions)
            .then((data) => {
                this.isRequesting = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    return;
                }
                this.pageMode = MntConst.eModeUpdate;
                this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                this.getVehicleList();
                this.formPristine();
            }).catch((error) => {
                this.handleError(error);
            });
    }

    private deleteVehicleRecord(): void {
        if (this.pageMode === MntConst.eModeDelete) {
            this.isRequesting = true;
            this.requestOptions = HttpRequestOptions
                .create(this.injector, HttpServiceUrl.TERRITORY_MANAGEMENT)
                .addPathParam(this.utils.getCountryCode())
                .addPathParam(this.utils.getBusinessCode())
                .addPathParam(this.getControlValue('CompartmentID'))
                .addMethodType(HTTP_METHOD.METHOD_TYPE_DELETE);

            this.ajaxSource.next(this.ajaxconstant.START);
            this.restService.executeRequest(this.requestOptions)
                .then((data) => {
                    this.isRequesting = false;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.formPristine();
                    if (this.hasError(data)) {
                        this.pageMode = MntConst.eModeUpdate;
                        this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                        return;
                    }
                    this.displayMessage(MessageConstant.Message.RecordDeletedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                    this.getVehicleList();
                    this.enablePageEditMode();
                    this.productGroupServiceSearchParams.active = {
                        id: '', text: '', multiSelectDisplay: ''
                    };
                }).catch((error) => {
                    this.pageMode = MntConst.eModeUpdate;
                    this.handleError(error);
                });
        }
    }

    public onProductSearchDataReceived(data: any): void {
        if (data) {
            this.setControlValue('ProductCodes', data['ProductCodes']);
            this.onProductCodeChange();
        }
    }

    public onProductCodeChange(): void {
        this.productCodeEllipsis.childparams.ProductCodes = this.getControlValue('ProductCodes');
    }

    private setFormControlValues(data: any): void {
        if (data) {
            this.uiForm.patchValue(data);
            if (data.ProductServiceGroups) {
                this.productservice.setValue(data.ProductServiceGroups);
                let serviceGroupcodes = data.ProductServiceGroups;
                this.productGroupServiceSearchParams.active = {
                    id: '', text: '', multiSelectDisplay: serviceGroupcodes
                };
            }
            else {
                this.productGroupServiceSearchParams.active = {
                    id: '', text: '', multiSelectDisplay: ''
                };
            }
            this.onProductCodeChange();
        }
    }

    private enablePageEditMode(): void {
        this.uiForm.reset();
        this.disableControls(['CompartmentID']);
        this.isDisabled = true;
        this.pageMode = MntConst.eModeUpdate;
        this.onProductCodeChange();
    }

    private handleError(error: any): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.isRequesting = false;
        this.displayMessage(error, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
    }

    public onProductGroupDataReceived(e: any): void {
        this.setControlValue('ProductServiceGroupCode', e[DropdownConstants.c_s_KEY_MULTISELECTVALUE]);
        this.setControlValue('ProductServiceGroupDesc', e.ProductServiceGroupDesc);
    }
}
