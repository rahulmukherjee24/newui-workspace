import { OnInit, Injector, Component, ViewChild, OnDestroy } from '@angular/core';

import { CommonMaintenanceFunction } from '@app/base/CommonMaintenanceFunction';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import {
    EllipsisGenericComponent,
    IGenericEllipsisControl,
    GenericEllipsisEvent
} from '@shared/components/ellipsis-generic/ellipsis-generic';
import { HTTP_METHOD, RestService } from '@shared/services/rest-service';
import { HttpRequestOptions, HttpServiceUrl } from '@app/base/httpRequestOptions';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { LookUpData } from '@shared/services/lookup';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst, RiTab } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { RouteAwayComponent } from '@shared/components/route-away/route-away';
import { Subscription } from 'rxjs';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { IGenericSearchQuery, GenericSearch } from '@app/base/GenericSearch';

@Component({
    templateUrl: 'iCABSBVehicleClassMaintenance.html',
    providers: [CommonLookUpUtilsService]
})

export class VehicleClassMaintenanceComponent extends LightBaseComponent implements OnInit, OnDestroy {

    @ViewChild('vehicleClassSearchEllipsis') public vehicleClassSearchEllipsis: EllipsisGenericComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;

    public pageId: string;
    public controls: IControls[] = [
        { name: 'ClassNumber', type: MntConst.eTypeInteger },
        { name: 'ClassType', type: MntConst.eTypeText },
        { name: 'ClassStartDate', type: MntConst.eTypeDate },
        { name: 'ClassEndDate', type: MntConst.eTypeDate },
        { name: 'Make', type: MntConst.eTypeText, required: true },
        { name: 'Model', type: MntConst.eTypeText, required: true },
        { name: 'FuelType', type: MntConst.eTypeText },
        { name: 'WheelBase', type: MntConst.eTypeText },
        { name: 'EngineSize', type: MntConst.eTypeInteger },
        { name: 'EngineType', type: MntConst.eTypeText },
        { name: 'OverNightParking', type: MntConst.eTypeText },
        { name: 'CompartmentIDs', type: MntConst.eTypeText },
        { name: 'VehicleNotes', type: MntConst.eTypeText },
        { name: 'FixedCost', type: MntConst.eTypeDecimal2 },
        { name: 'VariableCost', type: MntConst.eTypeDecimal2 },
        { name: 'MaxGrossUnladenWeight', type: MntConst.eTypeDecimal2, required: true },
        { name: 'MaxVehicleHeight', type: MntConst.eTypeDecimal2 },
        { name: 'MaxGrossWeight', type: MntConst.eTypeDecimal2, required: true },
        { name: 'MaxVehicleLength', type: MntConst.eTypeDecimal2 },
        { name: 'Tachograph', type: MntConst.eTypeCheckBox },
        { name: 'TailLift', type: MntConst.eTypeCheckBox },
        { name: 'VehicleTypeIDs', type: MntConst.eTypeText },
        { name: 'RestrictionTimeStart', type: MntConst.eTypeTime },
        { name: 'RestrictionTimeEnd', type: MntConst.eTypeTime },
        { name: 'SpeedLimiter', type: MntConst.eTypeInteger },
        { name: 'OpenClosed', type: MntConst.eTypeCheckBox },
        { name: 'TemporaryVehicle', type: MntConst.eTypeCheckBox },
        { name: 'FuelConsumptionValue1', type: MntConst.eTypeText },
        { name: 'FuelConsumptionValue2', type: MntConst.eTypeText },
        { name: 'FuelConsumptionValue3', type: MntConst.eTypeText }
    ];

    public ellipsisConfigs: Record<string, IGenericEllipsisControl> = {
        vehicleClass: {
            autoOpen: true,
            ellipsisTitle: 'Vehicle Class Search',
            configParams: {
                table: 'VehicleClass',
                shouldShowAdd: true
            },
            tableColumn: [
                { 'title': 'Vehicle Class Number', 'name': 'ClassNumber' },
                { 'title': 'Vehicle Class Type', 'name': 'ClassType' }
            ],
            disable: false
        },
        manufacturerModel: {
            autoOpen: false,
            ellipsisTitle: 'Vehicle Manufacture Model Search',
            configParams: {
                table: 'VehicleManufacturerModel',
                shouldShowAdd: false
            },
            tableColumn: [
                { title: 'Manufacturer', name: 'Manufacturer', type: MntConst.eTypeText },
                { title: 'Model', name: 'Model', type: MntConst.eTypeText }
            ],
            disable: true
        }
    };

    public MntConst = MntConst;
    public tab: any;
    public riTab: RiTab;
    private tabList: any;
    private translatedMessageList: Record<string, string> = {
        'Specification': 'Specification',
        'Metrics': 'Metrics',
        'Options': 'Options'
    };

    public isDisabled: boolean = true;
    public isDepotMappingEnabled: boolean = false;
    private tempVehicleClassData: Record<string, string>;
    private headerParams: IXHRParams = {
        operation: 'Business/iCABSBVehicleClassMaintenance',
        module: 'service',
        method: 'service-delivery/admin'
    };
    public commonMaintenanceFunction: CommonMaintenanceFunction;
    public dropdownData: {
        vehicleClassTypes: Array<string>,
        fuelTypes: Array<string>,
        wheelBases: Array<string>,
        overnightParkings: Array<string>
    };
    private requestOptions: HttpRequestOptions;
    private tempVehicleChangeSubscription: Subscription;

    public genericSearch: GenericSearch;
    public vehicleList: Array<any>;

    public genericSearchQuery: IGenericSearchQuery = {
        table: 'VehicleComponents'
    };

    constructor(
        private injector: Injector,
        private sysCharConstants: SysCharConstants,
        private restService: RestService,
        private commonLookup: CommonLookUpUtilsService
    ) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBVEHICLECLASSMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Vehicle Class Maintenance';
        this.commonMaintenanceFunction = new CommonMaintenanceFunction(this, injector);
        this.pageParams = {
            pageMode: MntConst.eModeSelect
        };
        this.genericSearch = new GenericSearch(injector);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.zone.run(() => {
            this.tabDraw();
        });
        this.genericSearch.init(this.genericSearchQuery);
        this.getSysCharDtetails();
        this.fetchDropdownData();
        this.disableControls(['ClassNumber']);
        this.tempVehicleChangeSubscription = this.uiForm.controls['TemporaryVehicle'].valueChanges.subscribe((val) => {
            this.onTempVehicleChange();
        });
        this.getRegistryValue();
    }

    private getSysCharDtetails(): any {
        this.ajaxSource.next(this.ajaxconstant.START);
        let syscharObj: QueryParams = this.getURLSearchParamObject();
        syscharObj.set(this.serviceConstants.Action, '0');
        syscharObj.set(this.serviceConstants.SystemCharNumber,
            [this.sysCharConstants.SystemCharEnableRouteOptimisationSoftwareIntegration].toString());
        this.httpService.sysCharRequest(syscharObj).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.records[0])
                this.pageParams.vbEnableRouteOptimisation = data.records[0].Required;
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage(error);
        });
    }

    private fetchDropdownData(): void {
        const query: Array<LookUpData> = [
            {
                table: 'VehicleClassType',
                fields: ['ClassType'],
                query: {}
            },
            {
                table: 'riRegistry',
                fields: ['RegValue'],
                query: {
                    RegSection: 'Vehicle Parameters',
                    RegKey: this.businessCode() + '_Fuel Type'
                }
            },
            {
                table: 'riRegistry',
                fields: ['RegValue'],
                query: {
                    RegSection: 'Vehicle Parameters',
                    RegKey: this.businessCode() + '_Wheel-Base'
                }
            },
            {
                table: 'riRegistry',
                fields: ['RegValue'],
                query: {
                    RegSection: 'Vehicle Parameters',
                    RegKey: this.businessCode() + '_Over Night Parking'
                }
            }
        ];
        this.LookUp.lookUpPromise(query).then((data: any) => {
            this.dropdownData = {
                vehicleClassTypes: data[0].map((vehicleClass: any) => vehicleClass.ClassType),
                fuelTypes: data[1][0]['RegValue'].split(';'),
                wheelBases: data[2][0]['RegValue'].split(';'),
                overnightParkings: data[3][0]['RegValue'].split(';')
            };
        }).catch((error: any) => {
            this.displayMessage(error);
        });
    }

    public onBtnClick(action: string): void {
        switch (action) {
            case 'Save':
                this.commonMaintenanceFunction.onSaveClick(
                    this.saveVehicleClassRecord.bind(this), null, true, this.tabList
                );
                break;
            case 'Cancel':
                this.commonMaintenanceFunction.onCancelClick(
                    this.tempVehicleClassData, this.enableSelectMode.bind(this), true, this.tabList['tab1']['id']
                );
                break;
            case 'Delete':
                this.commonMaintenanceFunction.onDeleteClick(
                    this.deleteVehicleClassRecord.bind(this)
                );
                break;
        }
    }

    private getCommonFormData(): Record<string, string> {
        return {
            table: 'VehicleClass',
            [this.serviceConstants.MethodType]: 'maintenance',
            UserCode: this.utils.getUserCode(),
            ClassNumber: this.getControlValue('ClassNumber')
        };
    }

    public onVehicleClassChange(): void {
        if (this.getControlValue('ClassNumber')) {
            this.fetchVehicleClassRecord();
        } else {
            this.enableSelectMode(false);
        }
    }

    private setupParams(path: string, methodType: string, action: number, operation: string = this.headerParams.operation): HttpRequestOptions {
        const queryParams: QueryParams = this.getURLSearchParamObject();
        queryParams.set(this.serviceConstants.Action, action);
        queryParams.set(this.serviceConstants.email, this.utils.getEmail());

        this.requestOptions = HttpRequestOptions
            .create(this.injector, HttpServiceUrl.ICABS_PROXY)
            .addPathParam(path)
            .addQueryParams(queryParams)
            .addMethodType(methodType)
            .addPageHeader(this.serviceConstants.Module, this.headerParams.module)
            .addPageHeader(this.serviceConstants.Operation, operation);
        return this.requestOptions;
    }

    private fetchVehicleClassRecord(): void {
        this.isRequesting = true;
        this.requestOptions = this.setupParams(this.headerParams.method, HTTP_METHOD.METHOD_TYPE_POST, 0);
        this.requestOptions.addBodyParams(this.getCommonFormData());

        this.ajaxSource.next(this.ajaxconstant.START);
        this.restService.executeRequest(this.requestOptions).then(data => {
            this.isRequesting = false;
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
                return;
            }
            data['ClassNumber'] = this.getControlValue('ClassNumber');
            this.enableEditMode(data);
        }).catch((error: any) => {
            this.isRequesting = false;
            this.displayMessage(error);
        });
    }

    private saveVehicleClassRecord(): void {
        this.isRequesting = true;
        this.requestOptions = this.setupParams(
            this.headerParams.method, HTTP_METHOD.METHOD_TYPE_POST, this.pageParams.pageMode === MntConst.eModeAdd ? 1 : 2
        );

        let formData = this.getCommonFormData();
        for (const control in this.uiForm.controls) {
            if (control) {
                if (this.controlDataTypes[control] === MntConst.eTypeCheckBox) {
                    formData[control] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue(control));
                } else {
                    formData[control] = this.getControlValue(control);
                }
            }
        }
        if (this.pageParams.pageMode === MntConst.eModeAdd) {
            delete formData.ClassNumber;
        }
        this.requestOptions.addBodyParams(formData);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.restService.executeRequest(this.requestOptions).then(data => {
            this.isRequesting = false;
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
                return;
            }
            let modifiedData: any;
            if (Object.keys(data).length === 0) {
                modifiedData = this.uiForm.value;
            } else if (data.hasOwnProperty('ClassNumber')) {
                modifiedData = {
                    ...this.uiForm.value,
                    ClassNumber: data.ClassNumber
                };
            }
            this.enableEditMode(modifiedData);
            this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
        }).catch((error: any) => {
            this.isRequesting = false;
            this.displayMessage(error);
        });
    }

    private deleteVehicleClassRecord(): void {
        this.isRequesting = true;
        this.requestOptions = this.setupParams(this.headerParams.method, HTTP_METHOD.METHOD_TYPE_POST, 3);
        this.requestOptions.addBodyParams(this.getCommonFormData());

        this.ajaxSource.next(this.ajaxconstant.START);
        this.restService.executeRequest(this.requestOptions).then(data => {
            this.isRequesting = false;
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.pageParams.pageMode = MntConst.eModeUpdate;
                this.displayMessage(data);
                return;
            }
            this.displayMessage(MessageConstant.Message.RecordDeletedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
            this.enableSelectMode();
        }).catch((error: any) => {
            this.isRequesting = false;
            this.displayMessage(error);
        });
    }

    public onEllipsisDataRecived(data: any, ellipsisFor: string): void {
        if (data) {
            switch (ellipsisFor) {
                case 'vehicleClass':
                    if (data[GenericEllipsisEvent.add]) {
                        this.enableControls([]);
                        this.disableControl('ClassNumber', true);
                        this.isDisabled = false;
                        this.ellipsisConfigs.vehicleClass.disable = true;
                        this.ellipsisConfigs.vehicleClass.autoOpen = false;
                        this.ellipsisConfigs.manufacturerModel.disable = false;
                        this.commonMaintenanceFunction.onAddClick(true, this.tabList['tab1']['id']);
                        this.setControlValue('ClassStartDate', '01/01/2001');
                        this.setControlValue('ClassEndDate', '31/12/2099');
                    } else {
                        this.commonMaintenanceFunction.clearErrorTabs();
                        if (data.hasOwnProperty('FuelConsumptionValue')) {
                            data['FuelConsumptionValue1'] = data['FuelConsumptionValue'][3];
                            data['FuelConsumptionValue2'] = data['FuelConsumptionValue'][4];
                            data['FuelConsumptionValue3'] = data['FuelConsumptionValue'][5];
                        }
                        this.enableEditMode(data);
                    }
                    break;
                case 'vehicleModel':
                    this.setControlValue('Make', data.Manufacturer);
                    this.setControlValue('Model', data.Model);
                    break;
            }
        }
    }

    private enableSelectMode(openModal: boolean = true): void {
        this.isDisabled = true;
        this.tempVehicleClassData = null;
        this.ellipsisConfigs.vehicleClass.disable = false;
        this.ellipsisConfigs.manufacturerModel.disable = true;
        this.disableControls([]);
        this.disableControl('ClassNumber', false);
        this.commonMaintenanceFunction.setPageMode(MntConst.eModeSelect);
        if (openModal) {
            this.vehicleClassSearchEllipsis.openModal();
        }
    }

    private enableEditMode(data: any): void {
        if (data) {
            this.isDisabled = false;
            this.ellipsisConfigs.vehicleClass.disable = false;
            this.ellipsisConfigs.manufacturerModel.disable = false;
            this.enableControls([]);
            this.disableControl('ClassNumber', true);
            this.tempVehicleClassData = data;
            this.commonMaintenanceFunction.setPageMode(MntConst.eModeUpdate, data);
        }
    }

    private tabDraw(): void {
        this.tabList = {
            tab1: { id: 'grdSpecification', name: this.translatedMessageList['Specification'], visible: true, active: true },
            tab2: { id: 'grdMetrics', name: this.translatedMessageList['Metrics'], visible: true, active: false },
            tab3: { id: 'grdOptions', name: this.translatedMessageList['Options'], visible: true, active: false }
        };
        this.riTab = new RiTab(this.tabList, this.utils);
        this.tab = this.riTab.tabObject;
    }

    public get isTempVehicleChecked(): boolean {
        return this.getControlValue('TemporaryVehicle') || false;
    }

    public onTempVehicleChange(): void {
        if (this.pageParams.vbEnableRouteOptimisation) {
            const controlsList: Array<string> = [
                'ClassType', 'ClassStartDate', 'ClassEndDate', 'FuelType',
                'WheelBase', 'EngineSize', 'EngineType', 'OverNightParking',
                'CompartmentIDs', 'VehicleNotes', 'FixedCost', 'VariableCost',
                'MaxVehicleHeight', 'MaxVehicleLength', 'VehicleTypeIDs',
                'RestrictionTimeStart', 'RestrictionTimeEnd', 'SpeedLimiter',
                'FuelConsumptionValue1', 'FuelConsumptionValue2', 'FuelConsumptionValue3'
            ];
            controlsList.forEach((control) => {
                this.setRequiredStatus(control, !this.isTempVehicleChecked);
            });
        }
    }

    public onMakeOrModelChange(): void {
        if (this.getControlValue('Make') && this.getControlValue('Model')) {
            this.isRequesting = true;
            this.requestOptions = this.setupParams(
                this.headerParams.method, HTTP_METHOD.METHOD_TYPE_POST, 0,
                'Business/iCABSBVehicleManufacturerModelMaintenance'
            );
            const formData = {
                table: 'VehicleManufacturerModel',
                [this.serviceConstants.MethodType]: 'maintenance',
                UserCode: this.utils.getUserCode(),
                Manufacturer: this.getControlValue('Make'),
                Model: this.getControlValue('Model')
            };
            this.requestOptions.addBodyParams(formData);

            this.ajaxSource.next(this.ajaxconstant.START);
            this.restService.executeRequest(this.requestOptions).then(data => {
                this.isRequesting = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.setControlValue('Make', '');
                    this.setControlValue('Model', '');
                    this.displayMessage(data.errorMessage || data);
                    return;
                }
                this.setControlValue('Make', data.Manufacturer);
                this.setControlValue('Model', data.Model);
            }).catch((error: any) => {
                this.isRequesting = false;
                this.displayMessage(error);
            });
        }
    }

    private getRegistryValue(): void {
        this.commonLookup.getBusinessRegistryValue(this.utils.getBusinessCode(), 'ServicePlus', 'Vehicle_Compartment_Mapping')
            .then((regData) => {
                if (regData[0][0]) {
                    this.isDepotMappingEnabled = regData[0][0].RegValue.toLowerCase() === 'true';
                }

                if (this.isDepotMappingEnabled) {
                    this.getVehicleList();
                }
            });
    }

    private getVehicleList(): void {
        this.genericSearch.fetch().then((data) => {
            this.vehicleList = data.records;
        });
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.tempVehicleChangeSubscription) {
            this.tempVehicleChangeSubscription.unsubscribe();
        }
    }
}
