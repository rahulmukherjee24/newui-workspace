import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { EllipsisGenericComponent, IGenericEllipsisControl } from '@shared/components/ellipsis-generic/ellipsis-generic';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { ModalAdvService } from '@shared/components/modal-adv/modal-adv.service';
import { PageIdentifier } from '@base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBVehicleTypeMaintenance.html'
})

export class VehicleTypeMaintenanceComponent extends LightBaseComponent implements OnInit, OnDestroy {
    @ViewChild('promptModal') public promptModal;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('vehicleTypeSearchEllipsis') vehicleTypeSearchEllipsis: EllipsisGenericComponent;

    public pageId: string;
    public pageMode: string = MntConst.eModeSelect;
    public promptConfirmContent: string;
    public vehicleTypeData: Object;

    private xhrParams: Record<string, string> = {
        method: 'service-delivery/admin',
        module: 'service',
        operation: 'Business/iCABSBVehicleTypeMaintenance'
    };

    public controls: IControls[] = [
        { name: 'VehicleTypeNumber', type: MntConst.eTypeInteger, required: true },
        { name: 'VehicleTypeDesc', type: MntConst.eTypeText, required: true },
        { name: 'Weight', type: MntConst.eTypeDecimal2, required: true },
        { name: 'Height', type: MntConst.eTypeDecimal2, required: true },
        { name: 'Depth', type: MntConst.eTypeDecimal2, required: true },
        { name: 'Width', type: MntConst.eTypeDecimal2, required: true },
        { name: 'CostPerKM', type: MntConst.eTypeDecimal2, required: true },
        { name: 'AverageLoadTime', type: MntConst.eTypeTime, required: true },
        { name: 'MaxGrossVehicleWeight', type: MntConst.eTypeDecimal2 }
    ];

    public vehicleTypeConfig: IGenericEllipsisControl = {
        autoOpen: true,
        ellipsisTitle: 'Vehicle Type Search',
        configParams: {
            table: 'VehicleType',
            shouldShowAdd: true
        },
        tableColumn: [
            { 'title': 'Type Number', 'name': 'VehicleTypeNumber' },
            { 'title': 'Description', 'name': 'VehicleTypeDesc' },
            { 'title': 'Weight', 'name': 'Weight' },
            { 'title': 'Height', 'name': 'Height' },
            { 'title': 'Depth', 'name': 'Depth' },
            { 'title': 'Width', 'name': 'Width' },
            { 'title': 'Cost Per KM', 'name': 'CostPerKM' },
            { 'title': 'Average Load Time', 'name': 'AverageLoadTime', 'type': MntConst.eTypeTime }],
        disable: false
    };

    constructor(injector: Injector, private modalAdvService: ModalAdvService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBVEHICLETYPEMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Vehicle Type Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.disableControls(['VehicleTypeNumber']);
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public onBtnClicked(action: string): void {
        switch (action) {
            case 'Save':
                if (this.riExchange.validateForm(this.uiForm)) {
                    this.promptModal.show();
                    this.promptConfirmContent = MessageConstant.Message.ConfirmRecord;
                }
                break;
            case 'Delete':
                this.pageMode = MntConst.eModeDelete;
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null,
                    this.deleteRecord.bind(this), this.deleteCanceled.bind(this)));
                this.promptConfirmContent = MessageConstant.Message.DeleteRecord;
                break;
            case 'Cancel':
                if (this.pageMode === MntConst.eModeUpdate) {
                    this.setControlData(this.vehicleTypeData);
                    this.formPristine();
                } else if (this.pageMode === MntConst.eModeAdd) {
                    this.uiForm.reset();
                    this.vehicleTypeConfig.disable = false;
                    this.disableControls(['VehicleTypeNumber']);
                    this.vehicleTypeSearchEllipsis.openModal();
                    this.pageMode = MntConst.eModeSelect;
                }
                break;
        }
    }

    public confirmed(): void {
        if (this.pageMode === MntConst.eModeDelete) {
            this.deleteRecord();
        }
        else {
            this.saveRecord();
        }
    }

    private saveRecord(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.pageMode === MntConst.eModeAdd ? 1 : 2);
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        let postObj: Object = {};

        this.controls.forEach(control => {
            postObj[control.name] = this.getControlValue(control.name);
        });

        postObj[this.serviceConstants.MethodType] = 'maintenance';
        postObj['table'] = 'VehicleType';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, search, postObj).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (this.hasError(data)) {
                        this.displayMessage(data.errorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    } else {
                        this.tempVehicleData();
                        this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully,
                            CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                        this.pageMode = MntConst.eModeUpdate;
                        this.vehicleTypeConfig.disable = false;
                        this.formPristine();
                    }
                }).catch(error => {
                    this.displayMessage(error);
                });
    }

    private deleteRecord(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, 3);
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        let postObj: Object = {};

        postObj['VehicleTypeNumber'] = this.getControlValue('VehicleTypeNumber');
        postObj[this.serviceConstants.MethodType] = 'maintenance';
        postObj['table'] = 'VehicleType';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, postObj).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.displayMessage(data.errorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                } else {
                    this.displayMessage(MessageConstant.Message.RecordDeletedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                    this.uiForm.reset();
                    this.pageMode = MntConst.eModeSelect;
                    this.vehicleTypeSearchEllipsis.openModal();
                    this.disableControls(['VehicleTypeNumber']);
                }
            }).catch(error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.pageMode = MntConst.eModeUpdate;
                this.displayMessage(error);
            });
    }

    private deleteCanceled(): void {
        this.pageMode = MntConst.eModeUpdate;
    }

    public onDataRecived(data: Object): void {
        if (data) {
            if (data['isAddEvent']) {
                this.uiForm.reset();
                this.changeControls();
                this.vehicleTypeConfig.autoOpen = false;
                this.vehicleTypeConfig.disable = true;
                this.pageMode = MntConst.eModeAdd;
            } else {
                this.setControlData(data, true);
                this.changeControls(true);
                this.vehicleTypeData = data;
                this.pageMode = MntConst.eModeUpdate;
                this.formPristine();
            }
        }
    }

    public onVehicleTypeNumberChange(): void {
        if (this.riExchange.validateForm(this.uiForm) &&
            (this.pageMode === MntConst.eModeUpdate || this.pageMode === MntConst.eModeSelect)) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, 0);
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            let postObj: Object = {};

            postObj['VehicleTypeNumber'] = this.getControlValue('VehicleTypeNumber');
            postObj[this.serviceConstants.MethodType] = 'maintenance';
            postObj['table'] = 'VehicleType';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module,
                this.xhrParams.operation, search, postObj).then(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (this.hasError(data)) {
                            this.displayMessage(data.errorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                            this.uiForm.reset();
                        } else {
                            this.setControlData(data, true);
                            this.changeControls(true);
                            this.vehicleTypeData = data;
                            this.vehicleTypeConfig.disable = false;
                            this.pageMode = MntConst.eModeUpdate;
                            this.formPristine();
                        }
                    }).catch(error => {
                        this.displayMessage(error);
                        this.uiForm.reset();
                    });
        }
    }

    private setControlData(data: Object, isSaveTemp?: boolean): void {
        if (data) {
            this.uiForm.patchValue(data);
            if (isSaveTemp) {
                this.tempVehicleData();
            }
        }
    }

    private tempVehicleData(): void {
        this.vehicleTypeData = {};
        this.controls.forEach(control => {
            this.vehicleTypeData[control.name] = this.getControlValue(control.name);
        });
    }

    private changeControls(descStatus?: boolean): void {
        this.enableControls([]);
        if (descStatus)
            this.disableControl('VehicleTypeDesc', descStatus);
    }

}
