import { BatchProcessMonitorSearchComponent } from './TableMaintenanceSystem/iCABSMBatchProcessMonitorSearch';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ITFunctionsModuleRoutes, ITFunctionsModuleRoutesConstant } from './../base/PageRoutes';
import { ITFunctionsRootComponent } from './it-functions.component';
import { BatchProcessMonitorComponent } from './BatchProcesses/iCABSOBatchProcessMonitorSearch.component';
import { RiRegistryComponent } from './Registry/riRegistry.component';
import { BatchProgramMaintenanceComponent } from './TableMaintenanceSystem/MaintainBatchProgram/riMGBatchProgramMaintenance.component';
import { BusinessSystemCharacteristicsCustomMaintenanceComponent } from './TableMaintenanceSystem/iCABSSBusinessSystemCharacteristicsCustomMaintenance';
import { SystemParameterMaintenanceComponent } from './Maintenance/MaintainParameters/iCABSSSystemParameterMaintenance.component';
import { BranchMaintenanceComponent } from './TableMaintenanceBusiness/Branch/iCABSBBranchMaintenance.component';
import { UserTypeMenuAccessMaintenanceComponent } from './Maintenance/UserTypeMenuAccess/riMUserTypeMenuAccessMaintenance.component';
import { TranslationMaintenanceComponent } from './Maintenance/Translation/riMGTranslationMaintenance.component';
import { UserInformationMaintenanceComponent } from './TableMaintenanceSystem/User/riMUserInformationMaintenance.component';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { CompanyMaintenanceComponent } from '../internal/maintenance/iCABSBCompanyMaintenance.component';
import { BusinessRegistryGridComponent } from './Configuration/BusinessRegistry/iCABSBBusinessRegistryGrid.component';
import { UserTypeMaintenanceComponent } from './TableMaintenanceSystem/UserType/riMUserTypeMaintenance.component';
import { ReportViewerSearchComponent } from './Tools/riMReportViewerSearch';
import { AddUserComponent } from './Tools/admin/riAddUserAllServers.component';
import { BatchTranlationsComponent } from './Tools/admin/translations/riBatchTranslations.component';
import { RiImportComponent } from './Importing/riImport.component';

const routes: Routes = [
    {
        path: '', component: ITFunctionsRootComponent, children: [
            { path: 'batchprocess/monitor/search', component: BatchProcessMonitorComponent },
            { path: 'registry/:section', component: RiRegistryComponent },
            { path: 'registry', component: RiRegistryComponent },
            { path: 'batchprocess/monitor', component: BatchProcessMonitorSearchComponent },
            { path: 'batchprogram/maintenance', component: BatchProgramMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: 'batchprocess/syscharmonitor', component: BusinessSystemCharacteristicsCustomMaintenanceComponent },
            { path: 'maintenance/maintainParameters/system', component: SystemParameterMaintenanceComponent },
            { path: ITFunctionsModuleRoutes.ICABSBBRANCHMAINTENANCE, component: BranchMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: ITFunctionsModuleRoutes.RIMGTRANSLATIONMAINTENANCE, component: TranslationMaintenanceComponent },
            { path: ITFunctionsModuleRoutes.RIMUSERTYPEMENUACCESSMAINTENANCE, component: UserTypeMenuAccessMaintenanceComponent },
            { path: ITFunctionsModuleRoutes.ICABSBCOMPANYMAINTENANCE, component: CompanyMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: ITFunctionsModuleRoutesConstant.RIMUSERINFORMATIONMAINTENANCE, component: UserInformationMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: ITFunctionsModuleRoutesConstant.ICABSBBUSINESSREGISTRYGRID, component: BusinessRegistryGridComponent, canDeactivate: [RouteAwayGuardService] },
            { path: ITFunctionsModuleRoutesConstant.RIMUSERTYPEMAINTENANCE, component: UserTypeMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: ITFunctionsModuleRoutes.RIMREPORTVIEWERSEARCH, component: ReportViewerSearchComponent },
            { path: ITFunctionsModuleRoutesConstant.RIADDUSERALLSERVERS, component: AddUserComponent },
            { path: ITFunctionsModuleRoutesConstant.RIBATCHTRANSLATIONS, component: BatchTranlationsComponent },
            { path: ITFunctionsModuleRoutesConstant.RIIMPORT, component: RiImportComponent }
        ], data: { domain: 'IT FUNCTIONS' }
    }
];

export const ITFunctionsRouteDefinitions: ModuleWithProviders = RouterModule.forChild(routes);
