import { LocalStorageService } from 'ngx-webstorage';
import { Component, ViewContainerRef, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../shared/services/auth.service';
import { RiExchange } from '../../shared/services/riExchange';

@Component({
    template: `<router-outlet></router-outlet>
<icabs-modal #errorModal="child" [(showHeader)]="showErrorHeader" [config]="{backdrop: 'static'}"></icabs-modal>`
})

export class ITFunctionsRootComponent implements OnInit, OnDestroy {
    @ViewChild('errorModal') public errorModal;
    public authJson: any;
    public muleJson: any;
    public displayNavBar: boolean;
    public showErrorHeader: boolean = true;
    public componentInteractionSubscription: Subscription;
    public errorSubscription: Subscription;
    public routerSubscription: Subscription;

    constructor(viewContainerRef: ViewContainerRef, private riExchange: RiExchange, private _authService: AuthService, private _ls: LocalStorageService) {
    }

    ngOnInit(): void {
        this.displayNavBar = true;
        if (!this._authService.oauthResponse) {
            this.authJson = this._ls.retrieve('OAUTH');
        } else {
            this.authJson = this._authService.oauthResponse;
        }
    }



    ngOnDestroy(): void {
        if (this.componentInteractionSubscription) {
            this.componentInteractionSubscription.unsubscribe();
        }
        if (this.errorSubscription) {
            this.errorSubscription.unsubscribe();
        }
        if (this.routerSubscription) {
            this.routerSubscription.unsubscribe();
        }
        this.riExchange.releaseReference(this);
    }
}
