import { QueryParams } from './../../../../shared/services/http-params-wrapper';
import { LocalStorageService } from 'ngx-webstorage';
import { Component, OnInit, OnDestroy, ViewChild, Injector, AfterContentInit } from '@angular/core';

import { PageIdentifier } from './../../../base/PageIdentifier';
import { QueryParametersCallback } from './../../../base/Callback';
import { BaseComponent } from '../../../../app/base/BaseComponent';
import { GoogleMapPagesModuleRoutes } from './../../../base/PageRoutes';
import { MntConst, RiTab } from './../../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { CBBConstants } from './../../../../shared/constants/cbb.constants';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { EllipsisComponent } from './../../../../shared/components/ellipsis/ellipsis';
import { CommonDropdownComponent } from './../../../../shared/components/common-dropdown/common-dropdown.component';
import { EmployeeSearchComponent } from './../../../internal/search/iCABSBEmployeeSearch';
import { BranchServiceAreaSearchComponent } from './../../../internal/search/iCABSBBranchServiceAreaSearch';

@Component({
    templateUrl: 'iCABSBBranchMaintenance.html'
})

export class BranchMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy, QueryParametersCallback {
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    //Dropdowns
    @ViewChild('branchsearch') branchsearch: CommonDropdownComponent;
    @ViewChild('chargeRateSearch') chargeRateSearch: CommonDropdownComponent;
    @ViewChild('logoTypeSearch') logoTypeSearch: CommonDropdownComponent;

    //Ellipsis
    @ViewChild('branchManagerSearch') branchManagerSearch: EllipsisComponent;
    @ViewChild('branchSecratarySearch') branchSecratarySearch: EllipsisComponent;
    @ViewChild('serviceAreaFBSearch') serviceAreaFBSearch: EllipsisComponent;
    @ViewChild('serviceAreaSearch') serviceAreaSearch: EllipsisComponent;

    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'BranchNumber', disabled: false, type: MntConst.eTypeCode, required: true, value: '' },
        { name: 'BranchName', disabled: false, type: MntConst.eTypeTextFree, required: true, value: '' },
        { name: 'BranchAddressLine1', disabled: false, type: MntConst.eTypeTextFree, required: true, value: '' },
        { name: 'BranchAddressLine2', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' },
        { name: 'BranchAddressLine3', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' },
        { name: 'BranchAddressLine4', disabled: false, type: MntConst.eTypeTextFree, required: true, value: '' },
        { name: 'BranchAddressLine5', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' },
        { name: 'BranchPostcode', disabled: false, type: MntConst.eTypeCode, required: true, value: '' },
        { name: 'GPSCoordinateX', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' },
        { name: 'GPSCoordinateY', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' },
        { name: 'RoutingGeonode', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'RoutingScore', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'RoutingSource', disabled: false, type: '', required: false, value: '' },
        { name: 'BranchVtxGeoCode', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'BranchManagerEmployeeCode', disabled: false, type: MntConst.eTypeCode, required: false, value: '' },
        { name: 'BranchManagerEmployeeSurname', disabled: true, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'BranchSecretaryEmployeeCode', disabled: false, type: MntConst.eTypeCode, required: false, value: '' },
        { name: 'BranchSecretaryEmployeeSurname', disabled: true, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'FBEVBranchServiceAreaCode', disabled: false, type: MntConst.eTypeCode, required: false, value: '' },
        { name: 'FBEVBranchServiceAreaDesc', disabled: true, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'BranchTelephone', disabled: false, type: MntConst.eTypeText, required: true, value: '' },
        { name: 'BranchFax', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'BranchEmail', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' },
        { name: 'BranchInternalTelephone', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'BranchInternalSalesTelephone', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'BranchInternalServiceTelephone', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'ContactName', disabled: false, type: MntConst.eTypeTextFree, required: true, value: '' },
        { name: 'ContactPosition', disabled: false, type: MntConst.eTypeText, required: true, value: '' },
        { name: 'ContactEmail', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' },
        { name: 'ServiceEmail', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' },
        { name: 'SalesEmail', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' },
        { name: 'NationalAccountBranch', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: 'LiveBranch', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: 'MailMergePrintAddress', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: 'DisablePostcodeChanges', disabled: false, type: '', required: false, value: '' },
        { name: 'EngineerRequiredInd', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: 'EnablePostCodeDefaulting', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: 'PropertyCareInd', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: 'ServiceAlignInd', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: 'EngineerServiceAreaCode', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'EngineerServiceAreaDesc', disabled: true, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'DOWBranchID', disabled: false, type: MntConst.eTypeCode, required: false, value: '' },
        { name: 'CostCentre', disabled: false, type: MntConst.eTypeCode, required: false, value: '' },
        { name: 'SMSCostCentre', disabled: false, type: MntConst.eTypeCode, required: false, value: '' },
        { name: 'OACostCentre', disabled: false, type: MntConst.eTypeCode, required: false, value: '' },
        { name: 'LanguageCode', disabled: false, type: MntConst.eTypeCode, required: true, value: '' },
        { name: 'LanguageDescription', disabled: true, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'LogoTypeCode', disabled: false, type: MntConst.eTypeText, required: true, value: '', isCutomDropdown: true, dropDown: 'logoTypeSearch' },
        { name: 'LogoTypeDesc', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'NextWasteConsignmentNoteNumber', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'WasteConsignmentNotePrefix', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'TaxAuthorityText', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'OutsideCityLimits', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: 'RegionCode', disabled: false, type: MntConst.eTypeCode, required: true, value: '' },
        { name: 'RegionDesc', disabled: true, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'ChargeRateCode', disabled: false, type: MntConst.eTypeCode, required: true, value: '', isCutomDropdown: true, dropDown: 'chargeRateSearch' },
        { name: 'ChargeRateDesc', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'DefaultType', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'cmdGeocode', disabled: false, type: '', required: false, value: 'Geocode' },
        { name: 'cmdSalesDefault', disabled: true, type: '', required: false, value: 'Default' },
        { name: 'cmdServiceDefault', disabled: true, type: '', required: false, value: 'Default' },
        { name: 'cmdAdmindDefault', disabled: true, type: '', required: false, value: 'Default' },
        //Conditional - vSCManpowerPlanning
        { name: '1WindowStart01', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowEnd01', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowDayInd01', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '2WindowStart01', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowEnd01', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowDayInd01', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '3WindowStart01', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowEnd01', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowDayInd01', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '1WindowStart02', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowEnd02', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowDayInd02', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '2WindowStart02', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowEnd02', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowDayInd02', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '3WindowStart02', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowEnd02', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowDayInd02', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '1WindowStart03', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowEnd03', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowDayInd03', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '2WindowStart03', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowEnd03', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowDayInd03', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '3WindowStart03', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowEnd03', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowDayInd03', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '1WindowStart04', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowEnd04', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowDayInd04', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '2WindowStart04', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowEnd04', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowDayInd04', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '3WindowStart04', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowEnd04', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowDayInd04', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '1WindowStart05', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowEnd05', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowDayInd05', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '2WindowStart05', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowEnd05', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowDayInd05', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '3WindowStart05', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowEnd05', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowDayInd05', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '1WindowStart06', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowEnd06', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowDayInd06', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '2WindowStart06', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowEnd06', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowDayInd06', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '3WindowStart06', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowEnd06', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowDayInd06', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '1WindowStart07', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowEnd07', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '1WindowDayInd07', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '2WindowStart07', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowEnd07', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '2WindowDayInd07', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: '3WindowStart07', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowEnd07', disabled: false, type: MntConst.eTypeTime, required: true, value: '' },
        { name: '3WindowDayInd07', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: 'StdHoursPerDay01', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'StdHoursPerDay02', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'StdHoursPerDay03', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'MaxOTPerDay01', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'MaxOTPerDay02', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'MaxOTPerDay03', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'MaxHoursPerWeek01', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'MaxHoursPerWeek02', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'MaxHoursPerWeek03', disabled: false, type: MntConst.eTypeText, required: false, value: '' },
        { name: 'TotalWindowTime01', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'TotalWindowTime02', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'TotalWindowTime03', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'StandardBreakTime01', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'StandardBreakTime02', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'StandardBreakTime03', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'PersonalDrivingTime01', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'PersonalDrivingTime02', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'PersonalDrivingTime03', disabled: false, type: MntConst.eTypeTime, required: false, value: '' },
        { name: 'AvgDrivingWorkDay01', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'AvgDrivingWorkDay02', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'AvgDrivingWorkDay03', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'AverageSickness01', disabled: false, type: MntConst.eTypeDecimal2, required: false, value: '' },
        { name: 'AverageSickness02', disabled: false, type: MntConst.eTypeDecimal2, required: false, value: '' },
        { name: 'AverageSickness03', disabled: false, type: MntConst.eTypeDecimal2, required: false, value: '' },
        { name: 'ManpowerDetailInd', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' }
    ];

    public ellipsis: any = {
        branchManagerSearch: {
            isDisabled: false,
            isCloseButtonEnabled: true,
            isHeaderEnabled: true,
            childparams: {
                parentMode: 'LookUp-Manager'
            },
            component: EmployeeSearchComponent
        },
        branchSecratarySearch: {
            isDisabled: false,
            isCloseButtonEnabled: true,
            isHeaderEnabled: true,
            childparams: {
                parentMode: 'LookUp-Secretary'
            },
            component: EmployeeSearchComponent
        },
        serviceAreaFBSearch: {
            isDisabled: false,
            isCloseButtonEnabled: true,
            isHeaderEnabled: true,
            childparams: {
                parentMode: 'LookUp-FB',
                FBEVBranchServiceAreaCode: ''
            },
            component: BranchServiceAreaSearchComponent
        },
        serviceAreaSearch: {
            isDisabled: false,
            isCloseButtonEnabled: true,
            isHeaderEnabled: true,
            childparams: {
                parentMode: 'LookUp-ServiceEngineer',
                ServiceBranchNumber: '',
                BranchName: ''
            },
            component: BranchServiceAreaSearchComponent
        }
    };
    public lookUpRefs: any = {};

    public isInit: boolean = false;
    public isUpdateEnabled: boolean = true;
    public queryParams: any;
    public cachedData: any = {};
    public pageMode: string = '';
    public riGridHandle: any = '';
    public promptContent: string = MessageConstant.Message.ConfirmRecord;
    public promptContentDel: string = MessageConstant.Message.DeleteRecord;
    public modalConfig: any = { backdrop: 'static', keyboard: true };
    public isBranchDataDefaulted: boolean = true;
    public isSalesDefault: boolean = true;
    public isServiceDefault: boolean = true;
    public isAdmindDefault: boolean = true;
    public isdorpdownVisible: boolean = true;
    public dropDown: any = {
        menu: [],
        branchSearch: {
            isRequired: true,
            isDisabled: false,
            isTriggerValidate: false,
            isFirstItemSelected: true,
            params: {
                module: 'structure',
                method: 'it-functions/search',
                operation: 'Business/iCABSBBranchSearch'
            },
            active: { id: '', text: '' },
            displayFields: ['BranchNumber', 'BranchName']
        },
        chargeRateSearch: {
            isRequired: true,
            isDisabled: false,
            isTriggerValidate: false,
            params: {
                module: 'rates',
                method: 'bill-to-cash/search',
                operation: 'Business/iCABSBChargeRateSearch'
            },
            active: { id: '', text: '' },
            displayFields: ['ChargeRateCode', 'ChargeRateDesc']
        },
        logoTypeSearch: {
            isRequired: false,
            isDisabled: false,
            isTriggerValidate: false,
            params: {
                module: 'invoicing',
                method: 'bill-to-cash/search',
                operation: 'Business/iCABSBLogoTypeSearch'
            },
            active: { id: '', text: '' },
            displayFields: ['LogoTypeCode', 'LogoTypeDesc']
        }
    };
    public uiDisplay: any = {
        trVtxGeoCode: false,
        tdOutsideCityLimits: false,
        tdOutsideCityLimitslab: false,
        trDOWBranch: false,
        tdDisablePostcodeChangesLabel: false,
        tdDisablePostcodeChanges: false,
        cmdGeocode: true,
        trGeonode: true,
        trGPSScore: true,
        trServiceAlignInd: true,
        trEngineerRequiredInd: false,
        trEngineerServiceAreaCode: false,
        PropertyCareInd: false,
        tdPropertyCareIndlbl: false
    };
    public tempCustomBusinessObjectAdditionalPostData: string = '';

    public xhrParams: any = {
        module: 'structure',
        method: 'it-functions/admin',
        operation: 'Business/iCABSBBranchMaintenance'
    };

    public riTab: RiTab;
    public tab: any = {
        tab1: { id: 'grdBranchAddress', name: 'Branch Details', visible: true, active: true },
        tab2: { id: 'grdBranchContact', name: 'Contact Details', visible: true, active: false },
        tab3: { id: 'grdBranchContact2', name: 'Other Details', visible: true, active: false },
        tab4: { id: 'grdBranchWorkingHours', name: 'Working Hours', visible: false, active: false }
    };

    public refLookUp: any = {
        Region: {
            table: 'Region',
            query: { BusinessCode: '', RegionCode: '' },
            fields: ['RegionDesc']
        },
        ChargeRate: {
            table: 'ChargeRate',
            query: { BusinessCode: '', ChargeRateCode: '' },
            fields: ['ChargeRateDesc']
        },
        BranchManager: {
            table: 'Employee',
            query: { BusinessCode: '', EmployeeCode: '' },
            fields: ['EmployeeSurname']
        },
        BranchSecretary: {
            table: 'Employee',
            query: { BusinessCode: '', EmployeeCode: '' },
            fields: ['EmployeeSurname']
        },
        Language: {
            table: 'Language',
            query: { BusinessCode: '', LanguageCode: '' },
            fields: ['LanguageDescription']
        },
        LogoType: {
            table: 'LogoType',
            query: { BusinessCode: '', LogoTypeCode: '' },
            fields: ['LogoTypeDesc']
        },
        BranchServiceArea: {
            table: 'BranchServiceArea',
            query: { BusinessCode: '', BranchNumber: '', BranchServiceAreaCode: '' },
            fields: ['BranchServiceAreaDesc']
        },
        Branch: {
            table: 'BranchServiceArea',
            query: { BusinessCode: '', BranchServiceAreaCode: '' },
            fields: ['BranchServiceAreaDesc']
        }
    };

    constructor(injector: Injector, private _ls: LocalStorageService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBBRANCHMAINTENANCE;
        this.setURLQueryParameters(this);
        this.browserTitle = this.pageTitle = 'Branch Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.genCtrlMap();
    }

    ngAfterContentInit(): void {
        this.isInit = true;
        this.init();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public getURLQueryParameters(param: any): void {
        this.queryParams = param;
        if (this.isInit) { this.init(); }
    }

    public init(): void {
        this.riTab = new RiTab(this.tab, this.utils);
        this.tab = this.riTab.tabObject;
        this.uiForm.reset();

        this.pageParams.vBusinessCode = this.utils.getBusinessCode();
        this.pageParams.vCountryCode = this.utils.getCountryCode();
        this.parentMode = this.riExchange.getParentMode();

        this.updateButton();
        this.getSysCharDtetails();
    }

    public updateButton(): void {
        this.utils.getTranslatedval('Geocode').then((res: string) => { this.setControlValue('cmdGeocode', res); });
        this.utils.getTranslatedval('Default').then((res: string) => {
            this.setControlValue('cmdSalesDefault', res);
            this.setControlValue('cmdServiceDefault', res);
            this.setControlValue('cmdAdmindDefault', res);
        });
    }

    //SpeedScript
    public getSysCharDtetails(): void {
        this.pageParams.vEnablePostcodeDefaulting = false;
        this.pageParams.vSCCapitalFirstLtr = false;
        this.pageParams.vEnableRouteOptimisation = false;
        this.pageParams.vEnableAccountLogo = false;
        this.pageParams.vSCEnableInstallsRemovals = false;
        this.pageParams.vSCManpowerPlanning = false;
        this.pageParams.vVtxGeoCode = false;
        this.pageParams.vDOWSentriconParams = '';
        this.pageParams.vEnableDOWSentricon = false;
        this.pageParams.vSCEnablePropertyCare = false;
        this.pageParams.vDisableBranchPostcodeChanges = false;

        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharEnablePostcodeDefaulting, //Required
            this.sysCharConstants.SystemCharDisableFirstCapitalOnAddressContact, //Required
            this.sysCharConstants.SystemCharEnableRouteOptimisationSoftwareIntegration, //Required
            this.sysCharConstants.SystemCharEnableAccountLogo, //Required
            this.sysCharConstants.SystemCharEnableInstallsRemovals, //Required
            this.sysCharConstants.SystemCharManpowerPlanning, //Required
            this.sysCharConstants.SystemCharVertexEnabled, //Required
            this.sysCharConstants.SystemCharEnablePropertyCare, //Required
            this.sysCharConstants.SystemCharDisableBranchPostcodeChanges //Required
        ];

        let sysCharIP = {
            module: this.xhrParams.module,
            operation: this.xhrParams.operation,
            action: 0,
            businessCode: this.utils.getBusinessCode(),
            countryCode: this.utils.getCountryCode(),
            SysCharList: sysCharList.toString()
        };

        this.speedScript.sysCharPromise(sysCharIP).then((data) => {
            let records = data.records;
            if (records) {
                this.pageParams.vEnablePostcodeDefaulting = records[0].Required;
                this.pageParams.vSCCapitalFirstLtr = records[1].Required;
                this.pageParams.vEnableRouteOptimisation = records[2].Required;
                this.pageParams.vEnableAccountLogo = records[3].Required;
                this.pageParams.vSCEnableInstallsRemovals = records[4].Required;
                this.pageParams.vSCManpowerPlanning = records[5].Required;
                this.pageParams.vVtxGeoCode = records[6].Required;
                this.pageParams.vSCEnablePropertyCare = records[7].Required;
                this.pageParams.vDisableBranchPostcodeChanges = records[8].Required;

                /*Find Dow Sentricon Business Registry*/
                this.LookUp.i_GetBusinessRegistryValue(this.utils.getBusinessCode(), 'DOW Sentricon', 'Enable DOW Sentricon').then((regData) => {
                    this.pageParams.vDOWSentriconParams = regData;
                    if (this.pageParams.vDOWSentriconParams === 'YES') this.pageParams.vEnableDOWSentricon = true;

                    this.pageParams.vbVtxGeoCode = this.pageParams.vVtxGeoCode ? true : false;
                    this.pageParams.vbEnableDOWSentricon = this.pageParams.vEnableDOWSentricon ? true : false;
                    this.pageParams.vbDisableBranchPostcodeChanges = this.pageParams.vDisableBranchPostcodeChanges ? true : false;
                    this.pageParams.EnableRouteOptimisation = this.pageParams.vEnableRouteOptimisation ? true : false;
                    this.pageParams.EnableAccountLogo = this.pageParams.vEnableAccountLogo ? true : false;
                    this.pageParams.EnableInstallsRemovals = this.pageParams.vSCEnableInstallsRemovals ? true : false;
                    this.pageParams.EnableMPPlanning = this.pageParams.vSCManpowerPlanning ? true : false;
                    this.pageParams.SCEnablePropertyCare = this.pageParams.vSCEnablePropertyCare ? true : false;

                    if (this.pageParams.EnableMPPlanning) {
                        this.riTab.TabAdd('Working Hours');
                    }

                    if (this.pageParams.vbVtxGeoCode) {
                        this.uiDisplay.trVtxGeoCode = true;
                        this.uiDisplay.tdOutsideCityLimits = true;
                        this.uiDisplay.tdOutsideCityLimitslab = true;
                        this.setRequiredStatus('BranchVtxGeoCode', true);
                    } else {
                        this.uiDisplay.trVtxGeoCode = false;
                        this.uiDisplay.tdOutsideCityLimits = false;
                        this.uiDisplay.tdOutsideCityLimitslab = false;
                        this.setRequiredStatus('BranchVtxGeoCode', false);
                    }

                    if (this.pageParams.vbEnableDOWSentricon) {
                        this.uiDisplay.trDOWBranch = true;
                    }

                    if (this.pageParams.vbDisableBranchPostcodeChanges) {
                        this.uiDisplay.tdDisablePostcodeChangesLabel = true;
                        this.uiDisplay.tdDisablePostcodeChanges = true;
                    }

                    if (this.pageParams.vEnableAccountLogo) {
                        this.setRequiredStatus('LogoTypeCode', true); //MntConst.eTypeText
                        this.dropDown.logoTypeSearch.isRequired = true;
                    } else {
                        this.setRequiredStatus('LogoTypeCode', false); //MntConst.eTypeTextFree
                        this.dropDown.logoTypeSearch.isRequired = false;
                    }

                    if (this.pageParams.vbEnableDOWSentricon) {
                        // this.riMaintenance.AddTableField('DOWBranchID', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
                    }

                    if (!(this.pageParams.EnableRouteOptimisation)) {
                        this.uiDisplay.cmdGeocode = false;
                        this.uiDisplay.trGeonode = false;
                        this.uiDisplay.trGPSScore = false;
                        this.uiDisplay.trServiceAlignInd = false;
                    }

                    this.onLoad();
                });
            }
        });
    }

    //Branch Dropdown - Start
    public onBranchDataDefaulted(obj: any): void {
        if (obj && obj.hasOwnProperty('firstRow') && this.isBranchDataDefaulted) {
            let branchNumber: string = (obj.firstRow.BranchNumber !== '') ? obj.firstRow.BranchNumber.toString() : '';
            let branchName: string = (obj.firstRow.BranchName) ? obj.firstRow.BranchName : '';
            this.setControlValue('BranchNumber', branchNumber);
            this.setControlValue('BranchName', branchName);
            this.ellipsis.serviceAreaSearch.childparams.ServiceBranchNumber = branchNumber;
            this.ellipsis.serviceAreaSearch.childparams.BranchName = branchName;
            this.onLoad();
        }
        this.isBranchDataDefaulted = true;
    }
    public onBranchDataReceived(obj: any): void {
        if (obj && obj.BranchNumber !== null && obj.BranchNumber !== undefined) {
            let branchname = (obj.hasOwnProperty('BranchName') ? obj.BranchName : obj.Object.BranchName);
            this.setControlValue('BranchNumber', obj.BranchNumber);
            this.setControlValue('BranchName', branchname);
            this.ellipsis.serviceAreaSearch.childparams.ServiceBranchNumber = obj.BranchNumber;
            this.ellipsis.serviceAreaSearch.childparams.BranchName = branchname;
            this.onLoad();
        }
    }
    //Dropdown - End

    public onLoad(): void {
        if (this.branchsearch) {
            this.pageMode = MntConst.eModeUpdate;
            if (this.getControlValue('BranchNumber') !== '') {
                this.updateFetch();
            } else {
                this.addFetch();
            }
            this.fnHideShowFields();
        }
    }

    public updateFetch(): void {
        this.pageMode = MntConst.eModeUpdate;
        this.disableControl('BranchNumber', true);
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set('BranchNumber', this.getControlValue('BranchNumber'));
        if (this.pageParams.EnableMPPlanning) {
            search.set('ManpowerPlanning', this.utils.convertCheckboxValueToRequestValue(this.pageParams.EnableMPPlanning));
        }
        this.httpService.xhrGet(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.cachedData = data;
                this.populateForm(this.cachedData);
                this.dropDown.branchSearch.active = {
                    id: this.getControlValue('BranchNumber'),
                    text: this.getControlValue('BranchNumber') + ' - ' + this.getControlValue('BranchName')
                };
                this.disableControl('RoutingSource', true);
                this.disableControl('cmdGeocode', true);

                this.btnDefaultEnableDisable();

                this.riMaintenanceFetch();
                this.riMaintenanceBeforeMode();
            }
        });
    }

    public addFetch(): void {
        this.pageMode = MntConst.eModeAdd;
        this.uiForm.reset();
        this.setControlValue('RoutingSource', '');
        this.setControlValue('BranchNumber', '');
        this.disableControl('BranchNumber', false);
        this.dropDown.branchSearch.active = { id: '', text: '' };

        this.btnDefaultEnableDisable();

        this.riMaintenanceBeforeMode();
        this.setControlValue('LiveBranch', true);
    }

    public populateForm(data: any): void {
        for (let i in data) {
            if (data.hasOwnProperty(i)) {
                this.setControlValue(i, data[i]);
            }
        }
        this.updateButton();
        this.doLookup();
    }

    public doLookup(): void {
        let refName: string = '';
        let lookupIP1: Array<any> = [];
        let lookupIP2: Array<any> = [];

        refName = 'Region';
        this.refLookUp[refName].query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp[refName].query.RegionCode = this.getControlValue('RegionCode');
        lookupIP1.push(this.refLookUp[refName]);

        refName = 'ChargeRate';
        this.refLookUp[refName].query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp[refName].query.ChargeRateCode = this.getControlValue('ChargeRateCode');
        lookupIP1.push(this.refLookUp[refName]);

        refName = 'BranchManager';
        this.refLookUp[refName].query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp[refName].query.EmployeeCode = this.getControlValue('BranchManagerEmployeeCode'); //BranchManagerEmployeeSurname
        lookupIP1.push(this.refLookUp[refName]);

        refName = 'BranchSecretary';
        this.refLookUp[refName].query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp[refName].query.EmployeeCode = this.getControlValue('BranchSecretaryEmployeeCode'); //BranchSecretaryEmployeeSurname
        lookupIP1.push(this.refLookUp[refName]);

        this.LookUp.lookUpPromise(lookupIP1).then((data) => {
            if (data) {
                if (this.refLookUp.Region.query.RegionCode) { this.utils.setLookupValue(this, this.refLookUp.Region.fields, data[0]); }
                if (this.refLookUp.ChargeRate.query.ChargeRateCode) { this.utils.setLookupValue(this, this.refLookUp.ChargeRate.fields, data[1]); }
                if (this.getControlValue('BranchManagerEmployeeCode')) {
                    this.utils.setLookupValue(this, this.refLookUp.BranchManager.fields, data[2], ['BranchManagerEmployeeSurname']);
                } else {
                    this.setControlValue('BranchManagerEmployeeSurname', '');
                }
                if (this.getControlValue('BranchSecretaryEmployeeCode')) {
                    this.utils.setLookupValue(this, this.refLookUp.BranchSecretary.fields, data[3], ['BranchSecretaryEmployeeSurname']);
                } else {
                    this.setControlValue('BranchSecretaryEmployeeSurname', '');
                }
            }
        });

        refName = 'Language';
        this.refLookUp[refName].query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp[refName].query.LanguageCode = this.getControlValue('LanguageCode');
        lookupIP2.push(this.refLookUp[refName]);

        refName = 'LogoType';
        this.refLookUp[refName].query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp[refName].query.LogoTypeCode = this.getControlValue('LogoTypeCode');
        lookupIP2.push(this.refLookUp[refName]);

        refName = 'BranchServiceArea';
        this.refLookUp[refName].query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp[refName].query.BranchNumber = this.getControlValue('BranchNumber');
        this.refLookUp[refName].query.BranchServiceAreaCode = this.getControlValue('EngineerServiceAreaCode'); //EngineerServiceAreaDesc
        lookupIP2.push(this.refLookUp[refName]);

        refName = 'Branch';
        this.refLookUp[refName].query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp[refName].query.BranchServiceAreaCode = this.getControlValue('FBEVBranchServiceAreaCode'); //FBEVBranchServiceAreaDesc
        lookupIP2.push(this.refLookUp[refName]);

        this.LookUp.lookUpPromise(lookupIP2).then((data) => {
            if (data) {
                if (this.refLookUp.Language.query.LanguageCode) { this.utils.setLookupValue(this, this.refLookUp.Language.fields, data[0]); }
                if (this.refLookUp.LogoType.query.LogoTypeCode) { this.utils.setLookupValue(this, this.refLookUp.LogoType.fields, data[1]); }
                if (this.getControlValue('EngineerServiceAreaCode')) {
                    this.utils.setLookupValue(this, this.refLookUp.BranchServiceArea.fields, data[2], ['EngineerServiceAreaDesc']);
                } else {
                    this.setControlValue('EngineerServiceAreaDesc', '');
                }
                if (this.getControlValue('FBEVBranchServiceAreaCode')) {
                    this.utils.setLookupValue(this, this.refLookUp.Branch.fields, data[3], ['FBEVBranchServiceAreaDesc']);
                } else {
                    this.setControlValue('FBEVBranchServiceAreaDesc', '');
                }
                setTimeout(() => { this.updateCustomDropDowns(); }, 500);
            }
        });
    }

    public updateCustomDropDowns(): void {
        for (let i = 0; i < this.controls.length; i++) {
            if (this.controls[i].hasOwnProperty('isCutomDropdown')) {
                if (this.controls[i].isCutomDropdown) {
                    if (this.getControlValue(this.dropDown[this.controls[i].dropDown].displayFields[0])) {
                        this.dropDown[this.controls[i].dropDown].active = {
                            id: this.getControlValue(this.dropDown[this.controls[i].dropDown].displayFields[0]),
                            text: this.getControlValue(this.dropDown[this.controls[i].dropDown].displayFields[0]) + ' - ' + this.getControlValue(this.dropDown[this.controls[i].dropDown].displayFields[1])
                        };
                    } else {
                        this.dropDown[this.controls[i].dropDown].active = { id: '', text: '' };
                    }
                }
            }
        }
    }

    //Buttons
    public save(): void {
        this.riExchange.validateForm(this.uiForm);

        for (let i in this.dropDown) {
            if (this.dropDown.hasOwnProperty(i)) {
                if (this.dropDown[i].hasOwnProperty('isRequired') && this.dropDown[i].isRequired) {
                    this.dropDown[i].isTriggerValidate = true;
                }
            }
        }

        setTimeout(() => {
            this.utils.highlightTabs();

            if (this.uiForm.status === 'VALID') {
                this.riTab.TabFocus(1);
                if (this.pageMode === MntConst.eModeAdd) {
                    this.add();
                } else {
                    this.update();
                }
            } else {
                if (this.pageParams.EnableMPPlanning) {
                    if (!this.fnValidateWorkingHourFieldsEntry()) {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.enterAllWorkingHrs));
                        return;
                    }
                    if (!this.fnValidateWorkingHourFields()) {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.startTimeBeforeEndTime));
                        return;
                    }
                }

                for (let control in this.uiForm.controls) {
                    if (this.uiForm.controls[control].invalid) {
                        this.logger.log('   >> Invalid:', control, this.uiForm.controls[control].errors, this.uiForm.controls[control]);
                    }
                }
            }
        }, 500);
    }
    public cancel(): void {
        if (this.pageMode === MntConst.eModeUpdate) {
            this.populateForm(this.cachedData);
        } else {
            this.setControlValue('BranchNumber', this.cachedData.BranchNumber);
            this.defaultToUpdateMode();
            this.populateForm(this.cachedData);
        }
        this.markAsPristine();
        this.riTab.TabsToNormal();
    }
    public checkFormDirty(): void {
        if (!this.uiForm.dirty) {
            this.isdorpdownVisible = false;
            this.switchToAddMode();
        } else {
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.RouteAway, null, this.switchToAddMode.bind(this), null));
        }
    }
    public add(): void {
        this.riMaintenanceAfterSave();
    }
    public update(): void {
        this.riMaintenanceBeforeSave();
    }
    public delete(): void {
        function deleteCanceled(): void { this.pageMode = MntConst.eModeUpdate; }
        function deleteSuccess(): void {
            let index: number = -1;
            let deletedBranchNumber: any = parseInt(this.getControlValue('BranchNumber'), 10);

            for (let i = 0; i < this.branchsearch.requestdata.length; i++) {
                if (this.branchsearch.requestdata[i].BranchNumber === deletedBranchNumber) {
                    index = i;
                    break;
                }
            }
            this.branchsearch.requestdata.splice(index, 1);

            this.setControlValue('BranchNumber', this.branchsearch.requestdata[0].BranchNumber);
            this.setControlValue('BranchName', this.branchsearch.requestdata[0].BranchName);
            this.onLoad();
        }
        function deleteXHR(): void {
            if (this.pageMode === MntConst.eModeDelete) {
                this.ajaxSource.next(this.ajaxconstant.START);
                let search: QueryParams = new QueryParams();
                search.set(this.serviceConstants.Action, '3');
                search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
                search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

                let formData: any = {};
                formData['BranchNumber'] = this.getControlValue('BranchNumber');

                this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.markAsPristine();
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        let tempModal: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully);
                        tempModal.closeCallback = deleteSuccess.bind(this);
                        this.modalAdvService.emitMessage(tempModal);
                    }
                });
            }
        }
        this.pageMode = MntConst.eModeDelete;
        this.modalAdvService.emitPrompt(new ICabsModalVO(this.promptContentDel, null, deleteXHR.bind(this), deleteCanceled.bind(this)));
    }
    public btnDefaultEnableDisable(): void {
        if (this.pageMode === MntConst.eModeAdd) {
            let isCheck: boolean = false;
            for (let i = 1; i <= 7; i++) {
                if (this.getControlValue('1WindowStart0' + i)) {
                    isCheck = true;
                } else {
                    isCheck = false;
                    break;
                }
                if (this.getControlValue('1WindowEnd0' + i)) {
                    isCheck = true;
                } else {
                    isCheck = false;
                    break;
                }
            }
            this.isSalesDefault = !isCheck;

            isCheck = false;
            for (let i = 1; i <= 7; i++) {
                if (this.getControlValue('2WindowStart0' + i)) {
                    isCheck = true;
                } else {
                    isCheck = false;
                    break;
                }
                if (this.getControlValue('2WindowEnd0' + i)) {
                    isCheck = true;
                } else {
                    isCheck = false;
                    break;
                }
            }
            this.isServiceDefault = !isCheck;

            isCheck = false;
            for (let i = 1; i <= 7; i++) {
                if (this.getControlValue('3WindowStart0' + i)) {
                    isCheck = true;
                } else {
                    isCheck = false;
                    break;
                }
                if (this.getControlValue('3WindowEnd0' + i)) {
                    isCheck = true;
                } else {
                    isCheck = false;
                    break;
                }
            }
            this.isAdmindDefault = !isCheck;
        } else {
            this.isSalesDefault = false;
            this.isServiceDefault = false;
            this.isAdmindDefault = false;
        }
        this.disableControl('cmdSalesDefault', this.isSalesDefault);
        this.disableControl('cmdServiceDefault', this.isServiceDefault);
        this.disableControl('cmdAdmindDefault', this.isAdmindDefault);
    }

    public riMaintenanceFetch(): void {
        this.fnCheckShowFields();
    }
    public riMaintenanceBeforeMode(): void {
        if (this.pageMode === MntConst.eModeUpdate || this.pageMode === MntConst.eModeAdd) {
            this.disableControl('cmdGeocode', false);
            this.disableControl('RoutingSource', true);
            if (this.pageParams.EnableMPPlanning) {
                if (this.pageMode === MntConst.eModeUpdate) {
                    this.getBranchDefault();
                }
            }
        }
    }
    public riMaintenanceBeforeSave(): void {
        if (this.pageParams.EnableMPPlanning) {
            if (!this.fnValidateWorkingHourFieldsEntry()) {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.enterAllWorkingHrs));
                return;
            }
            if (!this.fnValidateWorkingHourFields()) {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.startTimeBeforeEndTime));
                return;
            }
        }

        if (this.pageParams.EnableRouteOptimisation && this.getControlValue('RoutingSource') === '') {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Function'] = 'GetGeocodeAddress';
            formData['AddressLine1'] = this.getControlValue('AddressLine1');
            formData['AddressLine2'] = this.getControlValue('AddressLine2');
            formData['AddressLine3'] = this.getControlValue('AddressLine3');
            formData['AddressLine4'] = this.getControlValue('AddressLine4');
            formData['AddressLine5'] = this.getControlValue('AddressLine5');
            formData['Postcode'] = this.getControlValue('BranchPostcode');
            formData['GPSX'] = this.getControlValue('GPSCoordinateX');
            formData['GPSY'] = this.getControlValue('GPSCoordinateY');
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    if (data['AddressError'] !== 'Error') {
                        this.setControlValue('RoutingGeonode', data['Node']);
                        this.setControlValue('RoutingScore', data['Score']);
                        this.setControlValue('GPSCoordinateX', data['GPSX']);
                        this.setControlValue('GPSCoordinateY', data['GPSY']);
                        if (data['Score'] > 0) {
                            this.setControlValue('RoutingScore', 'T');
                        } else {
                            this.setControlValue('RoutingScore', '');
                        }
                        this.riMaintenanceAfterSave();
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.unableToGeocode));
                        return;
                    }
                }
            });
        } else {
            this.riMaintenanceAfterSave();
        }
    }
    public riMaintenanceAfterSave(): void {
        function doXHR(): void {
            let action: number = 2;
            if (this.pageMode === MntConst.eModeAdd) { action = 1; }

            this.ajaxSource.next(this.ajaxconstant.START);
            let obj: any = this.createXHRObj(action);
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, obj.search, obj.formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.markAsPristine();
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.logger.log('Saved:', this.pageMode, action, data);
                    for (let i in data) {
                        if (data.hasOwnProperty(i)) {
                            this.setControlValue(i, data[i]);
                        }
                    }

                    this.disableControl('cmdGeocode', true);

                    let tempModal: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.SavedSuccessfully);
                    tempModal.closeCallback = this.defaultToUpdateMode.bind(this);
                    if (this.pageMode === MntConst.eModeAdd) { tempModal.closeCallback = this.fetchDataAfterAdd.bind(this); }
                    this.modalAdvService.emitMessage(tempModal);
                }
            });
        }

        this.modalAdvService.emitPrompt(new ICabsModalVO(this.promptContent, null, doXHR.bind(this), null));
    }

    public fetchDataAfterAdd(): void {
        this.isdorpdownVisible = true;
        this.pageMode = MntConst.eModeUpdate;
        this.parentMode = 'UPDATE';
        let branch: any = {
            BranchNumber: this.getControlValue('BranchNumber'),
            BranchName: this.getControlValue('BranchName')
        };
        this.onBranchDataReceived(branch);
    }

    public switchToAddMode(): void {
        this.uiForm.reset();
        for (let i in this.dropDown) {
            if (this.dropDown.hasOwnProperty(i)) {
                if (this.dropDown[i].hasOwnProperty('active')) {
                    this.dropDown[i].active = { id: '', text: '' };
                }
            }
        }
        this.parentMode = 'ADD';
        this.pageMode = MntConst.eModeAdd;
        this.setControlValue('BranchNumber', '');
        this.updateButton();
        this.onLoad();
    }
    public defaultToUpdateMode(): void {
        this.pageMode = MntConst.eModeUpdate;
        this.isdorpdownVisible = true;
        this.parentMode = 'UPDATE';
        this.onLoad();
    }

    public createXHRObj(action: number): any {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, action.toString());
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (this.pageParams.EnableMPPlanning) {
            search.set('ManpowerPlanning', this.utils.convertCheckboxValueToRequestValue(this.pageParams.EnableMPPlanning));
        }

        let formData: any = {};
        let len = this.controls.length;
        for (let i = 0; i < len; i++) {
            let ctrlName: string = this.controls[i].name;
            let ctrlValue: any = this.getControlValue(this.controls[i].name, true);
            if (this.controls[i].type === MntConst.eTypeCheckBox) {
                ctrlValue = this.utils.convertCheckboxValueToRequestValue(ctrlValue);
            }
            formData[ctrlName] = ctrlValue;
        }
        this.cachedData = formData;
        return { search: search, formData: formData };
    }

    public getBranchDefault(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        formData['Function'] = 'GetBranchDefault';
        formData['BranchNumber'] = this.getControlValue('BranchNumber');
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                for (let i in data) {
                    if (data.hasOwnProperty(i)) {
                        this.setControlValue(i, data[i]);
                    }
                }
                this.btnDefaultEnableDisable();
            }
        });
    }
    public fnHideShowFields(): void {
        if (this.pageParams.EnableInstallsRemovals) {
            this.uiDisplay.trEngineerRequiredInd = true;
            this.uiDisplay.trEngineerServiceAreaCode = false;
        } else {
            this.uiDisplay.trEngineerRequiredInd = false;
            this.uiDisplay.trEngineerServiceAreaCode = false;
        }
        if (this.pageParams.SCEnablePropertyCare) {
            this.uiDisplay.PropertyCareInd = true;
            this.uiDisplay.tdPropertyCareIndlbl = true;
        } else {
            this.uiDisplay.PropertyCareInd = false;
            this.uiDisplay.tdPropertyCareIndlbl = false;
        }
    }
    public fnCheckShowFields(): void {
        if (this.getControlValue('EngineerRequiredInd', true)) {
            this.uiDisplay.trEngineerServiceAreaCode = true;
            this.setRequiredStatus('EngineerServiceAreaCode', true);
        } else {
            this.uiDisplay.trEngineerServiceAreaCode = false;
            this.setRequiredStatus('EngineerServiceAreaCode', false);
            this.setControlValue('EngineerServiceAreaCode', '');
            this.setControlValue('EngineerServiceAreaDesc', '');
        }
    }
    public fnUpdateDefault(): Promise<any> {
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        formData['Function'] = 'UpdateDefault';
        formData['BranchNumber'] = this.getControlValue('BranchNumber');
        formData['DefaultType'] = this.getControlValue('DefaultType');
        formData['TotalWindowTime01'] = this.getControlValue('TotalWindowTime01');
        formData['TotalWindowTime02'] = this.getControlValue('TotalWindowTime02');

        return this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                if (data['DisplayMessage'] !== '') {
                    this.modalAdvService.emitMessage(new ICabsModalVO(data['DisplayMessage']));
                    return true;
                } else {
                    return false;
                }
            }
        });
    }
    public fnProcessTimeString(vbTimeField: any): void {
        let vbTime: any;
        let vbDurationHours;
        let vbDurationMinutes;


        let isError: boolean = false;
        let vbTimeSeparator = ':';

        vbTime = this.getControlValue(vbTimeField);
        vbTime = vbTime.replace(vbTimeSeparator, '');
        if (isNaN(parseInt(this.getControlValue(vbTimeField), 10))) {
            isError = true;
        }
        if (!isError && (vbTime.length < 4 || vbTime.length > 7)) {
            isError = true;
        } else if (!isError) {
            if (!isError && vbTime === 0) {
                isError = true;
            }
            if (!isError) {
                vbDurationHours = this.utils.mid(vbTime, 1, this.utils.len(vbTime) - 2);
                vbDurationMinutes = this.utils.Right(vbTime, 2);
                if (vbDurationMinutes > 59) {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.vbDurationMinutes));
                    isError = true;
                } else {
                }
            }
        }
        if (!isError) {
            let vbTimeFormat = vbDurationHours + ':' + vbDurationMinutes;
            this.setControlValue(vbTimeField, vbTimeFormat);
        } else {
            this.setControlValue(vbTimeField, '');
        }
    }
    public setRoutingSource(): void {
        let decGPSX: number = 0;
        let decGPSY: number = 0;
        let intRoutingScore: number = 0;

        if (isNaN(parseInt(this.getControlValue('GPSCoordinateX'), 10))) {
            decGPSX = this.getControlValue('GPSCoordinateX');
        } else {
            decGPSX = 0;
        }
        if (isNaN(parseInt(this.getControlValue('GPSCoordinateY'), 10))) {
            decGPSY = this.getControlValue('GPSCoordinateY');
        } else {
            decGPSY = 0;
        }
        if (isNaN(parseInt(this.getControlValue('RoutingScore'), 10))) {
            intRoutingScore = this.getControlValue('RoutingScore');
        } else {
            intRoutingScore = 0;
        }
        if (intRoutingScore === 0 || decGPSY === 0 || decGPSX === 0) {
            this.setControlValue('RoutingSource', '');
        } else {
            this.setControlValue('RoutingSource', 'M');
        }
    }
    public fnValidateWorkingHourFields(): boolean {
        for (let i = 1; i <= 3; i++) {
            for (let j = 1; j <= 7; j++) {
                let obj1 = parseInt(this.getControlValue(i + 'WindowStart0' + j), 10);
                let obj2 = parseInt(this.getControlValue(i + 'WindowEnd0' + j), 10);

                if ((obj1 > obj2) || ((obj1 === obj2 && obj1 !== 0 && obj2 !== 0))) {
                    return false;
                }
            }
        }
        return true;
    }
    public fnValidateWorkingHourFieldsEntry(): boolean {
        for (let i = 1; i <= 3; i++) {
            for (let j = 1; j <= 7; j++) {
                let obj1 = this.getControlValue(i + 'WindowStart0' + j);
                let obj2 = this.getControlValue(i + 'WindowEnd0' + j);

                if ((obj1 === '' || obj1 === null) || (obj2 === '' || obj2 === null)) {
                    return false;
                }
            }
        }
        return true;
    }

    public onChangeEngineerRequiredInd(): void {
        this.fnCheckShowFields();
    }
    public onBlurEngineerServiceAreaCode(): void {
        if (this.getControlValue('EngineerServiceAreaCode') === '') {
            this.setControlValue('EngineerServiceAreaDesc', '');
        }
    }
    public onClickcmdGeocode(): void {
        let url: string = '#' + GoogleMapPagesModuleRoutes.ICABSAROUTINGSEARCH;
        url += '?parentMode=Branch';
        url += '&' + CBBConstants.c_s_URL_PARAM_COUNTRYCODE + '=' + this.utils.getCountryCode();
        url += '&' + CBBConstants.c_s_URL_PARAM_BUSINESSCODE + '=' + this.utils.getBusinessCode();
        url += '&' + CBBConstants.c_s_URL_PARAM_BRANCHCODE + '=' + this.utils.getBranchCode();
        url += '&BranchPostcode=' + this.getControlValue('BranchPostcode');
        url += '&BranchAddressLine1=' + this.getControlValue('BranchAddressLine1');
        url += '&BranchAddressLine2=' + this.getControlValue('BranchAddressLine2');
        url += '&BranchAddressLine3=' + this.getControlValue('BranchAddressLine3');
        url += '&BranchAddressLine4=' + this.getControlValue('BranchAddressLine4');
        url += '&BranchAddressLine5=' + this.getControlValue('BranchAddressLine5');
        url += '&GPSCoordinateX=' + this.getControlValue('GPSCoordinateX');
        url += '&GPSCoordinateY=' + this.getControlValue('GPSCoordinateY');
        url += '&RoutingScore=' + this.getControlValue('RoutingScore');
        url += '&SelRoutingSource=' + this.getControlValue('SelRoutingSource');
        let routingSearchWindow: any = window.open(url, '_blank', 'fullscreen=yes,menubar=no,resizable=no,status=no,titlebar=no,toolbar=no,width=' + screen.width + ',height=' + screen.height);
        routingSearchWindow.onbeforeunload = () => { this.getRoutingSearchScreenData(); };
    }
    public getRoutingSearchScreenData(): void {
        let routingSearchObj: any = this._ls.retrieve('BranchRoutingSearch');
        if (routingSearchObj) {
            routingSearchObj = JSON.parse(routingSearchObj);
            if (routingSearchObj.applyClicked !== 'true') {
                return;
            }
            if (routingSearchObj['BranchPostcode']) {
                this.setControlValue('BranchPostcode', routingSearchObj.BranchPostcode);
            }
            if (routingSearchObj['BranchAddressLine1']) {
                this.setControlValue('BranchAddressLine1', routingSearchObj.BranchAddressLine1);
            }
            if (routingSearchObj['BranchAddressLine2']) {
                this.setControlValue('BranchAddressLine2', routingSearchObj.BranchAddressLine2);
            }
            if (routingSearchObj['BranchAddressLine3']) {
                this.setControlValue('BranchAddressLine3', routingSearchObj.BranchAddressLine3);
            }
            if (routingSearchObj['BranchAddressLine4']) {
                this.setControlValue('BranchAddressLine4', routingSearchObj.BranchAddressLine4);
            }
            if (routingSearchObj['BranchAddressLine5']) {
                this.setControlValue('BranchAddressLine5', routingSearchObj.BranchAddressLine5);
            }
            if (routingSearchObj['GPScoordinateX']) {
                this.setControlValue('GPSCoordinateX', routingSearchObj.GPScoordinateX);
            }
            if (routingSearchObj['GPScoordinateY']) {
                this.setControlValue('GPSCoordinateY', routingSearchObj.GPScoordinateY);
            }
            if (routingSearchObj['RoutingScore']) {
                this.setControlValue('RoutingScore', routingSearchObj.RoutingScore);
            }
            if (routingSearchObj['RoutingSource']) {
                this.setControlValue('SelRoutingSource', routingSearchObj.RoutingSource);
            }
            this._ls.clear('BranchRoutingSearch');
        }
    }

    public onClickcmdVtxGeoCode(): void {
        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotCovered));
        // this.navigate('Branch', 'Application/iCABSVertexGeoCodeSearch');
    }
    public onClickcmdSalesDefault(): void {
        let successCallback: any = function successCallback(): void {
            this.setControlValue('DefaultType', '1');
            this.fnUpdateDefault();
        }.bind(this);

        let promptVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.updateAllSalesEmpWrkHrs, null, successCallback, null);
        promptVO.title = MessageConstant.PageSpecificMessage.workingHrs;
        promptVO.confirmLabel = 'Yes';
        promptVO.cancelLabel = 'No';
        this.modalAdvService.emitPrompt(promptVO);
    }
    public onClickcmdServiceDefault(): void {
        let successCallback: any = function successCallback(): void {
            this.setControlValue('DefaultType', '2');
            this.fnUpdateDefault();
        }.bind(this);

        let promptVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.updateAllServiceEmpWrkHrs, null, successCallback, null);
        promptVO.title = MessageConstant.PageSpecificMessage.workingHrs;
        promptVO.confirmLabel = 'Yes';
        promptVO.cancelLabel = 'No';
        this.modalAdvService.emitPrompt(promptVO);
    }
    public onClickcmdAdmindDefault(): void {
        let successCallback: any = function successCallback(): void {
            this.setControlValue('DefaultType', '3');
            this.fnUpdateDefault();
        }.bind(this);

        let promptVO: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.updateAllAdminEmpWrkHrs, null, successCallback, null);
        promptVO.title = MessageConstant.PageSpecificMessage.workingHrs;
        promptVO.confirmLabel = 'Yes';
        promptVO.cancelLabel = 'No';
        this.modalAdvService.emitPrompt(promptVO);
    }
    public onChangeBranchNumber(): void {
        if (this.pageMode === MntConst.eModeAdd && this.pageParams.EnableMPPlanning) {
            this.getBranchDefault();
        }
    }
    public onChangeMaxHoursPerWeek01(): void { this.fnProcessTimeString('MaxHoursPerWeek01'); }
    public onChangeMaxHoursPerWeek02(): void { this.fnProcessTimeString('MaxHoursPerWeek02'); }
    public onChangeMaxHoursPerWeek03(): void { this.fnProcessTimeString('MaxHoursPerWeek03'); }
    public onchangeGPSCoordinateX(): void { this.setRoutingSource(); }
    public onchangeGPSCoordinateY(): void { this.setRoutingSource(); }
    public onchangeRoutingScore(): void { this.setRoutingSource(); }

    //Ellipsis
    public ellipsisRespHandler(data: any, ellipsisID: string): any {
        switch (ellipsisID) {
            case 'BranchManagerEmployeeCode':
                this.setControlValue('BranchManagerEmployeeCode', data.BranchManagerEmployeeCode);
                this.setControlValue('BranchManagerEmployeeSurname', data.BranchManagerEmployeeSurname);
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'BranchManagerEmployeeCode');
                break;
            case 'BranchSecretaryEmployeeCode':
                this.setControlValue('BranchSecretaryEmployeeCode', data.BranchSecretaryEmployeeCode);
                this.setControlValue('BranchSecretaryEmployeeSurname', data.BranchSecretaryEmployeeSurname);
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'BranchSecretaryEmployeeCode');
                break;
            case 'FBEVBranchServiceAreaCode':
                this.setControlValue('FBEVBranchServiceAreaCode', data.FBEVBranchServiceAreaCode);
                this.setControlValue('FBEVBranchServiceAreaDesc', data.FBEVBranchServiceAreaDesc);
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'FBEVBranchServiceAreaCode');
                break;
            case 'EngineerServiceAreaCode':
                this.setControlValue('EngineerServiceAreaCode', data.EngineerServiceAreaCode);
                this.setControlValue('EngineerServiceAreaDesc', data.EngineerServiceAreaDesc);
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'EngineerServiceAreaCode');
                break;
        }
    }
    public getDropdownDesc(refName: string, id: string, dispId?: string, descIdArr?: Array<any>): void {
        let lookupIP: Array<any> = [];
        this.refLookUp[refName].query.BusinessCode = this.utils.getBusinessCode();
        this.refLookUp[refName].query[id] = (dispId ? this.getControlValue(dispId) : this.getControlValue(id));
        lookupIP.push(this.refLookUp[refName]);
        this.LookUp.lookUpPromise(lookupIP).then((data) => {
            if (data) {
                let descFld: Array<any> = [];
                if (descIdArr && descIdArr.length > 0) {
                    descFld = descIdArr;
                }

                let tempVal: string = this.utils.setLookupValue(this, this.refLookUp[refName].fields, data[0], descFld);
                if (tempVal === '') {
                    this.riExchange.riInputElement.markAsError(this.uiForm, dispId ? dispId : id);
                    if (descIdArr && descIdArr.length > 0) {
                        this.setControlValue(descFld[0], '');
                    } else {
                        this.setControlValue(this.refLookUp[refName].fields[0], '');
                    }
                }
            }
        });
    }
    public onChangeRegionCode(): void {
        if (this.getControlValue('RegionCode') !== '') {
            this.getDropdownDesc('Region', 'RegionCode');
        } else {
            this.riExchange.riInputElement.isCorrect(this.uiForm, 'RegionCode');
            this.setControlValue('RegionDesc', '');
        }
    }
    public onChangeLanguageCode(): void {
        if (this.getControlValue('LanguageCode') !== '') {
            this.getDropdownDesc('Language', 'LanguageCode');
        } else {
            this.riExchange.riInputElement.isCorrect(this.uiForm, 'LanguageCode');
            this.setControlValue('LanguageDescription', '');
        }
    }
    public onChangeBranchManagerEmployeeCode(): void {
        if (this.getControlValue('BranchManagerEmployeeCode') !== '') {
            this.getDropdownDesc('BranchManager', 'EmployeeCode', 'BranchManagerEmployeeCode', ['BranchManagerEmployeeSurname']);
        } else {
            this.riExchange.riInputElement.isCorrect(this.uiForm, 'BranchManagerEmployeeCode');
            this.setControlValue('BranchManagerEmployeeSurname', '');
        }
    }
    public onChangeBranchSecretaryEmployeeCode(): void {
        if (this.getControlValue('BranchSecretaryEmployeeCode') !== '') {
            this.getDropdownDesc('BranchSecretary', 'EmployeeCode', 'BranchSecretaryEmployeeCode', ['BranchSecretaryEmployeeSurname']);
        } else {
            this.riExchange.riInputElement.isCorrect(this.uiForm, 'BranchSecretaryEmployeeCode');
            this.setControlValue('BranchSecretaryEmployeeSurname', '');
        }
    }
    public onChangeFBEVBranchServiceAreaCode(): void {
        if (this.getControlValue('FBEVBranchServiceAreaCode') !== '') {
            this.getDropdownDesc('Branch', 'BranchServiceAreaCode', 'FBEVBranchServiceAreaCode', ['FBEVBranchServiceAreaDesc']);
        } else {
            this.riExchange.riInputElement.isCorrect(this.uiForm, 'FBEVBranchServiceAreaCode');
            this.setControlValue('FBEVBranchServiceAreaDesc', '');
        }
    }
    public onChangeEngineerServiceAreaCode(): void {
        if (this.getControlValue('EngineerServiceAreaCode') !== '') {
            this.getDropdownDesc('BranchServiceArea', 'BranchServiceAreaCode', 'EngineerServiceAreaCode', ['EngineerServiceAreaDesc']);
        } else {
            this.riExchange.riInputElement.isCorrect(this.uiForm, 'EngineerServiceAreaCode');
            this.setControlValue('EngineerServiceAreaDesc', '');
        }
    }
    public onChargeRateSearchDataReceived(data: any): void {
        this.setControlValue('ChargeRateCode', data.ChargeRateCode);
        this.setControlValue('ChargeRateDesc', data.ChargeRateDesc);
        this.uiForm.markAsDirty();
    }
    public onLogoTypeSearchDataReceived(data: any): void {
        this.setControlValue('LogoTypeCode', data.LogoTypeCode);
        this.setControlValue('LogoTypeDesc', data.LogoTypeDesc);
        this.uiForm.markAsDirty();
    }

    //Generic function
    public focusSave(obj: any): void {
        this.riTab.focusNextTab(obj);
    }
    public markAsPristine(): void {
        this.formPristine();
    }
}
