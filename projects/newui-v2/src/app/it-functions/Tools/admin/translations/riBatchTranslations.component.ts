import { ENotificationType } from './../../../../../shared/services/push.notification.service';
import { MessageConstant } from '@shared/constants/message.constant';
import { GlobalNotificationsService } from './../../../../../shared/services/global.notifications.service';
import { Component, OnInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { QueryParams } from '@shared/services/http-params-wrapper';
import { HttpService } from '@shared/services/http-service';
import { ServiceConstants } from '@shared/constants/service.constants';
import { CBBService } from '@shared/services/cbb.service';
import { AppWebWorker } from '@shared/web-worker/app-worker';
import { WebWorkerService } from '@shared/web-worker/web-worker';
import { HttpWorker } from '@shared/web-worker/http-worker';
import { AuthService } from '@shared/services/auth.service';
import { environment } from './../../../../../environments/environment';
import { PushNotificationsService } from '@shared/services/push.notification.service';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';

@Component({
    templateUrl: 'riBatchTranslations.html'
})

export class BatchTranlationsComponent implements OnInit {
    public error: string;
    public hasAccess: boolean = true;
    private controls: Array<string> = [];

    constructor(private http: HttpService,
        private constants: ServiceConstants,
        private cbb: CBBService,
        private ls: LocalStorageService,
        private auth: AuthService,
        private alertNotifications: GlobalNotificationsService,
        private pushNotifications: PushNotificationsService) {
    }

    public ngOnInit(): void {
        // Check If User Has riDeveloper Checked
        let queryParam: QueryParams = new QueryParams();

        queryParam.set(this.constants.BusinessCode, this.cbb.getBusinessCode());
        queryParam.set(this.constants.CountryCode, this.cbb.getCountryCode());
        queryParam.set(this.constants.Action, '2');
        let userData: any = [{
            'table': 'UserInformation',
            'query': { 'UserCode': this.ls.retrieve('RIUserCode') },
            'fields': ['UserCode', 'riDeveloper']
        }];

        this.http.lookUpPromise(queryParam, userData).then(data => {
            let rec: any = data.results;
            if (rec && rec[0] && rec[0][0] && rec[0][0].riDeveloper) {
                this.hasAccess = true;
            } else {
                this.hasAccess = false;
                this.error = 'User Record Not Found';
            }
        }).catch(error => {
            this.hasAccess = false;
            this.error = error;
        });

        this.addRows(5);
    }

    public updateIndex(index: number, _obj: any): any {
        return index;
    }

    public addRows(count: number = 1): void {
        let temp: any = [];

        count = count * 2;

        for (let i: number = 1; i <= count; i++) {
            temp.push('');
        }

        this.controls = this.controls.concat(temp);
    }

    public onUpdateClick(): void {
        let data: string = this.controls.filter(control => control).join('|');
        let queryParam: QueryParams = new QueryParams();
        let workerService: WebWorkerService;
        let worker: AppWebWorker = new AppWebWorker();
        let googleData: any = this.ls.retrieve('GOOGLEDATA');

        if (!data) {
            return;
        }

        queryParam.set(this.constants.BusinessCode, this.cbb.getBusinessCode());
        queryParam.set(this.constants.CountryCode, this.cbb.getCountryCode());
        queryParam.set(this.constants.Action, '6');

        workerService = new WebWorkerService(HttpWorker, environment);
        workerService.run(worker.delegatePromise, {
            url: 'translation/tables',
            method: 'POST',
            token: this.auth.getToken(),
            base: this.auth.getBaseURL(),
            search: {
                businessCode: this.cbb.getBusinessCode(),
                countryCode: this.cbb.getCountryCode(),
                action: '6',
                email: googleData.email
            },
            postHeaders: {
                module: 'translation-tables',
                operation: 'TranslationsTablesBatchUpdate'
            },
            formData: {
                TranslationPhrase: data
            }
        }, true);
        workerService.worker.onmessage = response => {
            let results: any = response.data;
            try {
                if (!results) {
                    this.alertNotifications.displayMessage(
                        MessageConstant.Message.GeneralError
                    );
                } else if (results.hasError || results.errorMessage || results.fullError) {
                    this.alertNotifications.displayMessage(results);
                } else {
                    this.pushNotifications.push = {
                        message: 'Translation Batch Completed',
                        data: results,
                        type: ENotificationType.translation
                    };
                }
            } catch (err) {
                this.alertNotifications.displayMessage(results);
            }

        };

        this.alertNotifications.displayMessage('Data is being processed in background. '
            + 'You will get a notification once the process is complete. '
            + 'You can continue using the system while the data is being processed.', CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
        this.controls = [];
        this.addRows(5);
    }
}
