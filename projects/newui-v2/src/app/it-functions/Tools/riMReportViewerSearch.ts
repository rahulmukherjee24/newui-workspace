import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';

import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { LocalStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { TableComponent } from './../../../shared/components/table/table';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { BIReportsRoutes } from './../../base/PageRoutes';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';



@Component({
    templateUrl: 'riMReportViewerSearch.html',
    providers: [CommonLookUpUtilsService]
})

export class ReportViewerSearchComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('riTable') riTable: TableComponent;

    private lookUpSubscription: Subscription;
    private queryParams: any = {
        operation: 'Model/riMReportViewerSearch',
        module: 'batch-process',
        method: 'it-functions/ri-model'
    };
    private pdfUrl: string = '';

    public pageId: string = '';
    public showTable: boolean = true;
    public controls: Array<any> = [
        { name: 'FilterSelect' },
        { name: 'FilterInput' },
        { name: 'FilterInputCheck' },
        { name: 'BatchProcessUserCode', type: MntConst.eTypeCode },
        { name: 'BatchProcessUniqueNumber', type: MntConst.eTypeInteger }
    ];
    public page: any;
    public itemsPerPage: number = 10;
    public warn: string;
    public riDeveloper: any = false;
    public userCode: any;
    public modalsh: any = false;
    public uiDisplay: any = {
        IdFilter: false,
        FilterInput: false,
        FilterInputCheck: true,
        FilterInputeTypeCode: true,
        FilterInputeTypeInt: false
    };
    public filterSelectValues: any;
    public tableheading: string = 'Report Viewer';
    public isRefreshDisabled: boolean = true;
    public inputTextLabel: string = '';

    constructor(injector: Injector, private _ls: LocalStorageService, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.pageId = PageIdentifier.RIMREPORTVIEWERSEARCH;
        this.pageTitle = this.browserTitle = 'Report Viewer';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.isRequesting = true;
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.lookUpSubscription) {
            this.lookUpSubscription.unsubscribe();
        }
    }

    ngAfterViewInit(): void {
        this.getRiDeveloperAccess();
    }

    private windowOnLoad(): void {
        this.userCode = this._ls.retrieve('RIUserCode');
        this.filterSelectValues = [];
        this.filterSelectValues.push({ text: ' ', value: '0' });
        this.filterSelectValues.push({ text: 'User Code', value: 'BatchProcessUserCode' });
        this.filterSelectValues.push({ text: 'Unique Number', value: 'BatchProcessUniqueNumber' });
        this.setControlValue('FilterSelect', 'BatchProcessUserCode');
        this.setControlValue('FilterInput', this.userCode);
        this.inputTextLabel = 'User Code';
        if ((this.userCode && this.userCode.toUpperCase() === 'CIT')
            || this.riDeveloper) {
            this.uiDisplay.IdFilter = true;
        } else {
            this.uiDisplay.IdFilter = false;
        }
        this.buildTableColumns();
        this.loadTable();
    }

    private buildTableColumns(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('BatchProcessUniqueNumber', MntConst.eTypeInteger, 'Key', 'Unique Number', 10);
        this.riTable.AddTableField('BatchProcessUserCode', MntConst.eTypeCode, 'Required', 'UserCode', 10);
        this.riTable.AddTableField('BatchProcessSubmittedDate', MntConst.eTypeDate, 'Required', 'Submitted Date', 10);
        this.riTable.AddTableField('BatchProcessSubmittedTime', MntConst.eTypeTime, 'Required', 'Submitted Time', 10);
        this.riTable.AddTableField('BatchProcessDescription', MntConst.eTypeText, 'Required', 'Description', 50);
        this.riTable.AddTableField('BatchProcessTypeDisplayDesc', MntConst.eTypeText, 'Virtual', 'Status', 12);
    }

    // Load table data
    private loadTable(): void {
        let search: QueryParams = new QueryParams(), selectedValue: any = '';
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set(this.serviceConstants.LanguageCode, this.riExchange.LanguageCode());
        search.set('BatchProcessReport', 'True');
        selectedValue = this.getControlValue('FilterSelect');
        switch (selectedValue) {
            case 'BatchProcessUserCode':
                search.set('BatchProcessUserCode', this.getControlValue('FilterInput'));
                search.set('search.op.BatchProcessUserCode', 'GE');
                break;
            case 'BatchProcessUniqueNumber':
                search.set('BatchProcessUniqueNumber', this.getControlValue('FilterInput'));
                search.set('search.op.BatchProcessUniqueNumber', 'GE');
                break;
        }
        this.queryParams.search = search;
        this.riTable.loadTableData(this.queryParams);
        this.isRequesting = false;
    }

    private getRiDeveloperAccess(): void {
        let lookupIP: any = [
            {
                'table': 'UserInformation',
                'query': {
                    'UserCode': this.userCode
                },
                'fields': ['riDeveloper']
            }
        ];

        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            let userData: any = data[0][0];
            if (userData) {
                this.riDeveloper = userData.riDeveloper;
                this.windowOnLoad();
            }
        });
    }

    private generatePdf(): void {
        window.open(this.pdfUrl, '_blank');
    }

    public selectedData(event: any): void {
        let search: QueryParams = this.getURLSearchParamObject();
        this.pdfUrl = '';
        search.set(this.serviceConstants.Action, '0');
        search.set('reportnumber', event['row']['BatchProcessUniqueNumber']);

        this.commonLookup.getBatchProcessProgramName(event['row']['BatchProcessUniqueNumber'])
            .then(batchElements => {
                if (batchElements[0][0].BatchProcessProgramName !== 'iCABSTaxCodeReportGeneration.p') {
                    this.ajaxSource.next(this.ajaxconstant.START);
                    this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
                        (data) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            if (data.hasError) {
                                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                                return;
                            }
                            if (data['confirm']) {
                                let promptVO: ICabsModalVO = new ICabsModalVO();
                                promptVO.msg = data['confirm'];
                                promptVO.confirmCallback = this.generatePdf.bind(this);
                                this.pdfUrl = data['url'];
                                this.modalAdvService.emitPrompt(promptVO);
                            } else if (data['url']) {
                                window.open(data['url'], '_blank');
                            }
                            else if (data['information']) {
                                this.modalAdvService.emitMessage(new ICabsModalVO(data['information']));
                            }
                        },
                        (error) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        });
                } else {
                    if (event['row']['BatchProcessTypeDisplayDesc'] === 'Completed') {
                        this.navigate('', BIReportsRoutes.RIBATCHCOMPONENT,
                        { ReportNumber: event['row']['BatchProcessUniqueNumber'],
                           BatchProcessSubmittedDate: event['row']['BatchProcessSubmittedDate'],
                           BatchProcessSubmittedTime: event['row']['BatchProcessSubmittedTime']
                        });
                    }
                }
            });
    }

    public filterSelectOnChange(): void {
        let selectedValue: any = this.getControlValue('FilterSelect');
        this.setControlValue('FilterInput', '');
        this.riTable.clearTable();
        switch (selectedValue) {
            case '0':
                this.uiDisplay.FilterInput = true;
                this.inputTextLabel = '';
                break;
            case 'BatchProcessUserCode':
                this.uiDisplay.FilterInput = false;
                this.uiDisplay.FilterInputeTypeCode = true;
                this.uiDisplay.FilterInputeTypeInt = false;
                this.inputTextLabel = 'User Code';
                break;
            case 'BatchProcessUniqueNumber':
                this.uiDisplay.FilterInput = false;
                this.uiDisplay.FilterInputeTypeInt = true;
                this.uiDisplay.FilterInputeTypeCode = false;
                this.inputTextLabel = 'Unique Number';
                break;
        }
    }

    public onSearchClick(): void {
        if (this.uiForm.valid) {
            this.showTable = true;
            this.buildTableColumns();
            this.loadTable();
        } else {
            this.showTable = false;
        }
    }
}
