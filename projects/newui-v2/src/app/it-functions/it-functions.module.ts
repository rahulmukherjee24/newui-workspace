import { HttpClientModule } from '@angular/common/http';
import { BatchProcessMonitorSearchComponent } from './TableMaintenanceSystem/iCABSMBatchProcessMonitorSearch';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { ITFunctionsRootComponent } from './it-functions.component';
import { ITFunctionsRouteDefinitions } from './it-functions.route';
import { BatchProcessMonitorComponent } from './BatchProcesses/iCABSOBatchProcessMonitorSearch.component';
import { RiRegistryComponent } from './Registry/riRegistry.component';
import { BatchProgramMaintenanceComponent } from './TableMaintenanceSystem/MaintainBatchProgram/riMGBatchProgramMaintenance.component';
import { BusinessSystemCharacteristicsCustomMaintenanceComponent } from './TableMaintenanceSystem/iCABSSBusinessSystemCharacteristicsCustomMaintenance';
import { SystemParameterMaintenanceComponent } from './Maintenance/MaintainParameters/iCABSSSystemParameterMaintenance.component';
import { BranchMaintenanceComponent } from './TableMaintenanceBusiness/Branch/iCABSBBranchMaintenance.component';
import { TranslationMaintenanceComponent } from './Maintenance/Translation/riMGTranslationMaintenance.component';
import { UserTypeMenuAccessMaintenanceComponent } from './Maintenance/UserTypeMenuAccess/riMUserTypeMenuAccessMaintenance.component';
import { CompanyMaintenanceComponent } from '../internal/maintenance/iCABSBCompanyMaintenance.component';
import { UserInformationMaintenanceComponent } from './TableMaintenanceSystem/User/riMUserInformationMaintenance.component';
import { BusinessRegistryGridComponent } from './Configuration/BusinessRegistry/iCABSBBusinessRegistryGrid.component';
import { UserTypeMaintenanceComponent } from './TableMaintenanceSystem/UserType/riMUserTypeMaintenance.component';
import { ReportViewerSearchComponent } from './Tools/riMReportViewerSearch';
import { AddUserComponent } from './Tools/admin/riAddUserAllServers.component';
import { BatchTranlationsComponent } from './Tools/admin/translations/riBatchTranslations.component';
import { RiImportComponent } from './Importing/riImport.component';

@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        ITFunctionsRouteDefinitions
    ],
    declarations: [
        ITFunctionsRootComponent,
        BatchProcessMonitorComponent,
        RiRegistryComponent,
        BatchProcessMonitorSearchComponent,
        BatchProgramMaintenanceComponent,
        BusinessSystemCharacteristicsCustomMaintenanceComponent,
        SystemParameterMaintenanceComponent,
        BranchMaintenanceComponent,
        TranslationMaintenanceComponent,
        UserTypeMenuAccessMaintenanceComponent,
        CompanyMaintenanceComponent,
        UserInformationMaintenanceComponent,
        BusinessRegistryGridComponent,
        UserTypeMaintenanceComponent,
        ReportViewerSearchComponent,
        AddUserComponent,
        BatchTranlationsComponent,
        RiImportComponent
    ]
})

export class ITFunctionsModule { }
