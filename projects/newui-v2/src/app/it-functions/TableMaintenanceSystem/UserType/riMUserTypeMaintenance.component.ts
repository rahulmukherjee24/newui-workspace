import { QueryParams } from './../../../../shared/services/http-params-wrapper';
import { Component, OnInit, Injector, OnDestroy, ViewChild } from '@angular/core';

import { BaseComponent } from './../../../../app/base/BaseComponent';
import { PageIdentifier } from './../../../../app/base/PageIdentifier';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { ErrorConstant } from './../../../../shared/constants/error.constant';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { GenericEllipsisEvent, IGenericEllipsisControl } from '@shared/components/ellipsis-generic/ellipsis-generic';

@Component({
    templateUrl: 'riMUserTypeMaintenance.html'
})

export class UserTypeMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private queryParams: any = {
        method: 'it-functions/ri-model',
        module: 'user',
        operation: 'Model/riMUserTypeMaintenance'
    };
    private pageMode: string;
    public pageId: string = '';
    public controls = [
        { name: 'UserTypeCode', required: true, type: MntConst.eTypeText },
        { name: 'UserTypeAllow', type: MntConst.eTypeCheckBox, disabled: true },
        { name: 'UserTypeDescription', type: MntConst.eTypeText, required: true, disabled: true },
        { name: 'ROWID' }
    ];
    //usertype ellipsis
    public userTypeSearchConfig: IGenericEllipsisControl = {
        autoOpen: true,
        ellipsisTitle: 'User Type Search',
        configParams: {
            table: '',
            shouldShowAdd: true,
            parentMode: 'Search'
        },
        httpConfig: {
            operation: 'Model/riMUserTypeSearch',
            module: 'user',
            method: 'it-functions/ri-model'
        },
        tableColumn: [
            { title: 'Code', name: 'UserTypeCode', type: MntConst.eTypeCode, size: 3 },
            { title: 'Description', name: 'UserTypeDescription', type: MntConst.eTypeText, size: 20, required: 'Required' }
        ],
        disable: false
    };
    public isSaveDisable: boolean = true;
    public isCancelDisable: boolean = true;
    public isDeleteHidden: boolean = true;
    public isDeleteDisable: boolean = true;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.RIMUSERTYPEMAINTENANCE;
        this.browserTitle = this.pageTitle = 'User Type Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    //Fetch formdata
    private fetchData(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '0');
        searchParams.set('UserTypeCode', this.getControlValue('UserTypeCode'));
        this.queryParams['search'] = searchParams;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'])
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.riExchange.riInputElement.Disable(this.uiForm, 'UserTypeCode');
                        this.setControlValue('UserTypeDescription', data.UserTypeDescription);
                        this.riExchange.riInputElement.Enable(this.uiForm, 'UserTypeDescription');
                        this.setControlValue('UserTypeAllow', this.utils.convertResponseValueToCheckboxInput(data.UserTypeAllow));
                        this.riExchange.riInputElement.Enable(this.uiForm, 'UserTypeAllow');
                        this.setControlValue('ROWID', data.ttUserType);
                        this.isSaveDisable = false;
                        this.isCancelDisable = false;
                        this.pageMode = 'Update';
                        if (this.pageMode !== 'AddModeOn') {
                            this.isDeleteHidden = false;
                            this.isDeleteDisable = false;
                        }
                        this.formPristine();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
                });
    }

    //Add callback
    private onAddSaveConfirm(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};
        searchParams.set(this.serviceConstants.Action, '1');
        this.queryParams['search'] = searchParams;
        formData['UserTypeCode'] = this.getControlValue('UserTypeCode');
        formData['UserTypeDescription'] = this.getControlValue('UserTypeDescription');
        formData['UserTypeAllow'] = this.getControlValue('UserTypeAllow') ? true : false;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                        this.pageMode = 'Update';
                        this.riExchange.riInputElement.Disable(this.uiForm, 'UserTypeCode');
                        this.setControlValue('ROWID', data.ttUserType);
                        this.isDeleteHidden = false;
                        this.isDeleteDisable = false;
                        this.userTypeSearchConfig.autoOpen = false;
                        this.userTypeSearchConfig.disable = false;
                        this.formPristine();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
                });
    }

    //Update callback
    private onUpdateSaveConfirm(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};
        searchParams.set(this.serviceConstants.Action, '2');
        this.queryParams['search'] = searchParams;
        formData['UserTypeCode'] = this.getControlValue('UserTypeCode');
        formData['UserTypeDescription'] = this.getControlValue('UserTypeDescription');
        formData['UserTypeAllow'] = this.getControlValue('UserTypeAllow') ? true : false;
        formData['ROWID'] = this.getControlValue('ROWID');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                        this.formPristine();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
                });
    }

    //Delete call back
    private onDeleteConfirm(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};
        searchParams.set(this.serviceConstants.Action, '3');
        this.queryParams['search'] = searchParams;
        formData['ROWID'] = this.getControlValue('ROWID');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully));
                        this.pageMode = 'update';
                        this.setControlValue('UserTypeCode', '');
                        this.setControlValue('UserTypeDescription', '');
                        this.setControlValue('UserTypeAllow', '');
                        this.setControlValue('ROWID', '');
                        this.riExchange.riInputElement.Enable(this.uiForm, 'UserTypeCode');
                        this.uiForm.controls['UserTypeCode'].markAsPristine();
                        this.riExchange.riInputElement.Disable(this.uiForm, 'UserTypeDescription');
                        this.riExchange.riInputElement.Disable(this.uiForm, 'UserTypeAllow');
                        this.isSaveDisable = true;
                        this.isCancelDisable = true;
                        this.isDeleteHidden = true;
                        this.isDeleteDisable = true;
                        this.formPristine();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
                });
    }

    //on save click
    public onSave(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            if (this.pageMode === 'AddModeOn') {
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.onAddSaveConfirm.bind(this)));
            } else {
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.onUpdateSaveConfirm.bind(this)));
            }
        }
    }

    //on delete click
    public onDelete(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.onDeleteConfirm.bind(this)));
        }
    }

    //on user type onchange
    public onUserTypeChange(): void {
        if (this.pageMode !== 'AddModeOn') {
            this.uiForm.controls['UserTypeCode'].markAsPristine();
        }
        if (this.getControlValue('UserTypeCode')) {
            if (this.pageMode !== 'AddModeOn') {
                this.fetchData();
            }
        }
    }

    //on user type ellipsis click
    public onUserTypeDataReceived(data: any): void {
        if (data[GenericEllipsisEvent.add]) {
            this.uiForm.reset();
            this.pageMode = 'AddModeOn';
            this.riExchange.riInputElement.Enable(this.uiForm, 'UserTypeCode');
            this.riExchange.riInputElement.Enable(this.uiForm, 'UserTypeDescription');
            this.riExchange.riInputElement.Enable(this.uiForm, 'UserTypeAllow');
            this.userTypeSearchConfig.disable = true;
            this.isSaveDisable = false;
            this.isCancelDisable = false;
            this.isDeleteHidden = true;
            return;
        }
        this.pageMode = 'Update';
        this.setControlValue('UserTypeCode', data['UserTypeCode']);
        this.isDeleteHidden = false;
        this.isDeleteDisable = false;
        this.fetchData();
    }

    //on cancel click
    public cancel(): void {
        if (this.pageMode === 'AddModeOn') {
            this.uiForm.reset();
            this.pageMode = 'update';
            this.userTypeSearchConfig.autoOpen = true;
            this.userTypeSearchConfig.disable = false;
            this.riExchange.riInputElement.Disable(this.uiForm, 'UserTypeDescription');
            this.riExchange.riInputElement.Disable(this.uiForm, 'UserTypeAllow');
            this.isSaveDisable = true;
            this.isCancelDisable = true;
            this.isDeleteHidden = true;
            this.isDeleteDisable = true;
        } else {
            this.fetchData();
        }
    }
}
