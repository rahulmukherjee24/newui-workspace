import { QueryParams } from './../../../../shared/services/http-params-wrapper';
import { Component, Injector, OnInit, AfterViewInit, OnDestroy, ViewChild, Renderer } from '@angular/core';
import { Validators } from '@angular/forms';

import { Observable } from 'rxjs/Rx';

import { PageIdentifier } from '../../../base/PageIdentifier';
import { BaseComponent } from '../../../base/BaseComponent';
import { ScreenNotReadyComponent } from '../../../../shared/components/screenNotReady';
import { LanguageSearchComponent } from '../../../internal/search/riMGLanguageSearch.component';
import { MUserSearchComponent } from '../../../internal/search/riMUserSearch.component';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { ErrorConstant } from '../../../../shared/constants/error.constant';
import { MessageConstant } from '../../../../shared/constants/message.constant';
import { GlobalConstant } from '../../../../shared/constants/global.constant';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';


@Component({
    templateUrl: 'riMUserInformationMaintenance.html'
})

export class UserInformationMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    private requestParams: Object = {
        operation: 'Model/riMUserInformationMaintenance',
        module: 'user',
        method: 'it-functions/ri-model'
    };
    private queryLookUp: QueryParams = new QueryParams();
    private querySysChar: QueryParams = new QueryParams();
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('userName') public userName;
    public creditAppLevelList: any[] = [];
    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'UserCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'UserName', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'LanguageCode', readonly: false, disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'LanguageDescription', readonly: false, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'Email', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'PhoneNumber', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'FaxNumber', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'Password', readonly: false, disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'PasswordlastChangedDate', readonly: false, disabled: false, required: true, type: MntConst.eTypeDate },
        { name: 'PasswordRetries', readonly: false, disabled: false, required: true, type: MntConst.eTypeInteger },
        { name: 'DateLastLoggedOn', readonly: false, disabled: false, required: false, type: MntConst.eTypeDate },
        { name: 'LocalDocumentServer', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'ReportServer', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'UserTypeCode', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'UserTypeDescription', readonly: false, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ApprovalLevelCode', value: '0', readonly: false, disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'SOUserQuoteTypeGroupCode', value: '0', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'OwnSubmissionsInd', value: false, readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'Inactive', value: false, readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'riDeveloper', value: false, readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'riMessagingEnabled', value: false, readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'riProgramRequestStats', value: false, readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'Save', readonly: false, exclude: true, disabled: false, required: false },
        { name: 'Cancel', readonly: false, exclude: true, disabled: false, required: false },
        { name: 'Delete', readonly: false, exclude: true, disabled: false, required: false },
        { name: 'btnMembers', readonly: false, exclude: true, disabled: false, required: false }
    ];

    public pageParams: any = {
        EnableAlphaNumPassword: '',
        ValidPassword: '',
        vDisableUserTypeCode: true,
        blnDisableUserTypeCode: '',
        vSCCapitalFirstLtr: '',
        PasswordLabel: 'Password',
        InactiveErrorMessage: '',
        riMessagingEnabled: false,
        btnMembers: false,
        fieldVisibility: {},
        fieldRequired: {
            UserCode: true,
            UserName: true,
            LanguageCode: true,
            PhoneNumber: true,
            FaxNumber: true,
            Password: true,
            PasswordLastChangedDate: true,
            PasswordRetries: true,
            LocalDocumentServer: true,
            ReportServer: true,
            UserTypeCode: true,
            ApprovalLevelCode: true,
            riDeveloper: true,
            riMessagingEnabled: true,
            riProgramRequestStats: true,
            Email: true
        }
    };
    public modalConfig: Object = {
        backdrop: 'static',
        keyboard: true
    };
    public ellipsis: Object = {
        riMGLanguageSearch: {
            inputParams: {
                parentMode: 'LookUp',
                countryCode: '',
                businessCode: ''
            },
            autoOpen: false,
            showHeader: true,
            isEllipsisDisabled: false,
            contentComponent: LanguageSearchComponent
        },
        riMUserTypeSearch: {
            autoOpen: false,
            ellipsisTitle: 'User Type Search',
            configParams: {
                table: '',
                shouldShowAdd: false,
                parentMode: 'LookUp'
            },
            httpConfig: {
                operation: 'Model/riMUserTypeSearch',
                module: 'user',
                method: 'it-functions/ri-model'
            },
            tableColumn: [
                { title: 'Code', name: 'UserTypeCode', type: MntConst.eTypeCode, size: 3 },
                { title: 'Description', name: 'UserTypeDescription', type: MntConst.eTypeText, size: 20, required: 'Required' }
            ],
            disable: false
        },
        riMUserSearch: {
            inputParams: {
                parentMode: 'LookUp',
                showAddNew: true
            },
            autoOpen: false,
            showHeader: true,
            isEllipsisDisabled: false,
            contentComponent: MUserSearchComponent
        },
        riMUserInformationSearch: {
            inputParams: {
                parentMode: 'LookUp',
                countryCode: '',
                businessCode: ''
            },
            autoOpen: false,
            showHeader: true,
            isEllipsisDisabled: false,
            contentComponent: ScreenNotReadyComponent
        }

    };

    constructor(injector: Injector, private renderer: Renderer) {
        super(injector);
        this.pageId = PageIdentifier.RIMUSERINFORMATIONMAINTENANCE;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.parentMode = this.riExchange.getParentMode();
        this.pageTitle = 'User Information Maintenance';
        this.utils.setTitle('User Information Maintenance');
        this.formMode = this.c_s_MODE_SELECT;
        this.fetchTranslationContent();
        this.postInit();
        this.getCreditApprovalData();
    }

    ngAfterViewInit(): void {
        if (!this.isReturning()) {
            this.displaySearchModal();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * This function is called when initialisation completes, it loads default data and process modes
     */
    private postInit(): void {
        let lookupdata: Array<any> = [
            {
                table: 'UserInformation',
                query: { 'UserCode': this.utils.getUserCode() },
                fields: ['UserTypeCode']
            }
        ];
        let observableBatch: Array<any> = [];
        observableBatch.push(this.lookUpRecord(lookupdata, 1));
        observableBatch.push(this.triggerFetchSysChar());
        observableBatch.push(this.ajaxPost('AlphaNumStatus', {}, {}));
        observableBatch.push(this.httpService.riGetErrorMessage(2988, this.utils.getCountryCode(), this.utils.getBusinessCode()));
        this.processMode();
        Observable.forkJoin(
            ...observableBatch
        ).subscribe((data: any) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data[0]['results'] && data[0]['results'].length > 0 && data[0]['results'][0].length > 0) {
                let lookup = [{
                    table: 'UserType',
                    query: { 'UserTypeCode': data[0]['results'][0][0]['UserTypeCode'], 'UserTypeAllow': true },
                    fields: ['UserTypeCode']
                }];
                this.lookUpRecord(lookup, 5).subscribe(
                    (e) => {
                        if (e['results'] && e['results'].length > 0 && e['results'][0].length > 0) {
                            this.pageParams['vDisableUserTypeCode'] = false;
                        }
                        if (this.pageParams['vDisableUserTypeCode']) {
                            this.pageParams['blnDisableUserTypeCode'] = true;
                        } else {
                            this.pageParams['blnDisableUserTypeCode'] = false;
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
                    }
                );
            }

            if (data[1].hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(ErrorConstant.Message.SystemCharacteristicsFetchError));
            } else {
                if (data[1].records && data[1].records.length > 0) {
                    this.pageParams['vSCCapitalFirstLtr'] = data[1].records[0].Required;
                }
            }

            if (data[2].hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data[2]['errorMessage'], data[2]['fullError']));
            } else {
                this.pageParams['EnableAlphaNumPassword'] = data[2].EnableAlphaNumPassword;
            }
            this.setFieldLabels();
            if (data[3] !== 0 && data[3] !== ErrorConstant.Message.ErrorMessageNotFound) {
                this.pageParams['InactiveErrorMessage'] = data[3];
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
            return;
        });
    }
    /**
     * This function is triggers update mode
     */
    private triggerUpdateMode(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxGet('', { UserCode2: this.getControlValue('UserCode'), action: '0' }).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.formMode = this.c_s_MODE_SELECT;
                this.processMode();
                this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
            } else {
                this.formMode = this.c_s_MODE_UPDATE;
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'Password', false);
                this.uiForm.controls['Password'].setValidators(Validators.required);
                this.processMode();
                this.populateFormAndParams(data);
                this.populteLookUpData();
                setTimeout(() => {
                    let focus = new CustomEvent('focus', { bubbles: true });
                    this.renderer.invokeElementMethod(this.userName.nativeElement, 'focus', [focus]);
                }, 0);
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
            return;
        });
    }

    /**
     * This function resets the form to empty
     */
    private formReset(): void {
        this.uiForm.reset();
        this.formPristine();
        this.setTranslatedValues();
    }

    /**
     * This function toggles ellipsis enable/disable state
     */
    private ellipsisToggle(riMUserDisabled: boolean, riMUserInformationDisabled: boolean, riMGLanguageDisabled: boolean, riMUserTypeDisabled: boolean): void {
        this.ellipsis['riMUserSearch'].isEllipsisDisabled = riMUserDisabled;
        this.ellipsis['riMUserInformationSearch'].isEllipsisDisabled = riMUserInformationDisabled;
        this.ellipsis['riMGLanguageSearch'].isEllipsisDisabled = riMGLanguageDisabled;
        this.ellipsis['riMUserTypeSearch'].disable = riMUserTypeDisabled;
    }

    /**
     * This is an API function for GET requests triggered in the screen
     * @param functionName
     * @param params
     */
    private ajaxGet(functionName: string, params: Object): Observable<any> {
        let requestParams: any = new QueryParams();
        requestParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        requestParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (functionName !== '') {
            requestParams.set(this.serviceConstants.Action, '6');
            requestParams.set('Function', functionName);
        }
        for (let key in params) {
            if (key) {
                requestParams.set(key, params[key]);
            }
        }
        return this.httpService.makeGetRequest(this.requestParams['method'], this.requestParams['module'], this.requestParams['operation'], requestParams);
    }
    /**
     * This is an API function for POST requests triggered in the screen
     * @param functionName
     * @param params
     * @param formData
     */
    private ajaxPost(functionName: string, params: Object, formData: Object): Observable<any> {
        let requestParams = new QueryParams();
        requestParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        requestParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (functionName !== '') {
            requestParams.set(this.serviceConstants.Action, '6');
            formData['Function'] = functionName;
        }
        for (let key in params) {
            if (key) {
                requestParams.set(key, params[key]);
            }
        }
        return this.httpService.makePostRequest(this.requestParams['method'], this.requestParams['module'], this.requestParams['operation'], requestParams, formData);
    }
    /**
     * This is an API function for Look-up requests triggered in the screen
     * @param data
     * @param maxresults
     */
    private lookUpRecord(data: Object, maxresults?: number): Observable<any> {
        this.queryLookUp.set(this.serviceConstants.Action, '0');
        this.queryLookUp.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        this.queryLookUp.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (maxresults) {
            this.queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        } else {
            this.queryLookUp.set(this.serviceConstants.MaxResults, '100');
        }
        return this.httpService.lookUpRequest(this.queryLookUp, data);
    }

    /**
     * This function sets the syschar parameters to be sent in the request
     */
    private sysCharParameters(): string {
        let sysCharList = [
            this.sysCharConstants.SystemCharDisableFirstCapitalOnAddressContact
        ];
        return sysCharList.join(',');
    }
    /**
     *  This is a trigger function for syschars
     */
    private triggerFetchSysChar(): Observable<any> {
        return this.fetchSysChar(this.sysCharParameters());
    }
    /**
     * This is an API function for Syschar requests triggered in the screen
     * @param sysCharNumbers
     */
    private fetchSysChar(sysCharNumbers: any): Observable<any> {
        this.querySysChar.set(this.serviceConstants.Action, '0');
        this.querySysChar.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        this.querySysChar.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        this.querySysChar.set(this.serviceConstants.SystemCharNumber, sysCharNumbers);
        return this.httpService.sysCharRequest(this.querySysChar);
    }

    /**
     * This is batch request to fetch translation
     */
    private fetchTranslationContent(): void {
        this.getTranslatedValuesBatch((data: any) => {
            if (data) {
                this.pageParams['Save'] = data[0];
                this.pageParams['Cancel'] = data[1];
                this.pageParams['Delete'] = data[2];
            }
        },
            ['Save'], ['Cancel'], ['Delete']);
    }

    /**
     * This function sets the translated values of the action buttons
     */
    private setTranslatedValues(): void {
        this.setControlValue('Save', this.pageParams['Save']);
        this.setControlValue('Delete', this.pageParams['Delete']);
        this.setControlValue('Cancel', this.pageParams['Cancel']);
    }

    /**
     * This function populates form data and page params with data recieved from connector
     * @param data
     */
    private populateFormAndParams(data: Object): void {
        if (data && !data['hasError']) {
            for (let i in data) {
                if (data.hasOwnProperty(i)) {
                    data[i] = !this.utils.isFalsy(data[i]) ? data[i].toString().trim() : '';
                    if (this.uiForm.controls[i] !== undefined && this.uiForm.controls[i] !== null) {
                        this.setControlValue(i, data[i].trim());
                        if (data[i].toUpperCase() === GlobalConstant.Configuration.Yes || data[i].toUpperCase() === GlobalConstant.Configuration.True) {
                            this.setControlValue(i, true);
                        } else if (data[i].toUpperCase() === GlobalConstant.Configuration.No || data[i].toUpperCase() === GlobalConstant.Configuration.False) {
                            this.setControlValue(i, false);
                        }
                    }
                    if (typeof this.pageParams[i] !== 'undefined') {
                        this.pageParams[i] = data[i];
                    }
                }
            }
        }
    }

    // This function fetches descriptions via lookup
    private populteLookUpData(): void {
        let data: Array<any> = [{
            table: 'UserType',
            query: { 'UserTypeCode': this.getControlValue('UserTypeCode') },
            fields: ['UserTypeDescription']
        },
        {
            table: 'Language',
            query: { 'LanguageCode': this.getControlValue('LanguageCode') },
            fields: ['LanguageDescription']
        }];
        this.lookUpRecord(data, 5).subscribe(
            (e) => {
                if (e['results'] && e['results'].length > 0) {
                    if (e['results'][0].length > 0) {
                        this.setControlValue('UserTypeDescription', e['results'][0][0]['UserTypeDescription']);
                    } else {
                        this.setControlValue('UserTypeDescription', '');
                    }
                    if (e['results'][1].length > 0) {
                        this.setControlValue('LanguageDescription', e['results'][1][0]['LanguageDescription']);
                    } else {
                        this.setControlValue('LanguageDescription', '');
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
            }
        );
    }

    /**
     * This function triggers before save operation
     */
    private beforeSave(): boolean {
        for (let i in this.uiForm.controls) {
            if (this.uiForm.controls.hasOwnProperty(i)) {
                this.uiForm.controls[i].markAsTouched();
                if (!this.uiForm.controls[i].enabled) {
                    this.uiForm.controls[i].clearValidators();
                }
                this.uiForm.controls[i].updateValueAndValidity();
            }
        }
        this.uiForm.updateValueAndValidity();
        return this.uiForm.valid;
    }


    // This function will be called after delete operation is successful
    private afterDelete(): void {
        this.formMode = this.c_s_MODE_SELECT;
        this.processMode();
    }

    /**
     * This function triggers when mode is changed
     */
    private processMode(): void {
        this.formReset();
        switch (this.formMode) {
            case this.c_s_MODE_ADD:
                this.uiForm.disable();
                this.enableControls(['LanguageDescription', 'UserTypeDescription']);
                this.ellipsisToggle(true, true, false, false);
                break;

            case this.c_s_MODE_UPDATE:
                this.uiForm.disable();
                this.enableControls(['UserCode', 'LanguageDescription', 'UserTypeDescription']);
                this.riMaintenanceBeforeUpdate();
                this.ellipsisToggle(false, false, false, false);
                break;

            case this.c_s_MODE_SELECT:
                this.disableControls(['UserCode']);
                this.uiForm.controls['UserCode'].enable();
                this.ellipsisToggle(false, true, true, true);
                break;
        }
    }
    /**
     * This function is called before save operation is processed
     */
    private riMaintenanceBeforeSave(isObservableReturn: boolean): any {
        if (this.formMode !== this.c_s_MODE_ADD && this.pageParams['ValidPassword'] === 'Changed') {
            if (isObservableReturn) {
                return this.ajaxPost('ValidatePassword', {}, {
                    Password: this.pageParams['Password'],
                    UserCode2: this.getControlValue('UserCode')
                });
            } else {
                this.ajaxPost('ValidatePassword', {}, {
                    Password: this.pageParams['Password'],
                    UserCode2: this.getControlValue('UserCode')
                }).subscribe((data) => {
                    this.riMaintenanceBeforeSaveInternal(data);
                }, (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
                });
            }
        } else {
            return true;
        }
    }
    /**
     * This function processes the data recieved from validate password service
     */
    private riMaintenanceBeforeSaveInternal(data: any): boolean {
        if (data['hasError']) {
            this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
        } else {
            this.pageParams['ValidPassword'] = data['ValidPassword'];
        }
        if (!this.pageParams['ValidPassword'] || (this.pageParams['ValidPassword'].toString().toUpperCase() !== GlobalConstant.Configuration.Yes)) {
            this.uiForm.controls['Password'].setErrors({});
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'Password', true);
            this.pageParams['ValidPassword'] = 'Changed';
            return false;
        }
        return true;
    }
    /**
     * This function is triggered before getting into update mode
     */
    private riMaintenanceBeforeUpdate(): void {
        if (this.pageParams['blnDisableUserTypeCode']) {
            this.uiForm.controls['riDeveloper'].disable();
        }
    }
    /**
     * This function calls the custom delete service
     */
    private riMaintenanceBeforeDelete(isObservableReturn: boolean): any {
        if (isObservableReturn) {
            return this.ajaxPost('CustomDelete', {}, {
                UserCode2: this.getControlValue('UserCode')
            });
        }
    }
    /**
     * This function is triggered after delete service is called
     */
    private riMaintenanceBeforeDeleteInternal(data: any): boolean {
        if (data['hasError']) {
            this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
        } else {
            if (data['Inactive'] && (data['Inactive'].toUpperCase() === GlobalConstant.Configuration.Yes || data['Inactive'].toUpperCase() === GlobalConstant.Configuration.True)) {
                this.setControlValue('Inactive', true);
                this.modalAdvService.emitMessage(new ICabsModalVO(this.pageParams['InactiveErrorMessage'], null));
            } else {
                let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully);
                modalVO.closeCallback = this.afterDelete.bind(this);
                this.modalAdvService.emitMessage(modalVO);
            }
        }
        return false;
    }

    /**
     * This function is changes the label of password field
     */
    private setFieldLabels(): void {
        if (this.pageParams['EnableAlphaNumPassword'].toUpperCase() === GlobalConstant.Configuration.Yes) {
            this.pageParams['PasswordLabel'] = 'AlphaNumeric Password';
        }
    }
    /**
     * This function displays search modal
     */
    private displaySearchModal(): void {
        this.ellipsis['riMUserSearch']['autoOpen'] = true;
        setTimeout(() => {
            this.ellipsis['riMUserSearch']['autoOpen'] = false;
        }, 0);
    }
    /**
     * This function is click handler for membership button
     */
    public btnMembersOnClick(): void {
        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.ScreenNotReady, null));
    }
    /**
     * This function is change handler for password
     */
    public passwordOnChange(): void {
        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'Password', false);
        this.uiForm.controls['Password'].markAsDirty();
        this.pageParams['ValidPassword'] = 'Changed';
        this.pageParams['Password'] = this.getControlValue('Password');
    }

    /**
     * This function triggers when add button is clicked
     */
    public addOnClick(): void {
        this.formMode = this.c_s_MODE_ADD;
        this.processMode();
    }
    /**
     * This function triggers when delete button is clicked
     */
    public deleteOnClick(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteOperation.bind(this)));
    }
    /**
     * This function has the core delete functionality
     */
    public deleteOperation(): void {
        this.riMaintenanceBeforeDelete(true).subscribe((data) => {
            let triggerDelete: boolean;
            triggerDelete = this.riMaintenanceBeforeDeleteInternal(data);
            if (triggerDelete) {
                this.deleteOperationInternal();
            }
        }, (error) => {
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }

    public deleteOperationInternal(): void {
        let formdata: Object = {};
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxPost('', { action: 3 }, formdata).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (!data['hasError']) {
                let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully);
                modalVO.closeCallback = this.afterDelete.bind(this);
                this.modalAdvService.emitMessage(modalVO);
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }
    /**
     * This function triggers when save button is clicked
     */
    public saveOnClick(): void {
        let valid: boolean = true;
        valid = this.beforeSave();
        if (valid) {
            let observe = this.riMaintenanceBeforeSave(true);
            if (observe instanceof Observable) {
                observe.subscribe((data) => {
                    valid = this.riMaintenanceBeforeSaveInternal(data);
                    if (valid) {
                        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveOperation.bind(this)));
                    }
                }, (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
                });
            } else if (observe === true) {
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveOperation.bind(this)));
            }
        }
    }

    /**
     * This function is the save operation that gets triggerred when save is clicked
     */
    public saveOperation(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let formData: Object = {};
        let requestParam: Object = {};
        let controlsLength = this.controls.length;
        for (let i = 0; i < controlsLength; i++) {
            if (!this.controls[i]['exclude']) {
                formData[this.controls[i].name] = this.getControlValue(this.controls[i].name);
            }
        }
        if (this.formMode === this.c_s_MODE_ADD) {
            requestParam = {
                action: 1
            };
        } else {
            requestParam = {
                action: 2
            };
        }
        this.ajaxPost('', requestParam, formData).subscribe((data) => {
            if (data['status'] === GlobalConstant.Configuration.Failure) {
                this.modalAdvService.emitError(new ICabsModalVO(data['oResponse']));
            } else {
                if (data['hasError']) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    this.formPristine();
                    let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully);
                    //modalVO.closeCallback = this.afterSave.bind(this);
                    this.modalAdvService.emitMessage(modalVO);
                    if (this.formMode === this.c_s_MODE_ADD) {
                        this.triggerUpdateMode();
                    }
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }

    /**
     * This function triggers when an option is selected in business dropdown
     */
    public cancelOnClick(): void {
        switch (this.formMode) {
            case this.c_s_MODE_ADD:
                this.formMode = this.c_s_MODE_SELECT;
                this.displaySearchModal();
                this.processMode();
                break;
            case this.c_s_MODE_UPDATE:
                this.triggerUpdateMode();
                break;
        }
    }

    public userCodeOnChange(): void {
        if (this.formMode === this.c_s_MODE_SELECT) {
            if (this.getControlValue('UserCode')) {
                this.triggerUpdateMode();
            } else {
                this.processMode();
            }
        }
    }

    public languageCodeOnChange(): void {
        if (this.getControlValue('LanguageCode')) {
            let data = [{
                table: 'Language',
                query: { 'LanguageCode': this.getControlValue('LanguageCode') },
                fields: ['LanguageDescription']
            }];
            this.lookUpRecord(data, 5).subscribe(
                (e) => {
                    if (e['results'] && e['results'].length > 0) {
                        if (e['results'][0].length > 0) {
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'LanguageCode', false);
                            this.uiForm.controls['LanguageCode'].setValidators(Validators.required);
                            this.setControlValue('LanguageDescription', e['results'][0][0]['LanguageDescription']);
                        } else {
                            this.uiForm.controls['LanguageCode'].setErrors({});
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'LanguageCode', true);
                            this.setControlValue('LanguageDescription', '');
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
                }
            );
        } else {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'LanguageCode', false);
            this.uiForm.controls['LanguageCode'].setValidators(Validators.required);
            this.setControlValue('LanguageDescription', '');
        }
    }

    public userTypeCodeOnChange(): void {
        if (this.getControlValue('LanguageCode')) {
            let data = [{
                table: 'UserType',
                query: { 'UserTypeCode': this.getControlValue('UserTypeCode') },
                fields: ['UserTypeDescription']
            }];
            this.lookUpRecord(data, 5).subscribe(
                (e) => {
                    if (e['results'] && e['results'].length > 0) {
                        if (e['results'][0].length > 0) {
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'UserTypeCode', false);
                            this.uiForm.controls['UserTypeCode'].setValidators(Validators.required);
                            this.setControlValue('UserTypeDescription', e['results'][0][0]['UserTypeDescription']);
                        } else {
                            this.uiForm.controls['UserTypeCode'].setErrors({});
                            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'UserTypeCode', true);
                            this.setControlValue('UserTypeDescription', '');
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
                }
            );
        } else {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'UserTypeCode', false);
            this.uiForm.controls['UserTypeCode'].setValidators(Validators.required);
            this.setControlValue('UserTypeDescription', '');
        }
    }

    // This function gets triggered when DateLastLoggedOn is changed is seletced
    public dateLastLoggedOnChange(value: Object): void {
        if (value && value['value']) {
            this.setControlValue('DateLastLoggedOn', value['value']);
        } else {
            this.setControlValue('DateLastLoggedOn', '');
        }
        this.uiForm.controls['DateLastLoggedOn'].markAsDirty();
    }

    // This function gets triggered when PasswordLastChangedDate is changed is seletced
    public passwordLastChangedDateOnChange(value: Object): void {
        if (value && value['value']) {
            this.setControlValue('PasswordlastChangedDate', value['value']);
        } else {
            this.setControlValue('PasswordlastChangedDate', '');
        }
        this.uiForm.controls['PasswordlastChangedDate'].markAsDirty();
    }

    // This function gets triggered when riMGLanguageSearch ellipsis is seletced
    public riMGLanguageSearchOnDataRecieved(data: Object): void {
        if (data) {
            this.setControlValue('LanguageCode', data['LanguageCode']);
            this.setControlValue('LanguageDescription', data['LanguageDescription']);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'LanguageCode', false);
            this.uiForm.controls['LanguageCode'].markAsDirty();
        }
    }

    // This function gets triggered when riMUserTypeSearch ellipsis is seletced
    public riMUserTypeSearchOnDataRecieved(data: Object): void {
        if (data) {
            this.setControlValue('UserTypeCode', data['UserTypeCode']);
            this.setControlValue('UserTypeDescription', data['UserTypeDescription']);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'UserTypeCode', false);
            this.uiForm.controls['UserTypeCode'].markAsDirty();
        }
    }

    // This function gets triggered when riMUserSearch ellipsis is selecTed
    public riMUserSearchOnDataRecieved(data: Object): void {
        if (data) {
            if (data === 'AddModeOn') {
                this.addOnClick();
            } else {
                this.setControlValue('UserCode', data['UserCode']);
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'UserCode', false);
                this.uiForm.controls['UserCode'].markAsDirty();
                this.triggerUpdateMode();
            }
        }
    }

    // This function gets triggered when riMUserSearch ellipsis is selected IN UPDATE MODE
    public riMUserInformationSearchOnDataRecieved(data: Object): void {
        if (data) {
            this.setControlValue('UserCode', data['UserCode']);
            this.setControlValue('UserName', data['UserName']);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'UserCode', false);
            this.uiForm.controls['UserCode'].markAsDirty();
        }
    }

    /**
     * This function triggers candeactive modal
     */
    public canDeactivate(): Observable<boolean> {
        this.routeAwayGlobals.setSaveEnabledFlag(this.uiForm.dirty);
        return this.routeAwayComponent.canDeactivate();
    }

    public getCreditApprovalData(): void {
        let lookupQuery: any;
        lookupQuery = [{
            'table': 'UserApprovalLevel',
            'query': {},
            'fields': ['ApprovalLevelCode', 'ApprovalLevelDescription', 'ApprovalLevelLowerLimit', 'ApprovalLevelUpperLimit', 'ApprovalLevelMonthlyLimit']
        }];

        this.LookUp.lookUpPromise(lookupQuery).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data[0].length) {
                this.creditAppLevelList = data[0];
            }
        }).catch(error => {
            this.displayError(MessageConstant.Message.GeneralError, error);
        });
    }

}
