import { OnInit, Component, Injector } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { LocalStorageService } from 'ngx-webstorage';

import { AppWebWorker } from '@shared/web-worker/app-worker';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { ENotificationType, PushNotificationsService } from '@shared/services/push.notification.service';
import { GlobalNotificationsService } from '@shared/services/global.notifications.service';
import { HttpRequestOptions, HttpServiceUrl, IWebWorkerData } from '@app/base/httpRequestOptions';
import { HttpWorker } from '@shared/web-worker/http-worker';
import { MessageConstant } from '@shared/constants/message.constant';
import { ModalAdvService } from '@shared/components/modal-adv/modal-adv.service';
import { RestService, HTTP_METHOD } from '@shared/services/rest-service';
import { Utils } from '@shared/services/utility';
import { WebWorkerService } from '@shared/web-worker/web-worker';
import { config } from '../Importing/template-config';
import { environment } from 'projects/newui-v2/src/environments/environment';

declare var gapi: any;
declare var google: any;

const Client_Id = environment['CLIENT_ID'];
const Scope = ['https://www.googleapis.com/auth/drive.file'];

@Component({
    templateUrl: 'riImport.html',
    styles: [
        `.red-bdr select {border-color: red}
    `]
})

export class RiImportComponent implements OnInit {
    public pageTitle: string = 'Import Records';
    public config = config;
    public template: string;
    public operation: string;

    public isRequesting: boolean = false;

    public importType: string;
    public fileName: string;
    public generatedTemplateURL: string;
    public importFileName: string;
    public importFilePath: string;
    public importTypeInvalid: boolean = false;
    public importFileInvalid: boolean = false;
    public localeId: string;

    public dropDownMenus: any = [];
    public driveTemplateURL: string;

    public recordList: Array<Object>;

    public menus: Array<any> = [
        { program: '/Application/iCABSANatAccountsUpload.htm', text: 'Key Accounts' },
        { program: '/ContactManagement/iCABSCMProspectBulkImport.htm', text: 'Prospects' }
    ];

    public requestOptions: HttpRequestOptions;

    private pickerApiLoaded = false;
    private oauthToken?: any;

    private titleService: Title;
    private restService: RestService;
    private utils: Utils;
    private notifications: GlobalNotificationsService;
    private pushNotifications: PushNotificationsService;
    private modal: ModalAdvService;
    private ls: LocalStorageService;

    constructor(private injector: Injector) {
        this.injectServices(injector);
    }

    private injectServices(injector: Injector): void {
        this.titleService = injector.get(Title);
        this.restService = injector.get(RestService);
        this.utils = injector.get(Utils);
        this.notifications = injector.get(GlobalNotificationsService);
        this.pushNotifications = injector.get(PushNotificationsService);
        this.modal = injector.get(ModalAdvService);
        this.ls = injector.get(LocalStorageService);
    }

    ngOnInit(): void {
        this.titleService.setTitle(this.pageTitle);
        this.getMenuAccess();
        this.localeSetup();
    }

    private localeSetup(): void {
        const cultureCode = this.ls.retrieve('SETUP_INFO').localeCultureCode;

        this.localeId = (cultureCode.languageCode === 'zh' || cultureCode.languageCode === 'pt') ?
            cultureCode.localeCode : cultureCode.languageCode;
    }

    public onGdriveBtnClick(): void {
        gapi.load('auth', { 'callback': this.onAuthApiLoad.bind(this) });
        gapi.load('picker', { 'callback': this.onPickerApiLoad.bind(this) });
    }

    private onAuthApiLoad(): void {
        gapi.auth.authorize(
            {
                'client_id': Client_Id,
                'scope': Scope,
                'immediate': false
            },
            this.handleAuthResult.bind(this));
    }

    public onPickerApiLoad(): void {
        this.pickerApiLoaded = true;
    }

    public handleAuthResult(authResult: any): void {
        let self = this;
        if (authResult && !authResult.error) {
            if (authResult.access_token) {
                let view = new google.picker.View(google.picker.ViewId.DOCS);
                view.setMimeTypes('application/vnd.google-apps.spreadsheet');
                let picker = new google.picker.PickerBuilder()
                    .enableFeature(google.picker.Feature.NAV_HIDDEN)
                    .setOAuthToken(authResult.access_token)
                    .setLocale(self.localeId)
                    .addView(view)
                    .addView(new google.picker.DocsUploadView())
                    .setCallback((res: any): void => {
                        if (res[google.picker.Response.ACTION] === google.picker.Action.PICKED) {
                            self.setPickerValue(res[google.picker.Response.DOCUMENTS][0]);
                        }
                    })
                    .build();
                picker.setVisible(true);
            }
        }
    }

    private setPickerValue(data: any): void {
        if (data) {
            this.importFileName = data.name;
            this.importFilePath = data.url;
        }
    }

    private getMenuAccess(): void {
        this.menus.forEach(menu => {
            if (this.utils.hasMenuAccessList(menu.program)) {
                this.dropDownMenus.push(menu.text);
            }
        });
    }

    public onCreateBtnClk(): void {
        if (!this.validateImportType()) {
            return;
        }

        const formData = {
            templateUrl: this.template,
            templateName: this.fileName || ''
        };

        this.requestOptions = this.getRequestOption(HTTP_METHOD.METHOD_TYPE_POST, 'create');
        this.requestOptions.addBodyParams(formData);

        this.isRequesting = true;
        this.restService.executeRequest(this.requestOptions)
            .then((data) => {
                this.isRequesting = false;
                if (!this.checkError(data)) {
                    this.generatedTemplateURL = data.url;
                    window.open(data.url, '_blank');
                }
            }).catch(error => {
                this.handleError(error);
            });
    }

    public onImportBtnClick(): void {
        if (!this.validateImportType(true)) {
            return;
        }

        this.requestOptions = this.getRequestOption(HTTP_METHOD.METHOD_TYPE_POST, 'upload');
        this.requestOptions.addBodyParam('templateUrl', this.importFilePath).addPageHeader('operation', this.operation);

        const workerData: IWebWorkerData = this.requestOptions.buildWorkerData({
            shouldIgnoreSecrets: true,
            shouldSerializeBody: true
        });

        this.onImportTypeChange();

        let workerService: WebWorkerService;
        const worker: AppWebWorker = new AppWebWorker();

        workerService = new WebWorkerService(HttpWorker, environment);
        workerService.run(worker.delegatePromise, workerData, true);
        workerService.worker.onmessage = response => {
            const results: any = response.data;
            try {
                if (!this.checkError(results)) {
                    this.fetchHistory(results.oResponse.historyRecordId).then((data) => {
                        this.pushNotifications.push = {
                            message: results.oResponse.message || results.oResponse.errorMessage,
                            data: data.result,
                            type: ENotificationType.import
                        };
                    }).catch(error => {
                        this.handleError(error);
                    });
                }
            } catch (error) {
                this.handleError(error);
            }
        };

        this.notifications.displayMessage('Data is being imported in the background. '
            + 'You will get a notification once the process is complete. '
            + 'You can continue using the system while the data is being imported.', CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);

    }

    public getHistory(recordId?: string): void {
        this.fetchHistory(recordId, true).then((data) => {
            this.isRequesting = false;
            if (!this.checkError(data)) {
                if (recordId) {
                    this.modal.emitHTMLMessage({
                        msg: data.result
                    });
                } else {
                    this.recordList = data.records.sort((x: any, y: any) => +new Date(y.CreatedTime) - +new Date(x.CreatedTime));
                }
            }
        }).catch(error => {
            this.handleError(error);
        });
    }

    public fetchHistory(historyId?: string, showLoader?: boolean): Promise<any> {
        if (showLoader) this.isRequesting = true;
        this.requestOptions = this.getRequestOption(HTTP_METHOD.METHOD_TYPE_GET, 'history');
        if (historyId) {
            this.requestOptions.addPathParam(historyId);
        }
        return this.restService.executeRequest(this.requestOptions);
    }

    private getRequestOption(methodType: string, endPoint: string): HttpRequestOptions {
        this.requestOptions = HttpRequestOptions
            .create(this.injector, HttpServiceUrl.IMPORT)
            .addEndpoint(endPoint)
            .addPathParam(this.utils.getCountryCode())
            .addPathParam(this.utils.getBusinessCode())
            .addIgnoreSecretKey(true)
            .addMethodType(methodType);

        return this.requestOptions;
    }

    public onImportTypeChange(): void {
        this.importFilePath = '';
        this.importFileName = '';
        this.generatedTemplateURL = '';
        this.fileName = '';
        if (this.importType) {
            const importKey = this.importType.split(/\s/).join('');
            this.template = this.config[importKey]['url'];
            this.operation = this.config[importKey]['operation'];
        }
    }

    private validateImportType(isImport?: boolean): boolean {
        this.importTypeInvalid = !this.importType;
        if (this.importTypeInvalid) {
            return false;
        }

        if (isImport && !this.importFileName) {
            this.importFileInvalid = true;
            return false;
        }
        return true;
    }

    private handleError(error: any): void {
        this.isRequesting = false;
        this.notifications.displayMessage(error, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
    }

    private checkError(data?: any): boolean {
        let isError: boolean = false;
        if (!data || data.hasOwnProperty('errorMessage') || data.hasOwnProperty('fullError')) {
            this.notifications.displayMessage(data.errorMessage || data.fullError || data || MessageConstant.Message.GeneralError);
            isError = true;
        }
        return isError;
    }
}
