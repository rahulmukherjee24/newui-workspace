export const config = {
    Prospects: {
        url: 'https://docs.google.com/spreadsheets/d/1mmx1uNXahhfJz9OMpkFcKsuzis-CoHN1w1-_NxpfYU0/edit#gid=1302914175',
        operation: 'prospect-bulk'
    },
    KeyAccounts: {
        url: 'https://docs.google.com/spreadsheets/d/1eapdwzQAidluLm3xsxxMBHFMD_yEjAXSmm-ZJeql290/edit#gid=1212991002',
        operation: 'nationalAccounts-bulk'
    }
};

