import { QueryParams } from './../../../../shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { LanguageSearchComponent } from './../../../internal/search/riMGLanguageSearch.component';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { PaginationComponent } from './../../../../shared/components/pagination/pagination';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { ErrorConstant } from './../../../../shared/constants/error.constant';

@Component({
    templateUrl: 'riMGTranslationMaintenance.html'
})

export class TranslationMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('screensConstantsGrid') screensConstantsGrid: GridAdvancedComponent;
    @ViewChild('screensConstantsPagination') screensConstantsPagination: PaginationComponent;
    @ViewChild('errorMessagesGrid') errorMessagesGrid: GridAdvancedComponent;
    @ViewChild('errorMessagesPagination') errorMessagesPagination: PaginationComponent;
    @ViewChild('batchProcessTypesGrid') batchProcessTypesGrid: GridAdvancedComponent;
    @ViewChild('batchProcessTypesPagination') batchProcessTypesPagination: PaginationComponent;
    @ViewChild('controlTextGrid') controlTextGrid: GridAdvancedComponent;
    @ViewChild('controlTextPagination') controlTextPagination: PaginationComponent;

    private focusElement: any;
    private queryParams: Object = {
        operation: 'Model/riMGTranslationMaintenance',
        module: 'translation',
        method: 'it-functions/ri-model'
    };

    public controls = [
        { name: 'LanguageCode', type: MntConst.eTypeTextFree },
        { name: 'View' },
        { name: 'LanguageDescription', disabled: true },
        { name: 'FilterTranslationLanguageValue', type: MntConst.eTypeTextFree }
    ];
    public languageSearch: any = {
        autoOpen: false,
        showCloseButton: true,
        childConfigParams: {
            'parentMode': 'LookUp',
            'showAddNew': false
        },
        modalConfig: {
            backdrop: 'static',
            keyboard: true
        },
        contentComponent: LanguageSearchComponent,
        showHeader: true,
        searchModalRoute: '',
        disabled: false
    };
    public screensConstantsGridParams: Object = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0,
        cacheRefresh: true
    };
    public errorMessagesGridParams: Object = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0,
        cacheRefresh: true
    };
    public batchProcessTypesGridParams: Object = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0,
        cacheRefresh: true
    };
    public controlTextGridParams: Object = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0,
        cacheRefresh: true
    };
    public uiDisplay: any = {
        tab: {
            tab1: { id: 'grdScreenConstants', visible: true, active: true },
            tab2: { id: 'grdErrorMessages', visible: true, active: false },
            tab3: { id: 'grdBatchProcessTypes', visible: true, active: false },
            tab4: { id: 'grdControlText', visible: true, active: false }
        }
    };

    public pageId: string = '';
    public isValidCode: boolean = false;
    public isScreensConstantsPagination: boolean = true;
    public iserrorMessagesPagination: boolean = true;
    public isbatchProcessTypesPagination: boolean = true;
    public iscontrolTextGridPagination: boolean = true;
    public isScreensConstantsRefresh: boolean = true;
    public iserrorMessagesRefresh: boolean = true;
    public isbatchProcessTypesRefresh: boolean = true;
    public iscontrolTextGridRefresh: boolean = true;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.RIMGTRANSLATIONMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Translations';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.screensConstantsGridHandle = this.utils.randomSixDigitString();
        this.pageParams.errorMessagesGridHandle = this.utils.randomSixDigitString();
        this.pageParams.batchProcessTypesGridHandle = this.utils.randomSixDigitString();
        this.pageParams.controlTextGridHandle = this.utils.randomSixDigitString();
    }

    ngAfterViewInit(): void {
        this.windowOnload();
        this.buildScreenConstantsGrid();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.screensConstantsGrid.DefaultBorderColor = 'ADD8E6';
        this.screensConstantsGrid.DefaultTextColor = '0000FF';
        this.screensConstantsGrid.HighlightBar = true;
        this.screensConstantsGrid.FunctionPaging = true;
        this.screensConstantsGrid.FunctionUpdateSupport = true;
        this.screensConstantsGrid.FunctionTabSupport = true;

        this.errorMessagesGrid.DefaultBorderColor = 'ADD8E6';
        this.errorMessagesGrid.DefaultTextColor = '0000FF';
        this.errorMessagesGrid.HighlightBar = true;
        this.errorMessagesGrid.FunctionPaging = true;
        this.errorMessagesGrid.FunctionUpdateSupport = true;
        this.errorMessagesGrid.FunctionTabSupport = true;

        this.batchProcessTypesGrid.DefaultBorderColor = 'ADD8E6';
        this.batchProcessTypesGrid.DefaultTextColor = '0000FF';
        this.batchProcessTypesGrid.HighlightBar = true;
        this.batchProcessTypesGrid.FunctionPaging = true;
        this.batchProcessTypesGrid.FunctionUpdateSupport = true;
        this.batchProcessTypesGrid.FunctionTabSupport = true;

        this.controlTextGrid.DefaultBorderColor = 'ADD8E6';
        this.controlTextGrid.DefaultTextColor = '0000FF';
        this.controlTextGrid.HighlightBar = true;
        this.controlTextGrid.FunctionPaging = true;
        this.controlTextGrid.FunctionUpdateSupport = true;
        this.controlTextGrid.FunctionTabSupport = true;

        this.setControlValue('View', 'All');
        this.pageParams.currentTab = this.uiDisplay.tab.tab1.id;
        this.pageParams.isLanguageCode = false;
    }

    //Fetch Language Description
    private doLookup(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();

        searchParams.set(this.serviceConstants.Action, '0');
        searchParams.set('LanguageCode', this.getControlValue('LanguageCode'));
        this.queryParams['search'] = searchParams;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'])
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setControlValue('LanguageDescription', '');
                        this.isValidCode = false;
                        if (this.pageParams.currentTab === 'grdScreenConstants') {
                            this.buildScreenConstantsGrid();
                        } else {
                            this.buildControlTextGrid();
                        }
                    } else {
                        if (data.LanguageDescription) {
                            this.setControlValue('LanguageDescription', data.LanguageDescription);
                            this.isValidCode = true;
                            this.loadCurrentGrid();
                        } else {
                            this.setControlValue('LanguageCode', '');
                            this.setControlValue('LanguageDescription', '');
                            this.isValidCode = false;
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error.fullError));
                });
    }

    //Screens Constants Grid Starts
    private buildScreenConstantsGrid(): void {
        this.screensConstantsGrid.Clear();
        this.screensConstantsGrid.AddColumn('TranslationValue', 'GridScreenConstants', 'TranslationValue', MntConst.eTypeTextFree, 60, false);
        this.screensConstantsGrid.AddColumnTabSupport('TranslationValue', true);
        this.screensConstantsGrid.AddColumn('TranslationLanguageValue', 'GridScreenConstants', 'TranslationLanguageValue', MntConst.eTypeTextFree, 60, false);
        this.screensConstantsGrid.AddColumnTabSupport('TranslationLanguageValue', true);
        this.screensConstantsGrid.AddColumnUpdateSupport('TranslationLanguageValue', true);
        this.screensConstantsGrid.SetColumnMaxLength('TranslationLanguageValue', 255);
        this.screensConstantsGrid.Complete();
        this.loadScreensConstantsGrid();
    }

    private loadScreensConstantsGrid(): void {
        this.screensConstantsGrid.UpdateHeader = true;
        this.screensConstantsGrid.UpdateBody = true;
        this.screensConstantsGrid.UpdateFooter = true;
        let search: QueryParams = this.getURLSearchParamObject();

        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.GridMode, this.screensConstantsGridParams['riGridMode']);
        search.set(this.serviceConstants.GridHandle, this.pageParams.screensConstantsGridHandle);
        search.set(this.serviceConstants.PageSize, this.screensConstantsGridParams['itemsPerPage']);
        search.set(this.serviceConstants.PageCurrent, this.screensConstantsGridParams['pageCurrent']);
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.screensConstantsGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.screensConstantsGrid.SortOrder);
        if (this.screensConstantsGridParams['cacheRefresh']) {
            search.set(this.serviceConstants.GridCacheRefresh, 'true');
        }

        search.set('LanguageCode', this.getControlValue('LanguageCode'));
        search.set('LanguageDescription', this.getControlValue('LanguageDescription'));
        search.set('Type', this.getControlValue('View'));
        search.set('FilterTranslationLanguageValue', this.getControlValue('FilterTranslationLanguageValue'));
        search.set('CurrentTabID', this.pageParams.currentTab);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.screensConstantsGridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                    this.screensConstantsGridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    if (this.screensConstantsGrid.Update) {
                        if (this.getAttribute('Row') !== '') {
                            this.screensConstantsGrid.StartRow = this.getAttribute('Row');
                            this.screensConstantsGrid.StartColumn = 0;
                            this.screensConstantsGrid.RowID = this.getAttribute('RowID');
                            this.screensConstantsGrid.UpdateHeader = true;
                            this.screensConstantsGrid.UpdateBody = true;
                            this.screensConstantsGrid.UpdateFooter = true;
                        }
                    }
                    this.isScreensConstantsRefresh = false;
                    if (data.pageData && (data.pageData.lastPageNumber * 10) > 0) {
                        this.isScreensConstantsPagination = false;
                    } else {
                        this.isScreensConstantsPagination = true;
                    }
                    this.screensConstantsGrid.RefreshRequired();
                    this.screensConstantsGrid.Execute(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error['fullError']));
            });
    }

    // update TranslationConstant column in screenConstantsGrid
    private updateScreensConstantsGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};

        search.set(this.serviceConstants.Action, '2');
        postParams[this.serviceConstants.GridMode] = '3';
        postParams[this.serviceConstants.GridHandle] = this.pageParams.screensConstantsGridHandle;
        postParams[this.serviceConstants.PageSize] = this.screensConstantsGridParams['itemsPerPage'];
        postParams[this.serviceConstants.PageCurrent] = this.screensConstantsGridParams['pageCurrent'];
        postParams[this.serviceConstants.GridHeaderClickedColumn] = this.screensConstantsGrid.HeaderClickedColumn;
        postParams[this.serviceConstants.GridSortOrder] = this.screensConstantsGrid.SortOrder;

        postParams['TranslationValueRowID'] = this.screensConstantsGrid.Details.GetAttribute('TranslationValue', 'rowid');
        postParams['TranslationValue'] = this.screensConstantsGrid.Details.GetValue('TranslationValue');
        postParams['TranslationLanguageValueRowID'] = this.screensConstantsGrid.Details.GetAttribute('TranslationLanguageValue', 'rowid');
        postParams['TranslationLanguageValue'] = this.screensConstantsGrid.Details.GetValue('TranslationLanguageValue');
        postParams['LanguageCode'] = this.getControlValue('LanguageCode');
        postParams['CurrentTabID'] = this.pageParams.currentTab;
        postParams['Mode'] = 'Update';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.screensConstantsGrid.Mode = MntConst.eModeNormal;
                if (data.hasError) {
                    let modalVO: ICabsModalVO = new ICabsModalVO(data.errorMessage, data.fullError);
                    modalVO.closeCallback = this.onErrorCloseCallback.bind(this);
                    this.modalAdvService.emitError(modalVO);
                }
            },
            error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error['fullError']));
            });
    }

    public onErrorCloseCallback(): void {
        this.screensConstantsGrid.setFocusBack(this.focusElement);
    }

    public getScreensConstantsGridCurrentPage(currentPage: any): void {
        this.screensConstantsGridParams['cacheRefresh'] = false;
        this.screensConstantsGridParams['pageCurrent'] = currentPage.value;
        this.screensConstantsGrid.RefreshRequired();
        this.loadScreensConstantsGrid();
    }

    public screenConstantsGridRefresh(): void {
        this.screensConstantsGridParams['cacheRefresh'] = true;
        this.screensConstantsGrid.RefreshRequired();
        this.loadScreensConstantsGrid();
    }
    //Screens Constants Grid Ends

    //Error Messages Grid Starts
    private buildErrorMessagesGrid(): void {
        this.errorMessagesGrid.Clear();
        this.errorMessagesGrid.AddColumn('ErrorMessageCode', 'GridErrorMessages', 'ErrorMessageCode', MntConst.eTypeInteger, 10, false);
        this.errorMessagesGrid.AddColumnTabSupport('ErrorMessageCode', true);
        this.errorMessagesGrid.AddColumn('ErrorMessageDescription', 'GridErrorMessages', 'ErrorMessageDescription', MntConst.eTypeTextFree, 255, false);
        this.errorMessagesGrid.AddColumnTabSupport('ErrorMessageDescription', true);
        this.errorMessagesGrid.AddColumn('ErrorMessageDisplayDescription', 'GridErrorMessages', 'ErrorMessageDisplayDescription', MntConst.eTypeTextFree, 50, false);
        this.errorMessagesGrid.AddColumnUpdateSupport('ErrorMessageDisplayDescription', true);
        this.errorMessagesGrid.SetColumnMaxLength('ErrorMessageDisplayDescription', 255);
        this.errorMessagesGrid.Complete();
        this.loadErrorMessagesGrid();
    }

    private loadErrorMessagesGrid(): void {
        this.errorMessagesGrid.UpdateHeader = true;
        this.errorMessagesGrid.UpdateBody = true;
        this.errorMessagesGrid.UpdateFooter = true;
        let search: QueryParams = this.getURLSearchParamObject(), sortOrder: string = 'Ascending';

        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.GridMode, this.errorMessagesGridParams['riGridMode']);
        search.set(this.serviceConstants.GridHandle, this.pageParams.errorMessagesGridHandle);
        search.set(this.serviceConstants.PageSize, this.errorMessagesGridParams['itemsPerPage']);
        search.set(this.serviceConstants.PageCurrent, this.errorMessagesGridParams['pageCurrent']);
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.screensConstantsGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, sortOrder);
        if (this.errorMessagesGridParams['cacheRefresh']) {
            search.set(this.serviceConstants.GridCacheRefresh, 'true');
        }

        search.set('LanguageCode', this.getControlValue('LanguageCode'));
        search.set('LanguageDescription', this.getControlValue('LanguageDescription'));
        search.set('Type', this.getControlValue('View'));
        search.set('FilterTranslationLanguageValue', this.getControlValue('FilterTranslationLanguageValue'));
        search.set('CurrentTabID', this.uiDisplay.tab.tab2.id);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.errorMessagesGridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                    this.errorMessagesGridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    if (this.errorMessagesGrid.Update) {
                        if (this.getAttribute('Row') !== '') {
                            this.errorMessagesGrid.StartRow = this.getAttribute('Row');
                            this.errorMessagesGrid.StartColumn = 0;
                            this.errorMessagesGrid.RowID = this.getAttribute('RowID');
                            this.errorMessagesGrid.UpdateHeader = true;
                            this.errorMessagesGrid.UpdateBody = true;
                            this.errorMessagesGrid.UpdateFooter = true;
                        }
                    }
                    this.iserrorMessagesRefresh = false;
                    if (data.pageData && (data.pageData.lastPageNumber * 10) > 0) {
                        this.iserrorMessagesPagination = false;
                    } else {
                        this.iserrorMessagesPagination = true;
                    }
                    this.errorMessagesGrid.RefreshRequired();
                    this.errorMessagesGrid.Execute(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // update TranslationDescription column in errorMessagesGrid
    private updateErrorMessagesGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject(), postParams: Object = {}, sortOrder: string = 'Ascending';

        search.set(this.serviceConstants.Action, '2');
        postParams[this.serviceConstants.GridMode] = '3';
        postParams[this.serviceConstants.GridHandle] = this.pageParams.errorMessagesGridHandle;
        postParams[this.serviceConstants.PageSize] = this.errorMessagesGridParams['itemsPerPage'];
        postParams[this.serviceConstants.PageCurrent] = this.errorMessagesGridParams['pageCurrent'];
        postParams[this.serviceConstants.GridHeaderClickedColumn] = this.errorMessagesGrid.HeaderClickedColumn;
        postParams[this.serviceConstants.GridSortOrder] = sortOrder;

        postParams['ErrorMessageCodeRowID'] = this.errorMessagesGrid.Details.GetAttribute('ErrorMessageCode', 'rowid');
        postParams['ErrorMessageCode'] = this.errorMessagesGrid.Details.GetValue('ErrorMessageCode');
        postParams['ErrorMessageDisplayDescriptionRowID'] = this.errorMessagesGrid.Details.GetAttribute('ErrorMessageDisplayDescription', 'rowid');
        postParams['ErrorMessageDisplayDescription'] = this.errorMessagesGrid.Details.GetValue('ErrorMessageDisplayDescription');
        postParams['ErrorMessageDescription'] = this.errorMessagesGrid.Details.GetValue('ErrorMessageDescription');
        postParams['LanguageCode'] = this.getControlValue('LanguageCode');
        postParams['CurrentTabID'] = this.pageParams.currentTab;
        postParams['Mode'] = 'Update';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.errorMessagesGrid.Mode = MntConst.eModeNormal;
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
            },
            error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error['fullError']));
            });
    }

    public getErrorMessagesGridCurrentPage(currentPage: any): void {
        this.errorMessagesGridParams['cacheRefresh'] = false;
        this.errorMessagesGridParams['pageCurrent'] = currentPage.value;
        this.errorMessagesGrid.RefreshRequired();
        this.loadErrorMessagesGrid();
    }

    public errorMessagesGridRefresh(): void {
        this.errorMessagesGridParams['cacheRefresh'] = true;
        this.pageParams.errorMessagesGridHandle = this.utils.randomSixDigitString();
        this.errorMessagesGrid.RefreshRequired();
        this.loadErrorMessagesGrid();
    }
    //Error Messages Grid Ends

    //Batch Process Types Starts
    private buildBatchProcessTypesGrid(): void {
        this.batchProcessTypesGrid.Clear();
        this.batchProcessTypesGrid.AddColumn('BatchProcessTypeCode', 'GridBatchProcessTypes', 'BatchProcessTypeCode', MntConst.eTypeInteger, 10, false);
        this.batchProcessTypesGrid.AddColumnTabSupport('BatchProcessTypeCode', true);
        this.batchProcessTypesGrid.AddColumn('BatchProcessTypeDescription', 'GridBatchProcessTypes', 'BatchProcessTypeDescription', MntConst.eTypeTextFree, 20, false);
        this.batchProcessTypesGrid.AddColumnTabSupport('BatchProcessTypeDescription', true);
        this.batchProcessTypesGrid.AddColumn('BatchProcessTypeDisplayDesc', 'GridBatchProcessTypes', 'BatchProcessTypeDisplayDesc', MntConst.eTypeTextFree, 20, false);
        this.batchProcessTypesGrid.AddColumnTabSupport('BatchProcessTypeDisplayDes', true);
        this.batchProcessTypesGrid.AddColumnUpdateSupport('BatchProcessTypeDisplayDesc', true);
        this.batchProcessTypesGrid.SetColumnMaxLength('BatchProcessTypeDisplayDesc', 255);
        this.batchProcessTypesGrid.Complete();
        this.loadBatchProcessTypesGrid();
    }

    private loadBatchProcessTypesGrid(): void {
        this.batchProcessTypesGrid.UpdateHeader = true;
        this.batchProcessTypesGrid.UpdateBody = true;
        this.batchProcessTypesGrid.UpdateFooter = true;
        let search: QueryParams = this.getURLSearchParamObject();

        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.GridMode, this.batchProcessTypesGridParams['riGridMode']);
        search.set(this.serviceConstants.GridHandle, this.pageParams.batchProcessTypesGridHandle);
        search.set(this.serviceConstants.PageSize, this.batchProcessTypesGridParams['itemsPerPage']);
        search.set(this.serviceConstants.PageCurrent, this.batchProcessTypesGridParams['pageCurrent']);
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.batchProcessTypesGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.batchProcessTypesGrid.SortOrder);
        if (this.batchProcessTypesGridParams['cacheRefresh']) {
            search.set(this.serviceConstants.GridCacheRefresh, 'true');
        }

        search.set('LanguageCode', this.getControlValue('LanguageCode'));
        search.set('LanguageDescription', this.getControlValue('LanguageDescription'));
        search.set('Type', this.getControlValue('View'));
        search.set('FilterTranslationLanguageValue', this.getControlValue('FilterTranslationLanguageValue'));
        search.set('CurrentTabID', this.uiDisplay.tab.tab3.id);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.batchProcessTypesGridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                    this.batchProcessTypesGridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    if (this.batchProcessTypesGrid.Update) {
                        if (this.getAttribute('Row') !== '') {
                            this.batchProcessTypesGrid.StartRow = this.getAttribute('Row');
                            this.batchProcessTypesGrid.StartColumn = 0;
                            this.batchProcessTypesGrid.RowID = this.getAttribute('RowID');
                            this.batchProcessTypesGrid.UpdateHeader = true;
                            this.batchProcessTypesGrid.UpdateBody = true;
                            this.batchProcessTypesGrid.UpdateFooter = true;
                        }
                    }
                    this.isbatchProcessTypesRefresh = false;
                    if (data.pageData && (data.pageData.lastPageNumber * 10) > 0) {
                        this.isbatchProcessTypesPagination = false;
                    } else {
                        this.isbatchProcessTypesPagination = true;
                    }
                    this.batchProcessTypesGrid.RefreshRequired();
                    this.batchProcessTypesGrid.Execute(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // update TranslationDescription column in batchProcessTypesGrid
    private updateBatchProcessTypesGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};

        search.set(this.serviceConstants.Action, '2');
        postParams[this.serviceConstants.GridMode] = '3';
        postParams[this.serviceConstants.GridHandle] = this.pageParams.batchProcessTypesGridHandle;
        postParams[this.serviceConstants.PageSize] = this.batchProcessTypesGridParams['itemsPerPage'];
        postParams[this.serviceConstants.PageCurrent] = this.batchProcessTypesGridParams['pageCurrent'];
        postParams[this.serviceConstants.GridHeaderClickedColumn] = this.batchProcessTypesGrid.HeaderClickedColumn;
        postParams[this.serviceConstants.GridSortOrder] = this.batchProcessTypesGrid.SortOrder;

        postParams['BatchProcessTypeCodeRowID'] = this.batchProcessTypesGrid.Details.GetAttribute('BatchProcessTypeCode', 'rowid');
        postParams['BatchProcessTypeCode'] = this.batchProcessTypesGrid.Details.GetValue('BatchProcessTypeCode');
        postParams['BatchProcessTypeDisplayDescRowID'] = this.batchProcessTypesGrid.Details.GetAttribute('BatchProcessTypeDisplayDesc', 'rowid');
        postParams['BatchProcessTypeDisplayDesc'] = this.batchProcessTypesGrid.Details.GetValue('BatchProcessTypeDisplayDesc');
        postParams['BatchProcessTypeDescription'] = this.batchProcessTypesGrid.Details.GetValue('BatchProcessTypeDescription');
        postParams['LanguageCode'] = this.getControlValue('LanguageCode');
        postParams['CurrentTabID'] = this.pageParams.currentTab;
        postParams['Mode'] = 'Update';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.batchProcessTypesGrid.Mode = MntConst.eModeNormal;
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
            },
            error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error['fullError']));
            });
    }

    public getBatchProcessTypesGridCurrentPage(currentPage: any): void {
        this.batchProcessTypesGridParams['cacheRefresh'] = false;
        this.batchProcessTypesGridParams['pageCurrent'] = currentPage.value;
        this.batchProcessTypesGrid.RefreshRequired();
        this.loadBatchProcessTypesGrid();
    }

    public batchProcessTypesGridRefresh(): void {
        this.batchProcessTypesGridParams['cacheRefresh'] = true;
        this.batchProcessTypesGrid.RefreshRequired();
        this.loadBatchProcessTypesGrid();
    }
    //Batch Process Types Grid Ends

    //Control Text Grid  Starts
    private buildControlTextGrid(): void {
        this.controlTextGrid.Clear();
        this.controlTextGrid.AddColumn('ControlText', 'GridControlText', 'ControlText', MntConst.eTypeTextFree, 20, false);
        this.controlTextGrid.AddColumnTabSupport('ControlText', true);
        this.controlTextGrid.AddColumn('TranslatedControlText', 'GridControlText', 'TranslatedControlText', MntConst.eTypeTextFree, 60, false);
        this.controlTextGrid.AddColumnTabSupport('TranslatedControlText', true);
        this.controlTextGrid.AddColumnUpdateSupport('TranslatedControlText', true);
        this.controlTextGrid.Complete();
        this.loadControlTextGrid();
    }

    private loadControlTextGrid(): void {
        this.controlTextGrid.UpdateHeader = true;
        this.controlTextGrid.UpdateBody = true;
        this.controlTextGrid.UpdateFooter = true;
        let search: QueryParams = this.getURLSearchParamObject(), sortOrder: string = 'Ascending';

        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.GridMode, this.controlTextGridParams['riGridMode']);
        search.set(this.serviceConstants.GridHandle, this.pageParams.controlTextGridHandle);
        search.set(this.serviceConstants.PageSize, this.controlTextGridParams['itemsPerPage']);
        search.set(this.serviceConstants.PageCurrent, this.controlTextGridParams['pageCurrent']);
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.controlTextGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, sortOrder);
        if (this.controlTextGridParams['cacheRefresh']) {
            search.set(this.serviceConstants.GridCacheRefresh, 'true');
        }

        search.set('LanguageCode', this.getControlValue('LanguageCode'));
        search.set('LanguageDescription', this.getControlValue('LanguageDescription'));
        search.set('Type', this.getControlValue('View'));
        search.set('FilterTranslationLanguageValue', this.getControlValue('FilterTranslationLanguageValue'));
        search.set('CurrentTabID', this.uiDisplay.tab.tab4.id);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.controlTextGridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                    this.controlTextGridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    if (this.controlTextGrid.Update) {
                        if (this.getAttribute('Row') !== '') {
                            this.controlTextGrid.StartRow = this.getAttribute('Row');
                            this.controlTextGrid.StartColumn = 0;
                            this.controlTextGrid.RowID = this.getAttribute('RowID');
                            this.controlTextGrid.UpdateHeader = true;
                            this.controlTextGrid.UpdateBody = true;
                            this.controlTextGrid.UpdateFooter = true;
                        }
                    }
                    this.iscontrolTextGridRefresh = false;
                    if (data.pageData && (data.pageData.lastPageNumber * 10) > 0) {
                        this.iscontrolTextGridPagination = false;
                    } else {
                        this.iscontrolTextGridPagination = true;
                    }
                    this.controlTextGrid.RefreshRequired();
                    this.controlTextGrid.Execute(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // update TranslatedControlText column in batchProcessTypesGrid
    private updateControlTextGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject(), postParams: Object = {}, sortOrder: string = 'Descending';

        search.set(this.serviceConstants.Action, '2');
        postParams[this.serviceConstants.GridMode] = '3';
        postParams[this.serviceConstants.GridHandle] = this.pageParams.controlTextGridHandle;
        postParams[this.serviceConstants.PageSize] = this.controlTextGridParams['itemsPerPage'];
        postParams[this.serviceConstants.PageCurrent] = this.controlTextGridParams['pageCurrent'];
        postParams[this.serviceConstants.GridHeaderClickedColumn] = this.controlTextGrid.HeaderClickedColumn;
        postParams[this.serviceConstants.GridSortOrder] = sortOrder;

        postParams['ControlTextRowID'] = this.controlTextGrid.Details.GetAttribute('ControlText', 'rowid');
        postParams['ControlText'] = this.controlTextGrid.Details.GetValue('ControlText');
        postParams['TranslatedControlText'] = this.controlTextGrid.Details.GetValue('TranslatedControlText');
        postParams['LanguageCode'] = this.getControlValue('LanguageCode');
        postParams['CurrentTabID'] = this.pageParams.currentTab;
        postParams['Mode'] = 'Update';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.controlTextGrid.Mode = MntConst.eModeNormal;
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
            },
            error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error['fullError']));
            });
    }

    public getControlTextGridCurrentPage(currentPage: any): void {
        this.controlTextGridParams['cacheRefresh'] = false;
        this.controlTextGridParams['pageCurrent'] = currentPage.value;
        this.controlTextGrid.RefreshRequired();
        this.loadControlTextGrid();
    }

    public controlTextGridRefresh(): void {
        this.controlTextGridParams['cacheRefresh'] = true;
        this.controlTextGrid.RefreshRequired();
        this.loadControlTextGrid();
    }
    //Control Text Grid Ends

    //Language Search Ellipsis Starts
    public onlanguageSearchDataRecieved(data: any): void {
        if (data) {
            this.setControlValue('LanguageCode', data.LanguageCode);
            this.setControlValue('LanguageDescription', data.LanguageDescription);
            this.isValidCode = true;
            this.loadCurrentGrid();
        } else {
            this.setControlValue('LanguageCode', '');
            this.setControlValue('LanguageDescription', '');
            this.isValidCode = false;
        }
    }

    public onlanguageSearchDataChange(): void {
        this.screensConstantsGrid.Clear();
        this.errorMessagesGrid.Clear();
        this.batchProcessTypesGrid.Clear();
        this.controlTextGrid.Clear();
        this.isScreensConstantsPagination = true;
        this.iserrorMessagesPagination = true;
        this.isbatchProcessTypesPagination = true;
        this.iscontrolTextGridPagination = true;
        this.isScreensConstantsRefresh = true;
        this.iserrorMessagesRefresh = true;
        this.isbatchProcessTypesRefresh = true;
        this.iscontrolTextGridRefresh = true;
        if (this.getControlValue('LanguageCode')) {
            if (this.riExchange.validateForm(this.uiForm)) {
                this.doLookup();
            }
        } else {
            this.setControlValue('LanguageCode', '');
            this.setControlValue('LanguageDescription', '');
            if (this.pageParams.currentTab === 'grdScreenConstants') {
                this.buildScreenConstantsGrid();
            } else {
                this.buildControlTextGrid();
            }
        }
    }
    //Language Search Ellipsis Ends

    // Tab click functionality
    public renderTab(tabname: string): void {
        switch (tabname) {
            case 'grdScreenConstants':
                this.pageParams.isLanguageCode = false;
                this.uiDisplay.tab.tab1.active = true;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = false;
                this.uiDisplay.tab.tab4.active = false;
                this.pageParams.currentTab = this.uiDisplay.tab.tab1.id;
                this.buildScreenConstantsGrid();
                break;
            case 'grdErrorMessages':
                if (this.getControlValue('LanguageCode') && this.riExchange.validateForm(this.uiForm) && this.isValidCode) {
                    this.doLookup();
                    this.uiDisplay.tab.tab1.active = false;
                    this.uiDisplay.tab.tab2.active = true;
                    this.uiDisplay.tab.tab3.active = false;
                    this.uiDisplay.tab.tab4.active = false;
                    this.pageParams.currentTab = this.uiDisplay.tab.tab2.id;
                    this.buildErrorMessagesGrid();
                } else {
                    switch (this.pageParams.currentTab) {
                        case 'grdScreenConstants':
                            this.uiDisplay.tab.tab1.active = true;
                            break;
                        case 'grdErrorMessages':
                            this.uiDisplay.tab.tab2.active = true;
                            break;
                        case 'grdBatchProcessTypes':
                            this.uiDisplay.tab.tab3.active = true;
                            break;
                        case 'grdControlText':
                            this.uiDisplay.tab.tab4.active = true;
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 'grdBatchProcessTypes':
                if (this.getControlValue('LanguageCode') && this.riExchange.validateForm(this.uiForm) && this.isValidCode) {
                    this.doLookup();
                    this.uiDisplay.tab.tab1.active = false;
                    this.uiDisplay.tab.tab2.active = false;
                    this.uiDisplay.tab.tab3.active = true;
                    this.uiDisplay.tab.tab4.active = false;
                    this.pageParams.currentTab = this.uiDisplay.tab.tab3.id;
                    this.buildBatchProcessTypesGrid();
                } else {
                    switch (this.pageParams.currentTab) {
                        case 'grdScreenConstants':
                            this.uiDisplay.tab.tab1.active = true;
                            break;
                        case 'grdErrorMessages':
                            this.uiDisplay.tab.tab2.active = true;
                            break;
                        case 'grdBatchProcessTypes':
                            this.uiDisplay.tab.tab3.active = true;
                            break;
                        case 'grdControlText':
                            this.uiDisplay.tab.tab4.active = true;
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 'grdControlText':
                this.pageParams.isLanguageCode = false;
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = false;
                this.uiDisplay.tab.tab4.active = true;
                this.pageParams.currentTab = this.uiDisplay.tab.tab4.id;
                this.buildControlTextGrid();
                break;
        }
    }

    //On changing of View dropdown
    public selectedOption(event: string): void {
        if (event) {
            this.setControlValue('View', event);
            this.loadCurrentGrid();
        }
    }

    // Updating column data in all grids
    public onUpdateGrid(currentTab: string, data?: any): void {
        switch (currentTab) {
            case 'grdScreenConstants':
                this.focusElement = data.srcElement;
                this.updateScreensConstantsGrid();
                break;
            case 'grdErrorMessages':
                this.updateErrorMessagesGrid();
                break;
            case 'grdBatchProcessTypes':
                this.updateBatchProcessTypesGrid();
                break;
            case 'grdControlText':
                this.updateControlTextGrid();
                break;
            default:
                break;
        }
    }

    private loadCurrentGrid(): void {
        switch (this.pageParams.currentTab) {
            case 'grdScreenConstants':
                this.buildScreenConstantsGrid();
                break;
            case 'grdErrorMessages':
                this.buildErrorMessagesGrid();
                break;
            case 'grdBatchProcessTypes':
                this.buildBatchProcessTypesGrid();
                break;
            case 'grdControlText':
                this.buildControlTextGrid();
                break;
            default:
                break;
        }
    }

    public riGridBodyOnDblClick(event: any): void {
        if (this.screensConstantsGrid.CurrentColumnName === 'TranslationValue') {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.ScreenNotReady));
        }
    }

}
