import { QueryParams } from './../../../../shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';

@Component({
    templateUrl: 'riMUserTypeMenuAccessMaintenance.html',
    styles: [`
        :host /deep/ .gridtable thead tr th:nth-child(1){
                white-space: nowrap;
                padding-right: 150px;
        }
        :host /deep/ .gridtable>tbody>tr>td:first-child span{
            white-space: pre;
        }
        `]
})

export class UserTypeMenuAccessMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    private xhr: Object = {
        module: 'user',
        method: 'it-functions/ri-model',
        operation: 'Model/riMUserTypeMenuAccessMaintenance'
    };
    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'Module' },
        { name: 'ProgramL2' },
        { name: 'ProgramL3' },
        { name: 'TextFilter' }
    ];
    public itemsPerPage: number = 10;
    public totalRecords: number = 1;
    public currentPage: number = 1;
    public moduleArray: Array<any> = [];
    public programL2Array: Array<any> = [];
    public programL3Array: Array<any> = [];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.RIMUSERTYPEMENUACCESSMAINTENANCE;
        this.browserTitle = 'User Menu Access Maintenance';
        this.pageTitle = 'Menu Selection';
    }
    ngOnInit(): void {
        super.ngOnInit();
    }
    ngAfterViewInit(): void {
        this.buildModuleCombo({ Function: 'BuildModuleCombo' });
        this.buildModuleCombo({ Function: 'BuildModuleComboAll', ProgramL2: '' });
        this.buildModuleCombo({ Function: 'BuildModuleComboAll', ProgramL3: '' });
        this.riGrid.DefaultBorderColor = 'ADD8E6';
        this.riGrid.HighlightBar = true;
        this.buildGrid();
    }
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public buildModuleCombo(postData: Object): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhr['method'], this.xhr['module'], this.xhr['operation'], search, postData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data) {
                        let arr: Array<any> = [], i: number, array: Array<any> = [];
                        switch (postData['Function']) {
                            case 'BuildModuleCombo':
                                this.moduleArray = [];
                                arr = data.ValidList.split(',');
                                break;
                            case 'BuildModuleCombo2':
                                this.programL2Array = [];
                                arr = data.ValidList2.split(',');
                                break;
                            case 'BuildModuleCombo3':
                                this.programL3Array = [];
                                arr = data.ValidList3.split(',');
                                break;
                            case 'BuildModuleComboAll':
                                arr = data.ValidListAll.split(',');
                                break;
                        }
                        for (i = 0; i < arr.length; i++) {
                            if (arr[i] !== '') {
                                array.push({ text: arr[i].split('|')[1], value: arr[i].split('|')[0] });
                            }
                        }
                        switch (postData['Function']) {
                            case 'BuildModuleCombo':
                                this.moduleArray = array;
                                this.setControlValue('Module', array[0].value);
                                break;
                            case 'BuildModuleCombo2':
                                this.programL2Array = array;
                                this.setControlValue('ProgramL2', array[0].value);
                                break;
                            case 'BuildModuleCombo3':
                                this.programL3Array = array;
                                this.setControlValue('ProgramL3', array[0].value);
                                break;
                            case 'BuildModuleComboAll':
                                switch (Object.keys(postData)[1].toString()) {
                                    case 'ProgramL2':
                                        this.programL2Array = array;
                                        break;
                                    case 'ProgramL3':
                                        this.programL3Array = array;
                                        break;
                                }
                                if (Object.keys(postData)[1]) { this.setControlValue(Object.keys(postData)[1].toString(), array[0].value); }
                                break;
                        }
                        this.riGrid.RefreshRequired();
                        this.riGridBeforeExecute();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }
    public onModuleChange(value: string): void {
        this.buildModuleCombo({ Function: 'BuildModuleComboAll', ProgramL2: '' });
        this.buildModuleCombo({ Function: 'BuildModuleComboAll', ProgramL3: '' });
        this.buildModuleCombo({ Function: 'BuildModuleCombo2', ProgramL1: value });
    }
    public onProgramL2Change(value: string): void {
        this.buildModuleCombo({ Function: 'BuildModuleComboAll', ProgramL3: '' });
        this.buildModuleCombo({ Function: 'BuildModuleCombo3', ProgramL2: value });
    }
    public onProgramL3Change(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
    public onTextFilterChange(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
    //Get UserGroupCode Columns
    public buildGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let postData: Object = {
            Function: 'GetHeaders'
        };

        this.riGrid.Clear();
        this.riGrid.AddColumn('MenuOption', 'Menus', 'MenuOption', MntConst.eTypeText, 45, false, 'MenuOption');

        search.set(this.serviceConstants.Action, '6');
        search.set('riSCModule', this.getControlValue('Module'));
        search.set('riSCModule2', this.getControlValue('ProgramL2'));
        search.set('riSCModule3', this.getControlValue('ProgramL3'));
        search.set('riTextFilter', this.getControlValue('TextFilter'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhr['method'], this.xhr['module'], this.xhr['operation'], search, postData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data) {
                        let arr = data.ColumnHeaders.split('|');
                        let i;
                        for (i = 0; i < arr.length; i++) {
                            if (arr[i] !== '') {
                                this.riGrid.AddColumn(arr[i], 'MenuItems', arr[i], MntConst.eTypeImage, 6, false, arr[i + 1]);
                                i = i + 1;
                            }
                        }
                        this.riGridBeforeExecute();
                        this.riGrid.Complete();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public riGridBeforeExecute(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set('riSCModule', this.getControlValue('Module'));
        search.set('riSCModule2', this.getControlValue('ProgramL2'));
        search.set('riSCModule3', this.getControlValue('ProgramL3'));
        search.set('riTextFilter', this.getControlValue('TextFilter'));
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.PageSize, this.itemsPerPage.toString());
        search.set(this.serviceConstants.PageCurrent, this.currentPage.toString());
        search.set(this.serviceConstants.GridHandle, this.utils.gridHandle);
        search.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        search.set('riSortOrder', this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhr['method'], this.xhr['module'], this.xhr['operation'], search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.itemsPerPage : 1;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }
    //Grid Refresh
    public btnRefresh(): void {
        this.riGrid.Mode = MntConst.eModeNormal;
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
    // pagination current page
    public getCurrentPage(currentPage: any): void {
        if (this.currentPage !== currentPage.value) {
            this.currentPage = currentPage.value;
            this.riGrid.RefreshRequired();
            this.riGridBeforeExecute();
        }
    }

    public onDblClick(): void {
        if (this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName) !== '-') {
            let search: QueryParams = this.getURLSearchParamObject(), postData: Object = {};
            postData['Function'] = 'Toggle';
            search.set(this.serviceConstants.Action, '6');
            search.set('ProgramURL', this.riGrid.Details.GetAttribute('MenuOption', 'additionalproperty'));
            search.set('UserTypeCode', this.riGrid.CurrentColumnName);
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.xhr['method'], this.xhr['module'], this.xhr['operation'], search, postData)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data) {
                            this.riGrid.RefreshRequired();
                            this.riGridBeforeExecute();
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }
}
