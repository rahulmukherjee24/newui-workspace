import { SharedLibModule } from './../../../shared-lib/src/lib/shared-lib.module';
import { LibModule } from './../../../lib/src/lib/lib.module';
import { PushNotificationsService } from '@shared/services/push.notification.service';
import { DeepLinkService } from './../shared/services/deeplink.service';
import { APP_BASE_HREF } from '@angular/common';
import { AppComponent } from './app.component';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { NgModule } from '@angular/core';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { RouterModule } from '@angular/router';
import { Store } from './reducers/index';
import { TabsModule } from 'ngx-bootstrap';
import { TranslateModule } from '@ngx-translate/core';

import { AccessGuardService } from './../shared/services/access-guard.service';
import { AjaxObservableConstant } from './../shared/constants/ajax-observable.constant';
import { AuthService } from './../shared/services/auth.service';
import { CanLoadService } from './../shared/services/can-load.service';
import { CBBService } from './../shared/services/cbb.service';
import { ComponentInteractionService } from './../shared/services/component-interaction.service';
import { ErrorConstant } from './../shared/constants/error.constant';
import { ErrorService } from './../shared/services/error.service';
import { GcpService } from './../GCP/gcp-service';
import { GlobalConstant } from './../shared/constants/global.constant';
import { GlobalizeService } from './../shared/services/globalize.service';
import { HttpService } from './../shared/services/http-service';
import { InternalSearchEllipsisModule } from './internal/search-ellipsis.module';
import { InternalSearchModule } from './internal/search.module';
import { LocaleTranslationService } from './../shared/services/translation.service';
import { LoginComponent } from './login/login';
import { LoginGuardService } from './login/login-guard.service';
import { LookUp } from './../shared/services/lookup';
import { MessageService } from './../shared/services/message.service';
import { ModalAdvService } from './../shared/components/modal-adv/modal-adv.service';
import { PageDataService } from './../shared/services/page-data.service';
import { PostCodeUtils } from './../shared/services/postCode-utility';
import { PostLoginComponent } from './login/post-login';
import { RiExchange } from './../shared/services/riExchange';
import { RouteAwayGlobals } from './../shared/services/route-away-global.service';
import { RouteAwayGuardService } from './../shared/services/route-away-guard.service';
import { SearchEllipsisBusinessModule } from './internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from './internal/search-ellipsis-dropdown.module';
import { ServiceConstants } from './../shared/constants/service.constants';
import { SetupComponent } from './setup/setup';
import { SharedModule } from './../shared/shared.module';
import { SpeedScript } from './../shared/services/speedscript';
import { SpeedScriptConstants } from './../shared/constants/speed-script.constant';
import { SubjectService } from './../shared/services/subject.service';
import { SysCharConstants } from './../shared/constants/syscharservice.constant';
import { UserAccessService } from './../shared/services/user-access.service';
import { Utils } from './../shared/services/utility';
import { VariableService } from './../shared/services/variable.service';
import { GlobalNotificationsService } from '@shared/services/global.notifications.service';
import { RestService } from '@shared/services/rest-service';
import { FocusService } from './../shared/services/focus.service';


@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        SetupComponent,
        PostLoginComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        HttpClientModule,
        InternalSearchEllipsisModule,
        InternalSearchModule,
        LoggerModule.forRoot({ level: NgxLoggerLevel.DEBUG }),
        NgxWebstorageModule.forRoot(),
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        SharedModule.forRoot(),
        Store,
        TabsModule.forRoot(),
        TranslateModule.forRoot(),
        LibModule,
        SharedLibModule,
        RouterModule.forRoot([
            { path: 'application/login', component: LoginComponent, canActivate: [LoginGuardService] },
            { path: 'postlogin', component: PostLoginComponent },
            { path: 'application/setup', component: SetupComponent },
            { path: 'application', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/maintenance-application.module#InternalMaintenanceApplicationModule' },
            { path: 'billtocash', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bill-to-cash/bill-to-cash.module#BillToCashModule' },
            { path: 'ccm', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './customer-contact-management/ccm.module#CCMModule' },
            { path: 'contractmanagement/accountadmin', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/account-admin.module#AccountAdminModule' },
            { path: 'contractmanagement/accountmaintenance', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/account-maintenance.module#AccountMaintenanceModule' },
            { path: 'contractmanagement/customerinfo', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/customer-info.module#CustomerInfoModule' },
            { path: 'contractmanagement/reports', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/reports.module#ReportsModule' },
            { path: 'contractmanagement/retention', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/retention.module#RetentionModule' },
            { path: 'contractmanagement/servicecoveradmin', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/service-cover-admin.module#ServiceCoverAdminModule' },
            { path: 'contractmanagement/wasteconsign', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/waste-consignement.module#WasteConsignComponent' },
            { path: 'contractmanagement/premisesmaintenance', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/premises-maintenance.module#PremiseMaintenanceModule' },
            { path: 'contractmanagement/groupaccount', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/group-account.module#GroupAccountModule' },
            { path: 'contractmanagement/general', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/general.module#GeneralModule' },
            { path: 'contractmanagement/areas', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/areas.module#AreasModule' },
            { path: 'contractmanagement/contractadmin', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/contract-admin.module#ContractAdminModule' },
            { path: 'contractmanagement/premisesadmin', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/premises-admin.module#PremiseAdminModule' },
            { path: 'grid', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/grid-search.module#InternalGridSearchModule' },
            { path: 'grid/application', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/grid-search-application.module#InternalGridSearchApplicationModule' },
            { path: 'grid/application/nav', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/grid-application.module#InternalGridApplicationModule' },
            { path: 'grid/sales', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/grid-search-sales.module#InternalGridSearchSalesModule' },
            { path: 'grid/sales/nav', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/grid-sales.module#InternalGridSalesModule' },
            { path: 'grid/service', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/grid-search-service.module#InternalGridSearchServiceModule' },
            { path: 'grid/service/drilldown', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/grid-service.module#InternalGridServiceModule' },
            { path: 'grid/service/nav', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/grid-service-menu.module#InternalGridServiceMenuModule' },
            { path: 'itfunctions', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './it-functions/it-functions.module#ITFunctionsModule' },
            { path: 'maintenance', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/maintenance.module#InternalMaintenanceModule' },
            { path: 'people', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './people/people.module#PeopleModule' },
            { path: 'renegotiations', loadChildren: './renegotiations/renegotiations.module#RenegotiationsModule' },
            { path: 'sales', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/maintenance-sales.module#InternalMaintenanceSalesModule' },
            { path: 'service', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/maintenance-service.module#InternalMaintenanceServiceModule' },
            { path: 'servicedelivery', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './service-delivery/service-delivery.module#ServiceDeliveryModule' },
            { path: 'serviceplanning', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './service-planning/service-planning.module#ServicePlanningModule' },
            { path: 'contractmanagement/reports', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/reports.module#ReportsModule' },
            { path: 'contractmanagement/servicecoveradmin', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/service-cover-admin.module#ServiceCoverAdminModule' },
            { path: 'contractmanagement/wasteconsign', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/waste-consignement.module#WasteConsignComponent' },
            { path: 'maintenance', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/maintenance.module#InternalMaintenanceModule' },
            { path: 'prospecttocontract', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './prospect-to-contract/prospect-to-contract.module#ProspectToContractModule' },
            { path: 'contractmanagement/productadmin', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/product-admin.module#ProductAdminModule' },
            { path: 'servicecovermaintenance', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './internal/service-cover-maintenance.module#ServiceCoverMaintenanceModule' },
            { path: 'contractmanagement', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './contract-management/contract-management.module#ContractManagementModule' },
            { path: 'pda-lead', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/pda-lead/bireports-pda-lead.module#PDALeadBIReportsModule' },
            { path: 'googlemap', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './google-map-pages/google-map-pages.module#GoogleMapPagesModule' },
            { path: 'bireports/sales', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/sales/bireports-sales.module#SalesBIReportsModule' },
            { path: 'bireports/invoice', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/invoice/bireports-invoice.module#InvoiceBIReportsModule' },
            { path: 'bireports/portfolio', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/portfolio/bireports-portfolio.module#PortfolioBIReportsModule' },
            { path: 'bireports/pda', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/pda/bireports-pda.module#PDABIReportsModule' },
            { path: 'bireports/service', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/service/bireports-service.module#ProductivityBIReportsModule' },
            { path: 'bireports/clientretention', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/client-retention/client-retention.module#ClientRetentionModule' },
            { path: 'bireports/turnover', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/turnover/turnover.module#TurnoverModule' },
            { path: 'bireports/creditanalysis', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/credit-analysis/bireports-credit-analysis.module#CreditAnalysisModule' },
            { path: 'bireports/actualvscontractual', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/actualvscontractual/actualvscontractual.module#ActualVsContractualBIReportsModule' },
            { path: 'bireports/cm', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/contact-management/bireports-contact-management.module#ContactManagementBIReportsModule' },
            { path: 'ccm-mobile', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './mobile-version/ccm.mobile.module#CCMMobileModule' },
            { path: 'vehiclemanagement', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './vehicle-management/vehicle.module#VehicleModule' },
            { path: 'bireports/api', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/api/bireports-api.module#ApiBIReportsModule' },
            { path: 'bireports/stockusage', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './bireports/stockusage/stockusage.module#StockUsageModule' },
            { path: 'riBatch', canActivate: [AccessGuardService], canLoad: [CanLoadService], loadChildren: './riBatch/riBatch.module#RiBatchModule' },
            { path: '**', redirectTo: 'application/login', pathMatch: 'full' },
            { path: '', redirectTo: 'application/login', pathMatch: 'full' }
        ], { useHash: true })
    ],
    providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        AccessGuardService,
        AjaxObservableConstant,
        AuthService,
        CanLoadService,
        CBBService,
        ComponentInteractionService,
        ErrorConstant,
        ErrorService,
        GcpService,
        GlobalConstant,
        GlobalizeService,
        HttpService,
        LocaleTranslationService,
        LoginGuardService,
        LookUp,
        MessageService,
        ModalAdvService,
        PageDataService,
        PostCodeUtils,
        RiExchange,
        RouteAwayGlobals,
        RouteAwayGuardService,
        ServiceConstants,
        SpeedScript,
        SpeedScriptConstants,
        SubjectService,
        SysCharConstants,
        Title,
        UserAccessService,
        Utils,
        VariableService,
        DeepLinkService,
        PushNotificationsService,
        RestService,
        GlobalNotificationsService,
        FocusService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
