import { CustomAction } from './custom-action';
import { ActionReducer } from '@ngrx/store';
import { ActionTypes } from '../actions/prospect';

interface Prospect {
    data: Object;
    code: Object;
}

const initialState: Prospect = {
    data: {},
    code: {}
};

export function storeFunction(state: any = initialState, action: CustomAction): any {
    switch (action.type) {
        case ActionTypes.SAVE_DATA:
            state = Object.assign({}, state, {
                data: action.payload,
                action: ActionTypes.SAVE_DATA
            });
            return state;
        case ActionTypes.SAVE_CODE:
            state = Object.assign({}, state, {
                code: action.payload,
                action: ActionTypes.SAVE_CODE
            });
            return state;
        case ActionTypes.CLEAR_DATA:
            state = Object.assign({}, state, {
                data: {}
            });
            return state;
        case ActionTypes.SAVE_SYSTEM_PARAMETER:
            state = Object.assign({}, state, {
                data: action.payload,
                action: ActionTypes.SAVE_SYSTEM_PARAMETER
            });
            return state;
        case ActionTypes.CONTROL_DEFAULT_VALUE:
            state = Object.assign({}, state, {
                data: action.payload,
                action: ActionTypes.CONTROL_DEFAULT_VALUE
            });
            return state;
        case ActionTypes.RI_EXCHANGE:
            state = Object.assign({}, state, {
                data: action.payload,
                action: ActionTypes.RI_EXCHANGE
            });
            return state;
        case ActionTypes.EXCHANGE_METHOD:
            state = Object.assign({}, state, {
                data: action.payload,
                action: ActionTypes.EXCHANGE_METHOD
            });
            return state;
        case ActionTypes.FORM_CONTROLS:
            state = Object.assign({}, state, {
                data: action.payload,
                action: ActionTypes.FORM_CONTROLS
            });
            return state;
        case ActionTypes.PARENT_FORM:
            state = Object.assign({}, state, {
                data: action.payload,
                action: ActionTypes.PARENT_FORM
            });
            return state;
        case ActionTypes.UPDATE_FORMS:
            state = Object.assign({}, state, {
                data: action.payload,
                action: ActionTypes.UPDATE_FORMS
            });
            return state;
        case ActionTypes.UPDATE_ACCOUNT_ADDRESS_FROM_PREMISE:
            state = Object.assign({}, state, {
                data: action.payload,
                action: ActionTypes.UPDATE_ACCOUNT_ADDRESS_FROM_PREMISE
            });
            return state;
        default:
            return state;
    }
}

export const prospect: ActionReducer<any> = storeFunction;
