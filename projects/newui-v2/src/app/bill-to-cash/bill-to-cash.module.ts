import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../../shared/shared.module';
/*import { InternalGridSearchModule } from '../internal/grid-search.module';
import { InternalMaintenanceModule } from '../internal/maintenance.module';*/
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { BillToCashRouteDefinitions } from './bill-to-cash.route';
import { BillToCashRootComponent } from './bill-to-cash.component';
import { BillToCashConstants } from './bill-to-cash-constants';
import { ARGenerateNextInvoiceRunForecastComponent } from './InvoiceAndAPIReporting/NextInvoiceRun-ForecastGeneration/iCABSARGenerateNextInvoiceRunForecast.component';
import { CreditAndReInvoiceGridComponent } from './InvoicedAndCreditReporting/iCABSACreditAndReInvoiceGrid';
import { ApiDateMaintenanceComponent } from './InvoiceProductionAndAPI/iCABSAAPIDateMaintenance';
import { ApiReverseMaintenanceComponent } from './InvoiceProductionAndAPI/iCABSAAPIReverse';
import { ApiGenerationMaintenanceComponent } from './InvoiceProductionAndAPI/iCABSAApplyAPIGeneration';
import { ApplyApiGridComponent } from './InvoiceProductionAndAPI/iCABSAApplyApiGrid';
import { ContractAPIMaintenanceComponent } from './InvoiceProductionAndAPI/iCABSAContractAPIMaintenance.component';
import { ServiceCoverAPIGridComponent } from './InvoiceProductionAndAPI/iCABSAServiceCoverAPIGrid';
import { APICodeMaintenanceComponent } from './InvoiceProductionAndAPI/iCABSBAPICodeMaintenance.component';
import { InvoiceRunDatesGridComponent } from './InvoiceProductionAndAPI/iCABSBInvoiceRunDatesGrid';
import { ReleaseForInvoiceGridComponent } from './JobReleaseInvoice/Employee/iCABSReleaseForInvoiceGrid.component';
import { PortfolioGeneralMaintenanceComponent } from './Portfolio/iCABSAPortfolioGeneralMaintenance.component';
import { ActualVsContractualBranchComponent } from './Portfolio/iCABSSeActualVsContractualBranch';
import { ActualVsContractualBusinessComponent } from './Portfolio/iCABSSeActualVsContractualBusiness';
import { ActualVsContractualServiceCoverComponent } from './Portfolio/iCABSSeActualVsContractualServiceCover.component';
import { CreditAndReInvoiceMaintenanceComponent } from './PostInvoiceManagement/CreditAndReInvoice/iCABSACreditAndReInvoiceMaintenance.component';
import { ContractInvoiceGridComponent } from './PostInvoiceManagement/iCABSAContractInvoiceGrid';
import { ServiceCoverAcceptGridComponent } from './PostInvoiceManagement/iCABSAServiceCoverAcceptGrid.component';
import { ServiceVisitReleaseGridComponent } from './PreInvoiceManagement/SharedVisitRelease/iCABSSeServiceVisitReleaseGrid.component';
import { InvoiceDetailsMaintainanceComponent } from './../internal/maintenance/iCABSAInvoiceDetailsMaintenance';
import { InvoiceGroupMaintenanceComponent } from './PreInvoiceManagement/iCABSAInvoiceGroupMaintenance.component';
import { TaxRegistrationChangeComponent } from './PreInvoiceManagement/iCABSATaxRegistrationChange.component';
import { VarianceFromPriceCostGridComponent } from './SalesReport/iCABSARVarianceFromPriceCostGrid.component';
import { AddressInvoiceTabComponent } from './tabs/AddressInvoiceTab.component';
import { AddressStatementTabComponent } from './tabs/AddressStatementTab.component';
import { EDIInvoicingTabComponent } from './tabs/EDIInvoicing.component';
import { GeneralTabComponent } from './tabs/GeneralTab.component';

@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        BillToCashRouteDefinitions
    ],
    declarations: [
        ARGenerateNextInvoiceRunForecastComponent,
        CreditAndReInvoiceGridComponent,
        ApiDateMaintenanceComponent,
        ApiReverseMaintenanceComponent,
        ApiGenerationMaintenanceComponent,
        ApplyApiGridComponent,
        ContractAPIMaintenanceComponent,
        ServiceCoverAPIGridComponent,
        APICodeMaintenanceComponent,
        InvoiceRunDatesGridComponent,
        ReleaseForInvoiceGridComponent,
        PortfolioGeneralMaintenanceComponent,
        ActualVsContractualBranchComponent,
        ActualVsContractualBusinessComponent,
        ActualVsContractualServiceCoverComponent,
        CreditAndReInvoiceMaintenanceComponent,
        ContractInvoiceGridComponent,
        ServiceCoverAcceptGridComponent,
        ServiceVisitReleaseGridComponent,
        InvoiceGroupMaintenanceComponent,
        TaxRegistrationChangeComponent,
        VarianceFromPriceCostGridComponent,
        AddressInvoiceTabComponent,
        AddressStatementTabComponent,
        EDIInvoicingTabComponent,
        GeneralTabComponent,
        BillToCashRootComponent,
        InvoiceDetailsMaintainanceComponent
    ],
    providers: [
        BillToCashConstants
    ],
    entryComponents: [
        CreditAndReInvoiceGridComponent,
        ARGenerateNextInvoiceRunForecastComponent,
        ApplyApiGridComponent,
        APICodeMaintenanceComponent,
        TaxRegistrationChangeComponent,
        InvoiceRunDatesGridComponent,
        AddressInvoiceTabComponent,
        AddressStatementTabComponent,
        EDIInvoicingTabComponent,
        GeneralTabComponent,
        InvoiceDetailsMaintainanceComponent
    ]
})

export class BillToCashModule { }
