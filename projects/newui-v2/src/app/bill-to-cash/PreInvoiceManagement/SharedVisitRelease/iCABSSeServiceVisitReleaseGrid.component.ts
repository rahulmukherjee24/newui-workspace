import { Component, OnInit, OnDestroy, Injector, ViewChild, EventEmitter } from '@angular/core';

import { BaseComponent } from './../../../base/BaseComponent';
import { ContractManagementModuleRoutes, InternalGridSearchServiceModuleRoutes } from './../../../base/PageRoutes';
import { ContractSearchComponent } from './../../../internal/search/iCABSAContractSearch';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { IGridHandlers } from '@app/base/BaseComponentLight';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { PaginationComponent } from './../../../../shared/components/pagination/pagination';
import { PremiseSearchComponent } from './../../../internal/search/iCABSAPremiseSearch';
import { QueryParams } from '../../../../shared/services/http-params-wrapper';
import { ServiceCoverSearchComponent } from './../../../internal/search/iCABSAServiceCoverSearch';

@Component({
    templateUrl: 'iCABSSeServiceVisitReleaseGrid.html'
})

export class ServiceVisitReleaseGridComponent extends BaseComponent implements OnInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    private queryParams: any = {
        module: 'release',
        method: 'bill-to-cash/maintenance',
        operation: 'Service/iCABSSeServiceVisitReleaseGrid'
    };
    public setFocusOnContractNumber = new EventEmitter<boolean>();
    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceVisitFrequency', disabled: true, type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'PremiseName', disabled: true, type: MntConst.eTypeText },
        { name: 'ProductCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate }
    ];
    public ellipsis: any = {
        contractNumberSearch: {
            childConfigParams: {
                parentMode: 'LookUp'
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: ContractSearchComponent
        },
        premiseNumberSearch: {
            childConfigParams: {
                parentMode: 'LookUp'
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: PremiseSearchComponent
        },
        productSearch: {
            childConfigParams: {
                parentMode: 'LookUp-Freq'
            },
            contentComponent: ServiceCoverSearchComponent
        }
    };
    ngOnInit(): void {
        super.ngOnInit();
        this.buildGrid();
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.riGridBeforeExecute();
        } else {
            this.pageParams.totalRecords = 0;
            this.pageParams.pageSize = 10;
            this.pageParams.curPage = 1;
            this.pageParams.riGridHandle = this.utils.randomSixDigitString();
            this.windowOnload();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.routeAwayGlobals.resetRouteAwayFlags();
    }

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEVISITRELEASEGRID;
        this.browserTitle = this.pageTitle = 'Shared Visit Release';
    }

    // set current page data
    private setCurrentContractType(): void {
        this.pageParams.currentContractType = this.riExchange.getCurrentContractType();
        this.pageParams.currentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
    }

    private windowOnload(): void {
        this.setCurrentContractType();

        this.riGrid.FunctionTabSupport = true;
        this.riGrid.FunctionUpdateSupport = true;

        this.setControlValue('DateFrom', new Date(new Date().getFullYear(), 0, 1));
        this.setControlValue('DateTo', new Date());
        this.setFocusOnContractNumber.emit(true);
    }

    private populateDescriptions(): void {
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'ContractNumber')
            && !this.riExchange.riInputElement.isError(this.uiForm, 'PremiseNumber')
            && !this.riExchange.riInputElement.isError(this.uiForm, 'ProductCode')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let postData: Object = {
                Function: 'SetDisplayFields',
                BranchNumber: this.utils.getBranchCode(),
                ContractNumber: this.getControlValue('ContractNumber'),
                PremiseNumber: this.getControlValue('PremiseNumber'),
                ProductCode: this.getControlValue('ProductCode'),
                ServiceCoverRowID: this.getAttribute('ServiceCoverRowID')
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    if (data.ContractName) this.setControlValue('ContractName', data.ContractName);
                    else {
                        this.setControlValue('ContractNumber', '');
                        this.setControlValue('ContractName', '');
                    }
                    if (data.PremiseName) this.setControlValue('PremiseName', data.PremiseName);
                    else {
                        this.setControlValue('PremiseNumber', '');
                        this.setControlValue('PremiseName', '');
                    }
                    if (data.ProductDesc) this.setControlValue('ProductDesc', data.ProductDesc);
                    else {
                        this.setControlValue('ProductCode', '');
                        this.setControlValue('ProductDesc', '');
                    }
                    if (data.ServiceVisitFrequency) this.setControlValue('ServiceVisitFrequency', data.ServiceVisitFrequency);
                    if (data.ServiceVisitFrequency === '0') this.setControlValue('ServiceVisitFrequency', '');
                    this.setEllipsisConfigParams();
                    // this.buildGrid();
                    this.riGrid.RefreshRequired();
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        }
    }

    private setEllipsisConfigParams(): void {
        this.ellipsis.premiseNumberSearch.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
        this.ellipsis.premiseNumberSearch.childConfigParams.ContractName = this.getControlValue('ContractName');
        this.ellipsis.productSearch.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
        this.ellipsis.productSearch.childConfigParams.ContractName = this.getControlValue('ContractName');
        this.ellipsis.productSearch.childConfigParams.PremiseNumber = this.getControlValue('PremiseNumber');
        this.ellipsis.productSearch.childConfigParams.PremiseName = this.getControlValue('PremiseName');
        this.ellipsis.productSearch.childConfigParams.ProductCode = this.getControlValue('ProductCode');
        this.ellipsis.productSearch.childConfigParams.ProductDesc = this.getControlValue('ProductDesc');
    }

    /*------- riGrid Routines -----*/
    // Create grid staructure
    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('ContractNumber', 'ServiceVisit', 'ContractNumber', MntConst.eTypeCode, 11, true);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNumber', 'ServiceVisit', 'PremiseNumber', MntConst.eTypeInteger, 5, true);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'ServiceVisit', 'PremiseName', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('Postcode', 'ServiceVisit', 'Postcode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('Postcode', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ProductCode', 'ServiceVisit', 'ProductCode', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceVisitFrequency', 'ServiceVisit', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceDateStart', 'ServiceVisit', 'ServiceDateStart', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('NumberOfVisits', 'ServiceVisit', 'NumberOfVisits', MntConst.eTypeInteger, 3, true);
        this.riGrid.AddColumnAlign('NumberOfVisits', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('ServiceDateStart', true);
        this.riGrid.Complete();
    }
    private riGridBeforeExecute(): void {
        let gridQueryParams: QueryParams = this.getURLSearchParamObject();
        gridQueryParams.set(this.serviceConstants.Action, '2');
        gridQueryParams.set('BranchNumber', this.utils.getBranchCode());
        gridQueryParams.set('DateFrom', this.getControlValue('DateFrom'));
        gridQueryParams.set('DateTo', this.getControlValue('DateTo'));
        gridQueryParams.set('ContractNumber', this.getControlValue('ContractNumber'));
        gridQueryParams.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        gridQueryParams.set('ProductCode', this.getControlValue('ProductCode'));
        gridQueryParams.set('ServiceCoverRowID', this.getAttribute('ServiceCoverRowID'));
        gridQueryParams.set(this.serviceConstants.PageSize, this.pageParams.pageSize.toString());
        gridQueryParams.set(this.serviceConstants.PageCurrent, this.pageParams.curPage.toString());
        gridQueryParams.set(this.serviceConstants.GridCacheRefresh, 'true');
        gridQueryParams.set(this.serviceConstants.GridMode, '0');
        gridQueryParams.set(this.serviceConstants.GridHandle, this.pageParams.riGridHandle);
        gridQueryParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridQueryParams.set(this.serviceConstants.GridHTMLPage, 'Service/iCABSSeServiceVisitReleaseGrid.htm');
        gridQueryParams.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridQueryParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    this.riGrid.ResetGrid();
                } else {
                    this.pageParams.curPage = data.pageData.pageNumber || 1;
                    this.pageParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.pageSize : 1;
                    setTimeout(() => {
                        this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                    }, 100);
                    this.riGrid.RefreshRequired();
                    if (this.riGrid.Update) {
                        this.riGrid.StartRow = this.getAttribute('Row');
                        this.riGrid.StartColumn = 0;
                        this.riGrid.RowID = this.getAttribute('ServiceVisitRowID');
                        this.riGrid.UpdateHeader = false;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = false;
                    }
                    this.riGrid.Execute(data);
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private serviceVisitFocus(rsrcElement: any): void {
        rsrcElement.focus();
        this.setAttribute('Row', this.riGrid.CurrentRow);
        this.setAttribute('ContractRowID', this.riGrid.Details.GetAttribute('ContractNumber', 'rowid'));
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('PremiseNumber', 'rowid'));
        this.setAttribute('ProductCodeRowID', this.riGrid.Details.GetAttribute('ProductCode', 'rowid'));
        this.setAttribute('ServiceVisitRowID', this.riGrid.Details.GetAttribute('NumberOfVisits', 'rowid'));

        switch (this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty')) {
            case 'C':
                this.pageParams.CurrentContractTypeURLParameter = '';
                this.pageParams.contractTypeCode = 'C';
                break;
            case 'J':
                this.pageParams.CurrentContractTypeURLParameter = '<job>';
                this.pageParams.contractTypeCode = 'J';
                break;
            case 'P':
                this.pageParams.CurrentContractTypeURLParameter = '<product>';
                this.pageParams.contractTypeCode = 'P';
                break;
        }
    }

    public riGridBodyOnDblClick(event: Object): void {
        this.serviceVisitFocus(event['srcElement']);
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractNumber':
                this.navigate('Release', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                    CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                    ContractRowID: this.getAttribute('ContractRowID')
                });
                break;
            case 'PremiseNumber':
                this.navigate('Release', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    contracttypecode: this.pageParams.contractTypeCode,
                    PremiseROWID: this.getAttribute('PremiseRowID')
                });
                break;
            case 'ProductCode':
                this.navigate('Release', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                    currentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                    ServiceCoverRowID: this.getAttribute('ProductCodeRowID')
                });
                break;
            case 'NumberOfVisits':
                this.navigate('Release', InternalGridSearchServiceModuleRoutes.ICABSSESERVICEVISITRELEASEDETAILGRID, {
                    CurrentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                    ServiceCoverRowID: this.getAttribute('ProductCodeRowID')
                });
                break;
        }
    }

    public getCurrentPage(event: Object): void {
        this.pageParams.curPage = event['value'];
        this.riGridBeforeExecute();
    }

    public riGridSort(event: Object): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
    /* =================== Page UI Rutines ============*/
    public contractNumberFormatOnChange(): void {
        if (this.getControlValue('ContractNumber')) this.setControlValue('ContractNumber', this.utils.numberPadding(this.getControlValue('ContractNumber'), 8));
        this.populateDescriptions();
    }
    public contractNumberSearchOnReceive(data: Object): void {
        if (data) {
            this.setControlValue('ContractNumber', data['ContractNumber']);
            this.setControlValue('ContractName', data['ContractName']);
            this.contractNumberFormatOnChange();
        }
    }
    public premiseNumberSearchOnReceive(data: Object): void {
        if (data) {
            this.setControlValue('PremiseNumber', data['PremiseNumber']);
            this.setControlValue('PremiseName', data['PremiseName']);
            this.populateDescriptions();
        }
    }
    public productSearchOnReceive(data: Object): void {
        this.setAttribute('ServiceCoverRowID', '');
        if (data) {
            this.setControlValue('ProductCode', data['ProductCode']);
            this.setControlValue('ProductDesc', data['ProductDesc']);
            this.setAttribute('ServiceCoverRowID', data['row']['ttServiceCover']);
            this.populateDescriptions();
        }
    }
    public contractNumberOnChange(): void {
        this.contractNumberFormatOnChange();
    }
    public premiseNumberOnChange(): void {
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'PremiseNumber')) this.populateDescriptions();
    }
    public productCodeOnChange(): void {
        this.setAttribute('ServiceCoverRowID', '');
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'ProductCode')) this.populateDescriptions();
    }

    public onRiGridRefresh(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        if (this.pageParams.curPage <= 0) this.pageParams.curPage = 1;
        this.riGridBeforeExecute();
    }
}
