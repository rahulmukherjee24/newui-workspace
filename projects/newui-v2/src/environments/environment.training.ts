import { locales } from './globalize/locale';

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: true,
    NODE_ENV: 'TRAINING',
    GCP_LOGIN_API: 'https://icabsui-api-europe-staging2.appspot.com/api/nextgenui/1.0/api/x/auths/login',
    MULE_LOGIN_API: 'https://api-staging-emea.ri-gateway.com/nextgenui/1.0/api/x/auths/login',
    CLIENT_ID: '894251908073-1jvuee1e7uips930n6pb98u8inan4e18.apps.googleusercontent.com',
    SCOPE: 'profile email',
    GA_TRACK_CODE: 'UA-93679236-3',
    RESPONSE_TYPE: 'token',
    REDIRECT_URL: 'http://localhost:3000/application/login',
    OAUTH_URL: 'https://accounts.google.com/o/oauth2/auth?scope=',
    PROFILE_URL: 'https://www.googleapis.com/plus/v1/people/me',
    REVOKE_URL: 'https://accounts.google.com/o/oauth2/revoke?token=',
    LOGOUT_URL: 'https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:3000',
    TOKENINFO_URL: 'https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=',
    LOOK_UP: 'lookup/data',
    SYS_CHAR: 'settings/data',

    USER_ACCESS_URL: 'https://api-staging-emea.ri-gateway.com/nextgenui/1.0/api/x/user/menu',
    USER_CODE_URL: 'https://api-staging-emea.ri-gateway.com/nextgenui/1.0/api/x/user/information',
    USER_ACCESS_POSTFIX: 'user/menu',
    USER_CODE_POSTFIX: 'user/information',
    ZIP_SEARCH_URL: 'https://api-staging-emea.ri-gateway.com/nextgenui/x/1.0/contract-management/search',
    BASE_URL: 'https://api-staging-emea.ri-gateway.com/nextgenui/1.0/api/x/',
    BASE_URL_EUROPE: 'https://api-staging-emea.ri-gateway.com/nextgenui/1.0/api/x/',
    BASE_URL_AMERICA: 'https://api-staging-na.ri-gateway.com/nextgenui/1.0/api/x/',
    BASE_URL_AUSTRALIA: 'https://icabsui-api-australia-staging2.appspot.com/nextgenui/1.0/api/x/',
    BASE_URL_PACIFIC: 'https://api-staging-asia.ri-gateway.com/nextgenui/1.0/api/x/',
    BASE_URL_ASIA: 'https://api-staging-asia.ri-gateway.com/nextgenui/1.0/api/x/',
    BASE_URL_INDIA: 'https://api-staging-india.ri-gateway.com/nextgenui/1.0/api/x/',
    BASE_URL_INDIA2: 'https://icabsui-api-india2-staging2.appspot.com//nextgenui/1.0/api/x/',
    IMAGE_REPO_URL: 'assets/images/',

    INVOICE_HISTORY: 'http://localhost:3030/api/contractInvoiceHistory',
    CONTACT_SEARCH: 'https://api-staging-emea.ri-gateway.com/nextgenui/1.0/api/x/',
    BYPASS_MULE: false,
    SCREEN_MEMORY_THRESOLD: 50,
    FETCH_TRANSLATION_FROM_FIREBASE: true,
    FETCH_LOCALIZATION_FROM_FIREBASE: true,
    FIREBASE_TRANSLATION_URL: 'https://storage.googleapis.com/ri-newui-next-stg.appspot.com/i18n/',
    FIREBASE_LOCALIZATION_URL: 'https://storage.googleapis.com/ri-newui-next-stg.appspot.com/',
    /*FIREBASE_CONFIG: JSON.stringify({
        apiKey: "AIzaSyAO_6m4m8d0HAovjvIZiPS5raN8lu9COoc",
        authDomain: "ri-newui-stg-ac8c7.firebaseapp.com",
        databaseURL: "https://ri-newui-stg-ac8c7.firebaseio.com",
        projectId: "ri-newui-stg-ac8c7",
        storageBucket: "ri-newui-stg-ac8c7.appspot.com",
        messagingSenderId: "884561069214"
    }),*/
    DEFAULT_LOCALE: {
        'name': 'English (UK)',
        'localeKey': '',
        'language': 'English',
        'country': 'UK',
        'languageCode': 'en',
        'countryCode': 'GB',
        'localeCode': 'en-GB',
        'iCABSLanguageCode': 'ENG',
        'globalizeLocaleCode': 'en-GB',
        'dateLocaleCode': 'en-GB',
        'currencyCode': 'GBP',
        'globalizeParserLocaleCode': 'en-GB'
    },
    REGIONS: {
        'localeRegionList': [
            {
                'code': 'Select Region',
                'name': 'Select Region',
                'locale_key': 'Select Region'
            },
            {
                'code': 'Europe',
                'name': 'Europe',
                'locale_key': 'Europe'
            },
            {
                'code': 'Asia',
                'name': 'Asia North',
                'locale_key': 'Asia'
            },
            {
                'code': 'Hongkong',
                'name': 'Asia Central',
                'locale_key': 'Hongkong'
            },
            {
                'code': 'Indonesia',
                'name': 'Asia South',
                'locale_key': 'Indonesia'
            },
            {
                'code': 'asiaWest',
                'name': 'Asia West',
                'locale_key': 'asiaWest'
            },
            {
                'code': 'Africa',
                'name': 'Africa',
                'locale_key': 'Africa'
            },
            {
                'code': 'Australia',
                'name': 'Pacific',
                'locale_key': 'Pacific'
            },
            {
                'code': 'North America',
                'name': 'North America',
                'locale_key': 'America'
            },
            {
                'code': 'South America',
                'name': 'South America',
                'locale_key': 'America'
            },
            {
                'code': 'Caribbean',
                'name': 'Caribbean ',
                'locale_key': 'Caribbean'
            },
            {
                'code': 'MENAT',
                'name': 'MENAT ',
                'locale_key': 'MENAT'
            },
            {
                'code': 'asiaWest',
                'name': 'Asia West',
                'locale_key': 'asiaWest'
            }
        ]
    },

    MULE_CLIENT_ID: '49b13f926c15491d9c06acd685bcf858',
    MULE_CLIENT_SECRET: '11b34e0cbd3346db84A189C65061B44F',
    JIRA: 'https://rentokilinitial.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-9ng3ko/b/c/7ebd7d8b8f8cafb14c7b0966803e5701/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-UK&collectorId=2e3dc31f',
    GMAP_API_KEY: 'AIzaSyD7WKU_j5HvvTwRml9suRb2ZH3hg2INWhA',
    USER_ID_SALT: 'rentokil',
    USER_ID_HASH_LEN: '48',
    LOCALES: JSON.stringify(locales.localeCultureList),
    AUTH0_CLIENT_ID: 'jz3BqjCDeYMnZqrkXAPwJDMgZMM4emzD',
    AUTH0_DOMAIN: 'rentokil-initial.eu.auth0.com',
    AUTH0_SCOPE: 'openid profile email',
    FIREBASE_AU_NZ_FORM_URL: 'https://storage.googleapis.com/ri-newui-next-stg.appspot.com/commonforms/'
};

/*
* For easier debugging in development mode, you can import the following file
* to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
*
* This import should be commented out in production mode because it will have a negative impact
* on performance if an error is thrown.
*/
    // import 'zone.js/dist/zone-error',  // Included with Angular CLI.
