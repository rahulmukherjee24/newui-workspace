import { locales } from './globalize/locale';
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    NODE_ENV: 'DEVONQA',
    GCP_LOGIN_API: 'https://icabsui-api-europe-test.appspot.com/api/nextgenui/1.0/api/x/auths/login',
    MULE_LOGIN_API: 'https://api-test.ri-gateway.com?/nextgenui/1.0/api/x/auths/login',
    CLIENT_ID: '894251908073-1jvuee1e7uips930n6pb98u8inan4e18.apps.googleusercontent.com',
    SCOPE: 'profile email',
    GA_TRACK_CODE: 'UA-93679236-2',
    RESPONSE_TYPE: 'token',
    REDIRECT_URL: 'http://localhost:3000/application/login',
    OAUTH_URL: 'https://accounts.google.com/o/oauth2/auth?scope=',
    PROFILE_URL: 'https://www.googleapis.com/plus/v1/people/me',
    REVOKE_URL: 'https://accounts.google.com/o/oauth2/revoke?token=',
    LOGOUT_URL: 'https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:3000',
    TOKENINFO_URL: 'https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=',
    LOOK_UP: 'lookup/data',
    SYS_CHAR: 'settings/data',

    USER_ACCESS_URL: 'https://api-test.ri-gateway.com/nextgenui/1.0/api/x/user/menu',
    USER_CODE_URL: 'https://api-test.ri-gateway.com/nextgenui/1.0/api/x/user/information',
    USER_ACCESS_POSTFIX: 'user/menu',
    USER_CODE_POSTFIX: 'user/information',
    ZIP_SEARCH_URL: 'https://api-test.ri-gateway.com/nextgenui/x/1.0/contract-management/search',
    BASE_URL_EUROPE: 'https://api-test.ri-gateway.com/nextgenui/1.0/api/x/',
    BASE_URL_AMERICA: 'https://api-test.ri-gateway.com/nextgenui/1.0/api/x/',
    BASE_URL_AUSTRALIA: 'https://api-test.ri-gateway.com/nextgenui/1.0/api/x/',
    BASE_URL_ASIA: 'https://api-test.ri-gateway.com/nextgenui/1.0/api/x/',
    IMAGE_REPO_URL: 'assets/images/',

    INVOICE_HISTORY: 'http://localhost:3030/api/contractInvoiceHistory',
    CONTACT_SEARCH: 'https://api-test.ri-gateway.com/nextgenui/1.0/api/x/',
    SCREEN_MEMORY_THRESOLD: 50,
    FETCH_TRANSLATION_FROM_FIREBASE: true,
    FETCH_LOCALIZATION_FROM_FIREBASE: true,
    FIREBASE_TRANSLATION_URL: 'https://storage.googleapis.com/ri-newui-next-sit.appspot.com/i18n/',
    FIREBASE_LOCALIZATION_URL: 'https://storage.googleapis.com/ri-newui-next-sit.appspot.com/',
    DEFAULT_LOCALE: {
        'name': 'English (UK)',
        'localeKey': '',
        'language': 'English',
        'country': 'UK',
        'languageCode': 'en',
        'countryCode': 'GB',
        'localeCode': 'en-GB',
        'iCABSLanguageCode': 'ENG',
        'globalizeLocaleCode': 'en-GB',
        'dateLocaleCode': 'en-GB',
        'currencyCode': 'GBP',
        'globalizeParserLocaleCode': 'en-GB'
    },
    REGIONS: {
        'localeRegionList': [
            {
                'code': 'Select Region',
                'name': 'Select Region',
                'locale_key': 'Select Region'
            },
            {
                'code': 'Europe',
                'name': 'Europe',
                'locale_key': 'Europe'
            },
            {
                'code': 'Asia',
                'name': 'Asia North',
                'locale_key': 'Asia'
            },
            {
                'code': 'Hongkong',
                'name': 'Asia Central',
                'locale_key': 'Hongkong'
            },
            {
                'code': 'Indonesia',
                'name': 'Asia South',
                'locale_key': 'Indonesia'
            },
            {
                'code': 'asiaWest',
                'name': 'Asia West',
                'locale_key': 'asiaWest'
            },
            {
                'code': 'Africa',
                'name': 'Africa',
                'locale_key': 'Africa'
            },
            {
                'code': 'Australia',
                'name': 'Pacific',
                'locale_key': 'Pacific'
            },
            {
                'code': 'North America',
                'name': 'North America',
                'locale_key': 'America'
            },
            {
                'code': 'South America',
                'name': 'South America',
                'locale_key': 'America'
            },
            {
                'code': 'Caribbean',
                'name': 'Caribbean ',
                'locale_key': 'Caribbean'
            },
            {
                'code': 'MENAT',
                'name': 'MENAT ',
                'locale_key': 'MENAT'
            },
            {
                'code': 'India',
                'name': 'India',
                'locale_key': 'India'
            }
        ]
    },

    MULE_CLIENT_ID: 'e12d3a8c044241bd914bee403c71ec12',
    MULE_CLIENT_SECRET: '846bccb2257d43639E783C2126E5BD07',
    JIRA: '',
    GMAP_API_KEY: 'AIzaSyD7WKU_j5HvvTwRml9suRb2ZH3hg2INWhA',
    USER_ID_SALT: 'rentokil',
    USER_ID_HASH_LEN: '48',
    LOCALES: JSON.stringify(locales.localeCultureList),
    AUTH0_CLIENT_ID: '4mtwbO2obMF8y87tLTqnD1Qsy6bZDJv3',
    AUTH0_DOMAIN: 'rentokil-initial.eu.auth0.com',
    AUTH0_SCOPE: 'openid profile email',
    FIREBASE_AU_NZ_FORM_URL: 'https://storage.googleapis.com/ri-newui-next-sit.appspot.com/commonforms/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
