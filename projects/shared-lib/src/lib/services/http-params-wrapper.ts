export class QueryParams {
    private params: any = {};

    constructor(params?: any) {
        this.params = params || {};
    }

    public get(name: string): string {
        return this.params[name];
    }

    public set(name: string, value: any): void {
        if (value !== undefined && value !== null) {
            this.params[name] = value;
        }
    }

    public delete(name: string): void {
        delete this.params[name];
    }

    public getMap(): any {
        return this.params;
    }

    public keys(): string[] {
        return Object.keys(this.params);
    }
}
